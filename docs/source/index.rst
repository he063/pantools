PanTools version |ProjectVersion|
=================================

PanTools is a toolkit for comparative analysis of large number of
genomes. It is developed in the Bioinformatics Group of Wageningen
University, the Netherlands. Please cite the relevant publication(s)
from the list of publications if you use PanTools in your research.

Licence
-------

PanTools has been licensed under `GNU GENERAL PUBLIC LICENSE version 3.
<https://www.gnu.org/licenses/gpl-3.0.en.html>`_

Publications
------------

-  `Pantools v3: functional annotation, classification, and phylogenomics
   <https://doi.org/10.1093/bioinformatics/btac506>`_
-  `The Pectobacterium pangenome, with a focus on Pectobacterium
   brasiliense, shows a robust core and extensive exchange of genes
   from a shared gene pool
   <https://doi.org/10.1186/s12864-021-07583-5>`_
-  `Pan-genomic read mapping
   <https://www.biorxiv.org/content/10.1101/813634v1>`_
-  `Efficient inference of homologs in large eukaryotic pan-proteomes
   <https://bmcbioinformatics.biomedcentral.com/articles/10.1186/
   s12859-018-2362-4>`_
-  `PanTools: representation, storage and exploration of pan-genomic
   data.
   <https://academic.oup.com/bioinformatics/article/32/17/i487/2450785>`_

Functionalities
---------------

PanTools currently provides these functionalities:

-  Construction of a panproteome
-  Adding new genomes to the pangenome
-  Adding structural/functional annotations to the genomes
-  Detecting homology groups based on similarity of proteins
-  Optimization of homology grouping using BUSCO
-  Read mapping
-  Gene classification
-  Phylogenetic methods

Requirements
------------

-  \ **Java Virtual Machine**\  version 1.8 or higher, Add path to the
   java executable to your OS path environment variable.
-  \ **KMC**\ : A disk-based k-mer counter, After downloading the
   appropriate version (linux, macos or windows), add path to the *kmc*
   and *kmc_tools* executables to your OS path environment variable.
-  \ **MCL**\ : The Markov Clustering Algorithm, After downloading and
   compiling the software, add path to the *mcl* executable to your OS
   path environment variable.

For installing and configuring all required software, please see our
:doc:`/user_guide/install` page.

Running the program
-------------------

Make sure you can run java on your command line. Then you can run PanTools from
the command line by:

.. substitution-code-block:: bash

   $ java <JVM options> -jar pantools-|ProjectVersion|.jar <subcommand> <arguments>

| **Useful JVM options**
| - **-server** : To optimize JIT compilations for higher performance
| - **-Xmn(a number followed by m/g)** : Minimum heap size in mega/giga
  bytes
| - **-Xmx(a number followed by m/g)** : Maximum heap size in mega/giga
  bytes


Options
-------

All options except for ``--version`` also apply to all subcommands.

.. list-table::
   :widths: 30 70

   * - ``--version``/``-V``
     - Display version info.
   * - ``--help``/``-h``
     - Display a help message for pantools or any subcommand.
   * - ``--manual``/``-M``
     - Open the manual for pantools or any subcommand in a local browser.
   * - ``--force``/``-f``
     - Force. For instance, force overwrite a database with ``build_pangenome``.
   * - ``--no-input``
     - Ignore prompts or any other interactive user input.
   * - ``--debug``/``-d``
     - Show debug messages in the console.
   * - ``--quiet``/``-q``
     - Only show warnings or higher level logging messages in the console.

Contents
--------

.. toctree::
   :caption: User guide
   :maxdepth: 1

   Install <user_guide/install>
   user_guide/construct
   user_guide/characterize
   user_guide/phylogeny
   user_guide/msa
   user_guide/explore
   user_guide/mapping
   user_guide/query
   user_guide/differences
   user_guide/workflows

.. toctree::
   :caption: Tutorial
   :maxdepth: 1

   Tutorial 1 - Install PanTools <tutorial/tutorial_part1>
   Tutorial 2 - Construct pangenome <tutorial/tutorial_part2>
   Tutorial 3 - Neo4j browser <tutorial/tutorial_part3>
   Tutorial 4 - Characterization <tutorial/tutorial_part4>
   Tutorial 5 - Phylogeny <tutorial/tutorial_part5>

.. toctree::
   :caption: Developer guide
   :maxdepth: 1

   Installation <developer_guide/install>
   Release steps <developer_guide/release>
   Database structure <developer_guide/database>

