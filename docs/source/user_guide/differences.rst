Differences between pangenome and panproteome
=============================================

PanTools offers functionalities to build and analyze a pangenome or
panproteome.

A **pangenome** is constructed from genome and annotation files. First,
genome sequences are k-merized and compressed into a De Bruijn graph.
Genes and other annotation features from annotation files are integrated
into the pangenome as ‘gene’, ‘mRNA’ and ‘CDS’ nodes. Gene start and
stop positions are annotated in the graph as relationships and connect
the annotation layer to the nucleotide layer. The protein sequences can
be clustered into homology groups and connect homologous proteins from
different genomes.

A **panproteome** is built from protein sequences only, ignoring the
underlying genome structure. Again, the protein sequences are clustered
into homology groups which serve as main input for many functionalities.

In addition to the single layer in panproteomes and three layers in
pangenomes, a functional layer can be included in both databases. This
layer consists of multiple functional annotation databases (e.g. GO,
PFAM) and connects proteins with a shared function.

Since there is only a protein layer and functional layer present in
panproteomes, not all functions can be utilized. See the table below for
which functions can be used for pangenomes and panproteomes.

.. figure:: /figures/layers.png
   :width: 600
   :align: center

   *Schematic of genome, annotation, and protein layer of a pangenome
   database. Figure taken from Efficient inference of homologs in large
   eukaryotic pan-proteomes*

Available functions
-------------------
:doc:`/user_guide/construct`

.. csv-table::
   :file: /tables/differences_construct.csv
   :header-rows: 1
   :delim: ;

:doc:`/user_guide/characterize`

.. csv-table::
   :file: /tables/differences_characterize.csv
   :header-rows: 1
   :delim: ;

:doc:`/user_guide/explore`

.. csv-table::
   :file: /tables/differences_explore.csv
   :header-rows: 1
   :delim: ;

:doc:`/user_guide/phylogeny`

.. csv-table::
   :file: /tables/differences_phylogeny.csv
   :header-rows: 1
   :delim: ;

:doc:`/user_guide/mapping`

.. list-table::
   :header-rows: 1

   * - Function
     - Pangenome
     - Panproteome
   * - Map
     - :green:`YES`
     - :red:`NO`
