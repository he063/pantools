Creating PanTools releases
==========================

The following steps are required to create a new release of PanTools:

1. Create a new branch for the release, e.g. ``release_v4.0.0``.
2. Update the version number in ``pom.xml`` and ``docs/source/conf.py``.
3. Put a date at the top of the ``CHANGELOG.md`` file.
4. Create a merge request for the **new release branch** to be merged into
   ``develop``.
5. Create a merge request for the **new release branch** to be merged into the
   **current stable release branch** (*e.g.* ``pantools_v4``).
6. Test the **new release branch** and fix any important bugs.
7. Merge the **new release branch** into ``develop``.
8. Merge the **new release branch** into the **current stable release branch**
   and delete the **new release branch**.
9. Put a tag on the merge commit in the **current stable release branch**.
10. Locally compile PanTools and put the compiled JAR file on the server.
11. Create a release from the tag and attach 1) the compiled JAR file, 2) the
    ``conda_linux.yaml`` file, and 3) the ``conda_macos.yaml`` file.
12. Update the bioconda recipe for the new release.
