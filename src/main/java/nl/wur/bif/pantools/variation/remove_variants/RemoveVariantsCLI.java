package nl.wur.bif.pantools.variation.remove_variants;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.utils.GraphUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static picocli.CommandLine.*;

/**
 * Removes variants from the pantools database.
 *
 * @author Dirk-Jan van Workum, Wageningen University, the Netherlands.
 */
@Command(name = "remove_variants", aliases = "remove_variant", sortOptions = false)
public class RemoveVariantsCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Override
    public Integer call() {
        // initialize logging
        pantools.createLogger(spec);

        // validate command line arguments
        BeanUtils.argValidation(spec, this);

        // initialize the neo4j graph database
        final Path databaseDirectory = pantools.getDatabaseDirectory();
        GraphUtils.createGraphDatabaseService(databaseDirectory);
        GraphUtils.setDatabaseParameters();

        // validate the neo4j graph database
        GraphUtils.validateAccessions("VCF");

        // run the main code
        new RemoveVariants();

        // return exit code 0 if successful
        return 0;
    }
}
