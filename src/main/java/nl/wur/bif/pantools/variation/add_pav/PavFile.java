package nl.wur.bif.pantools.variation.add_pav;

import nl.wur.bif.pantools.pantools.Pantools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

/**
 * Class for parsing a PAV file and storing its contents.
 */
public class PavFile {
    private final Path pavFilePath;
    private final Set<String> accessionIds;
    private final Set<String> mrnaNames;
    private final Map<String, Integer> pavMap;

    public PavFile(Path pavFilePath) throws IOException {
        this.pavFilePath = pavFilePath;
        this.accessionIds = new HashSet<>();
        this.mrnaNames = new HashSet<>();
        this.pavMap = new HashMap<>();
        parsePavFile();
    }

    /**
     * Parse the pav file
     */
    private void parsePavFile() throws IOException {
        // read the pav file
        final BufferedReader br = new BufferedReader(new FileReader(pavFilePath.toFile()));

        // validate header
        String line = br.readLine();
        final List<String> headers = Arrays.asList(line.split("\\t"));
        final int numFields = headers.size();
        if (numFields == 1) {
            Pantools.logger.error("The header of the pav file is not in the correct format.");
            throw new RuntimeException("Invalid header for PAV file");
        }
        final List<String> accessionIds = headers.subList(1, numFields);
        this.accessionIds.addAll(accessionIds);

        // go over all lines in the file
        while ((line = br.readLine()) != null) {
            final List<String> fields = Arrays.asList(line.split("\\t"));
            if (fields.isEmpty()) continue;
            // check that the line contains the correct number of fields
            if (fields.size() != numFields) {
                Pantools.logger.error("The PAV file is not in the correct format: expected {} fields, found {} fields.",
                        numFields, fields.size());
                throw new RuntimeException("Invalid number of fields in PAV file");
            }

            // get the mRNA name from the first column
            final String mrnaName = fields.get(0);
            if (mrnaNames.contains(mrnaName)) {
                Pantools.logger.error("The mRNA name {} is not unique.", mrnaName);
                throw new RuntimeException("Duplicate mRNA names in PAV file");
            }
            mrnaNames.add(mrnaName);

            // loop over other fields and add them to the map
            for (int i = 1; i < fields.size(); i++) {
                final String accessionId = accessionIds.get(i - 1);

                // check that the PAV value is an integer
                final int pavValue;
                try {
                    pavValue = Integer.parseInt(fields.get(i));
                } catch (NumberFormatException nfe) {
                    throw new RuntimeException(
                            "The pav file is not in the correct format: expected integer values for PAV values.", nfe);
                }

                // check that the PAV value is either 0 or 1
                if (pavValue != 0 && pavValue != 1) {
                    throw new RuntimeException(
                            "The pav file is not in the correct format: expected 0 or 1 for PAV values.");
                }

                // add the PAV value to the map
                pavMap.put(String.format("%s_%s", accessionId, mrnaName), pavValue);
            }
        }
    }

    /**
     * Get the PAV value for a given accession name and mRNA
     *
     * @param accessionId the accession name
     * @param mRNA        the mrna name
     * @return the PAV value
     * @throws IllegalArgumentException if the accession name or mRNA name is not found
     */
    public int getPavValue(String accessionId, String mRNA) throws IllegalArgumentException {
        if (!accessionIds.contains(accessionId)) {
            throw new IllegalArgumentException("The accession name " + accessionId + " is not found in the pav file");
        }
        if (!mrnaNames.contains(mRNA)) {
            throw new IllegalArgumentException("The mRNA name " + mRNA + " is not found in the pav file");
        }
        return pavMap.get(accessionId + "_" + mRNA);
    }

    /**
     * Get all accession IDs from pavMap
     *
     * @return all accession IDs
     */
    public Set<String> getAccessionIds() {
        return accessionIds;
    }

    /**
     * Get all mrna names from pavMap
     *
     * @return all mrna names
     */
    public Set<String> getMrnaNames() {
        return mrnaNames;
    }

    /**
     * Get the file path to the pav file
     *
     * @return the file path to the pav file
     */
    public Path getFilePath() {
        return pavFilePath;
    }
}
