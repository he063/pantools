package nl.wur.bif.pantools.variation.add_pav;

import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.variation.AddVariation;
import org.neo4j.graphdb.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

import static nl.wur.bif.pantools.utils.Globals.*;

/**
 * This class is used to add PAV variation to the pangenome graph.
 * Here, we handle accession nodes and variant nodes.
 * Accession nodes contain metadata about the added variation and are connected to the genome node in case of a
 * pangenome.
 * Variant nodes contain the actual variation and are connected to the mRNA nodes.
 * In case of a pangenome, the variant nodes also contain their consensus (nucleotide) sequence.
 *
 * @author Dirk-Jan van Workum
 */
public class AddPav extends AddVariation {

    private final Path pavLocationsFile;
    private final Map<Path, PavFile> pavFileMap;

    public AddPav(Path pavLocationsFile) throws IOException {
        super();
        this.pavLocationsFile = pavLocationsFile;
        this.pavFileMap = new HashMap<>();
        addPav();
    }

    /**
     * Main method for adding PAVs to the pangenome (pantools add_pav <input file>)
     * Read presence-absence variation from provided input files and add them to the pangenome.
     */
    public void addPav() throws IOException {
        Pantools.logger.info("Adding PAVs to the database.");

        // read the pavLocationsFile and check that it is valid
        Pantools.logger.info("Reading the provided PAV file list.");
        final Map<Path, Integer> pavFilePaths = readVariationLocationsFile(pavLocationsFile, "PAV");
        validateVariationFiles(pavFilePaths, "PAV");

        // per PAV file, add the PAV information to the database
        for (Map.Entry<Path, Integer> entry : pavFilePaths.entrySet()) {
            final Path file = entry.getKey();
            final PavFile pavFile = pavFileMap.get(file);
            final Set<String> samples = pavFile.getAccessionIds();
            final int genomeNr = entry.getValue();

            Pantools.logger.info("Adding PAVs from {} to genome {}.", file, genomeNr);
            Pantools.logger.debug("Adding {} samples ({}) to genome {}.",
                    samples.size(), String.join(", ", samples), genomeNr);

            try (Transaction tx = GRAPH_DB.beginTx()) {
                // separate pangenome and panproteome logic because there are no genome nodes in a panproteome
                if (PROTEOME) {
                    // add the accession to the pangenome by creating an accession node
                    createAccessionNodes(genomeNr, samples, "PAV");
                } else {
                    final Node genomeNode = GRAPH_DB.findNode(GENOME_LABEL, "number", genomeNr);

                    // skip if the genome is not annotated
                    if (!genomeNode.hasRelationship(RelTypes.annotates, Direction.INCOMING)) {
                        Pantools.logger.warn("Genome {} is not annotated; Skipping.", genomeNr);
                        continue;
                    }

                    // add the accession to the pangenome by creating an accession node
                    final Set<Node> accessionNodes = createAccessionNodes(genomeNr, samples, "PAV");
                    connectAccessionNodes(genomeNode, accessionNodes);
                }
                // add the PAV file to the database
                addPavNodes(genomeNr, pavFile);
                tx.success();
            }
        }
        Pantools.logger.info("Finished adding PAV information to the pangenome.");
    }

    /**
     * Adds PAVs of all accessions for a genome to the pangenome. It will do so by adding a property "present" to the
     * variant nodes for this genome and setting the property "present" to true when the mRNA node has a PAV value of 1.
     *
     * @param genomeNr the node of the genome
     * @param pavFile the file containing the PAVs for the genome
     * @throws NotFoundException if no mRNA nodes could be found
     * @throws NullPointerException if the PAV value could not be found
     */
    private void addPavNodes(int genomeNr, PavFile pavFile) throws NotFoundException, NullPointerException {

        // get all the samples from the pavFile
        final Set<String> samples = pavFile.getAccessionIds();
        Pantools.logger.debug("PAV file {} contains {} samples.", pavFile.getFilePath(), samples.size());
        Pantools.logger.trace("Samples: {}.", samples.toString());
        Pantools.logger.debug("PAV file {} contains {} mRNA names.",
                pavFile.getFilePath(), pavFile.getMrnaNames().size());
        Pantools.logger.trace("mRNA names: {}.", pavFile.getMrnaNames());

        // get all the mRNA nodes for the genome and store in an arraylist
        final List<Node> mrnaNodes = new ArrayList<>();
        try (ResourceIterator<Node> mrnaNodeIterator = GRAPH_DB.findNodes(MRNA_LABEL, "genome", genomeNr)) {
            while (mrnaNodeIterator.hasNext()) {
                mrnaNodes.add(mrnaNodeIterator.next());
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("No mRNA nodes could be found for genome {}.", genomeNr);
            throw nfe;
        }

        Pantools.logger.debug("Found {} mRNA nodes for genome {}: {}.", mrnaNodes.size(), genomeNr, mrnaNodes);

        // loop over all mRNA nodes and add the "present" property to the variant nodes (0=absent, 1=present)
        for (final Node mrnaNode : mrnaNodes) {

            // get the ID of the mRNA node
            final String mrnaId = getMrnaId(mrnaNode);
            Pantools.logger.trace("Adding PAVs for mRNA {}.", mrnaId);

            // create a map of samples and nodes of all variant nodes of the mRNA node
            final Map<String, Node> variantNodes = new HashMap<>();
            final Iterable<Relationship> rels = mrnaNode.getRelationships(RelTypes.has_variant, Direction.OUTGOING);
            for (Relationship rel : rels) {
                final Node variantNode = rel.getEndNode();
                final String variantId = (String) variantNode.getProperty("sample");
                variantNodes.put(variantId, variantNode);
            }

            // loop over all samples and add the "present" property to the variant nodes (1=present, 0=absent)
            for (final String sample : samples) {
                // check if variant node already exists, if not create it
                final Node variantNode;
                if (variantNodes.containsKey(sample)) {
                    variantNode = variantNodes.get(sample);
                } else {
                    Pantools.logger.trace("Creating variant node for sample {} and mrna {}.", sample, mrnaId);
                    variantNode = createVariantNode(mrnaNode, sample);
                }

                // add the "present" property to the variant node
                try {
                    final int pavValue = pavFile.getPavValue(sample, mrnaId);
                    variantNode.setProperty("present", (pavValue == 1));
                } catch (IllegalArgumentException iae) {
                    Pantools.logger.warn("No PAV value for sample {} for mRNA {}; Assuming variant is absent.",
                            sample, mrnaId);
                    variantNode.setProperty("present", false);
                }
            }
        }
    }

    /**
     * Get the identifier of a given mRNA node
     * @param mrnaNode the mRNA node
     * @return the identifier of the mRNA node
     */
    private String getMrnaId(Node mrnaNode) {
        final String mrnaId;
        if (PROTEOME) {
            mrnaId = (String) mrnaNode.getProperty("protein_ID");
        } else {
            mrnaId = (String) mrnaNode.getProperty("id");
        }
        assert mrnaId != null;
        return mrnaId;
    }

    /**
     * Adds information from a pav file to an accession node
     * @param accessionNode accession node
     */
    @Override
    protected void addAccessionNodeInformation(Node accessionNode) {
        accessionNode.setProperty("PAV", true);
        if (!accessionNode.hasProperty("VCF")) {
            accessionNode.setProperty("VCF", false);
        }
    }

    /**
     * Get PAV samples from PAV files. If the PAV file is not initialized, parse it first.
     * @param pavFilePath path to the PAV file
     * @return set of PAV samples
     * @throws IOException if the PAV file cannot be parsed
     */
    @Override
    protected Set<String> getSamples(Path pavFilePath) throws IOException {
        if (this.pavFileMap.containsKey(pavFilePath)) {
            return this.pavFileMap.get(pavFilePath).getAccessionIds();
        }
        final PavFile pavFile = new PavFile(pavFilePath);
        this.pavFileMap.put(pavFilePath, pavFile);
        return pavFile.getAccessionIds();
    }
}
