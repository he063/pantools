package nl.wur.bif.pantools.variation.add_variants.parallel;

import nl.wur.bif.pantools.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.Utils.delete_file_full_path;

/**
 * Class for creating variant nodes for one sample with respect to one genome
 */
public class CreateVariantFasta implements Callable<String> {
    private final Path genomeFastaFile;
    private final String sample;
    private final Path vcfFile;
    private final Path variantFastaFile;
    private final boolean keepTemporaryFiles;

    /**
     * Constructor
     *
     * @param genomeFastaFile    genome fasta file containing all sequences that need variants
     *                           (the fasta headers should be ">sequence_name:start-end feature" for each feature)
     * @param sample             sample name
     * @param vcfFile            path to vcf file (gzipped and tabix indexed)
     * @param variantFastaFile   path to output fasta file
     * @param keepTemporaryFiles if true, the temporary files will not be deleted
     */
    public CreateVariantFasta(Path genomeFastaFile, Path vcfFile, String sample, Path variantFastaFile, boolean keepTemporaryFiles) {
        this.genomeFastaFile = genomeFastaFile;
        this.sample = sample;
        this.vcfFile = vcfFile;
        this.variantFastaFile = variantFastaFile;
        this.keepTemporaryFiles = keepTemporaryFiles;
    }

    /**
     * This method creates a consensus sequence from the VCF file and the genome sequence.
     * The consensus sequence is created by bcftools.
     *
     * @return the variant sequence
     */
    @Override
    public String call() {
        final Path logFile = Paths.get(variantFastaFile + ".log");

        //create bcftools consensus command
        final List<String> cmd = new ArrayList<>();
        final String bcftoolsCommand = String.format(
                "bcftools consensus -I --mark-ins lc --mark-snv lc -s %s -f %s %s 1> %s 2> %s",
                sample, genomeFastaFile, vcfFile, variantFastaFile, logFile);
        cmd.add("sh");
        cmd.add("-c");
        cmd.add(bcftoolsCommand);

        Pantools.logger.debug("Creating variant fasta file for sample {} with command: {}", sample, cmd);

        //run bcftools consensus
        final ProcessBuilder processBuilder = new ProcessBuilder(cmd);
        final Process process;
        try {
            process = processBuilder.start();
            int exitCode = process.waitFor();
            if (exitCode != 0) {
                Pantools.logger.error("Something went wrong when running bcftools consensus.");
                throw new RuntimeException("Non-zero exit code from bcftools");
            }
        } catch (IOException | InterruptedException ioe) {
            Pantools.logger.error("Something went wrong when running bcftools consensus.");
            throw new RuntimeException("Error running bcftools");
        }

        if (!keepTemporaryFiles) {
            Pantools.logger.debug("Deleting log file {}", logFile);
            delete_file_full_path(logFile);
        }

        return sample;
    }
}