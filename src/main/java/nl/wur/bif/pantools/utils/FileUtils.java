package nl.wur.bif.pantools.utils;

import nl.wur.bif.pantools.pantools.Pantools;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.Comparator;

import java.util.Scanner;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

import static org.apache.logging.log4j.core.util.Loader.getClassLoader;

public final class FileUtils {

    private FileUtils() {}

    /**
     * Load a matrix file from the project resources.
     * @param matrixName name of the scoring matrix file
     * @return scoring matrix that can be indexed by char values
     */
    public static int[][] loadScoringMatrix(String matrixName) {
        // initialize empty matrix and column headers
        final int[][] scoringMatrix = new int[256][256];
        char[] headers = null;

        // load matrix file from project resources
        final String resourceLocation = Paths.get("scoring-matrices").resolve(matrixName).toString();
        final InputStream inputStream = getClassLoader().getResourceAsStream(resourceLocation);
        assert inputStream != null;
        final Scanner matrixFile = new Scanner(inputStream);

        // scan the matrix
        while (matrixFile.hasNextLine()) {
            final String line = matrixFile.nextLine();
            if (line.length() == 0 || line.startsWith("#")) continue;
            if (headers == null) {
                // set header char values
                headers = line.replaceAll("\\s+", "").toCharArray();
                continue;
            }
            // add values to matrix using char col/row headers as indices
            final String[] values = line.split("\\s+");
            final char a = values[0].charAt(0);
            for (int i = 1; i < values.length; i++) {
                final char b = headers[i - 1];
                scoringMatrix[a][b] = Integer.parseInt(values[i]);
            }
        }
        matrixFile.close();
        return scoringMatrix;
    }

    /**
     * Add a shutdown hook to a temporary directory to remove it on system exit.
     * @param directory path of temporary directory
     */
    public static void addShutDownHook(Path directory) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                org.apache.commons.io.FileUtils.deleteDirectory(directory.toFile());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }));
    }

    /**
     * Unarchive a tarball
     * @param tarballPath path to the tarball
     * @param outputDirectory directory where the tarball will be unarchived
     */
    public static void unarchiveTarball(Path tarballPath, Path outputDirectory) {
        int count = 0;
        try (TarArchiveInputStream tarIn = new TarArchiveInputStream(new GzipCompressorInputStream(new BufferedInputStream(new FileInputStream(tarballPath.toFile()))))) {
            TarArchiveEntry entry;
            while ((entry = tarIn.getNextTarEntry()) != null) {
                File outputFile = new File(outputDirectory.toFile(), entry.getName());
                if (entry.isDirectory()) {
                    outputFile.mkdirs();
                } else {
                    count += 1;
                    try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outputFile))) {
                        IOUtils.copy(tarIn, bos);
                    }
                }
            }
        } catch (IOException e) {
            Pantools.logger.error("Unable to unarchive tarball.", e);
        }
        Pantools.logger.debug("{} files unarchived from {}", count, tarballPath);

        // remove tarball
        try {
            Files.delete(tarballPath);
        } catch (IOException e) {
            Pantools.logger.error("Unable to remove tarball.", e);
        }
    }

    /**
     * Decompresses a GZIP file
     * @param file file to decompress
     */
    public static void decompressGzipFile(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            GZIPInputStream gis = new GZIPInputStream(fis);
            FileOutputStream fos = new FileOutputStream(file.getAbsolutePath().replace(".gz", ""));
            byte[] buffer = new byte[1024];
            int len;
            while((len = gis.read(buffer)) != -1){
                fos.write(buffer, 0, len);
            }
            fos.close();
            gis.close();
        } catch (IOException e) {
            Pantools.logger.error("Unable to decompress file {}", file.getAbsolutePath());
            e.printStackTrace();
        }
        Pantools.logger.debug("File {} decompressed.", file.getAbsolutePath());

        // remove gz file
        try {
            Files.delete(file.toPath());
        } catch (IOException e) {
            Pantools.logger.error("Unable to remove gz file.", e);
        }
    }

    /**
     * Converts afa format to fasta format
     * @param inputFasta original fasta file
     * @param inputAfa aligned fasta file in CLUSTAL afa format
     *
     * Requires original fasta input file and aligned output from mafft
     * mafft is run with a setting so that the original order of sequences remains
     */
    public static void convertAfaToAlignedFasta(Path inputFasta, Path inputAfa) {
        HashMap<Integer, StringBuilder> sequenceMap = new HashMap<>();
        ArrayList<String> sequenceList = new ArrayList<>();
        int totalSequences = 0;
        try (BufferedReader in = Files.newBufferedReader(inputFasta)) { // read original fasta files
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (line.startsWith(">")) {
                    totalSequences++;
                    String key = line.replace(">","");
                    StringBuilder builder = new StringBuilder();
                    sequenceMap.put(totalSequences, builder);
                    sequenceList.add(key);
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read the input fasta: {}", inputFasta);
            System.exit(1);
        }

        try (BufferedReader in = Files.newBufferedReader(inputAfa)) {
            int lineCounter = 0, seqCounter = 0;
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                lineCounter ++;
                if (lineCounter == 1) {
                    if (!line.startsWith("CLUSTAL")) {
                        Pantools.logger.error("File is not in afa (CLUSTAL) format: {}.", inputAfa);
                        System.exit(1);
                    }
                }
                if (lineCounter < 4) {
                    continue;
                }
                seqCounter ++;
                if (seqCounter == totalSequences+1) { // first row after sequences with **
                    continue;
                }
                if (seqCounter == totalSequences+2) { // always an empty line
                    seqCounter = 0;
                    continue;
                }
                String[] lineArray = line.split(" ");
                StringBuilder seqBuilder = sequenceMap.get(seqCounter);
                seqBuilder.append(lineArray[lineArray.length-1]).append("\n");
                sequenceMap.put(seqCounter, seqBuilder);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read the input alignment: {}.", inputAfa);
            System.exit(1);
        }

        String outputFile = inputAfa.toString().replace(".afa", ".fasta");
        try (BufferedWriter out1 = new BufferedWriter(new FileWriter(outputFile))) {
            for (int i = 1; i <= totalSequences; i++) {
                StringBuilder value = sequenceMap.get(i);
                out1.write(">" + sequenceList.get(i-1) + "\n" +value.toString());
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read: {}.", outputFile);
            System.exit(1);
        }
    }

    /*
     * Create a scratch directory. If it set to null, a new scratch directory will be created by
     * {@link Files#createTempDirectory(String, FileAttribute[])}. If it is set, it is checked whether the directory is
     * empty. If it is not empty a RuntimeException will be thrown. If the directory does not exist, it will be created.
     * @return path to the scratch directory.
     */
    public static Path createScratchDirectory(Path directory) throws IOException {
        if (directory == null)
            return Files.createTempDirectory("pantools-scratch-");
        else {
            // If a path to a scratch directory is given, make sure it is empty OR create it
            if (Files.notExists(directory))
                Files.createDirectory(directory);
            else {
                try (Stream<Path> files = Files.list(directory)) {
                    if (files.findAny().isPresent())
                        throw new RuntimeException("user-specified scratch directory " + directory + " should be empty");
                } catch (NotDirectoryException ignored) {
                    throw new RuntimeException("user-specified scratch directory " + directory + " is not a directory");
                }
            }
        }
        return directory;
    }

    /**
     * Delete a directory and its contents.
     * @param directory path to directory.
     */
    public static void deleteDirectoryRecursively(Path directory) throws IOException {
        try (Stream<Path> paths = Files.walk(directory)) {
            //noinspection ResultOfMethodCallIgnored
            paths
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
    }

    /**
     * Calculates size of a given folder in MB.
     *
     * @param dir The folder File object.
     * @return Size of the folder in MB
     */
    public static long getFolderSize(File dir) {
        long size = 0;
        for (File file : dir.listFiles()) {
            if (file.isFile()) {
                size += file.length();
            } else {
                size += getFolderSize(file);
            }
        }
        return size / 1048576 + 1;
    }

}
