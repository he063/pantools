package nl.wur.bif.pantools.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Class with string parsing utility functions
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public final class StringUtils {

    private StringUtils() {}

    /**
     * Converts a string of comma separated integers to a list of integer. Returns an empty list if the input cannot
     * be converted.
     *
     * @param value input string
     * @return list of integers
     */
    public static List<Integer> stringToIntegerList(String value) {
        if (!value.matches("[0-9,]+") || value.contains(",,") || value.matches(",.*")) {
            return Collections.emptyList();
        } else {
            return parseStringToIntegerList(value);
        }
    }

    /**
     * Converts a string of comma separated integers to a list of integers.
     * @param value string of comma separated integer values
     * @return list of integers
     */
    private static List<Integer> parseStringToIntegerList(String value) {
        return Arrays.stream(value.split(",")).map(Integer::parseInt).collect(Collectors.toList());
    }

    /**
     * Converts a string of comma separated integers to a list of longs. Returns an empty list if the input cannot
     * be converted.
     *
     * @param value input string
     * @return list of longs
     */
    public static List<Long> stringToLongList(String value) {
        if (!value.matches("[0-9,]+") || value.contains(",,") || value.matches(",.*")) {
            return Collections.emptyList();
        } else {
            return parseStringToLongList(value);
        }
    }

    /**
     * Converts a string of comma separated integers to a list of longs.
     * @param value string of comma separated integer values
     * @return list of longs
     */
    private static List<Long> parseStringToLongList(String value) {
        return Arrays.stream(value.split(",")).map(Long::parseLong).collect(Collectors.toList());
    }

    /**
     * Converts a string of two integer values separated by a "-" into a list of integers containing the full range
     * including and between both integers.
     *
     * @param value input string matching "[0-9]+-[0-9]+"
     * @return list of integers
     */
    public static List<Integer> stringToIntegerRange(String value) {
        final String[] arrayRange = value.split("-");
        final IntStream range = IntStream.range(Integer.parseInt(arrayRange[0]), Integer.parseInt(arrayRange[1]) + 1);
        return range.boxed().collect(Collectors.toList());
    }

    /**
     * Convert camelCase strings to kebab-case.
     * @param str camelCase string
     * @return kebab-case converted str parameter
     */
    public static String camelToKebab(String str) {
        // Regular Expression
        final String regex = "([a-z])([A-Z]+)";

        // Replacement string
        final String replacement = "$1-$2";

        // Replace the given regex with replacement string and convert it to lower case.
        return str.replaceAll(regex, replacement).toLowerCase();
    }
}
