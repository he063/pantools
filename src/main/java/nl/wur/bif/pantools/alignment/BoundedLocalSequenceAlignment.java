package nl.wur.bif.pantools.alignment;

import nl.wur.bif.pantools.utils.FileUtils;

import java.util.Stack;

/**
 * Implements required functionalities for bounded pseudo-global alignment of
 * two nucleotide/peptide sequences 
 * 
 * @author Siavash Sheikhizadeh, Bioinformatics chairgroup, Wageningen
 * University, the Netherlands
 */
public class BoundedLocalSequenceAlignment {

    public static int match[][];
    private int matrix[][];
    private char direction[][];
    private int up[][];
    private int left[][];
    private StringBuilder seq1;
    private StringBuilder seq2;
    private StringBuilder cigar;
    private Stack<Character> operation_stack;
    private Stack<Integer> count_stack;
    private int similarity;
    private double identity;
    private int GAP_OPEN;
    private int GAP_EXT;
    private int MAX_LENGTH;
    private int[] score_array;
    private int max_i;
    private int max_j;
    private int deletions;
    private int insertions;
    private int BOUND;
    private String TYPE;
    private int offset;
    private int range_len;
    private int mismatch_penalty, insertion_penalty;
    private int CLIPPING_STRIGENCY;
    
   
    /**
     * Initializes the alignment object. 
     * 
     * @param gap_open The gap opening penalty
     * @param gap_ext The gap extension penalty 
     * @param max_length The maximum possible length of the alignment
     * @param band Length of the he band
     * @param clip The stringency of soft-clipping in the range [0..3]
     * @param type Type of the input sequences(N for nucleotide P for peptide). 
     */
    public BoundedLocalSequenceAlignment(int gap_open, int gap_ext, int max_length, int band, int clip, String type) {
        int i, j;
        seq1 = new StringBuilder();
        seq2 = new StringBuilder();
        GAP_OPEN = gap_open;
        GAP_EXT = gap_ext;
        MAX_LENGTH = max_length;
        BOUND = band;
        TYPE = type;
        CLIPPING_STRIGENCY = clip;
        cigar = new StringBuilder();
    // initialize matrixes
        matrix = new int[MAX_LENGTH + 1][2 * BOUND + 3];
        direction = new char[MAX_LENGTH + 1][2 * BOUND + 3];
        up = new int[MAX_LENGTH + 1][2 * BOUND + 3];
        left = new int[MAX_LENGTH + 1][2 * BOUND + 3];
        score_array = new int[MAX_LENGTH];
        operation_stack = new Stack();
        count_stack = new Stack();
        direction[0][0] = 'M';
        matrix[0][0] = 0;
        up[0][0] = left[0][0] = -1000;
        for (i = 1; i <= MAX_LENGTH; i++) {
            direction[i][0] = 'M';
            direction[i][2 * BOUND + 2] = 'M';
            // below the bound
            up[i][0] = -1000;
            left[i][0] = -1000;
            matrix[i][0] = 0;
            // above the bound
            up[i][2 * BOUND + 2] = -1000;
            left[i][2 * BOUND + 2] = -1000;
            matrix[i][2 * BOUND + 2] = 0;
        }
        for (j = 1; j <= 2 * BOUND + 2; j++) {
            direction[0][j] = 'D';
            up[0][j] = -1000;
            left[0][j] = -1000;
            matrix[0][j] = 0;
        }
        match = FileUtils.loadScoringMatrix(TYPE);
        switch (CLIPPING_STRIGENCY) {
            case 1:
                mismatch_penalty = -1;
                insertion_penalty = -1;
                break;
            case 2:
                mismatch_penalty = -4;
                insertion_penalty = -2;
                break;
            case 3:
                mismatch_penalty = -9;
                insertion_penalty = -3;
                break;
        }
    }

    /**
     * Fills the the similarity and direction matrixes of the two input sequences.
     * First sequence should not be longer than the second sequence.
     * 
     * @param s1 The String containing the first sequence
     * @param s2 The String containing the second sequence
     */
    public void align(String s1, String s2) {
        int i, j, stop;
        int m, n, d;
        seq1.setLength(0); 
        seq1.append(s1);
        seq2.setLength(0); 
        seq2.append(s2);
        m = seq1.length();
        n = seq2.length();
        if (m < MAX_LENGTH) {
            similarity = Integer.MIN_VALUE;
            for (i = 1; i <= m; i++) {
                stop = 2 * BOUND + 1;
                for (j = 1; j <= stop; j++) {
                    up[i][j] = Math.max( up[i-1][j+1] + GAP_EXT , Math.max(matrix[i-1][j+1], left[i-1][j+1]) + GAP_OPEN + GAP_EXT);
                    left[i][j] = Math.max( left[i][j-1] + GAP_EXT , Math.max(matrix[i][j-1], up[i][j-1]) + GAP_OPEN + GAP_EXT);
                    d = match[seq1.charAt(i-1)][seq2.charAt(j+i-2)] + matrix[i-1][j];
                    if (d >= Math.max( up[i][j] , left[i][j])) {
                        matrix[i][j] = d;
                        direction[i][j] = 'M';
                    } else if (left[i][j] > up[i][j]) {
                        matrix[i][j] = left[i][j];
                        direction[i][j] = 'D';
                    } else {
                        matrix[i][j] = up[i][j];
                        direction[i][j] = 'I';
                    }
                    if (matrix[i][j] > similarity) {
                        similarity = matrix[i][j];
                        max_i = i;
                        max_j = j;
                    }
                    //Pantools.logger.info("{}", matrix[i][j] );
                    //Pantools.logger.info("{}", direction[i][j]);
                    //Pantools.logger.info("{}", left[i][j]);
                    //Pantools.logger.info("{}", up[i][j]);
                }
                //System.out.println();
            }
        } else {
            throw new RuntimeException("Sequences are too large for the aligner.");
        }
        /*Pantools.logger.info("m: {} n: {}", m, n);
        Pantools.logger.info("Coordinates = {} {}", max_i, max_j);
        Pantools.logger.info(this.get_alignment());
        Pantools.logger.info(this.get_cigar());
        Pantools.logger.info("offset: {}", this.get_offset());
        Pantools.logger.info("{}\n", similarity_score);*/
    }

    /**
     * Calculates the alignment from the similarity matrix. 
     * Call align() before this function.
     * 
     * @return the alignment string which may contains some gaps. 
     */
    public String get_alignment() {
        int i, j;
        int range[] = new int[]{1,max_i, max_j};
        StringBuilder subject = new StringBuilder();
        StringBuilder query = new StringBuilder();
        subject.setLength(0);
        query.setLength(0);
        i = max_i;
        j = max_j;
        if (CLIPPING_STRIGENCY > 0)
            range = calculate_clip_range();
        while (i > 0 && j > 0) {
            if (CLIPPING_STRIGENCY > 0 && i < range[0])
                break;
            if (direction[i][j] == 'I') {
                query.append( seq1.charAt(i-1) );
                subject.append( '-' );
                i = i - 1;
                j = j + 1;
            } else if (direction[i][j] == 'D') {
                query.append( '-' );
                subject.append( seq2.charAt(j+i-2) );
                j = j - 1;
            } else {
                query.append( seq1.charAt(i-1) );
                subject.append( seq2.charAt(j+i-2) );
                i = i - 1;
            }
        } 
        if (CLIPPING_STRIGENCY > 0) {
            for (;i > 0 && j > 1; --i, --j) {
                query.append( seq1.charAt(i-1) );
                subject.append( seq2.charAt(j+i-2) );
            }
        }
        for (;i > 0; --i) {
            query.append( seq1.charAt(i-1) );
            subject.append( '-' );
        }
        for (;j > 1; --j) {
            query.append( '-' );
            subject.append( seq2.charAt(i+j-2) );
        }            
        return subject.reverse() + "\n" + query.reverse();
    }

    /**
     * Calculates the similarity score of the shorter protein with itself.  
     * 
     * @return The similarity score of the shorter protein with itself.
     */
    public long perfect_score() {
        char ch;
        int i;
        long score = 0;
        for (i = 0; i < seq1.length(); ++i) {
            ch = seq1.charAt(i);
            score += match[ch][ch];
        }
        return score;
    }   
    
    /**
     * Calculates the score of un-gapped alignment of two sequences. 
     * 
     * @param s1 The first sequence.
     * @param s2 The second sequence.
     * @return The score of un-gapped alignment of two sequences.
     */
    public long get_match_score(String s1, String s2) {
        int i;
        long score = 0;
        for (i = 0; i < s1.length(); ++i) {
            score += match[s1.charAt(i)][s2.charAt(i)];
        }
        return score;
    }   

    /**
     * Calculates the score of un-gapped alignment of two sequences. 
     * 
     * @param s1 The first sequence.
     * @param s2 The second sequence.
     * @return The score of un-gapped alignment of two sequences.
     */
    public long get_match_score(StringBuilder s1, StringBuilder s2) {
        int i;
        long score = 0;
        for (i = 0; i < s1.length(); ++i) {
            score += match[s1.charAt(i)][s2.charAt(i)];
        }
        return score;
    }   
    
    /**
     * Calculates the score of un-gapped alignment of two sequences in percentage. 
     * 
     * @param s1 The first sequence.
     * @param s2 The second sequence.
     * @return The score of un-gapped alignment of two sequences in percentage.
     */
    public double get_match_percentage(String s1, String s2) {
        int i;
        long score = 0, p_score = 0;
        for (i = 0; i < s1.length(); ++i) {
            score += match[s1.charAt(i)][s2.charAt(i)];
            p_score += match[s1.charAt(i)][s1.charAt(i)];
        }
        return score * 100.0 / p_score;
    }   

    /**
     * @return The similarity score of two sequences, as the largest entry in 
     *         similarity matrix.
     */
    public int get_similarity() {
        return similarity;
    }
    
    /**
     * @return The identity of two sequences after being aligned.
     */
    public double get_identity() {
        return identity;
    }
 
    
    /**
     * Calculates the boundaries for soft-clipping of the alignment.
     * 
     * @return array [from1, to1, from2, to2] containing the clipping boundaries
     *         in the first and second sequences aligned. 
     */
    public int[] calculate_clip_range() {
        int i, j, x, max_ending_here, max_so_far, tmp_start, tmp_stop;
        int range[] = new int[4];
        x = i = max_i;
        j = max_j + max_i - 1;
        while (i > 0 && j > 0) {
            if (direction[i][j - i + 1] == 'I') {
                score_array[x--] = insertion_penalty;
                i = i - 1;
            } else if (direction[i][j - i + 1] == 'D') {
                j = j - 1;
            } else {
                score_array[x--] = seq1.charAt(i-1) == seq2.charAt(j-1) ? 1 : mismatch_penalty;
                i = i - 1;
                j = j - 1;
            }
        } 
        for (;i > 0; --i)
            score_array[x--] = 0;
        max_ending_here = max_so_far = score_array[1];
        tmp_start = tmp_stop = 1;
        range[0] = range[1] = 1;
        for (i = 2; i <= max_i; ++i) {
            if (score_array[i] > max_ending_here + score_array[i]) {
                max_ending_here = score_array[i];
                tmp_start = tmp_stop = i;
            } else {
                max_ending_here = max_ending_here + score_array[i];
                tmp_stop = i;
            }
            if (max_so_far < max_ending_here) {
                range[0] = tmp_start;
                range[1] = tmp_stop;
                max_so_far = max_ending_here;
            }
        }
        i = max_i;
        j = max_j + max_i - 1;
        while (i > 0 && j > 0) {
            if (i == range[1]) {
                range[3] = j;
            }
            if (i == range[0]) {
                range[2] = j;
            }
            if (direction[i][j - i + 1] == 'I') {
                i = i - 1;
            } else if (direction[i][j - i + 1] == 'D') {
                j = j - 1;
            } else {
                i = i - 1;
                j = j - 1;
            }
        } 
        return range;
    }
    
    /**
     * Calculates the SAM cigar string of the alignment.
     * 
     * @return  The SAM cigar string of the alignment.
     */
    public String get_cigar() {
        int i, j, move_counts, count, identicals = 0;
        int range[];
        char curr_move, prev_move, operation;
        insertions = deletions = 0;
        operation_stack.clear();
        count_stack.clear();
        cigar.setLength(0);
        if (CLIPPING_STRIGENCY > 0) {
            range = calculate_clip_range();
            if (seq1.length() - range[1] > 0) {
                operation_stack.push('S');
                count_stack.push(seq1.length() - range[1]);
            }
            prev_move = 'M';
            move_counts = 1;
        } else {
            range = new int[]{1, max_i, 1, max_j + max_i - 1};
            prev_move = 'M';
            move_counts = range[1] < seq1.length() ? seq1.length() - range[1] + 1 : 1;
        }
        range_len = range[1] - range[0] + 1;
        i = seq1.length();
        j = range[3] + seq1.length() - range[1];
        if (j > seq2.length()) {
            i -= j - seq2.length();
            j -= j - seq2.length();
        }
        for (; i >= range[1]; --i, --j)
             if (seq1.charAt(i-1) == seq2.charAt(j-1))
                identicals++;
        i = range[1] - 1;
        j = range[3] - 1;
        while (i >= range[0]) {
            curr_move = direction[i][j - i + 1];
            if (curr_move == 'I') {
                i = i - 1;
                ++insertions;
            } else if (curr_move == 'D') {
                j = j - 1;
                ++deletions;
            } else {
                if (seq1.charAt(i) == seq2.charAt(j))
                    ++identicals;
                i = i - 1;
                j = j - 1;
            } 
            if (prev_move == curr_move)
                ++move_counts;
            else{
                operation_stack.push(prev_move);
                count_stack.push(move_counts);
                move_counts = 1;
            }
            prev_move = curr_move;
            //Pantools.logger.info("{} {} {}", i, j, direction[i][j]);
        } 
        offset = j - i;
        if (CLIPPING_STRIGENCY > 0) {
            operation_stack.push(prev_move);
            count_stack.push(move_counts);
            if (i > 0) {
                operation_stack.push('S');
                count_stack.push(i);
                offset += i;
            }
        } else {
            if (prev_move == 'M')
                move_counts += i;
            operation_stack.push(prev_move);
            count_stack.push(move_counts);
        }
        while (!operation_stack.isEmpty()) {
            operation = operation_stack.pop();
            count = count_stack.pop();
            cigar.append(count).append(operation);
        }
        for (; i > 0 && j > 0; --i, --j)
             if (seq1.charAt(i-1) == seq2.charAt(j-1))
                identicals++;
        identity = ((double)identicals) / (seq1.length() + deletions); 
        return cigar.toString();
    }
        
    /**
     * Calculates offset as the number of gaps at start of the first sequence 
     * after alignment.
     * Can be negative if there are gaps at the start of the second sequence.
     * Should be called only after calling get_cigar().
     * 
     * @return The offset as the number of gaps at start of the first sequence 
     * after alignment.
     */
    public int get_offset() {
        return offset;
    }

    /**
     * @return The length of the alignment after soft-clipping
     */
    public int get_range_length() {
        return range_len;
    }

    /**
     * @return The total number of insertions in the alignment
     */
    public int get_insertions() {
        return insertions;
    }
    
    /**
     * @return The total number of deletions in the alignment
     */
    public int get_deletions() {
        return deletions;
    }
}
