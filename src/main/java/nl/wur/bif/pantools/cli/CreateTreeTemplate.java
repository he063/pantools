package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.Positive;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Create templates for coloring phylogenetic trees in iTOL.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "create_tree_template", sortOptions = false)
public class CreateTreeTemplate implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = "--color")
    @Positive(message = "{positive.color")
    int color;

    @Option(names = {"-p", "--phenotype"})
    String phenotype;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        phylogeny.create_tree_templates();
        return 0;
    }

    private void setGlobalParameters() {
        PHENOTYPE = phenotype;
        NODE_VALUE = String.valueOf(color);
    }

}
