package nl.wur.bif.pantools.cli.validation.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import nl.wur.bif.pantools.cli.validation.Constraints.Patterns.Flag;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static java.util.regex.Pattern.compile;
import static nl.wur.bif.pantools.cli.validation.Constraints.Patterns;

/**
 * Custom version of @Pattern constraint that matches a list of strings with a regular expression that matches a keyset of strings.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class PatternMapValidator implements ConstraintValidator<Patterns, HashMap<String, String>> {

    private Pattern pattern;

    @Override
    public void initialize(Patterns constraintAnnotation) {
        Flag[] flags = constraintAnnotation.flags();
        int intFlag = 0;
        for (Flag flag : flags) intFlag = intFlag | flag.getValue();

        try {
            pattern = compile(constraintAnnotation.regexp(), intFlag);
        } catch (PatternSyntaxException e) {
            throw new IllegalArgumentException("Invalid regular expression.", e);
        }
    }

    /**
     * Verifies whether the strings in the provided list match the regular expression provided in the keyset
     * @param map map of string value pairs, the keys of which need to be validated
     * @param context ConstraintValidatorContext containing contextual data for a given constraint validator
     * @return true if ALL strings in the list match the given regular expression, false otherwise
     */
    @Override
    public boolean isValid(HashMap<String, String> map, ConstraintValidatorContext context) {
        if (map == null) return true;
        for (String string : map.keySet()) {
            final Matcher matcher = pattern.matcher(string);
            if (!matcher.matches()) return false;
        }
        return true;
    }
}
