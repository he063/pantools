package nl.wur.bif.pantools.cli;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Update or alter the terminal nodes (leaves) of a phylogenic tree.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "rename_phylogeny")
public class RenamePhylogeny implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "tree-file", index = "0+")
    @InputFile(message = "{file.tree}")
    Path treeFile;

    @Option(names = "--no-numbers")
    boolean noNumbers;

    @Option(names = {"-p", "--phenotype"})
    String phenotype;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        phylogeny.rename_phylogeny();
        return 0;
    }

    private void setGlobalParameters() {
        INPUT_FILE = treeFile.toString();
        PHENOTYPE = phenotype;
        if (noNumbers) Mode = "NO-NUMBERS";
    }

}
