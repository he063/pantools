package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.Pattern;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static jakarta.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;
import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Remove an homology grouping from the pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "remove_grouping", sortOptions = false)
public class RemoveGrouping implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-v", "--grouping-version"}, required = true)
    @Pattern(regexp = "[0-9]+|all|all-inactive", flags = CASE_INSENSITIVE, message = "{pattern.grouping-version}")
    String groupingVersion;

    @Option(names = "--fast")
    boolean fast;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        proLayer.remove_grouping();
        return 0;
    }

    private void setGlobalParameters() {
        GROUPING_VERSION = groupingVersion.toUpperCase();
        FAST = fast;
    }

}
