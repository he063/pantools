package nl.wur.bif.pantools.cli.mixins;

import jakarta.validation.constraints.Size;
import nl.wur.bif.pantools.cli.validation.Constraints.InputFile;
import nl.wur.bif.pantools.utils.StringUtils;
import nl.wur.bif.pantools.utils.Utils;

import static picocli.CommandLine.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

/**
 * Mixin class for '--homology-file' and '--homology-groups' options.
 *
 * @author Dirk-Jan van Workum, Wageningen University, the Netherlands.
 */
public class SelectHmGroups {

    @Option(names = {"-G", "--homology-groups"})
    void setHomologyGroups(String homologyGroupsString) {
        homologyGroups = StringUtils.stringToLongList(homologyGroupsString);
    }

    @Option(names = {"-H", "--homology-file"})
    @InputFile(message = "{file.homology}")
    private Path homologyGroupsFile;

    @Size(min = 1, message = "{size.empty.homology-groups}")
    private List<Long> homologyGroups;

    public List<Long> getHomologyGroups() throws IOException, NumberFormatException {
        if (homologyGroups != null) return homologyGroups;
        return (homologyGroupsFile == null) ? null : Utils.parseHmFile(homologyGroupsFile);
    }
}
