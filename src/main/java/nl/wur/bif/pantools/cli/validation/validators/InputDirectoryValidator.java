package nl.wur.bif.pantools.cli.validation.validators;

import nl.wur.bif.pantools.cli.validation.Constraints.InputDirectory;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Custom ConstraintValidator that verifies that an input directory exists and is a directory.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class InputDirectoryValidator implements ConstraintValidator<InputDirectory, Path> {

    @Override
    public void initialize(InputDirectory constraintAnnotation) {
    }

    /**
     * Verifies whether a path is an existing directory.
     * @param directory root path to input directory
     * @param context ConstraintValidatorContext containing contextual data for a given constraint validator
     * @return true when the path is a directory or is unassigned, false otherwise
     */
    @Override
    public boolean isValid(Path directory, ConstraintValidatorContext context) {
        if (directory == null) return true;
        return Files.isDirectory(directory);
    }
}
