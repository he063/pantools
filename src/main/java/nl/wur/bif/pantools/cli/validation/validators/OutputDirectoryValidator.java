package nl.wur.bif.pantools.cli.validation.validators;

import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.utils.ConsoleUtils;
import org.apache.commons.beanutils.PropertyUtils;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

import static nl.wur.bif.pantools.cli.validation.Constraints.*;

/**
 * Custom ConstraintValidator that verifies whether a given directory can be created without needing to overwrite.
 * Can validate the @ParentCommand decorated field 'pantools' when the given directory is 'databaseDirectory'.
 * The subcommand needs getters for the Pantools parent class and the output directory path.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class OutputDirectoryValidator implements ConstraintValidator<OutputDirectory, Object> {

    String directoryField;
    OutputDirectory outputDirectory;

    @Override
    public void initialize(final OutputDirectory constraintAnnotation) {
        directoryField = constraintAnnotation.directory();
        outputDirectory = constraintAnnotation;
    }

    /**
     * Validate whether a directory does not exist or is empty. If the directory exists and contains files the user
     * has to give consent to overwrite the directory either with --force or with a command line prompt.
     *
     * @param classToValidate subcommand class containing the directory field and Pantools parent command
     * @param context ConstraintValidatorContext containing contextual data for a given constraint validator
     * @return boolean for validity of the constraint
     */
    @Override
    public boolean isValid(Object classToValidate, ConstraintValidatorContext context) {
        Pantools pantools;
        Path directoryPath;

        if (directoryField.equals("databaseDirectory")) {
            // When validating the pantools parent class for the <databaseDirectory> parameter
            pantools = (Pantools) classToValidate;
            directoryPath = pantools.getDatabaseDirectory();
        } else {
            // When validating any directory from a pantools subcommand
            try {
                pantools = (Pantools) PropertyUtils.getProperty(classToValidate, "pantools");
                directoryPath = (Path) PropertyUtils.getProperty(classToValidate, directoryField);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        if (directoryPath == null || !Files.exists(directoryPath) || !Files.isDirectory(directoryPath) ||
                pantools.isForce() || Objects.requireNonNull(directoryPath.toFile().list()).length == 0) {
            // No warnings when the directory does not exist, is empty or --force is given in the command line
            return true;
        } else if (!pantools.isInput()) {
            // If --no-input is given without --force the directory containing files is not valid
            return false;
        } else {
            // Ask for user input to decide whether to overwrite the directory or not
            final String query = String.format(
                    "<%s> already exists and contains files, do you want to overwrite it?", directoryField);
            return ConsoleUtils.askYesOrNo(query);
        }
    }
}
