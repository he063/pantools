package nl.wur.bif.pantools.cli;

import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import jakarta.validation.constraints.Pattern;

import java.io.IOException;
import java.util.concurrent.Callable;

import static jakarta.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Calculate Average Nucleotide Identity (ANI) scores between genomes.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "ani", sortOptions = false)
public class ANI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-m", "--mode"}, paramLabel = "MASH|fastANI")
    @Pattern(regexp = "MASH|fastANI", flags = CASE_INSENSITIVE, message = "{pattern.mode-ani}")
    String mode;

    @Option(names = {"-p", "--phenotype"})
    String phenotype;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes);

        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead

        phylogeny.calculate_ani();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
        Mode = mode;
        PHENOTYPE = phenotype;
    }

    public String getMode() { return mode; }
    public Integer getnThreads() { return threadNumber.getnThreads(); }

}
