package nl.wur.bif.pantools.cli;

import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Step 3/3 of MLSA. Run IQ-tree on the concatenated sequences.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "mlsa", sortOptions = false)
public class MLSA implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-p", "--phenotype"})
    String phenotype;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes);

        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead

        phylogeny.run_MLSA();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
        PHENOTYPE = phenotype;
    }

}
