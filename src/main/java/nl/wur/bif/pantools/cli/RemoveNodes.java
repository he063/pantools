package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.Size;
import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.cli.validation.Constraints.ExcludePatterns;
import nl.wur.bif.pantools.pantools.Pantools;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.StringUtils.stringToIntegerList;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Remove a selection of nodes and their relationships from the pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "remove_nodes", sortOptions = false)
@ExcludePatterns(regexp = "nucleotide,sequence,pangenome,genome,degenerate", field = "label",
        query = "Nodes of type '%s' should NOT be removed", message = "{exclude.core}")
@ExcludePatterns(regexp = "feature,CDS,tRNA,gene,mRNA,coding_gene,tRNA,annotation,exon,intron", field = "label",
        query = "'remove_annotations' should be used to remove nodes of type '%s'", message = "{exclude.ann}")
@ExcludePatterns(regexp = "homology_group", field = "label", message = "{exclude.homology}",
        query = "'remove_homology_groups' should be used to remove nodes of type '%s'")
@ExcludePatterns(regexp = "go,pfam,tigrfam,interpro", field = "label", message = "{exclude.functions}",
        query = "'remove_functions' should be used to remove nodes of type '%s'")
@ExcludePatterns(regexp = "cog,phobius,signalp", field = "label", message = "{exclude.function-properties}",
        query = "'remove_functions' should be used to remove properties of type '%s' from mRNA nodes")
public class RemoveNodes implements Callable<Integer> {

    @Spec static CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    static String label;

    @ArgGroup(multiplicity = "1") Identifiers identifiers;
    private static class Identifiers {
        @Option(names = {"-n", "--nodes"})
        void setNodes(String value) {
            nodes = stringToIntegerList(value);
        }
        @Size(min = 1, message = "{size.empty.node}")
        List<Integer> nodes;

        @ArgGroup(exclusive = false) LabelOptions labelOptions;
        private static class LabelOptions {
            @Option(names = "--label", required = true)
            void setLabel(String value) {
                label = value;
            }

            @ArgGroup SelectGenomes selectGenomes;
        }
    }

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, identifiers, identifiers.labelOptions.selectGenomes);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        classification.remove_nodes();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(identifiers.labelOptions.selectGenomes);
        if (identifiers.nodes != null) NODE_ID = identifiers.nodes.toString().replaceAll("[\\[\\]]", "");
        if (label != null) SELECTED_LABEL = label;
    }

}
