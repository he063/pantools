package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Determine the openness of the pangenome based on k-mer sequences.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "pangenome_structure", sortOptions = false, abbreviateSynopsis = true)
public class PangenomeStructure implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @ArgGroup
    VariationOptions variationOptions;
    static boolean pavs = false;
    static boolean kmer = false;

    static class VariationOptions {
        @Option(names = {"--pavs"})
        void isVariation(boolean value) {
            pavs = value;
        }

        @Option(names = {"-k", "--kmer"})
        void isKmer(boolean value) {
            kmer = value;
        }
    }

    @Option(names = "--loops")
    @Min(value = 0, message = "{min.loops}") // 0 is default
    @Max(value = 1000000, message = "{max.loops}")
    long loops;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes);

        pantools.setPangenomeGraph();
        if (kmer) {
            if (loops == 0) loops = 100; // set default value based on -k
            setGlobalParameters(); //TODO: use local parameters instead
            classification.pangenome_size_kmer(); //cannot use pavs
        } else {
            if (loops == 0) loops = 10000; // set default value based on -k
            setGlobalParameters(); //TODO: use local parameters instead
            classification.pangenome_size_genes(pavs);
        }
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
        NODE_VALUE = Long.toString(loops);
    }

}
