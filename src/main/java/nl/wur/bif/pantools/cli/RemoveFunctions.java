package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.Pattern;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pangenome.Classification;
import nl.wur.bif.pantools.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static jakarta.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;
import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static picocli.CommandLine.*;

/**
 * Remove an homology grouping from the pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "remove_functions", sortOptions = false)
public class RemoveFunctions implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "mode", index = "0+")
    @Pattern(regexp = "nodes|properties|bgc|all|COG|phobius|signalp", flags = CASE_INSENSITIVE,
            message = "{pattern.mode-rmf}")
    String mode;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph();

        final Classification classification = new Classification();
        classification.removeFunctions(this);
        return 0;
    }

    public String getMode() {
        return mode;
    }
}
