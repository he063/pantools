package nl.wur.bif.pantools.cli;

import jakarta.validation.constraints.Pattern;
import nl.wur.bif.pantools.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static jakarta.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;
import static nl.wur.bif.pantools.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Identify BUSCO genes in the pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "busco_protein", sortOptions = false, abbreviateSynopsis = true)
public class BuscoProtein implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-A", "--annotations-file"}, paramLabel = "<annotationsFile>")
    @InputFile(message = "{file.annotations}")
    Path annotationsFile;

    @Option(names = {"-v", "--busco-version"}, paramLabel = "BUSCO3|BUSCO4|BUSCO5")
    @Pattern(regexp = "BUSCO[345]", flags = CASE_INSENSITIVE, message = "{pattern.version.busco-version}")
    String buscoVersion;

    @ArgGroup(multiplicity = "1") BuscoDatasets buscoDatasets;
    static class BuscoDatasets {
        @Option(names = {"--busco9"})
        @InputFile(message = "{file.busco}")
        Path busco9;

        @Option(names = {"--busco10"})
        @Pattern(regexp = "(.+)_odb10$", message = "{pattern.busco-set}")
        String busco10;
    }

    @Option(names = "--skip-busco")
    void setSkip(String value) {
        buscoSkipList = Arrays.asList(value.toUpperCase().split(","));
    }
    List<String> buscoSkipList;

    @Option(names = "--longest")
    boolean longestTranscript;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes, buscoDatasets);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        classification.busco_protein();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
        INPUT_FILE = (buscoDatasets.busco9 != null) ? buscoDatasets.busco9.toString() : buscoDatasets.busco10;
        if (annotationsFile != null) PATH_TO_THE_ANNOTATIONS_FILE = annotationsFile.toString();
        BUSCO_VERSION = buscoVersion;
        if (longestTranscript) longest_transcripts = true;
        if (buscoSkipList != null) SELECTED_NAME = buscoSkipList.toString().replaceAll("[\\[\\]]", "");
    }

}
