package nl.wur.bif.pantools.pangenome.export.records;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Stores type of relation, label of start and end node, as well as their
 * UUIDs, and the key and value of the property.
 */
public class RelationshipProperty implements Record{
    private final String type;
    private final String startNodeLabel, startNodeUuid;
    private final String endNodeLabel, endNodeUuid;
    private final String key, value;

    public RelationshipProperty(String type, String startNodeLabel, String startNodeUuid, String endNodeLabel, String endNodeUuid, String key, String value) {
        this.type = type;
        this.startNodeLabel = startNodeLabel;
        this.startNodeUuid = startNodeUuid;
        this.endNodeLabel = endNodeLabel;
        this.endNodeUuid = endNodeUuid;
        this.key = key;
        this.value = value;
    }

    @Override
    public List<String> asList() {
        return ImmutableList.of(type, startNodeLabel, startNodeUuid, endNodeLabel, endNodeUuid, key, value);
    }
}

