package nl.wur.bif.pantools.pangenome.export.records;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Stores a Neo4j node property: a UUID, node label, and the node's property
 * key and value.
 */
public class NodeProperty implements Record {
    private final String uuid, label, key, value;

    public NodeProperty(String uuid, String label, String key, String value) {
        this.uuid = uuid;
        this.label = label;
        this.key = key;
        this.value = value;
    }

    @Override
    public List<String> asList() {
        return ImmutableList.of(uuid, label, key, value);
    }
}
