/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.wur.bif.pantools.pangenome;

import nl.wur.bif.pantools.index.IndexScanner;
import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.sequence.SequenceScanner;
import nl.wur.bif.pantools.utils.FileUtils;
import nl.wur.bif.pantools.utils.Utils;
import org.apache.commons.lang.ArrayUtils;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static nl.wur.bif.pantools.alignment.BoundedLocalSequenceAlignment.match;
import static nl.wur.bif.pantools.pangenome.Classification.*;
import static nl.wur.bif.pantools.pangenome.create_skip_arrays.create_skip_arrays;
import static nl.wur.bif.pantools.utils.FileUtils.convertAfaToAlignedFasta;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

/**
 *
 * @author Eef Jonkheer, Bioinformatics group, Wageningen University, the Netherlands.
 */
public class Phylogeny {

    private BlockingQueue<String> string_queue;
    private static AtomicLong atomic_counter1;
    private static AtomicLong atomic_counter2;
    private static int warning_counter = 0;
    public static boolean mlsa_function; // mlsa_find_gene can be used with or without the goal to use the sequences for a MLSA. Output directories are different
    final static public String[] COLOR_CODES = new String[]{"#fabebe","#bfef45","#42d4f4","#ffd8b1","#aaffc3","#fffac8","#e6beff","#469990","#e6194B","#f58231", "#ffe119",
            "#3cb44b","#4363d8","#911eb4","#a9a9a9","#800000","#808000","#9A6324","#000075","#f032e6"};
    final static public String[] COLORBLIND_CODES = new String[]{"#E69F00", "#56B4E9", "#009E73", "#0072B2", "#D55E00",
            "#CC79A7", "#999999", "#F0E442", "#000000"}; // colourblind friendly palette up to 8 colors (9th value is black)

    /**
     * Main body of pantools MLSA_find_genes() and find_genes_by_name() functions
     * @param mlsa_function_boolean is set to true when it's part of MLSA_find_genes functionality
     */
    public void mlsa_find_genes(boolean mlsa_function_boolean) {
        mlsa_function = mlsa_function_boolean;
        if (mlsa_function) {
            Pantools.logger.info("MLSA Step 1/3. Finding genes in pangenome.");
        } else {
            Pantools.logger.info("Finding genes in pangenome.");
        }

        if (Mode.equals("0")) {
            Pantools.logger.info("--mode extensive was NOT included. Can only find exact (and case insensitive) matches.");
        } else if (Mode.equals("EXTENSIVE")) {
            Pantools.logger.info("Extensive search mode was selected. Can find exactly matching or shorter gene names.");
        } else {
            Mode = "0";
            Pantools.logger.info("--mode was not recognized. Can only find exact (case insensitive) matches.");
        }

        check_database(); // starts up the graph database if needed
        check_if_program_exists_stderr("mafft -h", 100, "MAFFT", true); // check if program is set to $PATH
        try (Transaction tx = GRAPH_DB.beginTx()) {
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "MLSA_find_genes"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if --skip/-ref is provided by user
            retrieve_phenotypes(); // create geno_pheno_map and new_phenotype_map when a phenotype was provided by the user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        if (mlsa_function) {
            create_directory_in_DB("mlsa/input/protein");
            create_directory_in_DB("mlsa/input/nucleotide");
        } else {
            create_directory_in_DB("find_genes/by_name/protein_sequences");
            create_directory_in_DB("find_genes/by_name/nucleotide_sequences");
        }

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            if (mlsa_function) {
                int hm_nodes = (int) count_nodes(HOMOLOGY_GROUP_LABEL);
                if (hm_nodes == 0) {
                    System.out.println("\rMust first cluster the protein sequences with 'group'\n");
                    System.exit(1);
                }
            }
            annotation_identifiers = get_annotation_identifiers(true, true, PATH_TO_THE_ANNOTATIONS_FILE);
            ArrayList<String> gene_names = parse_gene_names();
            HashMap<String, ArrayList<Node>> found_genes_map = find_genes_by_name(gene_names);
            report_found_genes(gene_names, found_genes_map);
            tx.success(); // transaction successful, commit changes
        }

        if (warning_counter > 0) {
            Pantools.logger.warn("{} warnings found. See log file for details.", warning_counter);
        }
        Pantools.logger.info("Output written to:");
        if (mlsa_function) {
            Pantools.logger.info(" {}mlsa/input/nucleotide/", WORKING_DIRECTORY);
            Pantools.logger.info(" {}mlsa/input/protein/", WORKING_DIRECTORY);
            Pantools.logger.info(" {}mlsa/mlsa_find_genes.log", WORKING_DIRECTORY);
        } else {
            Pantools.logger.info(" {}find_genes/by_name/find_genes_by_name.log", WORKING_DIRECTORY);
            Pantools.logger.info(" {}find_genes/by_name/nucleotide_sequences/", WORKING_DIRECTORY);
            Pantools.logger.info(" {}find_genes/by_name/protein_sequences/", WORKING_DIRECTORY);
        }
    }

    /**
     * Searches the pangenome for gene nodes matching the user selection of gene ids/names
     * This generally only works for standardized annotations from tools like Prokka
     * @param selectedGeneNames
     * @param found_genes_map
     * @param case_insenstive
     */
    private void find_gene_nodes_for_given_names(ArrayList<String> selectedGeneNames, HashMap<String, ArrayList<Node>> found_genes_map,
                                                boolean case_insenstive) {

        ResourceIterator<Node> all_gene_nodes = GRAPH_DB.findNodes(GENE_LABEL);
        int gene_nodes = (int) count_nodes(GENE_LABEL);
        int gene_node_counter = 0;
        System.out.print("\rSearching for gene names in gene nodes: ");
        while (all_gene_nodes.hasNext()) {
            gene_node_counter++;
            if (gene_node_counter % 10000 == 0 || gene_nodes == gene_node_counter ) {
                System.out.print("\rSearching for gene names in gene nodes: " + gene_node_counter + "/" + gene_nodes + "  ");
            }
            Node gene_node = all_gene_nodes.next();
            int genome_nr = (int) gene_node.getProperty("genome");
            if (skip_array[genome_nr-1]) {
                continue;
            }

            String gene_id = (String) gene_node.getProperty("id");
            String[] geneNames = (String[]) gene_node.getProperty("name");
            matchGeneNamesAgainstIdOrName(gene_id, selectedGeneNames, found_genes_map, case_insenstive, gene_node);
            for (String geneName : geneNames) {
                matchGeneNamesAgainstIdOrName(geneName, selectedGeneNames, found_genes_map, case_insenstive, gene_node);
            }

        }
        System.out.print("\r                                                              ");
    }

    /**
     * Checks if a gene ID or name matches to one of the input gene names
     * @param gene_id gene id or name
     * @param selectedGeneNames list of gene names or ids given by user
     * @param found_genes_map key is gene name, value is arraylist with gene nodes matching the name
     * @param case_insenstive if the matching is case-insensitive
     * @param gene_node gene node belonging to the current gene id
     */
    private void matchGeneNamesAgainstIdOrName(String gene_id, ArrayList<String> selectedGeneNames, HashMap<String, ArrayList<Node>> found_genes_map,
                                               boolean case_insenstive, Node gene_node) {

        for (String target_gene_name : selectedGeneNames) {
            String gene_name_key = target_gene_name;
            if (case_insenstive) {
                target_gene_name = target_gene_name.toLowerCase();
                gene_id = gene_id.toLowerCase();
                gene_name_key += "#case_insensitive";
            }
            if (Mode.equals("EXTENSIVE") && gene_id.startsWith(target_gene_name)) {
                try_incr_AL_hashmap(found_genes_map, gene_name_key, gene_node);
            } else if (gene_id.equals(target_gene_name)) {
                try_incr_AL_hashmap(found_genes_map, gene_name_key, gene_node);
            }
        }
    }
    /**
     * Body surrounding the function that actually searches checks if genes match to input
     * @param gene_names list with gene names or ids, given by the user
     * @return key is gene name, value is arraylist with gene nodes matching the name
     */
    public HashMap<String, ArrayList<Node>> find_genes_by_name(ArrayList<String> gene_names) {
        HashMap<String, ArrayList<Node>> found_genes_map = new HashMap<>();
        for (String gene_name : gene_names) {
            ArrayList<Node> empty_list = new ArrayList<>();
            found_genes_map.put(gene_name, empty_list);
        }
        find_gene_nodes_for_given_names(gene_names, found_genes_map, false);
        ArrayList<String> genes_without_hit = new ArrayList<>();
        for (String gene_name : found_genes_map.keySet()) {
            if (found_genes_map.get(gene_name).isEmpty()) {
                genes_without_hit.add(gene_name);
            }
        }
        if (!genes_without_hit.isEmpty()) { // search again but now case insensitive for genes without a hit
            find_gene_nodes_for_given_names(genes_without_hit, found_genes_map, true);
        }
        return found_genes_map;
    }

    /**
     * Main body surrounding function that collects all statistics for genes that are found
     * @param gene_names
     * @param found_genes_map
     */
    public void report_found_genes(ArrayList<String> gene_names, HashMap<String, ArrayList<Node>> found_genes_map) {
        StringBuilder log_builder = new StringBuilder();
        System.out.println("\rgene name -> total hits/total genomes, range of gene lengths");
        for (String gene_name : gene_names) {
            SELECTED_NAME = gene_name;
            log_builder.append("#gene name ").append(gene_name).append("; genome; gene node id; mRNA node id; homology group id; gene length;"
                    + " protein length; gene address\n");
            ArrayList<Node> gene_list = found_genes_map.get(gene_name);
            if (!gene_list.isEmpty()) {
                retrieve_gene_seq_check_presence_hmgroup(gene_list, log_builder);
            } else {
                log_builder.append("No genes were found! Trying a case insensitive search\n");
                gene_list = found_genes_map.get(gene_name + "#case_insensitive");
                if (gene_list == null) {
                    log_builder.append("Still no gene found!\n\n");
                    Pantools.logger.debug("{}: 0.", SELECTED_NAME);
                } else {
                    retrieve_gene_seq_check_presence_hmgroup(gene_list, log_builder);
                }
            }
        }

        if (mlsa_function) {
            write_SB_to_file_in_DB(log_builder, "mlsa/mlsa_find_genes.log");
        } else {
            write_SB_to_file_in_DB(log_builder, "find_genes/by_name/find_genes_by_name.log");
        }
    }

    /**
     * SELECTED_NAME is by the user set via --gene-names
     * @return list of gene names, given by the user
     */
    public ArrayList<String> parse_gene_names() {
        ArrayList<String> gene_list = new ArrayList<>();
        if (SELECTED_NAME.contains(",")) {
            String[] names = SELECTED_NAME.split(",");
            for (String name : names) {
                gene_list.add(name);
            }
        } else {
            gene_list.add(SELECTED_NAME);
        }
        System.out.println("\rSearching for " + SELECTED_NAME + " gene(s)");
        return gene_list;
    }

    /**
     * Main body of pantools MLSA_concatenate function
     * First run mlsa_find_genes()
     *
     */
    public void mlsa_concatenate() {
        Pantools.logger.info("MLSA Step 2/3. Concatenate sequences.");
        if (SELECTED_NAME == null) {
            Pantools.logger.error("No gene names were provided via --gene-names.");
            System.exit(1);
        }
        delete_file_in_DB("mlsa/input/nucleotide/concatenated.fasta");
        String[] gene_names = SELECTED_NAME.replace(" ","").split(",");
        Pantools.logger.info("Selected {} genes: {}.", gene_names.length, SELECTED_NAME);
        report_number_of_threads(); // prints how many threads were selected by user
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "MLSA_concatenate"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if --skip/-ref is provided by user
            retrieve_phenotypes(); // create geno_pheno_map and new_phenotype_map when a phenotype was provided by the user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        gene_names = concatenate_pre_alignment(gene_names);
        concatenate_seq_in_dir(gene_names);
        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}mlsa/input/concatenated.fasta", WORKING_DIRECTORY);
        Pantools.logger.info("Log file:");
        Pantools.logger.info(" {}mlsa/mlsa.info", WORKING_DIRECTORY);
    }

    /*
     Pantools MLSA function
     --mode 1, skip over IQ-tree (this option is hidden in the manual)
    */
    public void run_MLSA() {
        delete_directory(WORKING_DIRECTORY + "mlsa/output"); // remove results from a previous run
        create_directory_in_DB("mlsa/output/var_inf_positions");
        Pantools.logger.info("MLSA Step 3/3. Multiple sequence alignment of concatenated sequences.");
        check_database(); // starts up the graph database if needed
        String threads_str = report_number_of_threads(); // prints how many threads were selected by user

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "MLSA"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if --skip/-ref is provided by user
            retrieve_phenotypes(); // create geno_pheno_map and new_phenotype_map when a phenotype was provided by the user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            System.out.println("\rUnable to start the database");
            System.exit(1);
        }

        read_concatenated_fasta_for_genome_order();
        check_if_program_exists_stderr("mafft -h", 100, "MAFFT", true);
        check_if_program_exists_stdout("iqtree -h", 100, "IQtree");

        Path mlsaPath = Paths.get(WORKING_DIRECTORY).resolve("mlsa");
        Path genomeOrderPath = mlsaPath.resolve("input").resolve("genome_order.info");
        Path mlsaOutputPath = mlsaPath.resolve("output");

        System.out.print("\rStep 1/3. Aligning sequences using MAFFT");
        run_alignment_for_mlsa();

        Pantools.logger.info("\nStep 2/3. Identifying SNPs in the alignment.");
        HashMap<String, Integer> shared_snps_map = new HashMap<>();
        int[] var_inf_sites = count_var_inf_sites_in_msa(shared_snps_map,
                mlsaOutputPath.resolve("mlsa.afa"),
                "MLSA", "nucleotide",
                genomeOrderPath,
                ""); // variable, informative, shared between all

        Pantools.logger.info("Total variable/informative sites: {}/{}.", var_inf_sites[0], var_inf_sites[1]);
        Classification.create_shared_site_matrices(shared_snps_map, mlsaOutputPath.resolve("var_inf_positions"),
                var_inf_sites[0], "", "nuc", genomeOrderPath);
        Classification.create_shared_site_matrices(shared_snps_map, mlsaOutputPath.resolve("var_inf_positions"),
                var_inf_sites[1], "#inf", "nuc", genomeOrderPath);
        delete_file_full_path(genomeOrderPath);

        Pantools.logger.info("Step 3/3. Running IQtree.");
        if (!Mode.equals("1") && !Mode.contains("SKIP") && var_inf_sites[1] > 0) { // these two modes were only created for development, not shown in manual
            String[] iq_command = {"iqtree", "-nt", threads_str, "-s", mlsaOutputPath.resolve("mlsa.fasta").toString(), "-redo","-bb", "1000"};
            ExecCommand.ExecCommand(iq_command);
        }
        // LOG global variable was removed. I don't know if the output from to print the iq-tree log output to the screen
        String[] iq_command = {"iqtree", "-nt", THREADS +"", "-s", mlsaOutputPath.resolve("mlsa.fasta").toString(),"-redo","-bb", "1000"};
        ExecCommand.ExecCommand(iq_command);

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}", mlsaOutputPath.resolve("mlsa.afa"));
        Pantools.logger.info(" {}", mlsaOutputPath.resolve("mlsa.fasta"));
        Pantools.logger.info(" {}", mlsaOutputPath.resolve("nuc_alignment.info"));
        Pantools.logger.info(" {}", mlsaOutputPath.resolve("mlsa.fasta.treefile"));
        Pantools.logger.info(" {}", mlsaOutputPath.resolve("mlsa.fasta.contree"));
        Pantools.logger.info(" {}", mlsaOutputPath.resolve("output/mlsa.fasta.iqtree"));

        Pantools.logger.info(" {}", mlsaOutputPath.resolve("output/var_inf_positions/variable_nuc_distance.csv"));
        Pantools.logger.info(" {}", mlsaOutputPath.resolve("output/var_inf_positions/variable_nuc_site_counts.csv"));
        if (var_inf_sites[1] > 0) {
            Pantools.logger.info(" {}", mlsaOutputPath.resolve("output/var_inf_positions/informative_nuc_distance.csv"));
            Pantools.logger.info(" {}", mlsaOutputPath.resolve("output/var_inf_positions/informative_nuc_site_counts.csv"));
        } else {
            Pantools.logger.error("The alignment does not contains parsimony informative sites.");
            throw new RuntimeException("Unable to infer a phylogeny");
        }
    }

    /**
     * Creates a genome_order.info file, used by the functions that count SNPs in the alignment
     * When a --phenotype is included, create a new fasta with phenotype info in the headers
     */
    private void read_concatenated_fasta_for_genome_order() {
        StringBuilder genome_order = new StringBuilder();
        StringBuilder target_genomes = new StringBuilder();
        StringBuilder new_fasta = new StringBuilder();

        if (!check_if_file_exists(WORKING_DIRECTORY + "mlsa/input/concatenated.fasta")) {
            System.out.println("\rPlease run 'MLSA_concatenate' first!"
                    + "\nNo sequences found in " + WORKING_DIRECTORY + "mlsa/input/concatenated.fasta\n");
            System.exit(1);
        }

        try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "mlsa/input/concatenated.fasta"))) {
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (line.startsWith(">")) {
                    String[] line_array = line.split("_");
                    String genome_nr = line_array[0].replace(">","");
                    target_genomes.append(genome_nr).append(",");
                    if (PHENOTYPE != null) {
                        String phenotypeProperty = geno_pheno_map.get(Integer.parseInt(genome_nr));
                        phenotypeProperty = phenotypeProperty.replace(" ","_"); // mafft is unable to handle spaces in fasta header
                        new_fasta.append(line_array[0]).append("_").append(phenotypeProperty).append("\n");
                        genome_order.append(genome_nr).append(",").append(genome_nr).append("_").append(phenotypeProperty).append("\n");
                    } else {
                        new_fasta.append(line_array[0]).append("\n");
                        genome_order.append(genome_nr).append(",").append(genome_nr).append("\n");
                    }
                } else {
                    new_fasta.append(line).append("\n");
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read: {}mlsa/input/concatenated.fasta", WORKING_DIRECTORY);
            System.exit(1);
        }

        // whether previous run was with or WITHOUT a phenotype, allways replace the old fasta file
        write_SB_to_file_in_DB(new_fasta, "mlsa/input/concatenated.fasta");

        String target_genomes_str = target_genomes.toString().replaceFirst(".$","");
        target_genome = target_genomes_str;
        create_skip_arrays(false, false); // create skip array if --skip/-ref is provided by user
        write_SB_to_file_in_DB(genome_order, "mlsa/input/genome_order.info");
    }

    /**
     * Run MAFFT against concatenated.fasta
     */
    private void run_alignment_for_mlsa() {
        String input_fasta = "concatenated.fasta";
        Path inputFastaPath = Paths.get(WORKING_DIRECTORY).resolve("mlsa").resolve("input").resolve(input_fasta);
        Path outputAfaPath = Paths.get(WORKING_DIRECTORY).resolve("mlsa").resolve("output").resolve("mlsa.afa");

        boolean pass = false;
        while (!pass) {
            String[] command = {"mafft","--auto", "--anysymbol", "--thread", THREADS +"", "--clustalout", inputFastaPath.toString()};
            String mafft_output = ExecCommand.ExecCommand(command);
            if (!mafft_output.equals("")) {
                pass = true;
                write_string_to_file_full_path(mafft_output, outputAfaPath.toString());
            }
        }
        convertAfaToAlignedFasta(inputFastaPath, outputAfaPath);
    }

    /**
     * Align sequences before MLSA concatenate
     * @param gene_name
     * @param path
     */
    public static void run_alignment_for_mlsa_concatenate(String gene_name, Path path) {
        boolean pass = false;
        while (!pass) {
            String[] command = new String[] {"mafft","--auto", "--anysymbol", "--thread", THREADS + "", "--clustalout", path.resolve(gene_name + ".fasta").toString()};
            String mafft_output = ExecCommand.ExecCommand(command);
            if (!mafft_output.equals("")) {
                pass = true;
                write_string_to_file_full_path(mafft_output, path.resolve(gene_name + "_aligned.afa"));
            }
        }
    }

    /**
     * 1. Align protein sequences
     * 2. Find longest start and end gap in protein prealignment. Multiply by aa by 3 to trim nucleotide input sequences.
     * 3. Align trimmed nucleotide sequences
     * 4. Report variable & informative sites
     * @param gene_names
     * @return
     */
    public String[] concatenate_pre_alignment(String[] gene_names) {
        String new_gene_names = ""; // exclude the genes that cannot be used
        Path mlsaProteinPath = Paths.get(WORKING_DIRECTORY).resolve("mlsa").resolve("input").resolve("protein");
        Path mlsaNucleotidePath = Paths.get(WORKING_DIRECTORY).resolve("mlsa").resolve("input").resolve("nucleotide");

        match = FileUtils.loadScoringMatrix("BLOSUM62");
        System.out.println("\rGene name: Maximum bases trimmed from START/END of nucleotide sequences -> " +
                "Parsimony informative sites in trimmed nucleotide alignment");

        boolean incorrect_input = false;
        for (String gene_name : gene_names) {
            Path genePath = mlsaProteinPath.resolve(gene_name + ".fasta");
            Path alignedGenePath = mlsaProteinPath.resolve(gene_name + "_aligned.fasta");
            Path alignedGeneAfaPath = mlsaProteinPath.resolve(gene_name + "_aligned.afa");
            if (!genePath.toFile().exists()) {
                Pantools.logger.info("No input sequence can be found for '{}' (Case sensitive). Run 'MLSA_find_genes' first.", gene_name);
                continue;
            }

            int seq_count = count_seqs_in_fasta(genePath.toString());
            if (seq_count != adj_total_genomes && seq_count != total_genomes) {
                Pantools.logger.info(" {}.fasta contains {} sequences while current selection consists of {}.", gene_name, seq_count, adj_total_genomes);
                incorrect_input = true;
                continue;
            }
            new_gene_names += gene_name + ",";
            String info = "\r" + gene_name + ": ";
            Pantools.logger.info("{} Running initial alignment.", info);
            run_alignment_for_mlsa_concatenate(gene_name, mlsaProteinPath);
            convertAfaToAlignedFasta(genePath, alignedGeneAfaPath);
            HashMap<String, Integer> shared_snps_map = new HashMap<>();
            int[] prot_var_inf_sites = count_var_inf_sites_in_msa(shared_snps_map,
                    alignedGeneAfaPath,
                    "Multiple sequence alignment", "prot",
                    mlsaProteinPath.resolve(gene_name + ".order"), ""); // variable, informative, shared between all

            HashMap<String, int[]> seq_trim_map = new HashMap<>();
            int[] longest_start_end_gap = count_aa_to_trim(alignedGenePath, seq_trim_map);
            info += longest_start_end_gap[0] * 3 + "/" + longest_start_end_gap[1] * 3;
            Pantools.logger.info("{}. Running second alignment.", info);
            if (longest_start_end_gap[0] + longest_start_end_gap[1] > longest_start_end_gap[2]) {
                Pantools.logger.warn("No sequence left after trimming.");
                continue;
            }
            trim_sequences(seq_trim_map, gene_name, WORKING_DIRECTORY + "mlsa/input/nucleotide/" + gene_name + ".fasta", true);
            run_alignment_for_mlsa_concatenate(gene_name +"_trimmed", mlsaNucleotidePath);
            shared_snps_map = new HashMap<>();

            int[] nuc_var_inf_sites = count_var_inf_sites_in_msa(shared_snps_map,
                    mlsaNucleotidePath.resolve(gene_name + "_trimmed_aligned.afa"), "Multiple sequence alignment", "nuc",
                    mlsaProteinPath.resolve(gene_name + ".order"), ""); // variable, informative, shared between all

            info += " -> " + nuc_var_inf_sites[1];
            Pantools.logger.info("{}", info);
            delete_file_full_path(mlsaProteinPath.resolve(gene_name + ".order"));
            delete_file_full_path(mlsaNucleotidePath.resolve("nuc_alignment_info.txt"));
        }
        new_gene_names = new_gene_names.replaceFirst(".$",""); // removes last character
        if (new_gene_names.length() < 2) {
            Pantools.logger.error("None of the given input genes were appropriate for the MLSA.");
            Pantools.logger.error("1. Use the same genome selection as in 'mlsa_find_genes'");
            Pantools.logger.error("2. If the same selection is used but the problem reoccurs, check the log file of 'mlsa_find_genes' and adjust the input files.");
            System.exit(1);
        }
        if (incorrect_input) {
            Pantools.logger.info("At least one of the given input genes had the incorrect number of sequences for the MLSA.");
            Pantools.logger.info("If you want to include this gene:");
            Pantools.logger.info("1. Use the same genome selection as in 'mlsa_find_genes'");
            Pantools.logger.info("2. If the same selection is used but the problem reoccurs, check the log file of 'mlsa_find_genes' and adjust the input files.");
        }
        return new_gene_names.split(",");
    }

    /**
     * Create a genome_order file, required for the SNP extraction function
     * @param input
     * @return
     */
    public int count_seqs_in_fasta(String input) {
        int seq_count = 0;
        StringBuilder genome_order = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new FileReader(input))) {
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.equals("")) {
                    continue;
                }
                if (line.contains(">")) {
                    seq_count++;
                    line = line.replace(">","");
                    String[] line_array = line.split("_gn_");
                    String[] line_array2 = line_array[1].split("_");
                    genome_order.append(line_array2[0]).append(",").append(line).append("\n");
                }
            }
        } catch (IOException e) {
            Pantools.logger.error(e.getMessage());
            System.exit(1);
        }
        write_SB_to_file_full_path(genome_order, input.replace(".fasta", ".order"));
        return seq_count;
    }

    /**
     * Concatenate sequences into single sequence per genome.
     * @param gene_names
     */
    public static void concatenate_seq_in_dir(String[] gene_names) {
        String path = WORKING_DIRECTORY + "mlsa/input/nucleotide/";
        StringBuilder log_builder = new StringBuilder();
        File folder = new File(path);
        String[] files = folder.list();
        StringBuilder output = new StringBuilder();
        HashMap<String, StringBuilder> dna_seq_map = new HashMap<>();
        boolean stop = false;
        String stop_info = "";
        for (String file : files) {
            if (!file.contains("_trimmed.fasta")) {
                continue;
            }
            String gene_name = file.replace("_trimmed.fasta","");
            if (!ArrayUtils.contains(gene_names, gene_name)) {
                continue;
            }

            try {
                BufferedReader in = new BufferedReader(new FileReader(path + file));
                int line_count = 0;
                String current_genome = "";
                while (in.ready()) {
                    String line = in.readLine().trim();
                    if (line.equals("")) {
                        continue;
                    }
                    if (line.contains(">")) {
                        line_count++;
                        String[] line_list = line.split("gn_");
                        String[] line_list2 = line_list[1].split("_");
                        current_genome = line_list2[0];
                    } else {
                        try_incr_SB_hashmap(dna_seq_map, current_genome, line);
                    }
                }
                in.close();
                if (line_count < adj_total_genomes) {
                    stop = true;
                    stop_info = " lines found: " + line_count + ". should be " + adj_total_genomes;
                }
            } catch (IOException e) {
                Pantools.logger.error(e.getMessage());
                System.exit(1);
            }
        }
        if (stop) {
            Pantools.logger.error("Unable to use the concatenated sequence for MLSA.");
            Pantools.logger.error(stop_info);
            System.exit(1);
        }

        int counter = 0;
        TreeSet<Integer> all_sizes = new TreeSet<>();
        for (String genome_key : dna_seq_map.keySet()) {
            int genome_nr = Integer.valueOf(genome_key);
            StringBuilder builder = dna_seq_map.get(genome_key);
            String seq_str = builder.toString();
            if (skip_array[genome_nr-1]) {
                continue;
            }

            if (PHENOTYPE != null) {
                String pheno = geno_pheno_map.get(genome_nr);
                genome_key += "_" + pheno;
            }

            log_builder.append(genome_key).append(", ").append(seq_str.length()).append("bp\n");
            all_sizes.add(seq_str.length());
            output.append(">").append(genome_key).append("\n").append(seq_str).append("\n");
            counter++;
        }

        String info = counter + " genomes\n"
                + "Genes: " + SELECTED_NAME + "\n"
                + "All sizes: " + all_sizes + "\n\n";
        if (all_sizes.size() == 1) {
            Pantools.logger.info("All concatenated sequences have the same length: {}.", all_sizes.iterator().next());
        } else {
            Pantools.logger.info("The concatenated sequence lengths are between: {}-{}.", all_sizes.first(), all_sizes.last());
        }
        write_string_to_file_in_DB(info + log_builder.toString(), "mlsa/mlsa.info");
        write_SB_to_file_full_path(output, WORKING_DIRECTORY + "mlsa/input/concatenated.fasta");
    }

    /**
     * Collects a lot of statistics about genes that were found
     * @param gene_list
     * @param log_builder
     * @return
     *
     * The functions assumes there is only one mRNA for each gene. Will not work for eukaryotes
     */
    public boolean retrieve_gene_seq_check_presence_hmgroup(ArrayList<Node> gene_list, StringBuilder log_builder) {
        HashMap<Node, StringBuilder> prot_seqs_per_hmgroup = new HashMap<>(); // key is homology node.
        HashMap<Node, StringBuilder> nuc_seqs_per_hmgroup = new HashMap<>();
        HashMap<Node, ArrayList<Integer>> dna_lengths_per_hmgroup = new HashMap<>();
        HashMap<Node, ArrayList<Integer>> prot_lengths_per_hmgroup = new HashMap<>();
        StringBuilder prot_seq_builder = new StringBuilder();
        StringBuilder nuc_seq_builder = new StringBuilder();
        ArrayList<Integer> gene_length_list = new ArrayList<>();
        ArrayList<Integer> protein_length_list = new ArrayList<>();
        HashMap<Integer, Integer> gene_genome_map = new HashMap<>();
        int [] presence_array = new int[total_genomes];
        for(int i = 1 ; i <= total_genomes; i++) {
            gene_genome_map.put(i, 0);
        }
        HashMap<String, String> prot_seqs_per_genome = new HashMap<>();
        // key is genome_nr "_" + copy_number. Value is protein sequence used for when multiple copies are found
        HashMap<String, String> nuc_seqs_per_genome = new HashMap<>();
        Set<Node> all_hms = new HashSet<>(); // homology_group nodes

        String address_str = "";
        for (Node gene_node : gene_list) {
            int gene_length = 0, protein_length = 0;
            int genome_nr = (int) gene_node.getProperty("genome");
            if (skip_array[genome_nr-1]) {
                continue;
            }
            String gene_name = retrieveNamePropertyAsString(gene_node);
            Iterable<Relationship> rels = gene_node.getRelationships(RelTypes.codes_for);
            String prot_sequence = "", nuc_sequence = "", seq_header = "", mrna_node_str = "", hm_node_str = "no homology group";
            for (Relationship rel : rels) { // for MLSA, the functions assumes there is only one mRNA for each gene. Will not work for eukaryotes
                Node mrna_node = rel.getEndNode();
                mrna_node_str = mrna_node.getId() +"";
                if (!mrna_node.hasProperty("protein_ID")) {
                    continue;
                }

                String protein_id = (String) mrna_node.getProperty("protein_ID");
                gene_length = (int) mrna_node.getProperty("length");
                protein_length = (int) mrna_node.getProperty("protein_length");
                int[] address = (int[]) mrna_node.getProperty("address");
                address_str = address[0] + " " + address[1] + " " + address[2] + " " + address[3];
                gene_length_list.add(gene_length);
                protein_length_list.add(protein_length);
                Relationship hm_rel = mrna_node.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);

                prot_sequence = get_protein_sequence(mrna_node);
                nuc_sequence = get_nucleotide_sequence(mrna_node);
                prot_sequence = split_seq_in_parts_of_80bp(prot_sequence);
                nuc_sequence = split_seq_in_parts_of_80bp(nuc_sequence);
                String phenotype = get_phenotype_for_genome(genome_nr, true);
                if (hm_rel != null) { // mrna is connected to an homology_group node
                    Node hm_node = hm_rel.getStartNode();
                    hm_node_str =  hm_node.getId() +"";
                    if (mlsa_function) {
                        seq_header = ">" + protein_id + " " + gene_name + "_gn_" + genome_nr + "_" + gene_node.getId() + "_hmgroup_" + hm_node.getId() + phenotype + "\n";
                    } else {
                        seq_header = ">" + protein_id + " " + gene_name + " " + genome_nr + phenotype + "\n";
                    }
                    all_hms.add(hm_node);
                    try_incr_AL_hashmap(dna_lengths_per_hmgroup, hm_node, gene_length);
                    try_incr_AL_hashmap(prot_lengths_per_hmgroup, hm_node, protein_length);
                    try_incr_SB_hashmap(prot_seqs_per_hmgroup, hm_node, seq_header + prot_sequence +"\n");
                    try_incr_SB_hashmap(nuc_seqs_per_hmgroup, hm_node, seq_header + nuc_sequence +"\n");
                } else { // proteins are not clustered
                    if (mlsa_function) {
                        seq_header = ">" + protein_id + " " + gene_name + "_gn_" + genome_nr + "_" + gene_node.getId() + phenotype + "\n";
                    } else {
                        seq_header = ">" + protein_id + " " + gene_name + " " + genome_nr + phenotype + "\n";
                    }
                }
            }
            if (gene_length == 0) {
                log_builder.append(gene_name).append(", ").append(genome_nr).append(", ").append(gene_node.getId()).append(", ").append(mrna_node_str)
                        .append(", Protein sequence is missing! Something wrong with GFF?\n");
                continue;
            }
            presence_array[genome_nr-1]++;
            log_builder.append(gene_name).append("; ").append(genome_nr).append("; ").append(gene_node.getId()).append("; ").append(mrna_node_str).append("; ")
                    .append(hm_node_str).append("; ").append(gene_length).append("; ").append(protein_length).append("; ").append(address_str).append("\n");
            int gene_count = gene_genome_map.get(genome_nr);
            gene_count++;
            gene_genome_map.put(genome_nr, gene_count);
            prot_seqs_per_genome.put(genome_nr + "_" + gene_count + "_header", seq_header);
            prot_seqs_per_genome.put(genome_nr + "_" + gene_count, prot_sequence);
            nuc_seqs_per_genome.put(genome_nr + "_" + gene_count, nuc_sequence);
        }

        boolean multi_groups = false; // when true, the genes are found in multiple homology groups
        if (prot_seqs_per_hmgroup.size() > 1) {
            multi_groups = true;
            if (mlsa_function) {
                create_directory_in_DB("/mlsa/input/protein/" + SELECTED_NAME);
                create_directory_in_DB("/mlsa/input/nucleotide/" + SELECTED_NAME);
            } else {
                create_directory_in_DB("find_genes/by_name/protein_sequences/" + SELECTED_NAME);
                create_directory_in_DB("find_genes/by_name/nucleotide_sequences/" + SELECTED_NAME);
            }
            for (Node hm_node : prot_seqs_per_hmgroup.keySet()) {
                StringBuilder prot_seq = prot_seqs_per_hmgroup.get(hm_node);
                StringBuilder nuc_seq = nuc_seqs_per_hmgroup.get(hm_node);
                if (mlsa_function) {
                    write_SB_to_file_in_DB(prot_seq, "/mlsa/input/protein/" + SELECTED_NAME + "/" + hm_node.getId() + ".fasta");
                    write_SB_to_file_in_DB(nuc_seq, "/mlsa/input/nucleotide/" + SELECTED_NAME + "/" + hm_node.getId() + ".fasta");
                    append_SB_to_file_in_DB(nuc_seq, "/mlsa/input/protein/" + SELECTED_NAME + "/all_sequence.fasta");
                    append_SB_to_file_in_DB(nuc_seq, "/mlsa/input/nucleotide/" + SELECTED_NAME + "/all_sequence.fasta");
                } else {
                    write_SB_to_file_in_DB(prot_seq, "find_genes/by_name/nucleotide_sequences/" + SELECTED_NAME + "/" + hm_node.getId() + ".fasta");
                    write_SB_to_file_in_DB(nuc_seq, "find_genes/by_name/protein_sequences/" + SELECTED_NAME + "/" + hm_node.getId() + ".fasta");
                    append_SB_to_file_in_DB(nuc_seq, "find_genes/by_name/nucleotide_sequences/" + SELECTED_NAME + "/all_sequence.fasta");
                    append_SB_to_file_in_DB(nuc_seq, "find_genes/by_name/protein_sequences/" + SELECTED_NAME + "/all_sequence.fasta");
                }
            }
        }

        ArrayList<Integer> distinct_gene_lengths = remove_duplicates_from_AL_int(gene_length_list);
        String gene_lengths = determine_frequency_list_int(gene_length_list);
        double gene_average = get_average_from_AL_int(gene_length_list);
        String gene_avg_str = String.format("%.2f", gene_average);
        double protein_average = get_average_from_AL_int(protein_length_list);
        String prot_avg_str = String.format("%.2f", protein_average);
        String protein_lengths = determine_frequency_list_int(protein_length_list);

        log_builder.append("\nAverage gene length: ").append(gene_avg_str)
                .append("\nAll gene lengths: ").append(gene_lengths)
                .append("\nAverage protein length: ").append(prot_avg_str)
                .append("\nAll protein lengths: ").append(protein_lengths)
                .append("\n\nNumber of copies per genome ").append(Arrays.toString(presence_array)).append("\n");
        String absent_genomes = "Warning! Absent in genomes: ", present_genomes = "\nFound in genomes: ", multiple_copies = "Genomes with multiple copies: ";
        for (int genome_nr : gene_genome_map.keySet()) {
            if (skip_array[genome_nr-1]) {
                continue;
            }
            int value = gene_genome_map.get(genome_nr);
            if (value == 0) {
                absent_genomes += genome_nr + ",";
            } else {
                present_genomes += genome_nr + ",";
                if (value > 1) {
                    HashSet<Integer> duplicate_set = check_if_duplicate_sequences(value, genome_nr, prot_seqs_per_genome);
                    if (value - duplicate_set.size() == 1) {
                        if (duplicate_set.size() == 1) {
                            log_builder.append("Removed 1 gene from genome ").append(genome_nr).append(" because it is identical\n");
                        } else {
                            log_builder.append("Removed ").append(duplicate_set.size()).append(" genes from genome ")
                                    .append(genome_nr).append(" because they are identical\n");
                        }
                        for (int copy_number: duplicate_set) {
                            prot_seqs_per_genome.remove(genome_nr + "_" + copy_number);
                            nuc_seqs_per_genome.remove(genome_nr + "_" + copy_number);
                        }
                    } else { // extra copy
                        multiple_copies += genome_nr + ",";
                    }
                }
            }
        }

        for (Map.Entry<String, String> entry : nuc_seqs_per_genome.entrySet()) {
            String genome_str = entry.getKey();
            String nuc_seq = entry.getValue();
            String prot_seq = prot_seqs_per_genome.get(genome_str);
            String header = prot_seqs_per_genome.get(genome_str + "_header");
            prot_seq_builder.append(header).append(prot_seq).append("\n");
            nuc_seq_builder.append(header).append(nuc_seq).append("\n");
        }

        absent_genomes = absent_genomes.replaceFirst(".$","");
        present_genomes = present_genomes.replaceFirst(".$","");
        multiple_copies = multiple_copies.replaceFirst(".$","");
        if (present_genomes.length() > 18) {
            // do nothing
        } else {
            log_builder.append("\nWarning! Gene was not found!\n\n");
            warning_counter++;
            return false;
        }
        String[] present_array = present_genomes.split(",");
        String size_str = get_size_str(distinct_gene_lengths);
        Pantools.logger.info("{} -> {}/{}, {}.", SELECTED_NAME, gene_list.size(), present_array.length, size_str);
        boolean absent = true;
        if (absent_genomes.length() > 27) {
            log_builder.append(present_genomes).append("\n")
                    .append(absent_genomes).append("\n");
            warning_counter++;
        } else {
            log_builder.append("Gene was found in every genome!\n");
            absent = false;
        }

        if (multiple_copies.length() > 29) {
            log_builder.append("Warning! ").append(multiple_copies).append("\n");
            warning_counter++;
        }

        if (all_hms.size() == 1) {
            log_builder.append("All genes are part of the same homology group\n");
            if (multiple_copies.length() > 29 && !absent) { // 1 group but multiple copies, extra warning!
                log_builder.append("Warning! You must remove duplicate copies from the input files\n ")
                        .append(WORKING_DIRECTORY).append("/mlsa/input/nucleotide/").append(SELECTED_NAME).append(".fasta\n ")
                        .append(WORKING_DIRECTORY).append("/mlsa/input/protein/").append(SELECTED_NAME).append(".fasta\n");
                warning_counter++;
            }
        } else if (all_hms.size() > 1) {
            log_builder.append("Warning! Genes were found in ").append(all_hms.size()).append(" different homology groups! Sequences for are written to:\n")
                    .append(" ").append(WORKING_DIRECTORY).append("mlsa/input/protein/").append(SELECTED_NAME).append("/\n")
                    .append(" ").append(WORKING_DIRECTORY).append("mlsa/input/nucleotide/").append(SELECTED_NAME).append("/\n\n");
            warning_counter++;
        } else {
            log_builder.append("Warning! No (active) homology grouping is present!\n");
            warning_counter++;
        }

        StringBuilder hm_builder = new StringBuilder();
        int perfect_group_counter = 0;
        for (Node hm_node : all_hms) {
            ArrayList<Integer> dna_lengths_lists = dna_lengths_per_hmgroup.get(hm_node);
            ArrayList<Integer> prot_lengths_lists = prot_lengths_per_hmgroup.get(hm_node);
            String dna_freqs = determine_frequency_list_int(dna_lengths_lists);
            String prot_freqs = determine_frequency_list_int(prot_lengths_lists);
            int num_members = (int) hm_node.getProperty("num_members");
            int[] temp_copy_number = (int[]) hm_node.getProperty("copy_number");
            int[] copy_number = remove_first_position_array(temp_copy_number);
            hm_builder.append("Homology group ").append(hm_node.getId()).append(", ").append(num_members).append(" members ").append(Arrays.toString(copy_number))
                    .append("\nGene lengths: ").append(dna_freqs)
                    .append("\nProtein lengths: ").append(prot_freqs).append("\n");

            if (multi_groups && num_members == total_genomes && perfect_group_counter == 0) {
                log_builder.append("Warning! Multiple homology groups are found! Group ").append(hm_node.getId()).append(" is selected for ").append(SELECTED_NAME)
                        .append(".fasta as the group size matches the number of genomes.\n");
                prot_seq_builder = prot_seqs_per_hmgroup.get(hm_node);
                nuc_seq_builder = nuc_seqs_per_hmgroup.get(hm_node);
                warning_counter++;
                perfect_group_counter++;
            } else if (multi_groups && num_members == total_genomes) {
                log_builder.append("Warning! Group ").append(hm_node.getId()).append(" is also suitable for the analysis for ").append(SELECTED_NAME)
                        .append(". It was not selected because a suitable candidate was already found. \n");
            }
        }

        log_builder.append("\n").append(hm_builder.toString()).append("\n");
        if (mlsa_function) {
            write_SB_to_file_in_DB(prot_seq_builder, "/mlsa/input/protein/" + SELECTED_NAME + ".fasta");
            write_SB_to_file_in_DB(nuc_seq_builder, "/mlsa/input/nucleotide/" + SELECTED_NAME + ".fasta");
        } else {
            write_SB_to_file_in_DB(prot_seq_builder, "find_genes/by_name/protein_sequences/" + SELECTED_NAME + ".fasta");
            write_SB_to_file_in_DB(nuc_seq_builder, "find_genes/by_name/nucleotide_sequences/" + SELECTED_NAME + ".fasta");
        }
        return true;
    }

    /**
     * Returns string of sequence length or string that shows the range of lengths in the set
     * @param distinct_gene_lengths
     * @return
     */
    public String get_size_str(ArrayList<Integer> distinct_gene_lengths) {
        int size1 = distinct_gene_lengths.get(0);
        String size_str = size1 +"";
        if (distinct_gene_lengths.size() == 1) {
            size_str += "bp";
            return size_str;
        }
        int size2 = distinct_gene_lengths.get(distinct_gene_lengths.size() - 1);
        size_str += "-" + size2 + "bp";
        return size_str;
    }

    /**
     * Checks if there are duplicated sequences in prot_seqs_per_genome
     * @param value
     * @param genome_nr
     * @param prot_seqs_per_genome
     * @return
     */
    public HashSet<Integer> check_if_duplicate_sequences(int value, int genome_nr, HashMap<String, String> prot_seqs_per_genome) {
        HashSet<Integer> duplicate_set = new HashSet<>();
        for (int i = 1; i <= value; i++) {
            String prot_sequence1 = prot_seqs_per_genome.get(genome_nr + "_" + i);
            for (int j = i+1; j <= value; j++) {
                if (i == j) {
                    continue;
                }
                String prot_sequence2 = prot_seqs_per_genome.get(genome_nr + "_" + j);
                if (prot_sequence1.equals(prot_sequence2)) {
                    duplicate_set.add(j);
                }
            }
        }
        return duplicate_set;
    }

    /**
     * Calculates ANI scores for every genome combination. These results should be comparable to JspeciesWS.
     */
    public void calculate_ani() {
        String program = mash_or_fastaANI(); // check --mode argument and checks if required software is set to $PATH
        create_directory_in_DB("/databases/genome.db/genomes");
        HashMap<Integer, String> genome_path_map = new HashMap<>();
        HashMap<String,Integer> path_genome_map = new HashMap<>();

        connect_pangenome(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "ANI"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        // index and genomescanner required for reconstructing genomes (if required)
        INDEX_SC = new IndexScanner(INDEX_DB);
        GENOME_SC = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_SC.get_pre_len());

        create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            ResourceIterator<Node> genome_nodes = GRAPH_DB.findNodes(GENOME_LABEL);
            while (genome_nodes.hasNext()) { // retrieve genome locations
                Node genome_node = genome_nodes.next();
                int genome_nr = (int) genome_node.getProperty("number");
                String genome_path = (String) genome_node.getProperty("path");
                genome_path = reconstruct_genome_if_not_present(genome_path, genome_nr);

                genome_path_map.put(genome_nr, genome_path);
                path_genome_map.put(genome_path, genome_nr);
            }

            HashMap<String, Double> ani_score_map = calculate_ani_loop(genome_path_map, path_genome_map, program);
            boolean typestrains_present = compare_to_typestrains(ani_score_map, program);
            create_ani_distance_matrix(program);
            create_ani_rscript(program);

            tx.success(); // transaction successful, commit changes

            Pantools.logger.info("Output written to:");
            Pantools.logger.info(" {}ANI/{}/ANI_scores.csv", WORKING_DIRECTORY, program);
            Pantools.logger.info(" {}ANI/{}/ANI_distance_matrix.csv", WORKING_DIRECTORY, program);
            Pantools.logger.info(" {}ANI/{}/ANI_tree.R", WORKING_DIRECTORY, program);
            if (typestrains_present) {
                Pantools.logger.info(" {}ANI/{}/typestrain_comparison.csv", WORKING_DIRECTORY, program);
            }
        }
    }

    /**
     * Creates a genome fasta file if it doesn't exist in the given location
     * @param genome_path
     * @param genome_nr
     * @return
     */
    public static String reconstruct_genome_if_not_present(String genome_path, int genome_nr) {
        if (check_if_file_exists(genome_path)) {
            return genome_path;
        }
        StringBuilder seq = new StringBuilder();
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "/databases/genome.db/Genome_" + genome_nr + ".fasta"))) {
            for (int sequence = 1; sequence <= GENOME_DB.num_sequences[genome_nr]; sequence++) {
                out.write(">" + GENOME_DB.sequence_titles[genome_nr][sequence] + "\n");
                int begin = 0;
                int end = (int) GENOME_DB.sequence_length[genome_nr][sequence] - 1;
                seq.setLength(0);
                GENOME_SC.get_sub_sequence(seq, genome_nr, sequence, begin, end - begin + 1, true);
                write_fasta(out, seq.toString(), 80);
                seq.setLength(0);
            }
            out.close();
        } catch (IOException e) {
            Pantools.logger.error(e.getMessage());
            System.exit(1);
        }
        String[] wd_array = WORKING_DIRECTORY.split("/");
        return wd_array[wd_array.length-1] + "/databases/genome.db/Genome_" + genome_nr + ".fasta";
    }

    /**
     * Bit of unneccesary function before actually starting the ani loop calculations
     * @param genome_path_map
     * @param path_genome_map
     * @param program is 'MASH' or 'fastANI'
     * @return
     */
    public static HashMap<String, Double> calculate_ani_loop(HashMap<Integer, String> genome_path_map,
                                                             HashMap<String, Integer> path_genome_map, String program) {

        StringBuilder header = new StringBuilder("Genome,");
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]){
                continue;
            }
            String phenotype = get_phenotype_for_genome(i, true);
            header.append(i).append(phenotype).append(",");
        }

        StringBuilder table_build = new StringBuilder(header.toString().replaceFirst(".$","")); //remove last character
        HashMap<String, Double> ani_score_map = new HashMap<>();
        if (Mode.equals("FASTANI")) {
            calculate_ani_loop_fastani(genome_path_map);
            read_fastani_output(path_genome_map, ani_score_map, table_build);
        } else { // MASH
            calculate_ani_loop_mash(table_build, genome_path_map, ani_score_map);
        }
        write_SB_to_file_in_DB(table_build, "ANI/" + program + "/ANI_scores.csv");
        return ani_score_map;
    }

    /**
     * Loop over all genomes to run MASH
     * @param table_build
     * @param genome_path_map
     * @param ani_score_map
     */
    public static void calculate_ani_loop_mash(StringBuilder table_build, HashMap<Integer, String> genome_path_map, HashMap<String, Double> ani_score_map) {
        double ani;
        for (int i = 1; i <= total_genomes; i++) {
            create_directory_in_DB("ANI/MASH/calculations/" + i);
        }

        for (int i = 1; i <= total_genomes; i++) {
            System.out.print("\rCalculating ANI: " + i + "/" + total_genomes);
            if (skip_array[i-1]) {
                continue;
            }
            String phenotype = get_phenotype_for_genome(i, true);
            table_build.append("\n").append(i).append(phenotype).append(",");
            String path1 = genome_path_map.get(i);
            for (int j = 1; j <= total_genomes; j++) {
                if (skip_array[j-1]) {
                    continue;
                }
                if (j < i) { // get ani score from earlier loop
                    ani = ani_score_map.get(j + " " + i);
                    table_build.append(ani * 100);
                    write_string_to_file_in_DB(ani +"", "ANI/MASH/calculations/" + j + "/" + i + "_" + j);
                } else if (j == i) {
                    table_build.append("100");
                } else {
                    String ani_file = WORKING_DIRECTORY + "/ANI/MASH/calculations/" + i + "/" + i + "_" + j;
                    if (check_if_file_exists(ani_file)) {
                        ani = read_ani_from_file(ani_file);
                        ani_score_map.put(i + " " + j, ani);
                        table_build.append(ani * 100);
                    } else {
                        String path2 = genome_path_map.get(j);
                        ani = run_mash(path1, path2, i, j);
                        table_build.append(ani * 100);
                        ani_score_map.put(i + " " + j, ani);
                        write_string_to_file_in_DB(ani +"", "ANI/MASH/calculations/" + i + "/" + i + "_" + j);
                        write_string_to_file_in_DB(ani +"", "ANI/MASH/calculations/" + j + "/" + i + "_" + j);
                    }
                }
                if (j != total_genomes) {
                    table_build.append(",");
                }

            }
        }
    }

    /**
     * Runs MASH between two genomes and returns the ANI score as a Double
     * @param path1
     * @param path2
     * @param genome1_nr
     * @param genome2_nr
     * @return ani score
     */
    public static double run_mash(String path1, String path2, int genome1_nr, int genome2_nr) {
        String[] mash_command = new String[] {"mash", "dist", path1, path2};
        String mash_output = ExecCommand.ExecCommand(mash_command);
        if (mash_output.equals("")) {
            Pantools.logger.error("Calculation between genome {} and {} failed. Please run the following command to identify the issue `mash dist {} {}`", genome1_nr, genome2_nr, path1, path2);
            System.exit(1);
        }
        String[] output_array = mash_output.split("\t");
        double distance = Double.parseDouble(output_array[2]);
        double ani = 1 - distance;
        return ani;
    }

    /**
     * Runs fastANI software
     * @param genome_path_map
     */
    public static void calculate_ani_loop_fastani(HashMap<Integer, String> genome_path_map) {
        StringBuilder files = new StringBuilder();
        for (int i = 1; i <= total_genomes; i++) {
            String path1 = genome_path_map.get(i);
            files.append(path1).append("\n");
        }

        write_SB_to_file_in_DB(files, "ANI/fastANI/references.txt");
        String path = WORKING_DIRECTORY + "ANI/fastANI/";
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            System.out.print("\r Running fastANI: " + i + "/" + total_genomes + " genomes");
            String query = genome_path_map.get(i);
            if (!check_if_file_exists(path + "calculations/" + i)) {
                String[] ani_command = {"fastANI", "-q", query, "--threads", THREADS +"", "--rl", path + "references.txt", "-o", path + "calculations/" + i}; // one vs all
                ExecCommand.ExecCommand(ani_command);
            }
        }
        delete_file_full_path(path + "references.txt");
    }

    /**
      Reads the output file from the FASTANI tool
     * @param path_genome_map
     * @param ani_score_map
     * @param table_build
     */
    public static void read_fastani_output(HashMap<String, Integer> path_genome_map, HashMap<String, Double> ani_score_map, StringBuilder table_build) {
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "ANI/fastANI/calculations/" + i))) {
                ArrayList<Integer> present_genomes = new ArrayList<>();
                while (in.ready()) {
                    String line = in.readLine().trim();
                    String[] line_array = line.split("\t");
                    double d = Double.parseDouble(line_array[2]);
                    d = d/100;
                    int genome2 = path_genome_map.get(line_array[1]); // based on the filename, get the genome number
                    present_genomes.add(genome2);
                    if (genome2 > i) {
                        ani_score_map.put(i + " " + genome2, d);
                    } else if (genome2 < i) {
                        ani_score_map.put(genome2 + " " + i, d);
                    } else { // if the same
                        ani_score_map.put(genome2 + " " + i, 1.0);
                    }
                }

                if (present_genomes.size() != total_genomes) { // no ANI score means the score is below 0.80
                    for (int j = 1; j <= total_genomes; j++) {
                        if (present_genomes.contains(j)) {
                            continue;
                        }
                        if (j > i) {
                            ani_score_map.put(i + " " + j, 0.70);
                        } else {
                            ani_score_map.put(j + " " + i, 0.70);
                        }
                    }
                }
            } catch (IOException ioe) {
                Pantools.logger.error("Failed to read: {}ANI/fastANI/calculations/{}", WORKING_DIRECTORY, i);
                System.exit(1);
            }
        }

        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            String phenotype = get_phenotype_for_genome(i, true);
            table_build.append("\n").append(i).append(phenotype);
            for (int j = 1; j <= total_genomes; j++) {
                if (skip_array[j-1]) {
                    continue;
                }
                double ani;
                if (j > i) {
                    ani = ani_score_map.get(i + " " + j);
                } else {
                    ani = ani_score_map.get(j + " " + i);
                }
                table_build.append(",").append(ani * 100);
            }
        }
    }

    /**
     * Write ANI scores to file
     * @param program is 'MASH' or 'fastANI'
     */
    public void create_ani_distance_matrix(String program) {
        StringBuilder rev_ani_builder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "/ANI/" + program + "/ANI_scores.csv"))) {
            boolean first = true;
            while (in.ready()) {
                String line = in.readLine().trim();
                if (first) { // header is the same
                    rev_ani_builder.append(line).append("\n");
                    first = false;
                    continue;
                }
                String[] line_array = line.split(",");
                boolean first_column = true;
                for (int i = 0; i < line_array.length; i++) {
                    if (first_column) {
                        rev_ani_builder.append(line_array[i]).append(",");
                        first_column = false;
                        continue;
                    }
                    if (line_array[i].equals("")) {
                        rev_ani_builder.append(",");
                        continue;
                    }
                    double new_value = 1 - (Double.parseDouble(line_array[i])/100);
                    rev_ani_builder.append(String.format("%.5f", new_value));
                    if (i != line_array.length-1) {
                        rev_ani_builder.append(",");
                    }
                }
                rev_ani_builder.append("\n");
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}/ANI/{}/ANI_scores.csv", WORKING_DIRECTORY, program);
            System.exit(1);
        }
        write_SB_to_file_in_DB(rev_ani_builder, "ANI/" + program + "/ANI_distance_matrix.csv");
    }

    /**
     * Create Rscript for ANI NJ tree
     * @param program is 'MASH' or 'fastANI'
     */
    public static void create_ani_rscript(String program) {
        String R_LIB = check_r_libraries_environment();
        StringBuilder rscript = new StringBuilder ();
        rscript.append("#! /usr/bin/env RScript\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"ape\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(ape)\n")
                .append("input = read.csv(\"").append(WD_full_path).append("ANI/").append(program).append("/ANI_distance_matrix.csv\", sep=\",\",header = TRUE)\n")
                .append("df2 = subset(input, select = -c(Genome))\n")
                .append("df.dist2 =as.matrix(df2, labels=TRUE)\n")
                .append("colnames(df.dist2) <- rownames(df.dist2) <- input[['Genome']]\n")
                .append("NJ_tree <- nj(df.dist2)\n")
                .append("pdf(NULL)\n")
                .append("plot(NJ_tree, main = \"Neighbor Joining\")\n")
                .append("write.tree(NJ_tree, tree.names = TRUE, file=\"").append(WD_full_path).append("ANI/").append(program).append("/ANI.newick\")\n")
                .append("cat(\"\\nANI tree written to: ").append(WD_full_path).append("ANI/").append(program).append("/ANI.newick\\n\\n\")");
        write_SB_to_file_in_DB(rscript, "ANI/" + program + "/ANI_tree.R");
    }


    /**
     * Read the ANI score from a previous run
     * @param ani_file
     * @return ani
     */
    public static double read_ani_from_file(String ani_file) {
        double ani = 0;
        try (BufferedReader in = new BufferedReader(new FileReader(ani_file))) {
            while (in.ready()) {
                String line = in.readLine().trim();
                ani = Double.valueOf(line);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", ani_file);
            System.exit(1);
        }
        return ani;
    }

    /**
     * Return true when 'typestrain' property is present in the phenotype node of genome 1
     * @return
     */
    public static boolean check_if_typestrain_present() {
        boolean present = false;
        ResourceIterator<Node> pheno_nodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL, "genome", 1);
        while (pheno_nodes.hasNext()) { // iterator contains only only one node
            Node pheno_node = pheno_nodes.next();
            if (pheno_node.hasProperty("typestrain")) {
                present = true;
            }
        }
        return present;
    }

    /**
     * Creates a table with the ANI scores to the available type strains in the pangenome
     * Only when phenotype nodes have the 'typestrain' phenotype
     * @param ani_score_map
     * @param program is 'MASH' or 'fastANI'
     * @return boolean
     */
    public boolean compare_to_typestrains(HashMap<String, Double> ani_score_map, String program) {
        boolean typestrains_present = check_if_typestrain_present();
        if (!typestrains_present) {
            return false;
        }
        ResourceIterator<Node> pheno_nodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
        ArrayList<Integer> genomes_with_typestrains = new ArrayList<>();
        HashMap<Integer, String> genome_ts_map = new HashMap<>();
        while (pheno_nodes.hasNext()) { //retrieve all 'typestrain' phenotypes from phenotype nodes
            Node pheno_node = pheno_nodes.next();
            int genome_nr = (int) pheno_node.getProperty("genome");
            if (skip_array[genome_nr-1]) {
                continue;
            }
            String typestrain = (String) pheno_node.getProperty("typestrain");
            if (!typestrain.equals("?")) {
                genomes_with_typestrains.add(genome_nr);
                genome_ts_map.put(genome_nr, typestrain);
            }
        }
        if (genomes_with_typestrains.isEmpty()) {
            Pantools.logger.warn("Even though the 'typestrain' property is present, no typestrains were found.");
            return false;
        }

        String header = "";
        for (int genome_nr : genomes_with_typestrains) {
            String name = genome_ts_map.get(genome_nr);
            header += genome_nr + " " + name + ",";
        }
        StringBuilder output_builder;
        if (PHENOTYPE == null) {
            output_builder = new StringBuilder("Genome number,Highest score,Closest type strain,Above 95%,," + header.replaceFirst(".$","") +"\n");
        } else {
            output_builder = new StringBuilder("Genome number,Current phenotype,Highest score,Closest type strain,Above 95%,,"
                    + header.replaceFirst(".$","") +"\n");
        }

        for (int genome1 = 1; genome1 <= total_genomes; genome1++) {
            if (skip_array[genome1-1]) {
                continue;
            }
            HashMap<Double, ArrayList<Integer>> score_genome_map = new HashMap<>();
            Double highest = 0.0;
            String all_scores = "";
            for (int genome2 : genomes_with_typestrains) {
                if (skip_array[genome2-1]) {
                    continue;
                }
                String key = genome1 + " " + genome2;
                if (genome1 > genome2) {
                    key = genome2 + " " + genome1;
                }
                Double score = ani_score_map.get(key);
                if (score == null) {
                    score = 1.0;
                }
                try_incr_AL_hashmap(score_genome_map, score, genome2);
                all_scores += score + ",";
                if (score > highest) {
                    highest = score;
                }
            }

            boolean close_enough = true;
            if (highest < 0.95) {
                close_enough = false;
            }

            // go over all genomes with the highest score
            StringBuilder bestTypeStrains = new StringBuilder();
            for (int genome_nr : score_genome_map.get(highest)) {
                String name = genome_ts_map.get(genome_nr);
                bestTypeStrains.append(genome_nr + " " + name + "/");
            }

            String phenotypeValue = geno_pheno_map.get(genome1);
            if (phenotypeValue != null) {
                phenotypeValue += ",";
            } else {
                phenotypeValue = "";
            }
            output_builder.append(genome1).append(",").append(phenotypeValue).append(highest).append(",")
                    .append(bestTypeStrains.toString().replaceFirst(".$","")).append(",")
                    .append(close_enough).append(",,").append(all_scores.replaceFirst(".$","")).append("\n");
        }
        write_SB_to_file_in_DB(output_builder, "ANI/" + program + "/typestrain_comparison.csv");
        return true;
    }

    /**
     * Create appropriate directories for selected ANI calculation tool
     * Two options: MASH or FASTANI
     * @return
     */
    public static String mash_or_fastaANI() {
        String program = "MASH";
        if (Mode.equals("0")) { // default value of Mode
            Pantools.logger.info("Calculate ANI scores.");
            create_directory_in_DB("ANI/MASH/calculations");
            check_if_program_exists_stdout("mash -h", 100, "MASH"); // check if program is set to $PATH
            Pantools.logger.info("No --mode provided. Using MASH (default).");
            Mode = "MASH";
        } else if (Mode.startsWith("MASH")) {
            create_directory_in_DB("ANI/MASH/calculations");
            check_if_program_exists_stdout("mash -h", 100, "MASH"); // check if program is set to $PATH
            Pantools.logger.info("Calculate ANI scores using MASH.");
        } else if (Mode.startsWith("FASTANI")) {
            create_directory_in_DB("ANI/fastANI/calculations");
            Pantools.logger.info("Calculate ANI scores using fastANI.");
            program = "fastANI";
            check_if_program_exists_stderr("fastANI -h", 100, "fastANI", true); // check if program is set to $PATH
            report_number_of_threads(); // prints how many threads were selected by user
        } else {
            Pantools.logger.error("Mode not recognized.");
            System.exit(1);
        }
        return program;
    }

    /**
     * Create overview of the color hex codes.
     */
    public void create_color_code_text_file() {
        String color_codes = "https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/ \n\n"
                + "Pink - #fabebe\nLime - #bfef45\nCyan - #42d4f4\nApricot - #ffd8b1\nMint - #aaffc3\nBeige - #fffac8\nLavender - #e6beff\nTeal - #469990\nRed - #e6194B\n"
                + "Orange - #f58231\nYellow - #ffe119\nGreen - #3cb44b\nBlue - #4363d8\nPurple - #911eb4\nGrey - #a9a9a9\nMaroon - #800000\nOlive - #808000\nBrown - #9A6324\n"
                + "Navy - #000075\nMagenta - #f032e6\n\nUnused colors\nLight grey #E8E8E8";
        write_string_to_file_in_DB(color_codes, "tree_templates/color_code_guide.txt");
    }

    /**
     Create ITOL templates
     https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/

     Requires
     -dp

     Optional
     --phenotype or -ph
     --value  the number of genomes a phenotype should have before a color is assigned
     --mode no-numbers

     */
    public void create_tree_templates() {
        Pantools.logger.info("Creating templates for phylogenetic trees in ITOL.");
        create_directory_in_DB("tree_templates/label/");
        create_directory_in_DB("tree_templates/ring/");
        create_color_code_text_file();
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            create_skip_arrays(true, true); // create skip array if -skip/-ref is provided by user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        boolean print_header = true;
        HashMap<String, HashSet<String>> pheno_property_map = new HashMap<>();
        HashMap<String, ArrayList<Integer>> genomes_per_phenotype_map = new HashMap<>();
        StringBuilder to_many_pheno_values = new StringBuilder();
        int required_genomes = 2;
        if (NODE_VALUE != null) {
            try {
                required_genomes = Integer.parseInt(NODE_VALUE);
            } catch (NumberFormatException no) {
                Pantools.logger.error("'{}' is not a numerical value.", NODE_VALUE);
                System.exit(1);
            }
            System.out.println("\rSelected --value is " + NODE_VALUE + ". A phenotype must have this many genomes before a color is assigned");
        } else {
            System.out.println("\rNo --value was selected. A color is assigned to phenotype values present in least 2 genomes");
        }

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            if (PHENOTYPE != null) {
                if (Mode.contains("NO-NUMBERS")) {
                    Pantools.logger.info("The phenotype '{}' was included and genome numbers are exluded! The created templates will match trees where nodes only contain a phenotype value.", PHENOTYPE);
                } else {
                    Pantools.logger.info("The phenotype '{}' was included. The created templates will match trees where nodes have genome numbers combined with the '{}' phenotype values! Include the --mode no-numbers argument to EXCLUDE genome numbers from templates.", PHENOTYPE, PHENOTYPE);
                }
                retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
                create_directory_in_DB("tree_templates/label/" + PHENOTYPE);
                create_directory_in_DB("tree_templates/ring/" + PHENOTYPE);
            } else {
                if (Mode.contains("NO-NUMBERS")) {
                    Pantools.logger.info("--mode no-numbers cannot be selected when no phenotype is included");
                    return;
                }
                Pantools.logger.info("No --phenotype included. The created templates will only match a tree with genome numbers.");
                create_directory_in_DB("tree_templates/label/genome_numbers");
                create_directory_in_DB("tree_templates/ring/genome_numbers");
            }
            retrieve_phenotypes_for_tree_templates(pheno_property_map, genomes_per_phenotype_map);
            for (String phenotype : pheno_property_map.keySet()) {
                HashSet<String> phenotype_values = pheno_property_map.get(phenotype);
                AtomicInteger counter = new AtomicInteger(0);
                String[] label_ring_output = create_label_ring_templates(phenotype, phenotype_values, genomes_per_phenotype_map, counter, required_genomes);
                print_header = write_label_ring_output(label_ring_output, counter.get(), print_header, phenotype, phenotype_values, to_many_pheno_values);
            }
            tx.success(); // transaction successful, commit changes
        }

        String to_many_pheno_val_str = to_many_pheno_values.toString();
        if (to_many_pheno_val_str.length() > 1) {
            Pantools.logger.info("Phenotypes with more than the 20 allowed values -> Required colors:");
            Pantools.logger.info("{}", to_many_pheno_val_str);
        }

        Pantools.logger.info("Template files written to:");
        if (PHENOTYPE != null) {
            Pantools.logger.info(" {}tree_templates/ring/{}/", WORKING_DIRECTORY, PHENOTYPE);
            Pantools.logger.info(" {}tree_templates/label/{}/", WORKING_DIRECTORY, PHENOTYPE);
        } else {
            Pantools.logger.info(" {}tree_templates/", WORKING_DIRECTORY);
        }
    }

    /**
     * Write the template to their files
     * @param label_ring_output
     * @param counter
     * @param print_header
     * @param phenotype
     * @param phenotype_values
     * @param to_many_pheno_values
     * @return
     */
    public boolean write_label_ring_output(String[] label_ring_output, int counter, boolean print_header, String phenotype,
                                           HashSet<String> phenotype_values, StringBuilder to_many_pheno_values) {

        if (counter-1 < COLOR_CODES.length) {
            if (print_header) {
                Pantools.logger.info("Phenotype name, Different phenotypes -> Used colors.");
                print_header = false;
            }
            Pantools.logger.info(" {}, {} -> {} colors.", phenotype, phenotype_values.size(), counter);
            if (PHENOTYPE != null) {
                write_string_to_file_in_DB(label_ring_output[0], "tree_templates/label/" + PHENOTYPE + "/" + phenotype + ".txt");
                write_string_to_file_in_DB(label_ring_output[1], "tree_templates/ring/" + PHENOTYPE + "/" + phenotype + ".txt");
            } else {
                write_string_to_file_in_DB(label_ring_output[0], "tree_templates/label/genome_numbers/" + phenotype + ".txt");
                write_string_to_file_in_DB(label_ring_output[1], "tree_templates/ring/genome_numbers/" + phenotype + ".txt");
            }
        } else {
            to_many_pheno_values.append(" ").append(phenotype).append(", ").append(counter).append("\n");
        }
        return print_header;
    }

    /**
     * Create the ring and label templates for ITOL
     * @param key
     * @param phenotypes
     * @param genomes_per_phenotype_map
     * @param counter
     * @param required_genomes default is 2, can be adjusted by user
     * @return
     */
    public String[] create_label_ring_templates(String key, HashSet<String> phenotypes,
                                                HashMap<String, ArrayList<Integer>> genomes_per_phenotype_map, AtomicInteger counter, int required_genomes) {

        String label = key;
        if (PHENOTYPE != null) {
            label = PHENOTYPE + " colored_by " + key;
        }
        StringBuilder label_builder = new StringBuilder("TREE_COLORS\nSEPARATOR COMMA\nDATA\n\n");
        StringBuilder ring_builder = new StringBuilder("DATASET_COLORSTRIP\nSEPARATOR COMMA\nDATASET_LABEL," + label
                + "\nCOLOR,#ff0000\nSTRIP_WIDTH,40\nSHOW_INTERNAL,1\nDATA\n\n");

        // first loop to check if the number of phenotype colors exceeds 8
        int requiredColors = 0;
        for (String value : phenotypes) {
            ArrayList<Integer> genomes = genomes_per_phenotype_map.get(key + "##" + value);
            if (genomes.size() < required_genomes) {
                continue;
            }
            requiredColors++;
        }

        for (String value : phenotypes) {
            String pheno_value = value;
            ArrayList<Integer> genomes = genomes_per_phenotype_map.get(key + "##" + value);
            if (genomes.size() < required_genomes) {
                continue;
            }
            for (int genome_nr : genomes) {
                if (counter.get() < COLOR_CODES.length) {
                    String phenotype_info = "";
                    if (PHENOTYPE != null) {
                        if (geno_pheno_map != null) {
                            pheno_value = geno_pheno_map.get(genome_nr);
                        }
                        phenotype_info = "_" + pheno_value;
                    }
                    if (Mode.contains("NO-NUMBERS")) {
                        label_builder.append(pheno_value);
                        ring_builder.append(pheno_value);
                    } else {
                        label_builder.append(genome_nr).append(phenotype_info);
                        ring_builder.append(genome_nr).append(phenotype_info);
                    }
                    if (requiredColors < 9) { // use colourblind friendly pallette
                        label_builder.append(",range,").append(COLORBLIND_CODES[counter.get()]).append(",").append(value).append(",1.0\n");
                        ring_builder.append(",").append(COLORBLIND_CODES[counter.get()]).append(",").append(value).append("\n");
                    } else {
                        label_builder.append(",range,").append(COLOR_CODES[counter.get()]).append(",").append(value).append(",1.0\n");
                        ring_builder.append(",").append(COLOR_CODES[counter.get()]).append(",").append(value).append("\n");
                    }
                }
            }
            counter.getAndIncrement();
        }
        return new String[]{label_builder.toString(), ring_builder.toString()};
    }

    /**
     * Extracts all available phenotypes from 'phenotype' nodes
     * @param pheno_property_map
     * @param genomes_per_phenotype_map
     */
    public void retrieve_phenotypes_for_tree_templates(HashMap<String, HashSet<String>> pheno_property_map,
                                                       HashMap<String, ArrayList<Integer>> genomes_per_phenotype_map) {

        ResourceIterator<Node> phenotype_nodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
        while (phenotype_nodes.hasNext()) {
            Node pheno_node = phenotype_nodes.next();
            int genome = (int) pheno_node.getProperty("genome");
            Iterable<String> node_properties = pheno_node.getPropertyKeys();
            for (String property : node_properties) {
                if (property.equals("genome")) {
                    continue;
                }
                String value_str = "";
                Object value = pheno_node.getProperty(property);
                if (value instanceof Integer || value instanceof String || value instanceof Boolean) {
                    value_str = value.toString();
                } else if (value instanceof Double || value instanceof Long) {
                    value_str = String.valueOf(value);
                } else {
                    Pantools.logger.error("this function must be extended.. 50623.");
                    System.exit(1);
                }
                if (value.equals("") || value.equals("?")) {
                    continue;
                }
                try_incr_hashset_hashmap(pheno_property_map, property, value_str);
                try_incr_AL_hashmap(genomes_per_phenotype_map, property + "##" + value_str, genome);
            }
        }
    }

    /**
     * Requires
     * --database-path
     * --input-file a phylogenetic tree
     *
     * Optional
     * --phenotype
     * --mode no-numbers == mode 1
     *
     * This function only works on the phylogenetic trees on 1 of the 5 methods. A genome number may only occur once.
     */
    public void rename_phylogeny() {
        Pantools.logger.info("Rename a Newick tree formatted phylogeny.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            total_genomes = (int) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_genomes");
            if (PHENOTYPE == null) {
                Pantools.logger.info("No --phenotype provided! Placing only genome numbers at the terminal nodes (leaves).");
            } else {
                Pantools.logger.info("Selected '{}' as phenotype. Using this to rename the terminal nodes (leaves).", PHENOTYPE);
                skip_array = new boolean [total_genomes]; // required by retrieve_phenotypes
                retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            }
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        boolean exclude_numbers = false;
        if (Mode.contains("NO-NUMBERS")) {
            if (PHENOTYPE == null) {
                Pantools.logger.info("--mode no-numbers can only be used in combination with --phenotype.");
                return;
            }
            Pantools.logger.info("Selected --mode no-numbers. Removing the genome numbers from the leaves.");
            exclude_numbers = true;
        }

        ArrayList<String> lines = readNewickOrNexus(true);
        if (lines.size() == 1) {
            String phylogeny = lines.get(0);
            int total_changes = 0;
            boolean correct_tree = true;
            for (int i = 1; i <= total_genomes; i++) {
                int changes_counter = 0; // per genome, this function should make 1 change, if not the case, the tree cannot be used.
                String phenotype = create_phenotype_for_rename_phylo(i);
                if (phylogeny.contains("(" + i + ":")) {
                    if (exclude_numbers) {
                        phylogeny = phylogeny.replace("(" + i + ":", "(" + phenotype + ":");
                    } else {
                        phylogeny = phylogeny.replace("(" + i + ":", "(" + i + phenotype + ":");
                    }
                    changes_counter++;
                } else if (phylogeny.contains("(" + i + "_")) {
                    String[] phylogeny_array = phylogeny.split("\\(" + i + "_");
                    if (phylogeny_array.length != 2) {
                        correct_tree = false;
                        break;
                    }
                    String str = phylogeny_array[1];
                    int index1 = str.indexOf(':');
                    if (exclude_numbers) {
                        phylogeny = phylogeny_array[0] + "(" + phenotype + str.substring(index1, str.length());
                    } else {
                        phylogeny = phylogeny_array[0] + "(" + i + phenotype + str.substring(index1, str.length());
                    }
                    changes_counter++;
                } else if (phylogeny.contains("(" + i + ",")) {
                    if (exclude_numbers) {
                        phylogeny = phylogeny.replace("(" + i + ",", "(" + phenotype + ",");
                    } else {
                        phylogeny = phylogeny.replace("(" + i + ",", "(" + i + phenotype + ",");
                    }
                    changes_counter++;
                } else if (phylogeny.contains("," + i + ")")) {
                    changes_counter++;
                    if (exclude_numbers) {
                        phylogeny = phylogeny.replace("," + i + ")", "," + phenotype + ")");
                    } else {
                        phylogeny = phylogeny.replace("," + i + ")", "," + i + phenotype + ")");
                    }
                } else if (phylogeny.contains("," + i + ":")) {
                    changes_counter++;
                    if (exclude_numbers) {
                        phylogeny = phylogeny.replace("," + i + ":", "," + phenotype + ":");
                    } else {
                        phylogeny = phylogeny.replace("," + i + ":", "," + i + phenotype + ":");
                    }
                } else if (phylogeny.contains("," + i + "_")) {
                    changes_counter++;
                    String[] phylogeny_array = phylogeny.split("," + i + "_");
                    if (phylogeny_array.length != 2) {
                        correct_tree = false;
                        break;
                    }
                    String str = phylogeny_array[1];
                    int index1 = str.indexOf(':');
                    if (exclude_numbers) {
                        phylogeny = phylogeny_array[0] + "," + phenotype + str.substring(index1, str.length());
                    } else {
                        phylogeny = phylogeny_array[0] + "," + i + phenotype + str.substring(index1, str.length());
                    }
                }

                if (changes_counter > 1) { // the function assumes each genome is present only once
                    correct_tree = false;
                    break;
                } else if (changes_counter == 0) { // genome nr is not present in the phylogeny
                    // do nothing
                } else { // 1 change
                    total_changes++;
                }
            }

            if (correct_tree) {
                String outputFile = addStringBeforeFinalDot(INPUT_FILE, "_RENAMED");
                write_string_to_file_full_path(phylogeny, outputFile);
                Pantools.logger.info("{} leaves were changed, new phylogeny written to: {}", total_changes, outputFile);
            } else {
                Pantools.logger.info("The provided phylogeny is incorrect. Only trees from one PanTools's phylogenetic methods can be used.");
            }
        } else {
            renameNexusFile(lines, exclude_numbers);
        }
    }

    /**
     * Update a nexus file to include phenotype (optionally remove genome number)
     * Example part of Nexus file. Part between '' is the label.
     * IMPORTANT the value within brackets [] is not a genome number!
     *
     * file1
     * #nexus
     *
     * BEGIN Taxa;
     * DIMENSIONS ntax=25;
     * TAXLABELS
     * [1] '1'
     * [2] '2'
     * [3] '3'
     * [4] '4'
     * [5] '5'
     * ;
     * END; [Taxa]
     *
     * file2
     * #nexus
     *
     * BEGIN Taxa;
     * DIMENSIONS ntax=12;
     * TAXLABELS
     * [1] '1_D._ananassae'
     * [2] '2_D._erecta'
     * [3] '3_D._grimshawi'
     * [4] '4_D._melanogaster'
     * [5] '5_D._mojavensis'
     * ;
     * END; [Taxa]
     */
    public void renameNexusFile(ArrayList<String> lines, boolean exclude_numbers) {
        boolean readLines = false;
        StringBuilder newNexus = new StringBuilder();
        for (String line : lines) {
            if (line.startsWith("TAXLABELS")) {
                readLines = true;
                newNexus.append(line).append("\n");
            } else if (readLines) {
                if (line.startsWith(";") || line.startsWith("END") ){
                    readLines = false;
                    newNexus.append(line).append("\n");
                } else {
                    String[] lineArray = line.split(" "); // example line: [1] '1_D._ananassae'
                    try {
                        String genomeNrStr2 = lineArray[1].replace("'","");
                        int genomeNr = Integer.parseInt(genomeNrStr2);
                        String newValue = create_phenotype_for_rename_nexus(genomeNr, exclude_numbers);
                        newNexus.append(lineArray[0]).append(" '").append(newValue).append("'\n");
                        continue;
                    } catch (NumberFormatException nfe) {
                        // file does not only contain a genome number. can still try the next option
                    }

                    String[] underscoreArray = lineArray[1].split("_");
                    try {
                        String genomeNrStr = underscoreArray[0].replace("'","");
                        int genomeNr = Integer.parseInt(genomeNrStr);
                        String newValue = create_phenotype_for_rename_nexus(genomeNr, exclude_numbers);
                        newNexus.append(lineArray[0]).append(" '").append(newValue).append("'\n");
                    } catch (NumberFormatException npe) {
                        Pantools.logger.error("Was unsuccessful in retrieving a genome number from this line: {}", line);
                        System.exit(1);
                    }
                }
            } else {
                newNexus.append(line).append("\n");
            }
        }
        String outFileName = addStringBeforeFinalDot(INPUT_FILE, "_RENAMED");
        write_SB_to_file_full_path(newNexus, outFileName);
        Pantools.logger.info("New phylogeny written to: {}", outFileName);
    }

    /**
     * Return a phenotype value of a specific genome as a string when a --phenotype is included
     * @param genomeNr a genome number
     * @return
     */
    public String create_phenotype_for_rename_nexus(int genomeNr, boolean exclude_numbers) {
        if (PHENOTYPE == null) {
            return genomeNr +"";
        }
        String phenotype = "";
        if (geno_pheno_map.containsKey(genomeNr)) {
            phenotype = geno_pheno_map.get(genomeNr);
        }
        if (phenotype.equals("?")) {
            phenotype = "Unknown";
        }
        if (exclude_numbers) {
            return phenotype;
        }
        return genomeNr + "_" + phenotype;
    }

    /**
     * tree.newick becomes tree*STRING*.newick
     *
     * @param originalFileName
     * @param add the string to be appended before the final .
     * @return
     */
    public static String addStringBeforeFinalDot(String originalFileName, String add) {
        StringBuilder newFileName = new StringBuilder();
        String[] file_array = originalFileName.split("\\."); // split on .
        for (int i = 0; i < file_array.length; i++) {
            newFileName.append(file_array[i]);
            if (i == file_array.length-2) { // add the new string before the final part is added
                newFileName.append(add);
            }
            if (i != file_array.length-1) {
                newFileName.append(".");
            }
        }
        return newFileName.toString();
    }

    /**
     Create an Rscript to reroot a phylogeny (using APE)
     */
    public void reroot_phylogeny() {
        Pantools.logger.info("Create an Rscript to reroot an phylogenetic tree.");
        if (WORKING_DIRECTORY == null) {
            WORKING_DIRECTORY = "";
        }
        check_if_node_exists_in_tree();
        create_reroot_script();
        Pantools.logger.info("Rscript written to: {}reroot.R", WORKING_DIRECTORY);
    }

    /**
     * Stops the program when the value specified by the user does not exist in the phylogeny
     */
    public void check_if_node_exists_in_tree() {
        // check if the required files were included
        boolean stop = false;
        if (INPUT_FILE == null){
            Pantools.logger.info("This function requires a newick file via --input-file/-if.");
            stop = true;
        }
        if (!check_if_file_exists(INPUT_FILE)) {
            Pantools.logger.info("{} does not exist.", INPUT_FILE);
            stop = true;
        }
        if (NODE_VALUE == null) {
            Pantools.logger.info("\nThis function requires the name of a terminal (external) node via --value.");
            stop = true;
        }
        if (stop) {
            System.exit(1);
        }

        String phylogeny = read_newick_file();
        // Check if node exists in the tree
        if (!phylogeny.contains(NODE_VALUE)) { // check if value is present at all
            Pantools.logger.error("{} was not found back in the provided phylogeny!.", NODE_VALUE);
            System.exit(1);
        }

        boolean correct = false;
        if (phylogeny.contains("(" + NODE_VALUE + ",")) {
            correct = true;
        } else if (phylogeny.contains("(" + NODE_VALUE + ":")) {
            correct = true;
        } else if (phylogeny.contains("," + NODE_VALUE + ")")) {
            correct = true;
        } else if (phylogeny.contains("," + NODE_VALUE + ":")) {
            correct = true;
        }

        if (!correct) { // not 100% sure if the 4 options cover all posibilities, therefore not stopping the program when not true
            Pantools.logger.info("{} was not found back in the provided phylogeny.", NODE_VALUE);
        } else {
            Pantools.logger.info("{} was successfully found back in the provided phylogeny.", NODE_VALUE);
        }
    }

    /**
     * Rscript that uses APE to reroot a phylogeny
     */
    public void create_reroot_script() {
        String R_LIB = check_r_libraries_environment();
        StringBuilder rscript = new StringBuilder();
        rscript.append("#! /usr/bin/env RScript\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"ape\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(ape)\n\n")
                .append("tree <- read.tree(file = \"").append(INPUT_FILE).append("\")\n")
                .append("rooted_tree <- ape::root(tree, \"").append(NODE_VALUE).append("\", resolve.root = TRUE)\n")
                .append("write.tree(rooted_tree, file=\"").append(INPUT_FILE).append("_REROOTED.newick\")\n")
                .append("cat(\"\\nRerooted tree written to: ").append(INPUT_FILE).append("_REROOTED.newick\\n\\n\")");
        write_SB_to_file_in_DB( rscript, "reroot.R");
    }

    /**
     * Recognizes newick or nexus tree file format. Return its content
     * @return 1 line = newick. multiple lines = nexus
     */
    public ArrayList<String> readNewickOrNexus(boolean print) {
        ArrayList<String> lines = new ArrayList<>();
        boolean beginTaxafound = false;
        boolean beginSplitsfound = false;
        int lineCounter = 0;
        if (INPUT_FILE == null) {
            Pantools.logger.error("Provide a phylogeny through --input-file/-if.");
            System.exit(1);
        }
        if (!INPUT_FILE.contains(".")) {
            Pantools.logger.error("The file provided through --input-file/-if must have a file extension, .newick for example.");
            System.exit(1);
        }

        try (BufferedReader in = new BufferedReader(new FileReader(INPUT_FILE))) {
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                lines.add(line); // add the regular line before uppercase
                line = line.toUpperCase();
                lineCounter++;
                if (line.startsWith("BEGIN TAXA;")) {
                    beginTaxafound = true;
                } else if (line.startsWith("BEGIN SPLITS;")) {
                    beginSplitsfound = true;
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", INPUT_FILE);
            System.exit(1);
        }

        if (lineCounter == 1) { // assume file is newick format, no file checks
            if (print) {
                Pantools.logger.info("Input file was recognized as newick.");
            }
        } else if (beginTaxafound && beginSplitsfound) {
            if (print) {
                Pantools.logger.info("Input file was recognized as nexus.");
            }
        } else {
            Pantools.logger.error("File could not be recognized as newick or nexus.");
            System.exit(1);
        }
        return lines;
    }

    /**
     * Read a newick file that was given by the --input-file argument
     * @return
     */
    public String read_newick_file() {
        if (INPUT_FILE == null) {
            Pantools.logger.error("Provide a phylogeny through --input-file/-if.");
            System.exit(1);
        }
        if (!INPUT_FILE.contains(".")) {
            Pantools.logger.error("The file provided through --input-file/-if must have a file extension, .newick for example.");
            System.exit(1);
        }
        String phylogeny = "";
        try (BufferedReader in = new BufferedReader(new FileReader(INPUT_FILE))) {
            int line_counter = 0;
            for (int c = 0; in.ready();) {
                phylogeny = in.readLine().trim();
                line_counter++;
            }
            if (line_counter > 1) {
                Pantools.logger.error("{} is not a (correctly formatted) Newick file.", INPUT_FILE);
                System.exit(1);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", INPUT_FILE);
            System.exit(1);
        }
        return phylogeny;
    }

    /**
     * Read a newick file that was given by input argument (Written by Dirk-Jan)
     * @param path_to_newick
     * @return
     */
    public String read_newick_file(Path path_to_newick) {
        String phylogeny = "";
        try (BufferedReader in = new BufferedReader(new FileReader(path_to_newick.toFile()))) {
            int line_counter = 0;
            while (in.ready()) {
                phylogeny = in.readLine().trim();
                line_counter++;
            }
            if (line_counter > 1) {
                Pantools.logger.error("{} is not a (correctly formatted) Newick file.", path_to_newick);
                System.exit(1);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", path_to_newick);
            System.exit(1);
        }
        return phylogeny;
    }

    /**
     * Return a phenotype value of a specific genome as a string when a --phenotype is included
     * @param genome_nr
     * @return
     */
    public String create_phenotype_for_rename_phylo(int genome_nr) {
        String phenotype = "";
        if (PHENOTYPE == null) {
            return phenotype;
        }
        if (geno_pheno_map.containsKey(genome_nr)) {
            phenotype = geno_pheno_map.get(genome_nr);
        }
        if (phenotype.equals("?")) {
            phenotype = "Unknown";
        }
        if (!phenotype.equals("") && Mode.equals("0")) {
            phenotype = "_" + phenotype;
        }
        return phenotype;
    }

    /**
     * Update skip_array with genome numbers that were skipped in the previous 'gene_classification' run
     */
    public void retrieve_skipped_genomes_during_glassification() {
        if (!check_if_file_exists(WORKING_DIRECTORY + "gene_classification/skipped_genomes.info")) {
            Pantools.logger.error("One of the required input file is missing. Please run 'gene_classification' again.");
            System.exit(1);
        }
        String missed_genome = "";
        try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "gene_classification/skipped_genomes.info"))) {
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.startsWith("None")) { // no genomes were skipped
                    // do nothing
                } else { // genomes were skipped
                    String[] line_array = line.split(",");
                    for (String genome_nr_str : line_array) {
                        int genome_nr = Integer.parseInt(genome_nr_str);
                        if (!skip_array[genome_nr-1]) {
                            skip_array[genome_nr-1] = true;
                            missed_genome += genome_nr_str + ",";
                        }
                    }
                }

                if (!missed_genome.equals("")) { // more genes were skipped in the previous gene classification run than were selected by --skip
                    missed_genome = missed_genome.replaceFirst(".$","");
                    String genome = "Genome";
                    if (missed_genome.contains(",")) {
                        genome += "s";
                    }
                    Pantools.logger.warn("Important! {} {} will be excluded from this analysis because it was skipped during gene_classification.", genome, missed_genome);
                }
            }
        } catch (IOException e) {
            Pantools.logger.error("Unable to read: {}gene_classification/skipped_genomes.info", WORKING_DIRECTORY);
            System.exit(1);
        }
    }

    /**
     * Does not create the phylogeny directly but only prepares the Rscript or IQ-tree command that creates it
     * @param hmGroups an array of the selected hmGroups (if null, only the single copy orthologs are used)
     * @param alignNucleotide whether to use nucleotide sequences
     * @param alignProtein whether to use protein sequences
     * @param alignVariants whether to use VCF information
     * @param pavs whether to use PAV information
     * @throws IOException if the single copy orthologs file cannot be read
     */
    public void core_snp_tree(List<Long> hmGroups, boolean alignNucleotide, boolean alignProtein, boolean alignVariants, boolean pavs) throws IOException {
        Pantools.logger.info("Inferring core phylogeny from SNPs or Amino acid substitutions identified in single-copy-orthologous groups.");
        check_database(); // starts up the graph database if needed
        FAST = true; // skip the fastTree phylogeny inference per homology group
        boolean useSingleCopyGroups = false;
        if (hmGroups == null) {
            Path singleCopyInputFile = Paths.get(WORKING_DIRECTORY + "gene_classification/single_copy_orthologs.csv");
            Pantools.logger.info("No homology groups provided. Using {} (was created during the previous 'gene_classification' run).", singleCopyInputFile);
            if (!singleCopyInputFile.toFile().exists()) {
                Pantools.logger.error("The file {} does not exist. Please run 'gene_classification' first.", singleCopyInputFile);
                System.exit(1);
            }
            hmGroups = Utils.parseHmFile(singleCopyInputFile);
            useSingleCopyGroups = true;
        }

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            check_current_grouping_version(); // check which version of homology grouping is active
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            stopWithLessThanGenomes(4);
            retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        Pantools.logger.info("Running MSA for all core groups.");
        String msaMethod = "per_group";
        MultipleSequenceAlignment msa = new MultipleSequenceAlignment(
                msaMethod,
                alignNucleotide,
                alignProtein,
                alignVariants,
                pavs,
                hmGroups
        );
        System.out.println();

        String alignmentTypeShort = msa.getAlignmentTypeShort();
        ArrayList<Node> hmNodeList = msa.getHmNodeList(); //get hmNodeList, this list is not updated by msa

        if (useSingleCopyGroups) { // genomes can only be validated when default SCO file is used.
            retrieve_skipped_genomes_during_glassification();
        }
        if (useSingleCopyGroups) {
            hmNodeList = checkSingleCopyGroups(hmNodeList);
        }

        String alignmentPhylogenyMode = checkModeCorePhylogeny();
        new File(WORKING_DIRECTORY + "core_snp_tree").mkdir(); // create directory
        Pantools.logger.info("Single copy orthology groups: {}.", hmNodeList.size());
        if (hmNodeList.isEmpty()) {
            Pantools.logger.error("No single copy orthologs. Run 'gene_classification'.");
            System.exit(1);
        }

        msa.alignSequences(true, false); //NB: I do not check for alignment_phylogeny_mode.contains("nucleotide") currently tho
        ArrayList<String> updatedMsaNames = msa.getMsaNames(); //get msaNames, since this list is updated with trimming results
        Path outDirMsa = msa.getOutDirMsa();

        match = FileUtils.loadScoringMatrix("BLOSUM" + BLOSUM);

        if (alignmentPhylogenyMode.contains("NJ")) { // --mode nj
            prepareNJCorePhylogenyResults(updatedMsaNames, outDirMsa, alignmentTypeShort);
        } else { // --mode ml
            prepareMLCorePhylogenyResults(updatedMsaNames, outDirMsa, alignmentTypeShort);
        }
    }

    /**
     * Verify if homology groups are single-copy
     * @param sco_node_list
     * @return
     */
    public static ArrayList<Node> checkSingleCopyGroups(ArrayList<Node> sco_node_list) {
        ArrayList<Node> updated_sco_node_list = new ArrayList<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (Node hm_node : sco_node_list) {
                int[] copy_number_array = (int[]) hm_node.getProperty("copy_number");
                boolean sco_group = true;
                for (int i = 1; i < copy_number_array.length; i++) { // walk over the cnv array in the homology node
                    if (skip_array[i - 1]) {
                        continue;
                    }
                    if (copy_number_array[i] != 1) { // group cannot be single copy ortholog
                        sco_group = false;
                    }
                }
                if (sco_group) {
                    updated_sco_node_list.add(hm_node);
                }
            }
            tx.success();
        }
        if (updated_sco_node_list.isEmpty()) {
            System.out.println("\rNone of the included homology groups are single copy");
        } else if (updated_sco_node_list.size() < sco_node_list.size()) {
            System.out.println("\rNot all included homology groups are single copy. Lowered the number of groups from " +  sco_node_list.size()
                    + " to " + updated_sco_node_list.size());
        }
        return updated_sco_node_list;
    }

    /**
     * Combine multiple temporary files into a single fasta file
     * @param variable_or_informative
     * @param path
     */
    private void concatenate_msa_inputs(String variable_or_informative, Path path) {
        Path tempDir = path.resolve("temp");
        Path outputFile = variable_or_informative.equals("_inf") ? path.resolve("informative.fasta") : path.resolve("variable.fasta");

        try (BufferedWriter out = Files.newBufferedWriter(outputFile)) {
            for (int i = 1; i <= total_genomes; i++) {
                if (skip_array[i-1]) {
                    continue;
                }

                Path inputFile = tempDir.resolve(i + variable_or_informative);
                String line = "";
                try (BufferedReader in = Files.newBufferedReader(inputFile)) {
                    while (in.ready()) {
                        line = in.readLine().trim();
                    }
                } catch (IOException e) {
                    Pantools.logger.error("Unable to read: {}", inputFile);
                    throw new RuntimeException("Unable to continue without " + inputFile);
                }

                String phenotype = get_phenotype_with_underscore(i);
                out.write(">" + i + phenotype + "\n" + line + "\n");
            }
        } catch (IOException ioe) {
            Pantools.logger.info("Unable to create: {}", outputFile);
        }
    }

    /**
     * Several functions with main two goals:
     * - to count the number of variable and informative positions
     * - create concatenated sequence per genome
     * @param inPath
     * @param tempPath
     * @param first_sequence // create or append the file
     * @param genome_order_list
     * @return
     */
    private int[] getSnpsFromMsaAndConcatenate(Path inPath, Path tempPath, boolean first_sequence, ArrayList<String> genome_order_list) {
        Pantools.logger.debug("inPath = {}", inPath);
        Pantools.logger.debug("tempPath = {}", tempPath);
        Pantools.logger.debug("first_sequence = {}", first_sequence);
        Pantools.logger.debug("genome_order_list = {}", genome_order_list);

        String[] sequence_array = new String[genome_order_list.size()]; // sequences are added in next function
        Pantools.logger.debug("sequence_array = {}", Arrays.toString(sequence_array));

        ArrayList<Integer> snp_positions = read_mafft_alignment(inPath, sequence_array, genome_order_list.size());
        Pantools.logger.debug("snp_positions = {}", snp_positions);

        String[][] all_seq_position_array = create_all_seq_position_array(sequence_array, genome_order_list);
        Pantools.logger.debug("all_seq_position_array = {}", Arrays.deepToString(all_seq_position_array));

        // this function also creates a concatenated sequence per genome
        int[] ignored_conserved_sites = retrieve_and_add_var_inf_sites_from_alignment(all_seq_position_array, first_sequence, genome_order_list, snp_positions, tempPath);
        Pantools.logger.debug("ignored_conserved_sites = {}", Arrays.toString(ignored_conserved_sites));

        return new int[] {snp_positions.size()-1-ignored_conserved_sites[1], snp_positions.size()-1-ignored_conserved_sites[0]-ignored_conserved_sites[1]};
    }

    /**
     * Retrieves variable and informative positions in an (already preprocessed) alignment
     * @param all_seq_position_array
     * @param first_sequence
     * @param genome_order_list
     * @param snp_positions
     * @param tempPath
     * @return
     */
    private int[] retrieve_and_add_var_inf_sites_from_alignment(String[][] all_seq_position_array, boolean first_sequence,
                                                                      ArrayList<String> genome_order_list, ArrayList<Integer> snp_positions,
                                                                     Path tempPath) {

        int ignore_positions = 0, conserved_positions = 0; // ignore pos are not parsimony informative positions, conserved pos are the none variable sites
        HashMap<String, StringBuilder> nuc_per_genome = new HashMap<>(); // key is genome nr for variable and genome nr + "_inf" for informative sites
        for (int l = 0; l < snp_positions.size()-1; l++) { // -1 because last number is the alignment length
            int snp_pos = snp_positions.get(l);
            HashMap<String, Integer> pos_count_map = new HashMap<>(); // used to determine if position is variable or informative
            for (int genome_nr = 1; genome_nr <= genome_order_list.size(); genome_nr++) {
                String genome_name = genome_order_list.get(genome_nr-1);
                try {
                    if (skip_array[Integer.parseInt(genome_name)-1]) {
                        continue;
                    }
                } catch (NumberFormatException ignored) {
                }
                String nuc = all_seq_position_array[genome_nr-1][snp_pos]; // -1 because data in array
                try_incr_hashmap(pos_count_map, nuc, 1);
            }
            if (pos_count_map.size() == 1) {
                conserved_positions++;
                continue;
            }
            boolean informative = check_if_informative_position(pos_count_map);
            if (!informative) {
                ignore_positions++;
            }

            for (int genome_nr = 1; genome_nr <= genome_order_list.size(); genome_nr++) {
                String genome_name = genome_order_list.get(genome_nr-1);
                try {
                    if (skip_array[Integer.parseInt(genome_name)-1]) {
                        continue;
                    }
                } catch (NumberFormatException ignored) {
                }
                String nucleotide = all_seq_position_array[genome_nr-1][snp_pos]; // -1 because data in array
                try_incr_SB_hashmap(nuc_per_genome, genome_name +"", nucleotide);
                if (informative) {
                    try_incr_SB_hashmap(nuc_per_genome, genome_name + "_inf", nucleotide);
                }
            }
        }

        for (String genome_key : nuc_per_genome.keySet()) {
            StringBuilder value_builder = nuc_per_genome.get(genome_key);
            if (first_sequence) {
                write_SB_to_file_full_path(value_builder, tempPath.resolve(genome_key));
            } else {
                append_SB_to_file_full_path(value_builder, tempPath.resolve(genome_key));
            }
        }
        return new int[]{ignore_positions, conserved_positions};
    }

    /**
     * Further split the MAFFT alignment in a two dimensional array[][]
     * @param sequence_array
     * @param genome_order_list
     * @return
     */
    public static String[][] create_all_seq_position_array(String[] sequence_array, ArrayList<String> genome_order_list) {
        String[][] all_seq_position_array = new String[genome_order_list.size()][0];
        int sequence_counter = 0;
        for (String sequence : sequence_array) {
            String genome_nr = genome_order_list.get(sequence_counter);
            try {
                if (skip_array[Integer.parseInt(genome_nr)-1]) {
                    continue;
                }
            } catch (NumberFormatException ignored) {
            }
            all_seq_position_array[sequence_counter] = sequence.split(""); // -1 because data in array
            sequence_counter++;
        }
        return all_seq_position_array;
    }

    /**
     * Collects several functions required for creating the core SNP (ML) tree after the alignments
     * @param sco_string_list list with 'homolog_group' node identifiers (formatted as strings)
     * @param outDirMsa path to output directory of msa
     */
    private void prepareMLCorePhylogenyResults(ArrayList<String> sco_string_list, Path outDirMsa, String alignmentTypeShort) {
        HashMap<String, String> groupInfoMap = gatherInfoCorePhylogenyLog(outDirMsa, sco_string_list);

        // define paths
        Path outputPath = Paths.get(WORKING_DIRECTORY).resolve("core_snp_tree"); // TODO rename to core_phylogeny
        Path tempPath = outputPath.resolve("temp");
        Path informativeFastaFile = outputPath.resolve("informative.fasta");
        Path variableFastaFile = outputPath.resolve("variable.fasta");
        Path sitesPerGroupFile = outputPath.resolve("sites_per_group.csv");
        Path iqtreeScriptFile = outputPath.resolve("iqtree.sh");

        int group_counter = 0, informative_sites = 0, variable_sites = 0;
        String gene_or_prot = "gene sizes()";
        if (alignmentTypeShort.equals("prot")) {
            gene_or_prot = "protein sizes";
        }
        StringBuilder gene_info = new StringBuilder("#Homology group node id;names;" + gene_or_prot +
                ";(max) trimmed start;(max) trimmed end;total seq;variable positions;informative positions\n");

        delete_directory(tempPath);
        tempPath.toFile().mkdir(); // create directory

        ArrayList<String> all_genome_order_list = new ArrayList<>();
        boolean first = true;
        for (String group_id : sco_string_list) {
            group_counter++;
            System.out.print("\r Reading alignments: " + group_counter + "/" + sco_string_list.size());

            Path genome_order_file = outDirMsa.resolve(group_id).resolve("input").resolve("genome_order.info");
            Path alignment_file = outDirMsa.resolve(group_id).resolve("output").resolve(alignmentTypeShort + "_trimmed.afa");

            ArrayList<String> genome_order_list = read_genome_order_file(genome_order_file, false);
            for (String genome_name : genome_order_list) {
                if (!all_genome_order_list.contains(genome_name)) {
                    all_genome_order_list.add(genome_name);
                }
            }

            int[] var_inf_sites = getSnpsFromMsaAndConcatenate(alignment_file, tempPath, first, genome_order_list);
            informative_sites += var_inf_sites[1];
            variable_sites += var_inf_sites[0];
            if (variable_sites > 0) {
                first = false;
            }

            String info = groupInfoMap.get(group_id);
            gene_info.append(info)
                     .append(genome_order_list.size()).append(";")
                     .append(var_inf_sites[0]).append(";")
                     .append(var_inf_sites[1]).append("\n"); // Total variable/informative positions:
        }
        write_SB_to_file_full_path(gene_info, sitesPerGroupFile);
        System.out.println("\rTotal variable sites   : " + variable_sites + "\n" +
                "Total informative sites: " + informative_sites);
        if (variable_sites == 0) {
            Pantools.logger.info("Unable to create a phylogeny as the {} genomes do not have any variation in the {} homology groups.", adj_total_genomes, sco_string_list.size());
            return;
        }
        concatenate_msa_inputs("", outputPath);

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}", sitesPerGroupFile);
        Pantools.logger.info(" {}", variableFastaFile);
        if (informative_sites > 0) {
            concatenate_msa_inputs("_inf", outputPath);
            Pantools.logger.info(" {}", informativeFastaFile);
            String iqtree_command = "iqtree -nt " + THREADS + " -s " + informativeFastaFile + " -redo -bb 1000";
            Pantools.logger.info("Run IQ-tree:");
            Pantools.logger.info(" {}", iqtree_command);
            Pantools.logger.info("When running IQ-tree, make sure there are no temp files present from a previous run:");
            Pantools.logger.info(" rm {}.*", informativeFastaFile);
            if (PHENOTYPE == null) {
                String rename_command = String.format("pantools rename_phylogeny -dp %s --phenotype PHENOTYPE -if %s", WORKING_DIRECTORY, outputPath);
                Pantools.logger.info("Since you did not provide a phenotype, the phylogeny will only contains genome numbers. When IQ-tree is done, run the following command to add phenotypes to the tree");
                Pantools.logger.info(" {}", rename_command);
                Pantools.logger.info(" {}", rename_command);
            }
            write_string_to_file_full_path(iqtree_command, iqtreeScriptFile);
            Pantools.logger.info("When using the core phylogeny in your research, please also cite:");
            Pantools.logger.info(" Minh, Bui Quang, et al. IQ-TREE 2: new models and efficient methods for phylogenetic inference in the genomic era.");
            Pantools.logger.info(" Molecular biology and evolution 37.5 (2020): 1530-1534.");
        } else {
            Pantools.logger.info("No parsimony informative sites were found in the input sequences. Unable to create a phylogeny.");
        }

        delete_directory(tempPath);
    }

    /**
     * Builds a consensus species phylogeny based on user specified homology groups (written by Dirk-Jan).
     * Old function name was phylogeny_using_groups()
     * @param hmGroups an array of homology groups
     * @param alignNucleotide whether to use nucleotide alignments
     * @param alignProtein whether to use protein alignments
     * @param alignVariants whether to use VCF information
     * @param pavs whether to use PAV information
     * NB: There is more recent version of ASTRAL-Pro available, both at https://github.com/chaoszhang/ASTER and at
     * https://github.com/chaoszhang/A-pro, but both of these do not accept polytomies. Our FastTree trees do
     * contain polytomies. So we will need to wait until they update ASTRAL-Pro to accept polytomies before updating to
     * a more recent version of their software.
     */
    public void consensus_tree(List<Long> hmGroups, boolean alignNucleotide, boolean alignProtein, boolean alignVariants, boolean pavs) {
        Pantools.logger.info("Building phylogeny using user-specified groups.");
        Pantools.logger.info("Using ASTRAL-PRO (https://github.com/chaoszhang/ASTER v1.3) for this.");
        Pantools.logger.info("Please cite the original authors:");
        Pantools.logger.info(" Chao Zhang, Celine Scornavacca, Erin K Molloy, Siavash Mirarab, ASTRAL-Pro: Quartet-Based Species-");
        Pantools.logger.info(" Tree Inference despite Paralogy, Molecular Biology and Evolution, Volume 37, Issue 11, November 2020, ");
        Pantools.logger.info(" Pages 3292–3307, https://doi.org/10.1093/molbev/msaa139");

        check_if_program_exists_stderr("astral-pro", 500, "ASTRAL-PRO", true);

        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenomeNode = GRAPH_DB.findNodes(PANGENOME_LABEL).next();

            check_if_panproteome(pangenomeNode); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if --skip/-ref is provided by user
            check_current_grouping_version(); // check which version of homology grouping is active

            if (grouping_version < 1) {
                Pantools.logger.error("No homology grouping is active.");
                System.exit(1);
            }

            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the pangenome database.");
            System.exit(1);
        }

        // Set file names and remove old files
        create_directory_in_DB("consensus_tree/grouping_v" + grouping_version);
        String all_gene_tree_file = WORKING_DIRECTORY + "consensus_tree/grouping_v" + grouping_version +
                "/all_trees.hmgroups.newick";
        String species_tree_file = WORKING_DIRECTORY + "consensus_tree/grouping_v" + grouping_version +
                "/consensus_tree.astral-pro.newick";
        delete_file_full_path(all_gene_tree_file);
        delete_file_full_path(species_tree_file);

        // Set MSA variables
        Path gene_tree_file;
        String trimming = "_trimmed";

        if (Mode.equals("0")) {
            Mode = "PROTEIN"; //set to protein if nothing chosen
        }

        MultipleSequenceAlignment msa = new MultipleSequenceAlignment(
                "per_group",
                alignNucleotide,
                alignProtein,
                alignVariants,
                pavs,
                hmGroups
        );

        //run MSA
        msa.alignSequences(true, false);
        ArrayList<Node> hmNodeList = msa.getHmNodeList();
        String alignmentTypeShort = msa.getAlignmentTypeShort();
        Path outDirMsa = msa.getOutDirMsa();

        // Concatenate all tree files into one large tree file
        String gene_tree;
        String gene_tree_reformatted;

        Pantools.logger.info("Reading in all alignments belonging to user-specified homology groups.");
        if (ALLOW_POLYTOMIES) {
            Pantools.logger.info("NB: Polytomies allowed for ASTRAL-PRO; accuracy of final tree not guaranteed.");
        }
        int group_counter = 0;
        int total_groups = hmNodeList.size();
        int total_trees = 0;
        for (Node hm_node : hmNodeList) {
            group_counter++;
            Pantools.logger.debug("Reading gene tree: {}/{}.", group_counter, total_groups);
            if (msa.isExcluded(hm_node.getId())) continue;
            gene_tree_file = outDirMsa.resolve(hm_node.getId() + "")
                    .resolve("output")
                    .resolve(alignmentTypeShort + trimming + ".newick");

            gene_tree = read_newick_file(gene_tree_file);

            Pantools.logger.debug("Gene tree {} has polytomies: {}", gene_tree_file, hasPolytomies(gene_tree));
            if (!hasPolytomies(gene_tree)) {
                gene_tree_reformatted = replace_gene_by_genome_in_newick(gene_tree);
                appendStringToFileFullPath(gene_tree_reformatted + "\n", all_gene_tree_file);
                total_trees++;
            } else if (ALLOW_POLYTOMIES) {
                gene_tree_reformatted = replace_gene_by_genome_in_newick(gene_tree);
                appendStringToFileFullPath(gene_tree_reformatted + "\n", all_gene_tree_file);
                total_trees++;
            }
        }

        // Check if there are any trees to run ASTRAL-PRO on
        if (total_trees == 0) {
            Pantools.logger.error("No suitable gene trees were found to run ASTRAL-PRO on; consider re-running with --polytomies.");
            System.exit(1);
        }

        // Run ASTRAL-pro
        String[] args;
        if (ALLOW_POLYTOMIES) {
            args = new String[]{"astral-pro", "-e", "1", "-o", species_tree_file, all_gene_tree_file};
        } else {
            args = new String[]{"astral-pro",  "-o", species_tree_file, all_gene_tree_file};
        }
        Pantools.logger.info("Running ASTRAL-PRO for all alignments. (Database is unlocked and can be used by other functions.)");
        Pantools.logger.debug("ASTRAL-PRO command: {}", String.join(" ", args));
        ExecCommand.ExecCommand(args);
        if (!check_if_file_exists(species_tree_file)) {
            Pantools.logger.error("ASTRAL-pro could not be run and output file was not created.");
            System.exit(1);
        }

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {} (all homology group trees)", all_gene_tree_file);
        Pantools.logger.info(" {} (ASTRAL-PRO consensus tree)", species_tree_file);
        Pantools.logger.info("NB: Please bear in mind that ASTRAL-PRO is not accurate for trees that contain polytomies; e are still working on this.");
    }

    /**
     * Replaces all gene labels in a gene tree by its genome number (Written by Dirk-Jan)
     * @param gene_tree newick format tree
     * @return tree in which genome number only is left
     */
    private String replace_gene_by_genome_in_newick(String gene_tree) {
        String gene_tree_reformatted = gene_tree;
        String genome;

        for (int genome_number = total_genomes; genome_number > 0; genome_number--) {
            genome = Integer.toString(genome_number);
            String regex = "(" + genome + "(\\|\\w+?)*)_.*?:";
            gene_tree_reformatted = gene_tree_reformatted.replaceAll(regex, "$1:");
        }

        return gene_tree_reformatted;
    }

    /**
     * Checks of a gene tree has polytomies
     * @param gene_tree newick format tree
     * @return boolean whether tree has polytomies
     */
    private boolean hasPolytomies(String gene_tree) {
        boolean output = false;
        String[] splitTree = gene_tree.split(",");
        for (int i = 1; i < splitTree.length - 1; i++) { //first and last part always contain brackets
            String part = splitTree[i];
            if (!part.contains("(") && !part.contains(")")) { //if any other part contains no bracket at all, it has at least one polytomy
                output = true;
                break;
            }
        }

        return output;
    }

    /**
     * Count shared SNPs in MAFFT alignments. Stores the counts in total_shared_snps_map
     * Is run in parallel.
     */
    private class retrieve_snps_NJ_core_snp_tree implements Runnable {
        HashMap<String, Long> total_shared_snps_map;
        HashMap<String, String> gene_info_map;
        StringBuilder sites_per_group_builder;
        Path outDirMsa;
        String alignmentTypeShort;

        private retrieve_snps_NJ_core_snp_tree(HashMap<String, Long> total_shared_snps_map, HashMap<String, String> gene_info_map,
                                              StringBuilder sites_per_group_builder, Path outDirMsa, String alignmentTypeShort) {

            this.total_shared_snps_map = total_shared_snps_map;
            this.gene_info_map = gene_info_map;
            this.sites_per_group_builder = sites_per_group_builder;
            this.outDirMsa = outDirMsa;
            this.alignmentTypeShort = alignmentTypeShort;
        }

        public void run() {
            while (!string_queue.isEmpty()) {
                String sco_group;
                try {
                    sco_group = string_queue.take();
                } catch (InterruptedException e) { // this will never happen
                    sco_group = "";
                    continue;
                }
                HashMap<String, Integer> shared_snps_map = new HashMap<>();//= create_shared_snps_map(true);
                System.out.print("\r Reading alignments: " + string_queue.size() + "  ");

                Path alignment = outDirMsa.resolve(sco_group).resolve("output").resolve(alignmentTypeShort + "_trimmed.afa");
                Path genome_order_file = outDirMsa.resolve(sco_group).resolve("input").resolve("genome_order.info");
                int[] var_inf_sites = count_var_inf_sites_in_msa(shared_snps_map, alignment, sco_group,
                        alignmentTypeShort, genome_order_file, ""); // variable, informative, conserved sites

                append_sites_per_group(sites_per_group_builder, var_inf_sites, gene_info_map, sco_group); // synchronized function
                atomic_counter1.addAndGet(var_inf_sites[0]);
                atomic_counter2.addAndGet(var_inf_sites[1]);
                increase_total_shared_snp_map(shared_snps_map, total_shared_snps_map, genome_order_file, ""); // synchronized function
                increase_total_shared_snp_map(shared_snps_map, total_shared_snps_map, genome_order_file, "#inf"); // synchronized function
            }
        }
    }

    /**
     * Increase the shared SNPs in total_shared_snps_map
     * Part of distance calculation for NJ tree
     * @param shared_snps_map
     * @param total_shared_snps_map
     * @param genome_order_file
     * @param informative
     */
    private synchronized void increase_total_shared_snp_map(HashMap<String, Integer> shared_snps_map,
                                                                  HashMap<String, Long> total_shared_snps_map,
                                                                  Path genome_order_file, String informative) {

        ArrayList<String> protein_id_list = get_full_protein_identifiers(genome_order_file, false);
        ArrayList<String> genome_order_list = read_genome_order_file(genome_order_file, false);
        for (int i=0; i < protein_id_list.size(); i++) {
            String genome_nr1 = genome_order_list.get(i);
            try {
                if (skip_array[Integer.parseInt(genome_nr1) - 1]) {
                    continue;
                }
            } catch (NumberFormatException ignored) {

            }

            for (int j=0; j < protein_id_list.size(); j++) {
                String genome_nr2 = genome_order_list.get(j);
                try {
                    if (skip_array[Integer.parseInt(genome_nr2) - 1]) {
                        continue;
                    }
                } catch (NumberFormatException ignored) {

                }
                if (genome_nr1.equals(genome_nr2) || genome_nr1.compareToIgnoreCase(genome_nr2) < 0) {
                    continue;
                }
                long count = 0;
                if (shared_snps_map.containsKey(i + "#" + j + informative)) {
                    count = shared_snps_map.get(i + "#" + j + informative);
                } else if (shared_snps_map.containsKey(j + "#" + i + informative)) {
                    count = shared_snps_map.get(j + "#" + i + informative);
                }
                try_incr_hashmap(total_shared_snps_map, genome_nr1 + "#" + genome_nr2 + informative, count); // using the GENOME NUMBERS here
            }
        }
    }

    /**
     * Synchronized function. Only 1 thread can access the map and append the stringbuilder
     * @param sites_per_group_builder
     * @param var_inf_sites
     * @param gene_info_map
     * @param sco_group
     */
    public static synchronized void append_sites_per_group(StringBuilder sites_per_group_builder, int[] var_inf_sites,
                                                           HashMap<String, String> gene_info_map, String sco_group) {

        String info = gene_info_map.get(sco_group);
        sites_per_group_builder.append(info).append(var_inf_sites[0]).append(";").append(var_inf_sites[1]).append("\n");
    }

    /**
     * Collects several functions required for creating the core SNP (NJ) tree after the alignments
     * @param sco_string_list list with 'homolog_group' node identifiers (formatted as strings)
     * @param outDirMsa
     * @param alignmentTypeShort
     */
    private void prepareNJCorePhylogenyResults(ArrayList<String> sco_string_list, Path outDirMsa, String alignmentTypeShort) {
        HashMap<String, String> groupInfoMap = gatherInfoCorePhylogenyLog(outDirMsa, sco_string_list);
        String output_path = WORKING_DIRECTORY + "core_snp_tree/"; // TODO rename to core_phylogeny
        HashMap<String, Long> total_shared_snps_map = new LinkedHashMap<>();
        StringBuilder sites_per_group_builder = new StringBuilder("#Homology group node id;gene names;gene sizes;(max) trimmed start;(max) trimmed end;"
                + "variable positions;informative positions\n");
        string_queue = new LinkedBlockingQueue<>();
        atomic_counter1 = new AtomicLong(0); // variable sites
        atomic_counter2 = new AtomicLong(0); // informative sites
        for (String group_id : sco_string_list) {
            string_queue.add(group_id);
        }

        try {
            ExecutorService es = Executors.newFixedThreadPool(THREADS);
            for(int i = 1; i <= THREADS; i++) {
                es.execute(new retrieve_snps_NJ_core_snp_tree(total_shared_snps_map, groupInfoMap, sites_per_group_builder, outDirMsa, alignmentTypeShort));
            }
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {

        }

        write_SB_to_file_full_path(sites_per_group_builder, output_path + "sites_per_group.csv");

        System.out.println("\rTotal variable sites   : " + atomic_counter1.get() + "\n" +
                "\rTotal informative sites: " + atomic_counter2.get());
        if (atomic_counter1.get() == 0) {
            Pantools.logger.info("Unable to create a phylogeny as the {} genomes do not have any variation in the {} homology groups.", adj_total_genomes, sco_string_list.size());
            return;
        }
        if (atomic_counter2.get() == 0) {
            Pantools.logger.info("No parsimony informative sites were found in the homology groups.");
        }

        create_shared_sites_from_total_shared_map(total_shared_snps_map, output_path, atomic_counter1.get(), alignmentTypeShort, "");
        create_shared_sites_from_total_shared_map(total_shared_snps_map, output_path, atomic_counter2.get(), alignmentTypeShort, "#inf");
        create_core_snp_nj_tree_rscript(alignmentTypeShort);

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}core_snp_NJ_tree.R", output_path);
        Pantools.logger.info(" {}sites_per_group.csv", output_path);
        Pantools.logger.info(" {}shared_variable_{}_positions.csv", output_path, alignmentTypeShort);
        if (atomic_counter2.get() > 0) {
            Pantools.logger.info(" {}shared_informative_{}_positions.csv", output_path, alignmentTypeShort);
        }
    }

    /**
     * Calculate the distance from the shared and total VARIABLE and INFORMATIVE sites
     *
     * Creates
     * - informative_nuc_distance.csv
     * - informative_prot_distance.csv
     *
     * - variable_nuc_distance.csv
     * - variable_prot_distance.csv
     *
     * - shared_informative_nuc_positions.csv
     * - shared_informative_prot_positions.csv
     *
     * - shared_variable_nuc_positions.csv
     * - shared_variable_prot_positions.csv
     *
     * @param shared_snps_map
     * @param output_path
     * @param total_sites
     * @param alignmentTypeShort
     * @param informative
     */
    private void create_shared_sites_from_total_shared_map(HashMap<String, Long> shared_snps_map, String output_path, long total_sites,
                                                                 String alignmentTypeShort, String informative) {

        if (total_sites == 0) { // variable or parsimony informative
            return;
        }
        DecimalFormat formatter = new DecimalFormat("0.0000000000");
        StringBuilder header = new StringBuilder("Genomes,");
        StringBuilder all_genes = new StringBuilder();
        StringBuilder gene_count_all = new StringBuilder();
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            String phenotype = get_phenotype_for_genome(i, true);
            all_genes.append(i).append(phenotype).append(",");
            gene_count_all.append(i).append(phenotype).append(",");
            header.append(i).append(phenotype);
            if (i != total_genomes) {
                header.append(",");
            }
            for (int j = 1; j <= total_genomes; j++) {
                if (skip_array[j-1]) {
                    continue;
                }
                String key = i + "#" + j;
                if (j < i) {
                    key = j + "#" + i;
                }
                long shared = total_sites;
                if (i != j) {
                    shared = shared_snps_map.get(key + informative); //total all, total distinct, shared all, shared distinct
                }
                double val = divide(shared, total_sites);
                String distance_str = formatter.format(1-val);
                if (distance_str.equals("0.0000000000") && shared != total_sites) { // these are not actually zero
                    distance_str = "0.0000000001";
                }
                all_genes.append(distance_str);
                gene_count_all.append(shared);
                if (j != total_genomes) {
                    all_genes.append(",");
                    gene_count_all.append(",");
                }
            }
            all_genes.append("\n");
            gene_count_all.append("\n");
        }
        String header_str = header.toString();
        header_str += "\n";
        String all_str = all_genes.toString();
        String count2 = gene_count_all.toString();
        if (informative.equals("#inf")) {
            write_string_to_file_full_path(header_str + all_str, output_path + "informative_" + alignmentTypeShort + "_distance.csv");
            write_string_to_file_full_path(header_str + count2, output_path + "shared_informative_" + alignmentTypeShort + "_positions.csv");
        } else {
            write_string_to_file_full_path(header_str + all_str, output_path + "variable_" + alignmentTypeShort + "_distance.csv");
            write_string_to_file_full_path(header_str + count2, output_path + "shared_variable_" + alignmentTypeShort + "_positions.csv");
        }
    }

    /**
     * Creates an Rscript to infer a core SNP (NJ) tree based on shared SNPs
     * @param alignmentTypeShort 'nuc', 'var or 'prot'
     */
    public static void create_core_snp_nj_tree_rscript(String alignmentTypeShort) {
        String R_LIB = check_r_libraries_environment();
        StringBuilder rscript = new StringBuilder();
        String input_path = WD_full_path + "core_snp_tree/";
        rscript.append("#! /usr/bin/env RScript\n")
                .append("\n")
                .append("# Select a distance based on variable or parsimony informative sites\n")
                .append("#  ").append(input_path).append("informative_").append(alignmentTypeShort).append("_distance.csv\n")
                .append("#  ").append(input_path).append("variable_").append(alignmentTypeShort).append("_distance.csv\n")
                .append("\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"ape\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(ape)\n")
                .append("\n")
                .append("input = read.csv(\"").append(input_path).append("informative_").append(alignmentTypeShort).append("_distance.csv\", sep=\",\", header = TRUE)\n")
                .append("df2 = subset(input, select = -c(Genomes))\n")
                .append("df.dist2 = as.matrix(df2, labels=TRUE)\n")
                .append("colnames(df.dist2) <- rownames(df.dist2) <- input[['Genomes']]\n")
                .append("NJ_tree <- nj(df.dist2)\n")
                .append("\n")
                .append("pdf(NULL)\n")
                .append("plot(NJ_tree, main = \"Neighbor Joining\")\n")
                .append("write.tree(NJ_tree, tree.names = TRUE, file=\"").append(input_path).append("_core_snp_NJ.tree\")\n")
                .append("cat(\"\\nSpecies tree written to: ").append(input_path).append("_core_snp_NJ.tree\\n\\n\")");
        write_SB_to_file_full_path(rscript, input_path + "core_snp_NJ_tree.R");
    }

    /**
     * Mode options nothing (0), protein, nucleotide, MJ, NJ
     * @return
     */
    public String checkModeCorePhylogeny() {
        String alignment_phylogeny_mode = "";
        if (Mode.equals("0")) { // no --clustering-method was provided
            if (PROTEOME) {
                System.out.println("\rNo --clustering-method selected. Preparing Maximum Likelihood phylogeny for protein sequences");
                alignment_phylogeny_mode = "ML,protein";
            } else { // pangenome
                System.out.println("\rNo --clustering-method selected. Preparing Maximum Likelihood phylogeny for nucleotide sequences");
                alignment_phylogeny_mode = "ML,nucleotide";
            }
        } else if (CLUSTERING_METHOD.equals("NJ")) {
            if (Mode.contains("PROTEIN") || PROTEOME) {
                System.out.println("\rPreparing Neighbor joining phylogeny for protein sequences");
                alignment_phylogeny_mode = "NJ,protein";
            } else {
                System.out.println("\rPreparing Neighbor joining phylogeny for nucleotide sequences");
                alignment_phylogeny_mode = "NJ,nucleotide";
            }
        } else if (CLUSTERING_METHOD.equals("ML") || Mode.contains("PROTEIN") || Mode.contains("NUCLEOTIDE")) {
            if (Mode.contains("PROTEIN") || PROTEOME) {
                System.out.println("\rPreparing Maximum likelihood phylogeny for protein sequences");
                alignment_phylogeny_mode = "ML,protein";
            } else {
                System.out.println("\rPreparing Maximum likelihood phylogeny for nucleotide sequences");
                alignment_phylogeny_mode = "ML,nucleotide";
            }
        } else {
            System.out.println("\rThe provided --clustering-method argument is not recognized");
            System.exit(1);
        }

        if (alignment_phylogeny_mode.contains("nucleotide")) {
            Mode = "BOTH"; // the initial alignment is done with both the protein and nucleotide sequences.
            //After the first alignment round, Mode is set to NUCLEOTIDE
        } else {
            Mode = "PROTEIN";
        }
        return alignment_phylogeny_mode;
    }

    /**
     * Collects some basic information about the single-copy genes of the core phylogeny.
     * Stores the information in a map.
     * @param outDirMsa output directory for the MSA
     * @param updatedNodeList list with all 'homology_group' names that were successfully aligned
     */
    private HashMap<String, String> gatherInfoCorePhylogenyLog(Path outDirMsa, ArrayList<String> updatedNodeList) {
        check_database();
        HashMap<String, String> info_map = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            int counter = 0;
            for (String hmNodeIdStr : updatedNodeList) {
                counter++;
                Node hm_node = GRAPH_DB.getNodeById( Long.parseLong(hmNodeIdStr));
                Iterable<Relationship> rels = hm_node.getRelationships();
                ArrayList<Integer> nuc_lengths = new ArrayList<>();
                HashSet<String> names = new HashSet<>();
                for (Relationship rel : rels) {
                    Node mrna_node = rel.getEndNode();
                    String name = "-";
                    if (!PROTEOME) {
                        name = retrieveNamePropertyAsString(mrna_node);
                    }
                    int genome_nr = (int) mrna_node.getProperty("genome");
                    if (skip_array[genome_nr-1]) {
                        continue;
                    }
                    if (name.equals("")) {
                        name = "-";
                    }
                    names.add(name);
                    String sequence;
                    if (Mode.startsWith("PROTEIN")) {
                        sequence = get_protein_sequence(mrna_node);
                    } else {
                        sequence = get_nucleotide_sequence(mrna_node);
                    }
                    nuc_lengths.add(sequence.length());
                }
                String length_freqs = determine_frequency_list_int(nuc_lengths);
                String start_end_removed = read_trimmed_info(outDirMsa, hm_node.getId());
                info_map.put(hmNodeIdStr,  hmNodeIdStr + ";" + names.toString().replace("[","").replace("]","") + ";" + length_freqs + ";" + start_end_removed + ";" );
            }
            tx.success(); // transaction successful, commit changes
        }
        return info_map;
    }

    /**
     * Obtain the number of trimmed nucleotides at start and end to use in the information file about the single-copy genes
     * @param outDirMsa output directory for the MSA
     * @param sco_group identifier of the single-copy homology group
     * @return
     */
    public static String read_trimmed_info(Path outDirMsa, long sco_group) {
        Path input = outDirMsa.resolve(sco_group + "")
                .resolve("input")
                .resolve("trimmed.info");
        String start_stop = "";
        try (BufferedReader in = new BufferedReader(new FileReader(input.toFile()))) {
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.startsWith("Longest start gap:")) {
                    String[] line_array = line.split(": ");
                    int positions = Integer.parseInt(line_array[1]);
                    start_stop += (positions*3) + ";";
                } else if (line.startsWith("Longest end gap:")) {
                    String[] line_array = line.split(": ");
                    int positions = Integer.parseInt(line_array[1]);
                    start_stop += (positions*3);
                }
            }
        } catch (IOException e) {
            Pantools.logger.error("Unable to read: {}", input);
            System.exit(1);
        }
        return start_stop;
    }

    /**
     * Obtain a phenotype value. Returns it with an underscore in front of the value.
     * @param genome a genome number
     */
    private String get_phenotype_with_underscore(int genome) {
        String add = "";
        if (PHENOTYPE != null) {
            String pheno = geno_pheno_map.get(genome);
            if (pheno.equals("?")) {
                pheno = "Unknown";
            }
            add += "_" + pheno;
        }
        return add;
    }
}

