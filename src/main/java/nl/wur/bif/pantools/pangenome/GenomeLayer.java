/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.wur.bif.pantools.pangenome;

import htsjdk.samtools.*;
import htsjdk.samtools.fastq.FastqReader;
import htsjdk.samtools.fastq.FastqRecord;
import nl.wur.bif.pantools.alignment.BoundedLocalSequenceAlignment;
import nl.wur.bif.pantools.alignment.LocalSequenceAlignment;
import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.utils.GraphUtils;
import nl.wur.bif.pantools.index.IndexDatabase;
import nl.wur.bif.pantools.index.IndexPointer;
import nl.wur.bif.pantools.index.IndexScanner;
import nl.wur.bif.pantools.index.kmer;
import nl.wur.bif.pantools.pangenome.parallel.LocalizeNodesParallel;
import nl.wur.bif.pantools.sequence.SequenceScanner;
import nl.wur.bif.pantools.utils.FileUtils;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;

import static nl.wur.bif.pantools.pangenome.Classification.genome_overview;
import static nl.wur.bif.pantools.pangenome.create_skip_arrays.create_skip_arrays;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.complement;
import static nl.wur.bif.pantools.utils.Utils.*;


/**
 * Implements all the functionalities related to the sequence layer of the pangenome
 * 
 * @author Siavash Sheikhizadeh, Eef Jonkheer, Bioinformatics group, Wageningen University, the Netherlands.
 */
public class GenomeLayer {
    private static final String NUCLEOTIDE_NODE_IDS_FILE_NAME = "nucleotide-node-ids.csv";
    private Node curr_node;
    private byte curr_side;
    private boolean finish;
    private AtomicLong[] num_shared_mapping;
    private AtomicLong[] num_unique_mapping;
    private AtomicLong[] num_unmapped;
    private AtomicLong number_of_reads;
    private AtomicLong number_of_alignments;
    private AtomicLong number_of_hits;
    private SequenceScanner genomeSc;
    public long highest_frequency = 0;
     
    /**
     * Implements a class for short sequencing reads
     */
    public class read {
        StringBuilder name;
        StringBuilder forward_seq;
        StringBuilder reverse_seq;
        //StringBuilder quality;
        
        public read() {
            name = new StringBuilder();
            forward_seq = new StringBuilder();
            reverse_seq = new StringBuilder();
            //quality = new StringBuilder();
        }
        
        public void clear() {
            name.setLength(0);
            forward_seq.setLength(0);
            reverse_seq.setLength(0);
            //quality.setLength(0);
        }
        
        public int length() {
            return forward_seq.length();
        }
    }
    
    /**
     * Implements a class for genomic hit of a single-layout read
     */
    public class single_hit {
        public int genome;
        public int sequence;
        public double identity;
        public int score;
        public int start;
        public int offset;
        public int length;
        public int deletions;
        public boolean forward;
        public String cigar;
        public String reference;
        public single_hit(int gn, int sq, double idn, int sc, int st, int off, int len, int del, boolean fw, String cg, String r) {
            genome = gn;
            sequence = sq;
            identity = idn;
            score = sc;
            start = st;
            offset = off;
            length = len;
            deletions = del;
            forward = fw;
            cigar = cg;
            reference = r;
        }
        
        public single_hit(single_hit h) {
            genome = h.genome;
            sequence = h.sequence;
            identity = h.identity;
            score = h.score;
            start = h.start;
            offset = h.offset;
            length = h.length;
            deletions = h.deletions;
            forward = h.forward;
            cigar = h.cigar;
            reference = h.reference;
        }
        
        public single_hit() {
            
        }
        

        @Override
        public String toString() {
            return "(genome:" + genome + 
                    ",sequence:" + sequence + 
                    ",identity:" + identity + 
                    ",score:" + score + 
                    ",start:" + start + 
                    ",offset:" + offset + 
                    ",length:" + length + 
                    ",deletions:" + deletions + 
                    ",forward:" + forward + 
                    ",reference:" + reference + 
                    ",cigar:" + cigar +")";
        }
    }

    /**
     * Implements a class for genomic hit of a paired_end-layout read
     */
    public class paired_hit {
        public int fragment_length;
        public single_hit h1;
        public single_hit h2;
        public paired_hit(int flen, int gn1, int sq1, double idn1, int sc1, int st1, int off1, int len1, int del1, boolean fw1, String cg1, String r1,
                          int gn2, int sq2, double idn2, int sc2, int st2, int off2, int len2, int del2, boolean fw2, String cg2, String r2) {
            fragment_length = flen;
            h1.genome = gn1;
            h1.sequence = sq1;
            h1.identity = idn1;
            h1.score = sc1;
            h1.start = st1;
            h1.offset = off1;
            h1.length = len1;
            h1.deletions = del1;
            h1.forward = fw1;
            h1.cigar = cg1;
            h1.reference = r1;

            h2.genome = gn2;
            h2.sequence = sq2;
            h2.identity = idn2;
            h2.score = sc2;
            h2.start = st2;
            h2.offset = off2;
            h2.length = len2;
            h2.deletions = del2;
            h2.forward = fw2;
            h2.cigar = cg2;
            h2.reference = r2;
        }

        public paired_hit(int f_len, single_hit hit1, single_hit hit2) {
            fragment_length = f_len;
            h1 = hit1;
            h2 = hit2;
        }

        public int get_score() {
            return h1.score + h2.score;
        }

        public int get_min_start() {
            return Math.min(h1.start, h2.start);
        }

        public int get_max_start() {
            return Math.max(h1.start, h2.start);
        }

        @Override
        public String toString() {
            return "(genome1:" + h1.genome + 
                    ",sequence1:" + h1.sequence + 
                    ",identity1:" + h1.identity + 
                    ",score1:" + h1.score + 
                    ",start1:" + h1.start + 
                    ",offset1:" + h1.offset + 
                    ",length1:" + h1.length + 
                    ",deletions1:" + h1.deletions + 
                    ",forward1:" + h1.forward + 
                    ",reference1:" + h1.reference + 
                    ",cigar1:" + h1.cigar +")" +
                    "\n" +
                    "(genome2:" + h2.genome + 
                    ",sequence2:" + h2.sequence + 
                    ",identity2:" + h2.identity + 
                    ",score2:" + h2.score + 
                    ",start2:" + h2.start + 
                    ",offset2:" + h2.offset + 
                    ",length2:" + h2.length + 
                    ",deletions2:" + h2.deletions + 
                    ",forward2:" + h2.forward + 
                    ",reference2:" + h2.reference + 
                    ",cigar2:" + h2.cigar +")";
        }
    }

    /**
     * Implements a comparator for single genomic hits
     */
    public static class single_hitComparator implements Comparator<single_hit> {
        @Override
        public int compare(single_hit x, single_hit y) {
            if (x.score > y.score) 
                return -1;
            else if (x.score < y.score) 
                return 1;
            else if (x.sequence > y.sequence) 
                return -1;
            else if (x.sequence < y.sequence) 
                return 1;
            else if (x.start > y.start) 
                return -1;
            else if (x.start < y.start) 
                return 1;
            else 
                return 0;
        }
    }      

    /**
     * Implements a comparator for paired-end genomic hits
     */
    public static class paired_hitComparator implements Comparator<paired_hit> {
        @Override
        public int compare(paired_hit x, paired_hit y) {
            if (x.get_score() > y.get_score()) 
                return -1;
            else if (x.get_score() < y.get_score()) 
                return 1;
            else if (x.fragment_length < y.fragment_length) 
                return -1;
            else if (x.fragment_length > y.fragment_length) 
                return 1;
            else 
                return 0;
        }
    }      

    /**
     * Implements a comparator for integer arrays of size 2
     */
    public static class IntPairComparator implements Comparator<int[]> {
        @Override
        public int compare(int[] x, int[] y) {
            if (x[0] > y[0]) 
                return -1;
            else if (x[0] < y[0]) 
                return 1;
            else if (x[1] < y[1]) 
                return -1;
            else if (x[1] > y[1]) 
                return 1;
            else
                return 0;
        }
    }    

    /**
     * Implements a comparator for integers
     */
    public static class IntComparator implements Comparator<Integer> {
        @Override
        public int compare(Integer v1, Integer v2) {
            return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
        }
    }
  
    /**
     * Implements read mapping functionality
     */
    public class Map implements Runnable {
        IndexScanner indexSc;
        SequenceScanner genomeSc;
        int thread_id;
        int K;
        FastqRecord[] fastq_record;
        ArrayList<int[]>[][]locations;
        IntComparator intcomp = new IntComparator();
        IndexPointer pointer;
        StringBuilder reference;
        BoundedLocalSequenceAlignment bounded_aligner;
        LocalSequenceAlignment aligner;
        LinkedList<int[]> node_results;
        PriorityQueue<single_hit>[][] hits;
        LinkedList<single_hit>[] alignments;
        PriorityQueue<single_hit> single_hits;
        Queue<single_hit> single_hits_2;
        PriorityQueue<paired_hit> paired_hits;
        Queue<paired_hit> paired_hits_2;
        boolean paired_end;
        IntPairComparator int_pair_comp;
        ArrayList<int[]> hit_counts;
        int num_neighbors = 0;
        int num_kmers = 0;
        int num_found = 0;
        int[] address = new int[3];
        ArrayList<Integer> genome_numbers;
        SAMFileWriter[] sam_writers;
        long[] genome_sizes;
        long total_genomes_size = 0;
        int[] shared;
        int[] unique;
        int[] unmapped;
        int total_unique = 1;
        int num_genomes;
        int[] num_sequences;
        long[][] sequence_length;
        String[][] sequence_titles;
        Random rand;
        double[] raw_abundance;
        paired_hitComparator phc = new paired_hitComparator();
        single_hitComparator shc = new single_hitComparator();
        //StringBuilder[] forward_read;
        StringBuilder[] reverse_read;
        //StringBuilder[] quality;
        kmer current_kmer; 
        //int[] read_len;
        String[] read_name;
        int num_hits = 0;
        int num_alns = 0;
        single_hit alignment_result;
        HashSet<Integer>[] unmapped_genomes;
        single_hit[] best_hit;
        int num_segments;
        FastqReader[] reader;
        
        /**
         * Initialized the read mapping object
         * @param id ID of the working thread
         * @param gn List of genomes to map reads against
         * @param paired Determines if reads are in pair
         * @param sams Array of SAM file writer objects
         * @param r
         */
        public Map(int id, ArrayList<Integer> gn, boolean paired, SAMFileWriter[] sams, FastqReader[] r) {
            int i, j, genome, abun;
            indexSc = new IndexScanner(INDEX_DB);
            K = indexSc.get_K();
            genomeSc = new SequenceScanner(GENOME_DB, 1, 1, K, indexSc.get_pre_len());
            current_kmer = new kmer(indexSc.get_K(), indexSc.get_pre_len());
            num_genomes = GENOME_DB.num_genomes;
            unmapped_genomes = new HashSet[2];
            num_sequences = new int[num_genomes + 1];
            sequence_length = new long[num_genomes + 1][];
            sequence_titles = new String[num_genomes + 1][];
            sam_writers = sams;
            thread_id = id;
            genome_numbers = gn;
            genome_numbers.sort(intcomp);
            paired_end = paired; 
            num_segments = paired_end ? 2 : 1;
            alignment_result = new single_hit();
            node_results = new LinkedList();
            hits = new PriorityQueue[2][];
            alignments = new LinkedList[2];
            locations = new ArrayList[2][];
            fastq_record = new FastqRecord[2];
            reverse_read = new StringBuilder[2];
            read_name = new String[2];
            best_hit = new single_hit[2];
            reader = r;
            reverse_read[0] = new StringBuilder();
            hits[0] = new PriorityQueue[num_genomes + 1];
            alignments[0] = new LinkedList();
            single_hits = new PriorityQueue(shc);
            single_hits_2 = new LinkedList();
            paired_hits = new PriorityQueue(phc);
            paired_hits_2 = new LinkedList();
            locations[0] = new ArrayList[num_genomes + 1];
            unmapped_genomes[0] = new HashSet();
            genome_sizes = new long[num_genomes + 1];
            shared = new int[num_genomes + 1];
            unique = new int[num_genomes + 1];
            unmapped = new int[num_genomes + 2]; // is one larger for unmapped read count in case of -am < 0
            raw_abundance = new double[num_genomes + 1];
            for (i = 0; i <= num_genomes; ++ i) {
                hits[0][i] = null;
                locations[0][i] = null;
                genome_sizes[i] = GENOME_DB.genome_length[i];
                total_genomes_size += genome_sizes[i];
                num_sequences[i] = GENOME_DB.num_sequences[i];
                sequence_length[i] = new long[num_sequences[i] + 1];
                sequence_titles[i] = new String[num_sequences[i] + 1];
                shared[i] = 0;
            }
            for (i = 0; i < genome_numbers.size(); ++i) {
                genome = genome_numbers.get(i);
                hits[0][genome] = new PriorityQueue(shc);
                locations[0][genome] = new ArrayList();
                for (j = 1; j <= num_sequences[genome]; ++j) {
                    sequence_length[genome][j] = GENOME_DB.sequence_length[genome][j];
                    sequence_titles[genome][j] = GENOME_DB.sequence_titles[genome][j];
                }
            }
            if (paired_end) {
                //quality[1] = new StringBuilder();
                //forward_read[1] = new StringBuilder();
                reverse_read[1] = new StringBuilder();
                hits[1] = new PriorityQueue[num_genomes + 1];
                alignments[1] = new LinkedList();
                locations[1] = new ArrayList[num_genomes + 1];
                unmapped_genomes[1] = new HashSet();
                for (i = 0; i <= num_genomes; ++ i) {
                    hits[1][i] = null;
                    locations[1][i] = null;
                }
                for (i = 0; i < genome_numbers.size(); ++i) {
                    genome = genome_numbers.get(i);
                    hits[1][genome] = new PriorityQueue(shc);
                    locations[1][genome] = new ArrayList();
                }
            }
            pointer = new IndexPointer();
            reference = new StringBuilder();
            bounded_aligner = new BoundedLocalSequenceAlignment(GAP_OPEN, GAP_EXT, MAX_ALIGNMENT_LENGTH, ALIGNMENT_BOUND, CLIPPING_STRINGENCY, "NUC.4.4");
            aligner = new LocalSequenceAlignment(GAP_OPEN, GAP_EXT, MAX_ALIGNMENT_LENGTH, CLIPPING_STRINGENCY, "NUC.4.4");
            int_pair_comp = new IntPairComparator();
            hit_counts = new ArrayList();
            rand = new Random(1);
            if (RAW_ABUNDANCE_FILE.equals("")) {
                for (i = 1; i <= num_genomes; ++i) {
                    raw_abundance[i] = 1.0;
                }
            } else {
                try {
                    String line;
                    String[] fields;
                    BufferedReader in = new BufferedReader(new FileReader(RAW_ABUNDANCE_FILE)); // file is provided via -raf 
                    while ((line = in.readLine()) != null) {
                        line = line.trim();
                        if (line.equals("") || line.startsWith("Genome") || line.startsWith("Unmapped")) {
                            continue;
                        }
                        fields = line.split("\\s+");
                        abun = Integer.parseInt(fields[2]);
                        if (abun <= 0)
                            abun = 1;
                        raw_abundance[Integer.parseInt(fields[0])] = abun;
                    }
                    in.close();
                } catch (IOException ex) {
                    Pantools.logger.info(ex.getMessage());
                }
            }
        }

        /**
         * Takes reads from a blocking queue and maps it to the genomes
         */
        @Override
        public void run() {
            int i, mate, genome, counter = 0;
            Iterator<Integer> itr;
            String read_string = null;
            try (Transaction tx = GRAPH_DB.beginTx()) {
                while (get_read()) {
                    for (mate = 0; mate < num_segments; ++mate) {
                        read_string = fastq_record[mate].getReadString();
                        reverse_complement(reverse_read[mate], read_string);
                        read_name[mate] = getBaseId(fastq_record[mate].getReadName());
                        find_locations(mate, read_string);
                        itr = genome_numbers.iterator();
                        while (itr.hasNext()) {
                            genome = itr.next();
                            cluster_and_examine_locations(mate, genome, 0, read_string);
                        }
                    }
                    report_hits();
                    ++counter;
                    if (counter % 1000 == 0) {
                        System.out.print("\rProcessed " + number_of_reads.getAndAdd(counter) + " reads  ");
                        counter = 0;
                    }
                }
                tx.success();
            }
  
            number_of_reads.getAndAdd(counter);
            number_of_hits.getAndAdd(num_hits);
            number_of_alignments.getAndAdd(num_alns); 
            for (i = 0; i < genome_numbers.size(); ++i) {
                genome = genome_numbers.get(i);
                num_shared_mapping[genome].getAndAdd(shared[genome]);
                num_unique_mapping[genome].getAndAdd(unique[genome]);
                num_unmapped[genome].getAndAdd(unmapped[genome]);
            }
            if (ALIGNMENT_MODE < 0) { // when competitive mapping
                num_unmapped[adj_total_genomes+1].getAndAdd(unmapped[adj_total_genomes+1]);
            }
        }
        
         /**
         * Kmerizes the read and collects all the candidate locations read may map in each genome
         * 
         * @param mate The number of segment in the read (can be 0 for single, 0/1 for paired-end) 
         * @param read_string
         */
        public void find_locations(int mate, String read_string) {
            Node node = null;
            int i, step, pos, prev_pos = 0, prev_offset = 0, loc_diff, read_len = read_string.length();
            int[] result;
            ListIterator<int[]> itr;
            long cur_index, prev_node_id;
            prev_node_id = -1l;
            step = read_len / NUM_KMER_SAMPLES;
            step = (step == 0 ? 1 : step);
            initialize_kmer(read_string);
            for (pos = K; pos < read_len;) {
                cur_index = indexSc.find(current_kmer);
                try{
                    if (cur_index != -1l) {
                        indexSc.get_pointer(pointer, cur_index);
                        if (prev_node_id != pointer.node_id) { // to save a bit of time
                            node = GRAPH_DB.getNodeById(pointer.node_id);
                            prev_node_id = pointer.node_id;
                            prev_pos = pos - K;
                            prev_offset = pointer.offset;
                            node_results.clear();
                            explore_node(node, mate, pos - K, read_len);
                            for (itr = node_results.listIterator(); itr.hasNext();) {
                                result = itr.next();
                                locations[mate][result[0]].add(new int[]{result[1], result[2]});
                            }
                        } else {
                            for (itr = node_results.listIterator(); itr.hasNext();) {
                                result = itr.next();
                                loc_diff = 0;
                                if (result[2] < 0) {
                                    loc_diff += (pos - K - prev_pos);
                                } else {
                                    loc_diff -= (pos - K - prev_pos);
                                }
                                if (result[3] < 0) {
                                    loc_diff -= (pointer.offset - prev_offset);
                                } else {
                                    loc_diff += (pointer.offset - prev_offset);
                                }
                                locations[mate][result[0]].add(new int[]{result[1], result[2] + loc_diff});
                            }
                        }
                    }
                    if (pos + step >= read_len) {
                        break;
                    }
                    for (i = 0; i < step; ++i, ++pos) {
                        current_kmer.next_fwd_kmer(binary[read_string.charAt(pos)] & 3);
                    }
                } catch (NotFoundException|ClassCastException ex) {
                    //num_exceptions++;
                    //Pantools.logger.info(ex.getMessage());
                } 
                //Pantools.logger.info(current_kmer.toString());
            }
        }
        
        public boolean get_read() {
            synchronized(reader) {
                int file_nr = 0;
                try {
                    if (reader[0].hasNext()) {
                        fastq_record[0] = reader[0].next();
                        if (paired_end) {
                            if (INTERLEAVED) {
                                fastq_record[1] = reader[0].next();
                            } else {
                                file_nr = 1;
                                fastq_record[1] = reader[1].next();
                                
                            }
                        }
                        return true;
                    } else {
                        return false;
                    }
               } catch (SAMException SAMexc) { // error given by fastqreader 
                    Pantools.logger.info("\nError with sample {}. Read starting on line {}. Stopping now.", (file_nr+1), reader[file_nr].getLineNumber()); 
                    return false;
                } catch (NoSuchElementException nse) { // file 1 is longer than file 2 
                    System.out.println("\rWARNING! File 1 has more reads as file 2!"); 
                    return false;
                }
            }
        }
        
        /**
         * Removes the /1 or /2 or spaces from the end of the read ID
         * @param Id
         * @return The base of the read ID
         */
        public String getBaseId(String Id) {
            int slashIdx = Id.indexOf("/");
            int spaceIdx = Id.indexOf(" ");

            if ((slashIdx == -1) && (spaceIdx == -1)) {
                return Id;
            }

            int idx = -1;
            if (slashIdx == -1) {
                idx = spaceIdx;
            } else if (spaceIdx == -1) {
                idx = slashIdx;
            } else {
                idx = spaceIdx < slashIdx ? spaceIdx : slashIdx;
            }

            return Id.substring(0, idx);
        }        
        
        /**
         * Takes the first k-mer of the read
         * 
         * @param read The read to be kmerized
         */
        public void initialize_kmer(String read) {
            int i;
            current_kmer.reset();
            for (i = 0; i < K; ++i) {
                current_kmer.next_fwd_kmer(binary[read.charAt(i)] & 3);
            }
        }

        /**
         * Explores all the incoming edges to a node and collects candidate genomic locations read may map in each genome
         * 
         * @param node A node of gDBG
         * @param mate The number of segment in the read (can be 0 for single, 0/1 for paired-end) 
         * @param position Offset of the sampled kmer in the read
         * @param read_len 
         */
        
        public void explore_node(Node node, int mate, int position, int read_len) {
            int i, loc, offset, node_len;
            long seq_len;
            char side;
            int[] location_array;
            int genome, sequence;
            boolean is_canonical;
            long frequency;
            offset = pointer.offset;
            is_canonical = current_kmer.get_canonical();
            frequency = (long) node.getProperty("frequency");
            if (frequency <= (int)(total_genomes_size / 10000000.0 + num_genomes * 5 * Math.log(total_genomes_size))) {// Filter out high frequent nodes
                node_len = (int) node.getProperty("length");
            // for each incoming edge to the node of the anchor
                for (Relationship r: node.getRelationships(Direction.INCOMING, RelTypes.FF, RelTypes.FR, RelTypes.RF, RelTypes.RR)) {
                    //num_neighbors++;
                    side = r.getType().name().charAt(1);
                // for all seuences passing that node
                    for (String seq_id: r.getPropertyKeys()) {
                        extract_address(address, seq_id);
                        genome = address[0];
                        if (locations[mate][genome] != null) {// should map against this genome
                            sequence = address[1];
                        // calculate the locations based on the offsets in the node
                            location_array = (int[])r.getProperty(seq_id);
                            seq_len = sequence_length[genome][sequence];
                            if (side == 'F') {
                                for (i = 0; i < location_array.length; i += 1) {
                                    if (pointer.canonical ^ is_canonical) {
                                        loc = location_array[i] + offset - read_len + position + K;
                                        if (loc >= 0 && loc <= seq_len - read_len) {
                                            node_results.add(new int[]{genome, sequence, -(1 + loc), 1});
                                        }
                                        Pantools.logger.trace("F-{}.", loc);
                                    } else {
                                        loc = location_array[i] + offset - position;
                                        if (loc >= 0 && loc <= seq_len - read_len) {
                                            node_results.add(new int[]{genome, sequence, loc, 1});
                                        }
                                        Pantools.logger.trace("F+{}.", loc);
                                    }
                                }
                            }else{
                                for (i = 0; i < location_array.length; i += 1) {
                                    if (pointer.canonical ^ is_canonical) {
                                        loc = location_array[i] + node_len - K - offset - position;
                                        if (loc >= 0 && loc <= seq_len - read_len) {
                                            node_results.add(new int[]{genome, sequence, loc, -1});
                                        }
                                        Pantools.logger.trace("R+{}.", loc);
                                    } else {
                                        loc = location_array[i] + node_len - offset - read_len + position;
                                        if (loc >= 0 && loc <= seq_len - read_len) {
                                            node_results.add(new int[]{genome, sequence, -(1 + loc), -1});
                                        }
                                        Pantools.logger.trace("R-{}.", loc);
                                    }
                                }
                            }
                        }
                    }
                } 
            }
        }
           
        /**
         * Extracts a genomic address array from a property string. For example array {1, 3} from property G1S3 
         * @param address
         * @param property
         */
        public void extract_address(int[] address, String property) {
            int i;
            char ch;
            address[0] = 0;
            address[1] = 0;
            for (i = 1; i < property.length(); ++i)
                if ((ch = property.charAt(i)) != 'S')
                    address[0] = address[0] * 10 + ch - 48;
                else
                    break;
            for (++i; i < property.length(); ++i)
                address[1] = address[1] * 10 + property.charAt(i) - 48;
        }

        /**
         * Clusters all the candidate genomic locations based on their proximity and align the read to the candidate locations
         * 
         * @param mate The number of segment in the read (can be 0 for single, 0/1 for paired-end) 
         * @param genome The number of genome read is being mapped against
         * @param sholder The maximum distance between two neighboring candidate locations in clusters
         * @param read_string
         */
        public void cluster_and_examine_locations(int mate, int genome, int sholder, String read_string) {
            int sequence, prev_sequence, prev_start;
            int[] intpair;
            int start, j, k, n, m, count;
            if (locations[mate][genome].size() > 0) {
                locations[mate][genome].sort(int_pair_comp);
                n = locations[mate][genome].size();
                for (j = 0; j < n;) {
                    intpair = locations[mate][genome].get(j);
                    prev_sequence = sequence = intpair[0];
                    prev_start = intpair[1];
                    for (count = 0; j < n; ++j) {
                        intpair = locations[mate][genome].get(j);
                        sequence = intpair[0];
                        start = intpair[1];
                        if (sequence == prev_sequence) {
                            if (start - prev_start > sholder) { 
                                hit_counts.add(new int[]{count, prev_start});
                                count = 1;
                                prev_start = start;
                            } else {
                                ++count;
                            }
                        } else {
                            break;
                        }
                        Pantools.logger.trace("{}_{}.", sequence, start);
                    }
                    Pantools.logger.trace("count: {}.", count);
                    hit_counts.add(new int[]{count, prev_start});
                    hit_counts.sort(int_pair_comp);
                    m = Math.min(hit_counts.size(), MAX_NUM_LOCATIONS);
                    for (k = 0; k < m; ++k) {
                        Pantools.logger.trace("{} {}.", hit_counts.get(k)[0], hit_counts.get(k)[1]);
                        if (sholder == 0) {
                            examine_location(mate, genome, prev_sequence, hit_counts.get(k)[1], read_string);
                        } else {
                            exhaustive_examine_location(mate, genome, prev_sequence, hit_counts.get(k)[1], read_string);
                        }
                        if (hit_counts.get(k)[0] == 1 || (k + 1 < m && hit_counts.get(k)[0] - hit_counts.get(k + 1)[0] > hit_counts.get(k)[0] / 2.0)) {
                            break;
                        }
                    }
                    hit_counts.clear();
                }
                locations[mate][genome].clear();
            }
        }
        
        /**
         * Aligns the read to the candidate location of each cluster
         * 
         * @param mate The number of segment in the read (can be 0 for single, 0/1 for paired-end) 
         * @param genome The number of genome read is being mapped against
         * @param sequence The number of sequence read is being mapped against
         * @param ref_start The candidate location in the genome
         * @param read_string
         */
        public void examine_location(int mate, int genome, int sequence, int ref_start, String read_string) {
            boolean forward = ref_start >= 0;
            boolean banded_alignment;
            single_hit h;
            int start, stop, read_len = read_string.length();
            if (!forward) {
                ref_start = -ref_start - 1;
            }
            start = ref_start - ALIGNMENT_BOUND;
            stop = start + read_len + 2 * ALIGNMENT_BOUND - 1;
            if (start >= 0 && stop <= sequence_length[genome][sequence] - 1) {
                banded_alignment = true;
            } else if (ref_start >= 0 && ref_start + read_len <= sequence_length[genome][sequence]) {
                start = ref_start;
                stop = start + read_len - 1;
                banded_alignment = false;
            } else {
                return;
            }
            num_hits++;
            reference.setLength(0);
            genomeSc.get_sub_sequence(reference, genome, sequence, start, stop - start + 1, true);
            if (alignments[mate].size() < 2 * ALIGNMENT_BOUND * read_len && 
                find_similar_subject(mate, genome, sequence, start, forward)) {
                if (valid_hit(read_len)) {
                    hits[mate][genome].offer(new single_hit(alignment_result));
                }
            } else {
                num_alns++;
                perform_alignment(banded_alignment, mate, genome, sequence, start, forward, read_string);
                h = new single_hit(alignment_result);
                alignments[mate].add(h);
                if (valid_hit(read_len)) {
                    hits[mate][genome].offer(h);
                }
            }
        }
        
        /**
         * Exhaustively aligns the read to a large region around the candidate location. Is used in very sensitive mode. 
         * 
         * @param mate The number of segment in the read (can be 0 for single, 0/1 for paired-end) 
         * @param genome The number of genome read is being mapped against
         * @param sequence The number of sequence read is being mapped against
         * @param ref_start The candidate location in the genome
         * @param read_string
         */
        public void exhaustive_examine_location(int mate, int genome, int sequence, int ref_start, String read_string) {
            boolean forward = ref_start >= 0;
            single_hit h;
            int start, stop, read_len = read_string.length();
            if (!forward) {
                ref_start = -ref_start - 1;
            }
            start = Math.max(ref_start - SHOULDER - read_len, 0);
            stop = Math.min(start + 2 * (read_len + SHOULDER), (int)sequence_length[genome][sequence] - 1);
            num_hits++;
            reference.setLength(0);
            genomeSc.get_sub_sequence(reference, genome, sequence, start, stop - start + 1, true);
            num_alns++;
            perform_alignment(false, mate, genome, sequence, start, forward, read_string);
            h = new single_hit(alignment_result);
            if (valid_hit(read_len)) {
                hits[mate][genome].offer(h);
            }
        }

        /**
         * Calls the [banded] Smith-Waterman to align query (read) to the subject (genomic hit)
         * 
         * @param banded_alignment Determines if alignment is banded
         * @param mate The number of segment in the read (can be 0 for single, 0/1 for paired-end) 
         * @param genome The number of genome read is being mapped against
         * @param sequence The number of sequence read is being mapped against
         * @param start The candidate location in the genome
         * @param forward Determines if read should be mapped in forward or in reverse direction
         * @param read_string 
         */
        public void perform_alignment(boolean banded_alignment, int mate, int genome, int sequence, int start, boolean forward, String read_string) {
            if (banded_alignment) {
                bounded_aligner.align(forward ? read_string : reverse_read[mate].toString(), reference.toString()); 
                alignment_result.genome = genome;
                alignment_result.sequence = sequence;
                alignment_result.cigar = bounded_aligner.get_cigar().toString();
                alignment_result.identity = bounded_aligner.get_identity();
                alignment_result.score = bounded_aligner.get_similarity();
                alignment_result.start = start;
                alignment_result.offset = bounded_aligner.get_offset();
                alignment_result.length = bounded_aligner.get_range_length();
                alignment_result.deletions = bounded_aligner.get_deletions();
                alignment_result.forward = forward;
                alignment_result.reference = reference.toString();
            } else {
                aligner.align(forward ? read_string : reverse_read[mate].toString(), reference.toString()); 
                alignment_result.genome = genome;
                alignment_result.sequence = sequence;
                alignment_result.cigar = aligner.get_cigar().toString();
                alignment_result.identity = aligner.get_identity();
                alignment_result.score = aligner.get_similarity();
                alignment_result.start = start;
                alignment_result.offset = aligner.get_offset();
                alignment_result.length = aligner.get_range_length();
                alignment_result.deletions = aligner.get_deletions();
                alignment_result.forward = forward;
                alignment_result.reference = reference.toString();
            }
        }

        /**
         * Looks for a similar subject (reference) sequence in the list of previous alignments.
         * 
         * @param mate The number of segment in the read (can be 0 for single, 0/1 for paired-end) 
         * @param genome The number of genome read is being mapped against
         * @param sequence The number of sequence read is being mapped against
         * @param ref_start The candidate location in the genome
         * @param fwd The direction of the alignment 
         * @return True if there is a similar subject in the list of previous alignments, or False.
         */
        public boolean find_similar_subject(int mate, int genome, int sequence, int ref_start, boolean fwd) {
            single_hit similar_alignment;
            boolean found = false;
            Iterator<single_hit> itr = alignments[mate].iterator();
            while (itr.hasNext() && !found) {
                similar_alignment = itr.next();
                if (are_equal(similar_alignment.reference, reference)) {
                    alignment_result.genome = genome;
                    alignment_result.sequence = sequence;
                    alignment_result.identity = similar_alignment.identity;
                    alignment_result.score = similar_alignment.score;
                    alignment_result.start = ref_start;
                    alignment_result.offset = similar_alignment.offset;
                    alignment_result.length = similar_alignment.length;
                    alignment_result.deletions = similar_alignment.deletions;
                    alignment_result.forward = fwd;
                    alignment_result.cigar = similar_alignment.cigar.toString();
                    alignment_result.reference = reference.toString();
                    found = true;
                }
            }
            return found;
        }  
        
        /**
         * Determines if an alignment is a valid hit.
         * 
         * @return True if the alignment is a valid hit, or False.
         */
        boolean valid_hit(int read_len) {
                return (alignment_result.identity > MIN_IDENTITY &&
                        alignment_result.length >= MIN_HIT_LENGTH && 
                        alignment_result.start + alignment_result.offset >= 0 && 
                        alignment_result.start + alignment_result.offset + 
                        alignment_result.deletions + read_len 
                        <= sequence_length[alignment_result.genome][alignment_result.sequence]);
        }
        
        /**
         * Determines if two strings are equal.
         * 
         * @param s1 The first string
         * @param s2 The second string
         * @return True if two strings are equal, or False
         */
        public boolean are_equal(String s1, StringBuilder s2) {
            boolean are_equal = s1.length() == s2.length();
            for (int i = 0; are_equal && i < s1.length(); ++i)
                if (s1.charAt(i) != s2.charAt(i))
                    are_equal = false;
            return are_equal;
        }
        
        /**
         * Collects and reports all the hits of the read in the genomes.
         */
        public void report_hits() {
            int genome, i;
            if (ALIGNMENT_MODE < 0) { // pangenomic best
                for (i = 0; i < genome_numbers.size(); ++i) {
                    genome = genome_numbers.get(i);
                    collect_hits(genome);
                }
                call_mode();
                clear_hits_list();
            } else {
                best_hit[0] = null;
                best_hit[1] = null;
                for (i = 0; i < genome_numbers.size(); ++i) {
                    genome = genome_numbers.get(i);
                    collect_hits(genome);
                    call_mode();
                    clear_hits_list();
                }
                if (VERYSENSITIVE && unmapped_genomes[0].size() > 0)
                    check_unmapped_genomes();
                unmapped_genomes[0].clear();
                if (paired_end)
                    unmapped_genomes[1].clear();
            }
            alignments[0].clear();
            if (paired_end)
                alignments[1].clear();
        }
        
        /**
         * Tries to remap an unmapped read to remaining genomes, exhaustively.
         */
        public void check_unmapped_genomes() {
            int i, mate;
            int genome1, genome2;
            single_hit s;
            Relationship rel;
            Node node, neighbor;
            IndexPointer pointer;
            String read_string;
            int[] loc2;
            int loc1, seq1, seq2, node_len, read_len;
            long high;
            char side1, side2;
            String origin1;
            Iterator<Integer> itr;
            for (mate = 0; mate < num_segments; ++mate) {
                read_string = fastq_record[mate].getReadString();
                read_len = read_string.length();
                s = best_hit[mate];
                if (s != null) {
                    genome1 = s.genome;
                    seq1 = s.sequence;
                    try (Transaction tx = GRAPH_DB.beginTx()) {
                        loc1 = Math.max(s.start - SHOULDER, 0);
                        pointer = locate(GRAPH_DB, genomeSc, indexSc, genome1, seq1, loc1);
                        origin1 = "G" + genome1 + "S" + seq1;
                        node = GRAPH_DB.getNodeById(pointer.node_id);
                        side1 = pointer.canonical ? 'F' : 'R';
                        high = Math.min(s.start + read_len + SHOULDER - 1, sequence_length[genome1][seq1] - 1);
                        while (loc1 < high) {
                            Pantools.logger.trace(loc1);
                            node_len = (int) node.getProperty("length");
                            for (Relationship r: node.getRelationships(Direction.INCOMING, RelTypes.FF, RelTypes.FR, RelTypes.RF, RelTypes.RR)) {
                                side2 = r.getType().name().charAt(1);
                                for (String origin2: r.getPropertyKeys()) {
                                    loc2 = (int[])r.getProperty(origin2);
                                    genome2 = Integer.parseInt(origin2.split("S")[0].substring(1));
                                    Pantools.logger.trace("{} {}.", unmapped_genomes[0].size(), genome2);
                                    if (unmapped_genomes[mate].contains(genome2)) {
                                        seq2 = Integer.parseInt(origin2.split("S")[1]);
                                        if (side1 == side2) {
                                            for (i = 0; i < loc2.length; ++i)
                                                locations[mate][genome2].add(new int[]{seq2, loc2[i]});
                                        } else {
                                            for (i = 0; i < loc2.length; ++i)
                                                locations[mate][genome2].add(new int[]{seq2, -loc2[i]});
                                        }
                                    }
                                }
                            }
                            loc1 += node_len - K + 1;
                            rel = get_outgoing_edge(node, origin1, loc1);
                            if (rel == null)
                                break;
                            else
                                neighbor = rel.getEndNode();
                            node = neighbor;
                            side1 = rel.getType().name().charAt(1);
                        } // while
                        itr = unmapped_genomes[mate].iterator();
                        while (itr.hasNext())
                            cluster_and_examine_locations(mate, itr.next(), SHOULDER + read_len, read_string);
                        tx.success();
                    }
                }
            }
        }
        
        /**
         * Calls the suitable method based on mapping mode.
         */
        public void call_mode() {
            switch (Math.abs(ALIGNMENT_MODE)) {
                case 0: // all hits
                    report_all_hit(false);
                break;
                case 1: // unique best hits
                    report_unique_hit();
                break;
                case 2: // one best hits
                    report_one_hit();
                break;
                case 3: // all best hits
                    report_all_hit(true);
            }            
        }
        
        /**
         * Collect all the hits of the current read in a genome
         * @param genome The genome for which hits are collected
         */
        public void collect_hits(int genome) {
            int read_len1 = fastq_record[0].getReadLength(), read_len2;
            if (!paired_end) {
                if (hits[0][genome].isEmpty()) {
                    unmapped_genomes[0].add(genome);
                    single_hits.offer(new single_hit(genome,0,0,-1,-1,0,0,0,true,null,null));
                } else {
                    while(!hits[0][genome].isEmpty())
                        single_hits.offer(hits[0][genome].remove());
                    single_hit h = single_hits.peek();
                    if (best_hit[0] == null || h.score > best_hit[0].score)
                       best_hit[0] = h;
                }
            } else {
                read_len2 = fastq_record[1].getReadLength();
                boolean reads_paired = false;
                single_hit h1, h2;
                Iterator<single_hit> itr1;
                Iterator<single_hit> itr2;
                int frag_len, best_frag_len;
                if (hits[0][genome].isEmpty() && hits[1][genome].isEmpty()) {
                    unmapped_genomes[0].add(genome);
                    unmapped_genomes[1].add(genome);
                    paired_hits.offer(new paired_hit(Integer.MAX_VALUE, new single_hit(genome,0,0,-1,-1,0,0,0,true,null,null),new single_hit(genome,0,0,-1,-1,0,0,0,true,null,null)));
                } else if (hits[0][genome].isEmpty() && !hits[1][genome].isEmpty()) {
                    unmapped_genomes[0].add(genome);
                    while (!hits[1][genome].isEmpty())
                        paired_hits.offer(new paired_hit(Integer.MAX_VALUE, new single_hit(genome,0,0,-1,-1,0,0,0,true,null,null),new single_hit(hits[1][genome].remove())));
                    h2 = paired_hits.peek().h2;
                    if (best_hit[1] == null || h2.score > best_hit[1].score)
                       best_hit[1] = h2;
                } else if (!hits[0][genome].isEmpty() && hits[1][genome].isEmpty()) {
                    unmapped_genomes[1].add(genome);
                    while (!hits[0][genome].isEmpty())
                        paired_hits.offer(new paired_hit(Integer.MAX_VALUE, new single_hit(hits[0][genome].remove()), new single_hit(genome,0,0,-1,-1,0,0,0,true,null,null)));
                    h1 = paired_hits.peek().h1;
                    if (best_hit[0] == null || h1.score > best_hit[0].score)
                       best_hit[0] = h1;
                } else {
                    itr1 = hits[0][genome].iterator();
                    while (itr1.hasNext()) {
                        frag_len = best_frag_len = Integer.MAX_VALUE;
                        h1 = itr1.next();
                        itr2 = hits[1][genome].iterator();
                        while (itr2.hasNext()) {
                            h2 = itr2.next();
                            if (h1.sequence != h2.sequence) {
                                continue;
                            }
                            frag_len = fragment_length(h1, h2, read_len1, read_len2);
                            if (frag_len < best_frag_len)
                                best_frag_len = frag_len;
                            else
                                continue;
                            paired_hits.offer(new paired_hit(frag_len, new single_hit(h1), new single_hit(h2)));
                            reads_paired = true;
                        }
                    }
                    if (!reads_paired)
                        paired_hits.offer(new paired_hit(Integer.MAX_VALUE, new single_hit(hits[0][genome].peek()), new single_hit(hits[1][genome].peek())));
                    h1 = paired_hits.peek().h1;
                    if (best_hit[0] == null || h1.score > best_hit[0].score)
                       best_hit[0] = h1;
                    h2 = paired_hits.peek().h2;
                    if (best_hit[1] == null || h2.score > best_hit[1].score)
                       best_hit[1] = h2;
                    hits[0][genome].clear();
                    hits[1][genome].clear();
                }
            }
        }

        /**
         * Reports hits which have only one best hit.
         */
        public void report_unique_hit() {
            if (!paired_end) { // single end reads 
                single_hit h, best_hit;
                h = single_hits.remove(); 
                best_hit = new single_hit(h);
                if (h.start != -1) {
                    if (!single_hits.isEmpty()) {
                        h = single_hits.remove();
                        if (h.score < best_hit.score) {
                            write_single_sam_record(best_hit, 0, 1);
                            unique[best_hit.genome]++;
                        } else {
                            shared[best_hit.genome]++;
                        }
                    } else {
                        write_single_sam_record(best_hit, 0, 1);
                        unique[best_hit.genome]++;
                    }
                } else {
                    if (ALIGNMENT_MODE >= 0) { 
                        unmapped[best_hit.genome]++;
                    } else { // when competitive mapping
                        unmapped[adj_total_genomes +1]++;
                    }
                    write_single_sam_record(best_hit, 4, 0);
                }
            } else {
                paired_hit h, best_hit;
                h = paired_hits.remove(); 
                best_hit = new paired_hit(h.fragment_length, h.h1, h.h2);
                if (best_hit.get_max_start() != -1) {
                    if (!paired_hits.isEmpty()) {
                        h = paired_hits.remove();
                        if (h.get_score() < best_hit.get_score()) {
                            if (best_hit.get_min_start() != -1) {
                                write_paired_sam_record(best_hit, 1, 1, 1, 1);
                                unique[best_hit.h1.genome]++;
                                unique[best_hit.h2.genome]++;
                            } else if (best_hit.h1.start != -1) {
                                write_paired_sam_record(best_hit, 1, 5, 1, 0);
                                unique[best_hit.h1.genome]++;
                                unmapped[best_hit.h2.genome]++;
                            } else if (best_hit.h2.start != -1) {
                                write_paired_sam_record(best_hit, 5, 1, 0, 1);
                                unique[best_hit.h2.genome]++;
                                unmapped[best_hit.h1.genome]++;
                            }
                        } else {
                            if (best_hit.get_min_start() != -1) {
                                write_paired_sam_record(best_hit, 1, 1, 1.0 / (2 + paired_hits.size()), 1.0 / (2 + paired_hits.size()));
                                shared[best_hit.h1.genome]++;
                                shared[best_hit.h2.genome]++;
                            } else if (best_hit.h1.start != -1) {
                                write_paired_sam_record(best_hit, 1, 5, 1.0 / (2 + paired_hits.size()), 0);
                                shared[best_hit.h1.genome]++;
                                unmapped[best_hit.h2.genome]++;
                            } else if (best_hit.h2.start != -1) {
                                write_paired_sam_record(best_hit, 5, 1, 0, 1.0 / (2 + paired_hits.size()));
                                shared[best_hit.h2.genome]++;
                                unmapped[best_hit.h1.genome]++;
                            }
                        }
                    } else {
                        if (best_hit.get_min_start() != -1) {
                            write_paired_sam_record(best_hit, 1, 1, 1, 1);
                            unique[best_hit.h1.genome]++;
                            unique[best_hit.h2.genome]++;
                        } else if (best_hit.h1.start != -1) {
                            write_paired_sam_record(best_hit, 1, 5, 1, 0);
                            unique[best_hit.h1.genome]++;
                            unmapped[best_hit.h2.genome]++;
                        } else if (best_hit.h2.start != -1) {
                            write_paired_sam_record(best_hit, 5, 1, 0, 1);
                            unique[best_hit.h2.genome]++;
                            unmapped[best_hit.h1.genome]++;
                        }
                    }
                } else { // unmapped 
                    if (ALIGNMENT_MODE >= 0) { 
                        unmapped[best_hit.h1.genome]++;
                        unmapped[best_hit.h2.genome]++;
                    } else { // when competitive mapping
                        unmapped[adj_total_genomes +1] += 2;
                    }
                    write_paired_sam_record(best_hit, 5, 5, 0, 0);
                }
            }
        }

        /**
         * Reports a random best-scored hit. Alignment mode -2 and 2 
         */
        public void report_one_hit() {
            if (!paired_end) { // single end reads 
                single_hit h, best_hit;
                int count;
                double rnd, freq = 0, sum_freq = 0;
                best_hit = single_hits.peek();
                for (count = 0; !single_hits.isEmpty(); ++count) {
                    h = single_hits.remove();
                    if (h.start == -1 || h.score < best_hit.score) {
                        break;
                    }
                    sum_freq += raw_abundance[h.genome] / genome_sizes[h.genome];
                    single_hits_2.add(h);
                }
                rnd = rand.nextDouble();
                while(!single_hits_2.isEmpty()) {
                    best_hit = single_hits_2.remove();
                    freq += raw_abundance[best_hit.genome] / genome_sizes[best_hit.genome];
                    if (rnd < freq / sum_freq)
                        break;
                }
                if (best_hit.start != -1) {
                    if (count == 1) {
                        unique[best_hit.genome]++;
                    } else {
                        shared[best_hit.genome]++;
                    } 
                    write_single_sam_record(best_hit, 0, 1.0 / count);
                } else {
                    if (ALIGNMENT_MODE >= 0) {
                        unmapped[best_hit.genome]++;
                    } else { // when competitive mapping
                        unmapped[adj_total_genomes +1]++;
                    }
                    write_single_sam_record(best_hit, 4, 0);
                }   
            } else { // paired
                paired_hit h, best_hit;
                int count;
                double rnd, freq = 0, sum_freq = 0;
                best_hit = paired_hits.peek();
                for (count = 0; !paired_hits.isEmpty(); ++count) {
                    h = paired_hits.remove();
                    if (h.get_max_start() == -1 || h.get_score() < best_hit.get_score())
                        break;
                    sum_freq += raw_abundance[h.h1.genome] / genome_sizes[h.h1.genome];
                    paired_hits_2.add(h);
                }
                
                rnd = rand.nextDouble();
                while(!paired_hits_2.isEmpty()) {
                    best_hit = paired_hits_2.remove();
                    freq += raw_abundance[best_hit.h1.genome] / genome_sizes[best_hit.h1.genome];
                    if (rnd < freq / sum_freq) {
                        break;
                    }
                }
                if (best_hit.get_max_start() != -1) {
                    if (best_hit.get_min_start() != -1 ) {
                        write_paired_sam_record(best_hit, 1, 1, 1.0 / count, 1.0 / count);
                        if (count == 1) {
                            unique[best_hit.h1.genome]++;
                            unique[best_hit.h2.genome]++;
                        } else {
                            shared[best_hit.h1.genome]++;
                            shared[best_hit.h2.genome]++;
                        }
                    } else if (best_hit.h1.start != -1 ) {
                        write_paired_sam_record(best_hit, 1, 5, 1.0 / count, 0);
                        unmapped[best_hit.h2.genome]++;
                        if (count == 1) {
                            unique[best_hit.h1.genome]++;
                        } else { 
                            shared[best_hit.h1.genome]++;
                        }
                    } else if (best_hit.h2.start != -1) {
                        write_paired_sam_record(best_hit, 5, 1, 0, 1.0 / count);
                        unmapped[best_hit.h1.genome]++;
                        if (count == 1) {
                            unique[best_hit.h2.genome]++;
                        } else { 
                            shared[best_hit.h2.genome]++;
                        }
                    }
                } else { 
                    write_paired_sam_record(best_hit, 5, 5, 0, 0);
                    if (ALIGNMENT_MODE >= 0) { 
                        unmapped[best_hit.h1.genome]++;
                        unmapped[best_hit.h2.genome]++;
                    } else { // when competitive mapping
                        unmapped[adj_total_genomes +1] += 2; 
                    }
                }
            }
        }
        
        /**
         * Reports all best-scored hits. Alignment mode -3, 0, 3 
         * @param best 
         */
        public void report_all_hit(boolean best) {
            if (!paired_end) {
                single_hit h, best_hit;
                int count, prev_genome;
                best_hit = single_hits.peek();
                for (count = 0; !single_hits.isEmpty(); ++count) {
                    h = single_hits.remove();
                    if (h.start == -1 || (best && h.score < best_hit.score)) {
                        break;
                    }
                    single_hits_2.add(h); 
                }
                if (best_hit.start != -1) {
                    prev_genome = -1;
                    while (!single_hits_2.isEmpty()) {
                        best_hit = single_hits_2.remove();
                        if (count == 1) {
                            unique[best_hit.genome]++;
                        } else {
                            shared[best_hit.genome]++;
                        }
                        write_single_sam_record(best_hit, best_hit.genome == prev_genome ? 256 : 0, 1.0 / count);
                        prev_genome = best_hit.genome;
                    }
                } else {
                    if (ALIGNMENT_MODE >= 0) {
                        unmapped[best_hit.genome]++;
                    } else { // when competitive mapping
                        unmapped[adj_total_genomes +1]++;
                    }
                    write_single_sam_record(best_hit, 4, 0);
                } 
            } else { // paired 
                paired_hit h, best_hit;
                int count, prev_genome;
                best_hit = paired_hits.peek();
                for (count = 0; !paired_hits.isEmpty(); ++count) {
                    h = paired_hits.remove();
                    if (h.get_max_start() == -1 || (best && h.get_score() < best_hit.get_score()))
                        break;
                    paired_hits_2.add(h);
                }
                if (best_hit.get_max_start() != -1) {
                    prev_genome = -1;
                    while (!paired_hits_2.isEmpty()) {
                        best_hit = paired_hits_2.remove();
                        if (best_hit.get_min_start() != -1) {
                            write_paired_sam_record(best_hit, best_hit.h1.genome == prev_genome ? 257 : 1,
                                    best_hit.h2.genome == prev_genome ? 257 : 1, 1.0 / count, 1.0 / count);
                            if (count == 1) {
                                unique[best_hit.h1.genome]++;
                                unique[best_hit.h2.genome]++;
                            } else {
                                shared[best_hit.h1.genome]++;
                                shared[best_hit.h2.genome]++;
                            }
                        } else if (best_hit.h1.start != -1) {
                            write_paired_sam_record(best_hit, best_hit.h1.genome == prev_genome ? 257 : 1, 5, 1.0 / count, 0);
                            unmapped[best_hit.h2.genome]++;
                            if (count == 1) {
                                unique[best_hit.h1.genome]++;
                            } else {
                                shared[best_hit.h1.genome]++;
                            }
                        } else if (best_hit.h2.start != -1) {
                            write_paired_sam_record(best_hit, 5, best_hit.h2.genome == prev_genome ? 257 : 1, 0, 1.0 / count);
                            unmapped[best_hit.h1.genome]++;
                            if (count == 1) {
                                unique[best_hit.h2.genome]++;
                            } else {
                                shared[best_hit.h2.genome]++;
                            }
                        }
                        prev_genome = best_hit.h1.genome;
                    }
                } else { 
                    write_paired_sam_record(best_hit, 5, 5, 0, 0);
                    if (ALIGNMENT_MODE >= 0) {
                        unmapped[best_hit.h1.genome]++;
                        unmapped[best_hit.h2.genome]++;
                    } else { // when competitive mapping
                        unmapped[adj_total_genomes +1] += 2;
                    }
                } 
            }
        }
        
        /**
         * Clears data structures of the collected hits
         */
        public void clear_hits_list() {
            if (!paired_end) {
                single_hits.clear();
                single_hits_2.clear();
            } else {
                paired_hits.clear();
                paired_hits_2.clear();
            }
        }        

        /**
         * Writes the SAM record for single-end mapping
         * 
         * @param h The hit to be reported
         * @param flag The SAM flag of the hit
         * @param p_value
         */
        public void write_single_sam_record(single_hit h, int flag, double p_value) {
            if (sam_writers != null) {
                SAMRecord sam_record = new SAMRecord(null);
                if (h.start == -1) {
                    sam_record.setReadName(read_name[0]);
                    sam_record.setFlags(flag);
                    sam_record.setReadString(fastq_record[0].getReadString());
                } else {
                    flag |= h.forward?0:16; // direction
                    sam_record.setReferenceName(sequence_titles[h.genome][h.sequence].split("\\s")[0]);
                    sam_record.setFlags(flag);
                    sam_record.setReadName(read_name[0]);
                    sam_record.setAlignmentStart(h.start + h.offset + 1);
                    sam_record.setMappingQuality((int)Math.ceil(p_value * -10 * Math.log10(1 - (h.identity - 0.000001))));
                    sam_record.setCigarString(h.cigar);
                    sam_record.setReadString(h.forward ? fastq_record[0].getReadString() : reverse_read[0].toString());
                    sam_record.setBaseQualityString(fastq_record[0].getBaseQualityString());
                }
                if (READ_GROUP != null){
                    sam_record.setAttribute(SAMTag.RG, READ_GROUP.getReadGroupId());
                }
                synchronized (sam_writers[h.genome]) {
                    if (flag == 4 && ALIGNMENT_MODE < 0) {
                        sam_writers[adj_total_genomes + 1].addAlignment(sam_record);
                    } else {
                        sam_writers[h.genome].addAlignment(sam_record);
                    }
                }
            }
        }
        
        /**
         * Writes the SAM record of a paired-end hit
         * @param h The hit to be reported
         * @param flag1 the initial SAM flag of read 1
         * @param flag2 the initial SAM flag of read 2
         * @param p_value1
         * @param p_value2 
         */
        public void write_paired_sam_record(paired_hit h, int flag1, int flag2, double p_value1, double p_value2) {
           
            if (sam_writers == null) { // when --out-format is 0
                return;
            }
            // the first segment
            SAMRecord sam_record1, sam_record2;
            sam_record1 = new SAMRecord(null);
            sam_record2 = new SAMRecord(null);
            int position1 = h.h1.start + h.h1.offset + 1;
            int position2 = h.h2.start + h.h2.offset + 1;
                
            String chr_name_1 = "";
            String chr_name_2 = "";

            if (h.h1.start!= -1) {
                chr_name_1 = sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0];
            }
            if (h.h2.start!=-1) {
                chr_name_2 = sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0]; 
            }
            flag1 |= 64; 
            if (h.h2.start == -1) {
                flag1 |=8;
            } else {
                flag1 |= !h.h2.forward?32:0; // SEQ being reverse complemented
            }
                
            if (h.h1.start == -1) {
                sam_record1.setReadName(read_name[0]);
                sam_record1.setFlags(flag1);
                sam_record1.setReadString(fastq_record[0].getReadString());
                if (h.h2.start != -1) {
                    sam_record1.setReferenceName(sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0]);
                    sam_record1.setAlignmentStart(position2);
                    sam_record1.setMateReferenceName(sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0]);
                    sam_record1.setMateAlignmentStart(position2);
                }
            } else {
                if (h.h1.start != -1 && h.h2.start != -1 ) {
                    if(chr_name_1.equals(chr_name_2)) {
                        if(h.h1.forward != h.h2.forward) {
                            if (h.fragment_length < Integer.MAX_VALUE ) {
                                flag1 |= 2;
                            }
                        }
                    }
                }
                
                flag1 |= h.h1.forward?0:16; // SEQ being reverse complemented
                    
                //qname
                    sam_record1.setReadName(read_name[0]); 
                //flag
                    sam_record1.setFlags(flag1); 
                //rname
                    sam_record1.setReferenceName(sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0]);
                //pos
                    sam_record1.setAlignmentStart(position1);
                //mapq
                    sam_record1.setMappingQuality((int)Math.ceil(p_value1 * -10 * Math.log10(1 - (h.h1.identity - 0.000001))));
                    //sam_record.setMappingQuality((int)Math.round(h.identity1 * 100));
                //cigar
                    sam_record1.setCigarString(h.h1.cigar);
                //rnext
                    if (h.h1.start != -1 && h.h2.start != -1 && ! chr_name_1.equals(chr_name_2)) {
                        sam_record1.setMateReferenceName(sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0]);
                    } else {
                        sam_record1.setMateReferenceName(sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0]);
                    }
                //pnext 
                    if (h.h2.start == -1) {
                        sam_record1.setMateAlignmentStart(position1);
                    } else {
                        sam_record1.setMateAlignmentStart(position2);
                    }
                //tlen
                    if (h.h1.start != -1 && h.h2.start != -1 && ! chr_name_1.equals(chr_name_2)) {
                        sam_record1.setInferredInsertSize(0);
                    } else if (h.h2.start == -1) {
                        sam_record1.setInferredInsertSize(0);
                    } else if(position2 > position1) {
                         sam_record1.setInferredInsertSize(position2 - position1 + fastq_record[0].getReadLength());
                    } else {
                        sam_record1.setInferredInsertSize(-(position1 - position2 + fastq_record[1].getReadLength()));
                    }
                //seq
                    sam_record1.setReadString((h.h1.forward ? fastq_record[0].getReadString() : reverse_read[0]).toString());
                //qual
                    sam_record1.setBaseQualityString(fastq_record[0].getBaseQualityString());
            }
            if (READ_GROUP != null){
                sam_record1.setAttribute(SAMTag.RG, READ_GROUP.getReadGroupId());
            }
                
            // the second segment
            flag2 |= 128; 
                
            if (h.h1.start == -1) {
                flag2 |=8;
            } else {
                flag2 |= !h.h1.forward?32:0; // SEQ being reverse complemented
            }
                
            if (h.h2.start == -1) { // not mapped
                sam_record2.setReadName(read_name[1]);
                sam_record2.setFlags(flag2);
                sam_record2.setReadString(fastq_record[1].getReadString());
                if (h.h1.start != -1) {
                    sam_record2.setReferenceName(sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0]);
                    sam_record2.setAlignmentStart(position1);
                    sam_record2.setMateReferenceName(sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0]);
                    sam_record2.setMateAlignmentStart(position1);
                }
                   
            } else {
                if (h.h1.start != -1 && h.h2.start != -1 && chr_name_1.equals(chr_name_2)) {
                    if(h.h1.forward != h.h2.forward) {
                        if (h.fragment_length < Integer.MAX_VALUE ) {
                            flag2 |= 2;
                        }
                    }
                }
                      
                    flag2 |= h.h2.forward?0:16; // SEQ being reverse complemented
                //qname
                    sam_record2.setReadName(read_name[1]); 
                //flag
                    sam_record2.setFlags(flag2); 
                //rname
                    sam_record2.setReferenceName(sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0]);
                //pos
                    sam_record2.setAlignmentStart(position2);
                //mapq
                    sam_record2.setMappingQuality((int)Math.ceil(p_value2 * -10 * Math.log10(1 - (h.h2.identity - 0.000001))));
                    //sam_record.setMappingQuality((int)Math.round(h.identity2 * 100));
                //cigar
                    sam_record2.setCigarString(h.h2.cigar);
                //rnext
                    if (h.h1.start != -1 && h.h2.start != -1 && ! chr_name_1.equals(chr_name_2)) {
                        sam_record2.setMateReferenceName(sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0]);
                    } else { 
                        sam_record2.setMateReferenceName(sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0]);
                    }
                //pnext
                    if (h.h1.start == -1) {
                        sam_record2.setMateAlignmentStart(position2);
                    } else{
                        sam_record2.setMateAlignmentStart(position1);
                    }
                //tlen
                    if (h.h1.start != -1 && h.h2.start != -1 && !chr_name_1.equals(chr_name_2)) {
                        sam_record2.setInferredInsertSize(0);
                    } else if (h.h1.start == -1){
                        sam_record2.setInferredInsertSize(0);
                    } else if (position2 > position1) {
                         sam_record2.setInferredInsertSize(-(position2 - position1 + fastq_record[0].getReadLength()));
                    } else {
                        sam_record2.setInferredInsertSize(position1 - position2 + fastq_record[1].getReadLength());
                    }
                //seq
                    sam_record2.setReadString((h.h2.forward ? fastq_record[1].getReadString() : reverse_read[1]).toString());
                //qual
                    sam_record2.setBaseQualityString(fastq_record[1].getBaseQualityString());
                }
                if (READ_GROUP != null){
                    sam_record2.setAttribute(SAMTag.RG, READ_GROUP.getReadGroupId());
                }
                synchronized(sam_writers[h.h1.genome]) {
                    if (ALIGNMENT_MODE < 0 && flag1 == 77 && flag2 == 141) { // both reads are unmapped & competitive mapping
                        sam_writers[adj_total_genomes+1].addAlignment(sam_record1);
                        sam_writers[adj_total_genomes+1].addAlignment(sam_record2);
                    } else {
                        sam_writers[h.h1.genome].addAlignment(sam_record1);
                        sam_writers[h.h2.genome].addAlignment(sam_record2);
                    }
                } 
        }

        /**
         * Calculates length of the fragment of a paired-end hit
         * @param h1 Hit of the first segment 
         * @param h2 Hit of the second segment
         * @param read_len1 Length of the first read 
         * @param read_len2 Length of the first read
         * @return The proper length of the fragment, or Infinity if it is invalid.
         */
        public int fragment_length(single_hit h1, single_hit h2, int read_len1, int read_len2) {
            int frag_len;
            int position1, position2;
            position1 = h1.start + h1.offset;
            position2 = h2.start + h2.offset;
            if (h1.forward) {
                frag_len = position2 + fastq_record[1].getReadLength() - position1;
            } else {
                frag_len = position1 + fastq_record[0].getReadLength() - position2;
            }
            
            if (frag_len < Math.max(read_len1, read_len2) || frag_len > MAX_FRAGMENT_LENGTH) {
                frag_len = Integer.MAX_VALUE;
            }
            return frag_len;
        }
    }   
    
    /**
     * Maps a single or paired-end library to the pangenome, generating a SAM/BAM
     * file for each genome.
     */
    public void map_reads() {
        Pantools.logger.info("Mapping single or paired-end short reads to the pangenome.");
        if (OUTPUT_PATH.equals(WORKING_DIRECTORY)) { // no --output-path given, write to database
            OUTPUT_PATH += "read_mapping/";
            new File(WORKING_DIRECTORY + "read_mapping").mkdir();
        }
        create_skip_arrays(false, true);
        report_number_of_threads();
        int i, j, t, genome;
        Node pangenome_node;
        BufferedWriter out;
        SAMFileWriter[] sams;
        SAMFileHeader[] headers;
        int total_unique, total_mapped, total_unmapped;
        boolean paired;
        if (PATH_TO_THE_FIRST_SRA == null) {
            Pantools.logger.error("PATH_TO_THE_FIRST_SRA is empty.");
            System.exit(1);
        }
        paired = PATH_TO_THE_SECOND_SRA != null;
        if (INTERLEAVED) { 
            paired = true; // both read pairs are stored in the same file
        }
        
        FastqReader[] reader = new FastqReader[2];
        reader[0] = new FastqReader(new File(PATH_TO_THE_FIRST_SRA), true);
        if (paired && !INTERLEAVED) {
            reader[1] = new FastqReader(new File(PATH_TO_THE_SECOND_SRA), true);
        }
        if (paired && MAX_FRAGMENT_LENGTH == 4998) { // 4998 is the default value
            MAX_FRAGMENT_LENGTH = 5000;
            Pantools.logger.info("No --max-fragment-length was set. Default is {}.", MAX_FRAGMENT_LENGTH);
        }
        connect_pangenome();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "map"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            if (pangenome_node == null) {
                Pantools.logger.error("Can not locate database node.");
                System.exit(1);
            }
            tx.success();
        }
       
        int extra = 0;
        if (ALIGNMENT_MODE < 0) { // when competitive mapping
            extra = 1;
        }
        num_shared_mapping = new AtomicLong[GENOME_DB.num_genomes + 1];
        num_unique_mapping = new AtomicLong[GENOME_DB.num_genomes + 1];
        num_unmapped = new AtomicLong[GENOME_DB.num_genomes + 1 + extra];
        number_of_reads = new AtomicLong(0);
        number_of_alignments = new AtomicLong(0);
        number_of_hits = new AtomicLong(0); 
        ArrayList<Integer>[] genome_numbers = retrieve_genomes_to_map_against();
        adj_total_genomes = genome_numbers[0].size();
        Pantools.logger.info("OUT FORMAT: {}.", OUTFORMAT);
        if (OUTFORMAT.equals("BAM") || OUTFORMAT.equals("SAM")) {
            sams = new SAMFileWriter[GENOME_DB.num_genomes + 1 + extra];
            headers = new SAMFileHeader[GENOME_DB.num_genomes + 1 + extra];
            for (i = 0; i < genome_numbers[0].size(); ++i) { 
                genome = genome_numbers[0].get(i);
                headers[genome] = new SAMFileHeader();
                for (j = 1; j <= GENOME_DB.num_sequences[genome]; ++j) {
                    headers[genome].addSequence(new SAMSequenceRecord(GENOME_DB.sequence_titles[genome][j].split("\\s")[0],
                            (int) GENOME_DB.sequence_length[genome][j]));
                }
                headers[genome].addProgramRecord(new SAMProgramRecord("PanTools"));
                // add read group header line in SAM
                if(READ_GROUP != null) headers[genome].addReadGroup(READ_GROUP);
                if (OUTFORMAT.equals("BAM")) {
                    sams[genome] = new SAMFileWriterFactory().makeBAMWriter(headers[genome], false, 
                            new File(OUTPUT_PATH + "/pantools_" + genome + ".bam"));
                } else { // SAM
                    sams[genome] = new SAMFileWriterFactory().makeSAMWriter(headers[genome], false, 
                            new File(OUTPUT_PATH + "/pantools_" + genome + ".sam"));
                }
            }
            if (ALIGNMENT_MODE < 0) { // when competitive mapping
                headers[genome_numbers[0].size()+1] = new SAMFileHeader();
                sams[genome_numbers[0].size()+1] = new SAMFileWriterFactory().makeSAMWriter(headers[genome_numbers[0].size()+1], false, 
                            new File(OUTPUT_PATH + "/unmapped.sam"));
            }
        } else { // --output-format none 
            sams = null;
        }
        
        if (!genome_numbers[0].isEmpty()) {
            String single_or_paired = "SINGLE-END";
            if (paired) {
                single_or_paired = "PAIRED-END";
            }
            Pantools.logger.info("Mapping {} reads on {} genome(s).", single_or_paired, genome_numbers[0].size());
            try {
                ExecutorService es = Executors.newFixedThreadPool(THREADS);
                for (t = 0; t < THREADS; ++t) {
                    es.execute(new Map(t, genome_numbers[t], paired, sams, reader));
                }
                es.shutdown();
                es.awaitTermination(10, TimeUnit.DAYS);
            } catch (InterruptedException e) {

            }
            if (paired) {
                System.out.println("\rProcessed " + number_of_reads.get()*2 + " paired-end reads, " + number_of_reads.get() + " read pairs."); 
            } else {
                System.out.println("\rProcessed " + number_of_reads.get() + " single-end reads"); 
            }
           
            total_unique = total_mapped = total_unmapped = 0;
            try {
                out = new BufferedWriter(new FileWriter(OUTPUT_PATH + "/mapping_summary.txt"));
                out.write("Genome\tTotal\tUnique\tUnmapped\n");
                Pantools.logger.info("Genome\tTotal\tUnique\tUnmapped");
                for (i = 0; i < genome_numbers[0].size(); ++i) {
                    genome = genome_numbers[0].get(i);
                    total_unique += num_unique_mapping[genome].get();
                    total_mapped += num_shared_mapping[genome].get() +
                                    num_unique_mapping[genome].get();
                    total_unmapped += num_unmapped[genome].get();
                    out.write(genome + "\t" + (num_shared_mapping[genome].get() + num_unique_mapping[genome].get())
                            + "\t" + num_unique_mapping[genome].get() + "\t" + num_unmapped[genome].get() + "\n");
                    Pantools.logger.info("{}\t{}\t{}\t{}", genome, (num_shared_mapping[genome].get() + num_unique_mapping[genome].get()), num_unique_mapping[genome].get(), num_unmapped[genome].get());
                    if (sams != null) {
                        sams[genome].close();
                    }
                }
                if (ALIGNMENT_MODE < 0) { // competitive mapping
                    sams[genome_numbers[0].size()+1].close();
                    System.out.println("........................................");
                    if (paired) {
                        Pantools.logger.info("+ Reads where none of the pair mapped: {}", num_unmapped[genome_numbers[0].size()+1].get());
                    } else {
                        Pantools.logger.info("+ Unmapped reads: {}", num_unmapped[genome_numbers[0].size()+1].get());
                    }
                    total_unmapped += num_unmapped[genome_numbers[0].size()+1].get();
                    out.write("Unmapped reads:\t\t\t" + num_unmapped[genome_numbers[0].size()+1].get() + "\n");
                } 
                out.close();
            } catch (IOException ex) {
                Pantools.logger.info(ex.getMessage());
            }
            System.out.println("........................................");
            Pantools.logger.info("\t{}\t{}\t{}", total_mapped, total_unique, total_unmapped);
            Pantools.logger.info("\nNumber of hits = {}", number_of_hits);
            Pantools.logger.info("Number of alignments = {}", number_of_alignments);
            Pantools.logger.info("Mapping statitics written to: {}mapping_summary.txt", OUTPUT_PATH);
            if (OUTFORMAT.equals("BAM") || OUTFORMAT.equals("SAM")) {
                Pantools.logger.info("Output {} files written to {}", OUTFORMAT, OUTPUT_PATH);
            }
        }
    }
    
    /**
     * Read file provided via --genome-numbers or read genomes directly from command line via --reference
     * @return 
     */
    public ArrayList<Integer>[] retrieve_genomes_to_map_against() {
        ArrayList<Integer>[] genome_numbers;
        genome_numbers = new ArrayList[THREADS];
        int t, genome_nr;
        for (t = 0; t < THREADS; ++t) {
            genome_numbers[t] = new ArrayList();
        }
        for (genome_nr = 1; genome_nr <= GENOME_DB.num_genomes; genome_nr++) {
            if (!skip_list.contains(genome_nr)) {
                for (t = 0; t < THREADS; ++t) {
                    genome_numbers[t].add(genome_nr);
                }
                num_shared_mapping[genome_nr] = new AtomicLong(0);
                num_unique_mapping[genome_nr] = new AtomicLong(0);
                num_unmapped[genome_nr] = new AtomicLong(0);
            } else {
                Pantools.logger.warn("Genome {} is not found in the database.", genome_nr);
            }
        }
        if (ALIGNMENT_MODE < 0) { // when competitive mapping
            num_unmapped[genome_numbers[0].size()+1] = new AtomicLong(0);
        }
        return genome_numbers;
    }
    
    /**
     * Multiple quality checks on the data.
     * - Stops when line contains a space
     * - Stop if one of the files does not exist.
     * - Gives a warning when the first line do not only contain A T C G N 
     */
    public void verify_if_all_genome_files_exist() {
        if (PATH_TO_THE_GENOMES_FILE == null) {
            Pantools.logger.error("Please provide a genome file.");
            System.exit(1);
        }
        int line_counter = 0;
        ArrayList<Integer> not_found = new ArrayList<>();
        ArrayList<Integer> not_nucleotide = new ArrayList<>();
        StringBuilder new_genomes_file = new StringBuilder(); // KMC requires Linux formatted textfile with newlines 
        try (BufferedReader in = new BufferedReader(new FileReader(PATH_TO_THE_GENOMES_FILE))) { // first check if all files exist
            for (int c = 0; in.ready();) {
                line_counter ++; 
                if (line_counter % 100 == 0 || line_counter < 10) {
                    System.out.println("\rVerifying if all input files exist: " + line_counter);
                }
                String line = in.readLine().trim();
                new_genomes_file.append(line).append("\n");
                String[] line_array = line.split(" ");
                if (line_array.length != 1) {
                    Pantools.logger.error("Line{}: {} is not allowed to have a space character! Please include the full path to the genome fasta file.", line_counter, line);
                    System.exit(1);
                }
                boolean file_exists = check_if_file_exists(line);
                if (!file_exists) {
                    Pantools.logger.info("{} {} does not exist.", line_counter, line);
                    not_found.add(line_counter);
                    continue;
                }
                boolean correct_seq = check_if_nucleotide_sequence(line);
                if (!correct_seq) {
                    not_nucleotide.add(line_counter);
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", PATH_TO_THE_GENOMES_FILE);
            System.exit(1);
        }
        if (not_found.size() > 0) {
            System.out.println("\rThe genome files for these on these lines were not found: " + not_found.toString().replace(" ","").replace("[","").replace("]",""));
            System.exit(1);
        }
        
        if (not_nucleotide.size() > 0) {
            System.out.println("\rThe first line of the genome files were checked for A, T, C, G, N."
                    + "\nIn the following genomes, the first line contained other characters: " + not_nucleotide.toString().replace(" ","").replace("[","").replace("]","") );
        }
        write_SB_to_file_full_path(new_genomes_file, PATH_TO_THE_GENOMES_FILE); // replace input file
    }
    
    /**
     * @param fasta_file  file location
     * Reads the first line of a .fasta file to check if it's a nucleotide sequence
     * @return
     */
    public boolean check_if_nucleotide_sequence(String fasta_file) {
        boolean correct = true;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(openTextFile(fasta_file)))) {
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (line.startsWith(">")) {
                    // do nothing
                } else { 
                    int position_counter = 0;
                    for (int i=0; i < line.length(); i++) { 
                        char character = line.charAt(i);
                        char fUpper = Character.toUpperCase(character);
                        if (fUpper != 'A' && fUpper != 'T' && fUpper != 'C' && fUpper != 'G' && fUpper != 'N') {
                            correct = false;
                        }
                        position_counter ++;
                        if (position_counter > 500) { 
                            // break the loop after the first 500 positions
                            break;
                        }
                    }
                    break; // break the loop after the first line (that is not the header)
                } 
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", fasta_file);
            System.exit(1);
        }
        return correct;
    }

    /**
     * Open a text file as an input stream. If the path ends with a .gz extension, it is assumed to be gzipped. If not,
     * the file is assumed to be uncompressed.
     * @param path path to (gzip-compressed) text file to open.
     * @return input stream.
     */
    public InputStream openTextFile(String path) throws IOException {
        // TODO: use Path for clarity instead of String
        // TODO: make more robust, e.g. with library (commons compress?)

        final InputStream stream = new FileInputStream(path);
        if (path.toLowerCase().endsWith(".gz"))
            return new GZIPInputStream(stream);

        return stream;
    }
    
    /**
     * Only KMC 2.3 and 3.0 (seem to) work. Stop when using another version
     */
    public void check_kmc_version() {
        StringBuilder exe_output = new StringBuilder();
        String line = "";
        Process p;
        try {
            p = Runtime.getRuntime().exec("kmc -h");
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = reader.readLine()) != null) {
                exe_output.append(line).append("\n");
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }    
        String log_str = exe_output.toString();
        if (log_str.length() < 100) {
            Pantools.logger.error("KMC is not installed ({}).", log_str.length());
            System.exit(1);
        } 
        check_if_program_exists_stdout("kmc_tools", 100, "kmc_tools");
        String[] log_array = log_str.split("\n");
        if (!log_array[0].contains(" 3.0.") && !log_array[0].contains(" 2.3.")) {
            Pantools.logger.error("Requires KMC v2.3 or v3.0");
            System.exit(1);
        }
    }
    
    /**
     * Constructs a pangenome (gDBG) for a set of genomes.
     * build_pangenome()
     */
    public void initialize_pangenome(Path databaseDirectory, Path scratchDirectory, int numBuckets, int transactionSize,
                                     int numDbWriterThreads, int nodePropertiesCacheSize, boolean keepIntermediateFiles)
                                    throws IOException {
        Pantools.logger.info("Constructing the pangenome graph database.");
        check_kmc_version(); // check if program is set to $PATH and if appropriate version
        Node pangenome_node;
        verify_if_all_genome_files_exist();
        scratchDirectory = FileUtils.createScratchDirectory(scratchDirectory);
        GraphUtils.createPangenomeDatabase(databaseDirectory);
        INDEX_SC = new IndexScanner(INDEX_DB);
        K_SIZE = INDEX_SC.get_K();
        Pantools.logger.info("k-size = {}", K_SIZE);
        genomeSc = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_SC.get_pre_len());
        try (Transaction tx = GRAPH_DB.beginTx()) {
            pangenome_node = GRAPH_DB.createNode(PANGENOME_LABEL);
            pangenome_node.setProperty("k_mer_size", K_SIZE);
            pangenome_node.setProperty("date", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
            tx.success();
        }
        num_nodes = 0;
        num_edges = 0;
        num_bases = 0;
        num_degenerates = 0;
        construct_pangenome(pangenome_node);

        add_sequence_properties(scratchDirectory);
        try {
            highest_frequency = new LocalizeNodesParallel(
                scratchDirectory,
                numBuckets,
                transactionSize,
                numDbWriterThreads,
                nodePropertiesCacheSize,
                keepIntermediateFiles
            ).run(scratchDirectory);
        } catch (Exception e) {
            Pantools.logger.info("Error occurred during localization.");
            e.printStackTrace(System.err);
            System.exit(1);
        } finally {
            if (!keepIntermediateFiles)
                FileUtils.deleteDirectoryRecursively(scratchDirectory);
        }

        try (Transaction tx = GRAPH_DB.beginTx()) {
            pangenome_node.setProperty("k_mer_size", K_SIZE);
            pangenome_node.setProperty("num_k_mers", INDEX_SC.length());
            pangenome_node.setProperty("num_nodes", num_nodes);
            pangenome_node.setProperty("num_degenerate_nodes", num_degenerates);
            pangenome_node.setProperty("num_edges", num_edges);
            pangenome_node.setProperty("num_genomes", GENOME_DB.num_genomes);
            pangenome_node.setProperty("num_bases", num_bases);
            pangenome_node.setProperty("k_mer_highest_frequency", highest_frequency);
            tx.success();
        }
       
        genome_overview();
        Pantools.logger.info("Number of kmers:   {}", INDEX_SC.length());
        Pantools.logger.info("Number of nodes:   {}", num_nodes);
        Pantools.logger.info("Number of edges:   {}", num_edges);
        Pantools.logger.info("Number of bases:   {}", num_bases);
        Pantools.logger.info("Number of degenerate nodes:   {}", num_degenerates);
        Pantools.logger.info("graph.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + GRAPH_DATABASE_PATH)));
        Pantools.logger.info("index.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + INDEX_DATABASE_PATH)));
        Pantools.logger.info("genome.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + GENOME_DATABASE_PATH)));
    }
  
    /**
     * Adds new genomes to an available pangenome.
     */
    public void add_genomes(Path scratchDirectory) throws IOException {
        Pantools.logger.info("Adding additional genomes to an already existing pangenome.");
        check_if_program_exists_stdout("kmc -h", 100, "kmc"); // check if program is set to $PATH
        int previous_num_genomes;
        Node pangenome_node;
        verify_if_all_genome_files_exist();
        scratchDirectory = FileUtils.createScratchDirectory(scratchDirectory);
        connect_pangenome();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            if (pangenome_node == null) {
                Pantools.logger.error("Can not locate database node.");
                System.exit(1);
            }
        // Reads the properties of the pangenome
            num_nodes = (long) pangenome_node.getProperty("num_nodes");
            num_edges = (long) pangenome_node.getProperty("num_edges");
            num_degenerates = (int) pangenome_node.getProperty("num_degenerate_nodes");
            num_bases = 0;
            previous_num_genomes = (int) pangenome_node.getProperty("num_genomes");
            tx.success();
        }
        GENOME_DB.add_genomes(PATH_TO_THE_GENOMES_FILE);
        INDEX_DB = new IndexDatabase(WORKING_DIRECTORY + INDEX_DATABASE_PATH, PATH_TO_THE_GENOMES_FILE, GENOME_DB, GRAPH_DB, INDEX_DB, previous_num_genomes);
        INDEX_SC = new IndexScanner(INDEX_DB);
        K_SIZE = INDEX_SC.get_K();
        genomeSc = new SequenceScanner(GENOME_DB, previous_num_genomes + 1, 1, K_SIZE, INDEX_SC.get_pre_len());
    // the sequences should be dropped out as they will change and add_sequence_properties() function will rebuild them.
        drop_nodes_property("sequence");
        drop_nodes_property("frequencies");
        drop_nodes_property("frequency");
    // the edge colors should be dropped out as they will change and localize_nodes() function will rebuild them again.
        drop_edges_colors();
        construct_pangenome(pangenome_node);
        add_sequence_properties(scratchDirectory);
        localize_nodes();
        genome_overview();

        FileUtils.deleteDirectoryRecursively(scratchDirectory);
        Pantools.logger.info("Number of kmers:   {}", INDEX_SC.length());
        Pantools.logger.info("Number of nodes:   {}", num_nodes);
        Pantools.logger.info("Number of edges:   {}", num_edges);
        Pantools.logger.info("Number of bases:   {}", num_bases);
        Pantools.logger.info("Number of degenerate nodes:   {}", num_degenerates);
        try (Transaction tx = GRAPH_DB.beginTx()) {
            pangenome_node.setProperty("k_mer_size", K_SIZE);
            pangenome_node.setProperty("num_k_mers", INDEX_SC.length());
            pangenome_node.setProperty("num_nodes", num_nodes);
            pangenome_node.setProperty("num_degenerate_nodes", num_degenerates);
            pangenome_node.setProperty("num_edges", num_edges);
            pangenome_node.setProperty("num_genomes", GENOME_DB.num_genomes);
            pangenome_node.setProperty("num_bases", num_bases);
            tx.success();
        }
        boolean exists = true;
        int file_counter = 0;
        while (exists){
            file_counter ++;
            exists = check_if_file_exists(WORKING_DIRECTORY + "log/added_genomes_" + file_counter + ".log");
        }
        write_string_to_file_full_path("Increased the size of the database with " + (GENOME_DB.num_genomes-previous_num_genomes) + " genomes to a total of " + GENOME_DB.num_genomes,
                WORKING_DIRECTORY + "log/added_genomes_" + file_counter + ".log");

        Pantools.logger.info("graph.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + GRAPH_DATABASE_PATH)));
        Pantools.logger.info("index.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + INDEX_DATABASE_PATH)));
        Pantools.logger.info("genome.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + GENOME_DATABASE_PATH)));
    }
    
    /**
     * Retrieves a selection of genomic regions from the genome database.
     * 
     */
    public void retrieve_regions() {
        int genome_nr, sequence_nr, begin, end, num_regions = 0, proper_regions = 0;
        Pantools.logger.info("Retrieving genomic regions from the pangenome.");
        if (PATH_TO_THE_REGIONS_FILE == null) {
            Pantools.logger.error("No regions file was provided.");
            System.exit(1);
        }
       
        try {
            BufferedReader in = new BufferedReader(new FileReader(PATH_TO_THE_REGIONS_FILE));
            while (in.ready()) {
                String line = in.readLine().trim(); 
                if (line.equals("")) {
                    continue;
                }
                ++num_regions;
            }
            in.close();
        } catch (IOException e) {
            Pantools.logger.error("Failed to read: {}", PATH_TO_THE_REGIONS_FILE);
            System.exit(1);
        }
        
        connect_pangenome();
        INDEX_SC = new IndexScanner(INDEX_DB);
        K_SIZE = INDEX_SC.get_K();
        total_genomes = GENOME_DB.num_genomes;
        genomeSc = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_SC.get_pre_len());
        create_directory_in_DB("retrieval/genomes");
        create_directory_in_DB("retrieval/regions");
        int line_counter = 0;
        StringBuilder log_builder = new StringBuilder("# split on semicolon\nline number;region;recognized as;successfull?\n");
        String combi_output_file = WORKING_DIRECTORY + "retrieval/regions/regions_combined.fasta";
        try (Transaction tx = GRAPH_DB.beginTx()) {
            try (BufferedReader in = new BufferedReader(new FileReader(PATH_TO_THE_REGIONS_FILE))) {
                BufferedWriter out = new BufferedWriter(new FileWriter(combi_output_file)); // file to combine the sequences 
                while (in.ready()) {
                    String line = in.readLine().trim(); 
                    StringBuilder seq = new StringBuilder();
                    line = line.trim();
                    if (line.equals("")) {
                        continue;
                    }
                    line_counter ++;
                    String region_file = WORKING_DIRECTORY + "retrieval/regions/region_" + line_counter + ".fasta";
                    String[] fields = line.trim().split("\\s");
                    
                    log_builder.append(line_counter).append(";").append(line).append(";");
                    switch (fields.length) {
                        case 1: // a genome number is provided OR the row is not split correctly
                            log_builder.append("genome;");
                            try {
                                genome_nr = Integer.parseInt(fields[0]);
                                if (!rr_check_if_appropriate_genome(genome_nr, log_builder, line)){
                                    continue;
                                }
                                log_builder.append("yes\n");
                                proper_regions ++;
                                write_genome_file(genome_nr);
                            } catch(NumberFormatException e) {
                                log_builder.append("no, cannot be converted a number\n");
                                Pantools.logger.info("{} -> Unable to be converted to a genome number.", line);
                            }
                            break;
                        case 2:  // a genome number + sequence number is provided OR the row is not split correctly
                            log_builder.append("genome & sequence;");
                            try {
                                genome_nr = Integer.parseInt(fields[0]);
                                sequence_nr = Integer.parseInt(fields[1]);
                                if (!rr_check_if_appropriate_genome(genome_nr, log_builder, line)) {
                                    continue;
                                }
                                if (!rr_check_if_appropriate_sequence(genome_nr, sequence_nr, log_builder, line)) {
                                   continue;
                                }
                                log_builder.append("yes\n");
                                begin = 0;
                                end = (int) GENOME_DB.sequence_length[genome_nr][sequence_nr] - 1;
                                genomeSc.get_sub_sequence(seq, genome_nr, sequence_nr, begin, end - begin + 1, true);
                                out.write(">genome" + genome_nr + "_sequence" + sequence_nr + "\n");
                                write_fasta(out, seq.toString(), 80);
                                
                                BufferedWriter region_writer = new BufferedWriter(new FileWriter(region_file));
                                region_writer.write(">genome" + genome_nr + "_sequence" + sequence_nr + "\n");
                                write_fasta(region_writer, seq.toString(), 80);
                                region_writer.close();
                                proper_regions ++;
                            } catch(NumberFormatException e) {
                                Pantools.logger.info("{} -> Unable to be converted to a genome & sequence number.", line);
                            }
                            break;
                        case 4: case 5:  
                            // 4: a genome number + sequence number + region start + region end position are provided
                            // 5: all numbers AND the strand orientation is provided
                            log_builder.append("genome,sequence, start coordinate, stop coordinate");
                            if (fields.length == 5) {
                                log_builder.append(",strand");
                            }
                            log_builder.append(";");
                            try {
                                genome_nr = Integer.parseInt(fields[0]);
                                sequence_nr = Integer.parseInt(fields[1]);
                                begin = Integer.parseInt(fields[2]);
                                end = Integer.parseInt(fields[3]);
                            } catch(NumberFormatException e) {
                                Pantools.logger.info("{} -> Unable to correctly retrieve four numbers (Integers).", line);
                                continue;
                            }
                            
                            if (!rr_check_if_appropriate_genome(genome_nr, log_builder, line)) {
                                continue;
                            }
                            if (!rr_check_if_appropriate_sequence(genome_nr, sequence_nr, log_builder, line)) {
                                continue;
                            }
                            
                            long last_sequence_position = GENOME_DB.sequence_length[genome_nr][sequence_nr];
                            if (begin < 1 || begin > last_sequence_position) { 
                                Pantools.logger.info("{} ->  The start coordinate should be between 1 and {} for genome {}.", line, last_sequence_position, genome_nr);
                                log_builder.append("no, the start coordinate should be between 1 and ").append(last_sequence_position)
                                        .append(" for genome ").append(genome_nr).append("\n");
                                continue;
                            }
                            
                            if (end <= begin || end > GENOME_DB.sequence_length[genome_nr][sequence_nr]) {
                                Pantools.logger.info("{} -> The end coordinate should be between {} and {} for genome {}.", line, (begin+1), last_sequence_position, genome_nr);
                                log_builder.append("no, the end coordinate should be between ").append((begin+1)).append(" and ")
                                        .append(last_sequence_position).append(" for genome ").append(genome_nr).append("\n");
                                continue;
                            }
                            
                            log_builder.append("yes\n");
                            proper_regions ++;
                            out.write(">genome" + genome_nr + "_sequence" + sequence_nr + "_" + begin + "_" + end + "\n");
                            begin -= 1;
                            end -= 1;
                                
                            if (line.endsWith("-") || line.endsWith("rv")) { // manual only mentions '-' 
                                genomeSc.get_sub_sequence(seq, genome_nr, sequence_nr, begin, end - begin + 1, false); // reverse complement
                            } else {
                                genomeSc.get_sub_sequence(seq, genome_nr, sequence_nr, begin, end - begin + 1, true);
                            }
                            write_fasta(out, seq.toString(), 80);
                            BufferedWriter region_writer = new BufferedWriter(new FileWriter(region_file));
                            region_writer.write(">genome" + genome_nr + "_sequence" + sequence_nr + "_" + (begin+1) + "_" + (end+1) + "\n");
                            write_fasta(region_writer, seq.toString(), 80);
                            region_writer.close();    
                            break;
                      
                        default: 
                            log_builder.append("region not recognized\n");
                            break;
                    }
                }
                in.close();
                out.close();
                write_SB_to_file_in_DB(log_builder, "retrieval/regions/retrieve_regions.log");
                Pantools.logger.info("{} our of {} genomic regions found and retrieved successfully.", proper_regions, num_regions);
                Pantools.logger.info("Log written to:");
                Pantools.logger.info(" {}retrieval/regions/retrieve_regions.log", WORKING_DIRECTORY);
                Pantools.logger.info("Output written to:");
                Pantools.logger.info(" {}retrieval/", WORKING_DIRECTORY);
                Pantools.logger.info(" {}", combi_output_file);

            } catch (IOException ioe) {
                Pantools.logger.error("Failed to write: {}", combi_output_file);
                System.exit(1);
            }
            tx.success();
        }
    }
    
    /**
     * Part of the retrieve_regions function, check if the genome number is correct
     * @param genome_nr
     * @param log_builder
     * @param line
     * @return 
     */
    public boolean rr_check_if_appropriate_genome(int genome_nr, StringBuilder log_builder, String line) {
        if (genome_nr > total_genomes || genome_nr < 1) {
            Pantools.logger.info("{} -> Genome number should be between 1 and {}.", line, total_genomes);
            log_builder.append("no, genome number should be between 1 and ").append(total_genomes).append("\n");
            return false;
        }
        return true;
    }
    
    /**
     * Part of the retrieve_regions function, check if the sequence number is correct
     * @param genome_nr
     * @param sequence_nr
     * @param log_builder
     * @param line
     * @return 
     */
    public boolean rr_check_if_appropriate_sequence(int genome_nr, int sequence_nr, StringBuilder log_builder, String line) {
        int allowed_sequence_nr = GENOME_DB.num_sequences[genome_nr];
        if (sequence_nr > allowed_sequence_nr || sequence_nr < 1) {
            log_builder.append("no, the sequence number should be between 1 and ").append(allowed_sequence_nr).append(" for genome ").append(genome_nr).append("\n");
            Pantools.logger.info("{} -> Sequence number should be between 1 and {} for genome {}.", line, allowed_sequence_nr, genome_nr);
            return false;
        }
        return true;
    }
    
    /**
     * 
     * @param genome 
     */
    public void write_genome_file(int genome) {
        StringBuilder seq = new StringBuilder();
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "/retrieval/genomes/genome_" + genome + ".fasta"));
            for (int sequence = 1; sequence <= GENOME_DB.num_sequences[genome]; ++sequence) {
                out.write(">" + GENOME_DB.sequence_titles[genome][sequence] + "\n");
                int begin = 0;
                int end = (int) GENOME_DB.sequence_length[genome][sequence] - 1;
                genomeSc.get_sub_sequence(seq, genome, sequence, begin, end - begin + 1, true);
                write_fasta(out, seq.toString(), 80);
                seq.setLength(0);
            }
            out.close();
        } catch (IOException e) {
            Pantools.logger.error("Unable to write a genome to {}/retrieval/genome_{}.fasta", WORKING_DIRECTORY, genome);
            System.exit(1);
        }    
    }

    /**
     * Gives the outgoing relationship to take based on the query coordinate.
     * 
     * @param current_node The current node to go out from. 
     * @param origin The string determining query genome and sequence, e.g. "G2S3"
     * @param pos The query position.
     * @return The proper outgoing edge.
     */
    public static Relationship get_outgoing_edge(Node current_node, String origin, int pos) {
        int[] occurrence;
        for (Relationship r_out : current_node.getRelationships(Direction.OUTGOING)) {
            occurrence = (int[])r_out.getProperty(origin, null);
            if (occurrence != null) {
                if (Arrays.binarySearch(occurrence, pos) >= 0)
                    return r_out;
            }
        }
        return null;
    }
    
    /**
     * Appends substring s[from..to] to a given StringBuilder.
     * @param seq The given StringBuilder.
     * @param s The string.
     * @param from Start position in the string.
     * @param to Stop position in the string.
     * @return The length of substring appended to the StringBuilder.
     */
    public static int append_fwd(StringBuilder seq, String s, int from, int to) {
        for (int i = from; i <= to; ++i)
            seq.append(s.charAt(i));
        return to - from + 1;
    }
    
    /**
     * Appends the reverse complement of substring s[from..to] to a given StringBuilder.
     * @param seq The given StringBuilder.
     * @param s The string.
     * @param from Start position in the string.
     * @param to Stop position in the string.
     * @return The length of substring appended to the StringBuilder.
     */
    public static int append_rev(StringBuilder seq, String s, int from, int to) {
        for (int i = to; i >= from; --i)
            seq.append(complement(s.charAt(i)));
        return to - from + 1;
    }
    
    /**
     * 
     * @param address An integer array lile {genome_number, sequence_number, begin_position, end_position}
     * @return A pointer to the genomic position in the pangenome
     */
    
    /**
     * Finds a graph pointer pointing to the specified genomic coordinate in the pangenome.
     * 
     * @param graphDb The pangenome graph database
     * @param genomeSc The genome database
     * @param indexSc The index database
     * @param genome The query genome
     * @param sequence The query sequence
     * @param position The query position
     * @return The graph pointer pointing to the specified genomic coordinate.
     */
    public static IndexPointer locate(GraphDatabaseService graphDb, SequenceScanner genomeSc, IndexScanner indexSc, int genome, int sequence, int position) {
        int i, code, node_start_pos, low, high, mid , node_len, genomic_pos, k_size, pre_len;
        boolean forward, degenerate;
        Node node, neighbor, seq_node;
        Relationship rel;
        String anchor_sides, origin;
        long[] anchor_nodes;
        int[] anchor_positions;
        IndexPointer pointer;
        k_size = indexSc.get_K();
        pre_len = indexSc.get_pre_len();
        kmer first_kmer = new kmer(k_size, pre_len);
        degenerate = false;
        
        if (position <= genomeSc.get_sequence_length(genome, sequence) - k_size) {
            for (i = 0; i < k_size && !degenerate; ++i) {
                code = genomeSc.get_code(genome, sequence, position + i);
                if (code < 4)
                    first_kmer.next_fwd_kmer(code);
                else
                   degenerate = true; 
            }
            if (!degenerate) {
                pointer = new IndexPointer();
                indexSc.get_pointer(pointer, indexSc.find(first_kmer));
                Pantools.logger.trace("voor {}.", pointer);
                pointer.canonical ^= first_kmer.get_canonical();
                Pantools.logger.trace("na {}.", pointer);
                return pointer;
            }
        }
        origin = "G" + genome + "S" + sequence;
        genomic_pos = genome - 1;
        seq_node = graphDb.findNode(SEQUENCE_LABEL, "identifier", genome + "_" + sequence);
        anchor_nodes = (long[]) seq_node.getProperty("anchor_nodes");
        anchor_positions = (int[]) seq_node.getProperty("anchor_positions");
        anchor_sides = (String) seq_node.getProperty("anchor_sides");
    // Find the immediate preceding anchor_node, searching in the sorted array of anchor positions.      
        for (low = 0, high = anchor_sides.length() - 1, mid = (low + high) / 2; low <= high; mid = (low + high) / 2) {
            if (genomic_pos < anchor_positions[mid]) {
                high = mid - 1;
            } else if (genomic_pos > anchor_positions[mid]) {
                low = mid + 1;
            } else {
                break;
            }
        }
        if (genomic_pos < anchor_positions[mid]) {
            --mid;
        }
        forward = anchor_sides.charAt(mid) == 'F';

        try (Transaction tx = graphDb.beginTx()) {
            node = graphDb.getNodeById(anchor_nodes[mid]);
            node_start_pos = anchor_positions[mid];
            node_len = (int) node.getProperty("length");
        // Traverse the pangenome from the anchor node until reach to the target
            while (node_start_pos + node_len <= genomic_pos) {
                genome = node_start_pos + node_len - k_size + 1;
                rel = get_outgoing_edge(node, origin, genome);
                if (rel == null) {
                    Pantools.logger.info("Failed to locate address : {} {} {}.", genome, sequence, genome);
                    break;
                }
                neighbor = rel.getEndNode();
                forward = rel.getType().name().charAt(1) == 'F';
                node_start_pos += node_len - k_size + 1;
                node = neighbor;
                node_len = (int) node.getProperty("length");
            }
            tx.success();
        }
        return new IndexPointer(node.getId(), forward, forward ? genomic_pos - node_start_pos : node_len - 1 - (genomic_pos - node_start_pos), -1l);
    }
    
    /***
     * Creates an edge from source node to destination node.
     * 
     * @param src The source node
     * @param des The destination node
     * @param edge_type One of the four possible edge types: FF, FR, RF, RR
     */
    private void connect(Node src, Node des, RelationshipType edge_type) {
        Pantools.logger.debug("connect {} {} {}.", src.getId(), edge_type.name(), des.getId());
        src.createRelationshipTo(des, edge_type);
    }
    
    /**
     * Splits a node at a specified position by creating a new node called 
     * split_node as a part separated from the node.
     * 
     * @param node The node which should be split.
     * @param pos The split position with respect to the start of the node.
     * @return The newly created split node.
     */
    private Node split(Node node, int pos) {
        int split_len, node_len;
        int i, s_id, gen, seq, loc,starts_at;
        long inx,split_first_kmer,node_last_kmer=0;
        int[] address;
        Node neighbor, split_node;
        Relationship rel;
        address = (int[]) node.getProperty("address");
        gen = address[0];
        seq = address[1];
        loc = address[2];
        node_len = (int) node.getProperty("length");
        ++num_nodes;
        split_node = GRAPH_DB.createNode(NUCLEOTIDE_LABEL);
        address[0] = gen;
        address[1] = seq;
        address[2] = loc + pos;
        split_node.setProperty("address", address);
        split_len = node_len - pos;
        split_node.setProperty("length", split_len);
       
        // Updates the 'start' edges comming from the gene node to the nucleotide node.
        Iterable<Relationship> relations = node.getRelationships(RelTypes.starts, Direction.INCOMING);
        for (Relationship r : relations) {
            if (!r.isType(RelTypes.starts)) {
                Pantools.logger.error("This is NOT a 'starts' relationship.");
                System.exit(1);
            }
            starts_at = (int)r.getProperty("offset");
            if (starts_at >= pos) {
                rel = r.getStartNode().createRelationshipTo(split_node, RelTypes.starts);
                rel.setProperty("offset", starts_at - pos);
                rel.setProperty("forward", r.getProperty("forward"));
                rel.setProperty("genomic_position", r.getProperty("genomic_position"));
                r.delete();
            } 
        }     
        
        // Updating the Kmers chain in the index  
        node_last_kmer = INDEX_SC.find(genomeSc.make_kmer(gen,seq,loc+pos-1));
        split_first_kmer = INDEX_SC.get_next_index(node_last_kmer);
        INDEX_SC.put_next_index(-1L, node_last_kmer);
        split_node.setProperty("first_kmer",split_first_kmer);
        split_node.setProperty("last_kmer",node.getProperty("last_kmer"));
        s_id=(int)split_node.getId();
        for(i=0,inx=split_first_kmer;inx!=-1L;inx= INDEX_SC.get_next_index(inx),++i) // update kmer coordinates
        {
            INDEX_SC.put_node_id(s_id, inx);
            INDEX_SC.put_offset(i, inx);
        }  
        // Moving forward-outgoing and reverse-incoming edges from node to split node.    
        for (Relationship r : node.getRelationships(Direction.OUTGOING, RelTypes.FR, RelTypes.FF)) {
            neighbor = r.getEndNode();
            if (neighbor.equals(node)) 
                neighbor = r.isType(RelTypes.FF) ? node : split_node;
            connect(split_node, neighbor, r.getType());
            r.delete();
        }
        for (Relationship r : node.getRelationships(Direction.INCOMING,RelTypes.RR,RelTypes.FR)) {
            neighbor = r.getStartNode();
            if (neighbor.equals(node)) 
                neighbor = r.isType(RelTypes.RR) ? node : split_node;
            connect(neighbor, split_node, r.getType());
            r.delete();
        }
    //  Connecting node to split node
        if (node.hasRelationship(Direction.INCOMING, RelTypes.FF, RelTypes.RF)) {
            connect(node, split_node, RelTypes.FF);
            ++num_edges;
        }
        if (split_node.hasRelationship(Direction.INCOMING, RelTypes.FR, RelTypes.RR)) {
            connect(split_node ,node, RelTypes.RR);
            ++num_edges;
        }
        node.setProperty("last_kmer",node_last_kmer);
        node.setProperty("length", pos + K_SIZE - 1);
        return split_node;
    }
    
    /**
     * Creates and extends a new node till reaching to a visited K-mer or a degenerate region.
     */
    private void create_extend() {
        int[] address;
        long node_id,last_kmer;
        address = genomeSc.get_address();
        address[2] -= K_SIZE - 1;
        ++num_nodes;
        Node new_node = GRAPH_DB.createNode(NUCLEOTIDE_LABEL);
        node_id = new_node.getId();
        Pantools.logger.trace("create {}.", new_node.getId());
        new_node.setProperty("address", address);
        new_node.setProperty("length", K_SIZE);
        new_node.setProperty("last_kmer",genomeSc.get_curr_index());
        new_node.setProperty("first_kmer",genomeSc.get_curr_index());
    // Set the pointer to the Kmer in the pointer database    
        INDEX_SC.put_pointer(node_id, 0, genomeSc.get_curr_kmer().get_canonical(), -1l, genomeSc.get_curr_index());
        connect(curr_node ,new_node, RelTypes.values()[curr_side*2]);
        ++num_edges;
        curr_node = new_node;
        curr_side = 0;

        Pantools.logger.trace("extending node {}.", curr_node.getId());
        int begin, len;
        last_kmer=(long)curr_node.getProperty("last_kmer");
        boolean broke;
        Node degenerate_node = null;
        len = (int) curr_node.getProperty("length");
        broke = false;
        while (genomeSc.get_position() < genomeSc.get_sequence_length() - 1) { // Not reached to the end of the sequence
            Pantools.logger.trace("extend {}.", genomeSc.get_position());
            if (genomeSc.get_code(1) > 3) { // hit a degenerate region
                genomeSc.next_position();
                begin = genomeSc.get_position() - K_SIZE + 1;
                curr_node.setProperty("length", len);
                curr_node.setProperty("last_kmer",last_kmer);                
                genomeSc.jump_forward();
                if (genomeSc.get_position() >= genomeSc.get_sequence_length() - 1) {
                    genomeSc.next_position();// to acheive the right length for the degenerate node    
                    finish = true;
                }                
                int[] add = genomeSc.get_address();
                add[2] = begin;
                degenerate_node = create_degenerate(add);
                connect(curr_node ,degenerate_node, RelTypes.FF); 
                ++num_edges;
                curr_node = degenerate_node;
                break;
            } else {
                genomeSc.next_position();
                genomeSc.get_curr_kmer().next_fwd_kmer(genomeSc.get_code(0));
                if (SHOW_KMERS) Pantools.logger.info(genomeSc.get_curr_kmer().toString());
            }
            genomeSc.set_curr_index(INDEX_SC.find(genomeSc.get_curr_kmer()));
            if (INDEX_SC.is_empty(genomeSc.get_curr_index())) {
                INDEX_SC.put_next_index(genomeSc.get_curr_index(),last_kmer);
                ++len;
                INDEX_SC.put_pointer(node_id, len - K_SIZE, genomeSc.get_curr_kmer().get_canonical(), -1l, genomeSc.get_curr_index());
                last_kmer=genomeSc.get_curr_index();
            } else {
                broke = true;
                break;
            }
        }
        if (degenerate_node == null) {
            new_node.setProperty("length", len);
            new_node.setProperty("last_kmer",last_kmer);
        }
        if (!broke && genomeSc.get_position() >= genomeSc.get_sequence_length() - 1) {// because the last kmer is somewhere in the graph we should get connected to
            finish = true;
        }
    }

    /**
     * Finds, splits and follows (in forward direction) a node pointed by a 
     * pointer till the first k-mer different to what is observed in the scanned sequence.
     * 
     * @param pointer The pointer 
     */
    private void follow_forward(IndexPointer pointer) {
        int l, pos, begin, g, s, loc, side;
        Node node, split_node1, split_node2,des, src, degenerate_node = null;
        RelationshipType rel_type;
        int[] address;
        boolean loop, repeated_edge;
        pos = pointer.offset;
        node = GRAPH_DB.getNodeById(pointer.node_id);
        Pantools.logger.trace("follow_forward {} at {}.", pointer.node_id, pos);
    // The first split might be done to seperate the part we need to enter in.
        if (pos > 0) {  
            Pantools.logger.trace("first_split {} at {}.", node.getId(), pos);
            split_node1 = split(node, pos);
            if (loop = (curr_node.equals(node) && curr_side == 0)) 
                src = split_node1;
            else 
                src = curr_node;
            node = split_node1; // Note : assigning reference variables is dangerous! if split_node changes node will change as well.
        } else {
            split_node1 = node;
            if (loop = (curr_node.equals(node) && curr_side == 0)) 
                src = split_node1;
            else 
                src = curr_node;
        }
        des = split_node1;
        side=curr_side*2;
        curr_side = 0;
        l = (int) node.getProperty("length") - K_SIZE;
        address = (int[]) node.getProperty("address");
        g = address[0];
        s = address[1];
        loc = address[2];
    // Follow the shared part
        for (pos = 0; pos <= l && genomeSc.get_position() <= genomeSc.get_sequence_length() - 1 && genomeSc.get_code(g, s, loc + pos + K_SIZE - 1) == genomeSc.get_code(0); ++pos) {
            genomeSc.next_position();
         // If hit a degenarate region aplit and branch to a degenerate node 
            if (genomeSc.get_position() <= genomeSc.get_sequence_length() - 1 && genomeSc.get_code(0) > 3) {
                begin = genomeSc.get_position() - K_SIZE + 1;
                genomeSc.jump_forward();
                if (genomeSc.get_position() >= genomeSc.get_sequence_length() - 1) {
                    genomeSc.next_position();// to acheive the right length for the degenerate node    
                    finish = true;
                }                
                if (pos + 1 <= l) {
                    split_node2 = split(node, pos + 1);
                    if (loop)
                        src = split_node2;
                }
                int[] add = genomeSc.get_address();
                add[2] = begin;
                degenerate_node = create_degenerate(add);
                connect(node ,degenerate_node, RelTypes.FF);
                ++num_edges;
                break;
            }
        }
        if (genomeSc.get_position() == genomeSc.get_sequence_length()) {
            finish = true;
        } else if (degenerate_node == null) // build the Kmer of difference 
            initialize(genomeSc.get_position() - K_SIZE + 1);
    //  A second split might be needed   
        if (degenerate_node == null && pos <= l) {
            Pantools.logger.trace("second_split {} at {}.", node.getId(), pos);
            split_node2 = split(node, pos);
            if (loop)
                src = split_node2;
        }
    // connect the current node before doing splits to the split_node1    
        rel_type = RelTypes.values()[side];
        repeated_edge = false;
        for (Relationship r: src.getRelationships(rel_type, Direction.OUTGOING))
            if (r.getEndNode().equals(des)) {
                repeated_edge = true;
                break;
            }
        if (!repeated_edge) {
            connect(src ,des, rel_type);
            ++num_edges;
        }
        if (degenerate_node != null) {
            curr_side = 0; // not really needed
            curr_node = degenerate_node;
        } else
            curr_node = node;
    }
    
    /**
     * Finds, splits and follows (in reverse direction) a node pointed by a 
     * pointer till the first k-mer different to what is observed in the scanned sequence.
     * 
     * @param pointer The pointer 
     */
    private void follow_reverse(IndexPointer pointer) {
        int pos, begin, g, s, loc, side;
        int[] address;
        Node node, split_node1, split_node2 ,des, src, degenerate_node = null;
        boolean loop, first_split = false, repeated_edge;
        pos = pointer.offset;
        node = GRAPH_DB.getNodeById(pointer.node_id);
        Pantools.logger.trace("follow_reverse {} at {}.", pointer.node_id, pos);
        RelationshipType rel_type;
        split_node2 = node; //if the second split does not happens remains unchanged
        if (pos < (int) node.getProperty("length") - K_SIZE) {
            Pantools.logger.trace("first_split {} at {}.", node.getId(), (pos+1));
            first_split = true;
            split_node1 = split(node, pos+1);
            if (loop = curr_node.equals(node) && curr_side == 0) // might be in reverse side due to a follow reverse
                src = split_node1;
            else 
                src = curr_node;
        } else {
            split_node1 = node;
            if (loop = curr_node.equals(node) && curr_side == 0)
                src = split_node1;
            else 
                src = curr_node;
        }
        des = node;
        side=curr_side*2+1;
        curr_side = 1;
        address = (int[]) node.getProperty("address");
        g = address[0];
        s = address[1];
        loc = address[2];
        for (pos = (int) node.getProperty("length") - K_SIZE; pos >= 0 && genomeSc.get_position() <= genomeSc.get_sequence_length() - 1 && genomeSc.get_code(g, s, loc + pos) == genomeSc.get_complement_code(0); --pos) {
            genomeSc.next_position();
            if (genomeSc.get_position() <= genomeSc.get_sequence_length() - 1 && genomeSc.get_code(0) > 3) {
                begin = genomeSc.get_position() - K_SIZE + 1;
                genomeSc.jump_forward();
                if (genomeSc.get_position() >= genomeSc.get_sequence_length() - 1) {
                    genomeSc.next_position();// to acheive the right length for the degenerate node    
                    finish = true;
                }                
                if (pos > 0) {
                    split_node2 = split(node, pos);
                    des = split_node2;
                    if (!first_split && loop)
                        src = split_node2;
                }
                int[] add = genomeSc.get_address();
                add[2] = begin;
                degenerate_node = create_degenerate(add);
                connect(split_node2, degenerate_node, RelTypes.RF);
                ++num_edges;
                break;
            }
        }
        if (genomeSc.get_position() == genomeSc.get_sequence_length()) {
            finish = true;
        } else if (degenerate_node == null) // build the Kmer of difference 
            initialize(genomeSc.get_position() - K_SIZE + 1);
        if (degenerate_node == null && pos >= 0) {
            Pantools.logger.trace("second_split {} at {}.", node.getId(), (pos + 1));
            split_node2 = split(node, pos+1);
            des = split_node2;
            if (!first_split && loop)
                src = split_node2;
        }
        rel_type = RelTypes.values()[side];
        repeated_edge = false;
        for (Relationship r: src.getRelationships(rel_type, Direction.OUTGOING))
            if (r.getEndNode().equals(des)) {
                repeated_edge = true;
                break;
            }
        if (!repeated_edge) {
            connect(src ,des, rel_type);
            ++num_edges;
        }
        if (degenerate_node != null) {
            curr_side = 0;
            curr_node = degenerate_node;
        } else
            curr_node = split_node2;
    }

    /**
     * Creates a degenerate node with a given address {genome, sequence, position}.
     * 
     * @param address The genomic position of the degenerate region
     */
    private Node create_degenerate(int[] address) {
        ++num_degenerates;
        ++num_nodes;
        Node degenerate_node = GRAPH_DB.createNode(DEGENERATE_LABEL);
        degenerate_node.addLabel(NUCLEOTIDE_LABEL);
        Pantools.logger.trace("create_degenerate:{} position:{} begin:{}.", degenerate_node.getId(), genomeSc.get_position(), address[2]);
        degenerate_node.setProperty("address", address);
        degenerate_node.setProperty("length", genomeSc.get_position() - address[2]);
        return degenerate_node;
    }
    
    /**
     * Initializes the first kmer of the current sequence which can be possibly
     * located immediately after a degenerate region. 
     * 
     * @param start The start position.
     */
    private void initialize(int start) {
        Node degenerate_node;
        if (genomeSc.initialize_kmer(start) < 0) {// start with a degenerate kmer
            if (genomeSc.get_position() >= genomeSc.get_sequence_length() - 1) {
                genomeSc.next_position();// to acheive the right length for the degenerate node    
                finish = true;
            }
            int[] add = genomeSc.get_address();
            add[2] = 0;
            degenerate_node = create_degenerate(add);
            connect(curr_node ,degenerate_node, RelTypes.values()[curr_side*2]);
            ++num_edges;
            curr_node = degenerate_node;
        }
    }
    
    /**
     * Constructs the pangenome starting from the pangenome node.
     * .
     * @param pangenome_node The pangenome node 
     */
    void construct_pangenome(Node pangenome_node) {
        int trsc = 0;
        Node genome_node, sequence_node;
        IndexPointer pointer = new IndexPointer();
        phaseTime = System.currentTimeMillis();
        Transaction tx = GRAPH_DB.beginTx();
        try {
            while (!genomeSc.end_of_scan()) {
                Pantools.logger.info("Processing genome {}, {}.", genomeSc.get_genome(), GENOME_DB.genome_names[genomeSc.get_genome()]);
                genome_node = GRAPH_DB.createNode(GENOME_LABEL);
                genome_node.setProperty("path", GENOME_DB.genome_names[genomeSc.get_genome()]);
                genome_node.setProperty("number", genomeSc.get_genome());
                genome_node.setProperty("num_sequences", GENOME_DB.num_sequences[genomeSc.get_genome()]);
                genome_node.setProperty("date", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
                pangenome_node.createRelationshipTo(genome_node, RelTypes.has);
                while (!genomeSc.end_of_genome()) {
                    sequence_node = GRAPH_DB.createNode(SEQUENCE_LABEL);
                    sequence_node.setProperty("genome", genomeSc.get_genome());
                    sequence_node.setProperty("number", genomeSc.get_sequence());
                    sequence_node.setProperty("identifier", genomeSc.get_genome() + "_" + genomeSc.get_sequence());
                    sequence_node.setProperty("title", genomeSc.get_title());
                    sequence_node.setProperty("length", genomeSc.get_sequence_length());
                    sequence_node.setProperty("offset", genomeSc.get_offset());
                    genome_node.createRelationshipTo(sequence_node, RelTypes.has);
                    finish = false;
                    System.out.print("\rProcessing sequence " + genomeSc.get_sequence() + "/" + GENOME_DB.num_sequences[genomeSc.get_genome()] +
                            " of genome " + genomeSc.get_genome() + "\tlength=" + genomeSc.get_sequence_length() + "       ");
                    curr_node = sequence_node;
                    curr_side = 0;
                    //long last_printed_position = 0;
                    if (genomeSc.get_sequence_length() >= K_SIZE) {
                        initialize(0);
                        while (!finish) {
                            genomeSc.set_curr_index(INDEX_SC.find(genomeSc.get_curr_kmer()));
                            INDEX_SC.get_pointer(pointer, genomeSc.get_curr_index());
                            /*if (genomeSc.get_position() > last_printed_position) {
                                System.out.print("\rProcessing sequence " + genomeSc.get_sequence() + "/" + genomeDb.num_sequences[genomeSc.get_genome()] + 
                                    " of genome " + genomeSc.get_genome() + "\tlength=" + genomeSc.get_sequence_length() );
                                last_printed_position += 1000000;
                            }*/
                            if (pointer.node_id == -1L) // kmer is new
                                create_extend();
                            else if (genomeSc.get_curr_kmer().get_canonical() ^ pointer.canonical)// if sides don't agree
                                follow_reverse(pointer);
                            else 
                                follow_forward(pointer);
                            ++trsc;
                            if (trsc >= MAX_TRANSACTION_SIZE) {    
                                tx.success();
                                tx.close();
                                tx = GRAPH_DB.beginTx();
                                trsc = 0;
                            }
                        }
                        connect(curr_node, sequence_node, RelTypes.values()[curr_side*2]);// to point to the last k-mer of the sequence located in the other strand
                        ++num_edges;
                    }
                    System.out.println("\rProcessing sequence " + genomeSc.get_sequence() + "/" + GENOME_DB.num_sequences[genomeSc.get_genome()] +
                            " of genome " + genomeSc.get_genome() + "\tlength=" + genomeSc.get_sequence_length() + "                                            " );
                    genomeSc.next_sequence();
                }//sequences
                System.out.println("\r" + (System.currentTimeMillis() - phaseTime) / 1000 + " seconds elapsed.                                   ");
                genomeSc.next_genome(); 
            }//genomes
            tx.success();
        } finally {
            tx.close();
        }
    }
    
    /**
     * Traces each sequence in the pangenome and adds coordinate information 
     * to the edges and list of anchors information to sequence nodes.
     */
    void localize_nodes() {
        ResourceIterator<Node> sequence_iterator;
        LinkedList<Node> sequence_nodes;
        int trsc = 0, i, len, m, neighbor_length = 0;
        long[] frequencies;
        long frequency;
        char node_side, neighbor_side;
        long length, distance;
        long[] anchor_nodes;
        int[] anchor_positions;
        int[] initial_coordinate = new int[1];
        Node node, neighbor = null, sequence_node;
        StringBuilder nds = new StringBuilder();
        StringBuilder pos = new StringBuilder();
        StringBuilder sds = new StringBuilder();
        String[] ids_list, posis_list;
        String rel_name,origin;
        int[] positions;
        int[] new_positions;
        int[] address = new int[3], addr = null;
        boolean is_node = false, is_degenerate = false, found = true;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            sequence_iterator = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            sequence_nodes = new LinkedList();
            while (sequence_iterator.hasNext())
                sequence_nodes.add(sequence_iterator.next());
            sequence_iterator.close();
            tx.success();
        }
        Transaction tx = GRAPH_DB.beginTx();
        try {
            while (!sequence_nodes.isEmpty()) {
                sequence_node = sequence_nodes.remove();
                origin = "G" + ((String)sequence_node.getProperty("identifier")).replace('_', 'S');
                address[0] = (int)sequence_node.getProperty("genome");
                address[1] = (int)sequence_node.getProperty("number");
                length = (long) sequence_node.getProperty("length");
                System.out.print("\rLocalizing sequence " + address[1] + "/" + GENOME_DB.num_sequences[address[0]] + " of genome " + address[0] + "                        ");
                if (length >= K_SIZE) {
                    node = sequence_node;
                    node_side = 'F';
                    distance = 0;
                    for (address[2] = 0; address[2] + K_SIZE - 1 < length && found;) { // K-1 bases of the last node not added
                        found = false;
                        for (Relationship r : node.getRelationships(Direction.OUTGOING)) {
                            rel_name = r.getType().name();
                            if (rel_name.charAt(0) != node_side)
                                continue;
                            neighbor = r.getEndNode();
                            neighbor_side = rel_name.charAt(1);
                            is_node = neighbor.hasLabel(NUCLEOTIDE_LABEL) && !neighbor.hasLabel(DEGENERATE_LABEL);
                            is_degenerate = neighbor.hasLabel(DEGENERATE_LABEL);
                            if (is_node || is_degenerate) {
                                addr = (int[]) neighbor.getProperty("address");
                                neighbor_length = (int) neighbor.getProperty("length");
                            }
                           
                            if ((is_node && genomeSc.compare(address, addr, K_SIZE - 1,
                                 neighbor_side == 'F' ? K_SIZE - 1 : neighbor_length - K_SIZE, 1, neighbor_side == 'F'))
                            || (is_degenerate && Arrays.equals(addr, address))) {
                                found = true;
                                positions = (int[]) r.getProperty(origin, null);
                                if (positions != null) {
                                    len = positions.length;
                                    new_positions = new int[len + 1];
                                    for (i = 0; i < len; ++i) {
                                        new_positions[i] = positions[i];
                                    }
                                    new_positions[i] = address[2];
                                    r.setProperty(origin, new_positions);
                                } else {
                                    initial_coordinate[0] = address[2];
                                    r.setProperty(origin, initial_coordinate);
                                }
                                frequencies = (long[])neighbor.getProperty("frequencies", new long[GENOME_DB.num_genomes + 1]);
                                ++frequencies[address[0]];
                                neighbor.setProperty("frequencies", frequencies);
                                frequency = (long)neighbor.getProperty("frequency", 0l);
                                ++frequency;
                                if (frequency > highest_frequency) {
                                    highest_frequency = frequency;
                                }
                                if (frequency >= GENOME_DB.num_genomes * 100) {
                                    if (!neighbor.hasProperty("high_frequency")) {
                                        neighbor.setProperty("high_frequency", true);
                                    }
                                }
                                neighbor.setProperty("frequency", frequency);
                                if (address[2] >= distance) {
                                    nds.append(neighbor.getId()).append(" ");
                                    sds.append(neighbor_side);
                                    pos.append(address[2]).append(" ");
                                    distance += ANCHORS_DISTANCE;
                                }
                                address[2] = address[2] + neighbor_length - K_SIZE + 1;
                                node = neighbor;
                                node_side = neighbor_side;
                                break;
                            }
                        }
                        ++trsc;
                        if (trsc >= 1000 * MAX_TRANSACTION_SIZE) {
                            tx.success();
                            tx.close();
                            tx = GRAPH_DB.beginTx();
                            trsc = 0;
                        }
                    }
                    if (!found) {
                        Pantools.logger.error("Could not locate position {} from node ID={}.", address[2], node.getId());
                        System.exit(1);
                    }
                    m = sds.length();
                    ids_list = nds.toString().split("\\s");
                    posis_list = pos.toString().split("\\s");
                    anchor_nodes = new long[m];
                    anchor_positions = new int[m];
                    for (i = 0; i < m; ++i) {
                        anchor_nodes[i] = Long.valueOf(ids_list[i]);
                        anchor_positions[i] = Integer.valueOf(posis_list[i]);
                    }
                    sequence_node.setProperty("anchor_nodes", anchor_nodes);
                    sequence_node.setProperty("anchor_positions", anchor_positions);
                    sequence_node.setProperty("anchor_sides", sds.toString());
                    nds.setLength(0);
                    pos.setLength(0);
                    sds.setLength(0);
                }
            }//while
            Pantools.logger.info("\n{} seconds elapsed.", (System.currentTimeMillis() - phaseTime) / 1000);
            tx.success();
        } finally {
           tx.close();
        }
        System.out.println();
    }
     
    /**
     * Extracts the sequence of the nodes from the genome database and store it 
     * in the nodes.
     */
    void add_sequence_properties(Path scratchDirectory) throws IOException {
        final long start = System.currentTimeMillis();
        // TODO: store IDs more efficiently, or find another way to stream in IDs without caching the whole list
        final Path nucleotideNodeIdsFile = scratchDirectory.resolve(NUCLEOTIDE_NODE_IDS_FILE_NAME);
        long numNodes = 0;
        try (Transaction ignored = GRAPH_DB.beginTx();
             BufferedWriter writer = Files.newBufferedWriter(nucleotideNodeIdsFile);
             ResourceIterator<Node> itr = GRAPH_DB.findNodes(NUCLEOTIDE_LABEL)) {
            while (itr.hasNext()) {
                writer.write(Long.toString(itr.next().getId()));
                writer.write('\n');
                numNodes++;
            }
        }

        // Read back node IDs, chunk them up into transactions and write the sequences

        try (BufferedReader reader = Files.newBufferedReader(nucleotideNodeIdsFile)) {
            for (long i = 0; i < numNodes; i += MAX_TRANSACTION_SIZE) {
                // Read chunks of max. transaction size
                try (Transaction tx = GRAPH_DB.beginTx()) {
                    for (long j = 0; j < Math.min(MAX_TRANSACTION_SIZE, numNodes - i); j++) {
                        final long id = Long.parseLong(reader.readLine());
                        final Node node = GRAPH_DB.getNodeById(id);
                        final int[] address = (int[]) node.getProperty("address");
                        final int length = (int) node.getProperty("length");
                        num_bases += length;
                        final StringBuilder sequence = new StringBuilder();
                        genomeSc.get_sub_sequence(sequence, address[0], address[1], address[2], length, true);
                        node.setProperty("sequence", sequence.toString());
                    }

                    tx.success();
                }
            }
        }

        // Clean up

        Files.delete(nucleotideNodeIdsFile);
        Pantools.logger.info("Finished adding sequence properties phase in {} ms.", System.currentTimeMillis() - start);
    }

    /**
     * Removes the given property from all the nucleotide and degenerate nodes.
     * 
     * @param property 
     */    
    void drop_nodes_property(String property) {
        int i;
        ResourceIterator<Node> nodes_iterator;
        LinkedList<Node> nodes = new LinkedList();
        Node node;
        try(Transaction tx = GRAPH_DB.beginTx()) {
            nodes_iterator = GRAPH_DB.findNodes(NUCLEOTIDE_LABEL);
            while (nodes_iterator.hasNext()) {
                nodes.add(nodes_iterator.next());
            }
            tx.success();
            nodes_iterator.close();
        }
        while (!nodes.isEmpty()) {
            try (Transaction tx = GRAPH_DB.beginTx()) {
                for (i = 0; i < MAX_TRANSACTION_SIZE && !nodes.isEmpty(); ++i) {
                    node = nodes.remove();
                    node.removeProperty(property);
                }
                tx.success();
            }
        }
    }

    /**
     * Removes the coordinate information from the edges.
     */    
    void drop_edges_colors() {
        int i;
        ResourceIterator<Relationship> rels;
        Relationship r;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            rels = GRAPH_DB.getAllRelationships().iterator();
            tx.success();
        }
        while (rels.hasNext()) {
            try (Transaction tx = GRAPH_DB.beginTx()) {
                for (i = 0; i < MAX_TRANSACTION_SIZE && rels.hasNext(); ++i) {
                    r = rels.next();
                    if (r.isType(RelTypes.FF) || r.isType(RelTypes.FR) || r.isType(RelTypes.RF) || r.isType(RelTypes.RR))
                        for(String p:r.getPropertyKeys())
                            r.removeProperty(p);
                }
                tx.success();
            }
        }
        rels.close();
    }
}
