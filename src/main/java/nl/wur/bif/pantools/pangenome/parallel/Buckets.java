package nl.wur.bif.pantools.pangenome.parallel;

import com.esotericsoftware.kryo.kryo5.Kryo;
import com.esotericsoftware.kryo.kryo5.io.Output;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static nl.wur.bif.pantools.pangenome.parallel.KryoUtils.getKryo;

/**
 * Convenience class for opening and automatically closing (through {@link AutoCloseable}) Kryo output files (buckets).
 * Each localization object is hashed to a key, and written to the bucket with index equal to the hash key % #buckets.
 */
public class Buckets implements AutoCloseable {
    private final List<Path> paths;
    private final List<Output> outputs;
    private final int numBuckets;
    private final Kryo kryo;
    private long numLocalizations;

    public Buckets(Path outputDirectory, int numBuckets) throws IOException {
        paths = new ArrayList<>(numBuckets);
        outputs = new ArrayList<>(numBuckets);
        this.numBuckets = numBuckets;
        // TODO: pass in Kryo object?
        kryo = getKryo();
        numLocalizations = 0;

        for (int i = 0; i < numBuckets; i++) {
            final Path path = outputDirectory.resolve(String.format("bucket-%05d.kryo", i));
            // TODO: make compression format and level configurable
            paths.add(path);
            outputs.add(KryoUtils.createOutput(path, 1));
        }
    }

    public void write(Localization localization, Function<Localization, Long> hasher) {
        final long key = hasher.apply(localization);
        final int bucketIndex = (int) (key % (long) numBuckets);
        kryo.writeObject(outputs.get(bucketIndex), localization);
        numLocalizations++;
    }

    public long getNumLocalizations() {
        return numLocalizations;
    }

    public List<Path> getPaths() {
        return paths;
    }

    @Override
    public void close() throws Exception {
        for (Output output : outputs) {
            output.close();
        }
    }
}
