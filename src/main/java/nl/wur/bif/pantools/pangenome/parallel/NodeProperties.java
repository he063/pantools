package nl.wur.bif.pantools.pangenome.parallel;

import java.util.Arrays;

/**
 * Class for storing length and address of a nucleotide or degenerate node, used during localization phase. Address is
 * given as an integer array of [genome index, sequence index, base pair offset].
 */
public class NodeProperties {
    private final int[] address;
    private final int length;

    public NodeProperties(int[] address, int length) {
        this.address = address;
        this.length = length;
    }

    public int[] getAddress() {
        return address;
    }

    public int getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "NodeProperties{" +
            "address=" + Arrays.toString(address) +
            ", length=" + length +
            '}';
    }
}
