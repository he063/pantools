package nl.wur.bif.pantools.pangenome.parallel;

import com.esotericsoftware.kryo.kryo5.Kryo;
import com.esotericsoftware.kryo.kryo5.io.Input;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.logging.log4j.Logger;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static nl.wur.bif.pantools.pangenome.parallel.KryoUtils.getKryo;
import static nl.wur.bif.pantools.utils.Globals.GRAPH_DB;

/**
 * Task for writing out localization information to relationships in the form of zero or more properties that store the
 * base pair offsets for a sequence. This task reads a list of {@link Localization} items from a file, groups them by
 * relationship ID, genome index and sequence index and aggregates the sorted base pair offsets before writing them out
 * to a relationship property.
 */
public class LocalizationsWriterTask implements Callable<Long> {
    private final Path path;
    private final Logger log;
    private final long maximumBatchSize;

    public LocalizationsWriterTask(Path path, Logger log, long maximumTransactionSize) {
        this.path = path;
        this.log = log;
        this.maximumBatchSize = maximumTransactionSize;
    }

    @Override
    public Long call() throws Exception {
        //noinspection resource
        final Transaction[] tx = {GRAPH_DB.beginTx()};

        AtomicLong counter = new AtomicLong(0);
        try {
            final List<Localization> localizations = loadLocalizations(path, getKryo());
            log.debug(String.format("Loaded %,d localizations from file %s", localizations.size(), path));

            if (localizations.isEmpty())
                return 0L;

            // TODO: optimize memory by looping
            // TODO: substitute triple with separate type
            final Map<Triple<Long, Integer, Integer>, List<Localization>> grouped = localizations
                .stream()
                .collect(Collectors.groupingBy(l -> Triple.of(l.getRelationshipId(), l.getGenomeIndex(), l.getSequenceIndex())));

            grouped.forEach((key, ls) -> {
                // TODO: how memory-efficient is this?
                final int[] offsets = ls.stream().mapToInt(Localization::getOffset).toArray();
                Arrays.sort(offsets);

                final Relationship relationship = GRAPH_DB.getRelationshipById(key.getLeft());
                // TODO: genome-sequence identifier to custom type
                final String propertyName = "G" + key.getMiddle() + "S" + key.getRight();
                relationship.setProperty(propertyName, offsets);

                counter.getAndIncrement();
                if (counter.get() % maximumBatchSize == 0) {
                    log.trace(String.format("Committing transaction of size %,d", maximumBatchSize));
                    tx[0].success();
                    tx[0].close();
                    //noinspection resource
                    tx[0] = GRAPH_DB.beginTx();
                }
            });
        } catch (IOException e) {
            throw new RuntimeException("error creating Kryo input for path " + path + ": " + e);
        } finally {
            // TODO: debug message for last transaction
            tx[0].success();
            tx[0].close();
        }
        log.debug(String.format("Written %,d properties for bucket %s", counter.get(), path));

        // TODO: return more involved metrics
        return counter.get();
    }

    // TODO: remove duplicate code
    public List<Localization> loadLocalizations(Path path, Kryo kryo) throws IOException {
        final List<Localization> updates = new ArrayList<>();

        try (final Input input = KryoUtils.createInput(path)) {
            while (!input.end())
                updates.add(kryo.readObject(input, Localization.class));
        }

        return updates;
    }
}
