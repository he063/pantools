package nl.wur.bif.pantools.pangenome;

import nl.wur.bif.pantools.index.IndexScanner;
import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.sequence.SequenceScanner;
import nl.wur.bif.pantools.utils.FileUtils;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static nl.wur.bif.pantools.alignment.BoundedLocalSequenceAlignment.match;
import static nl.wur.bif.pantools.pangenome.create_skip_arrays.create_skip_arrays;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

public class MultipleSequenceAlignment {
    private Path outDirMsa;
    private final String msaMethod; // can be "per_group", "multiple_groups", "functions", "regions"
    private String alignmentInputType; // can be "groups", "regions" or "functions"
    private final boolean alignProtein;
    private final boolean alignNucleotide;
    private final boolean alignVariants;
    private final String alignmentType; // can be "protein", "nucleotide" or "variants"
    private final String alignmentTypeShort; // can be "prot", "nuc" or "var"
    private final String alignmentTypeCapital; // can be "Protein", "Nucleotide" or "Variants"
    private final boolean pavs;
    private final List<Long> hmGroups;
    private final Set<String> excludedGroups;
    private ArrayList<Node> hmNodeList;
    private ArrayList<String> msaNames;

    /**
     *
     * @param msaMethod can be: "per_group", "multiple_groups", "functions", "regions"
     * @param alignNucleotide whether to align nucleotide sequences
     * @param alignProtein whether to align protein sequences
     * @param alignVariants whether to use VCF information
     * @param pavs whether to use PAV information
     * @param hmGroups an array with homology groups
     */
    public MultipleSequenceAlignment(String msaMethod, boolean alignNucleotide, boolean alignProtein, boolean alignVariants, boolean pavs, List<Long> hmGroups) {

        this.msaMethod = msaMethod;
        this.alignNucleotide = alignNucleotide;
        this.alignProtein = alignProtein;
        this.alignVariants = alignVariants;
        this.pavs = pavs;
        this.hmGroups = hmGroups;
        this.excludedGroups = new HashSet<>();

        // set alignmentTypeShort
        if (alignProtein) {
            this.alignmentType = "protein";
            this.alignmentTypeShort = "prot";
            this.alignmentTypeCapital = "Protein";
        } else if (alignNucleotide) {
            this.alignmentType = "nucleotide";
            this.alignmentTypeShort = "nuc";
            this.alignmentTypeCapital = "Nucleotide";
        } else { //alignVariants
            this.alignmentType = "variants";
            this.alignmentTypeShort = "var";
            this.alignmentTypeCapital = "Variants";
        }

        Pantools.logger.debug("Initializing MultipleSequenceAlignment with the following parameters: msaMethod: {}, alignNucleotide: {}, alignProtein: {}, alignVariants: {}, pavs: {}, hmGroups: {}.",
                msaMethod, alignNucleotide, alignProtein, alignVariants, pavs, hmGroups);

        prepareSequences();
    }

    /**
     * Creates input files for multiple sequence alignment
     */
    private void prepareSequences() {
        Pantools.logger.info("Multiple sequence alignment for {}.", msaMethod);

        // get variables from pangenome & check that 1) it is a pangenome, 2) a grouping is set
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            create_skip_arrays(false, true); // create skip array if --skip/-ref is provided by user

            check_current_grouping_version(); // check which version of homology grouping is active
            if (grouping_version < 1 && !(msaMethod.equals("regions") || msaMethod.equals("functions"))) { //exit if no grouping, except for regions or functions
                Pantools.logger.error("No homology grouping is active.");
                System.exit(1);
            }

            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the pangenome database.");
            throw new RuntimeException(nfe.getMessage());
        }

        //get path to alignments folder
        Path msaDir = Paths.get(WORKING_DIRECTORY).resolve("alignments");

        //prepare sequences //TODO create input should not be this amount of copying code, can be simplified
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            switch(msaMethod) { //set outDir + create input files + set msaNames
                case "per_group":
                    this.alignmentInputType = "groups";
                    this.hmNodeList = findHmNodes(hmGroups, 1);
                    if (alignVariants) {
                        this.outDirMsa = msaDir
                                .resolve("msa_" + msaMethod + "_var")
                                .resolve("grouping_v" + grouping_version);
                        this.msaNames = createInputMsaPerGroup(true); //only use unique hm groups if variants are added
                    } else {
                        this.outDirMsa = msaDir
                                .resolve("msa_" + msaMethod)
                                .resolve("grouping_v" + grouping_version);
                        this.msaNames = createInputMsaPerGroup(false); //only use unique hm groups if variants are added
                    }
                    break;
                case "multiple_groups":
                    this.alignmentInputType = "groups";
                    this.hmNodeList = findHmNodes(hmGroups, 1);
                    if (alignVariants) {
                        this.outDirMsa = msaDir
                                .resolve("msa_" + msaMethod + "_var")
                                .resolve("grouping_v" + grouping_version);
                    } else {
                        this.outDirMsa = msaDir
                                .resolve("msa_" + msaMethod)
                                .resolve("grouping_v" + grouping_version);
                    }
                    this.msaNames = createInputMsaMultipleGroups();
                    break;
                case "regions":
                    this.alignmentInputType = "regions";
                    this.hmNodeList = null; // not needed for regions
                    this.outDirMsa = msaDir
                            .resolve("msa_" + msaMethod);
                    this.msaNames = createInputMsaRegions();
                    break;
                case "functions":
                    this.alignmentInputType = "functions";
                    this.hmNodeList = null; // not needed for functions
                    if (alignVariants) {
                        this.outDirMsa = msaDir
                                .resolve("msa_" + msaMethod + "_var");
                    } else {
                        this.outDirMsa = msaDir
                                .resolve("msa_" + msaMethod);
                    }
                    this.msaNames = createInputMsaFunctions();
                    break;
                default:
                    Pantools.logger.error("Did not recognise the following MSA method: {}.", msaMethod);
                    System.exit(1);
            }

            tx.success(); // transaction successful, commit changes
        }
    }

    /**
     * Reads skipped_genomes.info and checks if this is correct with current skip array
     * @param inputPath path to location of skipped_genomes.info
     * @return boolean if skipping is correct
     */
    private boolean checkIfSkippingIsCorrect(Path inputPath) {
        boolean correctSkipping = false;

        String[] stringSkipArray = new String[skip_array.length];
        for (int i = 0; i < skip_array.length; i++) {
            if (skip_array[i]) {
                stringSkipArray[i] = "T";
            } else {
                stringSkipArray[i] = "F";
            }
        }
        String skipArrayString = String.join(",", stringSkipArray);

        try (BufferedReader file = Files.newBufferedReader(inputPath.resolve("skipped_genomes.info"))) {
            int counter = 1;
            while (file.ready()) {
                if (counter > 1) {
                    continue;
                }
                String line = file.readLine().trim();
                if (line.equals(skipArrayString)) {
                    correctSkipping = true;
                }
                counter++;
            }
        } catch (FileNotFoundException | NoSuchFileException e) { //first time running this function
            correctSkipping = true;
            write_string_to_file_full_path(skipArrayString, inputPath.resolve("skipped_genomes.info"));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        return correctSkipping;
    }

    /**
     * Prepare several input files for the input homology groups
     * @param ignoreUniqueLimit, still write unique sequences when true
     * @return updated list where groups with 1 member are removed
     */
    private ArrayList<String> createInputMsaPerGroup(boolean ignoreUniqueLimit) {
        ArrayList<String> updatedHmNodeList = new ArrayList<>(); // exclude homology groups with only 1 sequence
        long totalHmgroups = hmNodeList.size();
        int groupCounter = 0, notUniqueCounter = 0;

        int printThreshold = 100;
        if (totalHmgroups < 500) {
            printThreshold = 10;
        }

        Pantools.logger.info("Preparing input files for {} homology groups.", totalHmgroups);
        for (Node hmNode : hmNodeList) {
            groupCounter++;
            if ((groupCounter % printThreshold == 0 || groupCounter == totalHmgroups || groupCounter < 50)) {
                Pantools.logger.debug("Creating input sequences: {} / {} groups.", groupCounter, totalHmgroups);
            }

            checkIfHmIsCorrectGrouping(hmNode);

            Path inputPath = createMsaDirs(outDirMsa.resolve(String.valueOf(hmNode.getId())));
            if (inputPath.resolve(alignmentTypeShort + ".fasta").toFile().exists()) {
                updatedHmNodeList.add(Long.toString(hmNode.getId()));
                continue;
            }

            int numMembers = (int) hmNode.getProperty("num_members"); // number of sequences in group
            if (numMembers == 1 && !ignoreUniqueLimit) { // do not create files for groups with 1 sequence
                continue;
            }
            updatedHmNodeList.add(Long.toString(hmNode.getId()));
            notUniqueCounter++;

            writeMsaInputFiles(hmNode, inputPath);
        }

        int totalNonUniqueHms = countNotUniqueHmGroups();
        if (totalNonUniqueHms <= notUniqueCounter) {
            // this file is used to skip this function completely
            write_string_to_file_full_path("all sequences were extracted", outDirMsa.resolve("extract_sequences_done"));
        }

        if (hmNodeList.size() > updatedHmNodeList.size()) {
            Pantools.logger.info("Lowered the number of homology groups from {} to {} because some groups only had 1 sequence.", hmNodeList.size(), updatedHmNodeList.size());
        }

        Pantools.logger.info("Finished preparing input files for {} homology groups.", updatedHmNodeList.size());
        Pantools.logger.debug("Homology group input files created for: {}.", updatedHmNodeList);

        return updatedHmNodeList;
    }

    /**
     * Create the input and output directories for msa and return input path
     * @param path path to msa directory
     * @return path to input directory
     */
    private Path createMsaDirs(Path path) {
        Path inputPath = path.resolve("input");
        Path outputPath = path.resolve("output");
        inputPath.toFile().mkdirs();
        outputPath.toFile().mkdirs();

        if (!checkIfSkippingIsCorrect(inputPath)) {
            Pantools.logger.info("Skipping is incorrect, removing old files.");
            inputPath.toFile().delete();
            outputPath.toFile().delete();
            inputPath.toFile().mkdirs();
            outputPath.toFile().mkdirs();
        }

        return inputPath;
    }

    /**
     * Creates input for msa_of_multiple_groups
     * @return name of groups that are aligned (currently a list consisting of one string)
     */
    private ArrayList<String> createInputMsaMultipleGroups() {
        Pantools.logger.info("Preparing input files for MSA of multiple groups.");
        ArrayList<String> hmStringList = new ArrayList<>();
        ArrayList<String> outputHmStringList = new ArrayList<>();

        for (Node hmNode : hmNodeList) {
            checkIfHmIsCorrectGrouping(hmNode);
            hmStringList.add(String.valueOf(hmNode.getId()));
        }
        String hmString = String.join("_", hmStringList);
        outputHmStringList.add(hmString);

        Path inputPath = createMsaDirs(outDirMsa.resolve(hmString));
        if (inputPath.resolve(alignmentTypeShort + ".fasta").toFile().exists()) {
            return outputHmStringList;
        }

        writeMultiMsaInputFiles(hmNodeList, inputPath);

        return outputHmStringList;
    }

    /**
     * Create input for MSA of regions
     * @return name of the input file that is used as identifier for the alignment (list of one currently)
     */
    private ArrayList<String> createInputMsaRegions() {
        Pantools.logger.info("Preparing input files for MSA of regions.");

        if (target_genome != null || skip_genomes != null) { //stop if --ref or --skip is used
            Pantools.logger.error("'--method regions' is unable to use --reference/-ref or --skip. You should exclude these genomes from your --regions-file."); //TODO: incorrect error message
            System.exit(1);
        }

        //read regions (TODO put this in a util class)
        String[] regionsToSearch = Classification.read_regions_file();

        //create name for region (basename of file currently)
        ArrayList<String> outputRegionsStringList = new ArrayList<>();
        String[] regionsFileNameSplit = PATH_TO_THE_REGIONS_FILE.replace(".", "_").split("/");
        String regionsName = regionsFileNameSplit[regionsFileNameSplit.length - 1];
        outputRegionsStringList.add(regionsName);

        Path path = outDirMsa.resolve(regionsName);
        Path inputPath = path.resolve("input");
        Path outputPath = path.resolve("output");

        inputPath.toFile().mkdirs();
        outputPath.toFile().mkdirs();
        writeRegionsMsaInputFiles(regionsToSearch, inputPath);

        return outputRegionsStringList;
    }

    /**
     * Creates input for MSA of functions
     * @return list with names of functions that are aligned
     */
    private ArrayList<String> createInputMsaFunctions() {
        Pantools.logger.info("Preparing input files for MSA of functions.");

        if (SELECTED_NAME == null) { //TODO should be impossible to get here
            Pantools.logger.error("No functions selected. Please use --gene-names with a comma-separated list of function names.");
            System.exit(1);
        }

        ArrayList<String> outputFunctionsStringList = new ArrayList<>();
        HashSet<Node> functionNodeList = Classification.find_functional_annotation_node();

        long totalFunctions = functionNodeList.size();
        int functionCounter = 0;

        for (Node functionNode : functionNodeList) {
            functionCounter++;
            Pantools.logger.debug("Creating input sequences: {} / {} functions.", functionCounter, totalFunctions);

            String function = (String) functionNode.getProperty("id");
            function = function.replace(":", "");

            Path inputPath = createMsaDirs(outDirMsa.resolve(function));
            if (inputPath.resolve(alignmentTypeShort + ".fasta").toFile().exists()) {
                outputFunctionsStringList.add(function);
                continue;
            }

            writeFunctionsMsaInputFiles(functionNode, inputPath);
            outputFunctionsStringList.add(function);
        }

        return outputFunctionsStringList;
    }

    /**
     * Check if node has 'homology_group' label
     * @param hmNode node to be checked
     *
     * NB: does not return a boolean, but will exit if incorrect grouping
     */
    private void checkIfHmIsCorrectGrouping(Node hmNode) {
        boolean correct = test_if_correct_label(hmNode, HOMOLOGY_GROUP_LABEL, false);
        if (!correct) {
            correct = test_if_correct_label(hmNode, INACTIVE_HOMOLOGY_GROUP_LABEL, false);
            if (!correct) {
                Pantools.logger.error("Node {} is not a homology group.", hmNode);
            } else {
                int groupVersion = (int) hmNode.getProperty("group_version");
                Pantools.logger.error("Node {} is an inactive homology group! Grouping {} is active while this homology group is version {}.",
                        hmNode,
                        grouping_version,
                        groupVersion);
            }
            System.exit(1);
        }
    }

    /**
     * Counts the number of not unique homology groups of all homology groups
     * @return integer of the number of not unique homology groups
     */
    private int countNotUniqueHmGroups() {
        int counter = 0;
        ResourceIterator<Node> hmNodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
        while (hmNodes.hasNext()) {
            Node hmNode = hmNodes.next();
            int numMembers = (int) hmNode.getProperty("num_members");
            if (numMembers == 1) {
                continue;
            }
            counter++;
        }
        return counter;
    }

    /**
     * Writes several input files for the input homology groups
     * @param hmNode node for which to write input files
     * @param inputPath path to where to write input files
     */
    private void writeMsaInputFiles(Node hmNode, Path inputPath) {

        // get all mRNA nodes in homology group
        HashSet<Node> mrnaNodes = new HashSet<>();
        for (Relationship rel : hmNode.getRelationships(RelTypes.has_homolog)) {
            Node mrnaNode = rel.getEndNode();
            mrnaNodes.add(mrnaNode);
        }

        // build input files
        buildMrnaProteinSequences(mrnaNodes, inputPath);
    }

    /**
     * Writes the input files for MSA:
     *  prot.fasta, protein sequences (if alignProtein)
     *  nuc.fasta, nucleotide sequences (if alignNucleotide)
     *  var.fasta, variant sequences (if alignVariants)
     *  nuc.structure.tsv, structure of mRNA (if alignNucleotide)
     *  var.structure.tsv, structure of mRNA (if alignVariants)
     *  genome_order.info, list of genome number and identifiers
     *  sequences.info, relevant information for user
     *  skipped_genomes.info, skip array for current alignment
     * @param proteinBuilder StringBuilder with protein sequences
     * @param nucleotideBuilder StringBuilder with nucleotide sequences
     * @param genomeNrsBuilder StringBuilder with genome numbers and identifiers
     * @param infoBuilderHeader StringBuilder with header for info file
     * @param infoBuilder StringBuilder with info about sequences
     * @param infoGeneStructureBuilder StringBuilder with info about gene structure
     * @param inputPath path to where to write input files
     */
    private void writeMrnaProteinSequences(StringBuilder proteinBuilder,
                                           StringBuilder nucleotideBuilder,
                                           StringBuilder genomeNrsBuilder,
                                           StringBuilder infoBuilderHeader,
                                           StringBuilder infoBuilder,
                                           StringBuilder infoGeneStructureBuilder,
                                           Path inputPath) {

        if (alignProtein) {
            write_SB_to_file_full_path(proteinBuilder, inputPath.resolve("prot.fasta"));
        } else if (alignNucleotide) {
            write_SB_to_file_full_path(nucleotideBuilder, inputPath.resolve("nuc.fasta"));
            write_SB_to_file_full_path(infoGeneStructureBuilder, inputPath.resolve("nuc.structure.tsv"));
        } else if (alignVariants) {
            write_SB_to_file_full_path(nucleotideBuilder, inputPath.resolve("var.fasta"));
            write_SB_to_file_full_path(infoGeneStructureBuilder, inputPath.resolve("var.structure.tsv"));
        } else {
            Pantools.logger.error("Failed to write input files for MSA. No alignment mode was provided.");
            throw new RuntimeException("No alignment mode was provided");
        }

        write_SB_to_file_full_path(genomeNrsBuilder, inputPath.resolve("genome_order.info"));
        write_SB_to_file_full_path(infoBuilderHeader.append(infoBuilder), inputPath.resolve("sequences.info"));
    }

    /**
     * Builds the protein and nucleotide sequence for a given mRNA node
     * @param proteinFastaBuilder StringBuilder to which to append the protein sequence
     * @param nucleotideFastaBuilder StringBuilder to which to append the nucleotide sequence
     * @param genomeNrsBuilder StringBuilder to which to append the genome number
     * @param infoBuilderHeader StringBuilder to which to append the header for the info file
     * @param infoBuilder StringBuilder to which to append the mRNA information
     * @param infoGeneStructureBuilder StringBuilder to which to append the gene structure information
     * @param presentGenomes ArrayList of integers of the present genomes
     * @param mrnaNode node for which to build the sequence
     */
    private void addMrnaProteinSequence(StringBuilder proteinFastaBuilder,
                                        StringBuilder nucleotideFastaBuilder,
                                        StringBuilder genomeNrsBuilder,
                                        StringBuilder infoBuilderHeader,
                                        StringBuilder infoBuilder,
                                        StringBuilder infoGeneStructureBuilder,
                                        ArrayList<Integer> presentGenomes,
                                        Node mrnaNode) {
        //needed to get the sequence with getSequence(mrnaNode)
        if (alignNucleotide || alignVariants) {
            INDEX_SC = new IndexScanner(INDEX_DB);
            GENOME_SC = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_SC.get_pre_len());
        }

        int[] copies = new int[total_genomes];

        String mrnaProtein = getSequence(mrnaNode, "protein");
        mrnaProtein = splitSeqOnLength(mrnaProtein, 80);
        int genomeNr = (int) mrnaNode.getProperty("genome");
        if (skip_array[genomeNr-1]) {
            return;
        }
        copies[genomeNr-1] += 1;
        int copyNr = copies[genomeNr-1];
        presentGenomes.add(genomeNr);
        String proteinId = (String) mrnaNode.getProperty("protein_ID");
        if (proteinId.contains(":") || proteinId.contains(";") || proteinId.contains("|")) { // not allowed in newick format
            proteinId = proteinId
                    .replace(":",".")
                    .replace(";",".")
                    .replace("|",".");
        }
        String newId = genomeNr + "_" + copyNr + "_" + proteinId;

        long mrnaNodeId = mrnaNode.getId();
        genomeNrsBuilder.append(genomeNr).append(",").append(newId).append("\n");
        if (!PROTEOME) {
            int[] address = (int[]) mrnaNode.getProperty("address");
            String strand = (String) mrnaNode.getProperty("strand");
            String name = getMrnaNodeName(mrnaNode);

            if (alignProtein) {
                proteinFastaBuilder.append(">").append(newId).append(" ")
                        .append(address[0]).append("_").append(address[1]).append("_").append(address[2]).append("_").append(address[3]).append("\n")
                        .append(mrnaProtein).append("\n");
            }
            if (alignVariants || alignNucleotide) {
                //get sequence (mRNA sequence if variants involved, CDS if no variants involved)
                String nucSequence = getSequence(mrnaNode, "mRNA");
                nucSequence = splitSeqOnLength(nucSequence, 80);

                //add sequence to fasta file
                nucleotideFastaBuilder.append(">").append(newId).append(" ")
                        .append(address[0]).append("_").append(address[1]).append("_").append(address[2]).append("_").append(address[3]).append("\n")
                        .append(nucSequence).append("\n");

                //add structure to structure file
                infoGeneStructureBuilder.append(getStructure(mrnaNode, newId, address));

                //add variants to fasta file
                if (alignVariants) {
                    getVariation(mrnaNode, genomeNr, copyNr, proteinId, genomeNrsBuilder, nucleotideFastaBuilder);
                }
            }

            TreeSet<Integer> absent = getMissingGenomesInArrayList(presentGenomes);
            String genomeFreqs = determineFrequencyListInt(presentGenomes);
            infoBuilderHeader.append("Present: ").append(genomeFreqs).append("\n")
                        .append("Absent: ").append(absent.toString().replace("[","").replace("]","").replace(",","")).append("\n\n");
            infoBuilder.append(genomeNr).append(", ").append(name).append(", ").append(newId).append(", ").append(mrnaNodeId).append(", ")
                    .append(address[0]).append(" ").append(address[1]).append(" ").append(address[2]).append(" ").append(address[3]).append(", ").append(strand).append("\n");
        } else {
            proteinFastaBuilder.append(">").append(newId).append("\n").append(mrnaProtein).append("\n"); //we could check, but this is always alignProtein = true
            infoBuilder.append(genomeNr).append(", ").append(newId).append(mrnaNodeId).append(", ").append("\n");
        }
    }

    /**
     * Gets the name of the mrna node, if it has one
     * @param mrnaNode node to get name from
     * @return name of mrna node
     * NB: name used to be stored as a string, nowadays as a string array (keeping old style for backwards compatibility)
     */
    private String getMrnaNodeName(Node mrnaNode) {
        assert mrnaNode.hasLabel(MRNA_LABEL);

        String name = "-";
        if (mrnaNode.hasProperty("name")) {
            if (mrnaNode.getProperty("name") instanceof String) { // old style
                name = (String) mrnaNode.getProperty("name");
            } else if (mrnaNode.getProperty("name") instanceof String[]) { // new style
                String[] nameArray = (String[])(mrnaNode.getProperty("name"));
                name = Arrays.toString(nameArray).replace("[","").replace("]","").replace(",","/").replace(" ","");
            } else {
                throw new RuntimeException("Unexpected type for property 'name' of node " + mrnaNode.getId());
            }
        }

        return name;
    }

    /**
     * Writes several input files for the input homology groups
     *  prot.fasta, protein sequences (if alignProtein)
     *  nuc.fasta, nucleotide sequences (if alignNucleotide)
     *  var.fasta, variant sequences (if alignVariants)
     *  nuc.structure.tsv, structure of mRNA (if alignNucleotide)
     *  var.structure.tsv, structure of mRNA (if alignVariants)
     *  genome_order.info, list of genome number and identifiers
     *  itol_templates/homology_groups.txt, iTOL templates for input data
     *  prot_sequences.info, relevant information for user (if alignProtein)
     *  nuc_sequences.info, relevant information for user (if alignNucleotide)
     *  var_sequences.info, relevant information for user (if alignVariants)
     *  skipped_genomes.info, skip array for current alignment
     *  sequences.info, relevant information for user
     *  skipped_genomes.info, skip array for current alignment
     * @param hmNodeList list of nodes for which to write input files
     * @param inputPath path to where to write input files
     */
    private void writeMultiMsaInputFiles(ArrayList<Node> hmNodeList, Path inputPath) {
        StringBuilder protFasta = new StringBuilder();
        StringBuilder nucFasta = new StringBuilder();
        StringBuilder genomeOrder = new StringBuilder();
        StringBuilder infoBuilder = new StringBuilder("#Information about sequences in homology group\n#Occurance in genomes\n");
        StringBuilder infoBuilderPart2 = new StringBuilder("##genome, mRNA name, mRNA id, node id, address, gene length, gene copy number within the group\n");
        if (PROTEOME) {
            infoBuilderPart2 = new StringBuilder("##genome, protein id, node id, protein length, protein copy number within the group\n");
        }
        StringBuilder infoGeneStructure = new StringBuilder("#Seq\tStart\tStop\tStrand\tFeature\tmRNA\n");
        ArrayList<Integer> protLengths = new ArrayList<>();
        ArrayList<Integer> nucLengths = new ArrayList<>();

        int totalSequences = 0;

        //needed to get the sequence with getSequence(mrnNode)
        if (!PROTEOME) {
            INDEX_SC = new IndexScanner(INDEX_DB);
            GENOME_SC = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_SC.get_pre_len());
        }

        HashMap<Node, ArrayList<String>> proteinIds = new HashMap<>(); // key is homology group id,
        // value is arraylist with "protein_ID" proprties from mrna nodes
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (int i = 0; i < hmNodeList.size(); i++) {
                Node hmNode = hmNodeList.get(i);
                ArrayList<Integer> presentGenomes = new ArrayList<>();
                int[] genomeCounts = new int[total_genomes];
                infoBuilderPart2.append("#").append(hmNode.getId()).append("\n");

                for (Relationship rel : hmNode.getRelationships()) {
                    Node mrnaNode = rel.getEndNode();
                    int genomeNr = (int) mrnaNode.getProperty("genome");
                    String proteinId = (String) mrnaNode.getProperty("protein_ID");
                    if (skip_array[genomeNr-1]) {
                        continue;
                    }
                    genomeCounts[genomeNr-1] += 1;
                    int copyNr = genomeCounts[genomeNr-1];
                    totalSequences ++;

                    Pantools.logger.debug("Gathering sequences: {} / {} homology groups. {} sequences.", (i+1), hmNodeList.size(), totalSequences);

                    if (proteinId.contains(":") || proteinId.contains(";") || proteinId.contains("|")) { // not allowed in newick format
                        proteinId = proteinId
                                .replace(":",".")
                                .replace(";",".")
                                .replace("|",".");
                    }
                    String newProteinId = genomeNr + "_" + copyNr + "_" + proteinId;
                    ArrayList<String> ids = proteinIds.get(hmNode);
                    if (ids == null) {
                        ids = new ArrayList<>();
                    }
                    ids.add(newProteinId);
                    proteinIds.put(hmNode, ids);

                    genomeOrder.append(genomeNr).append(",").append(newProteinId).append("\n");

                    if (alignProtein) {
                        String proteinSequence = getSequence(mrnaNode, "protein");
                        proteinSequence = splitSeqOnLength(proteinSequence, 80);
                        protLengths.add(proteinSequence.length());
                        protFasta.append(">").append(newProteinId).append("\n").append(proteinSequence).append("\n");
                        infoBuilderPart2.append(genomeNr).append(", ").append(newProteinId).append(", ").append(mrnaNode.getId()).append(", ")
                                .append(proteinSequence.length()).append(", ").append(copyNr).append("\n");
                    }
                    if (alignVariants || alignNucleotide) {
                        String mrnaName = getMrnaNodeName(mrnaNode);
                        int[] address = (int[]) mrnaNode.getProperty("address");

                        //get sequence (mRNA sequence if variants involved, CDS if no variants involved)
                        String nucSequence = getSequence(mrnaNode, "mRNA");
                        nucSequence = splitSeqOnLength(nucSequence, 80);
                        nucLengths.add(nucSequence.length());

                        nucFasta.append(">").append(newProteinId).append("\n").append(nucSequence).append("\n");
                        infoBuilderPart2.append(genomeNr).append(", ").append(mrnaName).append(", ").append(newProteinId).append(", ").append(mrnaNode.getId())
                                .append(", ").append(genomeNr).append(" ").append(address[1]).append(" ").append(address[2]).append(" ").append(address[3]).append(", ")
                                .append(nucSequence.length()).append(", ").append(copyNr).append("\n");

                        //add structure to structure file
                        infoGeneStructure.append(getStructure(mrnaNode, newProteinId, address));

                        //add variants to fasta file
                        if (alignVariants) {
                            getVariation(mrnaNode, genomeNr, copyNr, proteinId, genomeOrder, nucFasta);
                        }
                    }
                }
                String genomeFreqs = Classification.determine_frequency_list_int(presentGenomes);
                TreeSet<Integer> absent = getMissingGenomesInArrayList(presentGenomes);
                infoBuilder.append("#").append(hmNode.getId()).append("\nPresent: ").append(genomeFreqs)
                        .append("\nAbsent: ").append(absent).append("\n\n");
                infoBuilderPart2.append("\n");
            }
            tx.success(); // transaction successful, commit changes
        }

        inputPath.resolve("similarity_identity").toFile().mkdirs(); // create directory
        inputPath.resolve("var_inf_positions").toFile().mkdirs(); // create directory
        inputPath.resolve("itol_templates").toFile().mkdirs(); // create directory
        String[] colorCodes = new String[]{"#fabebe","#bfef45","#42d4f4","#ffd8b1","#aaffc3","#fffac8","#e6beff","#469990","#e6194B","#f58231", "#ffe119",
                "#3cb44b","#4363d8","#911eb4","#a9a9a9","#800000","#808000","#9A6324","#000075","#f032e6"};
        String[] colorNames = new String[]{"Pink","Lime","Cyan","Apricot","Mint","Beige","Lavender","Teal","Red","Orange","Yellow","Green","Blue","Purple","Grey","Maroon",
                "Olive", "Brown", "Navy","Magenta"};
        createMsaMultiGroupTemplate(hmNodeList, proteinIds, inputPath, colorCodes, colorNames);
        createMsaMultiPhenoTemplate(inputPath, proteinIds, colorCodes, colorNames);
        write_string_to_file_full_path(genomeOrder.toString(), inputPath.resolve("genome_order.info"));

        if (alignProtein) {
            write_string_to_file_full_path(protFasta.toString(), inputPath.resolve("prot.fasta"));
            String protLengthsStr = Classification.determine_frequency_list_int(protLengths);
            write_string_to_file_full_path("Protein lengths: " + protLengthsStr + "\n\n"
                    + infoBuilder + infoBuilderPart2, inputPath.resolve("prot_sequences.info"));
        } else if (alignNucleotide) {
            write_string_to_file_full_path(nucFasta.toString(), inputPath.resolve("nuc.fasta"));
            String nucLengthsStr = Classification.determine_frequency_list_int(nucLengths);
            write_string_to_file_full_path("Nucleotide lengths: " + nucLengthsStr + "\n\n"
                    + infoBuilder + infoBuilderPart2, inputPath.resolve("nuc_sequences.info"));
            write_SB_to_file_full_path(infoGeneStructure, inputPath.resolve("nuc.structure.tsv"));
        } else if (alignVariants) {
            write_string_to_file_full_path(nucFasta.toString(), inputPath.resolve("var.fasta"));
            String nucLengthsStr = Classification.determine_frequency_list_int(nucLengths);
            write_string_to_file_full_path("Nucleotide lengths: " + nucLengthsStr + "\n\n"
                    + infoBuilder + infoBuilderPart2, inputPath.resolve("var_sequences.info"));
            write_SB_to_file_full_path(infoGeneStructure, inputPath.resolve("var.structure.tsv"));
        }
    }

    /**
     * Writes several input files for the input homology groups
     *  nuc.fasta, nucleotide sequences
     *  genome_order.info, list of genome number and identifiers
     *
     * @param regionsToSearch regions to search for
     * @param inputPath path to where to write input files
     */
    private void writeRegionsMsaInputFiles(String[] regionsToSearch, Path inputPath) {
        StringBuilder fastaBuilder = new StringBuilder();
        StringBuilder genomeNrsBuilder = new StringBuilder();

        //needed to get the sequence with getRegion()
        if (!PROTEOME) {
            INDEX_SC = new IndexScanner(INDEX_DB);
            GENOME_SC = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_SC.get_pre_len());
        }

        long totalRegions = regionsToSearch.length;
        int regionCounter = 0;

        for (String regionStr : regionsToSearch) {
            regionCounter++;
            Pantools.logger.debug("Creating input sequences: {} / {} regions.", regionCounter, totalRegions);

            String[] regionArray = regionStr.split(" ");
            int genomeNr;
            int sequenceNr;
            int startPos;
            int endPos;
            try {
                genomeNr = Integer.parseInt(regionArray[0]);
                sequenceNr = Integer.parseInt(regionArray[1]);
                startPos = Integer.parseInt(regionArray[2]);
                endPos = Integer.parseInt(regionArray[3]);
            } catch (NumberFormatException nfe) {
                Pantools.logger.warn("Unable to correctly retrieve four numbers in: {}.", regionStr);
                continue;
            }

            boolean reverse = false;
            String rev_complement = "";
            if (regionStr.endsWith("-")) {
                reverse = true;
                regionStr = regionStr.replace(" -","");
                rev_complement = "_rvComplement";
            }

            if (genomeNr > total_genomes) {
                Pantools.logger.error("A genome with number {} does not exists! Only {} genomes in pangenome.", genomeNr, total_genomes);
                System.exit(1);
            }
            if (endPos < startPos) {
                Pantools.logger.error("The start position is lower than the end position. {} (for reverse complement, please use a '-').", regionStr);
                System.exit(1);
            }

            regionStr = regionStr.replace(" ","_"); // 1 1 1 1000 becomes 1_1_1_1000
            String region;
            if (!reverse) {
                region = getRegion(genomeNr, sequenceNr, startPos-1, endPos-1, true);
            } else {
                region = getRegion(genomeNr, sequenceNr, startPos-1, endPos-1, false);
            }
            fastaBuilder.append(">").append(regionStr).append(rev_complement).append("\n")
                    .append(region).append("\n");

            genomeNrsBuilder.append(genomeNr).append(",").append(regionStr).append(rev_complement).append("\n");
            if (region.length() < 10) {
                Pantools.logger.warn("The length of {} is below 10 characters.", regionStr);
                System.exit(1);
            }

            if (alignVariants) { //TODO let MSA of regions work with variants too
                // Currently impossible to get here
                throw new RuntimeException("MSA of regions with variants is not yet implemented.");
            }
        }

        write_string_to_file_full_path(fastaBuilder.toString(), inputPath.resolve("nuc.fasta"));
        write_string_to_file_full_path(genomeNrsBuilder.toString(), inputPath.resolve("genome_order.info"));
    }

    /**
     * Writes several input files for the input homology groups
     * @param functionNode function node for which to create input files
     * @param inputPath path to where to write input files
     */
    private void writeFunctionsMsaInputFiles(Node functionNode, Path inputPath) {

        // get all relationships of functionNode
        Iterable<Label> nodeLabels = functionNode.getLabels();
        Node pangenomeNode = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
        Iterable<Relationship> allRelations = pangenomeNode.getRelationships(); // all_rels must be initialized already
        for (Label label1 : nodeLabels) {
            String label_str = label1.toString();
            switch (label_str) {
                case "GO":
                    allRelations = functionNode.getRelationships(RelTypes.has_go);
                    break;
                case "interpro":
                    allRelations = functionNode.getRelationships(RelTypes.has_interpro);
                    break;
                case "pfam":
                    allRelations = functionNode.getRelationships(RelTypes.has_pfam);
                    break;
                case "tigrfam":
                    allRelations = functionNode.getRelationships(RelTypes.has_tigrfam);
                    break;
                default:
                    Pantools.logger.error("{} is not a function node.", functionNode);
                    System.exit(1);
            }
        }

        // get all mRNA nodes for creating input files
        HashSet<Node> mrnaNodes = new HashSet<>();
        for (Relationship rel : allRelations) {
            Node mrnaNode = rel.getStartNode();
            mrnaNodes.add(mrnaNode);
        }

        // build input files
        buildMrnaProteinSequences(mrnaNodes, inputPath);
    }

    /**
     * Builds the input files for MSA for a given set of mRNA nodes
     * @param mrnaNodes set of mRNA nodes
     * @param inputPath path to where to write input files
     */
    private void buildMrnaProteinSequences(HashSet<Node> mrnaNodes, Path inputPath) {
        StringBuilder proteinFastaBuilder = new StringBuilder();
        StringBuilder nucleotideFastaBuilder = new StringBuilder();
        StringBuilder genomeNrsBuilder = new StringBuilder();
        ArrayList<Integer> presentGenomes = new ArrayList<>();
        StringBuilder infoBuilderHeader = new StringBuilder("#Information about sequences in homology group\n#Occurance in genomes\n");
        StringBuilder infoBuilder = new StringBuilder("#genome, mRNA name, mRNA identifier, node identifier, address, strand\n");
        if (PROTEOME) {
            infoBuilder = new StringBuilder("#genome, protein identifier, protein node id\n");
        }
        StringBuilder infoGeneStructureBuilder = new StringBuilder("#Seq\tStart\tStop\tStrand\tFeature\tmRNA\n");


        for (Node mrnaNode : mrnaNodes) {
            addMrnaProteinSequence(proteinFastaBuilder, nucleotideFastaBuilder, genomeNrsBuilder, infoBuilderHeader,
                    infoBuilder, infoGeneStructureBuilder, presentGenomes, mrnaNode);
        }

        writeMrnaProteinSequences(proteinFastaBuilder, nucleotideFastaBuilder, genomeNrsBuilder, infoBuilderHeader,
                infoBuilder, infoGeneStructureBuilder, inputPath);
    }

    /**
     * Returns a StringBuilder with the structure of a give mRNA.
     * @param mrnaNode node ID of mRNA
     * @param proteinId protein ID of mRNA
     * @param mrnaAddress address of the mRNA sequence
     * @return structure information (multi-line)
     * Identifies genomeNr, seqNr, start, stop of mRNA, exon and CDS features and creates separate lines per feature.
     * The lines are formatted as follows: <genomeNr>_<seqNr>\t<start>\t<stop>\t<strand>\t<feature type>\t<proteinId>
     */
    private StringBuilder getStructure(Node mrnaNode, String proteinId, int[] mrnaAddress) {
        StringBuilder structure = new StringBuilder();

        //get mRNA information
        int genomeNr = mrnaAddress[0];
        int seqNr = mrnaAddress[1];
        int mrnaStart = mrnaAddress[2];
        int mrnaStop = mrnaAddress[3];
        String mrnaStrand = (String) mrnaNode.getProperty("strand");

        //add mRNA information
        structure.append(genomeNr);
        structure.append("_");
        structure.append(seqNr);
        structure.append("\t");
        structure.append(mrnaStart);
        structure.append("\t");
        structure.append(mrnaStop);
        structure.append("\t");
        structure.append(mrnaStrand);
        structure.append("\t");
        structure.append("mRNA");
        structure.append("\t");
        structure.append(proteinId);
        structure.append("\n");

        Iterable<Relationship> relations = mrnaNode.getRelationships(RelTypes.is_parent_of);
        for (Relationship rel : relations) {
            Node cdsIntronExonNode = rel.getEndNode(); // CDS, intron or exon
            if (cdsIntronExonNode.hasLabel(EXON_LABEL)) {
                //get exon information
                int[] exonAddress = (int[]) cdsIntronExonNode.getProperty("address");
                int exonStart = exonAddress[2];
                int exonStop = exonAddress[3];
                String exonStrand = (String) cdsIntronExonNode.getProperty("strand");

                //add exon information
                structure.append(genomeNr);
                structure.append("_");
                structure.append(seqNr);
                structure.append("\t");
                structure.append(exonStart);
                structure.append("\t");
                structure.append(exonStop);
                structure.append("\t");
                structure.append(exonStrand);
                structure.append("\t");
                structure.append("exon");
                structure.append("\t");
                structure.append(proteinId);
                structure.append("\n");
            } else if (cdsIntronExonNode.hasLabel(CDS_LABEL)) {
                int[] cdsAddress = (int[]) cdsIntronExonNode.getProperty("address");
                int cdsStart = cdsAddress[2];
                int cdsStop = cdsAddress[3];
                String cdsStrand = (String) cdsIntronExonNode.getProperty("strand");

                //add CDS information
                structure.append(genomeNr);
                structure.append("_");
                structure.append(seqNr);
                structure.append("\t");
                structure.append(cdsStart);
                structure.append("\t");
                structure.append(cdsStop);
                structure.append("\t");
                structure.append(cdsStrand);
                structure.append("\t");
                structure.append("CDS");
                structure.append("\t");
                structure.append(proteinId);
                structure.append("\n");
            }
        }

        return structure;
    }

    /**
     * Append variants that are added with `pantools add_variants` to input files.
     * @param mrnaNode node of mRNA to which the variant is connected (via 'has_variant' relationship)
     * @param genomeNr genome number of the genome to which the variant is added
     * @param copyNr copy number of the protein ID of the mRNA
     * @param proteinId protein ID of the mRNA
     * @param genomeNrsBuilder StringBuilder for genome_order.info
     * @param nucleotideFastaFile StringBuilder for nuc.fasta
     *
     * This function updates the two input StringBuilders with information about the variants added.
     */
    private void getVariation(Node mrnaNode, int genomeNr, int copyNr, String proteinId,
                              StringBuilder genomeNrsBuilder, StringBuilder nucleotideFastaFile) {
        //create reference info about the protein
        String refInfo = copyNr + "_" + proteinId;
        Pantools.logger.debug("Adding variants to input files for {} (genome {}).", refInfo, genomeNr);

        //get nucleotide sequences of accessions
        Iterable<Relationship> relationships = mrnaNode.getRelationships(RelTypes.has_variant);

        // loop over all relationships (for each variant)
        int count = 0;
        for (Relationship relationship : relationships) {
            count++;

            //get variant node properties and assert it is indeed a variant node
            Node variantNode = relationship.getEndNode();
            Pantools.logger.trace("Variant node: {}.", variantNode);
            assert variantNode.hasLabel(VARIANT_LABEL);
            String type = (String) variantNode.getProperty("type");
            assert type.equals("mRNA");
            String variantId = (String) variantNode.getProperty("id");
            assert variantId.startsWith(genomeNr + "|");

            //skip variant node if it is absent (from `pantools add_pavs`) (ONLY IF --pavs WAS SET)
            if (pavs && variantNode.hasProperty("present")) {
                boolean present = (boolean) variantNode.getProperty("present");
                if (!present) {
                    Pantools.logger.trace("Skipping absent variant node {}.", variantNode);
                    continue;
                } else {
                    Pantools.logger.trace("Adding present variant node {}.", variantNode);
                }
            }

            //get variant sequence (skip variant node if it does not have a sequence)
            String variantSequence;
            if (!variantNode.hasProperty("mRNA_sequence")) {
                Pantools.logger.trace("Skipping variant node {} ({}) without mRNA sequence.", variantNode, variantId);
                continue;
            } else {
                variantSequence = (String) variantNode.getProperty("mRNA_sequence");
            }

            //add to genome_order.info and nuc.fasta
            String fastaHeader = variantId + "_" + refInfo;
            Pantools.logger.trace("Adding variant {} to input files.", fastaHeader);
            genomeNrsBuilder.append(variantId).append(",").append(fastaHeader).append("\n");
            nucleotideFastaFile.append(">").append(fastaHeader).append("\n")
                    .append(splitSeqOnLength(variantSequence, 80)).append("\n");
        }
        Pantools.logger.debug("Added {} variants to input files for {}.", count, refInfo);
    }

    /**
     * Gets sequence for a mRNA node
     * @param mrnaNode mRNA Node
     * @param feature whether to get mRNA or protein sequence
     * @return sequence belonging to feature of mrnaNode
     */
    private String getSequence(Node mrnaNode, String feature) {
        String sequence = "";

        switch (feature) {
            case "mRNA":
                StringBuilder sequenceSB = new StringBuilder();
                boolean strand = true; //setting true here to make sure I can run get_sub_sequence

                int[] address = (int[]) mrnaNode.getProperty("address");
                int length = (int) mrnaNode.getProperty("length");
                String strandStr = (String) mrnaNode.getProperty("strand");

                int genome = address[0];
                int sequenceNr = address[1];
                int position = address[2] - 1;

                if (strandStr.equals("+")) {
                    strand = true;
                } else if (strandStr.equals("-")) {
                    strand = false;
                } else {
                    Pantools.logger.error("Invalid strand ({}) for mRNA node {}.", strandStr, mrnaNode);
                    System.exit(1);
                }

                Pantools.logger.debug("Getting sequence for: {} ({}).", mrnaNode, Arrays.toString(address));

                GENOME_SC.get_sub_sequence(sequenceSB, genome, sequenceNr, position, length, strand);
                sequence = sequenceSB.toString();
                break;
            case "protein":
                if (mrnaNode.hasProperty("protein")) {
                    sequence = (String) mrnaNode.getProperty("protein", "");
                } else {
                    sequence = (String) mrnaNode.getProperty("protein_sequence", "");
                }
                break;
            default:
                Pantools.logger.error("Only mRNA or protein sequence can currently be retrieved.");
                System.exit(1);
        }

        return sequence;
    }

    /**
     * Retrieve the genomic region from the genome database
     * @param genome genome number
     * @param sequence sequence number
     * @param begin start of region
     * @param end end of region
     * @param forward whether or not to give the forward strand
     * @return sequence in specified region
     */
    private String getRegion(int genome, int sequence, int begin, int end, boolean forward) {
        if (begin > end) {
            Pantools.logger.error("Start location of this region is higher than its end location -> {} {} {} {}.", genome, sequence, begin, end);
            System.exit(1);
        }
        StringBuilder seq = new StringBuilder();
        GENOME_SC.get_sub_sequence(seq, genome, sequence, begin, end - begin + 1, forward);
        return seq.toString();
    }

    /**
     * Generates an ITOL template that colors tree labels based on different homology group (identifier)
     * @param hmNodeList list with 'homology_group' nodes
     * @param proteinIds the 'protein_ID' identifiers of mRNAs ordered per homology_group node
     * @param inputPath output location to write to
     * @param colorCodes hex color codes
     * @param colorNames name of color belonging to the hex code
     */
    private void createMsaMultiGroupTemplate(ArrayList<Node> hmNodeList, HashMap<Node, ArrayList<String>> proteinIds,
                                             Path inputPath, String[] colorCodes, String[] colorNames) {

        StringBuilder hmColorInfo = new StringBuilder("#Colors used for homology groups\n\n");
        StringBuilder itolHmTemplate = new StringBuilder("DATASET_COLORSTRIP\nSEPARATOR COMMA\nDATASET_LABEL,label1\nCOLOR,#ff0000\n"
                + "STRIP_WIDTH,40\nSHOW_INTERNAL,1\n\nDATA\n");

        int totalGroups = 0;
        for (int i = 0; i < hmNodeList.size(); i++) {
            Node hmNode = hmNodeList.get(i);
            String color = colorCodes[totalGroups];
            hmColorInfo.append("#").append(hmNode.getId()).append(" ").append(color).append(" ").append(colorNames[totalGroups]).append("\n");

            ArrayList<String> ids = proteinIds.get(hmNode);
            for (String proteinId : ids) {
                itolHmTemplate.append(proteinId).append(",").append(color).append("\n");
            }
        }
        write_SB_to_file_full_path(hmColorInfo.append(itolHmTemplate),
                inputPath.resolve("itol_templates").resolve("homology_groups.txt"));
    }

    /**
     * Creates an ITOL template that colors tree labels based on phenotype values
     * Generates one template per phenotype property
     * @param outputPath path to write files to
     * @param proteinIds the 'protein_ID' identifiers of mRNAs ordered per homology_group node
     * @param colorCodes hex color codes
     * @param colorNames name of color belonging to the hex code
     */
    private void createMsaMultiPhenoTemplate(Path outputPath, HashMap<Node, ArrayList<String>> proteinIds,
                                             String[] colorCodes, String[] colorNames) {

        HashMap<String, String> phenotypeMap = collectPhenotypes();
        if (phenotypeMap.isEmpty()) {
            return; // there was no phenotype information added
        }
        HashMap<String, HashSet<String>> phenotypeValues = combineAllPhenotypeValuesPerProperty(phenotypeMap, true);
        for (String phenotypeProperty : phenotypeValues.keySet()) {
            ArrayList<String> phenotypesList = new ArrayList<>();
            StringBuilder itolPhenoTemplate = new StringBuilder("DATASET_COLORSTRIP\nSEPARATOR COMMA\nDATASET_LABEL,label1\nCOLOR,#ff0000\n"
                    + "STRIP_WIDTH,40\nSHOW_INTERNAL,1\n\nDATA\n");
            HashMap<String, Integer> phenoCounterMap = new HashMap<>();
            AtomicInteger phenotypeCounter = new AtomicInteger(0);
            for (Node hmNode : proteinIds.keySet()) {
                ArrayList<String> ids = proteinIds.get(hmNode);
                for (String proteinId : ids) {
                    String[] protIdArray = proteinId.split("_");
                    String phenotypeValue = phenotypeMap.get(protIdArray[0] + "#" + phenotypeProperty);
                    if (phenotypeValue.equals("?") || phenotypeValue.toLowerCase().equals("unknown")) {
                        continue;
                    }
                    String color;
                    if (phenoCounterMap.containsKey(phenotypeValue)) { // this phenotype value already has color
                        int phenoCount = phenoCounterMap.get(phenotypeValue);
                        color = colorCodes[phenoCount];
                    } else {
                        if (phenotypeCounter.get() >= colorCodes.length) {
                           continue;
                        }
                        color = colorCodes[phenotypeCounter.get()];
                        phenotypesList.add(phenotypeValue);
                        phenoCounterMap.put(phenotypeValue, phenotypeCounter.get());
                        phenotypeCounter.getAndIncrement();
                    }
                    itolPhenoTemplate.append(proteinId).append(",").append(color).append("\n");
                }
            }

            int sequenceNr = 0;
            StringBuilder phenoColorInfo = new StringBuilder("Colors used for '" + phenotypeProperty + "' phenotype\n\n");
            if (phenotypeCounter.get() >= colorCodes.length) {
                phenoColorInfo.append("There were more than ").append(colorCodes.length).append(" values for this phenotype. ")
                        .append("Only selected the first ").append(colorCodes.length).append("\n");
            }

            for (String pheno : phenotypesList) {
                phenoColorInfo.append("#").append(pheno).append(" ").append(colorCodes[sequenceNr]).append(" ").append(colorNames[sequenceNr]).append("\n");
                sequenceNr ++;
            }
            write_string_to_file_full_path(phenoColorInfo + "\n" + itolPhenoTemplate,
                    outputPath.resolve("itol_templates").resolve(phenotypeProperty + ".txt"));
        }
    }

    /**
     * Splits sequences based on length
     * @param sequence sequence
     * @param length length to split sequence on
     * @return splitted sequence
     */
    private String splitSeqOnLength(String sequence, int length) {
        String newString = "";
        for (int i = 0; i < sequence.length(); i++) {
            char c = sequence.charAt(i);
            newString += c;
            if ((i+1) % length == 0) {
                newString += "\n";
            }
        }
        if (newString.endsWith("*")) {
            newString = newString.replaceFirst(".$","");
        }
        return newString;
    }

    /**
     * Gets missing genomes in an ArrayList
     * @param presentGenomes genomes that are present
     * @return missing genomes
     */
    private TreeSet<Integer> getMissingGenomesInArrayList(ArrayList<Integer> presentGenomes) {
        TreeSet<Integer> absent = new TreeSet<>();
        for (int i = 1; i <= total_genomes; ++i) {
            if (skip_array[i-1]) {
                continue;
            }
            if (!presentGenomes.contains(i)) {
                absent.add(i);
            }
        }
        return absent;
    }

    /**
     * //TODO integrate this function with removeDuplicatesFromALInt
     * @param sizeList
     * @return
     */
    private String determineFrequencyListInt(ArrayList<Integer> sizeList) {
        List<Integer> distinctSizeList = removeDuplicatesFromALInt(sizeList);
        StringBuilder output = new StringBuilder();
        for (int size : distinctSizeList) {
            int occurrences = Collections.frequency(sizeList, size);
            output.append(size).append("(").append(occurrences).append("x), ");
        }
        return output.toString().replaceFirst(".$", "").replaceFirst(".$", ""); // remove last character (2x)
    }

    /**
     * //TODO integrate this function with determineFrequencyListInt
     * @param list
     * @return
     */
    private List<Integer> removeDuplicatesFromALInt(ArrayList<Integer> list) {
        List<Integer> newList = list.stream()
                .distinct()
                .collect(Collectors.toList());
        Collections.sort(newList);
        return new ArrayList<>(newList);
    }

    /**
     * By default aligns sequences twice, once normal and second round with trimmed input.
     * @param trim can output be used for trimming?
     * @param stats
     *
     * NB: the global variable TRIMMING exists as well, this is used to run this function only once instead of trim.
     */
    public void alignSequences(boolean trim, boolean stats) {
        //TODO check which input exists and align that input

        check_if_program_exists_stderr("FastTree -h", 100, "Fasttree", true); // check if program is set to the $PATH
        check_if_program_exists_stderr("mafft -h", 100, "MAFFT", true); // check if program is set to the $PATH

        alignSequenceLoop(false);
        if (trim) { // when run from commandline: trim and restart to align the trimmed sequences
            trimSequences(); //trims sequences + sets msaNames to current set
            Pantools.logger.info("Trimmed sequences, restarting MSA.");
            alignSequenceLoop(true); // restarting this function to align the trimmed sequences
        }

        if (stats) { // this only happens when this function calls itself
            try {
                createMsaOutput(trim);
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Pantools.logger.error("This error is likely caused by skipping different genomes for this run compared to a previous run.");
                throw new RuntimeException(e);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("Something went wrong while writing the output files");
            }
            printResultsMsa(false);
        }
    }

    /**
     * Runs Mafft and FastTree on trimmed or untrimmed data.
     *
     * @param trim run on trimmed data or not
     */
    private void alignSequenceLoop(boolean trim) {
        boolean pass = false; // pass when all alignment (& tree) files exists
        int loopCounter = 0;
        while (!pass) {
            Pantools.logger.debug("Checking if MSA is complete for: {}.", msaNames);
            ArrayList<String> toBeAlignedList = checkIfMsaIsComplete(msaNames, trim);
            if (toBeAlignedList.isEmpty()) {
                pass = true;
                continue;
            }

            runParallelMafft(msaNames, trim);
            if (!FAST) {
                runParallelFasttree(msaNames, trim);
            } else {
                Pantools.logger.info("No gene trees are generated because --fast was included.");
            }

            loopCounter ++;
            if (loopCounter == 3) {
                Pantools.logger.error("Failed to align {} groups: {}.", toBeAlignedList.size(), toBeAlignedList);
                throw new RuntimeException("Failed aligning sequences");
            }
        }
    }

    /**
     * Go over all (selected) alignments to identify phenotype shared/specific/exclusive output
     * @param trimmedSequences whether the alignment was trimmed or not
     * @throws IOException if any of the newick files, phenotype output files, or the summary cannot be written
     */
    public void createPhenotypeMsaOutput(boolean trimmedSequences) throws IOException {
        Pantools.logger.info("Identifying phenotype shared/specific/exclusive output.");

        // setting variables
        String trimmedStr = trimmedSequences ? "_trimmed" : "";

        // collect all phenotypes
        HashMap<String, String> phenotypeMap = collectPhenotypes();
        if (phenotypeMap.isEmpty()) {
            Pantools.logger.warn("No phenotype information is available.");
            return; // there was no phenotype information added
        }
        HashMap<String, HashSet<String>> phenotypeValues = combineAllPhenotypeValuesPerProperty(phenotypeMap, false);

        // collect all groups with a phenotype position
        HashMap<String, ArrayList<String>> groupsWithPhenoPositions = new HashMap<>();
        for (String groupId : msaNames) {
            Path inputFile = outDirMsa.resolve(groupId).resolve("output").resolve(alignmentTypeShort + trimmedStr + ".afa");
            for (String phenotypeProperty : phenotypeValues.keySet()) {
                createNewNewickFileForPhenotype(inputFile, phenotypeProperty, phenotypeMap);
            }

            HashSet<String> foundPhenoCategories = determinePhenotypeSnps(outDirMsa.resolve(groupId), inputFile,
                    phenotypeValues, alignmentTypeShort, trimmedStr, phenotypeMap);

            for (String phenotypeKey : foundPhenoCategories) {
                // phenotypeKey is combination of shared/specific/exclusive +"#" phenotype property
                ArrayList<String> groupIds = groupsWithPhenoPositions.get(phenotypeKey);
                if (groupIds == null) {
                    groupIds = new ArrayList<>();
                }
                groupIds.add(groupId);
                groupsWithPhenoPositions.put(phenotypeKey, groupIds);
            }
        }

        createSummaryGroupsWithPhenoChange(phenotypeValues, groupsWithPhenoPositions);

        printResultsMsa(true);
    }

    /**
     * Collect all phenotype properties and values in the pangenome
     * @return key is genome number combined with a phenotype property. Value is the phenotype value.
     */
    private HashMap<String, String> collectPhenotypes() {
        Pantools.logger.debug("Collecting all phenotype properties and values in the pangenome.");

        HashMap<String, String> phenotypeMap = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> phenotypeNodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (phenotypeNodes.hasNext()) {
                Node phenotypeNode = phenotypeNodes.next(); // one phenotype node per genome
                Iterable<String> phenotypeProperties = phenotypeNode.getPropertyKeys();
                int genomeNr = (int) phenotypeNode.getProperty("genome");
                for (String property : phenotypeProperties) {
                    if (property.equals("genome")) {
                        continue;
                    }
                    Object value = phenotypeNode.getProperty(property);

                    if (value instanceof Integer || value instanceof Double) {
                        phenotypeMap.put(genomeNr + "#" + property, String.valueOf(value));
                    }

                    if (value instanceof Boolean) {
                        boolean booleanValue = (boolean) value;
                        phenotypeMap.put(genomeNr + "#" + property, String.valueOf(booleanValue));
                    } else if (value instanceof String) {
                        String valueStr = String.valueOf(value);
                        if (valueStr.equals("") || valueStr.toUpperCase().equals("UNKNOWN") || valueStr.equals("?")  ) {
                            valueStr = "unknown";
                        }
                        phenotypeMap.put(genomeNr + "#" + property, valueStr);
                    }
                }
            }
            tx.success();
        } catch (NotFoundException nfe) {
            System.out.println("Unable to start the database");
            throw new RuntimeException(nfe.getMessage());
        }
        return phenotypeMap;
    }

    /**
     * Include phenotype information into a newick formatted phylogeny.
     * @param input_afa an alignment in CLUSTAL (afa) format
     * @param phenotypeProperty a phenotype property
     * @param phenotypeMap key is genome number combined with a phenotype property. Value is the phenotype value
     * @throws IOException If the new newick files cannot be created
     */
    private void createNewNewickFileForPhenotype(Path input_afa, String phenotypeProperty,
                                                 HashMap<String, String> phenotypeMap) throws IOException {
        if (FAST) {
            return;
        }

        Path outPath = input_afa.getParent().getParent(); // alignment directory holding 'input' and 'output' folders
        String baseFileName = input_afa.getFileName().toString().replace(".afa","");

        Files.createDirectories(outPath.resolve("output").resolve("phenotype_trees") );
        Path genome_order_file = outPath.resolve("input").resolve("genome_order.info");
        ArrayList<String> genome_order_list = Classification.read_genome_order_file(genome_order_file, false);

        Path input_fasta = outPath.resolve("output").resolve(baseFileName + ".fasta");
        Path input_newick = outPath.resolve("output").resolve(baseFileName + ".newick");
        Path output_newick = outPath.resolve("output").resolve("phenotype_trees").resolve(baseFileName + "_" + phenotypeProperty + ".newick");

        // read the alignment FASTA file to extract the complete mrna identifiers. In the .afa they are cut short
        HashMap<String, String> sequence_phenotype = new HashMap<>();
        int sequence_counter = 0;
        try (BufferedReader file = Files.newBufferedReader(input_fasta)) {
            while (file.ready()) {
                String line = file.readLine().trim();
                if (line.startsWith(">")) {
                    String genomeNr = genome_order_list.get(sequence_counter);
                    String phenotypeValue = phenotypeMap.get(genomeNr + "#" + phenotypeProperty);
                    phenotypeValue = phenotypeValue .replace(" ","_");
                    String[] lineArray = line.split(" ");
                    sequence_phenotype.put(lineArray[0].replace(">",""), genomeNr + " " + phenotypeValue);
                }
            }
        } catch (IOException ioe) {
            System.out.println("Unable to read: " + input_fasta + "\n");
            throw new RuntimeException(ioe.getMessage());
        }

        // go over the single line of the newick file to include the phenotype values to the existing identifiers
        try (BufferedReader file = Files.newBufferedReader(input_newick)) {
            while (file.ready()) {
                String line = file.readLine().trim();
                for (String key : sequence_phenotype.keySet()) {
                    String value = sequence_phenotype.get(key);
                    String[] value_array = value.split(" ");
                    line = line.replace(key, value_array[1] + "_" + key);
                }
                write_string_to_file_full_path(line, output_newick);
            }
        } catch (IOException ioe) {
            System.out.println("Unable to read: " + input_newick + "\n");
            throw new RuntimeException(ioe.getMessage());
        }
    }

    /**
     * Creates a file that shows, for every phenotype in which groups/region/function a certain (phenotypic) variations are found
     * @param phenotypeValues a hashmap that holds all phenotype values for a phenotype property (key)
     * @param groupsWithPhenoPositions holds homology_group identifiers for a key consisting of:
     *                                  either SPECIFIC, EXCLUSIVE, SHARED, combined with a phenotype property
     * @throws IOException if the summary file cannot be created
     */
    private void createSummaryGroupsWithPhenoChange(HashMap<String, HashSet<String>> phenotypeValues,
                                                    HashMap<String, ArrayList<String>> groupsWithPhenoPositions) throws IOException {
        Pantools.logger.debug("Creating summary file for groups with phenotype changes.");

        if (msaMethod.equals("multiple_groups")) {
            Pantools.logger.warn("The summary file cannot be created for the multiple-groups method.");
            return; // this MSA method performs only 1 alignment and does not require the summary file
        }

        String outputFileName = alignmentInputType + "_with_phenotype_changes_" + alignmentTypeShort + ".txt";
        delete_file_full_path(outDirMsa.resolve(outputFileName));
        BufferedWriter out = Files.newBufferedWriter(outDirMsa.resolve(outputFileName));
        String[] categories = new String[]{"specific","exclusive","shared"};
        out.write("#Three categories: #SPECIFIC, #EXCLUSIVE, #SHARED\n\n");
        out.write("#" + phenotypeValues.size() + " included phenotypes\n");
        for (String phenotypeProperty : phenotypeValues.keySet()) {
            out.write("#" + phenotypeProperty + ": ");
            HashSet<String> values = phenotypeValues.get(phenotypeProperty);
            out.write(values.toString().replace("[", "").replace("]", "").replace(" ", ""));
            out.write("\n");
        }
        out.write("\n");

        for (String category : categories) {
            out.write("\n#" + category.toUpperCase() + "\n" );
            for (String phenotypeProperty : phenotypeValues.keySet()) {
                HashSet<String> values = phenotypeValues.get(phenotypeProperty);
                for (String phenotypeValue : values) {
                    if (groupsWithPhenoPositions.containsKey(category + "#" + phenotypeValue)) {
                        out.write(phenotypeProperty + " " + category + ": " + phenotypeValue + "\n" +
                                groupsWithPhenoPositions.get(category + "#" + phenotypeValue).toString()
                                        .replace("[", "").replace("]", "").replace(", ", ",") + "\n");
                    }
                }
            }
        }
        out.close();
    }

    /**
     * Create a hashmap for phenotypes. The key is a phenotype property with the value beining all possible phenotype values
     * @param phenotypeMap key is genome number combined with a phenotype property. Value is the phenotype value.
     * @param allowNumbers phenotypes values are allowed to be integers/doubles/floats
     * @return a hashmap that holds all phenotype values for a phenotype property
     */
    private HashMap<String, HashSet<String>> combineAllPhenotypeValuesPerProperty(HashMap<String, String> phenotypeMap, boolean allowNumbers) {
        Pantools.logger.debug("Combining all phenotype values per property.");

        HashMap<String, HashSet<String>> phenotypeValues = new HashMap<>();
        for (String phenotypekey : phenotypeMap.keySet()) {
            String phenotypeValue = phenotypeMap.get(phenotypekey);
            if (!allowNumbers) { // check if number
                try { // is it an Integer?
                    int i = Integer.parseInt(phenotypeValue);
                    continue;
                } catch (NumberFormatException ignored) {

                }

                try { // is it a Double?
                    double d = Double.parseDouble(phenotypeValue);
                    continue;
                } catch (NumberFormatException ignored) {

                }
            }

            String[] keyArray = phenotypekey.split("#"); // genome number # phenotype name
            HashSet<String> values = new HashSet<>();
            if (phenotypeValues.containsKey(keyArray[1])) {
                values = phenotypeValues.get(keyArray[1]);
            }
            values.add(phenotypeValue);
            phenotypeValues.put(keyArray[1], values);
        }

        // phenotypes with numerical values should be excluded
        // now they may still exists with only 'unknown' values
        ArrayList<String> deleteProperties = new ArrayList<>();
        for (String phenotypeProperty : phenotypeValues.keySet()) {
            HashSet<String> values = phenotypeValues.get(phenotypeProperty);
            if (!allowNumbers && values.size() == 1 && values.contains("unknown")) {
                deleteProperties.add(phenotypeProperty);
            }
        }

        for (String phenotypeProperty : deleteProperties) {
            phenotypeValues.remove(phenotypeProperty);
        }
        return phenotypeValues;
    }

    /**
     * Main body for two functionalities:
     *  - calculation of variable & informative positions
     *  - create similarity & identity matrices
     * @param trimmedSequences whether the alignment was trimmed or not
     * @throws IOException if any of the files cannot be created
     */
    private void createMsaOutput(boolean trimmedSequences) throws IOException {
        Pantools.logger.info("Reading alignments from {} for calculating variable/informative positions.", outDirMsa);
        String trimmedStr = trimmedSequences ? "_trimmed" : "";

        //setting match is required for Classification.count_var_inf_sites_in_msa to calculate sequence similarity
        match = FileUtils.loadScoringMatrix("BLOSUM" + BLOSUM);
        ArrayList<String> groups_with_inf_sites = new ArrayList<>();
        ArrayList<String> groups_with_var_sites = new ArrayList<>();
        long[] total_var_inf_sites = new long[2]; // variable, informative (sites for all groups)

        int group_counter = 1;
        for (String groupId : msaNames) {
            Path groupDirectory = outDirMsa.resolve(groupId);
            Files.createDirectories(groupDirectory.resolve("output").resolve("var_inf_positions"));
            Files.createDirectories(groupDirectory.resolve("output").resolve("similarity_identity"));
            HashMap<String, Integer> shared_snps_map = new HashMap<>();
            Pantools.logger.debug("Reading {} alignment of {}: {}/{}. variable/informative sites: {}/{}.", alignmentType, groupId, group_counter, msaNames.size(), total_var_inf_sites[0], total_var_inf_sites[1]);
            group_counter ++;

            Path genome_order = groupDirectory.resolve("input").resolve("genome_order.info");
            Path input_file = groupDirectory.resolve("output").resolve(alignmentTypeShort + trimmedStr + ".afa");
            int[] var_inf_sites = Classification.count_var_inf_sites_in_msa(shared_snps_map,
                        input_file, groupId, alignmentType, genome_order, trimmedStr); // variable, informative, conserved sites

            if (var_inf_sites[0] > 0) { // when variable sites are found
                groups_with_var_sites.add(groupId);
                total_var_inf_sites[0] += var_inf_sites[0];
            }
            if (var_inf_sites[1] > 0) { // when parsimony informative sites are present
                groups_with_inf_sites.add(groupId);
                total_var_inf_sites[1] += var_inf_sites[1];
            }
            Classification.create_shared_site_matrices(shared_snps_map, groupDirectory.resolve("output").resolve("var_inf_positions"), var_inf_sites[0],
                    "", alignmentTypeShort + trimmedStr, genome_order);
            Classification.create_shared_site_matrices(shared_snps_map, groupDirectory.resolve("output").resolve("var_inf_positions"), var_inf_sites[1],
                        "#inf", alignmentTypeShort + trimmedStr, genome_order);
            if (alignmentType .equals("protein")) {
                Classification.create_similarity_matrices(shared_snps_map,
                        groupDirectory.resolve("output").resolve("similarity_identity").resolve(alignmentTypeShort + "_trimmed_similarity.csv"),
                            var_inf_sites[0], var_inf_sites[2], genome_order);
            }
            Classification.create_identity_matrices(shared_snps_map, var_inf_sites[0], var_inf_sites[2],
                         groupDirectory.resolve("output").resolve("similarity_identity").resolve(alignmentTypeShort + "_trimmed_identity.csv"), genome_order);
        }

        String output = "# " + groups_with_var_sites.size() + " groups with variable sites:\n"
                + groups_with_var_sites.toString().replace("[","").replace(" ","").replace("]","") + "\n\n" +
                "# " + groups_with_inf_sites.size() + " groups with parsimony informative positions:\n"
                + groups_with_inf_sites.toString().replace("[","").replace(" ","").replace("]","");

        write_string_to_file_full_path(output, outDirMsa.resolve(alignmentInputType + "_with_var_inf_positions_" + alignmentTypeShort + ".txt"));

        Pantools.logger.info("{} variable sites: {} ({} alignments)", alignmentTypeCapital, total_var_inf_sites[0], groups_with_var_sites.size());
        Pantools.logger.info("{} informative sites: {} ({} alignments)", alignmentTypeCapital, total_var_inf_sites[1], groups_with_inf_sites.size());
    }

    /**
     * Identifies phenotype specific/shared/exclusive positions in an alignment
     * @param outputPath base directory of msa. subdirectories are /input/ and /output/
     * @param inputFile mafft CLUSTAL output (.afa) format
     * @param phenotypeValues a hashmap that holds all phenotype values for a phenotype property
     * @param nucleotideOrProteinShort string that is either "nuc" or "prot"
     * @param trimmedStr string that is either "" or "_trimmed"
     * @param phenotypeMap key is genome number combined with a phenotype property. Value is the phenotype value.
     * @return hashset with strings that state which phenotypes have at least one shared/specific/exclusive for the current alignment
     * @throws IOException if the phenotype output file cannot be written
     */
    private HashSet<String> determinePhenotypeSnps(Path outputPath, Path inputFile,
                                                   HashMap<String, HashSet<String>> phenotypeValues, String nucleotideOrProteinShort,
                                                   String trimmedStr, HashMap<String, String> phenotypeMap) throws IOException {

        ArrayList<String> phenotypeProperties = new ArrayList<>(phenotypeValues.keySet());
        Path genome_order_file = outputPath.resolve("input").resolve("genome_order.info");
        ArrayList<String> genome_order_list = Classification.read_genome_order_file(genome_order_file, false);
        String [] alignmentArray = new String[genome_order_list.size()]; // sequences are added in next function
        ArrayList<Integer> variablePositions = Classification.read_mafft_alignment(inputFile, alignmentArray, genome_order_list.size());
        // Delete last element because this is the alignment length
        variablePositions.remove(variablePositions.size() - 1);

        String[][] alignmentPositions = splitSequenceArray(alignmentArray);
        HashMap<String, Integer> phenotype_threshold_map = preparePhenotypeThresholds(genome_order_list, phenotypeProperties, phenotypeMap);

        String[] letters = new String[]{"A","T","C","G","-", "O"}; // [A T C G - other]
        if (nucleotideOrProteinShort.equals("prot")) {
            letters = new String[]{"A","R","N","D","C","Q","E","G","H","I","L","K","M","F","P","S","T","W","Y","V","-","O"}; //
        }
        ArrayList<String> allowedLetters = new ArrayList<>(Arrays.asList(letters));
        allowedLetters.remove("O");

        outputPath.resolve("output").resolve("phenotype").toFile().mkdirs();
        BufferedWriter[] writers = new BufferedWriter[phenotypeValues.size()];
        for (int i = 0; i < phenotypeValues.size(); i++) {
            writers[i] = new BufferedWriter(new FileWriter(
                    outputPath.resolve("output").resolve("phenotype").resolve(nucleotideOrProteinShort + trimmedStr + "_" + phenotypeProperties.get(i) + "_" + phenotype_threshold + ".csv").toString()));
            writers[i].write("Position,Letter,total count,");
            HashSet<String> values = phenotypeValues.get(phenotypeProperties.get(i));
            for (String phenotypeValue : values) {
                writers[i].write(phenotypeValue + ",");
            }
            writers[i].write("Specific,Exclusive,Shared\n");
        }

        HashSet<String> foundPhenoCategories = new HashSet<>();
        for (int position : variablePositions) { // go (only) through the variable positions
            for (String phenotype : phenotypeProperties) { // go over all phenotypes (properties)
                HashMap<String, Integer> phenotypeCounts = new HashMap<>();
                int[] letterFrequency = new int[letters.length];
                for (int i = 0; i < alignmentPositions.length; i++) { // go over all sequences
                    String genomeOfSequence = genome_order_list.get(i);
                    String letter = alignmentPositions[i][position].toUpperCase();
                    if (!allowedLetters.contains(letter)) {
                        letter = "O"; // O for other
                    }

                    increasePositionCounts(letter, letterFrequency, allowedLetters);
                    String phenotypeValue = "unknown";

                    if (phenotypeMap.containsKey(genomeOfSequence + "#" + phenotype)) {
                        phenotypeValue =phenotypeMap.get(genomeOfSequence + "#" + phenotype);
                    }
                    int count = 1;
                    if (phenotypeCounts.containsKey(alignmentPositions[i][position] + "#" + phenotypeValue)) {
                        count = phenotypeCounts.get(alignmentPositions[i][position] + "#" + phenotypeValue);
                        count++;
                    }
                    phenotypeCounts.put(letter + "#" + phenotypeValue, count);
                }

                // check if phenotype shared/specific/shared
                HashSet<String> values = phenotypeValues.get(phenotype);
                for (int i = 0; i < letterFrequency.length; i++) {
                    StringBuilder line = new StringBuilder();
                    if (letterFrequency[i] > 0) {
                        line.append((position+1)).append(",").append(letters[i]).append(",").append(letterFrequency[i]).append(",");
                        String exclusive = "";
                        ArrayList<String> shared = new ArrayList<>();
                        for (String phenoProperty : values) {
                            int count = 0;
                            if (phenotypeCounts.containsKey(letters[i] + "#" + phenoProperty)) {
                                count = phenotypeCounts.get(letters[i] + "#" + phenoProperty) ;
                            }
                            line.append(count).append(",");
                            if (phenoProperty.equals("unknown")) {
                                continue;
                            }

                            if (!phenotype_threshold_map.containsKey(phenotype + "#" + phenoProperty)) {
                                continue;
                            }
                            //System.out.println(phenotype + "#" + phenoProperty);
                            int threshold = phenotype_threshold_map.get(phenotype + "#" + phenoProperty);
                            if (count > 0 && exclusive.equals("")) {
                                exclusive = phenoProperty; // its EXCLUSIVE
                            } else if (count > 0) {
                                exclusive = "NO";
                            }
                            if (count >= threshold) { // its SHARED
                                shared.add(phenoProperty);
                            }
                        }

                        String specific = "";
                        if (!exclusive.equals("NO") && !exclusive.equals("") && shared.contains(exclusive)) {
                            specific = exclusive; // its SPECIFIC, remove exclusive and shared
                            shared.remove(exclusive);
                            exclusive = "";
                        } else if (exclusive.equals("NO")) {
                            exclusive = "";
                        }
                        updateFoundPhenoCategories(foundPhenoCategories, specific, exclusive, shared);
                        int index = phenotypeProperties.indexOf(phenotype);
                        writers[index].write(line + specific + "," +  exclusive + "," +
                                shared.toString().replace("[","").replace("]","").replace(", ",";") + "\n");
                    }
                }
            }
        }

        for (BufferedWriter writer : writers) {
            writer.close();
        }
        return foundPhenoCategories;
    }

    /**
     * Add phenotype property keys to foundPhenoCategories if the current position is shared/specific/exclusive
     * @param foundPhenoCategories holds strings that state which phenotypes have at least one shared/specific/exclusive for the current alignment
     * @param specific String that can either be "" or a phenotype property
     * @param exclusive String that can either be "" or a phenotype property
     * @param shared list with strings. Can be empty or contain multiple phenotype properties
     */
    private void updateFoundPhenoCategories(HashSet<String> foundPhenoCategories,
                                                 String specific, String exclusive, ArrayList<String> shared) {
        if (!specific.equals("")) {
            foundPhenoCategories.add("specific#" + specific);
        }

        if (!exclusive.equals("")) {
            foundPhenoCategories.add("exclusive#" + specific);
        }

        for (String phenotypeProperty : shared) {
            foundPhenoCategories.add("shared#" + phenotypeProperty);
        }
    }

    /**
     * Threshold is dependent on genomes found in the alignment, and --phenotype-threshold argument
     * @param genomesOfAlignmentlist a list with genome numbers. A number for every sequence in the alignment. Thus a genome can occur multiple times.
     * @param phenotypeProperties list with phenotype properties
     * @param phenotypeMap key is genome number combined with a phenotype property. Value is the phenotype value.
     * @return hashmap with thresholds. A key consists of a phenotype property + "#" + phenotype value. Hashmap value is the threshold.
     */
    private HashMap<String, Integer> preparePhenotypeThresholds(ArrayList<String> genomesOfAlignmentlist,
                                                                ArrayList<String> phenotypeProperties,
                                                                HashMap<String, String> phenotypeMap) {

        HashMap<String, Integer> phenotypeThresholds = new HashMap<>();
        for (String genomeNr : genomesOfAlignmentlist) {
            for (String phenotypeProperty : phenotypeProperties) {
                String phenotypeValue = phenotypeMap.get(genomeNr + "#" + phenotypeProperty);
                if (phenotypeValue == null || phenotypeValue.equals("unknown")) {
                    continue;
                }
                int value = 0;
                if (phenotypeThresholds.containsKey(phenotypeProperty + "#" + phenotypeValue)) {
                    value = phenotypeThresholds.get(phenotypeProperty + "#" + phenotypeValue);
                }
                value++;
                phenotypeThresholds.put(phenotypeProperty + "#" + phenotypeValue, value);
            }
        }

        if (phenotype_threshold < 100) { // --phenotype-threshold was included
            for (String phenotypeKey : phenotypeThresholds.keySet()) {
                double newPhenoThreshold = Math.round((double) phenotypeThresholds.get(phenotypeKey) * (phenotype_threshold/100.0));
                if (newPhenoThreshold == 0) {
                    newPhenoThreshold = 1;
                }
                phenotypeThresholds.put(phenotypeKey,  (int) newPhenoThreshold);
            }
        }
        return phenotypeThresholds;
    }

    /**
     * Increase the counts in 'letterFrequency'. Position is determined by 'allowedLetters'
     * @param letter a single letter.
     * @param letterFrequency always size of 'allowedLetters' +1 .
     * @param allowedLetters hold 4 nucleotide (and gap) OR 20 amino acid letters
     */
    private void increasePositionCounts(String letter, int[] letterFrequency, ArrayList<String> allowedLetters) {
        int index = allowedLetters.indexOf(letter);
        if (index == -1) {
            letterFrequency[letterFrequency.length-1]++; // other
        } else {
            letterFrequency[index]++; // other
        }
    }

    /**
     * Furthers split the MAFFT alignment in a two dimensional array[][]
     * @param alignmentArray holds all aligned sequences in their original order
     * @return alignment in a two dimensional array[][]
     */
    private String[][] splitSequenceArray(String[] alignmentArray) {
        String[][] alignmentPositions = new String[alignmentArray.length][0];
        for (int i = 0; i < alignmentArray.length; i++) {
            alignmentPositions[i] = alignmentArray[i].split("");
        }
        return alignmentPositions;
    }

    /**
     * Check if the required output files of a multiple sequence alignment exist.
     * @param hmNodeList arraylist with strings of homology group node (identifiers) that still need to be aligned
     * @param trim trimmed input?
     * @return arraylist of homology group nodes still to be aligned
     */
    private ArrayList<String> checkIfMsaIsComplete(ArrayList<String> hmNodeList, boolean trim) {
        ArrayList<String> toBeAlignedList = new ArrayList<>();

        String dot = ".";
        if (trim) {
            dot = dot.replace(".","_trimmed.");
        }

        String fileExtension;
        if (FAST) {
            fileExtension = "fasta";
        } else {
            fileExtension = "newick";
        }

        int doneCount = 0, notDoneCount = 0;
        for (String hmgroup : hmNodeList) {
            Path path = outDirMsa.resolve(hmgroup).resolve("output");

            if (path.resolve(alignmentTypeShort + dot + fileExtension).toFile().exists()) {
                doneCount ++;
            } else {
                toBeAlignedList.add(hmgroup);
                notDoneCount ++;
            }

            Pantools.logger.debug("Checking MSA completeness -> {} not done, {} done.", notDoneCount, doneCount);
            Pantools.logger.trace("Checked existence of: {}", path.resolve(alignmentTypeShort + dot + fileExtension));
        }

        return toBeAlignedList;
    }


    /**
     * MAFFT is run in parallel using ExecutorService
     * @param hmNodeList arraylist of homology group IDs
     * @param trim whether input sequences are trimmed or not
     */
    private void runParallelMafft(ArrayList<String> hmNodeList, boolean trim) {
        Pantools.logger.info("Aligning {} homology groups.", hmNodeList.size());

        String dot = ".";
        if (trim) {
            dot = dot.replace(".","_trimmed.");
        }

        //create threadpool for parallel running of MAFFT
        ExecutorService es = Executors.newFixedThreadPool(THREADS);
        List<Future<?>> futures = new ArrayList<>();

        for (String hmNode : hmNodeList) {
            Path path = outDirMsa.resolve(hmNode);

            Path input = path.resolve("input").resolve(alignmentTypeShort + dot + "fasta");
            Path output = path.resolve("output").resolve(alignmentTypeShort + dot + "afa");

            if (!output.toFile().exists()) {
                Future<?> f = es.submit(new runMafft(input, output));
                futures.add(f);
            } else {
                Pantools.logger.debug("Skipping alignment of {} because it already exists.", hmNode);
                Pantools.logger.trace("Skipping alignment of {} because it already exists: {}.", hmNode, output);
            }
        }

        //actually run mafft
        int counter = 1;
        int total = futures.size();
        for (Future<?> future : futures) {
            try {
                future.get();
                Pantools.logger.debug("Aligning sequences: {} / {}.", counter, total);
                counter++;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        es.shutdownNow(); //shutdown ExecutorService

        Pantools.logger.info("Performed {} multiple sequence alignments.", counter - 1);
    }

    /**
     * FastTree is run in parallel using ExecutorService
     * @param hmNodeList arraylist of homology group IDs
     * @param trim whether input is trimmed or not
     */
    private void runParallelFasttree(ArrayList<String> hmNodeList, boolean trim) {
        Pantools.logger.info("Building trees for {} multiple sequence alignments.", hmNodeList.size());

        String dot = ".";
        if (trim) {
            dot = dot.replace(".","_trimmed.");
        }

        //create threadpool for parallel running of fasttree
        ExecutorService es = Executors.newFixedThreadPool(THREADS);
        List<Future<?>> futures = new ArrayList<>();

        for (String groupId : hmNodeList) {
            Path path = outDirMsa.resolve(groupId);

            Path input = path.resolve("output").resolve(alignmentTypeShort + dot + "fasta");
            Path output = path.resolve("output").resolve(alignmentTypeShort + dot + "newick");

            if (!output.toFile().exists()) {
                Future<?> f = es.submit(new runFasttree(input, output, !alignProtein)); // !alignProtein because that indicates nucleotide alignment
                futures.add(f);
            } else {
                Pantools.logger.debug("Skipping tree inference for {} because it already exists.", groupId);
                Pantools.logger.trace("Skipping tree inference for {} because it already exists: {}.", groupId, output);
            }
        }

        //actually run fasttree
        int counter = 1;
        int total = futures.size();
        for (Future<?> future : futures) {
            try {
                future.get();
                Pantools.logger.debug("Phylogeny inference: {} / {}.", counter, total);
                counter++;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        es.shutdownNow(); //shutdown ExecutorService

        Pantools.logger.info("Finished building trees for {} multiple sequence alignments.", counter - 1);
    }

    /**
     * Converts afa format to fasta format
     * @param inputFasta original fasta file
     * @param inputAfa aligned fasta file in afa format
     * Requires original fasta input file and aligned output from mafft
     * mafft is run with a setting so that the original order of sequences remains
     */
    private void convertAfaToAlignedFasta(Path inputFasta, Path inputAfa) {
        HashMap<Integer, StringBuilder> sequenceMap = new HashMap<>();
        ArrayList<String> sequenceList = new ArrayList<>();
        int totalSequences = 0;
        try (BufferedReader in = Files.newBufferedReader(inputFasta)) { // read original fasta files
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (line.startsWith(">")) {
                    totalSequences++;
                    String key = line.replace(">","");
                    StringBuilder builder = new StringBuilder();
                    sequenceMap.put(totalSequences, builder);
                    sequenceList.add(key);
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read the input fasta: {}", inputFasta);
            throw new RuntimeException(ioe.getMessage());
        }

        try (BufferedReader in = Files.newBufferedReader(inputAfa)) {
            int lineCounter = 0, seqCounter = 0;
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                lineCounter ++;
                if (lineCounter == 1) {
                    if (!line.startsWith("CLUSTAL")) {
                        Pantools.logger.error("File is not in afa (CLUSTAL) format: {}.", inputAfa);
                        System.exit(1);
                    }
                }
                if (lineCounter < 4) {
                    continue;
                }
                seqCounter ++;
                if (seqCounter == totalSequences+1) { // first row after sequences with **
                    continue;
                }
                if (seqCounter == totalSequences+2) { // always an empty line
                    seqCounter = 0;
                    continue;
                }
                String[] lineArray = line.split(" ");
                StringBuilder seqBuilder = sequenceMap.get(seqCounter);
                seqBuilder.append(lineArray[lineArray.length-1]).append("\n");
                sequenceMap.put(seqCounter, seqBuilder);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read the input alignment: {}.", inputAfa);
            throw new RuntimeException(ioe.getMessage());
        }

        String outputFile = inputAfa.toString().replace(".afa", ".fasta");
        try (BufferedWriter out1 = new BufferedWriter(new FileWriter(outputFile))) {
            for (int i = 1; i <= totalSequences; i++) {
                StringBuilder value = sequenceMap.get(i);
                out1.write(">" + sequenceList.get(i-1) + "\n" +value.toString());
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read: {}.", outputFile);
            throw new RuntimeException(ioe.getMessage());
        }
    }

    /**
     * Trim nucleotide and protein input sequences based on the first PROTEIN alignment.
     *
     * 1. Read the protein alignment, find the longest gap at the end and start  (countAaToTrim)
     * 2. Trim sequences to remove gaps at the start and end of the alignment
     *
     * NB: Should be correct for both nucleotide and protein now
     */
    private void trimSequences() {
        Pantools.logger.info("Trimming alignments.");

        int groupCounter = 0;
        ArrayList<String> suitableGroups = new ArrayList<>();

        for (String groupId : msaNames) {
            groupCounter ++;
            Path path = outDirMsa.resolve(groupId);
            Path inPath = path.resolve("input");
            Path outPath = path.resolve("output");
            Pantools.logger.debug("Creating trimmed sequences: {} / {}.", groupCounter, msaNames.size());
            if (checkIfGroupIsTrimmed(groupId, inPath, suitableGroups, excludedGroups)) { // group is already trimmed
                continue;
            }

            HashMap<String, int[]> seqTrimMap = new HashMap<>();
            int[] longestStartEndGap;

            // key is the fasta header, value is array with number of aa at the start and end of the sequence that need to be trimmed
            longestStartEndGap = countAaToTrim(outPath.resolve(alignmentTypeShort + ".fasta").toString(), seqTrimMap); // gaps at start, gaps at end, alignment length
            if (longestStartEndGap[0] + longestStartEndGap[1] < longestStartEndGap[2]) {
                suitableGroups.add(groupId);
                trimSequences(seqTrimMap, groupId, inPath.resolve(alignmentTypeShort + ".fasta").toString());
            } else {
                excludedGroups.add(groupId);
            }

            createTrimmedInfoFile(longestStartEndGap, seqTrimMap, inPath);
        }

        if (excludedGroups.size() > 0) { // one ore multiple groups were excluded due to trimmed edges overlapping
            write_string_to_file_full_path(excludedGroups.toString().replace("[","").replace("]","").replace(" ",""),
                    outDirMsa.resolve("groups_excluded_based_on_trimming.txt"));
            Pantools.logger.info("Groups excluded based on trimming: {}; see {} for more information.",
                    excludedGroups.toString(),
                    outDirMsa.resolve("groups_excluded_based_on_trimming.txt"));
            Pantools.logger.debug("Groups excluded based on trimming: {}.", excludedGroups.toString());
        } else {
            Pantools.logger.info("No groups excluded based on trimming.");
        }

        if (suitableGroups.isEmpty()) {
            Pantools.logger.warn("None of the trimmed alignments can be used for the second alignment round. Use '--no-trimming' to further analyze them.");
        } else {
            // msaNames is a global variable and is read by this function again after restarting
            this.msaNames = suitableGroups;
        }
    }

    /**
     * Check if a alignments is trimmed
     * @param groupId alignments ID
     * @param inPath input path
     * @param suitableGroups alignments suitable for trimming
     * @param excludedGroups alignments not suitable for trimming
     * @return true if trimmed, false if not
     */
    private boolean checkIfGroupIsTrimmed(String groupId, Path inPath, ArrayList<String> suitableGroups, Set<String> excludedGroups) {
        boolean pass2 = check_if_file_exists(inPath.resolve("not_trimmed").toString());
        if (pass2) {
            excludedGroups.add(groupId); // long + str to convert to a string
            return true;
        }

        boolean pass1 = inPath.resolve(alignmentTypeShort + "_trimmed.fasta").toFile().exists();
        if (pass1) {
            suitableGroups.add(groupId);
        }

        return pass1;
    }

    /**
     * This function finds how many amino acids per sequence needs to be removed based on the longest start and stop gap lengths
     * 1. identify longest start and end gap in any sequence
     * 2. count number of gaps in all sequences according to the longest gaps
     *
     * @param protFastaFile
     * @param seqTrimMap
     * @return int[] array of length 3: gaps at start, gaps at end, alignment length
     */
    private int[] countAaToTrim(String protFastaFile, HashMap<String, int[]> seqTrimMap) {
        HashMap<String, String> sequenceMap = new HashMap<>(); // key is a fasta header, value is the sequence
        int alignmentLength;
        try {
            alignmentLength = readFastaForSequenceAndGaps(protFastaFile, seqTrimMap, sequenceMap);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        int longestStartGap = 0, longestEndGap = 0;
        for (String sequenceName : seqTrimMap.keySet()) {
            int[] gaps = seqTrimMap.get(sequenceName);
            if (gaps[0] > longestStartGap) {
                longestStartGap = gaps[0];
            }
            if (gaps[1] > longestEndGap) {
                longestEndGap = gaps[1];
            }
        }
        for (String sequenceName : seqTrimMap.keySet()) {
            int[] finalStartEndGaps = findNumberOfGapsWithoutBreak(sequenceMap.get(sequenceName),
                    longestStartGap, longestEndGap); // only find gaps in longest start and end gap
            seqTrimMap.put(sequenceName, finalStartEndGaps);
        }
        return new int[] {longestStartGap, longestEndGap, alignmentLength}; // gaps at start, gaps at end, alignment length
    }

    /**
     * Based on the longest start and end gaps that were found earlier, count the number of gaps for each sequence
     * @param sequence sequence
     * @param posUntilStartGap number of positions until start gap
     * @param posUntilEndGap number of positions until end gap
     * @return
     */
    private int[] findNumberOfGapsWithoutBreak(String sequence, int posUntilStartGap, int posUntilEndGap) {
        int startGapCounter = 0, endGapCounter = 0;
        for (int i = 0; i < posUntilStartGap; i++) {
            char aa = sequence.charAt(i);
            if (aa != '-') {
                startGapCounter ++;
            }
        }

        int endPosition = sequence.length()-1-posUntilEndGap;
        for (int i = sequence.length()-1; i > endPosition; i--) {
            char aa = sequence.charAt(i);
            if (aa != '-') {
                endGapCounter ++;
            }
        }
        return new int[]{startGapCounter, endGapCounter};
    }

    /**
     * Finds the longest start and end gap for each sequence
     *
     * @param protFastaFile
     * @param seqTrimMap
     * @param sequenceMap
     * @return
     */
    private int readFastaForSequenceAndGaps(String protFastaFile, HashMap<String, int[]> seqTrimMap, HashMap<String, String> sequenceMap) throws IOException {
        int alignmentLength = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader(protFastaFile));
            String prevSeq = "", sequence = "";
            //startEndGapArray = {0,0};
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.equals("")) {
                    continue;
                }
                if (line.contains(">")) {
                    if (!prevSeq.equals("")) {
                        int[] startEndGapArray = findNumberOfGaps(sequence, sequence.length(), sequence.length());
                        seqTrimMap.put(prevSeq, startEndGapArray);
                        sequenceMap.put(prevSeq, sequence);
                    }
                    prevSeq = line;
                    sequence = "";
                } else {
                    sequence += line;
                }
            }
            // when done, add the last sequence
            int[] startEndGapArray = findNumberOfGaps(sequence, sequence.length(), sequence.length());
            alignmentLength = sequence.length();
            seqTrimMap.put(prevSeq, startEndGapArray);
            sequenceMap.put(prevSeq, sequence);
        } catch (IOException e) {
            throw new IOException("Error reading file " + protFastaFile, e);
        }
        return alignmentLength;
    }

    /**
     * From the start and the end of the sequence, iterate over the sequence until no gap is found
     * @param sequence sequence
     * @param posUntilStartGap number of positions until start gap
     * @param posUntilEndGap number of positions until end gap
     * @return
     */
    private int[] findNumberOfGaps(String sequence, int posUntilStartGap, int posUntilEndGap) {
        int startGapCounter = 0, endGapCounter = 0;
        for (int i = 0; i < posUntilStartGap; i++) {
            char aa = sequence.charAt(i);
            if (aa == '-') {
                startGapCounter ++;
            } else {
                break;
            }
        }
        for (int i = sequence.length()-1; i > 0; i--) {
            char aa = sequence.charAt(i);
            if (aa == '-') {
                endGapCounter ++;
            } else{
                break;
            }
        }
        return new int []{startGapCounter, endGapCounter};
    }

    /**
     * Trims sequences
     * @param seqTrimMap
     * @param geneName
     * @param path
     */
    private void trimSequences(HashMap<String, int[]> seqTrimMap, String geneName, String path) {
        StringBuilder outputBuilder = new StringBuilder();
        String newPath = path.replace(".fasta", "_trimmed.fasta");
        int longestTrim = 0, shortestTrim = 999999;
        try {
            BufferedReader in = new BufferedReader(new FileReader(path));
            String sequence = "", prevSeq = "";
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.equals("")) {
                    continue;
                }

                if (line.contains(">")) {
                    if (prevSeq.length() > 1) {
                        int[] trimArray = seqTrimMap.get(prevSeq);
                        int trimSum = trimArray[0] + trimArray[1];
                        if (trimSum < shortestTrim && trimSum != 0) {
                            shortestTrim = trimSum;
                        }
                        if (trimSum > longestTrim) {
                            longestTrim = trimSum;
                        }
                        outputBuilder.append(prevSeq).append("\n");
                        trimSequence(sequence, trimArray, outputBuilder);
                    }
                    prevSeq = line;
                    sequence = "";
                } else {
                    sequence += line;
                }
            }

            int[] trimArray = seqTrimMap.get(prevSeq);
            int trimSum = trimArray[0] + trimArray[1];
            if (trimSum < shortestTrim && trimSum != 0) {
                shortestTrim = trimSum;
            }
            if (trimSum > longestTrim) {
                longestTrim = trimSum;
            }
            outputBuilder.append(prevSeq).append("\n");
            trimSequence(sequence, trimArray, outputBuilder);
        } catch (IOException e) {
            throw new RuntimeException("Error reading file " + path, e);
        }

        if (alignProtein) {
            longestTrim = longestTrim*3;
            shortestTrim = shortestTrim*3;
        }
        if (longestTrim < shortestTrim) {
            shortestTrim = longestTrim;
        }
        write_SB_to_file_full_path(outputBuilder, newPath);
    }

    /**
     * @param sequence
     * @param trimArray
     * @param outputBuilder
     */
    private void trimSequence(String sequence, int[] trimArray, StringBuilder outputBuilder) {
        int nucCount = 1, nucCountLoop = 0;
        int removeNucsStart = trimArray[0];
        int removeNucsEnd = trimArray[1];
        if (alignProtein) {
            removeNucsStart = removeNucsStart*3;
            removeNucsEnd = removeNucsEnd*3;
        }
        removeNucsEnd = sequence.length() - removeNucsEnd; // adjust the ending with the number of nucleotides
        String trimmedSeq = "";
        for (int i = 0; i < sequence.length(); i++) {
            char aa = sequence.charAt(i);
            nucCountLoop ++;
            if (nucCount <= removeNucsStart) {

            } else if (nucCount > removeNucsEnd) {

            } else {
                trimmedSeq += aa;
            }
            nucCount ++;
            if (nucCountLoop == 3) {
                nucCountLoop = 0;
            }
        }
        trimmedSeq = splitSeqOnLength(trimmedSeq, 80);
        outputBuilder.append(trimmedSeq).append("\n");
    }

    /**
     * Creates 'trimmed.info' or 'not_trimmed' when the group must be excluded
     *
     * @param longestStartEndGap
     * @param seqTrimMap
     * @param inPath
     */
    private void createTrimmedInfoFile(int[] longestStartEndGap, HashMap<String, int[]> seqTrimMap, Path inPath) {
        StringBuilder trimmed = new StringBuilder("Protein alignment length: " + longestStartEndGap[2]
                + "\nLongest start gap: " + longestStartEndGap[0]
                + "\nLongest end gap: " + longestStartEndGap[1]);

        if (longestStartEndGap[0] + longestStartEndGap[1] < longestStartEndGap[2]) {
            trimmed.append("\n\n#Total number of trimmed positions at the start and end of each protein sequence." +
                    " Multiply by three to obtain the number of positions for the nucleotide sequences.\n" +
                    "mRNA id,trimmed AA front,trimmed AA end\n");

            for (String sequenceName : seqTrimMap.keySet()) {
                int[] trimmedProtPositions = seqTrimMap.get(sequenceName);
                String[] seqArray = sequenceName.split(" ");
                trimmed.append(seqArray[0].replace(">", ""))
                        .append(",")
                        .append(trimmedProtPositions[0])
                        .append(",")
                        .append(trimmedProtPositions[1])
                        .append("\n");
            }
        } else {
            write_string_to_file_full_path("sequences to short", inPath.resolve("not_trimmed"));
            trimmed.append("\nNo trimmed sequence could be created.\n");
        }
        write_string_to_file_full_path(trimmed.toString(), inPath.resolve("trimmed.info"));
    }

    /**
     * Prints MSA results
     * @param phenotype whether we are printing the phenotype results
     */
    private void printResultsMsa(boolean phenotype) {
        if (!phenotype) {
            Pantools.logger.info("Output written to:");
            Pantools.logger.info(" {}", outDirMsa);
        } else {
            if (!msaMethod.equals("multiple_groups")) { // this MSA method performs only 1 alignment and did not create a summary file
                Pantools.logger.info("Phenotype output written to:");
                if (alignNucleotide) { // nucleotide sequences
                    Pantools.logger.info(" {}", outDirMsa.resolve(alignmentInputType + "_with_phenotype_changes_nuc.txt"));
                }
                if (alignProtein) { // protein sequences
                    Pantools.logger.info(" {}", outDirMsa.resolve(alignmentInputType + "_with_phenotype_changes_prot.txt"));
                }
            }
        }
    }

    /**
     * Gives the private variable hmNodeList
     * @return private variable hmNodeList
     * NB: this list IS NOT updated with trimming results!
     */
    public ArrayList<Node> getHmNodeList() {
        return hmNodeList;
    }


    /**
     * Checks if a homology group is excluded based on trimming
     * @param group homology group node identifier
     * @return boolean determining if the group is excluded
     */
    public boolean isExcluded(long group) {
        return excludedGroups.contains(Long.toString(group));
    }

    /**
     * Gives the private variable msaNames
     * @return private variable msaNames
     * NB: this list IS updated with trimming results!
     */
    public ArrayList<String> getMsaNames() {
        return msaNames;
    }

    /**
     * Return the path to the MSA output directory (outDirMsa)
     * @return outDirMsa
     */
    public Path getOutDirMsa() {
        return outDirMsa;
    }

    /**
     * Return the alignment type in short form: 'prot', 'nuc', 'var'
     * @return alignmentTypeShort
     */
    public String getAlignmentTypeShort() {
        return alignmentTypeShort;
    }

    /**
     * Small inner class for running MAFFT in a parallel fashion
     */
    private class runMafft implements Callable<String> {
        private final Path input;
        private final Path output;

        public runMafft(Path input, Path output) {
            this.input = input;
            this.output = output;
        }

        public String call() {
            String mafftCommand = "mafft --auto --anysymbol --quiet --thread 1 --clustalout " +
                    "--bl " + BLOSUM + " " +
                    input +
                    " 1> " +
                    output;

            Pantools.logger.debug("MAFFT command: {}", mafftCommand);

            runCommand(mafftCommand); //run MAFFT
            convertAfaToAlignedFasta(input, output); //convert output of MAFFT to fasta format

            return "Starting alignment of " + input;
        }

        private void runCommand(String command) {
            List<String> cmd = new ArrayList<>();
            cmd.add("sh");
            cmd.add("-c");
            cmd.add(command);
            ProcessBuilder pb = new ProcessBuilder(cmd);
            try {
                Process p = pb.start();
                p.waitFor();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    /**
     * Small inner class for running FastTree in a parallel fashion
     */
    private class runFasttree implements Callable<String> {
        private final Path input;
        private final Path output;
        private final boolean isNucleotide;
        private final Path log;

        public runFasttree(Path input, Path output, boolean isNucleotide) {
            this.input = input;
            this.output = output;
            this.isNucleotide = isNucleotide;
            this.log = Paths.get(output.toString() + ".log");
        }

        public String call() {
            String fasttree = "FastTree ";
            if (isNucleotide) {
                fasttree += "-nt ";
            }
            String fasttreeCommand = fasttree + input +
                    " 1> " + output +
                    " 2> " + log;

            Pantools.logger.debug("FastTree command: {}", fasttreeCommand);

            runCommand(fasttreeCommand); //run FastTree

            if (!Pantools.logger.isDebugEnabled()) {
                log.toFile().delete();
            }

            return "Starting alignment of " + input;
        }

        private void runCommand(String command) {
            List<String> cmd = new ArrayList<>();
            cmd.add("sh");
            cmd.add("-c");
            cmd.add(command);
            ProcessBuilder pb = new ProcessBuilder(cmd);
            try {
                Process p = pb.start();
                p.waitFor();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }
        }
    }
}
