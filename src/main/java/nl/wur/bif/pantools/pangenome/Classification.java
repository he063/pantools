/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.wur.bif.pantools.pangenome;

import cern.jet.math.Arithmetic;
import nl.wur.bif.pantools.cli.RemoveFunctions;
import nl.wur.bif.pantools.index.IndexDatabase;
import nl.wur.bif.pantools.index.IndexPointer;
import nl.wur.bif.pantools.index.IndexScanner;
import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.sequence.SequenceDatabase;
import nl.wur.bif.pantools.sequence.SequenceScanner;
import nl.wur.bif.pantools.utils.Utils;
import org.apache.commons.lang.ArrayUtils;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static nl.wur.bif.pantools.alignment.BoundedLocalSequenceAlignment.match;
import static nl.wur.bif.pantools.pangenome.FunctionalAnnotations.function_overview_per_group;
import static nl.wur.bif.pantools.pangenome.create_skip_arrays.create_skip_arrays;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Globals.RelTypes.has_homolog;
import static nl.wur.bif.pantools.utils.Globals.RelTypes.has_variant;
import static nl.wur.bif.pantools.utils.Utils.*;
import static org.neo4j.graphdb.Label.label;

/**
 * @author Eef Jonkheer, Bioinformatics group, Wageningen University, the Netherlands.
 */
public class Classification {

    public static String[] regions_to_search;
    private static int total_loops = 1000;
    private static BlockingQueue<Integer> loop_numbers;
    private static String percentage_counter;
    private static AtomicLong atomic_counter1;
    private static AtomicLong atomic_counter2;
    private static AtomicLong distinct_node_count;
    private static AtomicLong total_node_count;
    boolean compressed_kmers;
    private static ArrayList<String> selected_sequences;
    ConcurrentHashMap<Integer, ArrayList<Integer>> sequences_per_genome;
    public static boolean phased = false;
    
    private static HashMap<String, String> subgenome_nr_map;
    public static int[][] seq_distinct_total_kmers;
    public static int[][] seq_distinct_shared_kmers;
    public static int[][] seq_all_total_kmers;
    public static int[][] seq_all_shared_kmers;

    public static int[][] genome_distinct_total_kmers;
    public static int[][] genome_distinct_shared_kmers;
    public static int[][] genome_all_total_kmers;
    public static int[][] genome_all_shared_kmers;
    
    private BlockingQueue<String> string_queue;
    private BlockingQueue<int[]> frequency_queue;
    private BlockingQueue<Node> node_queue;
    private BlockingQueue<HashMap<Node, Integer>> hashmap_ni_queue;

    /*
     Requires 
     --database-path
    
    Optional 
     --skip or --reference
     --annotations-file
    */
    public void metrics() { 
        Pantools.logger.info("Generating metrics of the pangenome and for the individual genomes and sequences.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            throw new RuntimeException("Database not found");
        }
        if (PROTEOME) { // when run against a panproteome 
            new File(WORKING_DIRECTORY + "metrics").mkdir(); // create directory
            check_database(); // starts up the graph database if needed
        } else { // when run against a pangenome 
            connect_pangenome(); // starts up the graph, index and genome database
            System.out.print("\rStarting the pangenome index database");
            INDEX_SC = new IndexScanner(INDEX_DB);
            GENOME_SC = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_SC.get_pre_len());
            new File(WORKING_DIRECTORY + "metrics/GC").mkdirs(); // create directories
            System.out.print("\r                                                     "); // spaces are intentional 
        }
        
        ProteomeLayer proLayer = new ProteomeLayer();
        StringBuilder output_builder = new StringBuilder();
       
        System.out.print("\rGathering pangenome statistics");
        ArrayList<String> phenotype_properties = retrieve_phenotypes_properties();
        HashMap<String, String> all_phenotypes_map = retrieve_phenotypes_for_metrics(); 
        StringBuilder genome_csv_builder = create_header_for_genome_metrics_csv_file(phenotype_properties);
        HashMap<Integer, ArrayList<String>> annotations_map = get_all_annotation_identifiers();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            annotation_identifiers = get_annotation_identifiers(false, false, PATH_TO_THE_ANNOTATIONS_FILE); // do not print, USE the -af input file
            longest_transcripts = true;
            proLayer.find_longest_transcript_per_gene(false, 0);
            tx.success();
        }
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            HashMap<Integer, StringBuilder> busco_map = create_busco_summary(); 
            get_and_append_pangenome_statistics(output_builder);
            int[] annotations_array = count_nodes_per_genome(ANNOTATION_LABEL);
            HashMap<String, int[]> function_count_map = count_functions_for_metrics();
            int[][] hmgroup_counts = count_homology_groups_per_genome();
            int[] degenerate_nodes = gather_all_degenerate_nodes(true);
            add_sequences_and_functions_in_pangenome_to_output(output_builder, annotations_array, hmgroup_counts, function_count_map);
            HashMap<String, Integer> num_proteins_map = get_num_proteins_from_individual_genomes_map();
            HashMap<String, double[]> annotation_counts_per_seq = count_annotation_features_per_sequence();
            HashMap<String, Long> seq_length_map = get_length_per_sequence();
            HashMap<String, long[]> nuc_count_per_seq = new HashMap<>(); // number of A,T,C,G per sequence 
            for (int i=1; i <= total_genomes; i++) { 
                create_directory_in_DB("metrics/GC/" + i);
                System.out.print("\rGathering information for genome: " + i + "/" + total_genomes);
                if (skip_array[i-1]) {
                    output_builder.append("\n\nGenome ").append(i);
                    continue;
                }
                
                if (PROTEOME) { // panproteome
                    write_panproteome_anno_stats_to_builders(output_builder, genome_csv_builder, i, hmgroup_counts);
                } else { // pangenome
                    ArrayList<String> annotation_ids = annotations_map.get(i);
                    double[] sequence_values = count_occurrence_total_longest_shortest(SEQUENCE_LABEL, i); // total length, count, shortest, longest, lt count
                    //calculate_gene_orientation_blocks(i, sequence_values[1]); important for CIRCOS plots, DO NOT REMOVE
                    calculate_gc_content(i, (long) sequence_values[0], nuc_count_per_seq); 
                    long[] nucleotide_count = nuc_count_per_seq.get(i +""); // total number of A T C G in genome
                    write_genome_statistics_to_builders(output_builder, genome_csv_builder, i, sequence_values, nucleotide_count, annotation_ids, 
                            annotations_array, degenerate_nodes[(i*2)-2], phenotype_properties, all_phenotypes_map);
                    
                    if (busco_map.containsKey(i)) {
                        String busco_scores = busco_map.get(i).toString();
                        output_builder.append("\nBUSCO scores\n ").append(busco_scores);
                    }
                    int num_proteins = 0;
                    if (num_proteins_map.containsKey(i + "")) {
                        num_proteins = num_proteins_map.get(i + "");
                    }
                    write_pangenome_anno_stats_to_builders(output_builder, genome_csv_builder, i, sequence_values, num_proteins, hmgroup_counts);
                }    
                write_function_stats_to_builders(output_builder, genome_csv_builder, i, function_count_map);
                genome_csv_builder.append("\n"); 
            } 
            write_SB_to_file_in_DB(output_builder, "metrics/metrics.txt");
            write_SB_to_file_in_DB(genome_csv_builder, "metrics/metrics_per_genome.csv");
            output_builder.setLength(0); // to clear some memory
            genome_csv_builder.setLength(0); // to clear some memory
            create_per_sequence_statistics_csv(annotation_counts_per_seq, seq_length_map, nuc_count_per_seq, function_count_map);
            
            Pantools.logger.info("Output written to:");
            Pantools.logger.info(" {}metrics/metrics.txt", WORKING_DIRECTORY);
            Pantools.logger.info(" {}metrics/metrics_per_genome.csv", WORKING_DIRECTORY);
            if (!PROTEOME) {
                Pantools.logger.info(" {}metrics/metrics_per_sequence.csv", WORKING_DIRECTORY);
            }

            tx.success(); // transaction successful, commit changes
        }
    }
    
    /**
     * Count nodes with a specific label. The nodes must have the 'genome' property
     * @param node_label a node label
     * @return count per genome
     */
    public static int[] count_nodes_per_genome(Label node_label) {
        int[] count_array = new int[total_genomes];
        ResourceIterator<Node> nodes = GRAPH_DB.findNodes(node_label);
        while (nodes.hasNext()) {
            Node node = nodes.next();
            int genome_nr = (int) node.getProperty("genome");
            count_array[genome_nr-1] ++;
        }
        return count_array;
    }

    /**
     * Write the already retrieved values for functional annotations to the two output builders
     * @param output_builder genome.txt file
     * @param csv_builder genome.csv file
     * @param genome_nr a genome number
     * @param function_count_map
     */
    public void write_function_stats_to_builders(StringBuilder output_builder, StringBuilder csv_builder, int genome_nr, 
            HashMap<String, int[]> function_count_map) {
        
        int[] total_go = new int[1];
        int[] total_pfam = new int[1];
        int[] total_interpro = new int[1];
        int[] total_tigrfam = new int[1];
        int[] phobius_signalp = new int[1];
        int[] total_bgc = new int[1];
        int[] total_cog = new int[1];
        int[] mrna_with_function = new int[1];

        if (function_count_map.containsKey(genome_nr + "#GO")) {
            total_go = function_count_map.get(genome_nr + "#GO"); // [function nodes, connect to mRNAs, total relations]
        }
        if (function_count_map.containsKey(genome_nr + "#pfam")) {
            total_pfam = function_count_map.get(genome_nr + "#pfam");
        }
        if (function_count_map.containsKey(genome_nr + "#interpro")) {
            total_interpro = function_count_map.get(genome_nr + "#interpro");
        }        
        if (function_count_map.containsKey(genome_nr + "#tigrfam")) {
            total_tigrfam = function_count_map.get(genome_nr + "#tigrfam");
        }
        if (function_count_map.containsKey(genome_nr + "#phobius_signalp")) {
            phobius_signalp =function_count_map.get(genome_nr + "#phobius_signalp"); // [ phobius signalpeptide, phobius tranmembrane, signalp]
        } 
        if (function_count_map.containsKey(genome_nr + "#bgc")) {
            total_bgc = function_count_map.get(genome_nr + "#bgc");
        }
        if (function_count_map.containsKey(genome_nr + "#cog")) {
            total_cog = function_count_map.get(genome_nr + "#cog");
        } 

        if (function_count_map.containsKey(genome_nr + "#all")) {
            mrna_with_function = function_count_map.get(genome_nr + "#all");
        }
  
        String prot_or_mrna = " mRNA's", prot_or_mrna_cap = "mRNA's";
        if (PROTEOME) { // when run against a panproteome 
            prot_or_mrna = " proteins";
            prot_or_mrna_cap = "Proteins";
        }

        output_builder.append("\n\n").append(prot_or_mrna_cap).append(" with functional annotation: ").append(mrna_with_function[0]);
        csv_builder.append(mrna_with_function[0]).append(",");
        if (mrna_with_function[0] == 0) { // no function is present. any go, pfam, interpro, tigrfam, cog, (bgc), secreted protein, receptor, transmembrane
            csv_builder.append(",,,,,,,,,,,,,,,,,"); // 17 commas
            return;
        }
        
        if (phobius_signalp[0] > 0 || phobius_signalp[1] > 0 || phobius_signalp[2] > 0) { // Phobius annotations are present 
            output_builder.append("\nPhobius signal peptides: ").append(phobius_signalp[0])
                  .append("\nPhobius transmembrane domains: ").append(phobius_signalp[1])
                  .append("\nSignalP signal peptides: ").append(phobius_signalp[2]);
            csv_builder.append(phobius_signalp[0]).append(",").append(phobius_signalp[1]).append(",").append(phobius_signalp[2]).append(",");
        } else {
            csv_builder.append(",,,");
        }
        
        if (total_bgc[0] > 0) { // if biosynthethic gene clusters are present 
            output_builder.append("\nBiosynthetic gene clusters: ").append(total_bgc[0]);
            csv_builder.append(total_bgc[0]).append(",");
        } else {
            csv_builder.append(",");
        }
        
        if (total_cog[0] > 0) { // if COG functions are present 
            output_builder.append("\nCOG proteins: ").append(total_cog[0]);
            csv_builder.append(total_cog[0]).append(",");
        } else { 
            csv_builder.append(",");
        }
        
        if (total_go[0] > 0) { // when GO nodes are found
            output_builder.append("\n\nGO terms: ").append(total_go[1]).append(", total relationships: ").append(total_go[2])
                    .append(" to ").append(total_go[0]).append(prot_or_mrna);
            csv_builder.append(total_go[1]).append(",").append(total_go[2]).append(",").append(total_go[0]).append(",");
        } else {
            csv_builder.append(",,,");
        }
        
        if (total_pfam[0] > 0) { 
            output_builder.append("\nPfam domains: ").append(total_pfam[1]).append(", total relationships: ").append(total_pfam[2])
                    .append(" to ").append(total_pfam[0]).append(prot_or_mrna);
            csv_builder.append(total_pfam[1]).append(",").append(total_pfam[2]).append(",").append(total_pfam[0]).append(",");   
        } else {
            csv_builder.append(",,,");
        }
        
        if (total_interpro[0] > 0) { 
            output_builder.append("\nInterpro domains: ").append(total_interpro[1]).append(", total relationships: ").append(total_interpro[2])
                    .append(" to ").append(total_interpro[0]).append(prot_or_mrna);
            csv_builder.append(total_interpro[1]).append(",").append(total_interpro[2]).append(",").append(total_interpro[0]).append(",");
        } else {
            csv_builder.append(",,,");
        }
        
        if (total_tigrfam[0] > 0) {
            output_builder.append("\nTigrfam's: ").append(total_tigrfam[1]).append(", total relationships: ")
                    .append(total_tigrfam[2]).append(" to ").append(total_tigrfam[0]).append(prot_or_mrna).append("\n");
            csv_builder.append(total_tigrfam[1]).append(",").append(total_tigrfam[2]).append(",").append(total_tigrfam[0]).append(",");
        } else {
            csv_builder.append(",,,");
        }   
    }
    
    /**
     * Write various genome statistics to the two output builders 
     * @param output_builder
     * @param csv_builder
     * @param genome_nr
     * @param sequence_values
     * @param nucleotide_count
     * @param annotation_id_list
     * @param annotations_array 
     * @param degenerate_nodes
     * @param phenotype_properties
     * @param all_phenotypes_map
     */
    public void write_genome_statistics_to_builders(StringBuilder output_builder, StringBuilder csv_builder, int genome_nr, 
            double[] sequence_values, long[] nucleotide_count, ArrayList<String> annotation_id_list, int[] annotations_array, int degenerate_nodes, 
             ArrayList<String> phenotype_properties, HashMap<String, String> all_phenotypes_map) {
        
        String pheno_properties_string = "";
        for (String phenotype : phenotype_properties) {
            String pheno_value = all_phenotypes_map.get(genome_nr + "#" + phenotype);
            pheno_properties_string += pheno_value + ",";
        }
        
        String genome_name = get_genome_name(genome_nr);
        long[] n_statistics = calculate_N_statistics(genome_nr, (long) sequence_values[0]); // N25, L25, N50, L50, L75, N75, L90, N90, L95, N95,
        String gc_genome = get_percentage_str(nucleotide_count[2] + nucleotide_count[3], 
                nucleotide_count[0] + nucleotide_count[1] + nucleotide_count[2] + nucleotide_count[3], 2);
        String ambigious_percentage = get_percentage_str(nucleotide_count[4] + nucleotide_count[5], sequence_values[0], 2);

        String genome_size_str = b_Kb_Mb_or_Gb(sequence_values[0]);
        String avg_seq_len_str = b_Kb_Mb_or_Gb(sequence_values[0] / sequence_values[1]);
        String shortest_gene_str = b_Kb_Mb_or_Gb(sequence_values[2]);
        String longest_gene_str = b_Kb_Mb_or_Gb(sequence_values[3]);
        String annotation_id_str = "";
        String n25 = b_Kb_Mb_or_Gb(n_statistics[0]);
        String n50 = b_Kb_Mb_or_Gb(n_statistics[2]);
        String n75 = b_Kb_Mb_or_Gb(n_statistics[4]);
        String n90 = b_Kb_Mb_or_Gb(n_statistics[6]);
        String n95 = b_Kb_Mb_or_Gb(n_statistics[8]);
        if (annotation_id_list != null) {
            annotation_id_str = annotation_id_list.toString().replace("[","").replace("]","");
        } 
           
        output_builder.append("\n\n#Genome ").append(genome_nr).append(", ").append(genome_name)
                .append("\nGenome size: ").append(genome_size_str)
                .append("\nSequences: ").append((int) sequence_values[1])
                .append("\nNumber of annotations: ").append(annotations_array[genome_nr-1])
                .append("\nAnnotations: ").append(annotation_id_str)
                .append("\nCurrent annotation: ").append(annotation_identifiers.get(genome_nr-1))
                .append("\nNumber of degenerate k-mer nodes: ").append(degenerate_nodes)
                .append("\nShortest sequence: ").append(shortest_gene_str)
                .append("\nLongest sequence: ").append(longest_gene_str)
                .append("\nAverage sequence length: ").append(avg_seq_len_str)
                .append("\nGC content: ").append(gc_genome)
                .append("\nPercentage of genome unknown: ").append(ambigious_percentage)
                .append("%\nNumber of nucleotides:")
                .append("\n A: ").append(nucleotide_count[0])
                .append("\n T: ").append(nucleotide_count[1])
                .append("\n C: ").append(nucleotide_count[2])
                .append("\n G: ").append(nucleotide_count[3])
                .append("\n N: ").append(nucleotide_count[4])
                .append("\n Other letters: ").append(nucleotide_count[5])
                .append("\n\nN25: ").append(n25)
                .append("\nL25: ").append(n_statistics[1])
                .append("\nN50: ").append(n50)
                .append("\nL50: ").append(n_statistics[3])
                .append("\nN75: ").append(n75)
                .append("\nL75: ").append(n_statistics[5])
                .append("\nN90: ").append(n90)
                .append("\nL90: ").append(n_statistics[7])
                .append("\nN95: ").append(n95)
                .append("\nL95: ").append(n_statistics[9])
                .append("\n");
        
        String annotation_id = annotation_identifiers.get(genome_nr-1);
        if (annotation_id.endsWith("_0")) {
            annotation_id = "No annotation";
        }
        csv_builder.append(genome_nr).append(",")
                .append(pheno_properties_string) // already includes a comma 
                .append(annotation_id).append(",")
                .append(genome_size_str).append(",")
                .append(sequence_values[1]).append(",")
                .append(annotations_array[genome_nr-1]).append(",")
                .append(degenerate_nodes).append(",")
                .append(shortest_gene_str).append(",")
                .append(longest_gene_str).append(",")
                .append(avg_seq_len_str).append(",")
                .append(gc_genome).append(",")
                .append(ambigious_percentage).append(",")
                .append(nucleotide_count[0]).append(",")
                .append(nucleotide_count[1]).append(",")
                .append(nucleotide_count[2]).append(",")
                .append(nucleotide_count[3]).append(",")
                .append(nucleotide_count[4]).append(",")
                .append(nucleotide_count[5]).append(",")
                .append(n25).append(",")
                .append(n_statistics[1]).append(",")
                .append(n50).append(",")
                .append(n_statistics[3]).append(",")
                .append(n75).append(",")
                .append(n_statistics[5]).append(",")
                .append(n90).append(",")
                .append(n_statistics[7]).append(",")
                .append(n95).append(",")
                .append(n_statistics[9]).append(",");
    }
    
    /**
     * Retrieve properties stored in the 'pangenome' node 
     * @param output_builder 
     */
    public void get_and_append_pangenome_statistics(StringBuilder output_builder) {
        Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
        String date = (String) pangenome_node.getProperty("date");
        output_builder.append("Database creation data: ").append(date).append("\n\n");
        if (PROTEOME) {
            return;
        }
       
        int kmer_size = (int) pangenome_node.getProperty("k_mer_size");
        long num_bases = (long) pangenome_node.getProperty("num_bases");
        int degen_nodes = (int) pangenome_node.getProperty("num_degenerate_nodes");
        long num_edges = (long) pangenome_node.getProperty("num_edges");
        long total_kmers = (long) pangenome_node.getProperty("num_k_mers");
        long num_nodes = (long) pangenome_node.getProperty("num_nodes");
        output_builder.append("K-mer size: ").append(kmer_size)
                .append("\nNumber of k-mers: ").append(total_kmers)
                .append("\nNumber of k-mer nodes: ").append(num_nodes)
                .append("\nNumber of edges: ").append(num_edges)
                .append("\nNumber of bases: ").append(num_bases)
                .append("\nNumber of degenerate k-mer nodes: ").append(degen_nodes);
    }    
      
    /**
     *  Write results about the total number of sequences and annotations in the pangenome 
     * @param output_builder
     * @param annotations_array
     * @param hmgroup_counts [genome numbers][unique genes, homology groups, homologous genes]
     * @param function_count_map
     */
    public void add_sequences_and_functions_in_pangenome_to_output(StringBuilder output_builder, int[] annotations_array, 
             int[][] hmgroup_counts, HashMap<String, int[]> function_count_map) {
        
        Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
        int num_proteins = 0;
        if (pangenome_node.hasProperty("num_proteins")) { // if the pangenome is annotated 
            num_proteins = (int) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_proteins");
        }
        
        int[] total_go = function_count_map.get("pangenome#GO");
        int[] total_pfam = function_count_map.get("pangenome#pfam");
        int[] total_interpro = function_count_map.get("pangenome#interpro");
        int[] total_tigrfam = function_count_map.get("pangenome#tigrfam");
        int[] phobius_signalp_array = function_count_map.get("pangenome#phobius_signalp");
        int[] total_COG = function_count_map.get("pangenome#cog");
            
        long total_functions = total_go[0] + phobius_signalp_array[0] + phobius_signalp_array[1] + phobius_signalp_array[2]
                + total_pfam[0] + total_interpro[0] + total_tigrfam[2] + total_COG[0];
        long total_annotations = sum_of_array(annotations_array);
        int total_genes = (int) count_nodes(GENE_LABEL);
        int total_coding_genes = (int) count_nodes(CODING_GENE_LABEL);
        int total_hmgroups = (int) count_nodes(HOMOLOGY_GROUP_LABEL);
        int total_sequences = (int) count_nodes(SEQUENCE_LABEL);
        int total_phenotypes = (int) count_nodes(PHENOTYPE_LABEL);
        int singletons = (int) sum_of_array(hmgroup_counts[0]);
        
        //int total_accessions = (int) count_nodes(accession_label); not appliccable to pantools version 3
        output_builder.append("\n\nNumber of genomes: ").append(total_genomes);
        if (!PROTEOME) { // pangenome 
            output_builder.append("\nNumber of sequences: ").append(total_sequences)
                .append("\nNumber of annotations: ").append(total_annotations)
                .append("\nNumber of genes: ").append(total_genes)
                .append("\nNumber of coding genes: ").append(total_coding_genes);
        }
        
        output_builder.append("\nNumber of proteins: ").append(num_proteins)
            .append("\nNumber of homology groups: ").append(total_hmgroups)
            .append("\nNumber of singletons: ").append(singletons)
            //.append("\nNumber of VCF accessions: ").append(total_accessions) // include in next pantools version
            .append("\nNumber of genomes with phenotype(s): ").append(total_phenotypes);
            
        if (total_functions > 0) {
            // counts are based on longest transcripts
            if (!PROTEOME) {
                output_builder.append("\n\nFurther annotation statistics are calculated from the mRNAs belonging to the longest transcripts");
            }
            if (phobius_signalp_array[0] > 0 || phobius_signalp_array[1] > 0 || phobius_signalp_array[2] > 0) {
                output_builder.append("\nNumber of secreted proteins: ").append(phobius_signalp_array[0])
                    .append("\nNumber of receptors: ").append(phobius_signalp_array[1])
                    .append("\nNumber of transmembrane proteins: ").append(phobius_signalp_array[2]);
            }
            //if (total_bcg > 0) {
            //    output_builder.append("\nNumber of biosyntenic gene clusters: ").append(total_bcg);
            //}
            if (total_COG[0] > 0) {
                output_builder.append("\nNumber of COG proteins: ").append(total_COG[0]);
            }
            if (total_go[0] > 0) {
                output_builder.append("\n\nNumber of GO terms: ").append(total_go[1])
                    .append("\nGO nodes connected to gene: ").append(total_go[0])
                    .append("\nTotal connections: ").append(total_go[2])
                    .append("\n")
                    .append("\nNumber of InterPro domains: ").append(total_interpro[1])
                    .append("\nInterPro nodes connected to gene: ").append(total_interpro[0])
                    .append("\nTotal connections: ").append(total_interpro[2])
                    .append("\n")
                    .append("\nNumber of Pfam domains: ").append(total_pfam[1])
                    .append("\nPFAM nodes connected to gene: ").append(total_pfam[0])
                    .append("\nTotal connections: ").append(total_pfam[2])
                    .append("\n")
                    .append("\nNumber of TIGRFAMs: ").append(total_tigrfam[1])
                    .append("\nTIGRFAM nodes connected to gene: ").append(total_tigrfam[0])
                    .append("\nTotal connections: ").append(total_tigrfam[2]).append("\n");
            }
        }
    }
    
   
    /**
     * Prepare the first row for metrics_per_genome.csv
     * @param phenotype_properties
     * @return StringBuilder
     */
    public StringBuilder create_header_for_genome_metrics_csv_file(ArrayList<String> phenotype_properties) {
        StringBuilder genome_csv_builder;
        StringBuilder pheno_properties_string = new StringBuilder();
        for (String phenotype : phenotype_properties) {
            pheno_properties_string.append("Phenotype ").append(phenotype).append(",");
        }

        if (PROTEOME) {
            genome_csv_builder = new StringBuilder("Genome," + pheno_properties_string + "Protein count,Total protein length,Shortest protein,Longest protein,Average protein length,"
                    + "Median protein length,Homology groups,"
                + "Singletons,mRNA with functional annotation,Phobius signal peptides,Phobius transmembrane domains,SignalP signal peptides,"
                + "Biosynthetic gene clusters,COG proteins,"
                + "GO terms,Total GO connections,Total mRNAs with GO,"
                + "PFAM domains,Total PFAM connections,Total mRNAs with PFAM,"
                + "InterPro domains,Total InterPro connections,Total mRNAs with InterPro,"
                + "TIGRFAMS,Total TIGRFAM connections,Total mRNAs with TIGRFAMS\n");
        } else {
            genome_csv_builder = new StringBuilder("Genome," + pheno_properties_string + "Annotation,Genome size,Sequences,Annotations,Degenerate k-mer nodes,"
                + "Shortest sequence,Longest sequence,Average sequence length,GC content (%),Percentage of genome unknown,"
                + "Total A,Total T,Total C, Total G, Total N, Total other letters,N25,L25,N50,L50,N75,L75,N90,L90,N95,L95,"
                + "Gene count,Total gene length,Shortest gene,Longest gene,Average gene length,Median gene length,Genome covered by genes (%),Gene density per Mb,"
                + "mRNA count,mRNA count (longest transcripts),Total mRNA length,Shortest mRNA,Longest mRNA,Average mRNA length,Median mRNA length,Genome covered by mRNAs (%),mRNA density per Mb,"
                + "CDS count,CDS count (longest transcripts),Total CDS length, Shortest CDS,Longest CDS,Average CDS length,Median CDS length,Genome covered by CDS (%),"
                + "Exon count,Exon count (longest transcripts),Total exon length,Shortest exon,Longest exon,Average exon length,Median exon length,Genome covered by exons (%),"
                + "Intron count,Intron count (longest transcripts),Total intron length,Shortest intron,Longest intron,Average intron length,Median intron length,Genome covered by introns (%),"
                + "tRNA count,Total tRNA length,Shortest tRNA, Longest tRNA,Average tRNA length,Median tRNA lenth,Genome covered by tRNAs (%),"
                + "rRNA count,Total rRNA length,Shortest rRNA, Longest rRNA,Average rRNA length,Median rRNA length,Genome covered by rRNAs (%),"
                + "Repetitive element count,Total repeat length,Shortest repeat,Longest repeat,Average repeat length,Median repeat length,Genome covered by repeats (%),Repeat density per Mb,"
                + "Homology groups,Singletons,mRNA with functional annotation,Phobius signal peptides,Phobius transmembrane domains,Signalp signal peptides,"
                + "Biosynthetic gene clusters,COG proteins,"
                + "GO terms,Total GO connections,Total mRNAs with GO,"
                + "PFAM domains,Total PFAM connections,Total mRNAs with PFAM,"
                + "InterPro domains,Total InterPro connections,Total mRNAs with InterPro,"
                + "TIGRFAMS,Total TIGRFAM connections,Total mRNAs with TIGRFAMS\n");
        }
        //add phenotype, hashmap met string en string[]array 
        return genome_csv_builder;
    }

    /**
     * Add BGC nodes to hashmap. No geneclusters can be present in a panproteome
     * @param mrnas_with_function_set key is genome nr or sequence id. value are mrna's with at least one function
     * @param function_count_map key is genome or sequence id + type of function or "ALL"
     */
    public void get_BGCs_per_sequence(HashMap<String, HashSet<Node>> mrnas_with_function_set, HashMap<String, int[]> function_count_map) {
        if (PROTEOME) { // no geneclusters can be added to panproteome
            return;
        }
        HashMap<String, Integer> mrnas_with_annotations = new HashMap<>();
        ResourceIterator<Node> bgc_nodes = GRAPH_DB.findNodes(BGC_LABEL);
        while (bgc_nodes.hasNext()) {
            Node bgc_node = bgc_nodes.next();
            int[] address = (int[]) bgc_node.getProperty("address_genes");
            try_incr_hashmap(mrnas_with_annotations, address[0] +"", 1);
            try_incr_hashmap(mrnas_with_annotations, address[0] + "_" + address[1], 1);
            Iterable<Relationship> rels = bgc_node.getRelationships();
            for (Relationship rel : rels) {
                Node gene_node = rel.getStartNode();
                Iterable<Relationship> gene_rels = gene_node.getRelationships(RelTypes.codes_for);
                for (Relationship gene_rel : gene_rels) {
                    Node mrna_node = gene_rel.getEndNode();
                    if (!mrna_node.hasProperty("longest_transcript")) {
                        continue; // will only be counted if they are part of the longest transcript
                    }
                    try_incr_hashset_hashmap(mrnas_with_function_set,address[0] +"", mrna_node);
                    try_incr_hashset_hashmap(mrnas_with_function_set,address[0] + "_" + address[1], mrna_node);
                }
            }
        }
        for (String key : mrnas_with_annotations.keySet()) {
            int[] total_bgc = new int[]{mrnas_with_annotations.get(key)};
            function_count_map.put(key + "#bgc", total_bgc);
        }
    }

    /**
     * Add COG mRNA counts to 'function_count_map' hashmap. COG nodes are mRNA nodes
     * @param mrnas_with_function_set key is genome nr or sequence id. value are mrna's with at least one function
     * @param function_count_map
     */
    public void count_cog_for_metrics(HashMap<String, HashSet<Node>> mrnas_with_function_set, HashMap<String, int[]> function_count_map) {
        HashMap<String, Integer> cog_count = new HashMap<>();
        int cog_counter = 0;
        int total_cog = (int) count_nodes(COG_LABEL);
        ResourceIterator<Node> mrna_nodes = GRAPH_DB.findNodes(COG_LABEL);
        while (mrna_nodes.hasNext()) {
            if (cog_counter < 100 || cog_counter % 101 == 0 || cog_counter == total_cog) {
                System.out.print("\rGathering genes with assigned COG functions: " + cog_counter + "/" + total_cog + "     ");
            }
            Node mrna_node = mrna_nodes.next();
            if (!PROTEOME && !mrna_node.hasProperty("longest_transcript")) {
                continue; // will only be counted if they are part of the longest transcript
            }
            int genome_nr, sequence_nr = 0;
            if (PROTEOME) {
                genome_nr = (int) mrna_node.getProperty("genome");
            } else {
                int[] address = (int[]) mrna_node.getProperty("address");
                genome_nr = address[0];
                sequence_nr = address[1];
            }

            if (PROTEOME && skip_array[genome_nr-1]) { // skip the mRNAs from annotations that are not currently selected.
                continue;
            } else if (!PROTEOME) {
                String annotation_id = (String) mrna_node.getProperty("annotation_id");
                if (!annotation_identifiers.contains(annotation_id)) {
                    continue;
                }
            }

            cog_counter ++;
            try_incr_hashmap(cog_count, genome_nr +"", 1);
            try_incr_hashset_hashmap(mrnas_with_function_set, genome_nr +"", mrna_node);
            if (!PROTEOME) {
                try_incr_hashmap(cog_count, genome_nr + "_" + sequence_nr, 1);
                try_incr_hashset_hashmap(mrnas_with_function_set, genome_nr + "_" + sequence_nr, mrna_node);
            }
        }
        System.out.print("\r                                                                       "); // spaces are intentional
        for (String key : cog_count.keySet()) {
            int nr_of_mrnas = cog_count.get(key);
            int[] nr_of_mrnas_array = new int[]{nr_of_mrnas};
            function_count_map.put(key + "#cog", nr_of_mrnas_array);
        }
        function_count_map.put("pangenome#cog", new int[]{cog_counter});
    }

    /**
     * Find name of genome input file (includes .fasta extension)
     * @param genome_nr a genome number
     * @return file name of the genome
     */
    public static String get_genome_name(int genome_nr) {
        if (PROTEOME) { // No 'genome' nodes are available in panproteome
            return "";
        }
        Node genome_node = GRAPH_DB.findNodes(GENOME_LABEL, "number", genome_nr).next();
        String path = (String) genome_node.getProperty("path");
        String[] path_array = path.split("/");
        return path_array[path_array.length-1];
    }

    /**
     * Retrieve BUSCO scores for 'metrics' command line function
     * @return hashmap. key is genome number. value is a stringbuilder with overview of busco scores
     */
    public static HashMap<Integer, StringBuilder> create_busco_summary() { 
        HashMap<Integer, StringBuilder> busco_map = new HashMap<>();
        ResourceIterator<Node> busco_nodes = GRAPH_DB.findNodes(BUSCO_LABEL);
        while (busco_nodes.hasNext()) {
            Node busco_node = busco_nodes.next();
            int genome_nr = (int) busco_node.getProperty("genome");
            Iterable<String> properties = busco_node.getPropertyKeys();
            for (String property : properties) { // take all properties from busco node
                if (property.equals("genome")) {
                    continue;
                }
                String scores = (String) busco_node.getProperty(property);
                property = property.replace("_protein_score","").replace("_protein_longest_transcript_score", " (longest transcripts only)");
                try_incr_SB_hashmap(busco_map, genome_nr, property + ": " + scores + "\n ");
            }
        }
        return busco_map;
    }
       
    /**
     * Create metrics_per_sequence.csv
     * @param annotation_counts_per_seq
     * @param seq_length_map
     * @param nuc_count_per_seq
     * @param function_count_map
     */
    public static void create_per_sequence_statistics_csv(HashMap<String, double[]> annotation_counts_per_seq, HashMap<String, Long> seq_length_map, 
            HashMap<String, long[]> nuc_count_per_seq, HashMap<String, int[]> function_count_map) {

        if (PROTEOME) {
            return;
        }
        StringBuilder csv_builder = new StringBuilder("Genome,Annotation identifier,Sequence,Sequence size,GC content (%),Percentage of sequence unknown,"
                + "Total A,Total T,Total C,Total G,Total N, Total other letters,"
                + "Gene count,Total gene length,Shortest gene,Longest gene,Average gene length,Median gene length,Sequence covered by genes (%),Gene density per Mbp,"
                + "mRNA count,mRNA count (from longest transcripts),Total mRNA length,Shortest mRNA,Longest mRNA,Average mRNA length,Median mRNA length,"
                + "Sequence covered by mRNAs(%),mRNA density per Mbp,"
                + "CDS count,CDS count (from longest transcripts),Total CDS length,Shortest CDS,Longest CDS,Average CDS length,Median CDS length,Sequence covered by CDS(%),"
                + "Exon count,Exon count (from longest transcripts),Total exon length,SHortest exon,Longest exon,Average exon length,Median exon length,Sequence covered by exons(%),"
                + "Intron count,Intron count (from longest transcripts),Total intron length,Shortest intron,Longest intron,Average intron length,Median intron,Sequence covered by introns(%),"
                + "tRNA count,Total tRNA length,Shortest tRNA,Longest tRNA,Average tRNA length,Median tRNA length,Sequence covered by tRNAs(%),"
                + "rRNA count,Total rRNA length,Shortest rRNA,Longest rRNA,Average rRNA length,Median rRNA length,Sequence covered by rRNAs(%),"
                + "Repetitive element count,Total repeat length,Shortest repeat,Longest repeat,Average repeat length,Median repeat length,Sequence covered by repetitive sequences (%),"
                + "Total mRNA with at least one function,Biosynthetic Gene clusters,COG proteins,Phobius signal peptides,Phobius transmembrane domains,Signalp signal peptides,"
                + "GO terms,Total GO connections,Total mRNAs with GO,"
                + "PFAM domains,Total PFAM connections,Total mRNAs with PFAM,"
                + "InterPro domains,Total InterPro connections,Total mRNAs with InterPro,"
                + "TIGRFAMS,Total TIGRFAM connections,Total mRNAs with TIGRFAMS\n");

        for (int i=1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            String annotation_id = annotation_identifiers.get(i-1);
            if (annotation_id.endsWith("_0")) {
                annotation_id = "No annotation";
            } 
            for (int j=1; j <= 999999999; j++) { 
                String sequence_id = i + "_" + j; // example 1_1, 1_2, 1_3
                if (!seq_length_map.containsKey(sequence_id)) {
                    break;
                }
                long sequence_length = seq_length_map.get(sequence_id);
                String seq_size = b_Kb_Mb_or_Gb(sequence_length);
                long[] nuc_count = nuc_count_per_seq.get(sequence_id); // number of A T C G 
                String gc_pc = get_percentage_str(nuc_count[2] + nuc_count[3], nuc_count[0] + nuc_count[1] + nuc_count[2] + nuc_count[3], 2); 
                String ambigious_pc = get_percentage_str(nuc_count[4] + nuc_count[5], sequence_length, 2);
                csv_builder.append(i).append(",").append(annotation_id).append(",").append(sequence_id).append(",").append(seq_size)
                        .append(",").append(gc_pc).append(",").append(ambigious_pc).append(",").append(nuc_count[0]).append(",").append(nuc_count[1]).append(",")
                        .append(nuc_count[2]).append(",").append(nuc_count[3]).append(",").append(nuc_count[4]).append(",").append(nuc_count[5]).append(",");
                
                add_anno_stats_to_seq_builder(sequence_id, "gene", sequence_length, csv_builder, annotation_counts_per_seq);
                add_anno_stats_to_seq_builder(sequence_id, "mRNA", sequence_length, csv_builder, annotation_counts_per_seq);
                add_anno_stats_to_seq_builder(sequence_id, "CDS", sequence_length, csv_builder, annotation_counts_per_seq);
                add_anno_stats_to_seq_builder(sequence_id, "exon", sequence_length, csv_builder, annotation_counts_per_seq);
                add_anno_stats_to_seq_builder(sequence_id, "intron", sequence_length, csv_builder, annotation_counts_per_seq);
                add_anno_stats_to_seq_builder(sequence_id, "tRNA", sequence_length, csv_builder, annotation_counts_per_seq);
                add_anno_stats_to_seq_builder(sequence_id, "rRNA", sequence_length, csv_builder, annotation_counts_per_seq);
                add_anno_stats_to_seq_builder(sequence_id, "repeat", sequence_length, csv_builder, annotation_counts_per_seq);
               
                if (function_count_map.containsKey(sequence_id + "#all")) {
                    int[] nodes_with_function = function_count_map.get(sequence_id + "#all");
                    csv_builder.append(nodes_with_function[0]).append(",");
                } else {
                    csv_builder.append(",");
                }

                if (function_count_map.containsKey(sequence_id + "#bgc")) {
                    int[] bgc_nodes = function_count_map.get(sequence_id + "#bgc");
                    csv_builder.append(bgc_nodes[0]).append(",");
                } else {
                    csv_builder.append(",");
                }
                
                if (function_count_map.containsKey(sequence_id + "#cog")) {
                    int[] cog_count = function_count_map.get(sequence_id + "#cog");
                    csv_builder.append(cog_count[0]).append(",");
                } else {
                    csv_builder.append(",");
                }
                
                if (function_count_map.containsKey(sequence_id + "#phobius_signalp")) { // [ phobius signalpeptide, phobius tranmembrane, signalp]
                    int[] phobius_counts = function_count_map.get(sequence_id + "#phobius_signalp");
                    csv_builder.append(phobius_counts[0]).append(",").append(phobius_counts[1]).append(",").append(phobius_counts[2]).append(",");
                } else {
                    csv_builder.append(",,,");
                }
                
                if (function_count_map.containsKey(sequence_id + "#GO")) {
                    int[] go_counts = function_count_map.get(sequence_id + "#GO"); // [nr of function nodes, nr of mrnas with function, total relationships]
                    csv_builder.append(go_counts[1]).append(",").append(go_counts[2]).append(",").append(go_counts[0]).append(",");
                } else {
                    csv_builder.append(",,,");
                }
               
                if (function_count_map.containsKey(sequence_id + "#pfam")) {
                    int[] pfam_counts = function_count_map.get(sequence_id + "#pfam");
                    csv_builder.append(pfam_counts[1]).append(",").append(pfam_counts[2]).append(",").append(pfam_counts[0]).append(",");
                } else {
                    csv_builder.append(",,,");
                }
                
                if (function_count_map.containsKey(sequence_id + "#interpro")) {
                    int[] ipro_counts = function_count_map.get(sequence_id + "#interpro");
                    csv_builder.append(ipro_counts[1]).append(",").append(ipro_counts[2]).append(",").append(ipro_counts[0]).append(",");
                } else {
                    csv_builder.append(",,,");
                }

                if (function_count_map.containsKey(sequence_id + "#tigrfam")) {
                    int[] tigrfam_counts = function_count_map.get(sequence_id + "#tigrfam");
                    csv_builder.append(tigrfam_counts[1]).append(",").append(tigrfam_counts[2]).append(",").append(tigrfam_counts[0]).append(",");
                } else {
                    csv_builder.append(",,,");
                }
                csv_builder.append("\n");
            } 
        }
        write_SB_to_file_in_DB(csv_builder, "metrics/metrics_per_sequence.csv");
    }
    
    /**
     * Add the coding region features such as 'gene','mRNA' and 'CDS' to the per sequence output
     * @param identifier
     * @param type
     * @param sequence_length
     * @param csv_builder
     * @param annotation_counts_per_seq values are [median, average, standard deviation, shortest, longest, total, count] 
     */
    public static void add_anno_stats_to_seq_builder(String identifier, String type, long sequence_length, StringBuilder csv_builder, 
            HashMap<String, double[]> annotation_counts_per_seq) {
        
        DecimalFormat fm = new DecimalFormat("0.00");
        double[] feature_array = annotation_counts_per_seq.get(identifier + "#" + type); 
        boolean longest_transcripts = false;
        if (type.equals("mRNA") || type.equals("exon") || type.equals("intron") || type.equals("CDS")) {
            longest_transcripts = true;
        }
        if (feature_array == null) { // the feature is not present
            if (type.equals("mRNA") || type.equals("gene")) {
                csv_builder.append(",,,,,,,,"); // 8 commas 
            } else {
                csv_builder.append(",,,,,,,"); // 7
            }
            if (longest_transcripts) {
                csv_builder.append(","); // 7
            }
        } else {
            String feature_total_size = b_Kb_Mb_or_Gb(feature_array[5]);
            int shortest_length = (int) feature_array[3];
            int median = (int) feature_array[0];
            String feature_largest_size = b_Kb_Mb_or_Gb(feature_array[4]);
            String genome_pc_covered = get_percentage_str(feature_array[5], sequence_length, 2);
            if (longest_transcripts) {
                 csv_builder.append(feature_array[7]).append(",");
            }
            csv_builder.append(feature_array[6]).append(",").append(feature_total_size).append(",").append(shortest_length).append(",").append(feature_largest_size)
                    .append(",").append(fm.format(feature_array[1])).append(",").append(median).append(",").append(genome_pc_covered).append(",");
            if (type.equals("mRNA") || type.equals("gene")) {
                double density = calculate_density(sequence_length, feature_array[6]);
                csv_builder.append(density).append(",");
            }
        }
    }

    /**
     * 
     No 'sequence' nodes in panproteome
     * @return 
     */
    public static HashMap<String, Long> get_length_per_sequence() {
        int total_sequences = (int) count_nodes(SEQUENCE_LABEL);
        ResourceIterator<Node> seq_nodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
        HashMap<String, Long> seq_length_map = new HashMap<>();
        int seq_counter = 0;
        while (seq_nodes.hasNext()) {
            seq_counter ++;
            if (seq_counter % 100 == 0 || seq_counter == total_sequences) {
                System.out.print("\rRetrieving sequence lengths: Sequence " + seq_counter + "/" + total_sequences + "  ");
            }
            Node seq_node = seq_nodes.next();
            String seq_id = (String) seq_node.getProperty("identifier");
            long seq_length = (long) seq_node.getProperty("length");
            seq_length_map.put(seq_id, seq_length);
        }
        System.out.print("\r                                                            "); // spaces are intentional 
        return seq_length_map;
    }
    
    /**
     * Calculate statistics for annotation features (gene, mRNA CDS etc) per sequence
     * @return 
     */
    public static HashMap<String, double[]> count_annotation_features_per_sequence() { // gene & coding gene 
        HashMap<String, double[]> annotation_counts_per_seq = new HashMap<>(); // values are // [median, average, standard deviation, shortest, longest, total, count] 
        if (PROTEOME) { // only mrna nodes are present in panproteome. no genome length available in panproteome 
            return annotation_counts_per_seq;
        }
        //first store in seperate hashmaps to large map with lookup times 
        HashMap<String, double[]> gene_counts_per_seq = count_annotation_features_per_sequence_inner(GENE_LABEL);
        HashMap<String, double[]> mrna_counts_per_seq = count_annotation_features_per_sequence_inner(MRNA_LABEL);
        HashMap<String, double[]> cds_counts_per_seq = count_annotation_features_per_sequence_inner(CDS_LABEL);
        HashMap<String, double[]> exon_counts_per_seq = count_annotation_features_per_sequence_inner(EXON_LABEL);
        HashMap<String, double[]> intron_counts_per_seq = count_annotation_features_per_sequence_inner(INTRON_LABEL);
        HashMap<String, double[]> trna_counts_per_seq = count_annotation_features_per_sequence_inner(TRNA_LABEL);
        HashMap<String, double[]> rrna_counts_per_seq = count_annotation_features_per_sequence_inner(RRNA_LABEL);
        HashMap<String, double[]> repeat_counts_per_seq = count_annotation_features_per_sequence_inner(REPEAT_LABEL);
        System.out.print("\r                                                                        "); // spaces are intentional 
        copy_S_SA_hashmap_values_to_hashmap(gene_counts_per_seq, annotation_counts_per_seq);
        copy_S_SA_hashmap_values_to_hashmap(mrna_counts_per_seq, annotation_counts_per_seq);
        copy_S_SA_hashmap_values_to_hashmap(cds_counts_per_seq, annotation_counts_per_seq);
        copy_S_SA_hashmap_values_to_hashmap(exon_counts_per_seq, annotation_counts_per_seq);
        copy_S_SA_hashmap_values_to_hashmap(intron_counts_per_seq, annotation_counts_per_seq);
        copy_S_SA_hashmap_values_to_hashmap(trna_counts_per_seq, annotation_counts_per_seq);
        copy_S_SA_hashmap_values_to_hashmap(rrna_counts_per_seq, annotation_counts_per_seq);
        copy_S_SA_hashmap_values_to_hashmap(repeat_counts_per_seq, annotation_counts_per_seq);
        return annotation_counts_per_seq;
    }

    /**
     *  Will overwrite values if key already exists 
     * @param hashmap1
     * @param hashmap2 
     */
    public static void copy_S_SA_hashmap_values_to_hashmap(HashMap<String, double[]> hashmap1, HashMap<String, double[]> hashmap2) {
        for (String key : hashmap1.keySet()) {
            double[] values = hashmap1.get(key);
            hashmap2.put(key, values);
        }
    }
    
   /**
    * Calculate statistics for annotation features (gene, mRNA CDS etc) per sequence 
    * @param target_label 
    * @return 
    */
    public static HashMap<String, double[]> count_annotation_features_per_sequence_inner(Label target_label) {
        HashMap<String, double[]> annotation_counts_per_seq = new HashMap<>();
        HashMap<String, ArrayList<Integer>> lengths_per_seq = new HashMap<>();
        HashMap<String, Integer> counts_per_seq = new HashMap<>();
        
        String labels_for_longest_transcripts = "CDS, mRNA, intron, exon, ";
        String label_str = target_label.toString();
        int total_nodes = (int) count_nodes(target_label);
        ResourceIterator<Node> feature_nodes = GRAPH_DB.findNodes(target_label);
        int node_counter = 0;
        boolean check_annotation_id = true;
        if (target_label.equals(REPEAT_LABEL)) {
           check_annotation_id = false;
        }
        while (feature_nodes.hasNext()) {
            node_counter ++;
            if (node_counter < 100 || node_counter % 100 == 0 || node_counter == total_nodes) {
                System.out.print("\rCounting '" + target_label + "' nodes per sequence. " + node_counter + "/" + total_nodes + "     "); 
            }
            Node feature_node = feature_nodes.next();
            int[] address = (int[]) feature_node.getProperty("address");
            int feature_length = (int) feature_node.getProperty("length");
            
            if (check_annotation_id) {
                String anno_id = (String) feature_node.getProperty("annotation_id"); // this function is only run on a pangenome
                if (!annotation_identifiers.contains(anno_id)) {
                    continue;
                }   
            }
            try_incr_hashmap(counts_per_seq, address[0] + "_" + address[1], 1);
            if (labels_for_longest_transcripts.contains(label_str + ",")) {
                if (!feature_node.hasProperty("longest_transcript")) {
                   continue; // CDS, mRNA, intron, exon will only be counted if they are part of the longest transcript
                } 
            } 
            try_incr_AL_hashmap(lengths_per_seq, address[0] + "_" + address[1], feature_length);
        }

        for (String key : lengths_per_seq.keySet()) {
            ArrayList<Integer> lengths = lengths_per_seq.get(key);
            int total_count = counts_per_seq.get(key);
            double[] median_average_stdev = median_average_stdev_from_AL_int(lengths); // [median, average, standard deviation, shortest, longest, total, lt count] 
            double[] array_plus_one = new double[median_average_stdev.length + 1];
            for (int i = 0; i < median_average_stdev.length; i++) {
                 array_plus_one[i] = median_average_stdev[i];
            }
            array_plus_one[7] = total_count;
            annotation_counts_per_seq.put(key + "#" + target_label, array_plus_one);
        }
        return annotation_counts_per_seq;
    }
     
    /*
     Assumes copy number array does not have the first zero!
    */
    public static String create_readable_copy_number_with_skip(int[] copy_number) {
        int[] new_copy_number = new int[total_genomes];
        for (int i = 0; i < copy_number.length; ++i) { 
            if (skip_array[i]) {
                continue;
            }
            new_copy_number[i] = copy_number[i];
        }
        String frequency = determine_genome_freq_absence_presence_freq(new_copy_number);
        return frequency;
    }

    /**
     * 
     * @param mrna_gff_id
     * @param gene_name
     * @param genome_nr
     * @return 
     */
    /*
    public String create_fasta_header_grp_info(String mrna_gff_id, String gene_name, int genome_nr) {
        String fasta_header = ">" + mrna_gff_id;
        String phenotype = get_phenotype_for_genome(genome_nr, true);
        if (gene_name.equals("")) { 
            fasta_header += " no_gene_name genome " + genome_nr + phenotype;
        } else {
            fasta_header += " " + gene_name + " genome " + genome_nr + phenotype;
        }
        return fasta_header;
    }*/
    
    /**
     * #
     * Requires: 
     * 
     * -dp 
     * 
     * Requires one of the following
     * -hm
     * --node. the node id for the hmgroup
     * 
     * Optional 
     * -label 
     */
    public void homology_group_info(List<Long> homologyGroups) throws RuntimeException, IOException {
        Pantools.logger.info("Reporting all information from selected homology groups.");

        StringBuilder output_builder = new StringBuilder();
        HashSet<String> groups_with_function = new HashSet<>();
        HashMap<String, HashSet<String>> groups_with_function_or_name = new HashMap<>();

        BufferedWriter nodeBuilder = null;
        ArrayList<Node> hmNodeList = null;
        create_directory_in_DB("group_info");
        delete_file_in_DB("group_info/group_functional_annotations.txt"); 
        
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            check_current_grouping_version();
            hmNodeList = Utils.findHmNodes(homologyGroups, 0);
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        if (Mode.contains("K-MER")) { // user (that does not look at code) cannot see this mode
            Pantools.logger.info("K-MER mode was selected. Extracting nucleotide nodes from the graph.");
            INDEX_DB = new IndexDatabase(WORKING_DIRECTORY + INDEX_DATABASE_PATH, "sorted");
            INDEX_SC = new IndexScanner(INDEX_DB);
            GENOME_DB = new SequenceDatabase(WORKING_DIRECTORY + GENOME_DATABASE_PATH);
            GENOME_SC = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_SC.get_pre_len());
            try {
                nodeBuilder = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "group_info/group_nodes.txt"));
                nodeBuilder.write("#To visualize in the Neo4j browser:\n MATCH (n) where id(n) in [ PLACE,NODES,HERE ] return n\n\n");
            } catch (IOException e) {
                Pantools.logger.error("Unable to create {}", WORKING_DIRECTORY + "group_info/group_nodes.txt");
                e.printStackTrace();
                System.exit(1);
            }
        }

        ArrayList<String> name_list = new ArrayList<>();
        if (SELECTED_NAME != null) {
            name_list = new ArrayList<>(Arrays.asList(SELECTED_NAME.split("\\s*,\\s*")));
        }

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            function_overview_per_group(hmNodeList, WORKING_DIRECTORY + "group_info/");
            int counter = 0;
            for (Node hm_node : hmNodeList) {
                HashSet<Node> nucleotideNodes = new HashSet<>();
                ArrayList<Node> mrna_nodes = new ArrayList<>();
                counter ++; 
                String group_id = hm_node.getId() + "";
                boolean correct = test_if_correct_label(hm_node, HOMOLOGY_GROUP_LABEL, false);
                if (!correct) {
                    correct = test_if_correct_label(hm_node, INACTIVE_HOMOLOGY_GROUP_LABEL, false);
                    if (!correct) {
                        Pantools.logger.error("{} is not a homology group.", hm_node);
                        continue;
                    }
                }
                int num_members = (int) hm_node.getProperty("num_members");
                int[] temp_copy_number = (int[]) hm_node.getProperty("copy_number");
                int[] copy_number = remove_first_position_array(temp_copy_number);
                String frequency = create_readable_copy_number_with_skip(copy_number);
                if (Mode.contains("K-MER")) {
                    try {
                        assert nodeBuilder != null;
                        nodeBuilder.write("#Homology group " + group_id + "\n"); // do not remove, used by function that is currently disabled
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                StringBuilder allMrnaNucleotideNodes = new StringBuilder();
                StringBuilder gene_info_builder = new StringBuilder("num_members: " + num_members + 
                        "\nFound in " + frequency.split(",").length + " genomes: " + frequency +
                        "\ncopy_number: " + Arrays.toString(copy_number) + "\n");
                HashSet<String> names_set = new HashSet<>();
                HashSet<String> function_set = new HashSet<>();
                HashSet<Node> function_node_set = new HashSet<>();
                ArrayList<Integer> psize_list = new ArrayList<>();
                Iterable<Relationship> relations = hm_node.getRelationships();
                Pantools.logger.info("Retrieving group information: {}/{} homology groups.", counter, hmNodeList.size());
                for (Relationship rel : relations) {
                    Node mrna_node = rel.getEndNode();
                    long mrna_node_id = mrna_node.getId();
                    int[] address = (int[]) mrna_node.getProperty("address");
                    if (skip_array[address[0]-1]) {
                        continue;
                    }
                    Relationship mrna_rel = mrna_node.getSingleRelationship(RelTypes.codes_for, Direction.INCOMING);
                    Node gene_node = mrna_rel.getStartNode();
                    long gene_node_id = gene_node.getId();
                    String strand = (String) mrna_node.getProperty("strand");
                    String geneName = retrieveNamePropertyAsString(gene_node);
                    String[] geneNameArray = geneName.split("/");
                    if (geneName.equals("")) {
                        geneName = "-";
                    }
                    String gene_gff_id = (String) gene_node.getProperty("id");
                    String mrnaName = retrieveNamePropertyAsString(gene_node);
                    if (mrnaName.equals("")) {
                        mrnaName = "-";
                    } 

                    String mrna_gff_id = (String) mrna_node.getProperty("protein_ID");
                    int dna_length = (int) mrna_node.getProperty("length");
                    int prot_length = (int) mrna_node.getProperty("protein_length");
                    for (String name : geneNameArray) {
                        if (name_list.contains(name)) { // name_list is only filled when --name was included as argument
                            try_incr_hashset_hashmap(groups_with_function_or_name, geneName + "#name", group_id);
                        }
                        names_set.add(name);
                    }

                    psize_list.add(prot_length);
                    gene_info_builder.append("\n").append("Gene id: ").append(gene_gff_id)
                            .append("\nGene name: ").append(geneName)
                            .append("\nmRNA id: ").append(mrna_gff_id)
                            .append("\nmRNA name: ").append(mrnaName)
                            .append("\nGenome: ").append(address[0])
                            .append("\nAddress ").append(address[0]).append(" ").append(address[1]).append(" ").append(address[2]).append(" ").append(address[3])
                            .append("\nStrand: ").append(strand)
                            .append("\nNucleotide length: ").append(dna_length)
                            .append("\nProtein length: ").append(prot_length)
                            .append("\nGene node id: ").append(gene_node_id)
                            .append("\nmRNA node id: ").append(mrna_node_id).append("\n");
                    group_info_get_f_annotations(gene_node, mrna_node, function_set, function_node_set, gene_info_builder, groups_with_function, group_id);

                    if (Mode.contains("K-MER")) {
                        allMrnaNucleotideNodes.append(mrna_gff_id).append("\n");
                        find_nuc_nodes_of_hmgroup(nucleotideNodes, allMrnaNucleotideNodes, address);
                    }
                    mrna_nodes.add(mrna_node);  
                } 
               
                String prot_size_freq = determine_frequency_list_int(psize_list);
                if (SELECTED_LABEL != null) { // user has given --label with a function identifier
                    check_function_output_matches(function_node_set, group_id, groups_with_function_or_name);
                }
                
                output_builder.append("#Homology group ").append(group_id)
                        .append("\nAll gene names: ").append(names_set.toString().replace("[","").replace("]",""))
                        .append("\nAll functions: ").append(function_set.toString().replace("[","").replace("]",""))
                        .append("\nAll protein sizes: ").append(prot_size_freq.replace(" ","")).append("\n")
                        .append(gene_info_builder).append("\n\n");

                if (Mode.contains("K-MER")) {
                    addNodeIdentifiersToGroupInfoOutput(nodeBuilder, allMrnaNucleotideNodes, nucleotideNodes, mrna_nodes);
                }
            }

            write_SB_to_file_in_DB(output_builder, "group_info/group_info.txt");
            write_groups_with_specific_functions(groups_with_function_or_name); 
            write_groups_with_specific_gene_name(groups_with_function_or_name);
            if (Mode.contains("K-MER")) {
                try {
                    assert nodeBuilder != null;
                    nodeBuilder.close();
                } catch (IOException e) {
                    Pantools.logger.error("Unable to close {}", WORKING_DIRECTORY + "/group_info/nodes.txt");
                    e.printStackTrace();
                    System.exit(1);
                }
            }
            print_output_files_for_grp_info();
            tx.success(); // transaction successful, commit changes
        }
    }
    
    /**
     * 
     */
    public void print_output_files_for_grp_info() {
        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/group_info.txt");
        Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/functions_per_group_and_mrna.csv");
        Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/function_counts_per_group.csv");

        if (SELECTED_LABEL != null) {
            Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/groups_with_function.txt");
        }
        if (SELECTED_NAME != null) {
            Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/groups_with_name.txt");
        }
        if (Mode.contains("K-MER")) {
            Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/group_nodes.txt");
        }
    }
    
    /**
     * 
     * @param node_builder
     * @param allMrnaNucleotideNodes
     * @param nucleotideNodes
     * @param mrna_nodes
     */
    public void addNodeIdentifiersToGroupInfoOutput(BufferedWriter node_builder, StringBuilder allMrnaNucleotideNodes,
                                                   HashSet<Node> nucleotideNodes, ArrayList<Node> mrna_nodes) {

        try {
            StringBuilder builder = new StringBuilder("Distinct node identifiers of all genes in group:\n");
            for (Node nucleotideNode : nucleotideNodes) {
               builder.append(nucleotideNode.getId()).append(",");
            }
            node_builder.append(builder.toString().replaceFirst(".$", "\n\n"));

            builder = new StringBuilder("mRNA node identifiers part of group:\n");
            for (Node mrna_node : mrna_nodes) {
                builder.append(mrna_node.getId()).append(",");
            }
            node_builder.append(builder.toString().replaceFirst(".$", "\n\n"));
            node_builder.append(allMrnaNucleotideNodes.toString());
        }  catch (IOException e) {
                e.printStackTrace();
        }
    }
    
    /**
     * 
     * @param hms_per_id 
     */
    public void write_groups_with_specific_functions(HashMap<String, HashSet<String>> hms_per_id) { 
        if (SELECTED_LABEL == null) {
            return;
        }
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "group_info/groups_with_function.txt"))) {
            for (String key : hms_per_id.keySet()) {
                if (!key.endsWith("#function")) {
                    continue;
                }
                HashSet<String> group_set = hms_per_id.get(key);
                if (group_set.size() == 1) {
                     out.write("#" + key.replace("#function","") + ", 1 group\n");
                } else {
                    out.write("#" + key.replace("#function","") + ", " + group_set.size() + " groups\n");
                }
                for (String group : group_set) {
                    out.write(group + ",");
                }
                out.write("\n");
            }
            out.close();
        } catch (IOException e) {
            Pantools.logger.warn("Unable to write to: {}group_info/groups_with_function.txt", WORKING_DIRECTORY);
        }
    }
    
    /**
     * Groups with have at least one gene names that matches the one given by the user --name (SELECTED_NAME)
     * @param hms_per_id 
     */
    public static void write_groups_with_specific_gene_name(HashMap<String, HashSet<String>> hms_per_id) {
         if (SELECTED_NAME == null) {
             return;
         }
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "group_info/groups_with_name.txt"))) {
            for (String key : hms_per_id.keySet()) {
                if (!key.endsWith("#name")) {
                    continue;
                }
                HashSet<String> group_set = hms_per_id.get(key);
                if (group_set.size() == 1) {
                    out.write("#" + key.replace("#name","") + ", 1 group\n");
                } else {
                    out.write("#" + key.replace("#name","") + ", " + group_set.size() + " groups\n");
                }
                
                String groups_str = group_set.toString().replace("[","").replace("]","").replace(" ","");
                out.write(groups_str + "\n");
            }
        } catch (IOException e) {
            Pantools.logger.warn("Unable to write to: {}group_info/groups_with_name.txt", WORKING_DIRECTORY);
        }
    }
    
    /**
     * 
     * @param size_list
     * @return 
     */
    public static String determine_frequency_list_int(ArrayList<Integer> size_list) {
        ArrayList<Integer> distinct_size_list = remove_duplicates_from_AL_int(size_list);
        StringBuilder output = new StringBuilder();
        for (int size : distinct_size_list) {
            int occurrences = Collections.frequency(size_list, size);
            output.append(size).append("(").append(occurrences).append("x), ");
         }
        return output.toString().replaceFirst(".$", "").replaceFirst(".$", ""); // remove last character (2x)
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static String determine_genome_freq_absence_presence_freq(int[] array) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < array.length; ++i) {
            if (array[i] > 0) {
                output.append((i+1)).append("(").append(array[i]).append("x),");
            }
        }
        String output_str = output.toString().replaceFirst(".$", "");
        return output_str;
    }
    
    /**
     * 
     * @param nucleotideNodes
     * @param allMrnaNucleotideNodes
     * @param address 
     */
    public void find_nuc_nodes_of_hmgroup(HashSet<Node> nucleotideNodes, StringBuilder allMrnaNucleotideNodes, int[] address) {
        StringBuilder seq = new StringBuilder();
        IndexPointer start_ptr = seqLayer.locate(GRAPH_DB, GENOME_SC, INDEX_SC, address[0], address[1], address[2]-1);
        HashSet<Node> node_set = extract_sequence_return_unique_nodes(seq, start_ptr, address);
        StringBuilder node_str = new StringBuilder();
        for (Node nucleotideNode : node_set) {
            nucleotideNodes.add(nucleotideNode);
            node_str.append(nucleotideNode.getId()).append(",");
        }
        allMrnaNucleotideNodes.append(node_str.toString().replaceFirst(".$","")).append("\n\n"); // remove last character
    }

    /**
     * VERSION is set by --version argument
     */
    public static void report_selected_busco_mode() {
        if (null == BUSCO_VERSION) {
           Pantools.logger.info("No --version was selected. Using BUSCO v5.");
           BUSCO_VERSION = "BUSCO5";
        } else switch (BUSCO_VERSION) {
            case "BUSCO3":
                Pantools.logger.info("Selected BUSCO v3.");
                break;
            case "BUSCO4":
                Pantools.logger.info("Selected BUSCO v4.");
                break;
            case "BUSCO5":
                Pantools.logger.info("Selected BUSCO v5.");
                break;
            default:
                Pantools.logger.info("Provided --version argument was not recognized as 'busco3', 'busco4' or 'busco5'. Using BUSCO v5.");
                BUSCO_VERSION = "BUSCO5";
                break;
        }
        
        // Mode.contains("LONGEST-TRANSCRIPT") has been replaced by the -lt argument. 
        if ((Mode.contains("LONGEST-TRANSCRIPT") || longest_transcripts) && !PROTEOME) {
            Pantools.logger.info("The longest transcript option was selected. Only searching against the longest protein-coding transcript of genes.");
            longest_transcripts = true;
        } else if ((Mode.contains("LONGEST-TRANSCRIPT") || longest_transcripts) && PROTEOME) {
            Pantools.logger.info("The longest transcript option is NOT usable on a panproteome. All sequences of a proteome are used.");
            longest_transcripts = false;
        }
    }
    
    /**
     * Create 'busco' nodes if these don't exists yet
     */
     public static void create_busco_node_if_missing() {
         try (Transaction tx = GRAPH_DB.beginTx()) {
             ResourceIterator<Node> busco_nodes = GRAPH_DB.findNodes(BUSCO_LABEL);
             if (busco_nodes.hasNext()) {
                 return; // busco nodes already exist
             }

             // busco nodes do not exist, create one for every genome (even when skipped)
             if (PROTEOME) { // panproteome does not have genome nodes
                 for (int i = 1; i <= total_genomes; i++) {
                     Node busco_node = GRAPH_DB.createNode(BUSCO_LABEL);
                     busco_node.setProperty("genome", i);
                 }
             } else {
                 ResourceIterator<Node> genome_nodes = GRAPH_DB.findNodes(GENOME_LABEL);
                 while (genome_nodes.hasNext()) {
                     Node genome_node = genome_nodes.next();
                     int genome_nr = (int) genome_node.getProperty("number");
                     Node busco_node = GRAPH_DB.createNode(BUSCO_LABEL);
                     busco_node.setProperty("genome", genome_nr);
                     genome_node.createRelationshipTo(busco_node, RelTypes.has_busco);
                 }
             }
             tx.success();
         }
     }
    
    /**
     * Checks if the required input file is available depending on the --mode argument that was included 
     * @param genome_nr
     * @return 
     */
    public static String check_if_busco_protein_input_is_ready(int genome_nr) {
        String path_to_proteins = WORKING_DIRECTORY + "/proteins/proteins_" + genome_nr + ".fasta";
        if (longest_transcripts && !PROTEOME) {
            path_to_proteins = WORKING_DIRECTORY + "/proteins/longest_transcripts/proteins_" + annotation_identifiers.get(genome_nr-1) + ".fasta";
        }         

        if (!check_if_file_exists(path_to_proteins)) {
            Pantools.logger.error("Protein input_files do not exists: {}", path_to_proteins);
            System.exit(1);
        } 
        return path_to_proteins;
    }
     
    /**
     * Requires 
     * -database-path/-dp
     * --input-file
     * 
     * Optional 
     * --skip 
     * --reference
     * --name list of questionable buscos. creates an extra column in busco_overview.csv
     * --mode busco3, busco4, busco5 (default is busco5)
     */
    public void busco_protein() {
        Pantools.logger.info("Running BUSCO in protein mode.");
        report_selected_busco_mode(); // check if busco3 4 or 5 was selected 
        if (INPUT_FILE == null) {
            if (BUSCO_VERSION.equals("BUSCO4") || BUSCO_VERSION.equals("BUSCO5")) {
                Pantools.logger.error("No BUSCO dataset was selected.");
                Pantools.logger.error("Provide the full path to the database or just give the odb10 dataset name and it is automatically downloaded.");
                Pantools.logger.error("To display all available datasets, run the following command: `busco --list-datasets`.");
            } else { // busco3 
                Pantools.logger.error("No BUSCO dataset was selected.");
                Pantools.logger.error("Provide the full path to a odb9 database.");
            }
            throw new RuntimeException("No selected BUSCO dataset");
        }
        String[] questionable_buscos = new String[0];
        if (SELECTED_NAME != null) {
            questionable_buscos = SELECTED_NAME.split(",");
        } 
       
        String threads_str = report_number_of_threads(); // prints how many threads were selected by user
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) {
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if --skip/-ref is provided by user
            retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            annotation_identifiers = get_annotation_identifiers(false, true, PATH_TO_THE_ANNOTATIONS_FILE); // do not print, IGNORE the -af input file
            update_skip_array_based_on_annotation();
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) { 
           Pantools.logger.error("Unable to start the pangenome database.");
           throw new RuntimeException("Database error");
        }
       
        String[] anno_path_array = INPUT_FILE.split("/");
        String db_name = anno_path_array[anno_path_array.length-1];
        System.out.println("\rSelected BUSCO database: " + db_name);
        String busco_path = "";
        if (BUSCO_VERSION.equals("BUSCO3")) {
            busco_path = find_program_location("run_BUSCO.py");
            check_if_accurate_busco_database();
        } else { // busco4 & 5
            check_if_program_exists_stdout("busco -h", 100, "BUSCO"); // check if program is set to $PATH
        }

        HashMap<String, ArrayList<Integer>> hmm_map = new HashMap<>();
        double[][] busco_scores = new double[5][total_genomes]; // Complete, Single copy, Duplicated, Fragmented, Missing
        int total_buscos = 0;
        String output_name = "protein";
        try (Transaction tx = GRAPH_DB.beginTx()) {
            create_directory_in_DB("busco/" + db_name);
            if (longest_transcripts) {
                ProteomeLayer proLayer = new ProteomeLayer();
                proLayer.find_longest_transcript_per_gene(false, 0);
                create_directory_in_DB("busco/" + db_name + "/protein_longest_transcript/results");
                output_name = "protein_longest_transcript";
            } else {
                create_directory_in_DB("busco/" + db_name + "/protein/results");
            }

            for(int i=1; i <= total_genomes; i++) {
                String busco_directory = WORKING_DIRECTORY + "/busco/" + db_name + "/" + output_name + "/results/" + i + "/";
                boolean pass1 = check_if_file_exists(busco_directory + "short_summary.specific." + db_name + ".genome_" + i + ".txt");
                boolean pass1_2 = check_if_file_exists(busco_directory + "short_summary_genome_" + i + ".txt");
                if (skip_array[i-1]) {
                    continue; 
                }
                System.out.print("\rRunning BUSCO against proteome: " + i + "/" + total_genomes + "                                    "); // spaces are intentional
                HashMap<String, Node> gene_node_map = create_mrna_node_map_for_genome(i, "Retrieving mRNA's");
                if (gene_node_map.isEmpty()) { // genome is not annotated
                    continue;
                }
                if (pass1 || pass1_2) {
                    total_buscos = read_busco_full_table_tsv(output_name, db_name, hmm_map, i, busco_scores, gene_node_map);
                    continue;
                }
                String protein_input = check_if_busco_protein_input_is_ready(i);
                String[] run_busco;
                if (BUSCO_VERSION.equals("BUSCO3")) {
                    run_busco = new String[]{"python3", busco_path, "-f", "-i", protein_input, "-o", "genome_" + i,
                            "-l", INPUT_FILE, "-m", "proteins", "--cpu", threads_str, "--tarzip"};
                    ExecCommand.ExecCommand(run_busco);
                    copy_directory("run_genome_" + i, WORKING_DIRECTORY + "/busco/" + db_name + "/" + output_name + "/results/" + i);
                    delete_directory("run_genome_" + i);
                } else if (BUSCO_VERSION.equals("BUSCO4")) { // busco4
                    run_busco = new String[]{"busco", "-f", "-i", protein_input, "-o", "genome_" + i,
                            "-l", INPUT_FILE, "-m", "proteins", "--cpu", threads_str}; // --tarzip has not been implemented yet in busco 4.0.6
                    ExecCommand.ExecCommand(run_busco);
                    copy_directory("genome_" + i, WORKING_DIRECTORY + "/busco/" + db_name + "/" + output_name + "/results/" + i );
                    delete_directory("genome_" + i);
                } else { // busco 5
                     run_busco = new String[]{"busco", "-f", "-i", protein_input, "-o", "genome_" + i,
                            "-l", INPUT_FILE, "-m", "proteins", "--cpu", threads_str, "--tar"}; // --tarzip has not been implemented yet in busco 4.0.6
                    ExecCommand.ExecCommand(run_busco);
                    copy_directory("genome_" + i, WORKING_DIRECTORY + "/busco/" + db_name + "/" + output_name + "/results/" + i );
                    delete_directory("genome_" + i);
                }
                total_buscos = read_busco_full_table_tsv(output_name, db_name, hmm_map, i, busco_scores, gene_node_map);
            }
            tx.success(); // transaction successful, commit changes
        }

        HashMap<String, StringBuilder> hmm_per_genome = create_busco_hmm_overview(hmm_map, output_name, db_name, questionable_buscos, total_buscos);
        create_busco_csv_overview(questionable_buscos, total_buscos, hmm_per_genome, db_name, output_name);
        HashMap<Integer, String> busco_scores_map = read_busco_summary(output_name, db_name, busco_scores, total_buscos);
        createOrUpdateBuscoNodes(db_name, output_name, busco_scores_map);

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}busco/{}/{}/results/", WORKING_DIRECTORY, db_name, output_name);
        Pantools.logger.info(" {}busco/{}/{}/busco_overview.csv", WORKING_DIRECTORY, db_name, output_name);
        Pantools.logger.info(" {}busco/{}/{}/hmm_overview.txt", WORKING_DIRECTORY, db_name, output_name);
        Pantools.logger.info(" {}busco/{}/{}/busco_scores.txt", WORKING_DIRECTORY, db_name, output_name);
    }

    /**
     * Gather all mrna nodes of a genome and put this in a hashmap. key is the protein_id, value is the node.
     * Interpro/eggnog/BLAST2GO output must have the protein_id or the information cannot be added.
     * NB: this method will exclude all mRNA nodes that have a variant label.
     */
    public static HashMap<String, Node> create_mrna_node_map_for_genome(int genome_nr, String info) {
        Pantools.logger.debug("Creating mRNA node map for genome {}.", genome_nr);

        HashMap<String, Node> mrna_node_map = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> mrna_nodes;
            if (!PROTEOME) { // pangenome
                String annotation_id = annotation_identifiers.get(genome_nr-1);
                mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "annotation_id", annotation_id);
            } else { // panproteome does not have annotation identifiers
                mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", genome_nr);
            }
            int mrna_counter = 0;
            while (mrna_nodes.hasNext()) { 
                mrna_counter ++;
                if (mrna_counter % 1000 == 0 || mrna_counter < 10) {
                    System.out.print("\r" + info + " for genome " + genome_nr + ": " + mrna_counter + " mRNA's retrieved."); // TODO: replace with actual progress bar
                }
                Node mrnaNode = mrna_nodes.next();

                //Node mrna_or_gene_node = mrna_node; // return gene or mrna nodes depending on 'mrna_or_gene' string
                if (!PROTEOME) { // pangenome
                    String mrna_id = (String) mrnaNode.getProperty("id");
                    mrna_node_map.put(mrna_id, mrnaNode);
                    if (mrna_id.endsWith("_mRNA")) { // '_mRNA' was added
                        mrna_node_map.put(mrna_id.substring(0, mrna_id.length() - 5), mrnaNode);
                    }
                    String[] mrnaNameArray = retrieveNamePropertyAsArray(mrnaNode);
                    for (String name : mrnaNameArray) {
                        mrna_node_map.put(name, mrnaNode);
                    }
                    /*
                    Relationship gene_rel = mrna_node.getSingleRelationship(RelTypes.is_parent_of, Direction.INCOMING);
                    if (gene_rel == null) { // the mRNA is not connected to a gene node. Something went wrong with the annotation
                        continue;
                    }
                    Node gene_node = gene_rel.getStartNode();
                    if (mrna_or_gene.equals("gene")) {
                        mrna_or_gene_node = gene_node;
                    }
                    String gene_id = (String) gene_node.getProperty("id");
                    mrna_node_map.put(gene_id, mrna_or_gene_node);

                    String gene_name = (String) gene_node.getProperty("name");
                    mrna_node_map.put(gene_name, mrna_or_gene_node);
                    */
                }
            
                if (mrnaNode.hasProperty("protein_ID")) { // happens when an mRNA doesn't have a CDS, these are not
                    String mrna_protein_id = (String) mrnaNode.getProperty("protein_ID");
                    mrna_node_map.put(mrna_protein_id, mrnaNode);
                }
            }
            tx.success(); // transaction successful, commit changes
        }
        System.out.println(); // TODO: replace by actual progress bar

        Pantools.logger.debug("Created mRNA node map for genome {}.", genome_nr);
        return mrna_node_map;
    }
    
    /**
     * Put the BUSCO scores in the pangenome
     * Removes previous scores that match the orthodb name
   
     * @param db_name
     * @param busco_mode
     * @param busco_scores_map 
     */
    public static void createOrUpdateBuscoNodes(String db_name, String busco_mode, HashMap<Integer, String> busco_scores_map) {
        create_busco_node_if_missing();
        Pantools.logger.info("Updating BUSCO nodes.");
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> busco_nodes = GRAPH_DB.findNodes(BUSCO_LABEL);
            while (busco_nodes.hasNext()) {
                Node busco_node = busco_nodes.next();
                int genome_nr = (int) busco_node.getProperty("genome");
                if (skip_array[genome_nr - 1]) {
                    continue;
                }
                String score = busco_scores_map.get(genome_nr);
                if (busco_node.hasProperty(db_name + "_" + busco_mode + "_score")) {
                    busco_node.removeProperty(db_name + "_" + busco_mode);
                }
                busco_node.setProperty(db_name + "_" + busco_mode + "_score", score);
            }
            tx.success();
        }
    } 
   
    /**
     * 
     * @param dupli_list
     * @param gene_node_map
     * @param counts
     * @param hmm_map
     * @param busco
     * @param genome_nr
     * @return 
     */
    public static int [] check_if_busco_is_duplicated(ArrayList<String> dupli_list, HashMap<String, Node> gene_node_map, int[] counts, 
            HashMap<String, ArrayList<Integer>> hmm_map, String busco, int genome_nr) {
        
        HashSet<Node> nodes = new HashSet<>();
        for (String gene_id : dupli_list) {
            Node gene_node = gene_node_map.get(gene_id);
            nodes.add(gene_node);
        }
        if (nodes.size() == 1) { // single copy 
            counts[0] ++; 
            try_incr_AL_hashmap(hmm_map, busco + "#Complete", genome_nr);
        } else if (nodes.size() > 1) {
            counts[1] ++; // duplicated
            try_incr_AL_hashmap(hmm_map, busco + "#Duplicated", genome_nr);
        }
        return counts;
    }
    /**
     * 
     * @param mode
     * @param db_name
     * @param hmm_map
     * @param genome_nr
     * @param busco_scores Array1: Complete, Single copy, Duplicated, Fragmented, Missing .array2 genome numbers.
     * @param gene_node_map
     * @return int
     */
    public static int read_busco_full_table_tsv(String mode, String db_name, HashMap<String, ArrayList<Integer>> hmm_map,
                                                int genome_nr, double[][] busco_scores, HashMap<String, Node> gene_node_map) {
        
        String input_file = WORKING_DIRECTORY + "/busco/" + db_name + "/" + mode + "/results/" + genome_nr + "/run_" + db_name + "/full_table.tsv";
        if (BUSCO_VERSION.equals("BUSCO3")) {
            input_file = WORKING_DIRECTORY + "/busco/" + db_name + "/" + mode + "/results/" + genome_nr + "/full_table_genome_" + genome_nr + ".tsv";
        }
        int[] counts = new int[4]; // complete single copy, complete duplicated, fragmented, missing 
        if (!check_if_file_exists(input_file)) {
            Pantools.logger.error("The BUSCO run failed, please check your installation.");
            throw new RuntimeException("Missing output file: " + input_file);
        }
        
        String previous_busco = "";
        ArrayList<String> dupli_list = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(input_file))) {
            while (in.ready()) {
                String line = in.readLine().trim(); 
                if (line.startsWith("#")) {
                    continue;
                }
                
                String[] line_array = line.split("\t");
                if (!line_array[0].equals(previous_busco)) {
                    if (dupli_list.size() > 1) {
                        counts = check_if_busco_is_duplicated(dupli_list, gene_node_map, counts, hmm_map, previous_busco, genome_nr);
                    }
                    dupli_list = new ArrayList<>();
                }
                if (line_array[1].equals("Duplicated")) {
                    dupli_list.add(line_array[2]);
                } else {
                    try_incr_AL_hashmap(hmm_map, line_array[0] + "#" + line_array[1], genome_nr);
                    if (line_array[1].equals("Missing")) {
                        counts[3] ++;
                    }else if (line_array[1].equals("Complete")) {
                        counts[0] ++;
                    } else { // fragmented
                        counts[2] ++;
                    }
                }
                previous_busco = line_array[0];
            }

            counts = check_if_busco_is_duplicated(dupli_list, gene_node_map, counts, hmm_map, previous_busco, genome_nr);
            double total = counts[0] + counts[1] + counts[2] + counts[3];
            busco_scores[0][genome_nr-1] = ((counts[0] + counts[1]) / total) * 100; //complete 
            busco_scores[1][genome_nr-1] = (counts[0] / total) * 100; // single copy 
            busco_scores[2][genome_nr-1] = (counts[1] / total) * 100; // duplicated 
            busco_scores[3][genome_nr-1] = (counts[2] / total) * 100;
            busco_scores[4][genome_nr-1] = (counts[3] / total) * 100;
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read busco output: {}", input_file);
            throw new RuntimeException("File error");
        }
        return counts[0] + counts[1] + counts[2] + counts[3];
    }
    
    /**
     * 
     * @param output_name
     * @param db_name
     * @param busco_scores
     * @param total_buscos
     * @return 
     */
    public static HashMap<Integer, String> read_busco_summary(String output_name, String db_name, double[][] busco_scores, int total_buscos) {
        HashMap<Integer, String> result_map = new HashMap<>();
        StringBuilder txt_builder = new StringBuilder("Total BUSCO's " + total_buscos + "\n");
        double[] busco_highest_longest = new double[]{101, 0, 101, 0, 101, 0, 101, 0, 101, 0}; // C lowest [0] and highest [1] ,S,D,F,M 
        DecimalFormat formatter = new DecimalFormat("0.00");
        for (int i= 0; i < total_genomes; i++) {
            if (skip_array[i]) {
                continue;
            }
            String phenotype = get_phenotype_for_genome(i+1, true);
            txt_builder.append("\nGenome ").append((i+1)).append(phenotype).append("\n");
            String complete = formatter.format(busco_scores[0][i]);
            String single = formatter.format(busco_scores[1][i]);
            String duplicated = formatter.format(busco_scores[2][i]);
            String fragmented = formatter.format(busco_scores[3][i]);
             String missing = formatter.format(busco_scores[4][i]);
            txt_builder.append("C:").append(complete).append(" S:").append(single).append(" D:").append(duplicated).append(" F:")
                    .append(fragmented).append(" M:").append(missing).append("\n");
            check_if_lower_higher_array(busco_highest_longest, busco_scores[0][i], 0);
            check_if_lower_higher_array(busco_highest_longest, busco_scores[1][i], 2);
            check_if_lower_higher_array(busco_highest_longest, busco_scores[2][i], 4);
            check_if_lower_higher_array(busco_highest_longest, busco_scores[3][i], 6);
            check_if_lower_higher_array(busco_highest_longest, busco_scores[4][i], 8);
            result_map.put(i+1, "C:" + complete + " S:" + single + " D:" + duplicated + " F:" + fragmented + " M:" + missing); 
        }   
        create_busco_score_overview(output_name, busco_scores, txt_builder, db_name, busco_highest_longest);
        return result_map;
    }
    
    /**
     * 
     * @param array
     * @param value
     * @param position 
     */
    public static void check_if_lower_higher_array(double[] array, double value, int position) {
        if (value < array[position]) {
            array[position] = value;
        } 
        if (value > array[position+1]) {
              array[position+1] = value;
        } 
    }
    
    /**
     * 
     */
    public static void check_if_accurate_busco_database() {
        boolean correct_DB = true;
        if (!new File(INPUT_FILE + "/hmms").exists()) {
            correct_DB = false;
        }
        if (!new File(INPUT_FILE + "/info").exists()) {
              correct_DB = false;
        }
        if (!new File(INPUT_FILE + "/prfl").exists()) {
              correct_DB = false;
        }
        if (!correct_DB) {
            Pantools.logger.error("{} is not a BUSCO database.", INPUT_FILE);
            throw new RuntimeException("BUSCO database error");
        }
    }
    
    /**
     * 
     * @param output_name
     * @param busco_scores
     * @param txt_builder
     * @param db_name
     * @param busco_highest_longest 
     */
    public static void create_busco_score_overview(String output_name, double[][] busco_scores, StringBuilder txt_builder, 
            String db_name, double[] busco_highest_longest) {
        
        DecimalFormat df = new DecimalFormat("0.000");
        ArrayList<Double> complete_list = new ArrayList<>();
        ArrayList<Double> single_copy_list = new ArrayList<>();
        ArrayList<Double> dupli_list = new ArrayList<>();
        ArrayList<Double> frag_list = new ArrayList<>(); // fragmented
        ArrayList<Double> missing_list = new ArrayList<>(); // fragmented
        for (int i= 0; i < total_genomes; i++) {
            if (skip_array[i]) {
                continue;
            }
            complete_list.add(busco_scores[0][i]);
            single_copy_list.add(busco_scores[1][i]);
            dupli_list.add(busco_scores[2][i]);
            frag_list.add(busco_scores[3][i]);
            missing_list.add(busco_scores[4][i]);
        }
       
        double[] complete_m_a_sd = median_average_stdev_from_AL_double(complete_list); 
        double[] single_m_a_sd = median_average_stdev_from_AL_double(single_copy_list); 
        double[] dupli_m_a_sd = median_average_stdev_from_AL_double(dupli_list); 
        double[] frag_m_a_sd = median_average_stdev_from_AL_double(frag_list); 
        double[] missing_m_a_sd = median_average_stdev_from_AL_double(missing_list); 
     
        StringBuilder header = new StringBuilder();
        header.append("Category                            : average, median, lowest, highest ")
                .append("\nComplete BUSCOs (C)                 : ").append(df.format(complete_m_a_sd[1])).append(", ").append(df.format(complete_m_a_sd[0]))
                    .append(", ").append(df.format(busco_highest_longest[0])).append(", ").append(df.format(busco_highest_longest[1]))
                .append("\nComplete and single-copy BUSCOs (S) : ").append(df.format(single_m_a_sd[1])).append(", ").append(df.format(single_m_a_sd[0]) )
                    .append(", ").append(df.format(busco_highest_longest[2])).append(", ").append(df.format(busco_highest_longest[3]))
                .append("\nComplete and duplicated BUSCOs (D)  : ").append(df.format(dupli_m_a_sd[1])).append(", ").append(df.format(dupli_m_a_sd[0]))
                    .append(", ").append(df.format(busco_highest_longest[4])).append(", ").append(df.format(busco_highest_longest[5]))
                .append("\nFragmented BUSCOs (F)               : ").append(df.format(frag_m_a_sd[1])).append(", ").append(df.format(frag_m_a_sd[0]))
                    .append(", ").append(df.format(busco_highest_longest[6])).append(", ").append(df.format(busco_highest_longest[7]))
                .append("\nMissing BUSCOs (M)                  : ").append(df.format(missing_m_a_sd[1])).append(", ").append(df.format(missing_m_a_sd[0]))
                    .append(", ").append(df.format(busco_highest_longest[8])).append(", ").append(df.format(busco_highest_longest[9])).append("\n");
        write_string_to_file_in_DB(header.toString() + txt_builder.toString(), "busco/" + db_name + "/" + output_name + "/busco_scores.txt");
    }
    
    /**
     * 
     * @param hmm_map
     * @param output_path
     * @param db_name
     * @param questionable_buscos 
     * @param total_buscos
     * @return
     */
    public static HashMap<String, StringBuilder> create_busco_hmm_overview(HashMap<String, ArrayList<Integer>> hmm_map, String output_path, String db_name, 
            String[] questionable_buscos, int total_buscos) {

        String[] categories = {"Complete", "Missing", "Fragmented", "Duplicated"};
        HashSet<String> added_hms = new HashSet<>();
        HashMap<String, StringBuilder> hmm_per_genome = new HashMap<>();
        StringBuilder output_builder = new StringBuilder();
        StringBuilder perfect_builder = new StringBuilder();
        int perfect_counter = 0;
        for (String key : hmm_map.keySet()) {
            String[] key_array = key.split("#");
            if (added_hms.contains(key_array[0])) {
                continue;
            }
            added_hms.add(key_array[0]);
            boolean first = true, perfect = false;
            HashMap<String, Integer> phenotype_counter = new HashMap<>();
            for (String category : categories) {
                ArrayList<Integer> genome_nrs = hmm_map.get(key_array[0] + "#" + category);
                if (genome_nrs == null) {
                    if (first) {
                        output_builder.append(key_array[0]).append("\n"); 
                    }
                    output_builder.append(category).append(":\n");
                    continue;
                } else if (genome_nrs.size() == adj_total_genomes) {
                    if (category.equals("Complete")) {
                        perfect_counter ++;
                        perfect = true;
                        if (first) {
                            perfect_builder.append(key_array[0]).append("\n"); 
                        }
                        perfect_builder.append(category).append(" (").append(genome_nrs.size()).append("): all genomes\n\n");
                    } else {
                        if (first) {
                            output_builder.append(key_array[0]).append("\n"); 
                        }
                        output_builder.append(category).append(" (").append(genome_nrs.size()).append("): all genomes\n");
                    }
                   
                    for (int genome_nr : genome_nrs) {
                        try_incr_SB_hashmap(hmm_per_genome, genome_nr + "#" + category, key_array[0] + ",");
                    }
                    break;
                } else {
                    if (first) {
                        output_builder.append(key_array[0]).append("\n"); 
                    }
                    output_builder.append(category).append(" (").append(genome_nrs.size()).append("): ").append(genome_nrs.toString().replace("[","").replace("]","")).append("\n");
                }
               
                if (PHENOTYPE != null && !category.equals("Complete")) {
                    for (int genome_nr : genome_nrs) {
                        String pheno = geno_pheno_map.get(genome_nr);
                        try_incr_hashmap(phenotype_counter, pheno, 1);
                    }
                }
            
                for (int genome_nr : genome_nrs) {
                    try_incr_SB_hashmap(hmm_per_genome, genome_nr + "#" + category, key_array[0] + ",");
                }
                first = false;
            }

            if (!perfect) {
                if (PHENOTYPE != null) {
                    output_builder.append("M/F/D per phenotype: ");
                    for (Map.Entry<String, Integer> entry : phenotype_counter.entrySet()) {
                        String pheno = entry.getKey();
                        int count = entry.getValue();
                        if (pheno.equals("Unknown") || pheno.equals("?")) {
                            output_builder.append(pheno).append(" ").append(count).append(", ");
                        } else {
                            int threshold = phenotype_threshold_map.get(pheno);
                            output_builder.append(pheno).append(" ").append(count).append("/").append(threshold).append(", ");
                        }
                    }
                    output_builder.append("\n\n");
                } else {
                    output_builder.append("\n");
                } 
            }
        }
        String header = "HMMs 'Complete' in " + adj_total_genomes + " genomes: " + perfect_counter + "/" + total_buscos + "\n";
        write_string_to_file_in_DB(header + "\n" + output_builder.toString() + perfect_builder.toString(),
                "busco/" + db_name + "/" + output_path + "/hmm_overview.txt");
        return hmm_per_genome;
    }

    /**
     * 
     * @param questionable_buscos
     * @param total_buscos
     * @param hmm_per_genome
     * @param db_name
     * @param output_path 
     */
    public static void create_busco_csv_overview(String[] questionable_buscos, int total_buscos, HashMap<String, StringBuilder> hmm_per_genome, String db_name, String output_path) {
        String[] categories = {"Complete", "Missing", "Fragmented", "Duplicated"};
        StringBuilder output_builder = new StringBuilder("#split file on semicolon. Total BUSCOs: " + total_buscos 
                + "\nGenome;Complete single copy (total);Complete single copy (total percentage);Missing HMMs (total);"
                + "Missing HMMs (total percentage);Missing HMMs;Fragmented HMMs (total);Fragmented HMMs (total percentage);"
                + "Fragmented HMMs;Duplicated HMMs (total);Duplicated HMMs (total percentage);Duplicated HMMs");
        
        if (questionable_buscos.length > 0) {
            output_builder.append(";questionable BUSCOs not correct (max ").append(questionable_buscos.length).append(")\n");
        } else {
            output_builder.append("\n");
        }
                       
        for(int i=1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            output_builder.append(i).append(";");
            int wrong_busco_counter = 0;
            for (int j=0; j < categories.length; j++) {
                StringBuilder value_builder = hmm_per_genome.get(i + "#" + categories[j]);
                if (value_builder == null) {
                    output_builder.append("-;-;-");
                } else if (categories[j].equals("Complete")) {
                    String[] value_array = value_builder.toString().split(",");
                    String percentage = get_percentage_str(value_array.length, total_buscos, 3); 
                    output_builder.append(value_array.length).append(";").append(percentage); 
                } else {
                    String value = value_builder.toString().replaceFirst(".$","");
                    String[] value_array = value.split(",");
                    for (String busco : value_array) {
                        if (ArrayUtils.contains(questionable_buscos, busco)) {
                            wrong_busco_counter ++;
                        }
                    }
                    String percentage = get_percentage_str(value_array.length, total_buscos, 3); 
                    output_builder.append(value_array.length).append(";").append(percentage).append(";").append(value);
                }
                output_builder.append(";");
            }
            if (questionable_buscos.length > 0) {
                output_builder.append(wrong_busco_counter);
            }
            output_builder.append("\n");
        }
        write_SB_to_file_in_DB(output_builder, "busco/" + db_name + "/" + output_path + "/busco_overview.csv");
    }
    
    /**
     * The function id from GO, TIGRFAM, PFAM or InterPRO must exactly match one of the user input IDs
     * For phobius/signalp there are 2 user input options: 'signal_peptide' and 'transmembrane' 
     * 
     * @param function_node_set
     * @param hm_id_str
     * @param groups_with_function_or_name
     */
    public static void check_function_output_matches(HashSet<Node> function_node_set, String hm_id_str, 
            HashMap<String, HashSet<String>> groups_with_function_or_name) {
        
        if (function_node_set.isEmpty() && (SELECTED_LABEL.equals("NONE") || SELECTED_LABEL.equals("none"))) {
            try_incr_hashset_hashmap(groups_with_function_or_name, "No function#function", hm_id_str);
        }
        ArrayList<String> function_list = new ArrayList<>(Arrays.asList(SELECTED_LABEL.split("\\s*,\\s*"))); // multiple functions can be provided
        for (Node function_node : function_node_set) {
            if (function_node.hasLabel(PHOBIUS_LABEL)) {
                String signalpep = (String) function_node.getProperty("phobius_signal_peptide");
                int transmembrane = (int) function_node.getProperty("phobius_signal_peptide");
                if (function_list.contains("signal_peptide") && signalpep.equals("yes")) {
                    try_incr_hashset_hashmap(groups_with_function_or_name, "signal_peptide#function", hm_id_str); 
                }
                if (function_list.contains("transmembrane") && transmembrane > 0) {
                    try_incr_hashset_hashmap(groups_with_function_or_name, "transmembrane#function", hm_id_str); 
                }
            } else if (function_node.hasLabel(SIGNALP_LABEL) && function_list.contains("signal_peptide")) {
                try_incr_hashset_hashmap(groups_with_function_or_name, "signal_peptide#function", hm_id_str); 
            } else if (function_node.hasLabel(BGC_LABEL)) {
                String type = (String) function_node.getProperty("type");
                if (function_list.contains(type)) {
                    try_incr_hashset_hashmap(groups_with_function_or_name, type + "#function", hm_id_str);
                }
            } else { // pfam, go, tigrfam, interpro
                String id = (String) function_node.getProperty("id");
                if (function_list.contains(id)) {
                    try_incr_hashset_hashmap(groups_with_function_or_name, id + "#function", hm_id_str);
                }
            }
        }
    }
    
    /**
     * 
     * @param gene_node
     * @param mrna_node
     * @param function_set
     * @param function_node_set
     * @param gene_info_builder
     * @param groups_with_function
     * @param hm_id_str
     * @return 
     */
    public static String group_info_get_f_annotations(Node gene_node, Node mrna_node, HashSet<String> function_set, HashSet<Node> function_node_set, 
            StringBuilder gene_info_builder, HashSet<String> groups_with_function, String hm_id_str) {
        
        StringBuilder output_builder = new StringBuilder("Functional annotations: ");
        get_specific_FA_append_output(RelTypes.has_go, mrna_node, output_builder, function_set, function_node_set);
        get_specific_FA_append_output(RelTypes.has_pfam, mrna_node, output_builder, function_set, function_node_set);
        get_specific_FA_append_output(RelTypes.has_tigrfam, mrna_node, output_builder, function_set, function_node_set);
        get_specific_FA_append_output(RelTypes.has_interpro, mrna_node, output_builder, function_set, function_node_set);
        get_bgc_append_output(gene_node, output_builder, function_set, function_node_set);
        get_phobius_signalp_append_output(mrna_node, output_builder, function_set, function_node_set);
        get_cog_append_output(mrna_node, output_builder, function_set, function_node_set);
        String functions = output_builder.toString();
        if (functions.length() > 24) {
            groups_with_function.add(hm_id_str);
            gene_info_builder.append(functions).append("\n");
        } else {
            gene_info_builder.append("No functional annotations connected to the gene and mRNA nodes\n");
        }
        return output_builder.toString();
    }
    
    /**
     * 
     * @param mrna_node
     * @param output_builder
     * @param function_set
     * @param function_node_set 
     */
    public static void get_cog_append_output(Node mrna_node, StringBuilder output_builder, HashSet<String> function_set, HashSet<Node> function_node_set) {
        if (mrna_node.hasLabel(COG_LABEL)) {
            String cog_category = (String) mrna_node.getProperty("COG_category");
            function_set.add("COG category " + cog_category + ", "); 
            output_builder.append("COG category ").append(cog_category).append(", ");
            function_node_set.add(mrna_node);
        }
    }
    
    /**
     * 
     * @param mrna_node
     * @param output_builder
     * @param function_set
     * @param function_node_set 
     */
    public static void get_phobius_signalp_append_output(Node mrna_node, StringBuilder output_builder, HashSet<String> function_set, HashSet<Node> function_node_set) {
        if (!mrna_node.hasLabel(PHOBIUS_LABEL) && !mrna_node.hasLabel(SIGNALP_LABEL)) {
            return;
        }
       
        if (mrna_node.hasProperty("phobius_signal_peptide")) {
            String phobius_signalpep = (String) mrna_node.getProperty("phobius_signal_peptide");
            if (phobius_signalpep.equals("yes")) {
                function_set.add("Phobius_signal_peptide");
                output_builder.append("Phobius signalpeptide, ");
                function_node_set.add(mrna_node);
            }
        } 
        
        if (mrna_node.hasProperty("phobius_transmembrane")) { 
            int domains = (int) mrna_node.getProperty("Phobius_transmembrane");
            function_set.add("Phobius_transmembrane domains");
            output_builder.append(domains).append(" Phobius transmembrane domains, "); 
            function_node_set.add(mrna_node);
        }
            
        if (mrna_node.hasProperty("signalp_signal_peptide")) { 
            String signalp_signalpep = (String) mrna_node.getProperty("phobius_transmembrane"); // type can be 'yes', 'SP(Sec/SPI)', 'LIPO(Sec/SPII)' or 'TAT(Tat/SPI)' 
            if (signalp_signalpep.equals("yes")) {
                signalp_signalpep = "(no type)";
            }
            function_set.add("SignalP_signal_peptide");
            output_builder.append("SignalP signal peptide, "); 
            function_node_set.add(mrna_node);
        }
    }
    
    /**
     * 
     * @param gene_node
     * @param output_builder
     * @param function_set
     * @param function_node_set 
     */
    public static void get_bgc_append_output(Node gene_node, StringBuilder output_builder, HashSet<String> function_set, HashSet<Node> function_node_set) {
        Iterable<Relationship> rels = gene_node.getRelationships(RelTypes.part_of);
        for (Relationship rel : rels) {
            Node bcg_node = rel.getEndNode();
            String type = (String) bcg_node.getProperty("type");
            output_builder.append("part of '").append(type).append("' BGC,");
            function_set.add("part of '" + type + "' BGC");
            function_node_set.add(bcg_node);
        }
    }
    
    /**
     * 
     * @param reltype
     * @param mrna_node
     * @param output_builder
     * @param function_set
     * @param function_node_set 
     */
    public static void get_specific_FA_append_output(RelTypes reltype, Node mrna_node, StringBuilder output_builder, HashSet<String> function_set, 
            HashSet<Node> function_node_set) {
        
        Iterable<Relationship> rels = mrna_node.getRelationships(reltype);
        for (Relationship rel : rels) {
            Node function_node = rel.getEndNode();
            String type = (String) function_node.getProperty("id");
            output_builder.append(type).append(", ");
            function_set.add(type);
            function_node_set.add(function_node);
        }
    }


    /**
     * remove_phenotype callable by user
     *
     * Removal of phenotype nodes or properties on phenotype nodes.
     * phenotype properties:
     * The user included a phenotype property via --phenotype. In this case PHENOTYPE != null.
     *
     * phenotype nodes:
     * With no --phenotype included. PHENOTYPE == null and all nodes phenotype nodes will be removed.
     *
     */
    public void removePhenotype() {
        Pantools.logger.info("Removing phenotype information/nodes from the pangenome.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenomeNode = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenomeNode); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the pangenome database.");
            throw new RuntimeException("Database error");
        }
        if (PHENOTYPE != null) { // verify the phenotype, not allowed to be "genome"
            if (PHENOTYPE.equals("genome")) {
                System.out.println("\rThe --phenotype property is not allowed to be called \"genome\"");
                System.exit(1);
            }
        }

        int nodeCounter = 0, propertyCounter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> phenotypeNodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (phenotypeNodes.hasNext()) { // go over phenotype nodes. Delete phenotype node or certain property
                Node phenotypeNode = phenotypeNodes.next();
                int genomeNr = (int) phenotypeNode.getProperty("genome");
                if (skip_array[genomeNr-1]) {
                    continue;
                }
                if (PHENOTYPE == null) { // no phenotype given, remove nodes
                    Iterable<Relationship> relations = phenotypeNode.getRelationships(); // has_phenotype relationship
                    for (Relationship relation : relations){
                        relation.delete();
                    }
                    phenotypeNode.delete();
                    nodeCounter++;
                } else {
                    if (phenotypeNode.hasProperty(PHENOTYPE)) {
                        phenotypeNode.removeProperty(PHENOTYPE);
                        propertyCounter ++;
                    }
                }
            }

            // ask user for verification before removal of nodes and properties
            boolean remove = false;
            if (PHENOTYPE == null && nodeCounter > 1) {
                System.out.print("\r" + nodeCounter + " phenotype nodes will be removed. Do you want to continue [y/n]?\n-> ");
                Scanner s = new Scanner(System.in);
                String str = s.nextLine().toLowerCase();
                if (str.equals("y") || str.equals("yes")) {
                    remove = true;
                } else {
                    System.out.println("\rNothing was removed\n");
                    return;
                }
            } else if (propertyCounter > 1) {
                System.out.print("\rThe property '" + PHENOTYPE + "' will be removed from " + propertyCounter + " phenotype nodes. Do you want to continue [y/n]?\n-> ");
                Scanner s = new Scanner(System.in);
                String str = s.nextLine().toLowerCase();
                if (str.equals("y") || str.equals("yes")) {
                    remove = true;
                } else {
                    System.out.println("\rNothing was removed\n");
                    return;
                }
            } else if (PHENOTYPE != null && propertyCounter == 0) {
                System.out.println("\rThe property '" + PHENOTYPE + "' did not match with any phenotype nodes\n");
            }

            if (remove) {
                tx.success();
                System.out.println("\rRemoval was successful");
                long phenotypeNodeCount = count_nodes(PHENOTYPE_LABEL);
                new File(WORKING_DIRECTORY + "phenotype_overview.txt").delete(); // delete older summmary file
                if (phenotypeNodeCount > 0) { // create a new phenotype_overview.txt if there still are phenotype nodes
                    phenotype_overview();
                }
            } else {
                tx.failure();
                System.out.println("\rNothing was removed\n");
            }
        }
    }


    /**
     * Requires:
     * -dp 
     * --phenotype
     * 
     * Optional: 
     * --append 
     * --skip or --reference
     */
    public void addPhenotype(Path phenotypesFile, int bins, boolean updateNodes) {
        Pantools.logger.info("Adding phenotype information to the pangenome database.");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenomeNode = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenomeNode); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the pangenome database.");
            throw new RuntimeException("Database error");
        }

        int columnsInHeader = 0, lineCounter = 0;
        HashMap<Integer, String> propertyPerColumn = new HashMap<>();
        HashSet<String> phenotypeProperties = new HashSet<>();
        if (!updateNodes) {
            removePreviousPhenotypeNodes();
        }
        Node[] phenotypeNodes = getExistingPhenotypeNodes();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            try {
                BufferedReader br = Files.newBufferedReader(phenotypesFile);
                for (String line = br.readLine(); line != null; line = br.readLine()) {
                    line = line.trim();
                    String[] lineArray = line.replace(", ", ",").replace(" ,", ",").split(",");
                    if (lineCounter == 0) { // first line is the header
                        if (line.contains(",,")) { // header is not allowed to have empty columns
                            throw new RuntimeException("The header of your csv file is not correctly formatted, no empty column double commas allowed!");
                        }
                        if (lineArray.length < 2) {
                            throw new RuntimeException("The input csv file must have at least two columns. The first column must always be the genome numbers");
                        }
                        columnsInHeader = lineArray.length;
                        for (int i = 1; i < lineArray.length; i++) { // starts at 1 because 0 is 'Genome'
                            String phenotypeProperty = lineArray[i].trim();
                            propertyPerColumn.put(i, phenotypeProperty);
                        }
                    } else {
                        if (columnsInHeader < lineArray.length) {
                            throw new RuntimeException("The header (first line) contained less columns as line " +
                                    (line+1) + ":" + columnsInHeader + " against" + lineArray.length);
                        }

                        int genomeNr;
                        try {
                            genomeNr = Integer.parseInt(lineArray[0]);
                        } catch (NumberFormatException numberFormatException) {
                            throw new RuntimeException("Only genome numbers are allowed in the leftmost column. Found " + lineArray[0]);
                        }
                        Node phenotypeNode = getPhenotypeNode(genomeNr, phenotypeNodes);
                        for (int i = 1; i < columnsInHeader; i++) { // skip first column because it holds genome numbers
                            String phenotypeValue;
                            try {
                                phenotypeValue = lineArray[i];
                            } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) { // This happens when the last column is empty on some rows
                                phenotypeValue = "";
                            }
                            verifyPhenotypeValue(phenotypeValue);
                            String phenotypeProperty = propertyPerColumn.get(i);
                            writeValueToPhenotypeNode(phenotypeValue, phenotypeNode, phenotypeProperty);
                            phenotypeProperties.add(phenotypeProperty);
                        }
                    }
                    lineCounter ++;
                }
            } catch (IOException ioe) {
                throw new RuntimeException("Something went wrong while reading: " + phenotypesFile);
            }
            tx.success(); // transaction successful, commit changes
        }
        binPhenotypeValues(phenotypeProperties, bins, phenotypeNodes);
        String createdOrUpdated = "created";
        if (updateNodes) {
            createdOrUpdated = "updated";
        }

        System.out.println("\rSuccessfully " + createdOrUpdated + " phenotype nodes for " + (lineCounter-1) + " genomes!");
        phenotype_overview();
    }

    /**
     * Check if the phenotype value (string) contains any disallowed characters.
     * Currently only semicolon ;
     * @param phenotypeValue a phenotype value
     */
    private void verifyPhenotypeValue(String phenotypeValue) {
        if (phenotypeValue.contains(";")) {
            throw new RuntimeException("Phenotype values not allowed to have semicolon characters");
        }
    }

    private void updateValuesPerPhenotype(int genomeNr, HashMap<String, Object[]> valuesPerPhenotype,
                                          String phenotypeProperty, String phenotypeValue) {

        Object[] array = valuesPerPhenotype.get(phenotypeProperty);
        if (array == null) {
            array = new Object[total_genomes];
        }
        array[genomeNr-1] = phenotypeValue;
        valuesPerPhenotype.put(phenotypeProperty, array);
    }

    /**
     * only go over the recently added properties
     * @param phenotypeProperties all phenotype properties part of the current analysis
     * @param bins number of bins
     * @param phenotypeNodes array with phenotype nodes on genomeNr-1 positions
     */
    private void binPhenotypeValues(HashSet<String> phenotypeProperties, int bins, Node[] phenotypeNodes) {
        HashMap<String, Object[]> valuesPerPhenotype = retrieveAllPhenotypesAsObject();
        boolean first = true;

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (String phenotypeProperty : phenotypeProperties) {
                Object[] phenotypeValues = valuesPerPhenotype.get(phenotypeProperty);
                double[] lowestHighestValue = findHighestLowestValue(phenotypeValues);
                if (lowestHighestValue == null) { // null when not every value is a number
                    continue;
                }

                ArrayList<Double> levels = determineBinLevels(lowestHighestValue[0], lowestHighestValue[1], bins);
                if (first) {
                    Pantools.logger.info("Phenotypes with only numerical values recognized. Numbers are considered different phenotypes if they do not exactly match.");
                    Pantools.logger.info("Extra phenotypes are generated which place these values in {} equally sized bins.", bins);
                    first = false;
                }
                Pantools.logger.info(" Phenotype '{}_binned' range of bins: {}", phenotypeProperty, levels.toString().replace("[","").replace("]",""));
                for (int i = 0; i < phenotypeValues.length; i++) {
                    Object value = phenotypeValues[i];
                    if (value == null) {
                        continue;
                    }
                    Node phenotypeNode = phenotypeNodes[i];
                    phenotypeNode.removeProperty(phenotypeProperty + "_binned");
                    try {
                        double number = Double.parseDouble((String) value);
                        for (int l = 0; l < levels.size() - 1; l++) {
                            if (number >= levels.get(l) && number <= levels.get(l + 1)) {
                                phenotypeNode.setProperty(phenotypeProperty + "_binned", "bin " + (l + 1));
                                break;
                            }
                        }
                    } catch (NumberFormatException ex) {
                        phenotypeNode.setProperty(phenotypeProperty + "_binned", "Unknown");
                    }
                }
            }
            tx.success();
        }
    }

    private double[] findHighestLowestValue(Object[] numbers) {
        double lowest = Double.MAX_VALUE, highest = Double.MIN_VALUE;
        for (Object value : numbers) {
            if (value == null) { // genome was not included, skip entirely
                continue;
            }
            try {
                double doubleValue = Double.parseDouble((String) value);
                if (doubleValue > highest) {
                    highest = doubleValue;
                }
                if (doubleValue < lowest) {
                    lowest = doubleValue;
                }
            } catch (NumberFormatException numberFormatException) {
                return null;
            }
        }
        return new double[]{lowest, highest};
    }

    private ArrayList<Double> determineBinLevels(double lowest, double highest, int bins) {
        double difference = highest - lowest;
        double step = difference / bins;
        ArrayList<Double> levels = new ArrayList<>();
        levels.add(lowest);
        for (int i = 1; i < bins; i++) {
            levels.add((i * step) + lowest);
        }
        levels.add(highest);
        return levels;
    }

    /**
     * Retrieve or create 'phenotype' node of a specific genome
     * @param genomeNr
     * @param phenotypeNodes array with phenotype nodes on genomeNr-1 positions
     * @return 
     */
    public Node getPhenotypeNode(int genomeNr, Node[] phenotypeNodes) {
        Node phenotypeNode;
        if (phenotypeNodes[genomeNr-1] != null) { // node already exists from previous run
            phenotypeNode = phenotypeNodes[genomeNr-1];
        } else {
            phenotypeNode = GRAPH_DB.createNode(PHENOTYPE_LABEL);
            phenotypeNode.setProperty("genome", genomeNr);
            if (!PROTEOME) {
                Node genomeNode = GRAPH_DB.findNode(GENOME_LABEL, "number", genomeNr);
                if (genomeNode == null) {
                    throw new RuntimeException("No genome has been found with number: " + genomeNr);
                }
                genomeNode.createRelationshipTo(phenotypeNode, RelTypes.has_phenotype);
            }
            phenotypeNodes[genomeNr-1] = phenotypeNode;
        }
        return phenotypeNode;
    }

    /**
     * Three checks: is it an Integer? Double? Boolean? If not it stays a string.
     * @param phenotypeValue
     * @param phenotypeNode
     * @param phenotypeProperty
     * @return
     */
    private void writeValueToPhenotypeNode(String phenotypeValue, Node phenotypeNode, String phenotypeProperty) {
        if (phenotypeNode.hasProperty(phenotypeProperty)) {
            phenotypeNode.removeProperty(phenotypeProperty); // remove the current phenotype value
        }

        try { // is it an Integer?
            int i = Integer.parseInt(phenotypeValue);
            phenotypeNode.setProperty(phenotypeProperty, i);
            return;
        } catch (NumberFormatException nfe) {

        }

        try { // is it a Double?
            double d= Double.parseDouble(phenotypeValue);
            phenotypeNode.setProperty(phenotypeProperty, d);
            return;
        } catch (NumberFormatException nfe) {

        }

        // is it an Boolean?
        if (phenotypeValue.toUpperCase().equals("TRUE") || phenotypeValue.toUpperCase().equals("FALSE") ){
            boolean bool = Boolean.parseBoolean(phenotypeValue);
            phenotypeNode.setProperty(phenotypeProperty, bool);
            return;
        }
        // its just a string
        phenotypeNode.setProperty(phenotypeProperty, phenotypeValue);
    }
        
    /**
     * Retrieve phenotypes names and values from the phenotype nodes
     * @return 
     */
    public HashMap<String, Object[]> retrieveAllPhenotypesAsObject() {
        HashMap<String, Object[]> valuesPerPhenotype = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> phenotypeNodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (phenotypeNodes.hasNext()) {
                Node phenotypeNode = phenotypeNodes.next();
                int genomeNr = (int) phenotypeNode.getProperty("genome");
                for (String phenotypeProperty : phenotypeNode.getPropertyKeys()) {
                    if (phenotypeProperty.equals("genome")) {
                        continue;
                    }
                    updateValuesPerPhenotype(genomeNr, valuesPerPhenotype, phenotypeProperty, String.valueOf(phenotypeNode.getProperty(phenotypeProperty)));
                }
            }
            tx.success();
        }
        return valuesPerPhenotype;
    }

    /**
     * Retrieve phenotypes names and values from the phenotype nodes
     * @return
     */
    public HashMap<String, HashSet<String>> retrieve_all_phenotypes() {
        HashMap<String, HashSet<String>> values_per_phenotype = new HashMap<>();
        ResourceIterator<Node> pheno_nodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
        while (pheno_nodes.hasNext()) {
            Node pheno_node = pheno_nodes.next();
            Iterable<String> prop_keys = pheno_node.getPropertyKeys();
            for (String prop_key : prop_keys) {
                if (prop_key.equals("genome")) {
                    continue;
                }
                Object value = pheno_node.getProperty(prop_key);
                String value_str = "";
                if (value instanceof Integer) {
                    int value_int = (int) value;
                    value_str = Integer.toString(value_int);
                } else if (value instanceof Boolean) {
                    boolean value_bool = (boolean) value;
                    value_str = String.valueOf(value_bool);
                } else if (value instanceof Double) {
                    double valueDouble = (double) value;
                    value_str = Double.toString(valueDouble);
                } else if (value instanceof String) {
                    value_str = (String) value;
                } else {
                    Pantools.logger.error("something else.. finish function.");
                    System.exit(1);
                }
                try_incr_hashset_hashmap(values_per_phenotype, prop_key, value_str);
            }
        }
        return values_per_phenotype;
    }
    
    /**
     * Main method for gene_classification
     * Creates all files in <database dir>/gene_classification
     * @param pavs whether to use PAV information
     * NB: When variation is included, phenotype related calculations can NOT be used.
     * NB: Prepare CIRCOS input file when the `--mode 1` argument is included (Unavailable for the panproteome). Currently disabled!
    */
    public void gene_classification(boolean pavs) {
        Pantools.logger.info("Starting gene classification.");
        create_directory_in_DB("gene_classification");
        report_number_of_threads(); // prints how many threads were selected by user
        check_database(); // starts up the graph database if needed

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes  
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            check_current_grouping_version(); // check which version of homology grouping is active
            update_skip_array_based_on_grouping(pangenome_node);
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            throw new RuntimeException("Database error");
        }

        ArrayList<String> genome_list; // will store a list of all genome names

        ArrayList<Node> sco_nodes = new ArrayList<>();
        StringBuilder all_hmgroups = new StringBuilder();
        HashMap<String, int[]> hmgroup_counts = new HashMap<>(); // key is the hmgroup, value is an array with the copy number counts for each genome
        HashMap<String, StringBuilder> cnv_map = new HashMap<>(); // key is genome number combined with 'core' or 'access' 
        HashMap<String, int[]> core_access_uni_map = new HashMap<>(); // key is genome number + '_total' or '_distint'
        HashMap<String, HashSet<String>> unique_hm_per_genome = new HashMap<>(); // homology group ids per genome for for singletons
        HashMap<String, int[]> shared_genes_map = new HashMap<>(); // key is combination of genomes, value is [total all, total distinct, shared all, shared distinct]
      
        HashMap<Integer, String> accessory_per_count = new HashMap<>(); // key is total number of genomes,
        HashMap<String, Integer> hmsize_frequency = new HashMap<>();
        HashMap<String, StringBuilder> accessory_combinations = new HashMap<>();
        HashMap<String, ArrayList<Node>> hmgroups_per_class = new HashMap<>(); // three keys. core, accessory, unique
        
        HashMap<String, StringBuilder> phenotype_result_map = new HashMap<>(); // key is phenotype + "#shared" or + "#specific" or "#exclusive# + genomes with phenotype"
        HashMap<String, StringBuilder> phenotype_cnv = new HashMap<>();// key is phenotype 
        HashMap<Node, StringBuilder> phenotype_disrupted = new HashMap<>();// key is hmgroup id 
        HashMap<String, Integer> pheno_disrupt_counter = new HashMap<>();
        HashMap<Double, ArrayList<String>> pheno_xsquare_map = new HashMap<>();

        int hmgroup_counter = 0; 
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            genome_list = create_genome_list(pavs);

            retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            core_unique_thresholds_for_classification(genome_list, "mRNAs/proteins"); // sets core_threshold and unique_threshold
            if (PHENOTYPE != null) {
                Pantools.logger.info("The '{}' phenotype is included. Finding phenotype shared/specific mRNAs.", PHENOTYPE);
            } else { 
               Pantools.logger.info("No --phenotype was included. No phenotype specific genes can be identified.");
            }

            if (Mode.equals("0") && !PROTEOME) { // circos plots are disabled, but don't remove
                //Pantools.logger.info("Not creating Circos input files (Missing '--mode 1' argument).");
            } else if (Mode.equals("1") && PROTEOME) {
                //Pantools.logger.info("Unable to create Circos files with a panproteome.");
            }

            Pantools.logger.debug("Filling core_access_uni_map, cnv_map, accessory_per_count, shared_genes_map.");
            for (String genome : genome_list) {
                int[] empty1 = {0,0,0}; // core, accessory, unique
                int[] empty2 = {0,0,0}; // core, accessory, unique
                core_access_uni_map.put(genome + "_total", empty1);
                core_access_uni_map.put(genome + "_distinct", empty2);
                StringBuilder core_builder = new StringBuilder();
                StringBuilder access_builder = new StringBuilder();
                cnv_map.put(genome + "core", core_builder);
                cnv_map.put(genome + "access", access_builder);
                accessory_per_count.put(genome_list.indexOf(genome), "");
                for (String genome2 : genome_list) {
                    int[] empty3 = {0,0,0,0,0,0}; // [total all, total distinct, shared all, shared distinct, total informative distinct, shared informative distinct]
                    if (genome_list.indexOf(genome2) > genome_list.indexOf(genome)) {
                        shared_genes_map.put(genome + "_" + genome2, empty3);
                    } else {
                        shared_genes_map.put(genome2 + "_" + genome, empty3);
                    }
                }
            }

            ArrayList<Node> hm_nodes = retrieve_homology_group_nodes();
            Pantools.logger.info("Calculating core, accessory and unique homology groups ({})).", hm_nodes.size());
            for (Node hm_node : hm_nodes) {
                hmgroup_counter ++;
                long hm_id = hm_node.getId();

                // create hashmaps for phenotype
                HashMap<String, Integer> pheno_count_map = new HashMap<>();
                HashMap<String, int[]> phenotype_min_max = new HashMap<>();

                int[] copy_number_array = getCopyNumberArray(hm_node, genome_list, pavs);
                int members = 0;
                for (int copy_number : copy_number_array) {
                    members += copy_number;
                }
                hmgroup_counts.put(String.valueOf(hm_id), copy_number_array);
                Pantools.logger.debug("hmgroup {} has {} members (copy number array: {})).", hm_id, members, Arrays.toString(copy_number_array));

                // fill phenotype maps with 0's
                try_incr_hashmap(hmsize_frequency , members + "_total", 1);
                for (String phenotype : phenotype_map.keySet()) {
                    pheno_count_map.put(phenotype, 0);
                    int[] min_max = {9999, 0};
                    int[] class_value = {9999, 0};
                    phenotype_min_max.put(phenotype, min_max);
                    phenotype_min_max.put(phenotype + "_class", class_value);
                }

                // determine which positions in the copy number array are present
                ArrayList<Integer> present_genomes = check_array_if_unique(copy_number_array, genome_list);
                Pantools.logger.debug("hmgroup {} of {} ({})) has {} members and is present in {} genomes.", hmgroup_counter, hm_nodes.size(), hm_id, members, present_genomes.size());

                // determine whether the homology group is present in one genome only
                boolean unique = false; // this has to be known already so 'informative' genes can be added in a later function
                if (present_genomes.isEmpty()) { // due to skipped genomes, no proteins are present in the group
                    hmgroup_counter--;
                    continue;
                } else if (present_genomes.size() == 1) { // genes present in 1 genome are not informative for the gene distance tree
                    unique = true;
                    Pantools.logger.debug("hmgroup {} is present in only one genome.", hm_id);
                }
                all_hmgroups.append(hm_id).append(",");
                try_incr_hashmap(hmsize_frequency, present_genomes.size() + "_genomes", 1);

                // determine whether the homology group is single copy core
                AtomicInteger lowest = new AtomicInteger(99999);
                boolean sco_group = read_cnv_array_to_count_genes(genome_list, copy_number_array, shared_genes_map, unique, pheno_count_map, phenotype_min_max, lowest);
                if (sco_group && (present_genomes.size() >= core_threshold)) { // single copy ortholog
                    sco_nodes.add(hm_node);
                    Pantools.logger.debug("hmgroup {} is a single copy core group.", hm_id);
                }

                // ???
                determine_gclass_pheno_specific(pheno_count_map, hm_node, phenotype_result_map, hmgroups_per_class, phenotype_disrupted, pheno_disrupt_counter,
                        phenotype_min_max, pheno_xsquare_map);

                // set the number of core, accessory and unique genes in the core_access_uni_map
                if (present_genomes.size() <= unique_threshold && present_genomes.size() > 0) { // unique
                    Pantools.logger.debug("hmgroup {} is unique.", hm_id);
                    
                    // increment hmgroups_per_class
                    try_incr_AL_hashmap(hmgroups_per_class, "unique", hm_node);

                    // increment unique_hm_per_genome and total value in core_access_uni_map based on the copy number array
                    for (int i = 0; i < copy_number_array.length; i++) {
                        if (copy_number_array[i] > 0) {
                            String genome = genome_list.get(i);
                            try_incr_hashset_hashmap(unique_hm_per_genome, genome, hm_id + "");
                            incr_array_hashmap(core_access_uni_map, genome + "_total", 1, 2);
                        }
                    }

                    // increment distinct value in core_access_uni_map based on present_genomes
                    for (int genome_nr2 : present_genomes) {
                        String genome2 = genome_list.get(genome_nr2);
                        incr_array_hashmap(core_access_uni_map, genome2 + "_distinct", 1, 2);
                    }
                } else if (present_genomes.size() >= core_threshold) { // core 
                    Pantools.logger.debug("hmgroup {} is core.", hm_id);

                    determine_pheno_cnv(phenotype_min_max, phenotype_cnv, "core", hm_node);
                    try_incr_AL_hashmap(hmgroups_per_class, "core", hm_node);
                    increase_cnv_map(genome_list, copy_number_array, lowest.get(), hm_id, cnv_map, core_access_uni_map, "core");
                } else if (present_genomes.size() > 1) { // accessory
                    Pantools.logger.debug("hmgroup {} is accessory.", hm_id);
                    
                    determine_pheno_cnv(phenotype_min_max, phenotype_cnv, "accessory", hm_node);
                    try_incr_AL_hashmap(hmgroups_per_class, "accessory", hm_node);
                    increase_cnv_map(genome_list, copy_number_array, lowest.get(), hm_id, cnv_map, core_access_uni_map, "access");

                    ArrayList<String> genomes = new ArrayList<>();
                    for (int genome_nr : present_genomes) {
                        genomes.add(genome_list.get(genome_nr));
                    }
                    incr_hashmap(accessory_per_count, genomes.size(), hm_id + ",");
                    try_incr_SB_hashmap(accessory_combinations, genomes.toString(), hm_id + ",");
                }

                // print progress based on hmgroup_counter (only print the first ten and then per hundred) to logger
                if (hmgroup_counter <= 10 || hmgroup_counter % 100 == 0) {
                    Pantools.logger.info("Finished {} of {} hmgroups.", hmgroup_counter, hm_nodes.size());
                }
            } 

            if (hmgroup_counter == 0) {
                return;
            }
           
            if (!Mode.equals("0") && !PROTEOME) { // do not remove the function below
                //create_circos_output(hmgroups_per_class, sco_nodes);
            }
            
            tx.success(); // transaction successful, commit changes
        }

        Pantools.logger.info("Generating output files.");
        create_group_csv_overview(genome_list, hmgroup_counts, hmgroups_per_class, sco_nodes, phenotype_result_map); //writes classified_groups.csv

        create_skipped_info_file_for_alignments(); //created regardless of VARIATION
        create_gclass_sco(sco_nodes); //writes single_copy_orthologs.csv and mlsa_suggestions.txt
        write_all_hmgroups(genome_list, all_hmgroups); //writes all_homology_groups.csv
        find_significant_pheno_groups(pheno_xsquare_map); //writes phenotype_association.csv
        write_hmgroup_size_frequencies(genome_list, hmsize_frequency); //writes group_size_occurrences.txt
        create_accessory_combination_output(accessory_combinations); //writes accessory_combinations.csv
        write_glcass_pheno_groups(phenotype_result_map); //writes phenotype_shared_groups.csv
       
        create_gclass_cnv(genome_list, cnv_map); //writes cnv_core_accessory.txt
        create_shared_genes_output(genome_list, shared_genes_map); //writes input for gene_distance_tree.R
        create_gene_distance_rscript(genome_list); //writes gene_distance_tree.R
        create_gclass_accessory_groups(accessory_per_count); //writes accessory_groups.csv
        create_gclass_pheno_cnv(phenotype_cnv); //writes phenotype_cnv.txt
        create_gclass_pheno_disrupted(phenotype_disrupted); //writes phenotype_disrupted.txt
        create_gclass_pheno_overview(phenotype_result_map, pheno_disrupt_counter, phenotype_cnv); //writes gene_classification_phenotype_overview.txt

        int unique_groups = create_gclass_unique_groups(genome_list, unique_hm_per_genome); //writes unique_groups.csv
        int total_core_groups = create_gclass_core_groups(hmgroups_per_class); //writes core_groups.csv
        create_gclass_overview(genome_list, hmgroup_counter, sco_nodes.size(), core_access_uni_map, total_core_groups, unique_groups); //writes gene_classification_overview.txt //TODO: include variation

        // printing final output
        print_gclass_output_files();
    }
    
    /**
     * Update skip_array and skip_list based on the genomes that were skipped during 'group' 
     * @param pangenome_node 
     */
    public void update_skip_array_based_on_grouping(Node pangenome_node) {
        if (!pangenome_node.hasProperty("grouping_v" + grouping_version + "_skipped_genomes")) { // only present when genomes are skipped during 'group'
            return;
        }
        String additional = "";
        if (adj_total_genomes < total_genomes) {
            additional = " additional";
        }
        String skipped_genomes = (String) pangenome_node.getProperty("grouping_v" + grouping_version + "_skipped_genomes");
        String[] skipped_genomes_array = skipped_genomes.split(",");
        int additional_skipped = 0;
        for (String genome_nr_str : skipped_genomes_array) {
            int genome_nr = Integer.parseInt(genome_nr_str);
            if (!skip_array[genome_nr-1]) { // when the genome is not already skipped 
                skip_array[genome_nr-1] = true;
                skip_list.add(genome_nr);
                additional_skipped ++;
                adj_total_genomes--;
            }
        }
        if (additional_skipped > 0) {
            System.out.println("\r" + additional_skipped + additional + " genomes are skipped because these were exluded from the homology grouping");
        }
    }

    /**
     * Update skip_array and skip_list if genomes do not have an 'annotation node' 
     */
    public void update_skip_array_based_on_annotation() {
        int additional_skipped = 0;
        String additional = "";
        if (adj_total_genomes < total_genomes) {
            additional = " additional";
        }
       
        for (int i = 0; i < annotation_identifiers.size(); i++) {
            if (annotation_identifiers.get(i).endsWith("_0") && !skip_array[i]) {
                skip_array[i] = true;
                additional_skipped ++;
                adj_total_genomes--;
           }
        }
       
        if (additional_skipped > 0) {
            Pantools.logger.info("{} genomes are skipped because these don't have an annotation.", additional_skipped + additional);
        }
        
    }

    /**
     * Increase various maps by going over the 'copy_number' array of an homology group node
     * @param genome_list the list of genomes
     * @param copy_number_array an array of copy numbers for each genome
     * @param shared_genes_map
     * @param unique boolean to indicate if the homology group is unique for a genome
     * @param pheno_count_map
     * @param phenotype_min_max
     * @param lowest
     * @return boolean to indicate if the homology group is a single copy core group
     */
    public boolean read_cnv_array_to_count_genes(ArrayList<String> genome_list,
                                                 int[] copy_number_array,
                                                 HashMap<String, int[]> shared_genes_map,
                                                 boolean unique,
                                                 HashMap<String, Integer> pheno_count_map,
                                                 HashMap<String, int[]> phenotype_min_max,
                                                 AtomicInteger lowest) {
        
        boolean sco_group = true;// becomes false when any of the genome either misses the mRNA or has multiple copies 
        for (int i = 0; i < copy_number_array.length; ++i) { // walk over the cnv array in the homology node
            increase_shared_gene_map(genome_list, copy_number_array, i, shared_genes_map, unique);
            if (copy_number_array[i] != 1) { // group cannot be single copy ortholog
                sco_group = false;
            } 
            if (copy_number_array[i] == 0) { 
                continue;
            }
                   
            if (copy_number_array[i] < lowest.get()) { 
                lowest.set(copy_number_array[i]);
            }
            if (geno_pheno_map.isEmpty() || !geno_pheno_map.containsKey(i)) { 
                continue; // rest of the loop is not relevant when no phenotype is included
            }
            String current_pheno = geno_pheno_map.get(i);
            incr_hashmap(pheno_count_map, current_pheno, 1);
            int[] min_max = phenotype_min_max.get(current_pheno);
            if (copy_number_array[i] < min_max[0]) {
                min_max[0] = copy_number_array[i];
            }
            if (copy_number_array[i] > min_max[1]) {
                min_max[1] = copy_number_array[i];
            }
            phenotype_min_max.put(current_pheno, min_max);
        } 
        return sco_group;
    }
    
    /**
     * Retrieve 'homology_group' nodes. Stop if no groups are present.
     * @return 
     */
    public static ArrayList<Node> retrieve_homology_group_nodes() {
        ResourceIterator<Node> homology_groups = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
        ArrayList<Node> hm_nodes = new ArrayList<>();
        int counter = 0;
        while (homology_groups.hasNext()) { 
            counter ++;
            Node hm_node = homology_groups.next();
            hm_nodes.add(hm_node);
            if (counter % 10000 == 0) {
                System.out.print("\rGathering homology groups: " + counter);
            }
        } 
        if (hm_nodes.size() < 2) {
            check_if_there_are_inactive_homology_groups();
            System.exit(1);
        } 
        System.out.print("\r                                    "); // spaces are intentional
        return hm_nodes;
    }
            
    /**
     * Create 3 input file for gene_distance_tree.R and a file with 6 matrices with the counts
     * @param genome_list the list of genomes
     * @param shared_genes_map
     */
    private void create_shared_genes_output(ArrayList<String> genome_list,
                                            HashMap<String, int[]> shared_genes_map) {
        String output_path = WD_full_path + "gene_classification/";

        StringBuilder header = new StringBuilder("Genomes,");
        StringBuilder all_genes = new StringBuilder();
        StringBuilder distinct_genes = new StringBuilder();
        StringBuilder inf_distinct_genes = new StringBuilder();
        StringBuilder gene_count_all = new StringBuilder();
        StringBuilder gene_count_distinct = new StringBuilder();
        StringBuilder gene_count_inf_distinct = new StringBuilder();
        StringBuilder missing_gene_count_all = new StringBuilder();
        StringBuilder missing_gene_count_distinct = new StringBuilder();
        StringBuilder missing_gene_count_inf_distinct = new StringBuilder();
        for (String genome1 : genome_list) {
            String phenotype = get_phenotype_for_genome(genome1, true);
            header.append(genome1).append(phenotype).append(",");
            all_genes.append(genome1).append(phenotype).append(",");
            distinct_genes.append(genome1).append(phenotype).append(",");
            inf_distinct_genes.append(genome1).append(phenotype).append(",");
            gene_count_all.append(genome1).append(phenotype).append(",");
            gene_count_distinct.append(genome1).append(phenotype).append(",");
            gene_count_inf_distinct.append(genome1).append(phenotype).append(",");
            missing_gene_count_all.append(genome1).append(phenotype).append(",");
            missing_gene_count_distinct.append(genome1).append(phenotype).append(",");
            missing_gene_count_inf_distinct.append(genome1).append(phenotype).append(",");
            for (String genome2 : genome_list) {
                String key = genome1 + "_" + genome2;
                if (genome_list.indexOf(genome2) < genome_list.indexOf(genome1)) {
                    key = genome2 + "_" + genome1;
                }
                int[] shared = shared_genes_map.get(key); // has 6 values [total all, total distinct,
                    // shared all, shared distinct, total informative distinct, shared informative distinct]
                double val = divide(shared[2], shared[0]);
                double val2 = divide(shared[3], shared[1]);
                double val3 = divide(shared[5], shared[4]);
                if (genome1.equals(genome2)) { // is required when a genome does not have genes
                   val = 1;
                   val2 = 1;
                   val3 = 1;
                }
                all_genes.append((1-val)).append(",");
                distinct_genes.append((1-val2)).append(",");
                inf_distinct_genes.append((1-val3)).append(",");
                gene_count_all.append(shared[2]).append(",");
                gene_count_distinct.append(shared[3]).append(",");
                gene_count_inf_distinct.append(shared[5]).append(",");
                
                missing_gene_count_all.append(shared[0] - shared[2]).append(",");
                missing_gene_count_distinct.append(shared[1] - shared[3]).append(",");
                missing_gene_count_inf_distinct.append(shared[4] - shared[5]).append(",");
            }
            all_genes.append("\n");
            distinct_genes.append("\n");
            inf_distinct_genes.append("\n");
            gene_count_all.append("\n");
            gene_count_distinct.append("\n");
            gene_count_inf_distinct.append("\n");
            missing_gene_count_all.append("\n");
            missing_gene_count_distinct.append("\n");
            missing_gene_count_inf_distinct.append("\n");
        }
        header.append("\n");
        write_string_to_file_full_path(header.toString() + all_genes, output_path + "distance_all_genes.csv");
        write_string_to_file_full_path(header.toString() + distinct_genes, output_path + "distance_distinct_genes.csv");
        write_string_to_file_full_path(header.toString() + inf_distinct_genes, output_path + "distance_inf_distinct_genes.csv");
        write_string_to_file_full_path("#This file contains six matrices: shared distinct genes (1), unshared distinct genes (2), shared informative distinct genes (3), "
                + "unshared informative distinct genes (4), total shared genes (5) and total unshared genes (6).\n" 
                + "\n#shared distinct genes\n" + header + gene_count_distinct
                + "\n#unshared distinct genes\n" + header + missing_gene_count_distinct
                + "\n#shared informative distinct genes\n" + header + gene_count_inf_distinct
                + "\n#unshared informative distinct genes\n" + header + missing_gene_count_inf_distinct
                + "\n#shared (all) genes\n" + header + gene_count_all
                + "\n#unshared (all) genes\n" + header + missing_gene_count_all, output_path + "shared_unshared_gene_count.csv");
    }
   
   /**
    * Creates all_homology_groups.csv, file with node identifiers of all homology groups for the selected genomes.
    * @param all_hmgroups 
    */
    public static void write_all_hmgroups(ArrayList<String> genome_list, StringBuilder all_hmgroups) {
        String hmgroups = all_hmgroups.toString().replaceFirst(".$","");
        String[] hmgroups_array = hmgroups.split(",");
        write_string_to_file_in_DB("# All " + hmgroups_array.length + " homology groups of " + genome_list.size() + " genomes\n" + hmgroups,
                "gene_classification/all_homology_groups.csv");
    }
    
   /**
    * Create phenotype_association.csv. The results from the fisher exact tests
    * @param pheno_xsquare_map 
   */
    public static void find_significant_pheno_groups(HashMap<Double, ArrayList<String>> pheno_xsquare_map) {
        if (PHENOTYPE == null) {
            return;
        }
        DecimalFormat formatter = new DecimalFormat("0.0000000000"); // 10 decimals
        TreeSet<Double> p_values = new TreeSet<>(pheno_xsquare_map.keySet());
        
        int total_tests = 0;
        for (double p_value : p_values) { // get the total number of tests
            ArrayList<String> values = pheno_xsquare_map.get(p_value);
            for (int i=0; i < values.size(); i++) { 
                if (p_value < 9000) {
                    total_tests ++;
                }
            }
        }
       
        double p = 0.01;
        double one_rank = (1.0 / total_tests) *p;
        double bonf_p = p / total_tests;
        StringBuilder output_builder = new StringBuilder("#Fisher's exact test determines whether the two groups differ in the proportion"
                + "\n#When more than two values are available for the selected phenotype these are converted two groups. "
                + "One phenotype value is then tested against all others"
                + "\n#Please consider that there must an equal population structure for the Fisher's exact test results to be meaningful."
                + "\n#Present phenotypes:\n");
        for (String key : phenotype_threshold_map.keySet()) {   
            int value = phenotype_threshold_map.get(key);
            if (key.equals("?") || key.equals("Unknown")) {
                output_builder.append("# ").append(key).append(": ").append(value).append(" members. These were ignored for the tests\n");
            } else {
                output_builder.append("# ").append(key).append(": ").append(value).append(" members\n");
            }
        }

        output_builder.append("\n#total groups with test: ").append(total_tests);
        if (total_tests > 0) {
            output_builder.append("\n#total ranks: ").append((p_values.size()-1))
                .append("\n#For Bonferoni (P < ").append(p).append("), p must be below: ").append((p / total_tests)) 
                .append("\n#For Benjamini-Hochberg (P < ").append(p).append("), p must be below: ").append(one_rank)
                        .append(" for the first rank. This number is added to each rank")
                .append("\n\nRank,Fisher-exact P value")
                        .append(",Homology group id,Phenotype,Phenotype members present, Phenotype members absent,")
                        .append("Other phenotype members present,Other phenotype members absent,Odds ratio,")
                        .append("Significant Benjamini-Hochberg testing correction,Significant Bonferroni correction\n");
        } else {
            output_builder.append(". Tests are only performed on groups with an uneven proportion of phenotype members\n");
        }
       
        int last_correct_rank = find_last_correct_rank(p_values, one_rank);
        int rank = 0;
        boolean first_no_test = true;
        for (double p_value : p_values) {
            rank ++;
            ArrayList<String> values = pheno_xsquare_map.get(p_value);
            String benjamini = ",false,", bonf = "false";
            if (rank <= last_correct_rank) {
                 benjamini = ",true,";
            }
            if (p_value < bonf_p) {
                bonf = "true";
            }
            
            for (String value : values) {
                if (p_value < 9000) {
                    output_builder.append(rank).append(",").append(formatter.format(p_value)).append(",").append(value)
                            .append(benjamini).append(bonf).append("\n" );
                } else {
                    if (first_no_test) {
                        first_no_test = false;
                        output_builder.append("\n#No test\n");
                    }
                   output_builder.append(",No test,").append(value).append(",No test,No test\n");
                }
            }
        }
        write_SB_to_file_in_DB(output_builder, "gene_classification/phenotype_association.csv");
    }
    
    /**
     * Benjamini hochberg multiple testing. find the highest rank where the value that is still significant.
     * @param pvalues
     * @param benjamini_rank_p
     * @return 
     */
    public static int find_last_correct_rank(TreeSet<Double> pvalues, Double benjamini_rank_p) {
        int rank = 0, last_correct_rank = 0;
        for (double p_value : pvalues) {
            rank ++;
            double required_p = benjamini_rank_p*rank;
            if (p_value < required_p) {
                last_correct_rank = rank;
            }
        }
        return last_correct_rank;
    }
   
    /**
     * Creates classified_groups.csv, overview of the groups with the class (core,accessory, unique) and phenotype class (if applicable).
     * @param genome_list a list of all genomes
     * @param hmgroup_counts a map of all hmgroups and the copy number array for each genome (ordered by genome_list)
     * @param hmgroups_per_class a map of all hmgroups per class (core, accessory, unique)
     * @param sco_nodes a list of all sco nodes
     * @param phenotype_result_map
     */
    public static void create_group_csv_overview(ArrayList<String> genome_list,
                                                 HashMap<String, int[]> hmgroup_counts,
                                                 HashMap<String, ArrayList<Node>> hmgroups_per_class,
                                                 ArrayList<Node> sco_nodes,
                                                 HashMap<String, StringBuilder> phenotype_result_map) {
        
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "gene_classification/classified_groups.csv"))) {
            // write header
            StringBuilder header = new StringBuilder("Homology group id,Class");
            if (PHENOTYPE != null) {
                header.append(",phenotype class,phenotypes");
            }
            for (String genome : genome_list) {
                header.append(",Genome ").append(genome);
            }
            out.write(header + "\n");

            HashMap<String, ArrayList<String>> pheno_specific_map = transform_hashmap_AL_from_SB(phenotype_result_map, "#specific");
            HashMap<String, ArrayList<String>> pheno_shared_map = transform_hashmap_AL_from_SB(phenotype_result_map , "#shared");
            HashMap<String, ArrayList<String>> pheno_exclusive_map = transform_hashmap_AL_from_SB(phenotype_result_map , "#exclusive");

            // write the different classes (core, accessory, unique)
            String[] classTypeArray = {"core", "accessory", "unique"};
            for (String classType : classTypeArray) {
                ArrayList<Node> nodes = hmgroups_per_class.get(classType);
                if (nodes == null) {
                    continue;
                }

                // loop over all nodes belonging to this class type
                for (Node hm_node : nodes) {
                    long hm_id = hm_node.getId();

                    // do the phenotype check
                    String add = check_if_group_is_pheno_shared_specific_exclusive(String.valueOf(hm_id), pheno_specific_map, pheno_shared_map, pheno_exclusive_map);

                    // write the group id and class
                    if (classType.equals("core") && sco_nodes.contains(hm_node)) {
                        out.write(hm_id + ",core & single copy orthologous" + add + ",");
                    } else {
                        out.write(hm_id + "," + classType + add + ",");
                    }

                    // add the copy number for each genome
                    int[] copy_numbers = hmgroup_counts.get(String.valueOf(hm_id));
                    String copy_nr_str = Arrays.toString(copy_numbers).replace("[", "").replace(" ","").replace("]","");
                    out.write(copy_nr_str + "\n");
                } 
            }
        } catch (IOException e) {
            Pantools.logger.warn("Unable to write to: {}gene_classification/classified_groups.csv", WORKING_DIRECTORY);
        }
    }
    
    /**
     * 
     * @param hm_id_str
     * @param pheno_specific_map
     * @param pheno_shared_map
     * @param pheno_exclusive_map
     * @return 
     */
    public static String check_if_group_is_pheno_shared_specific_exclusive(String hm_id_str, HashMap<String, ArrayList<String>> pheno_specific_map,
            HashMap<String, ArrayList<String>> pheno_shared_map, HashMap<String, ArrayList<String>> pheno_exclusive_map) { 
        String add = "";
        if (PHENOTYPE == null) {
            return add;
        }
        for (String phenotype : pheno_specific_map.keySet()) { 
            ArrayList<String> pheno_specific_groups = pheno_specific_map.get(phenotype);
            if (pheno_specific_groups.contains(hm_id_str)) {
                add = ",phenotype specific," + phenotype.replace("#specific","");
                break;
            }
        }
        if (add.length() > 5) { // done when group is phenotype SPECIFIC
            return add;
        }
        
        for (String phenotype : pheno_exclusive_map.keySet()) { 
            ArrayList<String> pheno_exclusive_groups = pheno_exclusive_map.get(phenotype);
            String[] pheno_array = phenotype.split("#");
            if (pheno_exclusive_groups.contains(hm_id_str)) {
                add = ",phenotype exclusive," + pheno_array[0];
                break;
            }
        }
        if (add.length() > 5) { // done when group is phenotype EXCLUSIVE
            return add;
        }
        
        for (String phenotype : pheno_shared_map.keySet()) { 
            ArrayList<String> pheno_shared_groups = pheno_shared_map.get(phenotype);
            phenotype = phenotype.replace("#shared","");
            if (pheno_shared_groups.contains(hm_id_str)) {
                if (add.length() < 5) { // only the first time 
                    add += ",phenotype shared," + phenotype + "";
                } else {
                    add += " " + phenotype ;
                }
            }
        }
        if (add.length() < 5) { // there were NO phenotype shared/specific/exclusive groups
            add += ",,";
        }
        return add;
    }
    
    /**
     * Arraylist (AL) from StringBuilder (SB)
     * 
     * @param phenotype_result_map
     * @param additional_key
     * @return 
     */
    public static HashMap<String, ArrayList<String>> transform_hashmap_AL_from_SB(HashMap<String, StringBuilder> phenotype_result_map,
            String additional_key) {
        
        HashMap<String, ArrayList<String>> new_hashmap = new HashMap<>();
        for (String key : phenotype_result_map.keySet()) { 
            if (!key.contains(additional_key)) {
                continue;
            }
            String[] value_array = phenotype_result_map.get(key).toString().split(",");
            ArrayList<String> value_list = new ArrayList<>();
            for (String value : value_array) {
                value_list.add(value);
            }
            new_hashmap.put(key, value_list);
        }
        return new_hashmap;
    }
    
    /**
     * 
     */
    public static void print_gclass_output_files() {
        String dir = WORKING_DIRECTORY + "gene_classification/";

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}gene_classification_overview.txt", dir);
        Pantools.logger.info(" {}classified_groups.csv", dir);
        Pantools.logger.info(" {}group_size_occurrences.txt", dir);
        Pantools.logger.info(" {}cnv_core_accessory.txt", dir);
        Pantools.logger.info(" {}shared_unshared_gene_count.csv", dir);
        Pantools.logger.info(" {}gene_distance_tree.R (Select one of the three distances)", dir);
                 
        if (Mode.equals("1") && !PROTEOME) {
            //Pantools.logger.info("{}circos/\n ", dir);
        }
        if (!PROTEOME && Mode.contains("MLSA")) {
            Pantools.logger.info("{}mlsa_suggestions.txt", dir);
        }
        if (PHENOTYPE != null) { 
            Pantools.logger.info(" {}gene_classification_phenotype_overview.txt");
            Pantools.logger.info(" {}phenotype_disrupted.txt");
            Pantools.logger.info(" {}phenotype_cnv.txt");
            Pantools.logger.info(" {}phenotype_association.csv");
        }
         
        Pantools.logger.info("Files with 'homology_group' node identifiers:");
        Pantools.logger.info(" {}all_homology_groups.csv", dir);
        Pantools.logger.info(" {}core_groups.csv", dir);
        Pantools.logger.info(" {}single_copy_orthologs.csv", dir);
        Pantools.logger.info(" {}accessory_groups.csv", dir);
        Pantools.logger.info(" {}accessory_combinations.csv", dir);
        Pantools.logger.info(" {}unique_groups.csv", dir);
        if (PHENOTYPE != null) {
            Pantools.logger.info(" {}phenotype_exclusive_groups.csv", dir);
            Pantools.logger.info(" {}phenotype_shared_groups.csv", dir);
            Pantools.logger.info(" {}phenotype_specific_groups.csv", dir);
        }
    }
    
    /**
     * Check which genome has at least one gene
     * @param copy_number_array an array with the copy number of each genome, ordered by the genome order in the genome list
     * @param genome_list the list of genomes
     * @return an array with the positions in copy_number_array that have at least one gene
     */
    public static ArrayList<Integer> check_array_if_unique(int[] copy_number_array, ArrayList<String> genome_list) {
        ArrayList<Integer> present_genomes = new ArrayList<>();
        for (int i = 0; i < copy_number_array.length; ++i) { // skip first position because it is always zero...
            if (copy_number_array[i] > 0) {
                present_genomes.add(i);
            }
        }
        return present_genomes;
    }
    
    /**
     * part of gclassification
     * @param genome_list the list of genomes
     * @param fields
     * @param i
     * @param shared_genes_map
     * @param unique 
     */
    public void increase_shared_gene_map(ArrayList<String> genome_list, int[] fields, int i, HashMap<String, int[]> shared_genes_map, boolean unique) {
        for (int j = 0; j < fields.length; j ++) {
            if (i > j) {
                continue;
            }
            if (fields[i] == 0 && fields[j] == 0) {
                continue;
            }
            
            String key = genome_list.get(i) + "_" + genome_list.get(j);
            int[] shared_genes = shared_genes_map.get(key); //total all, total distinct, shared all, shared distinct, distinct informative all, distinct informative distinct
            shared_genes[1] += 1; // total distinct 
            if (!unique) {
                shared_genes[4] += 1;
            }
            if (fields[i] >= fields[j]) {
                shared_genes[0] += fields[i]; // total all 
                if (fields[j] != 0) {
                    shared_genes[2] += fields[j];
                    shared_genes[3] += 1;
                    if (!unique) {
                        shared_genes[5] += 1;
                    }
                } 
            } else { // fields[i] < fields[j]
                shared_genes[0] += fields[j];
                if (fields[i] != 0) { 
                    shared_genes[2] += fields[i];
                    shared_genes[3] += 1;
                    if (!unique) {
                        shared_genes[5] += 1;
                    }
                }
            } 
            shared_genes_map.put(key, shared_genes);
        }
    } 
    
    /**
     * Create gene_classification_phenotype_overview.txt. 
     * Overview of the number of TOTAL, EXCLUSIVE, SHARED and SPECIFIC groups per phenotype
     *
     * @param phenotype_result_map
     * @param pheno_disrupt_counter
     * @param phenotype_cnv 
     */
    public static void create_gclass_pheno_overview(HashMap<String, StringBuilder> phenotype_result_map, HashMap<String, Integer> pheno_disrupt_counter, 
            HashMap<String, StringBuilder> phenotype_cnv) {
        if (PHENOTYPE == null) {
            return;
        } 
        StringBuilder output_builder = new StringBuilder();
        int total_with_pheno = 0;
        for (String phenotype : phenotype_map.keySet()) {
            if (phenotype.equals("?") || !phenotype_result_map.containsKey(phenotype + "#total")) {
                Pantools.logger.info(phenotype);
                continue;
            }
           
            int total_shared = 0;
            if (phenotype_result_map.containsKey(phenotype + "#shared")) {
                total_shared = phenotype_result_map.get(phenotype + "#shared").toString().split(",").length;
            }
           
            int total_groups = phenotype_result_map.get(phenotype + "#total").toString().split(",").length;
            int pheno_threshold = phenotype_threshold_map.get(phenotype);
            int[] pheno_genomes = phenotype_map.get(phenotype);
            total_with_pheno += pheno_genomes.length;
            if (pheno_threshold == 0) {
                continue;
            }
            int total_exclusive = 0;
            if (phenotype_result_map.containsKey(phenotype + "#exclusive")) {
                total_exclusive = phenotype_result_map.get(phenotype + "#exclusive").toString().split(",").length;
            }
            
            String percentage_ex = get_percentage_str(total_exclusive, total_groups, 2); 
            String percentage_sh = get_percentage_str(total_shared, total_groups, 2); 
            String include_threshold = "";
            if (pheno_threshold < pheno_genomes.length) {
                include_threshold = ", used threshold " + pheno_threshold;
            }
            output_builder.append("Phenotype ").append(phenotype).append(", ").append(pheno_genomes.length).append(" members").append(include_threshold)
                    .append("\nTotal: ").append(total_groups)
                    .append("\nExclusive: ").append(total_exclusive).append(", ").append(percentage_ex).append("%")
                    .append("\nShared: ").append(total_shared).append(", ").append(percentage_sh).append("%")
                    .append("\nSpecific: ");
            
            if (!phenotype_result_map.containsKey(phenotype + "#specific")) {
                output_builder.append(0);
            } else {
                int total_specific = phenotype_result_map.get(phenotype + "#specific").toString().split(",").length;
                String percentage_sp = get_percentage_str(total_specific, total_groups, 2); 
                output_builder.append(total_specific).append(", ").append(percentage_sp).append("%");
            }
            output_builder.append("\nShared groups that are not specific as these were disrupted by:\n");
            for (String pheno_disrupt_key : pheno_disrupt_counter.keySet()) {
                if (!pheno_disrupt_key.startsWith(phenotype + "_")) {
                    continue;
                }
                int disrupt = pheno_disrupt_counter.get(pheno_disrupt_key);
                output_builder.append(" ").append(pheno_disrupt_key.replace(phenotype + "_", "")).append(": ").append(disrupt).append("\n");
            }
            
            String[] class_array = {"core", "accessory"};
            output_builder.append("Extra copy/copies in groups compared to other phenotypes: \n");
            for (String class_str: class_array) {
                String class_str_upper = class_str.substring(0,1).toUpperCase() + class_str.substring(1).toLowerCase(); // first letter to uppercase
                String val_str = "";
                if (phenotype_cnv.containsKey(phenotype + "#" + class_str)) {
                    val_str = phenotype_cnv.get(phenotype + "#" + class_str).toString();
                }
                String[] val_array = val_str.split("\n\n"); 
                if (val_array[0].equals("")) {
                    output_builder.append(" ").append(class_str_upper).append(" groups: 0\n");
                } else {
                    output_builder.append(" ").append(class_str_upper).append(" groups: ").append(val_array.length).append("\n");
                }
            }
            output_builder.append("\n");
        }
        write_string_to_file_in_DB("Total genomes with a phenotype: " + total_with_pheno + "\n\n" + output_builder.toString(), 
                "gene_classification/gene_classification_phenotype_overview.txt");
    }
    
    /**
     * 
     * @param shared_snps_map
     * @param output_path
     * @param total_sites
     * @param informative
     * @param alignmentTypeShort
     * @param genome_order_file 
     */
    public static void create_shared_site_matrices(HashMap<String, Integer> shared_snps_map, Path output_path, int total_sites, String informative,
                                                   String alignmentTypeShort, Path genome_order_file) {
        
        if (total_sites == 0) {
            return;
        }
        DecimalFormat fmt = new DecimalFormat("0.0000");
        StringBuilder shared_pos_builder = new StringBuilder();
        StringBuilder distance_builder = new StringBuilder();
        String variable_or_inf = "variable";
        if (informative.equals("#inf")) {
            variable_or_inf = "informative";
        }
      
        StringBuilder header_builder = new StringBuilder("Sequences,");
        ArrayList<String> genome_order_list = read_genome_order_file(genome_order_file, false);
        ArrayList<String> protein_id_list = get_full_protein_identifiers(genome_order_file, false);
        for (int i=0; i < protein_id_list.size(); i++) {
            String genome_nr1 = genome_order_list.get(i);
            try {
                if (skip_array[Integer.parseInt(genome_nr1) - 1]) {
                    continue;
                }
            } catch (NumberFormatException ignored) {
            }

            String seq_name = protein_id_list.get(i);
            header_builder.append(seq_name);
            shared_pos_builder.append(seq_name).append(",");
            distance_builder.append(seq_name).append(",");
            if (i != protein_id_list.size()-1) {
                header_builder.append(",");
            }
            for (int j=0; j < protein_id_list.size(); j++) {
                String genome_nr2 = genome_order_list.get(j);
                try {
                    if (skip_array[Integer.parseInt(genome_nr2) - 1]) {
                        continue;
                    }
                } catch (NumberFormatException ignored) {
                }

                int shared_sites = 0;
                if (i == j) { 
                    shared_sites = total_sites;
                } else if (i < j && shared_snps_map.containsKey(i + "#" + j + informative)) { 
                    shared_sites = shared_snps_map.get(i + "#" + j + informative);
                } else if (shared_snps_map.containsKey(j + "#" + i + informative)) { // i > j
                    shared_sites = shared_snps_map.get(j + "#" + i + informative);
                } 
                
                double distance = 1 - divide(shared_sites, total_sites);
                shared_pos_builder.append(shared_sites);
                distance_builder.append(fmt.format(distance));
                if (j != protein_id_list.size()-1) {
                    distance_builder.append(",");
                    shared_pos_builder.append(",");
                }   
            }
            shared_pos_builder.append("\n");
            distance_builder.append("\n");
        }
        
        write_string_to_file_full_path(header_builder + "\n" + shared_pos_builder,
                output_path.resolve(variable_or_inf + "_" + alignmentTypeShort + "_site_counts.csv"));
        write_string_to_file_full_path(header_builder + "\n" + distance_builder,
                output_path.resolve(variable_or_inf + "_" + alignmentTypeShort + "_distance.csv"));
    }
    
    /**
     * Calculate the sequence similariry based on shared similar positions and the total alignment length (- alignment correction).
     * @param shared_snps_map
     * @param output_file
     * @param total_sites
     * @param shared_pos
     * @param genome_order_file 
     */
    public static void create_similarity_matrices(HashMap<String, Integer> shared_snps_map, Path output_file, long total_sites,
            long shared_pos, Path genome_order_file) {
        
        ArrayList<String> protein_id_list = get_full_protein_identifiers(genome_order_file, false);
        ArrayList<String> genome_order_list = read_genome_order_file(genome_order_file, false);
        HashMap<String, String> percentage_map = new HashMap<>();
        StringBuilder output_builder = new StringBuilder();
        StringBuilder header_builder = new StringBuilder("Sequences,");
        long alignment_length = total_sites + shared_pos;
        for (int i=0; i < protein_id_list.size(); i++) {
            String seq_name = protein_id_list.get(i);
            String genome_nr1 = genome_order_list.get(i);
            try {
                if (skip_array[Integer.parseInt(genome_nr1) - 1]) {
                    continue;
                }
            } catch (NumberFormatException ignored) {
            }
            output_builder.append(seq_name).append(",");
            header_builder.append(seq_name);
            if (i != protein_id_list.size()-1) {
                header_builder.append(",");
            }
            for (int j=0; j < protein_id_list.size(); j++) {
                String genome_nr2 = genome_order_list.get(j);
                try {
                    if (skip_array[Integer.parseInt(genome_nr2) - 1]) {
                        continue;
                    }
                } catch (NumberFormatException ignored) {
                }
                String similarity = "100"; // when i = j
                if (i != j) {
                    if (percentage_map.containsKey(i + "#" + j)) {
                        similarity = percentage_map.get(i + "#" + j);  
                    } else {
                        long similar_snps = 0;
                        if (shared_snps_map.containsKey(i + "#" + j + "#similar")) {
                            similar_snps = shared_snps_map.get(i + "#" + j + "#similar");
                        } 
                        long alignment_correction = retrieve_alignment_correction(shared_snps_map, i, j);
                        similarity = get_percentage_str(similar_snps + shared_pos, alignment_length-alignment_correction, 2); 
                    }
                }
                output_builder.append(similarity);
                if (j != protein_id_list.size()-1) {
                    output_builder.append(",");
                }
                percentage_map.put(j + "#" + i, similarity);
            }
            output_builder.append("\n");
        }
        write_string_to_file_full_path(header_builder + "\n" + output_builder, output_file);
    }
    
    /**
     * When a gap position is shared by two genomes it must be deducted from alignment length
     * @param shared_snps_map
     * @param i
     * @param j
     * @return 
     */
    public static long retrieve_alignment_correction(HashMap<String, Integer> shared_snps_map, int i, int j) {
        long shared_snps = 0;
        if (shared_snps_map.containsKey(i + "#" + j + "#nogap")) {
            shared_snps = shared_snps_map.get(i +"#" + j + "#nogap");
        }
        long shared_positions = 0; 
        if (shared_snps_map.containsKey(i + "#" + j)) { //includes shared gap positions
            shared_positions = shared_snps_map.get(i + "#" + j);
        }
        return shared_positions-shared_snps; // remove the number of shared gaps between two sequences from the alignment length
    }
    
    /**
     *
     * @param genome_list
     * @param copy_number_array
     * @param lowest
     * @param hm_id
     * @param cnv_map
     * @param core_access_uni_map
     * @param class_str 
     */
    public static void increase_cnv_map(ArrayList<String> genome_list,
                                        int[] copy_number_array,
                                        int lowest,
                                        long hm_id,
                                        HashMap<String, StringBuilder> cnv_map,
                                        HashMap<String, int[]> core_access_uni_map,
                                        String class_str) {
        
        for (int i = 0; i < copy_number_array.length; ++i) { // walk over the cnv array of an homology node
            String genomeName = genome_list.get(i);

            if (copy_number_array[i] == 0) {
                continue;
            }
            if (copy_number_array[i] > lowest) { // genome has an extra copy of a gene
                try_incr_SB_hashmap(cnv_map, genomeName + class_str, " " + hm_id + ", " + (copy_number_array[i] - lowest) +"\n");
            }
            if (class_str.equals("core")) {
                incr_array_hashmap(core_access_uni_map, genomeName + "_total", copy_number_array[i], 0); // positon 0 of array
                incr_array_hashmap(core_access_uni_map, genomeName + "_distinct", 1, 0); // positon 0 of array
            } else { // matches "access" 
                incr_array_hashmap(core_access_uni_map, genomeName + "_total", copy_number_array[i], 1); // positon 1 of array
                incr_array_hashmap(core_access_uni_map, genomeName + "_distinct", 1, 1); // positon 1 of array
            }
        }
    }
    
    /**
     * Allows some functions to give a warning or stop running when not all genomes were included in determining single copy orthologs
     */
    public static void create_skipped_info_file_for_alignments() {
        String skipped_genomes = "";
        for (int skipped_genome : skip_list) {
            skipped_genomes += skipped_genome + ",";
        }
        if (skipped_genomes.equals("")) {
            skipped_genomes = "None were skipped ";
        } 
        write_string_to_file_in_DB(skipped_genomes.replaceFirst(".$",""), "gene_classification/skipped_genomes.info"); 
    }
    
    /**
     * Create single_copy_orthologs.csv and mlsa_suggestions.txt (with --mlsa)
     * Go over the homology groups to check if all genes can be found by using the gene name
     * NB: --mlsa cannot be used with variants, so we do not have to worry about those
     *
     * @param sco_nodes the lest of homology nodes that are single copy orthologs
     */
    private void create_gclass_sco(ArrayList<Node> sco_nodes) {
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            StringBuilder sco_builder = new StringBuilder();
            StringBuilder mlsa_builder = new StringBuilder("# Suitable genes for 'MLSA_find_genes'\n");
            StringBuilder mlsa_builder2 = new StringBuilder("\n# Possible gene names when including --mode extensive\n");
            if (sco_nodes.isEmpty()) {
                sco_builder.append("No single copy orthologs were found");
            }

            for (Node hm_node : sco_nodes) {
                long hm_id = hm_node.getId();
                sco_builder.append(hm_id).append(",");
                if (PROTEOME || !Mode.contains("MLSA")) { // --mode mlsa must be selected 
                    continue;
                }
                Iterable<Relationship> relationships = hm_node.getRelationships(has_homolog);
                HashSet<String> values_set = new HashSet<>();
                HashSet<String> values_set2 = new HashSet<>();
                for (Relationship rel : relationships) {
                    if (values_set.size() > 1 && values_set2.size() > 1 || values_set.contains("unknown")) { // when higher than 1, no longer a suitable group
                        break;
                    }
                    Node mrna_node = rel.getEndNode();
                    String[] mrnaNameArray = retrieveNamePropertyAsArray(mrna_node);
                    String mrna_name = mrnaNameArray[0];
                    if (mrna_name.equals("")) {
                        mrna_name = "unknown";
                    }
                    values_set.add(mrna_name);
                    String[] mrna_array = mrna_name.split("_");
                    if (!mrna_array[0].equals(mrna_name)) {
                        values_set2.add(mrna_array[0]);
                    }
                }
                if (values_set.contains("unknown")) {
                    continue;
                }
                if (values_set.size() == 1) {
                    mlsa_builder.append(values_set.iterator().next()).append("\n");
                } 
                if (values_set2.size() == 1) { // it is the same gene but has additional copies
                    mlsa_builder2.append(values_set2.iterator().next()).append(": ");
                    for (String name : values_set) {
                        mlsa_builder2.append(name).append(", ");
                    }
                    mlsa_builder2.append("\n"); 
                }
            }
            write_SB_to_file_in_DB(sco_builder, "gene_classification/single_copy_orthologs.csv"); 
            
            if (!PROTEOME && Mode.contains("MLSA")) {
                write_string_to_file_in_DB(mlsa_builder + mlsa_builder2.toString(), "gene_classification/mlsa_suggestions.txt");
            }
            System.out.print("\r                                                       "); // spaces are intentional
            tx.success(); // transaction successful, commit changes
        }
    }
    
    /**
     * Creates phenotype_disrupted.txt
     * @param phenotype_disrupted 
     */
    public static void create_gclass_pheno_disrupted(HashMap<Node, StringBuilder> phenotype_disrupted) {
        if (PHENOTYPE == null) {
            return;
        }
        StringBuilder output = new StringBuilder("#Why an homology group is not specific to a phenotype\n");
        for (Node group_node : phenotype_disrupted.keySet()) {
            String value_str = phenotype_disrupted.get(group_node).toString();
            output.append("\n").append(group_node.getId()).append("\n").append(value_str);
        }
        write_SB_to_file_in_DB(output, "gene_classification/phenotype_disrupted.txt");
    }

    /**
     * With the new gff parser introduced in v4.0.0 the "name" property now holds a String[] value instead of a String
     * By using this function the code still works on older versions
     *
     * String[] value is converted to string. Example [genA,genB] becomes genA/genB
     *
     * @param node can either be gene or mRNA node
     */
    public static String retrieveNamePropertyAsString(Node node) {
        String name;
        try {
            name = (String) node.getProperty("name");
        } catch (ClassCastException cce) { // value is not a String
            String[] nameArray = (String[]) node.getProperty("name");
            name = Arrays.toString(nameArray).replace("[", "").replace("]", "").replace(",","/");
        }
        return name;
    }

    /**
     * With the new gff parser introduced in v4.0.0 the "name" property now holds a String[] value instead of a String
     * By using this function the code still works on older versions
     *
     * String value is placed in String[]. Example genA becomes [genA]
     *
     * @param node can either be gene or mRNA node
     */
    public static String[] retrieveNamePropertyAsArray(Node node) {
        String[] nameArray;
        try {
            String name = (String) node.getProperty("name");
            nameArray = name.split(",");
        } catch (ClassCastException cce) { // value is not a String
           nameArray = (String[]) node.getProperty("name");
        }
        return nameArray;
    }

    /**
     * 
     * @param phenotype_cnv 
     */
    public static void create_gclass_pheno_cnv(HashMap<String, StringBuilder> phenotype_cnv) {
        if (PHENOTYPE == null) {
            return;
        }
        StringBuilder output_builder = new StringBuilder("#Genomes of a phenotype with additional copies compared "
                + "to other phenotypes in core and acccessory homology groups.\n\n");
        ArrayList<String> to_skip_list = new ArrayList<>();
        for (String phenotype_key : phenotype_cnv.keySet()) { 
            String[] key_array = phenotype_key.split("#");
            String present_genomes = Arrays.toString(phenotype_map.get(key_array[0])).replace("[","").replace("]","");
            output_builder.append("#Phenotype: ").append(key_array[0]).append(", genomes ").append(present_genomes).append("\n");
            if (to_skip_list.contains(key_array[0])) {
                continue;
            }
            to_skip_list.add(key_array[0]);
            String[] class_array = {"core", "accessory"};
            for (String class_str : class_array) {
                String class_str_upper = class_str.substring(0,1).toUpperCase() + class_str.substring(1).toLowerCase(); // first letter to uppercase
                output_builder.append(class_str_upper).append(" groups\n");
                StringBuilder value = new StringBuilder();
                if (phenotype_cnv.containsKey(key_array[0] + "#" + class_str)) {
                    value = phenotype_cnv.get(key_array[0] + "#" + class_str);
                } 
                output_builder.append(value.toString()).append("\n");
            }
            output_builder.append("\n");
        }
        write_SB_to_file_in_DB(output_builder, "gene_classification/phenotype_cnv.txt");
    }
    
    /**
     * 
     * @param phenotype_min_max
     * @param phenotype_cnv
     * @param class_str
     * @param hm_node 
     */
    public static void determine_pheno_cnv(HashMap<String, int[]> phenotype_min_max, HashMap<String, StringBuilder> phenotype_cnv, String class_str, Node hm_node) {
        if (PHENOTYPE == null) {
            return;
        }
        for (String phenotype : phenotype_min_max.keySet()) {
            if (phenotype.endsWith("_class")) { 
                continue;
            }
            int[] class1 = phenotype_min_max.get(phenotype + "_class"); // what is in here?
            if (class1[0] == 0) {
                continue;
            }
            int[] value = phenotype_min_max.get(phenotype);
            if (value[0] == 9999 || value[0] == 1) {
                continue;
            }
            boolean first = true;
            for (String phenotype2 : phenotype_min_max.keySet()) {
                if (phenotype2.endsWith("_class")) {
                    continue;
                }
                int[] class2 = phenotype_min_max.get(phenotype + "_class");
                if (class2[0] == 0) {
                    continue;
                }
                int[] value2 = phenotype_min_max.get(phenotype2);
                if (value2[0] == 9999 || phenotype.equals(phenotype2) || value2[1] >= value[0]) {
                    continue;
                }
                int difference = value[0] - value2[1];
                int difference2 = value[0] - value2[0];
                
                if (first) {
                    try_incr_SB_hashmap(phenotype_cnv, phenotype + "#" + class_str, "\n" + hm_node.getId() + "\n");
                    first = false;
                }
                if (difference != difference2) {
                    try_incr_SB_hashmap(phenotype_cnv, phenotype + "#" + class_str, " " + difference + "-" + difference2 + ", " + phenotype2 + "\n");
                } else {
                    try_incr_SB_hashmap(phenotype_cnv, phenotype + "#" + class_str, " " + difference + ", " + phenotype2 + "\n");
                }
            }
        }
    }
    
    /**
     * Check if the number of genomes from a certain phenotype reach the threshold 
     * Performs a fisher exact test when there is an uneven distribution of phenotype members
     * @param phenotype_count_map
     * @param hm_group
     * @param phenotype_result_map
     * @param hmgroups_per_class
     * @param phenotype_disrupted
     * @param pheno_disrupt_counter
     * @param phenotype_min_max
     * @param pheno_xsquare_map 
     */
    public void determine_gclass_pheno_specific(HashMap<String, Integer> phenotype_count_map, Node hm_group, HashMap<String, StringBuilder> phenotype_result_map, 
            HashMap<String, ArrayList<Node>> hmgroups_per_class, HashMap<Node, StringBuilder> phenotype_disrupted, HashMap<String, Integer> pheno_disrupt_counter, 
            HashMap<String, int[]> phenotype_min_max, HashMap<Double, ArrayList<String>> pheno_xsquare_map) {
        
        DecimalFormat formatter = new DecimalFormat("0.00");
        long hm_id = hm_group.getId();
        for (String phenotype : phenotype_count_map.keySet()) { 
            int pheno_count = phenotype_count_map.get(phenotype);
            if ("?".equals(phenotype) || "Unknown".equals(phenotype) || pheno_count == 0) { 
                continue;
            }
            try_incr_SB_hashmap(phenotype_result_map, phenotype + "#total", hm_id +",");
            int pheno_threshold = phenotype_threshold_map.get(phenotype); 
            boolean enough = false, disrupted = false;
            int rest_current_pheno = pheno_threshold-pheno_count;
            if (pheno_count >= pheno_threshold) { 
                enough = true; // number of phenotype members meets the threshold
            }
            boolean first = true;
            int present_rest = 0, total_rest = 0;
            for (String other_phenotype : phenotype_threshold_map.keySet()) {
                int threshold_other_phenotype = phenotype_threshold_map.get(other_phenotype);
                if (other_phenotype.equals(phenotype) || "?".equals(other_phenotype) || "Unknown".equals(other_phenotype)) { 
                    continue;
                }  
                int count_other_pheno = phenotype_count_map.get(other_phenotype);
                total_rest += threshold_other_phenotype;
                if (count_other_pheno == 0) { 
                    continue;
                }
                
                present_rest += count_other_pheno;
                if (enough && !first) {
                    try_incr_SB_hashmap(phenotype_disrupted, hm_group, " " + other_phenotype + " (" + count_other_pheno + "/" + threshold_other_phenotype + ")\n");
                    try_incr_hashmap(pheno_disrupt_counter, phenotype + "_" + other_phenotype, 1);
                } else if (enough && first) {
                    first = false;
                    try_incr_SB_hashmap(phenotype_disrupted, hm_group, phenotype + " (" + pheno_count + "/" + pheno_threshold + ")" + " disrupted by:\n "
                           + other_phenotype + " (" + count_other_pheno + "/" + threshold_other_phenotype+ ")\n");
                    try_incr_hashmap(pheno_disrupt_counter, phenotype + "_" + other_phenotype, 1);
                }
                disrupted = true;
            }
            int not_present_rest = total_rest-present_rest;
            double[] ratios = calculate_odds_ratios((double) pheno_count, (double) rest_current_pheno, (double) present_rest, (double) not_present_rest); 
            String info = hm_id + "," + phenotype + "," + pheno_count + "," + rest_current_pheno + "," + present_rest +
                    "," + not_present_rest + "," + formatter.format(ratios[0]);
            if (ratios[1] < 1.35 || ratios[2] > 0.65) {
                try_incr_AL_hashmap(pheno_xsquare_map, 9999.0, info);
            } else {
                double pValue = fisher_exact(pheno_count, rest_current_pheno, present_rest, not_present_rest);
                try_incr_AL_hashmap(pheno_xsquare_map, pValue * 2, info); // multiply p-value by 2 to make it two sided
            }
            
            if (enough && !disrupted) { // group is phenotype specific 
                try_incr_SB_hashmap(phenotype_result_map, phenotype + "#specific", hm_id +",");
                try_incr_AL_hashmap(hmgroups_per_class, "pheno_SPECIFIC", hm_group); 
                int[] class_value = {2}; // 0 means not enough for shared or specific, 1 is shared, 2 is specific.
                phenotype_min_max.put(phenotype + "_class", class_value);
                Pantools.logger.trace("phenotype specific ({}).", phenotype);
            }
            if (!disrupted) { // group is phenotype exclusive
                try_incr_AL_hashmap(hmgroups_per_class, "pheno_EXCLUSIVE", hm_group); 
                try_incr_SB_hashmap(phenotype_result_map, phenotype + "#exclusive#" + pheno_count, hm_id + ",");
                try_incr_SB_hashmap(phenotype_result_map, phenotype + "#exclusive", hm_id + ",");
            }
            
            if (enough) {
                int[] class_value = {1}; // 0 means not enough for shared or specific, 1 is shared, 2 is specific.
                phenotype_min_max.put(phenotype + "_class", class_value);
                try_incr_SB_hashmap(phenotype_result_map, phenotype + "#shared", hm_id +",");
                try_incr_AL_hashmap(hmgroups_per_class, "pheno_SHARED", hm_group);
            } else {
                int[] class_value = {0}; // 0 means not enough for shared or specific, 1 is shared, 2 is specific.
                phenotype_min_max.put(phenotype + "_class", class_value);
            }
        }
    } 
    
    /**
     * Calculate and returns 3 ratios. 
     * 1. ratio between pheno and none pheno, 
     * 2. ratio between pheno present and absent
     * 3. ratio between non pheno present and absent
     * @param pheno_count
     * @param rest_current_pheno
     * @param present_rest
     * @param not_present_rest
     * @return array with 3 values 
     */
    public double[] calculate_odds_ratios(double pheno_count, double rest_current_pheno, double present_rest, double not_present_rest) {
        if (pheno_count == 0 || rest_current_pheno == 0 || present_rest == 0|| not_present_rest == 0) {
            pheno_count += 0.5;
            rest_current_pheno += 0.5;
            present_rest += 0.5;
            not_present_rest += 0.5;
        }
        double odds_ratio = ((pheno_count/rest_current_pheno)/(present_rest/not_present_rest));
        double ratio_pheno = pheno_count/rest_current_pheno;
        double ratio_rest = present_rest/not_present_rest;
        return new double[]{odds_ratio, ratio_pheno, ratio_rest};
        
    }
    
    /**
     * One-sided fisher exact test. To get the two-sided value, multiply by 2.
     * @param present_pheno
     * @param rest_current_pheno
     * @param present_rest
     * @param not_present_rest
     * @return P value
     */
    public static double fisher_exact(int present_pheno, int rest_current_pheno, int present_rest, int not_present_rest) {
        if (rest_current_pheno + not_present_rest == 0) { // both phenotypes only have present, unable to test 
            return 9999;
        }
        int total_genomes = present_pheno + rest_current_pheno + present_rest + not_present_rest;
        double value1 = Arithmetic.binomial(present_pheno + rest_current_pheno, present_pheno);
        double value2 = Arithmetic.binomial(present_rest + not_present_rest, present_rest);
        double value3 = Arithmetic.binomial(total_genomes, present_pheno + present_rest);
        return (value1 * value2) / value3; // return p value
    }

    public static long fact(int i) {
        if(i <= 1) {
            return 1;
        }
        return i * fact(i - 1);
    }

    /**
     * Create core_groups.csv, contains the node identifiers of the CORE homology groups.
     * @param hmgroups_per_class
     * @return 
     */
    private int create_gclass_core_groups(HashMap<String, ArrayList<Node>> hmgroups_per_class) {
        if (!hmgroups_per_class.containsKey("core")) {
            write_string_to_file_in_DB("", "gene_classification/core_groups.csv");
            return 0;
        } 
        StringBuilder output = new StringBuilder();
        ArrayList<Node> hmgroups = hmgroups_per_class.get("core");
        for (Node hmgroup : hmgroups) {
            long hm_id = hmgroup.getId();
            output.append(hm_id).append(","); 
        }
        write_string_to_file_in_DB(output.toString().replaceFirst(".$",""), "gene_classification/core_groups.csv"); 
        return hmgroups.size();
    }
    
    /**
     * Creates accessory_groups.csv, contains the node identifiers of ACCESSORY homology groups. 
     * The groups are ordered (descendingly) by the group size based on the total number of genomes present.
     * @param accessory_per_count 
     */
    public static void create_gclass_accessory_groups(HashMap<Integer, String> accessory_per_count) {
        StringBuilder output_builder = new StringBuilder(); 
        for (int i = core_threshold-1; i > 1; --i) {
            String percentage = get_percentage_str(i, core_threshold, 2); 
            String value = accessory_per_count.get(i).replaceFirst(".$",""); // retrieve & immediately remove last character
            String[] groups_array = value.split(",");
            output_builder.append("#").append(i).append(" genomes ").append(percentage).append("%. ").append(groups_array.length).append(" groups\n")
                    .append(value).append("\n\n");
        }
        write_SB_to_file_in_DB(output_builder, "gene_classification/accessory_groups.csv");
    } 
    
    /**
     * Creates cnv_core_accessory.txt, core and accessory groups with genomes that have additional copies compared to the lowest number (at least 1) in the group.
     * @param genome_list a list of all genomes
     * @param cnv_map 
     */
    private void create_gclass_cnv(ArrayList<String> genome_list, HashMap<String, StringBuilder> cnv_map) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "gene_classification/cnv_core_accessory.txt"))) {
            out.write("#Genomes with additional copies compared to other genomes\n"
                    + "#Homology group identifier, number of additional copies compared to the lowest number in a homology group (at least 1).\n");
            for (String genome : genome_list) {
                StringBuilder copies1 = cnv_map.get(genome + "core");
                StringBuilder copies2 = cnv_map.get(genome + "access");
                out.write("\n#Genome " + genome
                        + "\nCore\n" + copies1.toString() 
                        + "\nAccessory\n" + copies2.toString());
            }
        } catch (IOException e) {
            Pantools.logger.warn("Unable to write to: {}cnv_core_accessory.txt", WORKING_DIRECTORY);
        }
    }

    /**
     * @param phenotype_result_map
     * Creates phenotype_shared_groups.csv, the node identifiers of phenotype shared homology groups.
     */
    public void write_glcass_pheno_groups(HashMap<String, StringBuilder> phenotype_result_map) {
        if (PHENOTYPE == null) {
            return;
        }
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "gene_classification/phenotype_shared_groups.csv"))) {
            for (String phenotype_key : phenotype_result_map.keySet()) {
                if (!phenotype_key.endsWith("#shared")) {
                    continue;
                }
                StringBuilder value = phenotype_result_map.get(phenotype_key); 
                String phenotype = phenotype_key.replace("#shared","");
                int[] genomes = phenotype_map.get(phenotype); 
                int pheno_threshold = phenotype_threshold_map.get(phenotype); 
                out.write("Phenotype: " + phenotype + ", " + genomes.length + " genomes, threshold of " + pheno_threshold + " genomes\n" 
                        + value.toString().replaceFirst(".$","") + "\n\n"); 
            } 
            out.close();
        } catch (IOException e) {
            Pantools.logger.error("Unable to create: {}gene_classification/phenotype_shared_groups.csv", WORKING_DIRECTORY);
            System.exit(1);
        }
        
        StringBuilder specific_builder = new StringBuilder();
        for (String phenotype_key : phenotype_result_map.keySet()) { 
            if (!phenotype_key.endsWith("#specific")) {
                continue;
            }
           
            StringBuilder hm_nodes_builder = phenotype_result_map.get(phenotype_key);
            String phenotype = phenotype_key.replace("#specific","");
            int[] genomes = phenotype_map.get(phenotype); 
            int pheno_threshold = phenotype_threshold_map.get(phenotype); 
            if (phenotype.equals("?") || phenotype.equals("Unknown")) {
                continue;
            }
            
            if (hm_nodes_builder != null) { 
                specific_builder.append("Phenotype: ").append(phenotype).append(", ").append(genomes.length).append(" genomes, threshold of ")
                        .append(pheno_threshold).append(" genomes\n").append(hm_nodes_builder.toString().replaceFirst(".$","")).append("\n\n");
            } else {
                specific_builder.append("Phenotype: ").append(phenotype).append("\n\n");
            }
        }
        write_SB_to_file_in_DB(specific_builder, "gene_classification/phenotype_specific_groups.csv");
        
        StringBuilder exclusive_builder = new StringBuilder();
        for (String phenotype : phenotype_map.keySet()) { 
            int[] genomes = phenotype_map.get(phenotype); 
            if (phenotype.equals("?") || phenotype.equals("Unknown")) {
                continue;
            }  
            exclusive_builder.append("Phenotype: ").append(phenotype).append(", ");
            for (int i = genomes.length; i > 0; i--) { 
                StringBuilder hm_nodes_builder = phenotype_result_map.get(phenotype + "#exclusive#" + i);
                if (hm_nodes_builder != null) { 
                    exclusive_builder.append(phenotype).append(" ").append(i).append("/").append(genomes.length).append(" genomes\n")
                            .append(hm_nodes_builder.toString().replaceFirst(".$","")).append("\n\n");
                } else { 
                    exclusive_builder.append(phenotype).append(" ").append(i).append("/").append(genomes.length).append(" genomes\n\n");
                }
            }
        }
        write_SB_to_file_in_DB(exclusive_builder, "gene_classification/phenotype_exclusive_groups.csv");
    }
    
    /**
     * Creates unique_groups.csv, the node identifiers of unique homology groups ordered by genome.
     * @param genome_list a list of all genome identifiers
     * @param unique_hm_per_genome a hashmap with genome identifiers as keys and a hashset of hm node identifiers as values.
     * @return the total number of unique homology groups.
     */
    public int create_gclass_unique_groups(ArrayList<String> genome_list, HashMap<String, HashSet<String>> unique_hm_per_genome) {
        StringBuilder output_builder = new StringBuilder(); 
        HashSet<String> hm_group_set = new HashSet<>();
        for (String genome : genome_list) {
            if (unique_hm_per_genome.containsKey(genome)) {
                HashSet<String> value = unique_hm_per_genome.get(genome);
                String value_str = value.toString().replace("[","").replace("]","").replace(", ",",");
                output_builder.append("Genome ").append(genome).append("\n").append(value_str).append("\n\n");
                hm_group_set.addAll(value);
            } else {
                output_builder.append("Genome ").append(genome).append("\n\n");
            }
        }
        write_SB_to_file_in_DB(output_builder, "gene_classification/unique_groups.csv");
        return hm_group_set.size();
    } 
  
    /**
     * Key is annotation identifers and also stores the genome number as key.
     * The genome number matches the annotation id from annotation_identifiers
     * On a panproteome the returned hashmap is empty as no annotation nodes are present
     * @return 
     */
    public static HashMap<String, Integer> get_num_proteins_from_individual_genomes_map() { 
        HashMap<String, Integer> proteins_per_genome = new HashMap<>();
        ResourceIterator<Node> annotation_nodes = GRAPH_DB.findNodes(ANNOTATION_LABEL);
        int[] highest_id_array = new int[total_genomes];
        while (annotation_nodes.hasNext()) { 
            Node annotation_node = annotation_nodes.next();
            String identifier = (String) annotation_node.getProperty("identifier");
            String[] id_array = identifier.split("_");
            int prot_count = (int) annotation_node.getProperty("num_proteins");
            int genome_nr = Integer.parseInt(id_array[0]);
            int anno_counter = Integer.parseInt(id_array[1]);
            if (anno_counter > highest_id_array[genome_nr-1]) {
                highest_id_array[genome_nr-1] = anno_counter;
                proteins_per_genome.put(identifier, prot_count);
            }
            if (annotation_identifiers.contains(identifier)) {
                proteins_per_genome.put(id_array[0], prot_count);
            }
        }
        return proteins_per_genome;
    }

    /**
     * Access to get_annotation_identifiers with Path annotationsFile instead of String
     * TODO: remove the other method and use get_annotation_identifiers(Path annotationsFile) instead
     * @param print whether or not to print the number of selected annotations
     * @param ignore_file whether to ignore the annotations file and use all annotations
     * @param annotationsFile the file containing the annotations to use
     * @return arraylist of identifiers to use
     */
    public static ArrayList<String> get_annotation_identifiers(boolean print, boolean ignore_file, Path annotationsFile) {
        String annotationsFileString;
        if (annotationsFile == null) {
            annotationsFileString = null;
        } else {
            annotationsFileString = annotationsFile.toFile().getAbsolutePath();
        }
        return get_annotation_identifiers(print, ignore_file, annotationsFileString);
    }
    
    /**
     * 
     * @param print whether or not to print the number of selected annotations
     * @param ignore_file whether to ignore the annotations file and use all annotations
     * @param annotationsFile the file containing the annotations to use
     * @return arraylist of identifiers to use
     */
    public static ArrayList<String> get_annotation_identifiers(boolean print, boolean ignore_file, String annotationsFile) {
        if (skip_array == null) { // for development, prevents NullPointerExceptions
            Pantools.logger.error("skip_array does not exist yet.");
            System.exit(1);
        }
        ArrayList<String> selected_annotation_ids = new ArrayList<>();
        if (PROTEOME) { // panproteomes don't have annotation identifiers, only genome numbers
            return selected_annotation_ids;
        }
        ResourceIterator<Node> annotation_nodes = GRAPH_DB.findNodes(ANNOTATION_LABEL);
        int[] highest_ids = new int[total_genomes];
        String[] highest_ids_str = new String[total_genomes];
        while (annotation_nodes.hasNext()) {
            Node annotation_node = annotation_nodes.next();
            String identifier = (String) annotation_node.getProperty("identifier");
            String[] id_array = identifier.split("_");
            int genome_nr = Integer.parseInt(id_array[0]);	
            if (skip_array[genome_nr-1]) {
                continue;
            }
            int anotation_number = Integer.parseInt(id_array[1]);	
            if (anotation_number > highest_ids[genome_nr-1]) {
                highest_ids[genome_nr-1] = anotation_number;
            }
        }
       
        for (int i=0; i < highest_ids.length; i++) { 
             highest_ids_str[i] = (i+1) + "_" + highest_ids[i];
        }
        if (annotationsFile != null && !ignore_file) {
            ArrayList<String> selected_ids = new ArrayList<String>();
            try (BufferedReader in = new BufferedReader(new FileReader(annotationsFile))) {
                for (int c = 0; in.ready();) {
                    String line = in.readLine().trim();
                    if (line.contains(",") || line.contains(".") || !line.contains("_")) {
                        Pantools.logger.error("Not all identifiers in {} are correctly formatted: {}", annotationsFile, line);
                        System.exit(1);
                    }
                    selected_ids.add(line);
                }
            } catch (IOException ioe) {
                Pantools.logger.error("Failed to read file given by -af: {}", annotationsFile);
                System.exit(1);
            }
            if (print) {
                Pantools.logger.info("Using {} selected annotations.", selected_ids.size());
            }
            for (String identifier : selected_ids) {
                String[] id_array = identifier.split("_");
                int genome_nr = Integer.parseInt(id_array[0]);	
                highest_ids_str[genome_nr-1] = identifier;
            } 
        }
        
        for (String text : highest_ids_str) {
            selected_annotation_ids.add(text);
        }
        return selected_annotation_ids;
    }
    
    /**
     * Get all the annotation identifiers of annotation nodes
     * @return 
     */
    public static HashMap<Integer, ArrayList<String>> get_all_annotation_identifiers() {
        HashMap<Integer, ArrayList<String>> annotations_map = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> annotation_nodes = GRAPH_DB.findNodes(ANNOTATION_LABEL);
            while (annotation_nodes.hasNext()) {
                Node annotation_node = annotation_nodes.next();
                String identifier = (String) annotation_node.getProperty("identifier");
                String[] id_array = identifier.split("_");
                int genome_nr = Integer.parseInt(id_array[0]);
                if (skip_array[genome_nr - 1]) {
                    continue;
                }
                try_incr_AL_hashmap(annotations_map, genome_nr, identifier);
            }
            tx.success();
        }
        return annotations_map;
    }
    
    /*
    Requires 
     -database-path 
     */
    public void core_unique_thresholds() {
        Pantools.logger.info("Calculating number of CORE and UNIQUE groups when using different cut-offs.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        HashMap<Integer, StringBuilder> output_build_map = new HashMap<>();
        create_directory_in_DB("core_unique_thresholds"); 
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            check_current_grouping_version(); // check which version of homology grouping is active
            if (grouping_version < 1) {
                System.out.println("\rNo grouping active");
                System.exit(1);
            }
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            double percentage_per_genome = 100.0/adj_total_genomes;
            double genomes_per_pc = adj_total_genomes/100.0;
            String genomes_per_pc_str = String.format("%.2f", genomes_per_pc);
            System.out.println("\r" + adj_total_genomes + " genomes -> " + genomes_per_pc_str + " genomes per 1 percent");
            for (int i=100; i>= 0; i--) { // yes, must go to zero
                String start_csv = i + ",";
                StringBuilder output_map = new StringBuilder(start_csv);
                output_build_map.put(i,output_map);
                if (i == 0) {
                    continue;
                }
                double test = i/percentage_per_genome;
                int unique_low = (int) Math.round(test);
                if (unique_low == 0) {
                    unique_low = 1;
                }
            }
             
            ResourceIterator<Node> homology_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
            HashMap<Integer, int[]> total_hmgroups_map = new HashMap<>(); 
            for (int i=0; i <= 100; i++) {
                int[] values = new int[2]; // core, unique 
                total_hmgroups_map.put(i, values);
            }
            
            int hmgroup_counter = 0, normal_core = 0, normal_unique = 0;
            int total_hmgroups = (int) count_nodes(HOMOLOGY_GROUP_LABEL);
            while (homology_nodes.hasNext()) {
                hmgroup_counter ++;
                if (hmgroup_counter % 100 == 0 || hmgroup_counter == total_hmgroups) {
                    System.out.print("\rGathering homology groups: " + hmgroup_counter + "/" + total_hmgroups);
                }
               
                Node homology_node = homology_nodes.next();
                int[] copy_number = (int[]) homology_node.getProperty("copy_number");
                int genome_counter = 0;
                for (int i = 1; i < copy_number.length; i++) {
                    if (copy_number[i] != 0) {
                        genome_counter ++;
                    }
                }
                if (genome_counter == adj_total_genomes) {
                    normal_core ++;
                } else if (genome_counter == 1) {
                    normal_unique ++;
                }
              
                for (int j=100; j >= 1; j--) { // determine core groups
                    double threshold = j/percentage_per_genome;
                    if (genome_counter >= threshold) {
                        incr_array_hashmap(total_hmgroups_map, j, 1, 0);
                    }
                }
                
                for (int j=1; j <= 100; j++) { // determine unique groups 
                    double threshold = j/percentage_per_genome;
                    if (threshold < 1) {
                        threshold = 1.0;
                    }
                    if (genome_counter <= threshold) {
                        incr_array_hashmap(total_hmgroups_map, j, 1, 1);
                    }
                }
            } 
            create_core_unique_thresholds_rscript_input(total_hmgroups_map, hmgroup_counter, normal_core, normal_unique);
            create_core_unique_thresholds_rscript(); 
            tx.success(); // transaction successful, commit changes
        }
        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}core_unique_thresholds/core_unique_thresholds.csv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}core_unique_thresholds/core_unique_thresholds.R", WORKING_DIRECTORY);
    }
    
    /**
     * Stop PanTools when less than ? genomes are included in the analysis
     * @param genome_threshold minimum required genomes
     */
    public static void stopWithLessThanGenomes(int genome_threshold) {
        if (adj_total_genomes < genome_threshold) {
            Pantools.logger.error("The pangenome should contain at least {} genomes for this analysis.", genome_threshold);
            System.exit(1);
        }
    }
    
    /**
     * Known as pangenome_structure_gene() in manual
     * @param pavs whether to use PAV information
     * Optional
     * --value
     */
    public void pangenome_size_genes(boolean pavs) {
        Pantools.logger.info("Calculate the pangenome size based on genes.");
        report_number_of_threads(); // prints how many threads were selected by user
        create_directory_in_DB("pangenome_size/gene/gains_losses");
        check_database(); // starts up the graph database if needed
        retrieve_number_of_loops_for_size(10000);

        ArrayList<String> genome_list; // will store a list of all genome names
        int[][] allHmGroupArray; // will store the number of copies of each homology group (outer) in each genome (inner)
        int[][] all_core, all_accessory, all_unique; // will store the number of core, accessory and unique groups for each loop (outer) and #genomes (inner)

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            check_current_grouping_version(); // check which version of homology grouping is active
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            update_skip_array_based_on_grouping(pangenome_node);
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
       
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            genome_list = create_genome_list(pavs);

            // stop if less than 3 genomes are included in the analysis
            if (genome_list.size() < 3) {
                Pantools.logger.error("The pangenome should contain at least 3 genomes for this analysis.");
                System.exit(1);
            }

            // create allHmGroupArray (that will store the number of copies of each homology group (outer) in each genome (inner))
            int hmCounter;
            try (ResourceIterator<Node> hm_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL)) {
                int total_hmgroups = (int) count_nodes(HOMOLOGY_GROUP_LABEL);
                all_core = new int[total_loops][genome_list.size()];
                all_accessory = new int[total_loops][genome_list.size()];
                all_unique = new int[total_loops][genome_list.size()];
                allHmGroupArray = new int[total_hmgroups][genome_list.size()];
                hmCounter = 0;
                while (hm_nodes.hasNext()) {
                    Pantools.logger.info("Getting copy number for homology group {}/{}", hmCounter + 1, total_hmgroups);
                    Node hmNode = hm_nodes.next();
                    allHmGroupArray[hmCounter] = getCopyNumberArray(hmNode, genome_list, pavs);
                    hmCounter++;
                }
            }

            // stop the function if no grouping is found
            if (hmCounter == 0) {
                check_if_there_are_inactive_homology_groups();
                return;
            }

            // transaction successful, commit changes
            tx.success();
        }

        // update the all_core, all_accessory and all_unique arrays
        calculatePangenomeSizeGenes(genome_list, allHmGroupArray, all_core, all_accessory, all_unique);

        // calculate gains/losses
        calculate_gains_losses_at_last_genome(genome_list, allHmGroupArray);
        calculate_gains_losses_per_added_genome(genome_list, all_core, all_accessory, all_unique);

        // write the pangenome size
        write_pangenome_size_gene_output(genome_list, all_core, all_accessory, all_unique);

        // create Rscripts
        create_pangenome_size_rscript("gene");
        create_pangenome_size_gains_losses_rscript();

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}pangenome_size/gene/pangenome_size.txt", WORKING_DIRECTORY);
        Pantools.logger.info(" {}pangenome_size/gene/pangenome_growth.R", WORKING_DIRECTORY);
        Pantools.logger.info(" {}pangenome_size/gene/gains_losses.txt", WORKING_DIRECTORY);
        Pantools.logger.info(" {}pangenome_size/gene/gains_losses_last_genome.txt", WORKING_DIRECTORY);
        Pantools.logger.info(" {}pangenome_size/gene/gains_losses_median_or_average.R", WORKING_DIRECTORY);
        Pantools.logger.info(" {}pangenome_size/gene/gains_losses_median_and_average.R", WORKING_DIRECTORY);
        Pantools.logger.info(" {}pangenome_size/gene/heaps_law.R", WORKING_DIRECTORY);
    }

    /**
     * Count the number of mRNA nodes for a given homology group node
     * @param hmNode homology group node
     * @param genomeList a list of all genomes, used to complete the hashmap
     * @param pavs whether to use PAV information
     * @return an array with the copy number per genome, ordered by the genomeList
     */
    private int[] getCopyNumberArray(Node hmNode, ArrayList<String> genomeList, boolean pavs) {
        Pantools.logger.debug("Getting copy number for homology group {}.", hmNode.getId());

        int[] copyNumbers = new int[genomeList.size()];

        // loop over all mRNA nodes attached to the homology group
        for (Relationship rel : hmNode.getRelationships(Direction.OUTGOING, has_homolog)) {
            Node mrnaNode = rel.getEndNode();
            String mrnaGenome = String.valueOf((int) mrnaNode.getProperty("genome"));

            if (genomeList.contains(mrnaGenome)) {
                int index = genomeList.indexOf(mrnaGenome);
                copyNumbers[index]++;
                Pantools.logger.debug("{} in genome {} has {} copies.", mrnaNode.getProperty("protein_ID"), mrnaGenome, copyNumbers[index]);
            } else {
                Pantools.logger.debug("Genome {} not found in genome list.", mrnaGenome);
            }

            if (pavs) {
                // loop over all variant nodes attached to the mRNA node
                for (Relationship rel2 : mrnaNode.getRelationships(Direction.OUTGOING, has_variant)) {
                    Node variantNode = rel2.getEndNode();
                    String variantGenome = (String) variantNode.getProperty("id");
                    Pantools.logger.debug("Found variant {} for sample {}.", variantNode.getProperty("protein_ID"), variantNode.getProperty("sample"));

                    // only add variant count if it is present
                    if (variantNode.hasProperty("present")) {
                        if ((boolean) variantNode.getProperty("present")) {
                            Pantools.logger.debug("Variant is present.");
                            int index2 = genomeList.indexOf(variantGenome);
                            copyNumbers[index2]++;
                        } else {
                            Pantools.logger.debug("Variant is absent.");
                        }
                    }
                }
            }
        }

        Pantools.logger.debug("Copy numbers: {}.", Arrays.toString(copyNumbers));

        return copyNumbers;
    }

    /**
     * Calculate the pangenome size for genes. This function calculates the core, accessory and unique groups per genome
     * by recalculating the core, accessory and unique size X (defined by loop_numbers) times. For every loop,
     * genomes are one by one randomly added to the set of genomes. The core, accessory and unique groups are
     * recalculated for every loop. The core, accessory and unique groups are stored in the all_core, all_accessory
     * and all_unique arrays and are updated in this function.
     * @param genomeList a list with all genome names
     * @param allHmGroupArray a 2D array with the copy numbers per genome (inner array) per hm group (outer array)
     *                        this array is not updated in this function
     * @param coreGroupsPerGenome a 2D array with the number of core groups per genome (inner array) per loop (outer
     *                           array)
     *                           this array is updated in this function
     * @param accessoryGroupsPerGenome a 2D array with the number of accessory groups per genome (inner array) per loop
     *                                (outer array)
     *                                this array is updated in this function
     * @param uniqueGroupsPerGenome a 2D array with the number of unique groups per genome (inner array) per loop (outer
     *                             array)
     *                             this array is updated in this function
     */
    protected void calculatePangenomeSizeGenes(ArrayList<String> genomeList,
                                             int[][] allHmGroupArray,
                                             int[][] coreGroupsPerGenome,
                                             int[][] accessoryGroupsPerGenome,
                                             int[][] uniqueGroupsPerGenome) {
        Pantools.logger.info("Calculating pangenome size for {} in {} loops.", genomeList, total_loops);

        int[][] newGroupsPerGenome = new int[total_loops][genomeList.size()]; // key = loop number, value = array with new genes per genome

        // create threadpool for parallel processing
        ExecutorService es = Executors.newFixedThreadPool(THREADS);
        List<Future<?>> futures = new ArrayList<>();
        Pantools.logger.debug("Created threadpool with {} threads.", THREADS);

        // perform the calculations as often as total_loops indicates in parallel
        for (int loopNr = 0; loopNr < total_loops; loopNr++) {
            Future<?> f = es.submit(new callCalculatePangenomeSizeGenes(genomeList,
                    allHmGroupArray,
                    coreGroupsPerGenome,
                    accessoryGroupsPerGenome,
                    uniqueGroupsPerGenome,
                    newGroupsPerGenome,
                    loopNr));
            futures.add(f);
        }

        // actually run the threads
        int futureCount = 0;
        int totalFutures = futures.size();
        Pantools.logger.debug("Running {} threads.", totalFutures);
        for (Future<?> f : futures) {
            futureCount++;
            try {
                f.get();

                if (Pantools.logger.isDebugEnabled()) {
                    Pantools.logger.debug("Finished loop {}/{}.", futureCount, totalFutures);
                } else {
                    if (futureCount < 10 || (futureCount) % 100 == 0) {
                        Pantools.logger.info("Finished loop {}/{}.", futureCount, totalFutures);
                    }
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        // shutdown the threadpool
        es.shutdownNow();
        Pantools.logger.debug("Shutdown threadpool.");

        calculate_heaps_law(newGroupsPerGenome);
    }

    private class callCalculatePangenomeSizeGenes implements Callable<Integer> {
        private final ArrayList<String> genomeList;
        private final int[][] allHmGroupArray;
        private final int[][] coreGroupsPerGenome;
        private final int[][] accessoryGroupsPerGenome;
        private final int[][] uniqueGroupsPerGenome;
        private final int[][] newGroupsPerGenome;
        private final int loopNr;

        public callCalculatePangenomeSizeGenes(ArrayList<String> genomeList,
                                               int[][] allHmGroupArray,
                                               int[][] coreGroupsPerGenome,
                                               int[][] accessoryGroupsPerGenome,
                                               int[][] uniqueGroupsPerGenome,
                                               int[][] newGroupsPerGenome,
                                               int loopNr) {
            this.genomeList = genomeList;
            this.allHmGroupArray = allHmGroupArray;
            this.coreGroupsPerGenome = coreGroupsPerGenome;
            this.accessoryGroupsPerGenome = accessoryGroupsPerGenome;
            this.uniqueGroupsPerGenome = uniqueGroupsPerGenome;
            this.newGroupsPerGenome = newGroupsPerGenome;
            this.loopNr = loopNr;
        }

        public Integer call() {
            // create a random generator
            Random generator = new Random();

            // create a new list for genomes and a list for picking a random number
            ArrayList<String> copyGenomeList = new ArrayList<>(genomeList);
            ArrayList<String> newGenomeList = new ArrayList<>();
            ArrayList<Integer> seenHmGroups = new ArrayList<>();

            // add one genome at a time
            int totalGenomes = genomeList.size();
            for (int genomeNr = 0; genomeNr < totalGenomes; genomeNr++) {
                int randomGenome = generator.nextInt(copyGenomeList.size());
                String genome = copyGenomeList.get(randomGenome);
                copyGenomeList.remove(randomGenome);
                newGenomeList.add(genome);
                Pantools.logger.debug("Adding genome {} to the set of genomes.", genome);

                // calculate the core, accessory, unique and new genes for the new genome list
                int[] coreAccessoryUniqueNew = calculateCoreAccessoryUniqueNewGroups(newGenomeList, genomeList, allHmGroupArray, seenHmGroups);
                Pantools.logger.debug("[core, accessory, unique, new] = {}.", Arrays.toString(coreAccessoryUniqueNew));
                coreGroupsPerGenome[loopNr][genomeNr] = coreAccessoryUniqueNew[0];
                accessoryGroupsPerGenome[loopNr][genomeNr] = coreAccessoryUniqueNew[1];
                uniqueGroupsPerGenome[loopNr][genomeNr] = coreAccessoryUniqueNew[2];
                newGroupsPerGenome[loopNr][genomeNr] = coreAccessoryUniqueNew[3];
            }

            return loopNr;
        }
    }

    /**
     * Calculate the core, accessory, unique and new groups for a given list of genomes. Core means that the group is
     * present in all genomes, accessory means that the group is present in at least one genome but not in all
     * genomes and unique means that the group is present in only one genome. New means that the homology group was not
     * seen in a provided previous list of indices corresponding to allHmGroups.
     * @param genomeList a list with all genome names to use for the calculation
     * @param allGenomeList a list with all genome names for getting the indices of genomeList in allHmGroupsArray
     * @param allHmGroupArray a 2D array with the copy numbers per genome (inner array) per hm group (outer array)
     *                        this array is not updated in this function
     * @param seenHmGroups a list of homology groups that have already been seen (for calculating the new groups)
     * @return an array with the number of core, accessory, unique and new genes
     */
    protected int[] calculateCoreAccessoryUniqueNewGroups(ArrayList<String> genomeList,
                                                         ArrayList<String> allGenomeList,
                                                         int[][] allHmGroupArray,
                                                         ArrayList<Integer> seenHmGroups) {
        Pantools.logger.debug("Calculating core, accessory, unique and new genes for {} genomes.", genomeList.size());

        int coreGroups = 0;
        int accessoryGroups = 0;
        int uniqueGroups = 0;
        int newGroups = 0;

        // loop over all homology groups
        for (int hmGroupNr = 0; hmGroupNr < allHmGroupArray.length; hmGroupNr++) {
            int[] hmGroup = allHmGroupArray[hmGroupNr];
            int genomes = 0;

            // loop over all genomes
            for (String genome : genomeList) {
                int genomeNr = allGenomeList.indexOf(genome);
                if (hmGroup[genomeNr] != 0) {
                    genomes++;
                }
            }

            // check if the homology group is core, accessory or unique
            if (genomes == genomeList.size()) {
                coreGroups++;
            } else if (genomes == 1) {
                uniqueGroups++;
            } else if (genomes > 1 && genomes < genomeList.size()) {
                accessoryGroups++;
            } else if (genomes == 0) {
                continue;
            } else {
                Pantools.logger.error("Something went wrong with the calculation of core, accessory and unique genes.");
                System.exit(1);
            }

            // if we get here, the gene is definitely present
            if (!seenHmGroups.contains(hmGroupNr)) {
                newGroups++;
                seenHmGroups.add(hmGroupNr);
            }
        }

        return new int[]{coreGroups, accessoryGroups, uniqueGroups, newGroups};
    }

    /**
     * If the highest_grouping property is higher than 0, there must be a grouping available
     * @return 
     */
    public static boolean check_if_there_are_inactive_homology_groups() {
        Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
        boolean present = true;
        int highest_grouping = 0;
        if (pangenome_node.hasProperty("highest_grouping")) {
            highest_grouping = (int) pangenome_node.getProperty("highest_grouping");
        }
        if (highest_grouping > 0) {
            Pantools.logger.info("No homology grouping is currently active. Run 'change_active_grouping' first.");
        } else {
           Pantools.logger.info("No homology groups are present in the pangenome. Run 'group' first.");
           present = false;
        }
        return present;
    }
    
    /**
     * Each genome is added to the pangenome of the remaining genomes to identify the number of groups gained (accessory, unique) or lost (core, unique)
     * @param genomeList a list with all genome names
     * @param allHmGroupArray a 2D array with the copy numbers per genome (inner array) per hm group (outer array)
     *                        this array is not updated in this function
     */
    public void calculate_gains_losses_at_last_genome(ArrayList<String> genomeList, int[][] allHmGroupArray) {
        Pantools.logger.info("Calculating gains and losses at last genome.");

        DecimalFormat df = new DecimalFormat("0.00");
        int totalGenomes = genomeList.size();
        int[] core_scores = new int[totalGenomes];
        int[] accessory_scores = new int[totalGenomes];
        int[] unique_scores = new int[totalGenomes];
        int position = 0, highest_core = 999999, highest_unique = 0;
        String highest_core_genome = null, highest_unique_genome = null;
        StringBuilder output_builder = new StringBuilder();
        for (String reference_genome : genomeList) {
            try {
                if (skip_array[Integer.parseInt(reference_genome)-1]) {
                    continue;
                }
            } catch (NumberFormatException ignored) {
            }
            int accessory_counter = 0, unique_counter = 0, not_core_counter = 0, unique_lost = 0, distinct_genes = 0, total_genes = 0;
            for (int[] hmGroup : allHmGroupArray) {
                int reference_count = 0;
                int prot_counter = 0;
                for (int genomeIndex = 0; genomeIndex < hmGroup.length; genomeIndex++) {
                    String hmGenome = genomeList.get(genomeIndex);
                    int copy_number = hmGroup[genomeIndex];
                    try {
                        if (skip_array[Integer.parseInt(hmGenome)-1]) {
                            continue;
                        }
                    } catch (NumberFormatException ignored) {
                    }

                    if (hmGenome.equals(reference_genome)) {
                        reference_count = copy_number;
                        continue;
                    }
                    if (copy_number != 0) {
                        prot_counter ++;
                    }
                }
                if (reference_count != 0) {
                    distinct_genes ++;
                    total_genes += reference_count;
                }
                if (prot_counter == totalGenomes-1 && reference_count == 0) { // core loss
                    not_core_counter --;
                } else if (prot_counter == 0 && reference_count != 0) { // unique gain 
                    unique_counter ++;
                } else if (prot_counter == 1 && reference_count != 0) { // unique loss/accessory gain
                    unique_lost ++; 
                    accessory_counter ++;
                } else if (prot_counter > 0 && reference_count >= 0) { // is already accessory 
                   // do nothing 
                } else { // happens when genomes are skipped
                   // do nothing 
                }
            }
          
            if (not_core_counter < highest_core) {
                highest_core_genome = reference_genome;
                highest_core = not_core_counter;
            }
            
            if (unique_counter > highest_unique) {
                highest_unique_genome = reference_genome;
                highest_unique = unique_counter;
            }
            
            output_builder.append("Genome ").append(reference_genome) 
                    .append("\nDistinct genes: ").append(distinct_genes).append(" (").append(total_genes).append(" total)")
                    .append("\n Core loss ").append(not_core_counter)
                    .append("\n Accessory gain ").append(accessory_counter) 
                    .append("\n Unique gain/loss ").append(unique_counter).append("/").append(unique_lost).append("\n\n");
                  
            unique_scores[position] = unique_counter-unique_lost;
            accessory_scores[position] = accessory_counter;
            core_scores[position] = not_core_counter;
            position ++;
        }
        
        double[] unique_m_a_sd = median_average_stdev_from_array(unique_scores);
        double[] core_m_a_sd = median_average_stdev_from_array(core_scores);
        double[] accessory_m_a_sd = median_average_stdev_from_array(accessory_scores);
        String header = "#Each genome is added to the pangenome of the remaining genomes to identify the number of groups gained (accessory, unique) or lost (core, unique)"
                + "\n\nAverage gain/loss when adding the " + adj_total_genomes + "th genome, standard deviation, range "
                + "\nCore: " + df.format(core_m_a_sd[1]) + ", " + df.format(core_m_a_sd[2]) + ", " 
                    + core_scores[0] + " " + core_scores[core_scores.length-1] 
                + "\nAccessory: " + df.format(accessory_m_a_sd[1]) + ", " + df.format(accessory_m_a_sd[2]) 
                    + ", " + accessory_scores[0] + " " + accessory_scores[accessory_scores.length-1]
                + "\nUnique: " + df.format(unique_m_a_sd[1]) + ", " + df.format(unique_m_a_sd[2])
                     + ", " + unique_scores[0] + " " + unique_scores[unique_scores.length-1]
                + "\n\nMedian gain/loss when adding the " + adj_total_genomes + "th genome"
                + "\nCore: " + core_m_a_sd[0]
                + "\nAccessory: " + accessory_m_a_sd[0]
                + "\nUnique: " + unique_m_a_sd[0]
                + "\n\nHighest unique gain " + highest_unique + " was caused by genome " + highest_unique_genome 
                + "\nHighest core loss " + highest_core + " was caused by genome " + highest_core_genome 
                + "\n\n\n#Group gain/loss per genome\n";
        write_string_to_file_in_DB(header + output_builder, "pangenome_size/gene/gains_losses_last_genome.txt");

        Pantools.logger.info("Written gains and losses at last genome to {}pangenome_size/gene/gains_losses_last_genome.txt", WORKING_DIRECTORY);
    }
    
    /**
     * 
     * @param default_nr_loops 
     */
    public void retrieve_number_of_loops_for_size(int default_nr_loops) {
        if (NODE_VALUE == null) {
            total_loops = default_nr_loops;
            Pantools.logger.info("No number of loops was provided. Performing {} loops.", default_nr_loops);
        } else {
            try {
                total_loops = Integer.parseInt(NODE_VALUE);
            } catch (NumberFormatException no) {
                Pantools.logger.error("'{}' is not a numerical value.", NODE_VALUE);
                System.exit(1);
            }
        }
    }
    
    /**
     * Requires 
     * -dp 
     * 
     * Optional 
     * --threads 
     * --skip/--reference
     * --value the number of loops
     */
    public void pangenome_size_kmer() {
        Pantools.logger.info("Calculate pangenome based on k-mers.");
        create_directory_in_DB("pangenome_size/kmer"); 
        check_database(); // starts up the graph database if needed
        retrieve_number_of_loops_for_size(100);
        
        report_number_of_threads(); // prints how many threads were selected by user
        long num_nodes = 0, degen_nodes2 = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "pangenome_size_kmer"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            KMinOne = K_SIZE -1; 
            num_nodes = (long) pangenome_node.getProperty("num_nodes");
            degen_nodes2 = count_nodes(DEGENERATE_LABEL);
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        
        create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
        stopWithLessThanGenomes(3);
        long one_procent = (num_nodes-1)/100;
        long procent_threshold = one_procent;
        int total_procent = 0;
        
        long[][] all_core = new long[total_loops][adj_total_genomes-1];
        long[][] all_accessory = new long[total_loops][adj_total_genomes-1];
        long[][] all_unique = new long[total_loops][adj_total_genomes-1];
        if (num_nodes-degen_nodes2 > 2147483647) { //2,147,483,647 = max integer size 
            Pantools.logger.error("The pangenome is too large for this analysis :(");
            System.exit(1);
        }
        int size_of_array = Math.toIntExact(num_nodes)-Math.toIntExact(degen_nodes2);
        long[][] frequency_array = new long[size_of_array][0];
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            System.out.print("\rGathering compressed k-mer nodes: 0/" + num_nodes);
            int kmer_nodes = 0, not_degen_nodes = 0;
            ResourceIterator<Node> nuc_nodes = GRAPH_DB.findNodes(NUCLEOTIDE_LABEL);
            while (nuc_nodes.hasNext()) { 
                kmer_nodes ++; 
                if (kmer_nodes > procent_threshold) {
                    procent_threshold += one_procent;
                    total_procent ++;
                }
                Node nuc_node = nuc_nodes.next();
                if (nuc_node.hasLabel(DEGENERATE_LABEL)) { // skip degenerate nodes
                    continue;
                }
                int k_length = (int) nuc_node.getProperty("length");
                long[] frequency = (long[]) nuc_node.getProperty("frequencies");
                long actual_kmers = (long) k_length - KMinOne;
                frequency[0] = actual_kmers; // 
                if (kmer_nodes % 1001 == 0 || kmer_nodes == num_nodes) {
                    System.out.print("\rGathering compressed k-mer nodes: " + kmer_nodes + "/" + num_nodes + " (" + total_procent + "%)");
                }
                frequency_array[not_degen_nodes] = frequency;
                not_degen_nodes++;
            }
            tx.success(); // transaction successful, commit changes
        }
        calculate_pangenome_size_kmer(all_core, all_accessory, all_unique, frequency_array);
        create_pangenome_size_rscript("kmer");
        write_pangenome_size_output_kmer(all_core, all_accessory, all_unique);
        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}pangenome_size/kmer/pangenome_size_kmer.txt", WORKING_DIRECTORY);
        Pantools.logger.info(" {}pangenome_size/kmer/pangenome_growth.R", WORKING_DIRECTORY);
    }
     
    /**
     * 
     * @param all_core
     * @param all_accessory
     * @param all_unique
     * @param frequency_array 
     */
    public void calculate_pangenome_size_kmer(long[][] all_core, long[][] all_accessory, long[][] all_unique, long[][] frequency_array) {
        pangenome_size_kmer_calculcate_all_genomes(all_core, all_accessory, all_unique, frequency_array);
        loop_numbers = new LinkedBlockingQueue<>();
        for (int n=0; n < total_loops; n++) {
            loop_numbers.add(n);
        }
        atomic_counter1 = new AtomicLong(0);
        try {
            ExecutorService es = Executors.newFixedThreadPool(THREADS);
            for(int i = 1; i <= THREADS; i++) {
                es.execute(new multi_thread_pangenome_size_kmer(frequency_array, all_core, all_accessory, all_unique));
            }
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            
        }
        
        System.out.println("\rFinished the pangenome growth simulation");
        for (int i = 1; i < total_loops; i++) { // putting the results from the last loop with all genomes, in all arrays 
            all_core[i][adj_total_genomes-2] = all_core[0][adj_total_genomes-2];
            all_accessory[i][adj_total_genomes-2] = all_accessory[0][adj_total_genomes-2];
            all_unique[i][adj_total_genomes-2] = all_unique[0][adj_total_genomes-2]; 
        }
    }
    
    /**
     * 
     */
    public class multi_thread_pangenome_size_kmer implements Runnable {
        long[][] frequency_array;
        long[][] all_core;
        long[][] all_accessory;
        long[][] all_unique;
         
        public multi_thread_pangenome_size_kmer(long[][] frequency_array, long[][] all_core, long[][] all_accessory, long[][] all_unique) {
           this.frequency_array = frequency_array;
           this.all_core = all_core;
           this.all_accessory = all_accessory;
           this.all_unique = all_unique;
        }
        
        public void run() {
            Random generator = new Random();
            while (!loop_numbers.isEmpty()) {
                int i;
                try {
                    i = loop_numbers.take();
                } catch (InterruptedException e) { // this will never happen
                    i = 0;
                    continue;
                }
                atomic_counter1.getAndIncrement();
                int percent = (int) (atomic_counter1.get() * 100 / total_loops);
                System.out.print("\rNumber of loops " + atomic_counter1.get() + "/" + total_loops + " (" + percent + "%)   ");
                ArrayList<String> genome_list = create_genome_list(false);
                ArrayList<Integer> new_genome_list = new ArrayList<>();
                while (genome_list.size() > 1) {
                    int randomIndex = generator.nextInt(genome_list.size());
                    int genome_nr = Integer.parseInt(genome_list.get(randomIndex));
                    genome_list.remove(randomIndex);
                    new_genome_list.add(genome_nr);
                    Collections.sort(new_genome_list);
                    if (new_genome_list.size() > 1) {
                        calculate_kmer_freq(new_genome_list, all_core, all_accessory, i, all_unique, frequency_array); 
                    } 
                }
            }
        }
     }
  
    /**
     * One loop with all genomes to calculate the core, accessory unique k-mers.
     * @param all_core
     * @param all_accessory
     * @param all_unique
     * @param frequency_array 
     */
    public void pangenome_size_kmer_calculcate_all_genomes(long[][] all_core, long[][] all_accessory, long[][] all_unique, long[][] frequency_array) {
        ArrayList<String> genome_list = create_genome_list(false);
        Pantools.logger.info("First loop with {} genomes.", genome_list.size());
        ArrayList<Integer> new_genome_list = new ArrayList<>(); 
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            new_genome_list.add(i);
        }
        calculate_kmer_freq(new_genome_list, all_core, all_accessory, 0, all_unique, frequency_array);
    }
    
    /**
     * 
     * @param new_genome_list
     * @param all_core
     * @param all_accessory
     * @param loop
     * @param all_unique
     * @param frequency_array 
     */ 
    public static void calculate_kmer_freq(ArrayList<Integer> new_genome_list, long[][] all_core, long[][] all_accessory, int loop, 
            long[][] all_unique, long[][] frequency_array) { 
        
        int[] core_access_unique_genes = new int[3]; // core, accessory, unique
        for (long[] frequency : frequency_array) {
            long actual_kmers = frequency[0];
            boolean core = true;
            int counter = 0;
            for (int index : new_genome_list) {
                if (frequency[index] == 0) {
                    core = false;
                } else {
                    counter ++;
                }
            }
             
            if (counter == 1) {
                core_access_unique_genes[2] += actual_kmers;
            } else if (core) {
                core_access_unique_genes[0] += actual_kmers;
            } else if (counter > 1) {
                core_access_unique_genes[1] += actual_kmers;
            }
        }
        increase_kmer_freq_core_accessory_unique(all_core, all_accessory, loop, all_unique, core_access_unique_genes, new_genome_list.size()-2); // synchronized 
    }
    
    /**
     * 
     * @param all_core
     * @param all_accessory
     * @param loop
     * @param all_unique
     * @param core_access_unique_genes
     * @param position 
    */
    public synchronized static void increase_gene_freq_core_accessory_unique(int[][] all_core, int[][] all_accessory, int loop, int[][] all_unique, 
            int[] core_access_unique_genes, int position) {
        
        all_core[loop][position] = core_access_unique_genes[0];
        all_accessory[loop][position] = core_access_unique_genes[1]; 
        all_unique[loop][position] = core_access_unique_genes[2]; 
    }
    /**
     * 
     * @param all_core
     * @param all_accessory
     * @param loop
     * @param all_unique
     * @param core_access_unique_genes
     * @param position 
     */
    public synchronized static void increase_kmer_freq_core_accessory_unique(long[][] all_core, long[][] all_accessory, int loop, long[][] all_unique, 
            int[] core_access_unique_genes, int position) {
        
        all_core[loop][position] = core_access_unique_genes[0];
        all_accessory[loop][position] = core_access_unique_genes[1]; 
        all_unique[loop][position] = core_access_unique_genes[2]; 
    }
     
    /**
     * 
     * @param class_values
     * @param class_str
     * @param values_set
     * @param values_per_size
     * @param size 
     */
    public static void add_ksize_results_to_set(long[] class_values, String class_str, HashSet<String> values_set, HashMap< String, long[]> values_per_size, int size) {
        for (int i = 0; i < class_values.length; i++) {
            String value_str = (i+2) + "," + class_values[i] + "," + class_str;
            values_set.add(value_str);
            long[] value_array = values_per_size.get((i+2) + class_str);
            value_array[size] = class_values[i];
            values_per_size.put((i+2) + class_str, value_array);
        }
    }
     
    /**
     * 
     * @param all_core
     * @param all_access
     * @param all_unique 
     */
    public static void write_pangenome_size_output_kmer(long[][] all_core, long[][] all_access, long[][] all_unique) {
        StringBuilder input_builder = new StringBuilder("x,y,Category\n");
        StringBuilder input_builder2 = new StringBuilder("x,y,Category\n");
        HashSet<String> values_set = new HashSet<>(); // core, access & unique 
        HashSet<String> values_set2 = new HashSet<>(); // core & acccessory 
        HashMap<String, long[]> values_per_size = new HashMap<>(); // 
        for (int i = 2; i <= adj_total_genomes; i++) {
            //Pantools.logger.info(j);
            long[] address = new long[total_loops]; 
            values_per_size.put(i + "Core", address);
            address = new long[total_loops]; 
            values_per_size.put(i + "Accessory", address);
            address = new long[total_loops]; 
            values_per_size.put(i + "Unique", address);
        }
         
        for (int i = 0; i < all_core.length; i ++) {
            add_ksize_results_to_set(all_core[i], "Core", values_set, values_per_size, i);
            add_ksize_results_to_set(all_core[i], "Core", values_set2, values_per_size, i);
            add_ksize_results_to_set(all_access[i], "Accessory", values_set, values_per_size, i);
            add_ksize_results_to_set(all_unique[i], "Unique", values_set, values_per_size, i);
            for (int j = 0; j < all_access[i].length; j++) {
                String value_str = (j+2) + "," + (all_access[i][j] + all_unique[i][j]) + ",Accessory";
                values_set2.add(value_str);
            }
        }
   
        for (String value_str: values_set) {
            input_builder.append(value_str).append("\n");
        }
        for (String value_str: values_set2) { // core & access 
            input_builder2.append(value_str).append("\n");
        }
           
        write_SB_to_file_in_DB(input_builder, "pangenome_size/kmer/core_accessory_unique_size.csv"); // core, accessory and unique 
        write_SB_to_file_in_DB(input_builder2, "pangenome_size/kmer/core_accessory_size.csv"); // only core and accessory
        create_ksize_overview(values_per_size, adj_total_genomes);
    }
     
    /**
     * 
     * @param j
     * @param values_per_size
     * @param class_str
     * @param output_builder 
     */
    public static void calc_min_max_avg_ksize(int j, HashMap< String, long[]> values_per_size, String class_str, StringBuilder output_builder) {
        long[] value1 = values_per_size.get(j + class_str); 
        long lowest = Long.parseLong("99999999999999999");
        long highest = 0;
        double total = 0;
        for (long value: value1) {
            if (value > highest) {
                highest = value;
            }
            if (value < lowest) {
                lowest = value;
            }
            total += value;
        }
        total = total/total_loops;
        output_builder.append(" ").append(class_str).append(" range ").append(lowest).append("-").append(highest)
                .append("\n Average ").append(Math.round(total)).append("\n\n");
    }
        
    /**
     * 
     * @param values_per_size
     * @param total_genomes_adj 
     */
    public static void create_ksize_overview(HashMap< String, long[]> values_per_size, int total_genomes_adj) { 
        StringBuilder output_builder = new StringBuilder();
        for (int j = 2; j < total_genomes_adj+1; j++) {
            output_builder.append("N = ").append(j).append("\n");
            calc_min_max_avg_ksize(j, values_per_size, "Core", output_builder);
            calc_min_max_avg_ksize(j, values_per_size, "Accessory", output_builder);
            calc_min_max_avg_ksize(j, values_per_size, "Unique", output_builder);
            output_builder.append("\n");
        }
        write_SB_to_file_in_DB(output_builder, "pangenome_size/kmer/pangenome_size_kmer.txt");
    }
     
    /**
     * @param variation whether to use accession nodes or not
     * @return
     */
    public ArrayList<String> create_genome_list(boolean variation) {
        ArrayList<String> genomeList = new ArrayList<>();

        // add genomes
        for (int j = 1; j <= total_genomes; j++) {
            if (skip_array[j - 1]) {
                continue;
            }
            genomeList.add(String.valueOf(j));
        }

        // add accession if needed
        if (variation) {
            try (ResourceIterator<Node> accessionNodes = GRAPH_DB.findNodes(ACCESSION_LABEL)) {
                while (accessionNodes.hasNext()) {
                    Node accession = accessionNodes.next();
                    String sample = (String) accession.getProperty("sample");
                    String genome = String.valueOf((int) accession.getProperty("genome"));
                    String accessionId = genome + "|" + sample;
                    genomeList.add(accessionId);
                }
            }
        }

        Pantools.logger.debug("Genome List: ." + genomeList);
        Pantools.logger.info("Generated genome list of size: ." + genomeList.size());

        return genomeList;
    }
     
    /**
     * Requires 
     * --database-path 
     * --input-file
     * 
     * Optional 
     * --phenotype 
     * --mode no-numbers
     * --skip 
     * --reference
     */
    public void rename_matrix() {
        Pantools.logger.info("Rename a CSV formatted matrix file.");
        if (INPUT_FILE == null) {
            Pantools.logger.info("Provide a matrix through --input-file/-if.");
            return;
        }
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            total_genomes = (int) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_genomes");
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            if (PHENOTYPE == null) {
                System.out.println("\rNo --phenotype provided! Placing only genome numbers in the headers (first row/column)" );
            } else {
                System.out.println("\rSelected '" + PHENOTYPE + "' as phenotype. Using this to rename the headers (first row/column)");
                retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            }
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nee) {
            Pantools.logger.error("Unable to retrieve data from the pangenome.");
            System.exit(1);
        }
            
        StringBuilder output_builder = new StringBuilder();
        boolean first = true;
        int matrix_width = 0;
        int line_counter = -1;
        try (BufferedReader in = new BufferedReader(new FileReader(INPUT_FILE))) {
          
            for (int c = 0; in.ready();) {
                line_counter ++;
                String[] line_array = in.readLine().trim().split(",");
                if (line_array.length != matrix_width && matrix_width > 0) { 
                    Pantools.logger.error("Not all rows have the same number of columns.");
                    System.exit(1);
                }
                matrix_width = line_array.length;
                if (first) { // first row
                    StringBuilder header_builder = new StringBuilder(line_array[0] + ",");
                    for (int i = 1; i < line_array.length; i++) {
                        rename_value_for_matrix(line_array[i], i, header_builder);
                    }
                    first = false;
                    output_builder.append(header_builder.toString().replaceFirst(".$","")).append("\n");
                } else {
                    StringBuilder row_builder = new StringBuilder();
                    int genome_nr = rename_value_for_matrix(line_array[0], line_counter, row_builder);
                    if (skip_array[genome_nr-1]) {
                        continue; // skip row completely 
                    }  
                    if (line_counter % 100 == 0) {
                        System.out.print("\rRenaming matrix: line " + line_counter);
                    }
                    for (int i = 1; i < line_array.length; i++) {
                        if (skip_array[i-1]) { // skip a column
                            continue;
                        }
                        row_builder.append(line_array[i]).append(","); 
                    }
                    output_builder.append(row_builder.toString().replaceFirst(".$","")).append("\n");
                }
            }
            //output_builder.append("\n");
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}.", INPUT_FILE);
            System.exit(1);
        }
        write_SB_to_file_full_path(output_builder, INPUT_FILE + "_RENAMED");
        Pantools.logger.info("New matrix written to: {}_RENAMED", INPUT_FILE);
    }
    
    /**
     * 
     * @param value
     * @param i
     * @param output_builder
     * @return Returns the genome number 
     */
    public static int rename_value_for_matrix(String value, int i, StringBuilder output_builder) {
        String[] value_array = value.split(" ");
        int genome_nr = 0;
        if (i == 0) {
            output_builder.append(value);
        } else if (PHENOTYPE != null) {
            try {
                genome_nr = Integer.parseInt(value_array[0]);
            } catch (NumberFormatException nfe) {
                Pantools.logger.error("Phenotype information can only be included when the header of a cell only consists of a value or the cell starts with the genome number\n.");
                System.exit(1);
            }

            if (!skip_array[genome_nr-1]) {
                String phenotype = geno_pheno_map.get(genome_nr);
                if (phenotype.equals("?")) {
                    phenotype = "Unknown";
                }
                if (Mode.equals("NO-NUMBERS")) {
                    output_builder.append(phenotype).append(",");
                } else {
                    output_builder.append(value_array[0]).append(" ").append(phenotype).append(",");
                }
            }
        } else if (PHENOTYPE == null) { // no --phenotype given, so remove the last value/string of value_array 
            String new_value = "";
            if (value_array.length > 1) {
                for (int j = 0; j < value_array.length-1; j++) {
                    new_value += value_array[j] + " ";
                } 
                 new_value = new_value.replaceFirst(".$",""); // remove last character
            } else {
                new_value = value_array[0];
            }
            try {
                genome_nr = Integer.parseInt(value_array[0]);
                if (!skip_array[genome_nr-1]) {
                    output_builder.append(new_value).append(",");
                }
            } catch (NumberFormatException nfe) {
               // not a genome number so unable to skip 
               output_builder.append(new_value).append(",");
            }
           
        }
        return genome_nr;
    }
    
    /**
     * 
     */
    public void write_rscript_for_heaps_law() {
        StringBuilder rscript = new StringBuilder();
        rscript.append("#! /usr/bin/env RScript\n")
                .append("df1 <- read.csv(\"").append(WD_full_path).append("pangenome_size/gene/gene_nrs_for_heaps_law.csv\", header=FALSE)\n")
                .append("df2 <- read.csv(file = \"").append(WD_full_path).append("pangenome_size/gene/genes_for_heaps_law.csv\", header = FALSE)\n")
                .append("cat(\"\\nThe Heaps law model is fitted to the number of new genes observed when genomes are ordered in a random way.\\n"
                    + "If alpha is higher than 1.0 the pangenome is closed, if alpha is below 1.0 it is open.\\n\\n\")\n\n")
                .append("x <- as.integer(df1)\n")
                .append("y <- as.numeric(df2)\n")
                .append("p0 <- c(mean(y[which(x == 2)] ), 1)\n\n")
                .append("objectFun <- function(p, x, y) {\n")
                .append("  y.hat <- p[1] * x^(-p[2])\n")
                .append("  J <- sqrt(sum((y - y.hat)^2))/length(x)\n")
                .append("  return(J)\n}\n\n")
                .append("fit <- optim(p0, objectFun, gr = NULL, x, y, method = \"L-BFGS-B\", lower = c(0, 0), upper = c(10000, 2))\n\n")
                .append("sink(\"").append(WD_full_path).append("pangenome_size/gene/heaps_law_alpha.txt\")\n")
                .append("cat(fit$par[2])\n")
                .append("sink()\n")
                .append("cat(\"alpha =\", fit$par[2], \"\\n\\n\")");
        write_SB_to_file_in_DB(rscript, "pangenome_size/gene/heaps_law.R");
    }
    
    /**
     * https://rdrr.io/cran/micropan/man/heaps.html
     * @param newGroupsPerGenome an integer array storing the number of new genes per added genome (inner array) per
     *                             loop (outer array)
     */
    public void calculate_heaps_law(int[][] newGroupsPerGenome) {
        Pantools.logger.info("Calculating Heaps' Law for pangenome.");

        write_rscript_for_heaps_law();
        StringBuilder new_genes_builder = new StringBuilder();
        StringBuilder gene_nr_builder = new StringBuilder();

        for (int[] loop : newGroupsPerGenome) {
            for (int index = 0; index < loop.length; index++) {
                int genomeNr = index + 1;
                new_genes_builder.append(loop[index]).append(",");
                gene_nr_builder.append(genomeNr).append(",");
            }
        }

        write_string_to_file_in_DB(new_genes_builder.toString().replaceFirst(".$","") + "\n", "pangenome_size/gene/genes_for_heaps_law.csv");
        write_string_to_file_in_DB(gene_nr_builder.toString().replaceFirst(".$","") + "\n", "pangenome_size/gene/gene_nrs_for_heaps_law.csv");
    }

    /**
     * 
     * @param list
     * @return 
     */
    public int[] convert_arraylist_int_to_array(ArrayList<Integer> list) {
        int[] new_array = new int[list.size()];
        for (int i = 0; i < list.size(); i++) { 
            new_array[i] = list.get(i);
        }
        return new_array;
    }
    
    /**
     * Creates the input files for the gains and losses rscripts
     * - average_all.csv
     * - median_all.csv
     * - average_core_accessory.csv
     * - median_core_accessory.csv
     *
     * @param genomeList a list with all genome names
     * @param coreGenesPerGenome a 2D array with the number of core genes per genome (inner array) per loop (outer
     *                           array)
     * @param accessoryGenesPerGenome a 2D array with the number of accessory genes per genome (inner array) per loop
     *                                (outer array)
     * @param uniqueGenesPerGenome a 2D array with the number of unique genes per genome (inner array) per loop (outer
     *                             array)
     */
    public void calculate_gains_losses_per_added_genome(ArrayList<String> genomeList,
                                                        int[][] coreGenesPerGenome,
                                                        int[][] accessoryGenesPerGenome,
                                                        int[][] uniqueGenesPerGenome) {
        Pantools.logger.info("Calculating gains and losses per added genome.");

        int num_genomes = 1;
        DecimalFormat df = new DecimalFormat("0.00");
        boolean first = true;
        StringBuilder all_average_builder = new StringBuilder("x,y,Category,Metric\n");
        StringBuilder all_median_builder = new StringBuilder("x,y,Category,Metric\n");
        StringBuilder CA_average_builder = new StringBuilder("x,y,Category,Metric\n");
        StringBuilder CA_median_builder = new StringBuilder("x,y,Category,Metric\n");
        StringBuilder output_builder = new StringBuilder();
        double first_core_avg = 0, first_unique_avg = 0, first_accessory_avg = 0, last_core_avg = 0, last_unique_avg = 0, last_accessory_avg = 0;
        int genome_diff = genomeList.size() - 3; // TODO: why -3?
        for (int i = 0; i < coreGenesPerGenome[0].length; i++) {
            num_genomes ++;
            if (num_genomes < 3) {
                continue;
            }
            if (i+1 == coreGenesPerGenome[0].length) {
                continue;
            }
            ArrayList<Integer> all_core_list = new ArrayList<>();
            ArrayList<Integer> all_accessory_list = new ArrayList<>();
            ArrayList<Integer> all_unique_list = new ArrayList<>();
            for (int j = 0; j < coreGenesPerGenome.length; j++) {
                all_accessory_list.add(accessoryGenesPerGenome[j][i]);
                all_core_list.add(coreGenesPerGenome[j][i]);
                all_unique_list.add(uniqueGenesPerGenome[j][i]);
            }
            ArrayList<Integer> next_all_core_list = new ArrayList<>();
            ArrayList<Integer> next_all_accessory_list = new ArrayList<>();
            ArrayList<Integer> next_all_unique_list = new ArrayList<>();
            for (int j = 0; j < coreGenesPerGenome.length; j++) {
                next_all_accessory_list.add(accessoryGenesPerGenome[j][i+1]);
                next_all_core_list.add(coreGenesPerGenome[j][i+1]);
                next_all_unique_list.add(uniqueGenesPerGenome[j][i+1]);
            }
           
            int[] all_core_array = convert_arraylist_int_to_array(all_core_list);
            int[] all_accessory_array = convert_arraylist_int_to_array(all_accessory_list);
            int[] all_unique_array = convert_arraylist_int_to_array(all_unique_list);
            int[] next_all_core_array = convert_arraylist_int_to_array(next_all_core_list);
            int[] next_all_accessory_array = convert_arraylist_int_to_array(next_all_accessory_list);
            int[] next_all_unique_array = convert_arraylist_int_to_array(next_all_unique_list);
            last_core_avg = get_average_from_array(all_core_array);
            last_unique_avg = get_average_from_array(all_unique_array);
            last_accessory_avg = get_average_from_array(all_accessory_array);
            if (first) {
                first = false;
                first_core_avg = last_core_avg;
                first_unique_avg = last_unique_avg;
                first_accessory_avg = last_accessory_avg;
            }
            output_builder.append("\nN=").append(num_genomes)
                    .append("\nCore groups ").append(last_core_avg)
                    .append("\nAccessory groups ").append(last_accessory_avg)
                    .append("\nUnique groups ").append(last_unique_avg).append("\n");
           
            output_builder.append("\nN=").append(num_genomes).append(" -> N=").append((num_genomes+1)).append("\n");
            double[] core_m_a_sd = get_differences_between_two_sizes(all_core_array, next_all_core_array); // [median, average, stdv]
            double[] unique_m_a_sd = get_differences_between_two_sizes(all_unique_array, next_all_unique_array);
            double[] accessory_m_a_sd = get_differences_between_two_sizes(all_accessory_array , next_all_accessory_array ); 
            int[] combi_access = combine_unique_accessory_counts(all_accessory_array, all_unique_array);
            int[] combi_access_plus1 = combine_unique_accessory_counts(next_all_accessory_array, next_all_unique_array);
            double[] combi_accessory_m_a_sd = get_differences_between_two_sizes(combi_access, combi_access_plus1); 
            output_builder.append("Core difference: ").append(df.format(core_m_a_sd[1])).append(", ").append(df.format(core_m_a_sd[0]))
                    .append(", ").append(df.format(core_m_a_sd[2]))
                    .append("\nUnique difference: ").append(df.format(unique_m_a_sd[1])).append(", ").append(df.format(unique_m_a_sd[0]))
                    .append(", ").append(df.format(unique_m_a_sd[2]))
                    .append("\nAccessory difference: ").append(df.format(accessory_m_a_sd[1])).append(", ").append(df.format(accessory_m_a_sd[0]))
                    .append(", ").append(df.format(accessory_m_a_sd[2]))
                    .append("\nAccessory combined with unique difference: ").append(df.format(combi_accessory_m_a_sd[1]))
                        .append(", ").append(df.format(combi_accessory_m_a_sd[0])).append(", ").append(df.format(combi_accessory_m_a_sd[2])).append("\n");
            
            all_average_builder.append((num_genomes+1)).append(",").append(df.format(core_m_a_sd[1])).append(",Core,Average\n")
                    .append((num_genomes+1)).append(",").append(df.format(accessory_m_a_sd[1])).append(",Accessory,Average\n")
                    .append((num_genomes+1)).append(",").append(df.format(unique_m_a_sd[1])).append(",Unique,Average\n");
            all_median_builder.append((num_genomes+1)).append(",").append(df.format(core_m_a_sd[0])).append(",Core,Median\n")
                    .append((num_genomes+1)).append(",").append(df.format(accessory_m_a_sd[0])).append(",Accessory,Median\n")
                    .append((num_genomes+1)).append(",").append(df.format(unique_m_a_sd[0])).append(",Unique,Median\n");
            
            CA_average_builder.append((num_genomes+1)).append(",").append(df.format(core_m_a_sd[1])).append(",Core,Average\n")
                    .append((num_genomes+1)).append(",").append(df.format(combi_accessory_m_a_sd[1])).append(",Accessory,Average\n");
            CA_median_builder.append((num_genomes+1)).append(",").append(df.format(core_m_a_sd[0])).append(",Core,Median\n")
                    .append((num_genomes+1)).append(",").append(df.format(combi_accessory_m_a_sd[0])).append(",Accessory,Median\n");
        }
       
        double first_last_avg_core = (last_core_avg - first_core_avg) / genome_diff;
        double first_last_avg_unique = (last_unique_avg - first_unique_avg) / genome_diff;
        double first_last_avg_accessory = (last_accessory_avg - first_accessory_avg) / genome_diff;
        double first_last_avg_acces_combi = ((last_accessory_avg + last_unique_avg) - (first_accessory_avg + first_unique_avg)) / genome_diff;
        String output_start = "#Average number of core, accessory, unique groups gained or lost per additional genome based on " 
                + total_loops + " pangenome size iterations.\n"
            + "#The AVERAGE, MEDIAN gain/loss, and STANDARD DEVIATION are shown for each gene category between two pangenome sizes"
            + "\n\nNumber of groups of first (N=3) and last (N=" + adj_total_genomes + ") size -> Average number of genes gained/lost per additional genome"
            + "\nCore: " + df.format(first_core_avg) + " - " + df.format(last_core_avg) + " -> " + df.format(first_last_avg_core)
            + "\nAccessory: " + df.format(first_accessory_avg) + " - " + df.format(last_accessory_avg) + " -> " + df.format(first_last_avg_accessory)
            + "\nUnique: " + df.format(first_unique_avg) + " - " + df.format(last_unique_avg) + " -> " + df.format(first_last_avg_unique)
            + "\nAccessory combined with unique: " + df.format((first_accessory_avg + first_unique_avg)) + " - "
            + df.format((last_accessory_avg + last_unique_avg)) + " -> " + df.format(first_last_avg_acces_combi) + "\n";
       
        write_SB_to_file_in_DB(all_average_builder, "pangenome_size/gene/gains_losses/average_all.csv");
        Pantools.logger.debug("Written average all to {}pangenome_size/gene/gains_losses/average_all.csv", WORKING_DIRECTORY);
        write_SB_to_file_in_DB(all_median_builder, "pangenome_size/gene/gains_losses/median_all.csv");
        Pantools.logger.debug("Written median all to {}pangenome_size/gene/gains_losses/median_all.csv", WORKING_DIRECTORY);
        write_SB_to_file_in_DB(CA_average_builder, "pangenome_size/gene/gains_losses/average_core_accessory.csv");
        Pantools.logger.debug("Written average core accessory to {}pangenome_size/gene/gains_losses/average_core_accessory.csv", WORKING_DIRECTORY);
        write_SB_to_file_in_DB(CA_median_builder, "pangenome_size/gene/gains_losses/median_core_accessory.csv");
        Pantools.logger.debug("Written median core accessory to {}pangenome_size/gene/gains_losses/median_core_accessory.csv", WORKING_DIRECTORY);
        write_string_to_file_in_DB(output_start + output_builder.toString(), "pangenome_size/gene/gains_losses.txt");
        Pantools.logger.debug("Written gains/losses to {}pangenome_size/gene/gains_losses.txt", WORKING_DIRECTORY);

        Pantools.logger.info("Written gains and losses per genome to multiple files in {}pangenome_size/gene/gains_losses/", WORKING_DIRECTORY);
    }
    
    /**
     * 
     * @param values
     * @param values2
     * @return [median, average, standard deviation, shortest, longest, total, count] 
     */
    public double[] get_differences_between_two_sizes(int[] values, int[] values2) { 
        ArrayList<Integer> differences = new ArrayList<>();
        for (int i = 0; i < values.length; i++) {
            differences.add(values2[i]-values[i]);
        }
        double [] median_average_stdev = median_average_stdev_from_AL_int(differences); 
        return median_average_stdev;
    }
    
    /**
     * 
     * @param access_values
     * @param unique_values
     * @return 
     */
    public static int[] combine_unique_accessory_counts(int[] access_values, int[] unique_values ) {
        int[] combi_access = new int [access_values.length];
        for (int i = 0; i < access_values.length; i++) {
            combi_access[i] = access_values[i] + unique_values[i];
        }
        return combi_access;
    }
    
    /**
     * This function will write the core, accessory and unique gene counts for each number of genomes to a file.
     *
     * Creates 
     * - pangenome_size.txt
     * - core_accessory_unique_size.csv
     * - core_dispensable_size.csv
     * - core_dispensable_total_size.csv
     *
     * @param genomeList a list of all genomes
     * @param coreGenesPerGenome a 2D array with the number of core genes per genome (inner array) per loop (outer
     *                           array)
     * @param accessoryGenesPerGenome a 2D array with the number of accessory genes per genome (inner array) per loop
     *                                (outer array)
     * @param uniqueGenesPerGenome a 2D array with the number of unique genes per genome (inner array) per loop (outer
     *                             array)
     */
    public void write_pangenome_size_gene_output(ArrayList<String> genomeList,
                                                 int[][] coreGenesPerGenome,
                                                 int[][] accessoryGenesPerGenome,
                                                 int[][] uniqueGenesPerGenome) {
        Pantools.logger.info("Writing pangenome size gene output.");

        // define output builder
        StringBuilder output_builder = new StringBuilder();

        // All values are added to Set to remove redundancy
        HashSet<String> values_set = new HashSet<>(); // core accessory and unique
        HashSet<String> median_set = new HashSet<>();
        HashSet<String> values_set2 = new HashSet<>(); // core dispensable
        HashSet<String> median_set2 = new HashSet<>();
        HashSet<String> values_set3 = new HashSet<>(); // core dispensable total
        HashSet<String> median_set3 = new HashSet<>();

        // go over every number of genomes (in this case, these are the number of combined genomes)
        for (int genomeNr = 1; genomeNr <= genomeList.size(); genomeNr++) {
            ArrayList<Integer> all_core_list = new ArrayList<>();
            ArrayList<Integer> all_accessory_list = new ArrayList<>();
            ArrayList<Integer> all_unique_list = new ArrayList<>();

            // go over all loops
            for (int loop = 0; loop < coreGenesPerGenome.length; loop++) {
                // add gene counts to the values_set
                values_set.add(genomeNr + "," + coreGenesPerGenome[loop][genomeNr - 1] + ",Core");
                values_set.add(genomeNr + "," + uniqueGenesPerGenome[loop][genomeNr - 1] + ",Unique");
                if (accessoryGenesPerGenome[loop][genomeNr - 1] > 0) {
                    values_set.add(genomeNr + "," + accessoryGenesPerGenome[loop][genomeNr - 1] + ",Accessory");
                }

                // add gene counts to the values_set2
                values_set2.add(genomeNr + "," + coreGenesPerGenome[loop][genomeNr - 1] + ",Core");
                values_set2.add(genomeNr + "," + (accessoryGenesPerGenome[loop][genomeNr - 1] + uniqueGenesPerGenome[loop][genomeNr - 1]) + ",Dispensable");

                // add gene counts to the values_set3
                values_set3.add(genomeNr + "," + coreGenesPerGenome[loop][genomeNr - 1] + ",Core");
                values_set3.add(genomeNr + "," + (accessoryGenesPerGenome[loop][genomeNr - 1] + uniqueGenesPerGenome[loop][genomeNr - 1]) + ",Dispensable");
                values_set3.add(genomeNr + "," + (coreGenesPerGenome[loop][genomeNr - 1] + accessoryGenesPerGenome[loop][genomeNr - 1] + uniqueGenesPerGenome[loop][genomeNr - 1]) + ",Total");


                // add gene counts to the lists
                all_core_list.add(coreGenesPerGenome[loop][genomeNr - 1]);
                all_accessory_list.add(accessoryGenesPerGenome[loop][genomeNr - 1]);
                all_unique_list.add(uniqueGenesPerGenome[loop][genomeNr - 1]);
            }

            // calculate medians
            double [] core_m_a_sd = median_average_stdev_from_AL_int(all_core_list); // [median, average, standard deviation, shortest, longest, total, count]
            double [] accessory_m_a_sd = median_average_stdev_from_AL_int(all_accessory_list);
            double [] unique_m_a_sd = median_average_stdev_from_AL_int(all_unique_list);
            median_set.add(genomeNr + "," + core_m_a_sd[0] + ",Median");
            median_set.add(genomeNr + "," + accessory_m_a_sd[0] + ",Median");
            median_set.add(genomeNr + "," + unique_m_a_sd[0] + ",Median");
            median_set2.add(genomeNr + "," + core_m_a_sd[0] + ",Median");
            median_set2.add(genomeNr + "," + (accessory_m_a_sd[0] + unique_m_a_sd[0]) + ",Median");
            median_set3.add(genomeNr + "," + core_m_a_sd[0] + ",Median");
            median_set3.add(genomeNr + "," + (accessory_m_a_sd[0] + unique_m_a_sd[0]) + ",Median");
            median_set3.add(genomeNr + "," + (core_m_a_sd[0] + accessory_m_a_sd[0] + unique_m_a_sd[0]) + ",Median");

            // build output builder
            output_builder.append("N=").append(genomeNr)
                    .append("\n Core range: ").append(all_core_list.get(0)).append("-").append(all_core_list.get(all_core_list.size()-1))
                    .append("\n Average: ").append(core_m_a_sd[1])
                    .append("\n Median: ").append(core_m_a_sd[0])
                    .append("\n\n Accessory range: ").append(all_accessory_list.get(0)).append("-").append(all_accessory_list.get(all_accessory_list.size()-1))
                    .append("\n Average: ").append(accessory_m_a_sd[1])
                    .append("\n Median: ").append(accessory_m_a_sd[0])
                    .append("\n\n Unique range: ").append(all_unique_list.get(0)).append("-").append(all_unique_list.get(all_unique_list.size()-1))
                    .append("\n Average: ").append(unique_m_a_sd[1])
                    .append("\n Median: ").append(unique_m_a_sd[0]).append("\n\n");
        }

        // add all values to input builders
        StringBuilder input_builder = new StringBuilder("x,y,Category\n"); // core accessory and unique
        StringBuilder input_builder2 = new StringBuilder("x,y,Category\n"); // core dispensable
        StringBuilder input_builder3 = new StringBuilder("x,y,Category\n"); // core dispensable and total
        for (String value : values_set) {
            input_builder.append(value).append("\n");
        }
        for (String value : median_set) {
            input_builder.append(value).append("\n");
        }
        for (String value : values_set2) {
            input_builder2.append(value).append("\n");
        }
        for (String value : median_set2) {
            input_builder2.append(value).append("\n");
        }
        for (String value : values_set3) {
            input_builder3.append(value).append("\n");
        }
        for (String value : median_set3) {
            input_builder3.append(value).append("\n");
        }
       
        write_SB_to_file_in_DB(input_builder, "pangenome_size/gene/core_accessory_unique_size.csv");
        Pantools.logger.debug("Written core accessory unique sizes to {}pangenome_size/gene/core_accessory_unique_size.csv", WORKING_DIRECTORY);
        write_SB_to_file_in_DB(output_builder, "pangenome_size/gene/pangenome_size.txt");
        Pantools.logger.debug("Written pangenome size to {}pangenome_size/gene/pangenome_size.txt", WORKING_DIRECTORY);
        write_SB_to_file_in_DB(input_builder2, "pangenome_size/gene/core_dispensable_size.csv");
        Pantools.logger.debug("Written core dispensable sizes to {}pangenome_size/gene/core_dispensable_size.csv", WORKING_DIRECTORY);
        write_SB_to_file_in_DB(input_builder3, "pangenome_size/gene/core_dispensable_total_size.csv");
        Pantools.logger.debug("Written core dispensable total sizes to {}pangenome_size/gene/core_dispensable_total_size.csv", WORKING_DIRECTORY);

        Pantools.logger.info("Written pangenome size files to {}pangenome_size/gene/", WORKING_DIRECTORY);
    }
    
    /**
     * Check all the labels of the pangenome to see if the given label is valid
     * @return 
     */
    public Label check_if_label_exists() {
        if (SELECTED_LABEL == null) {
            System.out.println("\rNo --label was provided\n");
            System.exit(1);
        }
        Label selected_label = label(SELECTED_LABEL);
        ResourceIterable<Label> all_labels = GRAPH_DB.getAllLabels();
        boolean found_label = false;
        String present_labels = "";
        for (Label label1 : all_labels) {
            if (label1.equals(selected_label)) {
                found_label = true;
                break;
            }
            present_labels += label1 + ",";
        }
        if (!found_label) {
            System.out.println("\rProvided label '" + selected_label + "' does not match with anything in the pangenome\n"
                    + "Present labels: " + present_labels.replaceFirst(".$", "") + "\n");
            System.exit(1);
        }
        return selected_label;
    }
    
    /**
     * Not allowed to remove anything from 'build_pangenome' and 'annotation' nodes
     */
    public void remove_nodes() {
        Pantools.logger.info("Removing nodes from the pangenome.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            total_genomes = (int) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_genomes");
            create_skip_arrays(false, true); // create skip array if --skip/-ref is provided by user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nee ) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        
        if (NODE_ID == null && SELECTED_LABEL == null) {
            System.out.println("\rProvide either --node or --label\n");
            System.exit(1);
        } else if (NODE_ID != null && SELECTED_LABEL == null) { // --node argument was used
            remove_selected_nodes();
        } else if (SELECTED_LABEL != null && NODE_ID == null) { // --label argument was used
            remove_all_nodes_with_label(label(SELECTED_LABEL));
        }
    }
    
    /**
     * Remove a set of nodes from the pangenome that were given by the user via the command line (--node) 
     * Not allowed to remove anything from 'build_pangenome' and 'add_annotations'
     */
    public void remove_selected_nodes() { 
        HashSet<Node> all_nodes = new HashSet<>(); // holds all nodes that will be removed 
        HashSet<Relationship> all_relations = new HashSet<>(); // the relationships belonging to the nodes that will be removed. are removed aswell
        HashSet<Node> mrna_nodes = new HashSet<>(); // check if mRNA nodes have 'protein_ID' to adjust the 'num_proteins' property
        boolean remove = false;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            retrieve_selected_nodes_for_removal(all_nodes, all_relations, mrna_nodes);
            int total_proteins = check_protein_ids(mrna_nodes); 
            Scanner s = new Scanner(System.in);
            Pantools.logger.info("Do you really want to remove these from the database [y/n]?\n-> ");
            String str = s.nextLine().toLowerCase();
            if (str.equals("y") || str.equals("yes")) {
                remove = true;
                if (total_proteins > 0) {
                    Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
                    int num_proteins = (int) pangenome_node.getProperty("num_proteins");
                    pangenome_node.removeProperty("num_proteins"); 
                    pangenome_node.setProperty("num_proteins", (num_proteins-total_proteins));
                }
            } else { // no successfull transaction
                Pantools.logger.info("Did not remove them.");
            }
            tx.success(); // transaction successful, commit changes
        }
        if (remove) {
            delete_nodes_and_relationships(all_nodes, all_relations);
        }
    }
    
    /**
     * Properties for phobius and COG are removed without asking permission
     * @param all_nodes
     * @param all_relations
     * @param mrna_nodes 
     */
    public void retrieve_selected_nodes_for_removal(HashSet<Node> all_nodes, HashSet<Relationship> all_relations, HashSet<Node> mrna_nodes) {
        String[] label_array_no = new String[]{"nucleotide","sequence","pangenome","genome","degenerate","annotation"}; // these cannot be removed 
        String[] remove_properties = new String[]{"phobius","COG"}; // only the functions properties are allowed to be be removed 
        String[] node_str_array = NODE_ID.replace(",,","").replace(" ","").split(",");
        for (String node_str : node_str_array) {
            long node_id = Long.parseLong(node_str);
            Node target_node = GRAPH_DB.getNodeById(node_id);
            Iterable<Label> labels = target_node.getLabels();
            for (Label label1 : labels) {
                String label_str = label1.toString();
                if (ArrayUtils.contains(label_array_no, label_str)) { 
                    Pantools.logger.info("Unable to delete Node {} because it has one of these labels {}.", node_id, Arrays.toString(label_array_no));
                } else if (label_str.equals("mRNA")) {
                    mrna_nodes.add(target_node);
                } else if (ArrayUtils.contains(remove_properties, label_str)) { 
                    remove_cog_or_phobius_properties(target_node, label_str);
                }
            }
            all_nodes.add(target_node);
            Iterable<Relationship> rels = target_node.getRelationships();
            for (Relationship rel : rels) {
                all_relations.add(rel);
            }
        }
        Pantools.logger.info("Found {} nodes with {} relationships.", all_nodes.size(), all_relations.size());
    }
    
    /**
     * Remove some properties related to COG and phobius 
     * @param node
     * @param label_str 
     */
    public void remove_cog_or_phobius_properties(Node node, String label_str) {
        if (label_str.equals("COG")) {
            node.removeProperty("COG_category");
            node.removeProperty("COG_description");
        } else { // phobius
            node.removeProperty("phobius_secreted_protein");
            node.removeProperty("phobius_receptor");
            node.removeProperty("phobius_transmembrane");
        }
    }
    
    /**
     * Count how many of the mRNAs are protein coding 
     * @param mrna_nodes
     * @return 
     */
    public int check_protein_ids(HashSet<Node> mrna_nodes) {
        int counter = 0;
        for (Node mrna_node : mrna_nodes) {
            if (mrna_node.hasProperty("protein_ID")) {
                counter ++;
            }
        }
        return counter;
    }
    
    /**
     * 
     * @param selected_label
     * @param all_nodes
     * @param all_rels 
     */
    public void collect_nodes_relations_matching_label(Label selected_label, HashSet<Node> all_nodes, HashSet<Relationship> all_rels) {
        ResourceIterator<Node> nodes = GRAPH_DB.findNodes(selected_label);
        while (nodes.hasNext()) { 
            Node node = nodes.next();
            Iterable<Relationship> rels = node.getRelationships();
            for (Relationship rel : rels) {
                all_rels.add(rel);
            }
            all_nodes.add(node);
        }
    }

    public void removeFunctions(RemoveFunctions cli) {
        final String mode = cli.getMode().toUpperCase();
        final HashSet<Node> allNodes = new HashSet<>();
        final HashSet<Relationship> allRelationships = new HashSet<>();
        String whatToDo = "do_nothing";
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            switch (mode) {
                case "NODES":
                    collectFunctionNodes(allNodes, allRelationships);
                    whatToDo = "remove_nodes_relations";
                    break;
                case "BGC":
                    Pantools.logger.info("Remove AntiSMASH BGC.");
                    collect_nodes_relations_matching_label(BGC_LABEL, allNodes, allRelationships);
                    whatToDo = "remove_nodes_relations";
                    break;
                case "PROPERTIES":
                    whatToDo = "remove_all_function_properties";
                    break;
                case "COG":
                    whatToDo = "remove_cog_functions";
                    break;
                case "PHOBIUS":
                    whatToDo = "remove_phobius_functions";
                    break;
                case "SIGNALP":
                    whatToDo = "remove_signalp";
                    break;
                case "ALL":
                    collectFunctionNodes(allNodes, allRelationships);
                    whatToDo = "remove_all_functional_annotations";
                    break;
            }
        }
        remove_selected_nodes_from_database(whatToDo, allNodes, allRelationships);
    }

    private void collectFunctionNodes(HashSet<Node> allNodes, HashSet<Relationship> allRelationships) {
        collect_nodes_relations_matching_label(GO_LABEL, allNodes, allRelationships);
        collect_nodes_relations_matching_label(PFAM_LABEL, allNodes, allRelationships);
        collect_nodes_relations_matching_label(TIGRFAM_LABEL, allNodes, allRelationships);
        collect_nodes_relations_matching_label(INTERPRO_LABEL, allNodes, allRelationships);
        collect_nodes_relations_matching_label(BGC_LABEL, allNodes, allRelationships);
    }
    
    /**
     * Collects nodes and relations that match a specific or multiple node labels. These will be removed at the end of the function
     * @param selected_label 
     */
    public void remove_all_nodes_with_label(Label selected_label) {
        final String label_str = selected_label.toString();
        final HashSet<Node> all_nodes = new HashSet<>();
        final HashSet<Relationship> all_relationships = new HashSet<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            check_if_label_exists();
            collect_nodes_relations_matching_label(selected_label, all_nodes, all_relationships);
            tx.success(); // transaction successful, commit changes
        }
        remove_selected_nodes_from_database("remove_nodes_relations", all_nodes, all_relationships);
    }
    
    /**
     * 
     * @param what_to_do
     * @param all_nodes
     * @param all_relations 
     */
    public void remove_selected_nodes_from_database(String what_to_do, HashSet<Node> all_nodes, HashSet<Relationship> all_relations) {
        switch (what_to_do) {
            case "do_nothing":
                // do nothing 
                break;
            case "remove_nodes_relations": 
                delete_nodes_and_relationships(all_nodes, all_relations);
                break;
            case "remove_phobius_functions":
                remove_phobius_properties();
                break;
            case "remove_cog_functions":
                remove_COG_properties();
                break;
            case "remove_signalp":
                remove_signalp_properties();
                break;
            case "remove_all_function_properties":
                remove_phobius_properties();
                remove_COG_properties();
                remove_signalp_properties();
            case "remove_all_functional_annotations":
                remove_COG_properties();
                remove_phobius_properties();
                remove_signalp_properties();
                delete_nodes_and_relationships(all_nodes, all_relations);
                break;
        }
    }
    
    /**
     * Remove the properties related to COG
     */
    public void remove_COG_properties() {
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> nodes = GRAPH_DB.findNodes(COG_LABEL);
            int counter = 0;
            while (nodes.hasNext()) { 
                counter ++;
                if (counter % 10000 == 0) {
                    System.out.print("\rRemoving COG annotations: " + counter + " nodes "); 
                }
                Node mrna_node = nodes.next();
                mrna_node.removeLabel(COG_LABEL);
                mrna_node.removeProperty("COG_id"); // old, can be removed later 
                mrna_node.removeProperty("COG_category"); 
                mrna_node.removeProperty("COG_name"); // old, can be removed later 
                mrna_node.removeProperty("COG_description");
            }
            System.out.println("\rRemoved the COG annotations from " + counter + " nodes "); 
            tx.success(); // transaction successful, commit changes
        }
    }
    
    /**
     * Remove some properties related to Phobius 
     */
    public void remove_phobius_properties() {
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> nodes = GRAPH_DB.findNodes(PHOBIUS_LABEL);
            int counter = 0;
            while (nodes.hasNext()) { 
                counter ++;
                if (counter % 10000 == 0) {
                    System.out.print("\rRemoving Phobius properties: " + counter + " nodes "); 
                }
                Node mrna_node = nodes.next();
                mrna_node.removeLabel(PHOBIUS_LABEL);
                mrna_node.removeProperty("phobius_effector"); // old can be removed later
                mrna_node.removeProperty("phobius_secreted_protein"); // old can be removed later
                mrna_node.removeProperty("phobius_receptor"); // old can be removed later
                mrna_node.removeProperty("phobius_transmembrane");
                mrna_node.removeProperty("phobius_signal_peptide");
            }
            System.out.println("\rRemoved the Phobius annotations from " + counter + " nodes "); 
            tx.success(); // transaction successful, commit changes
        }
    }
    
    /**
     * Remove some properties related to signalp
    */
    public void remove_signalp_properties() {
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> nodes = GRAPH_DB.findNodes(SIGNALP_LABEL);
            int counter = 0;
            while (nodes.hasNext()) { 
                counter ++;
                Node mrna_node = nodes.next();
                mrna_node.removeLabel(SIGNALP_LABEL);
                mrna_node.removeProperty("signal_signal_peptide"); 
            }
            System.out.println("\rRemoved the SignalP annotations from " + counter + " nodes "); 
            tx.success(); // transaction successful, commit changes
        }
    }
    
    /**
     * Removes nodes and relationships. Commits a transaction every 10k changes.
     * @param all_nodes
     * @param all_relations 
     */
    public static void delete_nodes_and_relationships(HashSet<Node> all_nodes, HashSet<Relationship> all_relations) {
        MAX_TRANSACTION_SIZE = 10000;
        long one_procent_rel = (all_relations.size()+100)/100;
        long one_procent_node = (all_nodes.size()+100)/100;
        int trsc = 0; 
        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        try {
            double rel_counter = 0, node_counter = 0;
            for (Relationship relation : all_relations) {
                rel_counter ++;
                if (rel_counter % 10000 == 0) {
                    String percentage = String.format("%.2f", rel_counter/one_procent_rel);
                    System.out.print("\rRemoving relationships " + (int) rel_counter +"/" + all_relations.size() + " (" + percentage + "%)       "); // spaces are intentional
                }
                relation.delete();
                trsc ++;
                if (trsc >= MAX_TRANSACTION_SIZE) {
                    tx.success();
                    tx.close();
                    trsc = 0;
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                }
            }
            System.out.println("\rRemoving relationships " + (int) rel_counter + "/" + all_relations.size() + " (100%)        "); // spaces are intentional
           
            for (Node node : all_nodes) {
                node_counter ++;
                if (node_counter % 10000 == 0) {
                    String percentage = String.format("%.2f", node_counter/one_procent_node);
                    System.out.print("\rRemoving nodes " + (int) node_counter + "/" + all_nodes.size() + " (" + percentage + "%)      "); // spaces are intentional
                } 
                node.delete();
                trsc ++;
                if (trsc >= MAX_TRANSACTION_SIZE) {
                    tx.success();
                    tx.close();
                    trsc = 0;
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                }
            }
            System.out.println("\rRemoving nodes " + (int) node_counter + "/" + all_nodes.size() + " (100%)       "); // spaces are intentional
            tx.success();  
        } finally {
            tx.close();
        }
    }
     
    /*
    Requires 
      --database-path/-dp 
    
    Optional 
      --core-threshold
      --unique-threshold
      --phenotype/-ph
      ---mode 
    */
    public void kmer_classification2() {
        compressed_kmers = false;
        long highest_frequency = 0;
        Pantools.logger.info("Calculating CORE, UNIQUE, and ACCESSORY k-mer sequences.");
        //report_number_of_threads(true, true); // prints how many threads were selected by user
        //report_number_of_threads(true, false); //do not print how many threads were given, only when more than 1 thread was selected
       
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "kmer_classification"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if --skip/--reference is provided by user
            KMinOne = K_SIZE -1; 
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        
        if (Mode.equals("0")) {
            System.out.println("\rNo --mode compressed has been selected, counting all k-mers in compressed k-mers."
                + " K-mer size is " + K_SIZE + ", extracting " + KMinOne + " of every compressed k-mer");
        } else if (Mode.contains("COMPRESSED")) {
            Pantools.logger.info("--mode {} is selected. Considering a compressed k-mer as a single k-mer.", Mode);
            compressed_kmers = true;  
        } else if (Mode.contains("OCCURRENCE")) {
         
        } else {
            Pantools.logger.info("--mode {} is not recognized.", Mode);
            return;
        }
       
        delete_directory_in_DB("kmer_classification/temp");
        delete_directory_in_DB("kmer_classification/kmer_identifiers");
        create_directory_in_DB("kmer_classification/distances_for_tree/");
        create_directory_in_DB("kmer_classification/kmer_identifiers");
        create_directory_in_DB("kmer_classification/temp");
        
        ConcurrentHashMap<Integer, long[]> kmer_freq_map = new ConcurrentHashMap<>(); // key is genome number, long[] with total, core, access, unique, distinct total, distinct core, distinct access, distinct uni
        ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map = new ConcurrentHashMap<>(); // key is phenotype 
        ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map = new ConcurrentHashMap<>(); // key is frequency of kmer. This can become quite large, written to log every 250k keys
        ConcurrentHashMap<Integer, HashMap<Long, Long>> freq_per_genome = new ConcurrentHashMap<>(); // key is genome. second key is the frequency. 
        long[] kmer_class_count = new long[3]; // distinct: [core, accessory, unique]
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ArrayList<String> genome_list = create_genome_list(false); // create a list of all genomes in the database, needed for compatibility with --variants in other functions

            if (PHENOTYPE == null) {
                System.out.println("\rNo --phenotype was provided. Unable to find phenotype shared/specific k-mers.");
            } else {
                retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            }
            retrieve_selected_sequences();

            if (compare_sequences && compare_genomes) {
                System.out.println("\rBoth the--sequence and --genome arguments were included. Please select one.");
                System.exit(1);
            } else if (compare_sequences) {
               System.out.println("\r--sequence is selected. Number of sequences in analysis: " + selected_sequences.size());
               if (selected_sequences.size() > 50000) {
                   Pantools.logger.error("Can only analyze up to 50.000 sequences. Please make a selection using the --skip argument.");
                   System.exit(1);
               }
            }
            
            core_unique_thresholds_for_classification(genome_list, "k-mers"); // set 'core_threshold' and 'unique_threshold' variables
            if (!Mode.contains("OCCURRENCE")) {
                kmer_occur_map = null;
                freq_per_genome = null;
            }
          
            for (int i=1; i <= total_genomes; i++) {    
                if (Mode.contains("OCCURRENCE")) {
                    HashMap<Long, Long> empty_map = new HashMap<>();
                    freq_per_genome.put(i, empty_map);              
                }
                long[] empty_array = new long[8]; // total, core, access, unique, distinct total, distinct core, distinct access, distinct uni 
                kmer_freq_map.put(i, empty_array);
            }       
            tx.success(); // transaction successful, commit changes
        }
       
        if (compare_sequences && THREADS < 5) {
            THREADS = 5;
        } else if (!compare_sequences) {
            THREADS = 1;
        }
       
        HashMap<String, long[]> degen_node_per_seq_genome = gather_degen_nodes_per_seq();     
        long[] node_count = gather_kmer_information(kmer_occur_map, kmer_freq_map, pheno_kmer_map, freq_per_genome, kmer_class_count);
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Pantools.logger.info("Generating output files.");
            create_kmer_occurrence_output(kmer_occur_map, highest_frequency, freq_per_genome);
            create_kmer_classification_overview2(kmer_freq_map, node_count, kmer_class_count, degen_node_per_seq_genome);
            kmer_classi_pheno_overview(pheno_kmer_map);
            tx.success();
        }
       
        combine_core_kmer_output_files();
        combine_accessory_kmer_output_files();
        combine_unique_kmer_output_files();
        create_shared_kmers_between_genomes();
        create_shared_kmers_between_sequences();
        create_kmer_distance_rscript2(WD_full_path + "kmer_classification/");
        delete_directory_in_DB("kmer_classification/temp");
        print_kmer_classification_output2();
    }
    
    /**
     * Number of shared k-mers between sequences
     */
    public static void create_shared_kmers_between_sequences() {
        if (!compare_sequences) {
            return;
        } 
        int write_every_sequence = 1000;
        if (selected_sequences.size() > 999) {
            write_every_sequence = 500;
        } 
        if (selected_sequences.size() >= 10000) { // 10k genomes 
            write_every_sequence = 100;
        }

        String output_path = WD_full_path + "kmer_classification/distances_for_tree/";
        StringBuilder mash_distance = new StringBuilder();
        StringBuilder all_kmer_distance = new StringBuilder();
        StringBuilder distinct_kmer_distance = new StringBuilder();
        StringBuilder all_kmer_shared = new StringBuilder();
        StringBuilder distinct_kmer_shared = new StringBuilder();
        StringBuilder kmer_count_distinct_total = new StringBuilder();
        StringBuilder kmer_count_all_total = new StringBuilder();

        StringBuilder genome_i_total_shared_builder = new StringBuilder();
        StringBuilder genome_j_total_shared_builder = new StringBuilder();
        StringBuilder genome_i_distinct_shared_builder = new StringBuilder();
        StringBuilder genome_j_distinct_shared_builder = new StringBuilder();
        
        StringBuilder header_builder2 = new StringBuilder("Sequences,");
        for (int i = 0; i < selected_sequences.size(); i ++) { 
            String seq_id = selected_sequences.get(i);        
            String add = "", add2 = "";
            if (phased) {
                add = " (" + subgenome_nr_map.get(seq_id + "_phasing_id") + ")";
                add2 = "_" + subgenome_nr_map.get(seq_id + "_phasing_id");
            }
            
            //header_builder.append(seq_id).append(add).append(",");
            header_builder2.append(seq_id).append(add2);
            if (i != selected_sequences.size()-1) {
                header_builder2.append(",");
            }
        } 
        String header2 = header_builder2.toString() + "\n";
        long pairwise_counter = 0; 
        int sequence_counter = 0;
        for (int i = 0; i < selected_sequences.size(); i ++) {
            String seq_id = selected_sequences.get(i);     
            String add = "", add2 = "";
            if (phased) {
                add = " (" + subgenome_nr_map.get(seq_id + "_phasing_id") + ")";
                add2 = "_" + subgenome_nr_map.get(seq_id + "_phasing_id");
            }
            mash_distance.append(seq_id).append(add2).append(",");
            all_kmer_distance.append(seq_id).append(add2).append(",");
            distinct_kmer_distance.append(seq_id).append(add2).append(",");
            all_kmer_shared.append(seq_id).append(",");
            distinct_kmer_shared.append(seq_id).append(",");
            kmer_count_distinct_total.append(seq_id).append(",");
            kmer_count_all_total.append(seq_id).append(",");

            genome_i_total_shared_builder.append(seq_id).append(",");
            genome_j_total_shared_builder.append(seq_id).append(",");
            genome_i_distinct_shared_builder.append(seq_id).append(",");
            genome_j_distinct_shared_builder.append(seq_id).append(",");
            sequence_counter ++;
            if (sequence_counter % 100 == 0 ) {
                System.out.print("\rWriting shared k-mers between sequences " + (i+1));
            } 
            for (int j = 0; j < selected_sequences.size(); j ++) {
                pairwise_counter ++; 
                int all_shared_kmers = seq_all_shared_kmers[i][j];
                int all_total_kmers = seq_all_total_kmers[i][j];
                int distinct_shared_kmers = seq_distinct_shared_kmers[i][j];
                int distinct_total_kmers = seq_distinct_total_kmers[i][j];
                if (j < i) { 
                    all_shared_kmers = seq_all_shared_kmers[j][i];
                    all_total_kmers = seq_all_total_kmers[j][i];
                    distinct_shared_kmers = seq_distinct_shared_kmers[j][i];
                    distinct_total_kmers = seq_distinct_total_kmers[j][i];
                }
                double percentage_distinct_shared = divide(distinct_shared_kmers, distinct_total_kmers); // distinct 
                double percentage_all_shared = divide(all_shared_kmers, all_total_kmers); // all kmers 
                if (i == j) {
                    percentage_distinct_shared = 1;
                    percentage_all_shared = 1;
                }
                if (percentage_distinct_shared == 0) {
                    mash_distance.append("1");   
                } else {
                    double mash_dist = -1.0/ K_SIZE * Math.log(percentage_distinct_shared); // 𝑑=−1/𝑘 * ln(𝑤/𝑡) where k is 17; w/t is the jaccard index.   
                    mash_distance.append(Math.abs(mash_dist));  
                }        
                distinct_kmer_distance.append((1 - percentage_distinct_shared));  
                all_kmer_distance.append((1 - percentage_all_shared));  
                all_kmer_shared.append(all_shared_kmers);
                distinct_kmer_shared.append(distinct_shared_kmers);
                kmer_count_all_total.append(all_total_kmers);
                kmer_count_distinct_total.append(distinct_total_kmers);
                if (j != selected_sequences.size()-1) {
                    mash_distance.append(",");
                    distinct_kmer_distance.append(",");
                    all_kmer_distance.append(",");
                    all_kmer_shared .append(",");
                    distinct_kmer_shared.append(",");
                    kmer_count_all_total.append(",");
                    kmer_count_distinct_total.append(",");
                }      
            }
            mash_distance.append("\n");
            all_kmer_distance.append("\n");
            distinct_kmer_distance.append("\n");
            all_kmer_shared.append("\n");
            distinct_kmer_shared.append("\n");
            kmer_count_all_total.append("\n");
            kmer_count_distinct_total.append("\n");

            genome_i_total_shared_builder.append("\n");
            genome_j_total_shared_builder.append("\n");
            genome_i_distinct_shared_builder.append("\n");
            genome_j_distinct_shared_builder.append("\n");
            
            boolean reset_builders = false;
            if (sequence_counter == write_every_sequence) { // first time writing output     
                write_string_to_file_in_DB(header2 + all_kmer_distance.toString(), "kmer_classification/distances_for_tree/sequence_distance_all_kmers.csv");
                write_string_to_file_in_DB(header2 + distinct_kmer_distance.toString(), "kmer_classification/distances_for_tree/sequence_distance_distinct_kmers.csv"); 
                write_string_to_file_in_DB(header2 + mash_distance.toString(), "kmer_classification/distances_for_tree/sequence_mash_distance.csv");        
                
                write_string_to_file_in_DB( "\n#SHARED distinct k-mers\n" + distinct_kmer_shared.toString(), "kmer_classification/temp/distinct_shared_kmers.csv");
                write_string_to_file_in_DB("\n#SHARED (all) k-mers\n" + all_kmer_shared.toString(), "kmer_classification/temp/all_shared_kmers.csv");
                write_string_to_file_in_DB("\n#TOTAL distinct k-mers\n" + kmer_count_distinct_total.toString(), "kmer_classification/temp/distinct_total_kmers.csv");
                write_string_to_file_in_DB("\n#TOTAL all k-mers\n" + kmer_count_all_total.toString(), "kmer_classification/temp/all_total_kmers.csv");
                reset_builders = true;
            } else if (sequence_counter % write_every_sequence == 0) {        
                append_SB_to_file_in_DB(all_kmer_distance, "kmer_classification/distances_for_tree/sequence_distance_all_kmers.csv");
                append_SB_to_file_in_DB(distinct_kmer_distance, "kmer_classification/distances_for_tree/sequence_distance_distinct_kmers.csv"); 
                append_SB_to_file_in_DB(mash_distance, "kmer_classification/distances_for_tree/sequence_mash_distance.csv");  

                append_SB_to_file_in_DB(distinct_kmer_shared, "kmer_classification/temp/distinct_shared_kmers.csv");
                append_SB_to_file_in_DB(all_kmer_shared, "kmer_classification/temp/all_shared_kmers.csv");
                append_SB_to_file_in_DB(kmer_count_distinct_total, "kmer_classification/temp/distinct_total_kmers.csv");
                append_SB_to_file_in_DB(kmer_count_all_total, "kmer_classification/temp/all_total_kmers.csv");
                reset_builders = true;
            }
            if (reset_builders) {
                all_kmer_distance.setLength(0);
                distinct_kmer_distance.setLength(0);
                mash_distance.setLength(0);         
                kmer_count_distinct_total.setLength(0);
                kmer_count_all_total.setLength(0);
                all_kmer_shared.setLength(0);
                distinct_kmer_shared.setLength(0);
            }
        }
       
        if (sequence_counter < write_every_sequence) { // files were not written yet 
            write_string_to_file_full_path(header2 + mash_distance.toString(), output_path + "sequence_mash_distance.csv");
            write_string_to_file_full_path(header2 + all_kmer_distance.toString(), output_path + "sequence_distance_all_kmers.csv");
            write_string_to_file_full_path(header2 + distinct_kmer_distance.toString(), output_path + "sequence_distance_distinct_kmers.csv");
            write_string_to_file_in_DB("#This file contains four matrices: TOTAL distinct k-mers (1), TOTAL all k-mers (2),"
                + " SHARED distinct k-mers (3) and SHARED all k-mers (4).\n"
                + "\n#TOTAL distinct k-mers\n" + header2 + kmer_count_distinct_total.toString()
                + "\n#TOTAL all k-mers\n" + header2 + kmer_count_all_total.toString()
                + "\n#SHARED distinct k-mers\n" + header2 + distinct_kmer_shared.toString()
                + "\n#SHARED (all) k-mers\n" + header2 + all_kmer_shared.toString()
                , "kmer_classification/shared_kmers_between_sequences.csv");
           
        } else {
            append_SB_to_file_in_DB(all_kmer_distance, "kmer_classification/distances_for_tree/sequence_distance_all_kmers.csv");
            append_SB_to_file_in_DB(distinct_kmer_distance, "kmer_classification/distances_for_tree/sequence_distance_distinct_kmers.csv"); 
            append_SB_to_file_in_DB(mash_distance, "kmer_classification/distances_for_tree/sequence_mash_distance.csv");  
            
            copy_file(WORKING_DIRECTORY + "kmer_classification/temp/distinct_total_kmers.csv",  
                    WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_sequences.csv");
            append_file_to_other_file(WORKING_DIRECTORY + "kmer_classification/temp/all_total_kmers.csv",
                    WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_sequences.csv");
            append_file_to_other_file(WORKING_DIRECTORY + "kmer_classification/temp/distinct_shared_kmers.csv",
                    WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_sequences.csv");
            append_file_to_other_file(WORKING_DIRECTORY + "kmer_classification/temp/all_shared_kmers.csv",
                    WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_sequences.csv");
        }
        System.out.print("\r                                                      ");
        // free some memory
        seq_distinct_total_kmers = null;
        seq_distinct_shared_kmers = null ;
        seq_all_total_kmers = null;
        seq_all_shared_kmers = null;
    }
    
     /**
     * Loop over all nucleotide nodes of the pangenome 
     * @param kmer_occur_map is null except when --mode OCCURENCE is included
     * @param kmer_freq_map
     * @param pheno_kmer_map
     * @param freq_per_genome is null except when --mode OCCURENCE is included
     * @param kmer_class_count
     * 
     * @return 
     */
    public long[] gather_kmer_information(ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map,
        ConcurrentHashMap<Integer, long[]> kmer_freq_map, ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map, 
        ConcurrentHashMap<Integer, HashMap<Long,Long>> freq_per_genome, 
        long[] kmer_class_count) { 
        
        distinct_node_count = new AtomicLong(0);
        total_node_count = new AtomicLong(0);
        atomic_counter1 = new AtomicLong(0);
        atomic_counter2 = new AtomicLong(0); 
        frequency_queue = new LinkedBlockingQueue<>();
        string_queue = new LinkedBlockingQueue<>();
        node_queue = new LinkedBlockingQueue<>();
        hashmap_ni_queue = new LinkedBlockingQueue<>();

        if (compare_sequences) {
            seq_distinct_total_kmers = new int[selected_sequences.size()][selected_sequences.size()];
            seq_distinct_shared_kmers = new int[selected_sequences.size()][selected_sequences.size()];
            seq_all_total_kmers = new int[selected_sequences.size()][selected_sequences.size()];
            seq_all_shared_kmers = new int[selected_sequences.size()][selected_sequences.size()];
        }

        genome_distinct_total_kmers = new int[total_genomes][total_genomes];
        genome_distinct_shared_kmers = new int[total_genomes][total_genomes];
        genome_all_total_kmers = new int[total_genomes][total_genomes];
        genome_all_shared_kmers = new int[total_genomes][total_genomes];

        try {
            ExecutorService es = Executors.newFixedThreadPool(THREADS);       
            es.execute(new find_shared_kmers2(kmer_occur_map, kmer_freq_map, pheno_kmer_map, freq_per_genome, kmer_class_count));
            if (compare_sequences) {           
                es.execute(new add_freq_per_seq(1));
                for (int i = 1; i <= THREADS-2; i++) {
                    es.execute(new count_kmers_per_sequence_class(i));
                }          
            }
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            
        }
        long[] node_count_array = new long[2];
        node_count_array[0] = distinct_node_count.get(); // distinct 
        node_count_array[1] = total_node_count.get(); // total 
        return node_count_array;
    }
    
    /**
     * Is only executed when --sequence argument is included 
     */
    public class count_kmers_per_sequence_class implements Runnable {
        int thread_nr;
        int[][] distinct_shared_array = new int[selected_sequences.size()][selected_sequences.size()];
        int[][] distinct_total_array = new int[selected_sequences.size()][selected_sequences.size()];
        int[][] all_shared_array = new int[selected_sequences.size()][selected_sequences.size()];
        int[][] all_total_array = new int[selected_sequences.size()][selected_sequences.size()];
        int[] freq_array = new int[selected_sequences.size()];

        public count_kmers_per_sequence_class(int thread_nr) {        
            this.thread_nr = thread_nr;
        }
        
        public void run() {
            long nr_of_kmers = determine_appropriate_nr_for_printing();
            HashMap<String, Integer> seq_nr_map = create_seq_nr_map();
            
            if (!compare_sequences) {
                return;
            }
            HashMap<Node, Integer> hashmap = new HashMap<>();
            try {
               hashmap = hashmap_ni_queue.take();
            } catch(InterruptedException e) {
                // do nothing (this never happens)            
            }
            long num_nodes = 0, one_procent = 0, local_counter = 0;
            Transaction tx = GRAPH_DB.beginTx(); // start database transaction
            try {
                num_nodes = (long) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_nodes");
                one_procent = (num_nodes+100)/100;
                Map.Entry<Node, Integer> entry = hashmap.entrySet().stream().findFirst().get();
                Node nuc_node = entry.getKey();
                atomic_counter1.getAndIncrement();
                int actual_kmers = entry.getValue();
                long last_printed = 0;
                while (actual_kmers != -1) {
                    if (thread_nr == 1 && (atomic_counter1.get() > last_printed || atomic_counter1.get() < 1000)) {
                        double nodes_d = (double) atomic_counter1.get();
                        percentage_counter = String.format("%.2f", nodes_d / one_procent);
                        System.out.print("\rGathering k-mer nodes: " + atomic_counter1.get() + "/" + num_nodes + " (" + percentage_counter + "%) ");
                        last_printed += nr_of_kmers;
                    }
                    
                    HashMap<String, Integer> kmer_count_per_seq = get_kmers_per_sequence(nuc_node);
                    
                    // second version with array, only able to count distinct kmers 
                    increase_shared_kmers_per_seq2(kmer_count_per_seq, actual_kmers, seq_nr_map, nuc_node, distinct_shared_array, distinct_total_array, freq_array, all_shared_array, all_total_array); 
                    local_counter ++;
                    try {
                        hashmap = hashmap_ni_queue.take();
                    } catch(InterruptedException e) {
                        // do nothing (this never happens)
                    }
                   
                    entry = hashmap.entrySet().stream().findFirst().get();
                    nuc_node = entry.getKey(); // retrieve new node and number of k-mers for next loop
                    atomic_counter1.getAndIncrement();
                    actual_kmers = entry.getValue();
                    if (local_counter % 1000 == 0) {
                        local_counter = 0;
                        tx.success();
                        tx.close();
                        tx = GRAPH_DB.beginTx(); // start a new database transaction
                    }
                }
                tx.success();
            } finally {
                tx.close();
            }  
           
            int [] empty = new int[] {-1};
            frequency_queue.add(empty); // -1 in the array will make the "add_freq_per_seq" function stop
            if (thread_nr == 1) {
                Pantools.logger.trace("ending of 1 {} ", frequency_queue.size());
                double nodes_d = (double) atomic_counter1.get();
                percentage_counter = String.format("%.2f", nodes_d/one_procent);
                System.out.println("\rGathering k-mer nodes: " + num_nodes + "/" + num_nodes + " (100%)    ");
                while (frequency_queue.size() > 1000) {
                    System.out.print("\rAdding frequencies to the k-mer nodes: " + frequency_queue.size() + "                   ");
                    try {
                        Thread.sleep(1000); // 1 second
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
            } 
            fill_shared_kmer_array_sequence(distinct_shared_array, distinct_total_array, all_shared_array, all_total_array, thread_nr);
        }
              
        /**
        
        */
        public long determine_appropriate_nr_for_printing() {
            long nr_for_printing = 10000;
            if (selected_sequences.size() >= 1000) {
                nr_for_printing = 1000;
            }
            if (selected_sequences.size() >= 10000) {
                nr_for_printing = 100;
            }
            return nr_for_printing;
        }
    
         /**
         * 
         * @param kmer_count_per_seq
         * @param actual_kmers
         * @param seq_nr_map
         * @param nuc_node
         * @param distinct_shared_array
         * @param distinct_total_array
         * @param freq_array
         * @param all_shared_array
         * @param all_total_array 
         */
        public void increase_shared_kmers_per_seq2(HashMap<String, Integer> kmer_count_per_seq,
            int actual_kmers, HashMap<String, Integer> seq_nr_map, Node nuc_node, int[][] distinct_shared_array, int[][] distinct_total_array, 
            int[] freq_array, int[][] all_shared_array, int[][] all_total_array) {
             
            ArrayList<Integer> positions = new ArrayList<>();
            for (String seq_id : kmer_count_per_seq.keySet()) {
                int position = seq_nr_map.get(seq_id);      
                positions.add(position);
                freq_array[position] = kmer_count_per_seq.get(seq_id);
            }
            
            ArrayList<Integer> missing_positions = new ArrayList<>();     
            for (int i = 0; i < selected_sequences.size(); i++) {    
                if (!positions.contains(i)) {
                    missing_positions.add(i);
                }
            }

            for (int i = 0; i < positions.size(); i++) {
                for (int j = i; j < positions.size(); j++) {
                    int i_pos = positions.get(i);
                    int j_pos = positions.get(j);  
                    if (j_pos < i_pos) {
                        i_pos = positions.get(j);
                        j_pos = positions.get(i);  
                    }
                    int highest = freq_array[i_pos];
                    int lowest = freq_array[j_pos];
                    if (lowest > highest) {
                        highest = freq_array[j_pos];    
                        lowest = freq_array[i_pos]; 
                    } 
                    if (compressed_kmers) {
                        distinct_shared_array[i_pos][j_pos] ++;
                        distinct_total_array[i_pos][j_pos] ++;
                        all_shared_array[i_pos][j_pos] += lowest;
                        all_total_array[i_pos][j_pos] += highest; 
                    } else {
                        distinct_shared_array[i_pos][j_pos] += actual_kmers;
                        distinct_total_array[i_pos][j_pos] += actual_kmers;
                        all_shared_array[i_pos][j_pos] += (lowest * actual_kmers);
                        all_total_array[i_pos][j_pos] += (highest * actual_kmers); 
                    }
                }
                    
                //  increase the total number of k-mers for sequences that are not present 
                for (int j = 0; j < missing_positions.size(); j++) {
                    int i_pos = positions.get(i);
                    int j_pos = missing_positions.get(j);
                    int i_freq = freq_array[i_pos]; 
                    if (j_pos < i_pos) {
                        i_pos = missing_positions.get(j);
                        j_pos = positions.get(i);
                    }
                   
                    if (compressed_kmers) {
                        distinct_total_array[i_pos][j_pos] ++;        
                        all_total_array[i_pos][j_pos] += i_freq;   
                    } else {
                        distinct_total_array[i_pos][j_pos] += actual_kmers;
                        all_total_array[i_pos][j_pos] += (i_freq * actual_kmers);
                    }             
                }  
            } 
        }    
    }

    /**
     * synchronized 
     * @param distinct_shared_array
     * @param distinct_total_array
     * @param all_shared_array
     * @param all_total_array
     * @param thread 
     */
    public synchronized void fill_shared_kmer_array_sequence(int[][] distinct_shared_array, int[][] distinct_total_array, 
            int[][] all_shared_array, int[][] all_total_array, int thread) {

        for (int i=0; i < distinct_shared_array.length; i++) { 
            for (int j=0; j < distinct_shared_array.length; j++) {    
                seq_distinct_shared_kmers[i][j] += distinct_shared_array[i][j];
                seq_distinct_total_kmers[i][j] += distinct_total_array[i][j];
            }
        }

        for (int i=0; i < all_shared_array.length; i++) { 
            for (int j=0; j < all_shared_array.length; j++) {    
                seq_all_total_kmers[i][j] += all_total_array[i][j];
                seq_all_shared_kmers[i][j] += all_shared_array[i][j];
            }
        }
    }
    
    /**
     * Synchronized 
     * @param distinct_shared_array
     * @param distinct_total_array
     * @param all_shared_array
     * @param all_total_array
     * @param thread 
     */
    public synchronized void fill_shared_kmer_array_genome(int[][] distinct_shared_array, int[][] distinct_total_array, 
            int[][] all_shared_array, int[][] all_total_array, int thread) {

     
        for (int i=0; i < distinct_shared_array.length; i++) { 
            for (int j=0; j < distinct_shared_array.length; j++) {    
                genome_distinct_shared_kmers[i][j] += distinct_shared_array[i][j];
                genome_distinct_total_kmers[i][j] += distinct_total_array[i][j];
            }
        }

        for (int i=0; i < all_shared_array.length; i++) { 
            for (int j=0; j < all_shared_array.length; j++) {    
                genome_all_total_kmers[i][j] += all_total_array[i][j];
                genome_all_shared_kmers[i][j] += all_shared_array[i][j];
            }
        }
    }
    
    public static HashMap<String, Integer> create_seq_nr_map() {
            HashMap<String, Integer> seq_nr_map = new HashMap<>();
            int counter = 0;
            for (String seq_id : selected_sequences) {
                seq_nr_map.put(seq_id, counter);
                counter ++;
            }
        return seq_nr_map;
    }
    
    /**
     * 
     */
    public class find_shared_kmers2 implements Runnable {   
        ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map; // is null except when --mode OCCURRENCE is included
        ConcurrentHashMap<Integer, long[]> kmer_freq_map;
        ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map; 
        ConcurrentHashMap<Integer, HashMap<Long,Long>> freq_per_genome; // is null except when --mode OCCURRENCE is included
        long[] kmer_class_count; 
        int[][] distinct_shared_array = new int[total_genomes][total_genomes];
        int[][] distinct_total_array = new int[total_genomes][total_genomes];
        int[][] all_shared_array = new int[total_genomes][total_genomes];
        int[][] all_total_array = new int[total_genomes][total_genomes];

        public find_shared_kmers2(ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map, ConcurrentHashMap<Integer, long[]> kmer_freq_map, 
               ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map, ConcurrentHashMap<Integer, HashMap<Long,Long>> freq_per_genome, long[] kmer_class_count) {
            this.kmer_occur_map = kmer_occur_map;
            this.kmer_freq_map = kmer_freq_map;
            this.pheno_kmer_map = pheno_kmer_map;
            this.freq_per_genome = freq_per_genome;
            this.kmer_class_count = kmer_class_count;
        }
         
        public void run() {
            HashMap<Integer, ArrayList<String>> kmer_genome_presence = new HashMap<>();
            HashMap<Integer, ArrayList<String>> unique_kmers = new HashMap<>();
            long node_counter = 0;          
            Transaction tx = GRAPH_DB.beginTx(); // start database transaction
            int kmer_print_nr = get_number_of_kmers_for_printing();
            try {
                long num_nodes = (long) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_nodes");
                long one_procent = (num_nodes+100)/100;
                ResourceIterator<Node> nuc_nodes = GRAPH_DB.findNodes(NUCLEOTIDE_LABEL);
               
                while (nuc_nodes.hasNext()) {
                    node_counter ++;
                    if (!compare_sequences && (node_counter % kmer_print_nr == 0 || node_counter == num_nodes || node_counter < 1001)) {
                        double nodes_d = (double) node_counter;
                        percentage_counter = String.format("%.2f", nodes_d/one_procent);
                        System.out.print("\rGathering k-mer nodes: " + node_counter + "/" + num_nodes + " (" + percentage_counter + "%)    ");
                    }
                    
                    if (node_counter % 1000000 == 0 && node_counter > 1) { // 1 million
                        tx.success();
                        tx.close();
                        tx = GRAPH_DB.beginTx(); // start a new database transactionn
                        nuc_nodes = GRAPH_DB.findNodes(NUCLEOTIDE_LABEL);
                        long skip_node_counter = 0;
                        while (nuc_nodes.hasNext()) {
                            skip_node_counter ++;
                            Node nuc_node = nuc_nodes.next();
                            if (skip_node_counter == node_counter-1) {
                                break;
                            }
                        }
                        write_core_kmers_to_file(kmer_genome_presence);
                        write_accessory_kmers_to_file(kmer_genome_presence);
                        write_unique_kmers_to_file(unique_kmers);
                    }
                   
                    while (compare_sequences && hashmap_ni_queue.size() > 50000) {
                        try {
                            Thread.sleep(100); // 100 milliseconds 
                        } catch (InterruptedException ex) {
                            Thread.currentThread().interrupt();
                        }
                    }
                        
                    Node nuc_node = nuc_nodes.next();
                    HashMap<String, Integer> pheno_count_map = new HashMap<>();
                    if (nuc_node.hasLabel(DEGENERATE_LABEL)) {
                        if (compare_sequences) { 
                            // no degenerate nodes are given to the other function, count them here already
                            atomic_counter1.getAndIncrement();
                        }
                        continue;
                    }
                   
                    //int[] freq_array = get_kmer_frequencies_property(nuc_node);
                    long[] freq_array = (long[]) nuc_node.getProperty("frequencies");
                    int kmer_frequency = get_kmer_frequency(nuc_node);
                    int kmer_size = (int) nuc_node.getProperty("length");
                    int actual_kmers = kmer_size - KMinOne;
                    Pantools.logger.trace("{} {} {}", nuc_node, kmer_size, actual_kmers + Arrays.toString(freq_array));
            
                    if (compressed_kmers) {
                        distinct_node_count.getAndIncrement(); // node_count1 is distinct
                    } else {
                        distinct_node_count.addAndGet(actual_kmers);
                    }
                    increase_kmer_occur_map(kmer_occur_map, kmer_frequency, nuc_node, 1);
                   
                    boolean core = true, accessory = false, unique = false;
                    int counter = 0; 
                    for (int i=1; i <= freq_array.length-1; i++) {
                        if (skip_array[i-1]) { 
                            continue;
                        }
               
                        long frequency = freq_array[i];
                        if (compressed_kmers) {
                            total_node_count.getAndIncrement(); // node_count2 is total
                        } else {
                            total_node_count.addAndGet((frequency * actual_kmers));
                        }
                
                        if (frequency == 0) {
                            core = false;
                            continue;
                        }
                        increase_freq_per_genome_map(freq_per_genome, i, frequency);
                        counter ++; 
                        if (PHENOTYPE != null) {
                            String pheno = geno_pheno_map.get(i);
                            try_incr_hashmap(pheno_count_map, pheno, 1); // pheno_count_map is not shared by other threads
                        }
                    }
                    if (compare_sequences) {
                        HashMap<Node, Integer> map = new HashMap<>();
                        map.put(nuc_node, actual_kmers);
                        hashmap_ni_queue.add(map);
                        //continue;
                    }
                    try_incr_AL_hashmap(kmer_genome_presence, counter, nuc_node.getId() +"");
                    if (counter >= core_threshold) { // core
                        if (compressed_kmers) {
                            kmer_class_count[0] ++; // kmer_count is distinct
                        } else { 
                            kmer_class_count[0] += actual_kmers;
                        }
                    } else if (counter <= unique_threshold) { // unique 
                        unique = true;
                        if (compressed_kmers) {
                            kmer_class_count[2] ++; 
                        } else {
                            kmer_class_count[2] += actual_kmers;
                        } 
                    } else { // accessory
                        if (compressed_kmers) {
                            kmer_class_count[1] ++;
                        } else {
                            kmer_class_count[1] += actual_kmers;
                        }
                        accessory = true;
                    }         
                    increase_shared_kmers_per_genome(actual_kmers, distinct_shared_array, distinct_total_array, freq_array, all_shared_array, all_total_array);
                    determine_kmer_pheno_shared_specific(nuc_node, pheno_count_map, pheno_kmer_map);    
                    for (int i = 1; i < freq_array.length; i++) {
                        if (skip_array[i-1]) { 
                            continue;
                        }
                        // hashmap replaced by twodimensional array
                        //increase_shared_kmers_map(freq_array, i, shared_kmers_map, actual_kmers, unique); // synchronized function
                        if (freq_array[i] == 0) {
                            continue;
                        }
                        increase_kmer_freq_map(kmer_freq_map, i, core, accessory, freq_array, nuc_node, actual_kmers);   
                        if (unique) {
                            try_incr_AL_hashmap(unique_kmers, i, nuc_node.getId()+""); 
                        }
                    }
                }
                           
                if (!compare_sequences) {                  
                    double nodes_d = (double) node_counter;
                    percentage_counter = String.format("%.2f", nodes_d/one_procent);
                    System.out.print("\rGathering k-mer nodes: " + node_counter + "/" + num_nodes + " (" + percentage_counter + "%)");
                } else {
                    for(int i = 1; i <= THREADS-2; i++) {
                        HashMap<Node, Integer> map = new HashMap<>();
                        Node nuc_node = GRAPH_DB.getNodeById(0);
                        map.put(nuc_node, -1);
                        hashmap_ni_queue.add(map);
                    }
                }
                tx.success();
            } finally {
                tx.close();
            }  
            write_core_kmers_to_file(kmer_genome_presence);
            write_accessory_kmers_to_file(kmer_genome_presence);
            write_unique_kmers_to_file(unique_kmers);
            fill_shared_kmer_array_genome(distinct_shared_array, distinct_total_array, all_shared_array, all_total_array, 0); 
        }
        
        /**
        
        */
        public int get_number_of_kmers_for_printing() {
            int nr_kmers = 10000;
            if (total_genomes > 100 ) {
                nr_kmers = 5000;
            }
            if (total_genomes > 1000 ) {
                nr_kmers = 1000;
            }
            if (total_genomes >= 10000 ) {
                nr_kmers = 100;
            }
            return nr_kmers;
        }
        
        public void write_unique_kmers_to_file(HashMap<Integer, ArrayList<String>> unique_kmers) { 
            for (int i=1; i <= total_genomes; i++) {
                if (!unique_kmers.containsKey(i)) {
                    continue;
                }
                ArrayList<String> nodes = unique_kmers.get(i);
                String node_str = nodes.toString().replace("[", "").replace("]", "").replace(" ", "");
                boolean exists = check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/temp_unique_kmers_" + i + ".csv");    
                if (!exists) {                              
                    write_string_to_file_in_DB("#Genome " + i + "\n" + node_str, "kmer_classification/temp_unique_kmers_" + i + ".csv");
                } else {
                    appendStringToFileFullPath(node_str, WORKING_DIRECTORY + "kmer_classification/temp_unique_kmers_" + i + ".csv");
                }  
                unique_kmers.remove(i);
            }
        }
        
        /**
         * 
         * @param kmer_genome_presence 
         */
        public void write_accessory_kmers_to_file(HashMap<Integer, ArrayList<String>> kmer_genome_presence) {
            for (int i= adj_total_genomes; i > 1; i--) {
                if (!kmer_genome_presence.containsKey(i)) {
                    continue;
                }               
                ArrayList<String> nodes = kmer_genome_presence.get(i);  
                kmer_genome_presence.remove(i);
                String node_str = nodes.toString().replace("[", "").replace("]", "").replace(" ", "");
                boolean exists = check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/temp/accessory_kmers_" + i + ".csv");    
                if (!exists) {
                    String percentage = get_percentage_str(i, adj_total_genomes, 1);                  
                    write_string_to_file_in_DB("#" + i + " genomes " + percentage + "%\n" + node_str , 
                                "kmer_classification/temp/accessory_kmers_" + i + ".csv");
                } else {
                    appendStringToFileFullPath(node_str,
                                WORKING_DIRECTORY + "kmer_classification/temp/accessory_kmers_" + i + ".csv");
                }           
            }
        }
 
        public void write_core_kmers_to_file(HashMap<Integer, ArrayList<String>> kmer_genome_presence) {
            long file_nr = atomic_counter2.getAndIncrement();
            ArrayList<String> nodes = kmer_genome_presence.get(core_threshold);  
            if (nodes == null) {
                return;
            }
            kmer_genome_presence.remove(core_threshold);
            String node_str = nodes.toString().replace("[", "").replace("]", "").replace(" ", "");
            write_string_to_file_in_DB(node_str, "kmer_classification/temp_core_kmers_" + file_nr + ".csv");
        }
    }
   
    /**
     * 
     * @param actual_kmers
     * @param distinct_shared_array
     * @param distinct_total_array
     * @param freq_array
     * @param all_shared_array
     * @param all_total_array 
     */
    public void increase_shared_kmers_per_genome(int actual_kmers, int[][] distinct_shared_array, int[][] distinct_total_array, 
            long[] freq_array, int[][] all_shared_array, int[][] all_total_array) {
             
        for (int i = 1; i < freq_array.length; i++) {
            for (int j = i; j < freq_array.length; j++) {
                int highest = (int) freq_array[i];
                int lowest = (int) freq_array[j];
                if (lowest > highest) {
                    highest = (int) freq_array[j];    
                    lowest = (int) freq_array[i]; 
                } 
                if (highest == 0) {
                    continue;
                }
                if (compressed_kmers) {
                    distinct_shared_array[i-1][j-1] ++;
                    distinct_total_array[i-1][j-1] ++;
                    all_shared_array[i-1][j-1] += lowest;
                    all_total_array[i-1][j-1] += highest; 
                } else {
                    if (lowest > 0) {
                        distinct_shared_array[i-1][j-1] += actual_kmers;
                    }
                    distinct_total_array[i-1][j-1] += actual_kmers;
                    all_shared_array[i-1][j-1] += (lowest * actual_kmers);              
                    all_total_array[i-1][j-1] += (highest * actual_kmers); 
                }
            }     
        } 
    }   

    public synchronized void increase_kmer_freq_map(ConcurrentHashMap<Integer, long[]> kmer_freq_map, int i, boolean core, boolean accessory, 
            long[] freq_array, Node nuc_node, int actual_kmers) {
        
        long[] current_freq = kmer_freq_map.get(i); // total, core, access, unique, distinct total, distinct core, distinct access, distinct uni
        int pos1 = 3, pos2 = 7; // unique positions in array
        if (core) {
            pos1 = 1;
            pos2 = 5;
        } else if (accessory) {
            pos1 = 2;
            pos2 = 6;
        } else {
           
        }
        if (compressed_kmers) { 
            current_freq[0] += freq_array[i];
            current_freq[4] += 1;
            current_freq[pos1] += freq_array[i];
            current_freq[pos2] += 1;
        } else {
            current_freq[0] += freq_array[i] * actual_kmers;
            current_freq[4] += actual_kmers;
            current_freq[pos1] += (freq_array[i] * actual_kmers);
            current_freq[pos2] += actual_kmers;
        }
        kmer_freq_map.put(i, current_freq);
    }
    
    /**
     * Synchronized
     * @param freq_per_genome
     * @param genome_nr
     * @param frequency 
     */
    public synchronized void increase_freq_per_genome_map(ConcurrentHashMap<Integer, HashMap<Long, Long>> freq_per_genome, int genome_nr, long frequency) {
        if (freq_per_genome == null) {
            return;
        }
        HashMap<Long,Long> freq_map = freq_per_genome.get(genome_nr);
        try_incr_hashmap(freq_map, frequency, 1L);
        freq_per_genome.put(genome_nr, freq_map);
    }
    
    /**
     * 
     * @param kmer_occur_map
     * @param kmer_frequency
     * @param nuc_node
     * @param thread_nr 
     */
    public synchronized void increase_kmer_occur_map(ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map,
                                                     long kmer_frequency, Node nuc_node, int thread_nr) {
        if (kmer_occur_map == null) {
            return;
        }
        try_incr_AL_chashmap(kmer_occur_map, kmer_frequency, nuc_node);
        if (thread_nr == 1) {
            if (kmer_occur_map.size() > 250000) { // prevents the hasmap from becoming too large
                write_kmer_occurrence_output(kmer_occur_map);
            }
        }
    }
    
    /**
     * 
     * @param nuc_node
     * @return 
     */
    public HashMap<String, Integer> get_kmers_per_sequence(Node nuc_node) {
        HashMap<String, Integer> kmer_count_per_seq = new HashMap<>();
        boolean frequencies_present = false;
        int genomes_present = 0;
        for (int i = 1; i <= total_genomes; i++) { // i is a genome number 
            //nuc_node.removeProperty("freq_per_seq_genome_" + i);
            if (nuc_node.hasProperty("freq_per_seq_genome_" + i)) { 
                frequencies_present = true; // if one is present, the frequency was added for all genomes
                if (skip_array[i-1]) {
                    continue;
                }
                int[] freq_of_seq = (int[]) nuc_node.getProperty("freq_per_seq_genome_" + i); 
                ArrayList<Integer> selected_sequences = sequences_per_genome.get(i);
                if (selected_sequences == null) {
                    continue;
                }
                for (int j : selected_sequences) { // j is a sequence number  
                    if (freq_of_seq[j-1] == 0) {
                        continue;
                    }
                    if (skip_seq_array[i-1][j-1]) {
                        continue;
                    }                  
                    if (freq_of_seq[j-1] == 0 || skip_seq_array[i-1][j-1]) {
                        continue;
                    }
                    try_incr_hashmap(kmer_count_per_seq, i + "_" + j, freq_of_seq[j-1]);
                }
               
            } else {
               // do nothing 
            }
        }
        if (frequencies_present) { // the frequencies were already stored on the relationships 
            return kmer_count_per_seq;
        }
        // frequencies are not known yet. need to be calculated now 
        kmer_count_per_seq = new HashMap<>();   
        int[][] freq_per_seq = new int[total_genomes][0];
        for (int i = 1; i <= total_genomes; i++) {
            ArrayList<Integer> sequences = sequences_per_genome.get(i);
            //if (number_of_sequences == null) {
            //    continue;
            //}
            freq_per_seq[i-1] = new int[sequences.size()];
        }
        
        // add all sequences, even when skipped 
        Iterable<Relationship> relations_incoming = nuc_node.getRelationships(Direction.INCOMING, RelTypes.FF, RelTypes.FR, RelTypes.RR, RelTypes.RF);
        for (Relationship relation : relations_incoming) {
            Iterable<String> all_keys = relation.getPropertyKeys();
            for (String key : all_keys) { // do not skip anything here so the array is always complete
                int[] rels = (int[]) relation.getProperty(key);
                key = key.replace("G","").replace("S", "_");
                String[] key_array = key.split("_"); 
                int genome_nr = Integer.parseInt(key_array[0]);
                int sequence_nr = Integer.parseInt(key_array[1]);
                freq_per_seq[genome_nr-1][sequence_nr-1] += rels.length;
                if (!skip_seq_array[genome_nr-1][sequence_nr-1]) {
                    try_incr_hashmap(kmer_count_per_seq, key, rels.length);
                }
            }        
        }               
        add_frequency_to_queue(nuc_node, freq_per_seq); //synchronized function
        return kmer_count_per_seq;
    }  
    
    public synchronized void add_frequency_to_queue(Node nuc_node, int[][] freq_per_seq) {
        if (frequency_queue.size() > 50000) {
            return;
            /*
            if (FAST) { // do not add every frequency because it slows down the calculations
                return;
            }
            try {
                Thread.sleep(1000); // 1 second
            } catch (InterruptedException ex) {
               Thread.currentThread().interrupt();
            }*/
        }
        for (int i = 1; i <= total_genomes; i++) {
            Pantools.logger.trace("add to queue {} freq_per_seq_genome_{} {}.", nuc_node, i, Arrays.toString(freq_per_seq[i-1]));
            frequency_queue.add(freq_per_seq[i-1]);  
            string_queue.add("freq_per_seq_genome_" + i);
            node_queue.add(nuc_node);     
        }
     }
    
    /**
      7 Stringbuilders. Writes the output every X genomes to a file to prevent going out of memory
    */
    public void create_shared_kmers_between_genomes() {
        int write_every_genome = 1000;
        if (adj_total_genomes > 999) {
            write_every_genome = 500;
        } 
        if (adj_total_genomes >= 10000) { // 10k genomes 
            write_every_genome = 100;
        }
       
        StringBuilder all_kmer_distance = new StringBuilder();
        StringBuilder distinct_kmer_distance = new StringBuilder();
        StringBuilder mash_distance = new StringBuilder();
       
        StringBuilder kmer_count_all_total = new StringBuilder();
        StringBuilder kmer_count_distinct_total = new StringBuilder();
        
        StringBuilder kmer_count_all_sh = new StringBuilder(); // shared number of kmers 
        StringBuilder kmer_count_distinct_sh = new StringBuilder(); // shared number of kmers 

        ArrayList<Integer> selectedgenomes = new ArrayList<>();   // to later determine what is the last genome in analysis
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            selectedgenomes.add(i);
        }

        StringBuilder header = new StringBuilder("Genomes,");
        for (int i : selectedgenomes) {
            String phenotype = get_phenotype_for_genome(i, true);
            header.append(i).append(phenotype);
            if (i != selectedgenomes.get(selectedgenomes.size()-1)) {
                header.append(",");
            }
        }
        String header_str = header.toString() + "\n";
        int genome_counter = 0;
        for (int i : selectedgenomes) {
            genome_counter++;
            if (genome_counter % 100 == 0 ) {
                System.out.print("\rWriting shared k-mers between genomes " + i);
            }
            String phenotype = get_phenotype_for_genome(i, true);
            all_kmer_distance.append(i).append(phenotype).append(",");
            distinct_kmer_distance.append(i).append(phenotype).append(",");
            mash_distance.append(i).append(phenotype).append(",");
            
            kmer_count_all_sh.append(i).append(phenotype).append(","); 
            kmer_count_distinct_sh.append(i).append(phenotype).append(",");
            
            kmer_count_all_total.append(i).append(phenotype).append(",");
            kmer_count_distinct_total.append(i).append(phenotype).append(",");

            for (int j : selectedgenomes) {
                int all_shared_kmers = genome_all_shared_kmers[i-1][j-1];
                int all_total_kmers = genome_all_total_kmers[i-1][j-1];
                int distinct_shared_kmers = genome_distinct_shared_kmers[i-1][j-1];
                int distinct_total_kmers = genome_distinct_total_kmers[i-1][j-1];
                if (j < i) { 
                    all_shared_kmers = genome_all_shared_kmers[j-1][i-1];
                    all_total_kmers = genome_all_total_kmers[j-1][i-1];
                    distinct_shared_kmers = genome_distinct_shared_kmers[j-1][i-1];
                    distinct_total_kmers = genome_distinct_total_kmers[j-1][i-1];
                }
                
                double percentage_distinct_shared = divide(distinct_shared_kmers, distinct_total_kmers); // distinct 
                double percentage_all_shared = divide(all_shared_kmers, all_total_kmers); // all kmers 
                if (percentage_distinct_shared == 0) {
                    mash_distance.append("1");
                } else {
                    double mash_dist = -1.0/ K_SIZE * Math.log(percentage_distinct_shared); // 𝑑=−1/𝑘 * ln(𝑤/𝑡) where k is 17; w/t is the jaccard index.
                    mash_distance.append(Math.abs(mash_dist));
                }
                
                all_kmer_distance.append((1-percentage_all_shared));
                distinct_kmer_distance.append((1-percentage_distinct_shared));
                
                kmer_count_all_sh.append(all_shared_kmers);
                kmer_count_distinct_sh.append(distinct_shared_kmers); 
                
                kmer_count_all_total.append(all_total_kmers);
                kmer_count_distinct_total.append(distinct_total_kmers); 
                if (j != selectedgenomes.get(selectedgenomes.size()-1)) { // not the last genome
                    mash_distance.append(",");
                    all_kmer_distance.append(",");
                    distinct_kmer_distance.append(",");
                    kmer_count_all_sh.append(",");
                    kmer_count_distinct_sh.append(","); 
                    kmer_count_all_total.append(",");
                    kmer_count_distinct_total.append(","); 
                }  
            }
            all_kmer_distance.append("\n");
            distinct_kmer_distance.append("\n");
            mash_distance.append("\n");
            
            kmer_count_all_sh.append("\n");
            kmer_count_distinct_sh.append("\n");
            
            kmer_count_all_total.append("\n");
            kmer_count_distinct_total.append("\n");
            boolean reset_builders = false;
            if (genome_counter == write_every_genome) { // first time writing output     
                write_string_to_file_in_DB(header_str + all_kmer_distance, "kmer_classification/distances_for_tree/genome_distance_all_kmers.csv");
                write_string_to_file_in_DB(header_str + distinct_kmer_distance, "kmer_classification/distances_for_tree/genome_distance_distinct_kmers.csv");
                write_string_to_file_in_DB(header_str + mash_distance, "kmer_classification/distances_for_tree/genome_mash_distance.csv");

                write_string_to_file_in_DB("\n#SHARED distinct k-mers\n" + header_str + kmer_count_distinct_sh.toString(), "kmer_classification/temp/g_distinct_shared_kmers.csv");
                write_string_to_file_in_DB("\n#SHARED (all) k-mers\n" + header_str + kmer_count_all_sh.toString() ,"kmer_classification/temp/g_all_shared_kmers.csv");
                write_string_to_file_in_DB("#K-mers between two genomes\n\n#TOTAL distinct k-mers\n" + header_str + kmer_count_distinct_total.toString(), 
                    "kmer_classification/temp/g_distinct_total_kmers.csv");
                write_string_to_file_in_DB("\n#TOTAL all k-mers\n" + header_str + kmer_count_all_total.toString() , "kmer_classification/temp/g_all_total_kmers.csv");
                reset_builders = true;
            } else if (genome_counter % write_every_genome == 0) {        
                append_SB_to_file_in_DB(all_kmer_distance, "kmer_classification/distances_for_tree/genome_distance_all_kmers.csv");
                append_SB_to_file_in_DB(distinct_kmer_distance, "kmer_classification/distances_for_tree/genome_distance_distinct_kmers.csv"); 
                append_SB_to_file_in_DB(mash_distance, "kmer_classification/distances_for_tree/genome_mash_distance.csv");  
                append_SB_to_file_in_DB(kmer_count_distinct_total, "kmer_classification/temp/g_distinct_total_kmers.csv");
                append_SB_to_file_in_DB(kmer_count_all_total, "kmer_classification/temp/g_all_total_kmers.csv");
                append_SB_to_file_in_DB(kmer_count_distinct_sh, "kmer_classification/temp/g_distinct_shared_kmers.csv");
                append_SB_to_file_in_DB(kmer_count_all_sh, "kmer_classification/temp/g_all_shared_kmers.csv");
                reset_builders = true;
            }
            if (reset_builders) {
                all_kmer_distance.setLength(0);
                distinct_kmer_distance.setLength(0);
                mash_distance.setLength(0);         
                kmer_count_distinct_total.setLength(0);
                kmer_count_all_total.setLength(0);
                kmer_count_distinct_sh.setLength(0);
                kmer_count_all_sh.setLength(0);
            }
        }
        
        if (genome_counter < write_every_genome) { // files were not written yet 
            write_string_to_file_in_DB(header_str + all_kmer_distance.toString(), "kmer_classification/distances_for_tree/genome_distance_all_kmers.csv");
            write_string_to_file_in_DB(header_str + distinct_kmer_distance.toString(), "kmer_classification/distances_for_tree/genome_distance_distinct_kmers.csv"); 
            write_string_to_file_in_DB(header_str + mash_distance.toString(), "kmer_classification/distances_for_tree/genome_mash_distance.csv");
            StringBuilder shared_kmer_output = new StringBuilder();
            shared_kmer_output.append("#TOTAL k-mers between two genomes\n")
                .append("#TOTAL distinct k-mers\n")
                .append(header_str).append(kmer_count_distinct_total.toString()).append("\n")
                .append("\n#TOTAL all k-mers\n")
                .append(header_str).append(kmer_count_all_total.toString()).append("\n")
                .append("#SHARED\n")
                .append("#SHARED distinct k-mers\n")
                .append(header_str).append(kmer_count_distinct_sh.toString()).append("\n")
                .append("#SHARED all k-mers\n")
                .append(header_str).append(kmer_count_all_sh.toString());
            write_SB_to_file_in_DB(shared_kmer_output, "kmer_classification/shared_kmers_between_genomes.csv");
        } else {
            Pantools.logger.info("header2 {}.", header_str  );
            append_SB_to_file_in_DB(all_kmer_distance, "kmer_classification/distances_for_tree/genome_distance_all_kmers.csv");
            append_SB_to_file_in_DB(distinct_kmer_distance, "kmer_classification/distances_for_tree/genome_distance_distinct_kmers.csv"); 
            append_SB_to_file_in_DB(mash_distance, "kmer_classification/distances_for_tree/genome_mash_distance.csv");  
            
            copy_file(WORKING_DIRECTORY + "kmer_classification/temp/g_distinct_total_kmers.csv",  
                    WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_genomes.csv");
            append_file_to_other_file(WORKING_DIRECTORY + "kmer_classification/temp/g_all_total_kmers.csv",
                    WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_genomes.csv");
            append_file_to_other_file(WORKING_DIRECTORY + "kmer_classification/temp/g_distinct_shared_kmers.csv",
                    WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_genomes.csv");
            append_file_to_other_file(WORKING_DIRECTORY + "kmer_classification/temp/g_all_shared_kmers.csv",
                    WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_genomes.csv");
        }
        // free some memory
        genome_distinct_total_kmers = null;
        genome_distinct_shared_kmers = null ;
        genome_all_total_kmers = null;
        genome_all_shared_kmers = null;
    }
    
    public void combine_core_kmer_output_files() {
        long total_files = atomic_counter2.get();
        StringBuilder script = new StringBuilder();
        appendStringToFileFullPath("\n", WORKING_DIRECTORY + "kmer_classification/temp_core_kmers_" + total_files+ ".csv");
        for (int i=0; i <= total_files; i++) {
            script.append("cat ").append(WORKING_DIRECTORY).append("kmer_classification/temp_core_kmers_").append(i).append(".csv >> ") 
                   .append(WORKING_DIRECTORY).append("kmer_classification/kmer_identifiers/core_kmers.csv\n"); 
        }
        write_SB_to_file_full_path(script, "script.sh");
        String[] command = new String[]{"/bin/sh", "script.sh"};
        ExecCommand.ExecCommand(command);
        delete_file_full_path("script.sh");
        for (int i=0; i <= total_files; i++) {
            delete_file_in_DB("kmer_classification/temp_core_kmers_" + i + ".csv");
        }  
    }
    
    /**
     * Creates unique_kmers.csv, the node identifiers of unique k-mers ordered by genome.
     */
    public void combine_unique_kmer_output_files() {
        if (adj_total_genomes == 1) {
            write_string_to_file_in_DB("No unique k-mers are found with only one genome is selected", "kmer_classification/kmer_identifiers/unique_kmers.csv");
            return;
        }
        StringBuilder script = new StringBuilder();
        for (int i=1; i <= total_genomes; i++) {
            boolean exists = check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/temp_unique_kmers_" + i + ".csv");  
            if (!exists) {
                continue;
            }
            appendStringToFileFullPath("\n", WORKING_DIRECTORY + "kmer_classification/temp_unique_kmers_" + i + ".csv");
            script.append("cat ").append(WORKING_DIRECTORY).append("kmer_classification/temp_unique_kmers_").append(i).append(".csv >> ") 
                   .append(WORKING_DIRECTORY).append("kmer_classification/kmer_identifiers/unique_kmers.csv\n"); 
        }
        write_SB_to_file_full_path(script, "script.sh");
        String[] command = new String[]{"/bin/sh", "script.sh"};
        ExecCommand.ExecCommand(command);
        delete_file_full_path("script.sh");
        for (int i=1; i <= total_genomes; i++) {
            delete_file_in_DB("kmer_classification/temp_unique_kmers_" + i + ".csv");
        }
    }
    
    /**
     *  Creates accessory_kmers.csv, 
     */
    public void combine_accessory_kmer_output_files() {
        if (adj_total_genomes == 1) {
            write_string_to_file_in_DB("No accessory k-mers are found with only one genome is selected", "kmer_classification/kmer_identifiers/accessory_kmers.csv");
            return;
        }
        StringBuilder script = new StringBuilder();
        for (int i= adj_total_genomes; i > 1; i--) {
            boolean exists = check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/temp/accessory_kmers_" + i + ".csv");    
            if (!exists) {
                continue;
            }
            appendStringToFileFullPath("\n", WORKING_DIRECTORY + "kmer_classification/temp/accessory_kmers_" + i + ".csv");
            script.append("cat ").append(WORKING_DIRECTORY).append("kmer_classification/temp/accessory_kmers_").append(i).append(".csv >> ") 
                   .append(WORKING_DIRECTORY).append("kmer_classification/kmer_identifiers/accessory_kmers.csv\n"); 
        }
        write_SB_to_file_full_path(script, "script.sh");
        String[] command = new String[]{"/bin/sh", "script.sh"};
        ExecCommand.ExecCommand(command);
       
        delete_file_full_path("script.sh");
        for (int i= adj_total_genomes; i > 1; i--) {
            delete_file_in_DB("kmer_classification/temp/accessory_kmers_" + i + ".csv");
        }
    }
    
    /**
     * Creates kmer_classification_overview.txt
     * @param kmer_freq_map
     * @param node_count
     * @param kmer_class_count
     * @param degen_node_per_seq_genome
     */
    public void create_kmer_classification_overview2(ConcurrentHashMap<Integer, long[]> kmer_freq_map, long[] node_count, 
            long[] kmer_class_count, HashMap<String, long[]> degen_node_per_seq_genome) {  
        
        StringBuilder output_builder = new StringBuilder();
        StringBuilder csv_builder = new StringBuilder("Genome,Total number of k-mers,Core k-mers,% of genome core k-mers,Accessory k-mers,"
                + "% of genome accessory k-mers,Unique k-mers,% of genome unique k-mers,Degenerate k-mers,% of genome degenerate k-mers,"
                + "Number of distinct k-mers,Distinct core k-mers,% of genome distinct core k-mers,Distinct accessory k-mers,% of genome distinct accessory k-mers,"
                + "Distinct unique k-mers,% of genome distinct unique k-mers,Distinct degenerate k-mers,% of genome distinct degenerate k-mers\n");
        
        long total_kmers = kmer_class_count[0] + kmer_class_count[1] + kmer_class_count[2];
        String core_pc = get_percentage_str(kmer_class_count[0], total_kmers, 2);
        String accessory_pc = get_percentage_str(kmer_class_count[1], total_kmers, 2);
        String unique_pc = get_percentage_str(kmer_class_count[2], total_kmers, 2);
        long[] degen_counts = degen_node_per_seq_genome.get("nodes_in_pangenome"); // [nodes, distinct kmers, total kmers]
        if (degen_counts == null) {
             degen_counts = new long[3]; // is required because the degenerate node counting is currently skipped
        }
        
        if (!compressed_kmers) {
            output_builder.append("Uncompressed the k-mers. K-mer size is ").append(K_SIZE).append(", extracting ").append(KMinOne)
                    .append(" of every compressed k-mer\n");
        } else {
            output_builder.append("Counted compressed k-mers. Each k-mer node is counted once\n");
        }
        output_builder.append("To obtain the distinct k-mer counts, the frequency of k-mers was ignored\n\n");
        output_builder.append("Total k-mers: ").append(node_count[1])
                .append("\nTotal degenerate k-mers: ").append(degen_counts[2])
                .append("\n\nDistinct k-mers: ").append(node_count[0]) 
                .append("\nDistinct degenerate k-mers: ").append(degen_counts[1]) 
                .append("\nCore: ").append(kmer_class_count[0]).append(" (").append(core_pc).append("%)")
                .append("\nAccessory: ").append(kmer_class_count[1]).append(" (").append(accessory_pc).append("%)")
                .append("\nUnique: ").append(kmer_class_count[2]).append(" (").append(unique_pc).append("%)").append("\n\n");
             
        for (int i=1; i <= total_genomes; i++) {
            if (skip_array[i-1]) { 
                continue;
            }
            long[] value_array = kmer_freq_map.get(i);
            String percentage1 = get_percentage_str(value_array[1], value_array[0], 2);
            String percentage2 = get_percentage_str(value_array[2], value_array[0], 2);
            String percentage3 = get_percentage_str(value_array[3], value_array[0], 2);
            String percentage5 = get_percentage_str(value_array[5], value_array[4], 2);
            String percentage6 = get_percentage_str(value_array[6], value_array[4], 2);
            String percentage7 = get_percentage_str(value_array[7], value_array[4], 2);
            
            long[] genome_degen_counts = new long [3]; // [nodes, distinct kmers, total kmers]
            if (degen_node_per_seq_genome.containsKey("genome_" + i)) {
                genome_degen_counts = degen_node_per_seq_genome.get("genome_" + i); 
            }
            String percentage8 = get_percentage_str(genome_degen_counts[1], value_array[0], 2);
            String percentage9 = get_percentage_str(genome_degen_counts[2], value_array[4], 2);
            output_builder.append("Genome ").append(i)
                    .append("\nAll k-mers: ").append(value_array[0])
                    .append("\nDegenerate k-mers: ").append(genome_degen_counts[1]).append(", ").append(percentage8).append("% of all k-mers")
                    .append("\nCore: ").append(value_array[1]).append(", ").append(percentage1)
                    .append("%\nAccessory: ").append(value_array[2]).append(", ").append(percentage2)
                    .append("%\nUnique: ").append(value_array[3]).append(", ").append(percentage3).append("%\n\n")
                    .append("Distinct k-mers: ").append(value_array[4])
                    .append("\nDistinct degenerate k-mers: ").append(genome_degen_counts[2]).append(", ").append(percentage9).append("% of distinct k-mers")
                    .append("\nCore: ").append(value_array[5]).append(", ").append(percentage5)
                    .append("%\nAccessory: ").append(value_array[6]).append(", ").append(percentage6)
                    .append("%\nUnique: ").append(value_array[7]).append(", ").append(percentage7).append("%\n\n");
            
            csv_builder.append(i).append(",").append(value_array[0]).append(",").append(value_array[1]).append(",").append(percentage1)
                .append(",").append(value_array[2]).append(",").append(percentage2).append(",").append(value_array[3]).append(",").append(percentage3).append(",")
                .append(genome_degen_counts[1]).append(",").append(percentage8).append(",").append(value_array[4]).append(",").append(value_array[5]).append(",")
                .append(percentage5).append(",").append(value_array[6]).append(",").append(percentage6).append(",").append(value_array[7]).append(",")
                .append(percentage7).append(",").append(genome_degen_counts[2]).append(",").append(percentage9).append("\n");         
       }
       write_SB_to_file_in_DB(output_builder, "kmer_classification/kmer_classification_overview.txt"); 
       write_SB_to_file_in_DB(csv_builder, "kmer_classification/kmer_classification_overview.csv"); 
    }
    
    /**
     * Print which files are created depending on input arguments
     */
    public void print_kmer_classification_output2() {
        String dir = WORKING_DIRECTORY + "kmer_classification/";

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}kmer_classification_overview.txt", dir);
        Pantools.logger.info(" {}kmer_classification_overview.csv", dir);
        if (PHENOTYPE != null) {
            Pantools.logger.info(" {}kmer_classification_phenotype_overview.txt", dir);
        }
        Pantools.logger.info(" {}shared_kmers_between_genomes.csv", dir);
        if (Mode.contains("OCCURRENCE")) {
            Pantools.logger.info(" {}kmer_occurrence.txt", dir);
        }
        if (NODE_VALUE != null) {
            Pantools.logger.info(" {}highly_occuring_kmers.txt", dir);
        }
        if (compare_sequences) {
            Pantools.logger.info(" {}shared_kmers_between_sequences.csv", dir);
            Pantools.logger.info(" {}sequence_kmer_distance_tree.R (Select one of the three distances)", dir);
        }
        Pantools.logger.info(" {}genome_kmer_distance_tree.R (Select one of the three distances)", dir);
        Pantools.logger.info("Files with 'nucleotide' node identifiers:");
        Pantools.logger.info(" {}kmer_identifiers/core_kmers.csv", dir);
        Pantools.logger.info(" {}kmer_identifiers/accessory_kmers.csv", dir);
        Pantools.logger.info(" {}kmer_identifiers/unique_kmers.csv", dir);
        if (PHENOTYPE != null) {
            Pantools.logger.info(" {}kmer_identifiers/phenotype_shared_kmers.csv", dir);
            Pantools.logger.info(" {}kmer_identifiers/phenotype_specific_kmers.csv", dir);
            Pantools.logger.info(" {}kmer_identifiers/phenotype_exclusive_kmers.csv", dir);
        }
    }

    /**
     * k-mer distance NJ tree
     * @param output_path 
     */
    public void create_kmer_distance_rscript2(String output_path) {
        String R_LIB = check_r_libraries_environment();
        StringBuilder rscript = new StringBuilder ();
        rscript.append("#! /usr/bin/env RScript\n\n")
                .append("# Use one of the following files\n")
                .append("# ").append(WD_full_path).append("kmer_classification/distances_for_tree/genome_mash_distance.csv\n")
                .append("# ").append(WD_full_path).append("kmer_classification/distances_for_tree/genome_distance_distinct_kmers.csv\n")
                .append("# ").append(WD_full_path).append("kmer_classification/distances_for_tree/genome_distance_all_kmers.csv\n\n")
                .append("\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"ape\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(ape)\n")
                .append("\n")
                .append("input = read.csv(\"").append(WD_full_path).append("kmer_classification/distances_for_tree/genome_mash_distance.csv\", sep=\",\",header = TRUE)\n")
                .append("df2 = subset(input, select = -c(Genomes))\n")
                .append("df.dist2 =as.matrix(df2, labels=TRUE)\n")
                .append("colnames(df.dist2) <- rownames(df.dist2) <- input[['Genomes']]\n")
                .append("NJ_tree <- nj(df.dist2)\n")
                .append("pdf(NULL)\n")
                .append("plot(NJ_tree, main = \"Neighbor Joining\")\n")
                .append("write.tree(NJ_tree, tree.names = TRUE, file=\"").append(output_path).append("genome_kmer_distance.tree\")\n")
                .append("cat(\"\\nK-mer distance tree written to: ").append(output_path).append("genome_kmer_distance.tree\\n\\n\")");
        write_SB_to_file_full_path(rscript, output_path + "genome_kmer_distance_tree.R");
        
        if (!compare_sequences) {
            return;
        }
        rscript = new StringBuilder();
        rscript.append("#! /usr/bin/env RScript\n\n")
                .append("# Use one of the following files\n")
                .append("# ").append(WD_full_path).append("kmer_classification/distances_for_tree/sequence_mash_distance.csv\n")
                .append("# ").append(WD_full_path).append("kmer_classification/distances_for_tree/sequence_distance_distinct_kmers.csv\n")
                .append("# ").append(WD_full_path).append("kmer_classification/distances_for_tree/sequence_distance_all_kmers.csv\n")
                .append("\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"ape\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(ape)\n")
                .append("\n")
                .append("input = read.csv(\"").append(WD_full_path).append("kmer_classification/distances_for_tree/sequence_mash_distance.csv\", sep=\",\",header = TRUE)\n")
                .append("df2 = subset(input, select = -c(Sequences))\n")
                .append("df.dist2 =as.matrix(df2, labels=TRUE)\n")
                .append("colnames(df.dist2) <- rownames(df.dist2) <- input[['Sequences']]\n")
                .append("NJ_tree <- nj(df.dist2)\n")
                .append("pdf(NULL)\n")
                .append("plot(NJ_tree, main = \"Neighbor Joining\")\n")
                .append("write.tree(NJ_tree, tree.names = TRUE, file=\"").append(output_path).append("sequence_kmer_distance.tree\")\n")
                .append("cat(\"\\nK-mer distance tree written to: ").append(output_path).append("sequence_kmer_distance.tree\\n\\n\")");
        write_SB_to_file_full_path(rscript, output_path + "sequence_kmer_distance_tree.R");
    }
    
    /**
     * 
     * @return key is a string: "genome_" + genome number or sequence id 
     */
    public HashMap<String, long[]> gather_degen_nodes_per_seq() {
        frequency_queue = new LinkedBlockingQueue<>();
        string_queue = new LinkedBlockingQueue<>();
        node_queue = new LinkedBlockingQueue<>();
        HashMap<String, long[]> degen_node_count = new HashMap<>();
      
        try {
            ExecutorService es = Executors.newFixedThreadPool(THREADS);     
            if (compare_sequences) {
                es.execute(new find_degen_nodes_sequence(degen_node_count));      
                //for (int i = 1; i <= THREADS-1; i++) {
                es.execute(new add_freq_per_seq(1));          
                //}
            } else { // genome level, only 1 thread 
                 es.execute(new find_degen_nodes_genome(degen_node_count));
            }
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            
        }
        return degen_node_count;
    }
    
    /**
     * When this is run with multiple threads at the same time, it sometimes happens that the 3 values belonging together end up in different threads
     */
    public class add_freq_per_seq implements Runnable {     
        
        int thread_nr;
        public add_freq_per_seq(int thread_nr) {
            this.thread_nr = thread_nr;
        }
                 
        public void run() {
            Transaction tx = GRAPH_DB.beginTx(); // start database transaction
            long transaction_counter = 0;
            try {
                try {
                    int[] frequencies = frequency_queue.take();
                    if (frequencies[0] == -1) {  // everything was already known          
                        Pantools.logger.trace("everything was known already {}.", thread_nr);
                        return;
                    }
                    String property_name = string_queue.take();
                    Node node = node_queue.take();       
                    while (frequencies[0] != -1) {                                        
                        transaction_counter ++;
                        node.removeProperty(property_name);
                        node.setProperty(property_name, frequencies);
                        if (transaction_counter % 5000 == 0 && transaction_counter > 1) {
                            tx.success();
                            tx.close();
                            tx = GRAPH_DB.beginTx(); // start a new database transaction
                        }
                        frequencies = frequency_queue.take();
                        if (frequencies[0] == -1) {
                            Pantools.logger.trace("i should stop {}.", thread_nr);
                            continue;
                        }
                        property_name = string_queue.take();
                        node = node_queue.take();  
                    }                 
                    Pantools.logger.trace("im out1.");
                    tx.success();
                } finally {
                    tx.close();
                    Pantools.logger.trace("im out2.");
                }  
            } catch(InterruptedException e) {
                          
            } 
            Pantools.logger.trace("im out {}.", thread_nr);
        }  
    }
    
    /**
     * 
     */
    public class find_degen_nodes_sequence implements Runnable {
        HashMap<String, long[]> degen_node_count;
        
        public find_degen_nodes_sequence(HashMap<String, long[]> degen_node_count) {
            this.degen_node_count = degen_node_count; 
        }
        
        public void run() {
            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
                long degen_nodes = count_nodes(DEGENERATE_LABEL);
                ResourceIterator<Node> all_kmers = GRAPH_DB.findNodes(DEGENERATE_LABEL);
                long node_counter = 0; // number of degen nodes 
                long distinct_counter = 0; // number of k-mers in degen nodes    
                long total_counter = 0; // total frequency of the degenerate k-mers in all (selected) genomes of the pangenome
                while (all_kmers.hasNext()) {
                    node_counter ++;
                    Node nuc_node = all_kmers.next();
                    int node_len = (int) nuc_node.getProperty("length");
                    HashMap<String, Integer> kmer_count_per_seq = get_kmers_per_sequence(nuc_node);
                    int actual_kmers = node_len - KMinOne;
                    if (node_counter % 1000 == 0 || node_counter == degen_nodes || node_counter < 101) {
                        System.out.print("\rGathering degenerate nodes " + node_counter + "/" + degen_nodes);
                    }
                    distinct_counter += actual_kmers;
                    for (String seq_id : kmer_count_per_seq.keySet()) {
                        // count k-mers for an entire genome, based on individual sequences
                        String[] seq_array = seq_id.split("_");
                        try_incr_hashmap(kmer_count_per_seq, "genome_" + seq_array[0], kmer_count_per_seq.get(seq_id));
                        total_counter += kmer_count_per_seq.get(seq_id) * actual_kmers;
                    }
                 
                    for (String seq_id : kmer_count_per_seq.keySet()) {
                        long[] values = new long[]{1, actual_kmers, actual_kmers * kmer_count_per_seq.get(seq_id)};
                        if (degen_node_count.containsKey(seq_id)) {
                            values = degen_node_count.get(seq_id);
                            values[0] += 1;
                            if (compressed_kmers) {
                                values[1] += 1;
                                values[2] += kmer_count_per_seq.get(seq_id);
                            } else {
                                values[1] += actual_kmers;
                                values[2] += actual_kmers * kmer_count_per_seq.get(seq_id);
                            }
                        }
                        degen_node_count.put(seq_id, values); 
                    }
                }
                long[] degen_values = new long[]{node_counter, distinct_counter, total_counter};
                degen_node_count.put("nodes_in_pangenome", degen_values);        
                tx.success(); // end of neo4j transaction
            }
            for (int i = 1; i <= THREADS-1; i++) {
                int [] empty = new int[] {-1};
                frequency_queue.add(empty); // -1 in the array will make the "add_freq_per_seq" function stop
            }
         
            while (frequency_queue.size() > 1000) {
                System.out.print("\rAdding frequencies to the k-mer nodes: " + frequency_queue.size() + "                   ");
                try {
                    Thread.sleep(1000); // 1 second
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            System.out.print("\r                                                        ");
        }
    }

    
    /**
     * 
     */
    public class find_degen_nodes_genome implements Runnable {
        HashMap<String, long[]> degen_node_count;
        
        public find_degen_nodes_genome(HashMap<String, long[]> degen_node_count) {
            this.degen_node_count = degen_node_count; 
        }
        
        public void run() {
            for (int i = 1; i <= total_genomes; i++) { // first value is always a 0                  
                long[] counts = new long[3];
                degen_node_count.put("genome_" + i, counts);
             }
            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
                long degen_nodes = count_nodes(DEGENERATE_LABEL);
                ResourceIterator<Node> all_kmers = GRAPH_DB.findNodes(DEGENERATE_LABEL);
                long node_counter = 0; // number of degen nodes in pangenome 
                long distinct_counter = 0; // number of k-mers in degen nodes (in pangenome)    
                long total_counter = 0; // total frequency of the degenerate k-mers in all (selected) genomes of the pangenome
                while (all_kmers.hasNext()) {
                    node_counter ++;
                    Node nuc_node = all_kmers.next();
                    int node_len = (int) nuc_node.getProperty("length");
                    int actual_kmers = node_len - KMinOne;
                    //int[] freq_array = get_kmer_frequencies_property(nuc_node);      
                    long[] freq_array = (long[]) nuc_node.getProperty("frequencies");
                    if (node_counter % 1000 == 0 || node_counter == degen_nodes || node_counter < 101) {
                        System.out.print("\rGathering degenerate nodes " + node_counter + "/" + degen_nodes);
                    }
                    if (compressed_kmers) {
                       distinct_counter ++;
                    } else {
                        distinct_counter += actual_kmers;
                    }
                    
                    for (int i = 1; i < freq_array.length; i++) { // first value is always a 0   
                        if (freq_array[i] == 0 || skip_array[i-1]) {
                            continue;
                        }
                        long[] degen_counts = degen_node_count.get("genome_" + i);
                        degen_counts[0] += 1;
                        if (compressed_kmers) {
                            total_counter += freq_array[i];
                            degen_counts[1] += 1;
                            degen_counts[2] += freq_array[i];
                        } else {
                            total_counter += actual_kmers * freq_array[i];
                            degen_counts[1] += actual_kmers;
                            degen_counts[2] += actual_kmers * freq_array[i];
                        }
                    }
                }
                long[] degen_values = new long[]{node_counter, distinct_counter, total_counter};
                degen_node_count.put("nodes_in_pangenome", degen_values);        
                tx.success(); // end of neo4j transaction
            }
            System.out.print("\r                                                        ");
        }
    }
    
    /** 
     Genome nodes are not always retrieved in the correct order (1,2,3) 
    */
    public void retrieve_selected_sequences() {
        selected_sequences = new ArrayList<>();
        HashMap<Integer, Integer> sequence_count_per_genome = new HashMap<>();
        sequences_per_genome = new ConcurrentHashMap<>();
        int counter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> genome_nodes = GRAPH_DB.findNodes(GENOME_LABEL);
            while (genome_nodes.hasNext()) {
                Node genome_node = genome_nodes.next();
                int genome_nr = (int) genome_node.getProperty("number");
                counter ++;
                if (counter % 10 == 0) {
                    System.out.print("\rRetrieving sequences: genome " + counter);
                }
                int num_sequences = (int) genome_node.getProperty("num_sequences");
                sequence_count_per_genome.put(genome_nr, num_sequences);
            }
            tx.success();
        }
        
        for (int i = 1; i <= total_genomes; i++) { // i is genome nr
            int num_sequences = sequence_count_per_genome.get(i);
            for (int j = 1; j <= num_sequences; ++j) {
                try_incr_AL_chashmap(sequences_per_genome, i, j); // holds all sequences per genome, use skip_seq_array to through list faster
                if (skip_seq_array[i-1][j-1]) {
                    continue;
                }
                selected_sequences.add(i + "_" + j);   
            }
        }
    }
    
    /**
      * in the newest pantools version, a frequency is changed from a long to an integer
      * @param nuc_node
      * @return 
      */
    public int get_kmer_frequency(Node nuc_node) {
        int frequency;
        try {
            frequency = (int) nuc_node.getProperty("frequency");
        } catch (ClassCastException classcast) {
            long freq_long = (long) nuc_node.getProperty("frequency"); 
            frequency = (int) freq_long;
        }
        return frequency; 
    }
    
    /**
     * Count the number of degenerate 'nucleotide' nodes 
     * @param ignore_mode
     * @return 
     */
    
    public int[] gather_all_degenerate_nodes(boolean ignore_mode) {
        int[] degenerate_count = new int[total_genomes*2]; // [ distinct kmers genome 1, total kmers genome 2, distinct kmers genome 2, etc.
        ResourceIterator<Node> degen_nuc_nodes = GRAPH_DB.findNodes(DEGENERATE_LABEL);
        long node_counter = 0;
        while (degen_nuc_nodes.hasNext()) {
            Node nuc_node = degen_nuc_nodes.next();
            long[] freq_array = (long[]) nuc_node.getProperty("frequencies");
            int kmer_size = (int) nuc_node.getProperty("length");
            int actual_kmers = kmer_size-KMinOne;
            node_counter ++;
            if (node_counter % 1000 == 0 && node_counter != 0) {
                System.out.print("\rCounting degenerate nodes: " + node_counter);
            }
            for (int i=1; i < freq_array.length; i++) { // first position is skipped as it always 0
                if (skip_array[i-1]) { 
                    continue;
                }
                if (freq_array[i] == 0) {
                    continue;
                }
                if (Mode.contains("COMPRESSED") || ignore_mode) {
                    degenerate_count[(i*2)-2] += 1; // distinct 
                    degenerate_count[(i*2)-1] += freq_array[i]; // total 
                } else {
                    degenerate_count[(i*2)-2] += actual_kmers; // distinct 
                    degenerate_count[(i*2)-1] += freq_array[i]*actual_kmers; // total 
                }
            }
        }
        System.out.print("\r                                                  "); // spaces are intentional
        return degenerate_count;
    }
    
    /**
     * Used by gene/kmer/function classification to set the CORE And UNIQUE thresholds 
     * Transform the percentages into number of genomes
     * @param type 
     */
    public void core_unique_thresholds_for_classification(ArrayList<String> genome_list, String type) {
        int total_genomes = genome_list.size();
        if (total_genomes < 3 && !compare_sequences) {
            Pantools.logger.error("The pangenome should contain at least 3 genomes for this analysis.");
            System.exit(1);
        }
        if (core_threshold != 0) { // --core-threshold was set by user 
            int ct_percentage = core_threshold;
            core_threshold = (int) Math.round(total_genomes * core_threshold / 100.0); // transform percentage into number of genome
            Pantools.logger.info("A --core-threshold was set, {} are considered SOFTCORE/CORE when shared by {} genomes ({}%).", type, core_threshold, ct_percentage);
        } else {
            core_threshold = total_genomes;
            Pantools.logger.info("No --core-threshold was set, {} are considered CORE when shared by {} genomes (100%).", type, core_threshold);
        } 
        if (unique_threshold == 0) {
            unique_threshold = 1;
            Pantools.logger.info("No --unique-threshold was set, {} are considered UNIQUE when shared by a single genome.", type);
        } else {
            int ut_percentage = unique_threshold;
            unique_threshold = (int) Math.round(total_genomes/100.0 * unique_threshold);
            Pantools.logger.info("A --unique-threshold was set, {} are considered UNIQUE/CLOUD when shared by {} genomes ({}%).", type, unique_threshold, ut_percentage);
        }
    }
    
    /**
     * 
     * @param kmer_occur_map 
     */
    public synchronized void write_kmer_occurrence_output(ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map) {
        TreeSet<Long> current_frequencies = new TreeSet<>(kmer_occur_map.keySet());
        StringBuilder output = new StringBuilder();
        for (Long frequency : current_frequencies) { 
            output.append("#").append(frequency).append("#,");
            ArrayList<Node> nodes = kmer_occur_map.get(frequency);
            kmer_occur_map.remove(frequency);
            for (Node nuc_node : nodes) {
                output.append(nuc_node.getId()).append(",");
            }
            output.append("\n");
        }
        if (!check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/kmer.log")) { 
            write_SB_to_file_full_path(output, WORKING_DIRECTORY + "kmer_/kmer.log");
        } else {
            append_SB_to_file_full_path(output, WORKING_DIRECTORY + "kmer_classification/kmer.log");
        }
    }
    
    /**
     * 
     * @param kmer_occur_map
     * @return 
     */
    public synchronized ConcurrentHashMap<Long, ArrayList<Node>> read_kmer_occurrence_output(
            ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map) {

        if (!check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/kmer.log")) { 
            return kmer_occur_map;
        }
        try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "kmer_classification/kmer.log"))) {
            System.out.print("\rReading k-mer frequencies...");
            while (in.ready()) {
                String line = in.readLine().trim(); 
                String[] line_array = line.split(",");
                long freq = Long.parseLong(line_array[0].replace("#",""));
                for (int i=1; i < line_array.length; i++) { 
                    long node_id = Long.parseLong(line_array[i]);
                    Node nuc_node = GRAPH_DB.getNodeById(node_id);
                    try_incr_AL_chashmap(kmer_occur_map, freq, nuc_node);
                }
            } 
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}kmer_classification/kmer.log", WORKING_DIRECTORY);
            System.exit(1);
        }
        delete_file_full_path(WORKING_DIRECTORY + "kmer_classification/kmer.log");
        return kmer_occur_map;
    }
    
    /**
     * 
     * @param kmer_occur_map
     * @param highest_frequency
     * @param freq_per_genome 
     */
    
    public void create_kmer_occurrence_output(ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map,
            long highest_frequency, ConcurrentHashMap<Integer, HashMap<Long, Long>> freq_per_genome) {  
        if (!Mode.contains("OCCURRENCE")) {
            return;
        }
        long[] compressed_length_freq = new long[10000]; 
        StringBuilder high_freq_builder = new StringBuilder("#K-mer nodes with a frequency above ");
        int threshold = 0;
        if (NODE_VALUE != null) {
            threshold = Integer.parseInt(NODE_VALUE);
            high_freq_builder.append(NODE_VALUE).append("\n");
        }
      
        kmer_occur_map = read_kmer_occurrence_output(kmer_occur_map);
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "kmer_classification/kmer_occurrence.txt"))) {
            out.write("##This file consists of three parts\n##Part1\n"
                    + "#occurrence of a k-mer in the pangenome, number of k-mers with X occurrence for COMPRESSED k-mers, "
                    + "number of k-mers with X occurrence for TOTAL k-mers\n");
            for (long i=1; i <= highest_frequency; i++) {
                System.out.print("\rWriting k-mers with frequency of " + i + "/" + highest_frequency + "      "); // spaces are intentional
                ArrayList<Node> node_list = kmer_occur_map.get(i);
                if (node_list == null) {
                    continue;
                }
                long actual_freq = 0;
                int freq = node_list.size();
                for (Node nuc_node : node_list) {
                    int kmer_size = (int) nuc_node.getProperty("length");
                    actual_freq += kmer_size - KMinOne;
                } 
                out.write(i + ", " + freq + ", " + actual_freq + "\n"); 
            }
            
            out.write("\n##Part2\n#occurrence of a k-mer within a single genome, number of k-mers with X occurrence\n");
            for (int i=1; i <= total_genomes; i++) {
                out.write("#Genome " + i + "\n");
                HashMap<Long, Long> found_frequencies_map = freq_per_genome.get(i);
                for (long j = 1; j <= highest_frequency; j++) { 
                    if (found_frequencies_map.containsKey(j)) {
                        long freq = found_frequencies_map.get(j);
                        out.write(j + ", " + freq + "\n");
                    } 
                }
                out.write("\n");
            }
             
            out.write("\n##Part 3. Frequency array per kmer\n"
                 + "#Node identifier, total frequency, k-mer length, k-mer sequence, [frequency per genome]\n");
            for (long i=1; i <= highest_frequency; i++) {
                System.out.print("\rWriting k-mers with frequency of " + i + "/" + highest_frequency + "       "); // spaces are intentional
                ArrayList<Node> node_list = kmer_occur_map.get(i); 
                kmer_occur_map.remove(i);
                if (node_list == null) {
                    continue;
                }
                out.write("Frequency of " + i + "\n");
                for (Node nuc_node : node_list) {
                    long node_id = nuc_node.getId();
                    long[] freq_array = (long[]) nuc_node.getProperty("frequencies");
                    long[] freq_array2 = remove_first_position_array(freq_array);
                    int freq1 = get_kmer_frequency(nuc_node);
                    if (NODE_VALUE != null && freq1 > threshold) {
                        high_freq_builder.append(node_id).append(",");
                    }
                    String sequence = (String) nuc_node.getProperty("sequence");
                    out.write(node_id + ", " + freq1 + ", " + sequence.length() + ", " + sequence +
                             ", " + Arrays.toString(freq_array2) + "\n");
                    compressed_length_freq[sequence.length()] += 1;
                }
            }
            
            out.write("\n#Compressed k-mer, Count of distinct compressed kmer lengths\n");
            for (int i= 0; i < compressed_length_freq.length; i++) {
                if (0 == compressed_length_freq[i]) {
                    continue;
                }
                out.write(i + ", " + compressed_length_freq[i] + "\n");
            }
            out.close();
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to create: {}kmer_classification/kmer_occcurance.txt", WORKING_DIRECTORY);
            System.exit(1);
        } 
        
        if (NODE_VALUE != null) {
            write_string_to_file_in_DB(high_freq_builder.toString().replaceFirst(".$",""),
                    "kmer_classification/highly_occurring_kmers.txt" );
        }
        System.out.print("\r                                                           "); // spaces are intentional
    }
    
    /**
     * 
     * @param pheno_kmer_map
     */
    public void kmer_classi_pheno_overview(ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map) {
        if (PHENOTYPE == null) {
            return;
        }
        StringBuilder overview_builder = new StringBuilder("Phenotype, threshold, total members: members (genome numbers)\n");
        for (String phenotype : phenotype_map.keySet()) {
            int[] value = phenotype_map.get(phenotype);
            int threshold = phenotype_threshold_map.get(phenotype);
            overview_builder.append(phenotype).append(", ").append(threshold).append(", ").append(value.length).append(": ") // header of kmer_classification_phenotype_overview.txt
                    .append(Arrays.toString(value).replace(" ","").replace("[","").replace("]","")).append("\n"); 
        }
        overview_builder.append("\nOn each line. The phenotype: number of nodes (compressed k-mers), total number of k-mers\n"
                + "Phenotype shared\n");
        write_pheno_kmer_classi_file(overview_builder, "kmer_classification/kmer_identifiers/phenotype_shared_kmers.csv", pheno_kmer_map, "#shared");
        overview_builder.append("\nPhenotype specific\n");
        write_pheno_kmer_classi_file(overview_builder,"kmer_classification/kmer_identifiers/phenotype_specific_kmers.csv", pheno_kmer_map, "#specific");
        overview_builder.append("\nPhenotype exclusive\n");
        write_pheno_kmer_classi_file(overview_builder,"kmer_classification/kmer_identifiers/phenotype_exclusive_kmers.csv", pheno_kmer_map, "#exclusive");
        write_SB_to_file_in_DB(overview_builder, "kmer_classification/kmer_classification_phenotype_overview.txt");
        System.out.print("\r                                        "); // spaces are intentional
    }
    
    /**
     * Creates phenotype_specific_kmers.csv and phenotype_shared_kmers.csv
     * @param overview_builder
     * @param filename
     * @param pheno_kmer_map 
     * @param phenotype_class
     */
    public void write_pheno_kmer_classi_file(StringBuilder overview_builder, String filename, ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map, 
            String phenotype_class) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + filename))) {
            for (String phenotype : phenotype_map.keySet()) {
                if (!pheno_kmer_map.containsKey(phenotype + phenotype_class)) {
                    out.write("#Phenotype " + phenotype + ": 0 nodes and k-mers\n");
                    overview_builder.append(phenotype).append(": 0\n");
                    continue;
                }
                int[] genomes = phenotype_map.get(phenotype);
                int threshold = phenotype_threshold_map.get(phenotype);
                ArrayList<Node> node_list = pheno_kmer_map.get(phenotype + phenotype_class);
                StringBuilder node_builder = new StringBuilder();
                long total_kmers = 0;
                for (Node nuc_node : node_list) {
                    int length = (int) nuc_node.getProperty("length"); 
                    total_kmers += length-KMinOne;
                    long node_id = nuc_node.getId();
                    node_builder.append(node_id).append(",");
                    if (total_kmers % 1001 == 0 || total_kmers == node_list.size()) {
                        System.out.print("\rCreating phenotype output: " + total_kmers);
                    }
                }
                String total_kmers_str = "";
                if (!compressed_kmers) {
                    total_kmers_str = " & " + total_kmers + " distinct k-mers";
                }
                out.write("#Phenotype " + phenotype + ", " + genomes.length + " genomes, threshold of " + threshold + " genomes: " 
                        + node_list.size() + " nodes" + total_kmers_str + "\n" + node_builder.toString() + "\n");
                overview_builder.append(phenotype).append(": ").append(node_list.size()).append(", ").append(total_kmers).append("\n");
                out.write("\n");
            }
            out.close();
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to create: {}", WORKING_DIRECTORY + filename);
            System.exit(1);
        } 
    }
    
    /**
     * Check if a node/kmer meets the threshold and is not shared by other phenotypes. 
     * If both yes, phenotype specific
     * If only threshold is met, phenotype shared
     * 
     * @param nuc_node
     * @param pheno_count_map
     * @param pheno_kmer_map 
     */
    public static void determine_kmer_pheno_shared_specific(Node nuc_node, HashMap<String, Integer> pheno_count_map, 
          ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map) { 
        if (PHENOTYPE == null) {
            return;
        }
        for (String phenotype : pheno_count_map.keySet()) {
            if (phenotype.equals("?") || phenotype.equals("Unknown")) {
                continue;
            }
            int threshold = phenotype_threshold_map.get(phenotype);
            int value = pheno_count_map.get(phenotype);
            boolean pass_threshold = value >= threshold; // if value is higher or equal to threshold
            boolean no_other_pheno = false;
            for (String other_phenotype : pheno_count_map.keySet()) {
                if (phenotype.equals(other_phenotype) || other_phenotype.equals("?") || other_phenotype.equals("Unknown")) { 
                    continue;
                }
                no_other_pheno = true;// disrupted by presence of other phenotype
            }
            if (pass_threshold) {
                try_incr_AL_chashmap(pheno_kmer_map, phenotype + "#shared", nuc_node); // k-mer is phenotype shared
            }
            if (no_other_pheno && pass_threshold) {
                try_incr_AL_chashmap(pheno_kmer_map, phenotype + "#specific", nuc_node); // k-mer is phenotype specific
            }
            if (no_other_pheno) {
                try_incr_AL_chashmap(pheno_kmer_map, phenotype + "#exclusive", nuc_node); // k-mer is phenotype exclusive
            }
        }
    }
     
    /*
     Reports all phenotype nodes and their properties 
    */
    public void phenotype_overview() {
        HashMap<String, Integer> count_phenotype = new HashMap<>();
        HashMap<String, ArrayList<Integer>> genomes_per_phenotype = new HashMap<>(); 
        Set<String> phenotype_set = new HashSet<>(); // phenotype properties
        String phenos_per_genomes = retrieve_phenotypes_for_overview(count_phenotype, genomes_per_phenotype, phenotype_set);
        String phenotype_occurrence = count_phenotype_occurrence(phenotype_set, count_phenotype);
        String genomes_per_pheno = print_genomes_per_phenotype(phenotype_set, genomes_per_phenotype);  
        write_string_to_file_in_DB(genomes_per_pheno + phenotype_occurrence + phenos_per_genomes, "phenotype_overview.txt");
        Pantools.logger.info("Overview written to: {}phenotype_overview.txt", WORKING_DIRECTORY);
    }
    
    /**
     * Get all available phenotypes from 'phenotype' nodes
     * @return Arraylist with all phenotype properties (no values)
     */
    public ArrayList<String> retrieve_phenotypes_properties() {
        HashSet<String> phenotype_properties_set = new HashSet<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> pheno_nodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (pheno_nodes.hasNext()) {
                Node pheno_node = pheno_nodes.next();           
                Iterable<String> properties = pheno_node.getPropertyKeys();
                for (String property : properties) {
                    if (property.equals("genome")) { 
                        continue;
                    }
                    phenotype_properties_set.add(property);
                }
            }
            tx.success();
        }  
        //converting set to a list to make sure the order is always the same
        ArrayList<String> phenotype_properties_list = new ArrayList<>(phenotype_properties_set);
        return phenotype_properties_list;
     }
    
    /**
     * 
     * @return 
     */
    public HashMap<String, String> retrieve_phenotypes_for_metrics() {    
        HashMap<String, String> all_phenotypes_map = new HashMap<>(); // key is genome nr with phenotype property, value is the phenotype value
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> pheno_nodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (pheno_nodes.hasNext()) {
                Node pheno_node = pheno_nodes.next();
              
                Iterable<String> properties = pheno_node.getPropertyKeys();
                int genome_nr = (int) pheno_node.getProperty("genome");
            
                for (String property : properties) {
                    if (property.equals("genome")) { 
                        continue;
                    }
                    Object phenotype_value = pheno_node.getProperty(property); // print a value from node without converting?!
                    // value is only used for printing, therefore can always be converted to a string 
                    String phenotype_str_value = String.valueOf(phenotype_value);
                    all_phenotypes_map.put(genome_nr + "#" + property, phenotype_str_value);
                }
            } 
            tx.success(); // transaction successful, commit changes
        }
        return all_phenotypes_map;
    }
    
    /**
     * 
     * @param count_phenotype
     * @param genomes_per_phenotype
     * @param phenotype_set
     * @return 
     */
    public String retrieve_phenotypes_for_overview(HashMap<String, Integer> count_phenotype, HashMap<String, ArrayList<Integer>> genomes_per_phenotype, Set<String> phenotype_set) {
        StringBuilder per_genome_builder = new StringBuilder("##Part 3. Phenotypes per genome\n");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> pheno_nodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (pheno_nodes.hasNext()) {
                Node pheno_node = pheno_nodes.next();
                long pheno_id = pheno_node.getId();
                Iterable<String> properties = pheno_node.getPropertyKeys();
                int genome_nr = (int) pheno_node.getProperty("genome");
                per_genome_builder.append("\nGenome ").append(genome_nr).append(", phenotype node: ").append(pheno_id).append("\n");
                for (String property : properties) {
                    if (property.equals("genome")) { 
                        continue;
                    }
                    Object prop_value = pheno_node.getProperty(property); // print a value from node without converting?!
                    per_genome_builder.append(" ").append(property).append(": '").append(prop_value).append("'\n");
                    phenotype_set.add(property);
                    try_incr_hashmap(count_phenotype, property + "#" + prop_value, 1);
                    try_incr_AL_hashmap(genomes_per_phenotype, property + ":" + prop_value, genome_nr);
                }
            } 
            per_genome_builder.append("\n"); 
            tx.success(); // transaction successful, commit changes
        }
        return per_genome_builder.toString();
    }
    
    /**
     * 
     * @param phenotype_set
     * @param genomes_per_phenotype
     * @return 
     */
    public String print_genomes_per_phenotype(Set<String> phenotype_set, HashMap<String, ArrayList<Integer>> genomes_per_phenotype) {
        StringBuilder output_builder = new StringBuilder("##This file consists of three parts.\n##Part 1. Overview of values and genomes per phenotype.\n");
        StringBuilder header = new StringBuilder("#Present phenotypes: ");
        for (String pheno : phenotype_set) {
            header.append(pheno).append(", ");
            output_builder.append("#").append(pheno).append("\n");
            for (Map.Entry<String, ArrayList<Integer>> entry : genomes_per_phenotype.entrySet()) {
                String key = entry.getKey();
                ArrayList<Integer> value_list = entry.getValue();
                if (key.startsWith(pheno + ":")) {
                    StringBuilder allValues = new StringBuilder();
                    for (int value: value_list) {
                        allValues.append(value).append(",");
                    }     
                    output_builder.append(" ").append(key.replace(pheno + ":", "")).append(": ").append(allValues).append("\n");
                }
            }
            output_builder.append("\n");
        }
        return header.toString().replaceFirst(".$","").replaceFirst(".$","") + "\n\n" + output_builder.toString();
    }
        
    /**
     * 
     * @param phenotype_set
     * @param count_phenotype
     * @return 
     */
    public String count_phenotype_occurrence(Set<String> phenotype_set, HashMap<String, Integer> count_phenotype) {
        StringBuilder pheno_occurrence = new StringBuilder("##Part 2. Occurrence of each value within a phenotype\n");
        for (String phenotype : phenotype_set) {
            StringBuilder part_builder = new StringBuilder();
            int counter = 0; 
            for (String phenotype_value : count_phenotype.keySet()) {
                if (phenotype_value.startsWith(phenotype + "#")) {
                    int value = count_phenotype.get(phenotype_value);
                    phenotype_value = phenotype_value.replace(phenotype + "#", "");
                    part_builder.append(" ").append(phenotype_value).append(", ").append(value).append("\n");
                    counter += value;
                }
            }
            pheno_occurrence.append(phenotype).append(" (").append(counter).append(" genomes)\n").append(part_builder).append("\n");
        }
        return pheno_occurrence.toString();
    }
    
    /**
     * Still called 'protein' in older PT versions 
     * @param mrna_node
     * @return 
     */
    public static String get_protein_sequence(Node mrna_node) {
        String sequence;
        if (mrna_node.hasProperty("protein")) {
            sequence = (String) mrna_node.getProperty("protein", "");
         } else {
            sequence = (String) mrna_node.getProperty("protein_sequence", "");
        }
        return sequence;
    }
    
    /*
     Still called 'dna_sequence' or 'sequence' in older PT versions 
    */
    public static String get_nucleotide_sequence(Node mrna_node) {
        String sequence;
        Object value = mrna_node.getProperty("sequence");  // old PT version
        if (value instanceof String) {
            sequence = String.valueOf(value); 
        } else {
            if (mrna_node.hasProperty("nucleotide_sequence")) {
                sequence = (String) mrna_node.getProperty("nucleotide_sequence");
            } else {
                sequence = (String) mrna_node.getProperty("dna_sequence"); // old version, can be removed soon
            }
        }
        return sequence;
    }
    
    /**
     * Creates gains_losses_median_or_average.R & gains_losses_median_and_average.R rscripts for pangenome_structure
     */
    public void create_pangenome_size_gains_losses_rscript() {
        String R_LIB = check_r_libraries_environment();
        StringBuilder rscript = new StringBuilder();
        rscript.append("#! /usr/bin/env RScript\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"ggplot2\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(ggplot2)\n")
                .append("\n")
                .append("# AVERAGE: core, accessory and unique -> ").append(WD_full_path).append("pangenome_size/gene/gains_losses/average_all.csv\n")
                .append("# MEDIAN : core, accessory and unique -> ").append(WD_full_path).append("pangenome_size/gene/gains_losses/median_all.csv\n")
                .append("# AVERAGE: core & accessory -> ").append(WD_full_path).append("pangenome_size/gene/gains_losses/average_core_accessory.csv\n")
                .append("# MEDIAN : core & accessory -> ").append(WD_full_path).append("pangenome_size/gene/gains_losses/median_core_accessory.csv\n")
                .append("\n")
                .append("df = read.csv(\"").append(WD_full_path).append("pangenome_size/gene/gains_losses/average_all.csv\", sep=\",\",header = TRUE)\n")
                .append("plot1 = ggplot(df, aes(x=x, y=y, color=Category)) +\n")
                .append("   scale_color_manual(values=c(\"Accessory\" = \"#0072B2\", \"Core\" = \"#009E73\", \"Unique\" = \"#D55E00\"))\n")
                .append("if (packageVersion(\"ggplot2\") < \"3.4.0\") {\n")
                .append("   plot1 = plot1 + geom_line(size = 1.5) #increase linewidth for ggplot2 version < 3.4.0\n")
                .append("} else {\n")
                .append("   plot1 = plot1 + geom_line(linewidth = 1.5) #increase linewidth for ggplot2 version >= 3.4.0\n")
                .append("}\n")
                .append("plot1 = plot1 + theme(text = element_text(size=12)) +\n")
                .append("   labs(x = \"Number of genomes\") +\n")
                .append("   labs(y = \"Homology group gain/loss\") +\n")
                .append("   theme_classic(base_size = 20) +\n")
                .append("   theme(legend.position = \"none\") +\n")
                .append("   #theme(legend.justification=c(1,1), legend.position=c(1,1)) +\n")
                .append("   geom_hline(yintercept=0, linetype=\"dashed\", color = \"darkgrey\")#+\n")
                .append("   #ggtitle(\"Average number of genes gained/lost per added genome\") +\n")
                .append("   #ylim(-500, 500) #set a limit to the y axis if outliers stretch the plot too much\n\n")
                .append("pdf(NULL)\n")
                .append("ggsave(\"").append(WD_full_path).append("pangenome_size/gene/gains_losses.png\", plot= plot1, height = 10, width = 10)\n")
                .append("cat(\"\\nGain and losses between pangenome sizes written to: ").append(WD_full_path).append("pangenome_size/gene/gains_losses.png\\n\\n\")");
        write_SB_to_file_in_DB(rscript, "pangenome_size/gene/gains_losses_median_or_average.R");
        
        rscript = new StringBuilder();
        rscript.append("#! /usr/bin/env RScript\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"ggplot2\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(ggplot2)\n")
                .append("\n")
                .append("# core, accessory and unique -> ").append(WD_full_path).append("pangenome_size/gene/gains_losses/average_all.csv\n")
                .append("#                            -> ").append(WD_full_path).append("pangenome_size/gene/gains_losses/median_all.csv\n")
                .append("# core & accessory -> ").append(WD_full_path).append("pangenome_size/gene/gains_losses/average_core_accessory.csv\n")
                .append("#                  -> ").append(WD_full_path).append("pangenome_size/gene/gains_losses/median_core_accessory.csv\n")
                .append("\n")
                .append("df_average = read.csv(\"").append(WD_full_path).append("pangenome_size/gene/gains_losses/average_all.csv\", sep=\",\", header = TRUE)\n")
                .append("df_median = read.csv(\"").append(WD_full_path).append("pangenome_size/gene/gains_losses/median_all.csv\", sep=\",\", header = TRUE)\n");
        append_common_part_gains_losses_rscript(rscript);
        rscript.append("pdf(NULL)\n")
                .append("ggsave(\"").append(WD_full_path).append("pangenome_size/gene/gains_losses_core_accessory_unique.png\", plot= plot1, height = 10, width = 10)\n")
                .append("cat(\"\\nGain and losses between pangenome sizes written to: ").append(WD_full_path)
                .append("pangenome_size/gene/gains_losses_core_accessory_unique.png\\n\\n\")\n\n");
        
        // part 2 of script
        rscript.append("df_average = read.csv(\"").append(WD_full_path).append("pangenome_size/gene/gains_losses/average_core_accessory.csv\", sep=\",\", header = TRUE)\n")
                .append("df_median = read.csv(\"").append(WD_full_path).append("pangenome_size/gene/gains_losses/median_core_accessory.csv\", sep=\",\", header = TRUE)\n");
        append_common_part_gains_losses_rscript(rscript);
        rscript.append("pdf(NULL)\n")
                .append("ggsave(\"").append(WD_full_path).append("pangenome_size/gene/gains_losses_core_accessory.png\", plot= plot1, height = 10, width = 10)\n")
                .append("cat(\"Gain and losses between pangenome sizes written to: ").append(WD_full_path)
                .append("pangenome_size/gene/gains_losses_core_accessory.png\\n\\n\")\n");          
        write_SB_to_file_in_DB(rscript, "pangenome_size/gene/gains_losses_median_and_average.R");   
    }
    
    /**
     * 
     * @param rscript 
     */
    public void append_common_part_gains_losses_rscript(StringBuilder rscript) {
        rscript.append("plot1 = ggplot(df_average, aes(x=x, y=y, color=Category, linetype=Metric))\n")
                .append("if (packageVersion(\"ggplot2\") < \"3.4.0\") {\n")
                .append("   plot1 = plot1 + geom_line(size = 1.0, data = df_median, aes(x =x, y =y, color=Category)) + #increase linewidth for ggplot2 version < 3.4.0\n")
                .append("   geom_line(size = 1.7) #increase linewidth for ggplot2 version < 3.4.0\n")
                .append("} else {\n")
                .append("   plot1 = plot1 + geom_line(linewidth = 1.0, data = df_median, aes(x =x, y =y, color=Category)) + #increase linewidth for ggplot2 version >= 3.4.0\n")
                .append("   geom_line(linewidth = 1.7) #increase linewidth for ggplot2 version >= 3.4.0\n")
                .append("}\n")
                .append("plot1 = plot1 + scale_color_manual(values=c(\"Accessory\" = \"#0072B2\", \"Core\" = \"#009E73\", \"Unique\" = \"#D55E00\")) +\n")
                .append("   scale_linetype_manual(values=c(\"Average\" = \"dotted\", \"Median\" = \"solid\")) +\n")
                .append("   labs(x = \"Number of genomes\") +\n")
                .append("   labs(y = \"Homology group gain/loss\") +\n")        
                .append("   geom_hline(yintercept=0, linetype=\"dashed\", color = \"darkgrey\") +\n")
                .append("   #ggtitle(\"Average number of genes gained/lost per added genome\") +\n")
                .append("   #ylim(-500, 500) + #set a limit to the y axis if outliers stretch the plot too much\n")
                .append("\n")
                .append("   # Three options for the legend.\n")
                .append("   # 1. Uncomment the next two lines for a legend to the right of the plot\n")
                .append("   # 2. Only uncomment the first line to have a legend inside the plot\n")
                .append("   # 3. Only uncomment the second line to have no legend\n")
                .append("\n")
                .append("   #theme(legend.position = \"none\") + # line 1\n") 
                .append("   theme(legend.justification=c(1,1), legend.position=c(1,1)) + # line 2\n")
                .append("   guides(colour = guide_legend(override.aes = list(size=5))) +\n")
                .append("   theme_bw(base_size = 18) # increase this number of have larger text and numbers\n")
                .append("\n"); 
    }

    /**
     * Creates the pangenome_growth.R rscript. Used by pangenome_structure_gene/kmer functions
     *
     * @param gene_or_kmer
     */
    public void create_pangenome_size_rscript(String gene_or_kmer) {
        StringBuilder rscript = new StringBuilder();

        if (gene_or_kmer.equals("kmer")) {
            rscript.append(create_pangenome_size_rscript_kmer(""));
            rscript.append(create_pangenome_size_rscript_kmer("_unique"));
        } else if (gene_or_kmer.equals("gene")) {
            rscript.append(create_pangenome_size_rscript_gene());
        } else {
            Pantools.logger.error("Error in create_pangenome_size_rscript. gene_or_kmer must be either gene or kmer.");
        }

        write_string_to_file_in_DB(rscript + "\n", "pangenome_size/" + gene_or_kmer + "/pangenome_growth.R");
    }

    /**
     * Creates the pangenome_growth.R rscript for Kmers. Used by pangenome_structure --kmer function
     * @param unique string can be empty or 'unique' (kept for old code)
     * @return a stringbuilder with the rscript
     */
    private StringBuilder create_pangenome_size_rscript_kmer(String unique) {
        StringBuilder rscript = new StringBuilder();

        String R_LIB = check_r_libraries_environment();
        String path = WD_full_path + "pangenome_size/kmer/";

        if (unique.equals("")) {
            rscript.append("#! /usr/bin/env RScript\n")
                    .append("#Assuming the R libraries were installed via Conda, the next lines can be ignored. If not, use them to manually install the required packages\n")
                    .append("#install.packages(\"ggplot2\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                    .append("#install.packages(\"svglite\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n").append("#install.packages(\"dplyr\", \"").append(R_LIB).append("\", \"https://cloud.r-project.org\")\n")
                    .append("#install.packages(\"scales\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n\n").append("library(ggplot2)\n")
                    .append("#library(svglite) #required to save plot as SVG\n").append("#library(scales) #uncomment when you want to show the total number of k-mers in millions instead of a scientific number\n")
                    .append("#library(dplyr) #uncomment when you want to show the total number of k-mers in millions instead of a scientific number\n\n");
        }

        rscript.append("int_breaks <- function(x, n = 5) { # to make sure line breaks are integers\n")
                .append("  l <- pretty(x, n)\n")
                .append("  l[abs(l %% 1) < .Machine$double.eps ^ 0.5]\n")
                .append("}\n\n").append("df = read.csv(\"").append(path).append("core_accessory").append(unique).append("_size.csv\", sep=\",\", header = TRUE)\n").append("#df <- mutate(df,y = y / 10^6) # convert scientific number to millions (requires dplyr library)\n").append("plot1 = ggplot(df, aes(x=x, y=y, color=Category, size=Category)) +\n")
                .append("    scale_color_manual(values=c(\"Median\" = \"#000000\", \"Accessory\" = \"#0072B2\", \"Core\" = \"#009E73\", \"Unique\" = \"#D55E00\")) +\n")
                .append("    scale_size_manual(values=c(\"Median\" = 0.2, \"Accessory\" = 0.8, \"Core\" = 0.8, \"Unique\" = 0.8)) +\n")
                .append("    geom_point() + \n")
                .append("    theme(text = element_text(size=12)) + \n")
                .append("    labs(x = \"Number of genomes\") + \n")
                .append("    #scale_y_continuous(labels = unit_format(unit = \"M\")) + # convert scientific number to millions (requires scales library)\n")
                .append("    labs(y = \"K-mers\") +\n")
                .append("    scale_x_continuous(breaks = int_breaks) +\n")
                .append("\n")
                .append("    # Three options for the legend.\n")
                .append("    # 1. Uncomment the next two lines for a legend to the right of the plot\n")
                .append("    # 2. Only uncomment the first line to have a legend inside the plot\n")
                .append("    # 3. Only uncomment the second line to have no legend\n")
                .append("\n")
                .append("    #theme(legend.position = \"none\") + # line 1\n")
                .append("    theme(legend.justification=c(1,1), legend.position=c(1,1)) + # line 2\n")
                .append("    guides(colour = guide_legend(override.aes = list(size=5))) +\n")
                .append("    theme_bw(base_size = 18) # increase this number of have larger text and numbers\n\n").append("pdf(NULL)\n")
                .append("#ggsave(\"").append(path).append("core_accessory").append(unique).append("_growth.svg\", plot= plot1, height = 10, width = 10)\n")
                .append("ggsave(\"").append(path).append("core_accessory").append(unique).append("_growth.png\", plot= plot1, height = 10, width = 10)\n")
                .append("cat(\"\\nPangenome growth written to: ").append(path).append("core_accessory").append(unique).append("_growth.png\\n\\n\")\n");

        return rscript;
    }

    /**
     * Creates the pangenome_growth.R Rscript for genes. Used by pangenome_structure default function
     * This function creates an Rscript for three plots based on similarly named csv files:
     *      core_accessory_unique_size.png (core, accessory, unique)
     *      core_dispensable_size.png (core, dispensable)
     *      core_dispensable_total_size.png (core, dispensable, total)
     * @return a stringbuilder with the rscript
     */
    private StringBuilder create_pangenome_size_rscript_gene() {
        StringBuilder rscript = new StringBuilder();

        String R_LIB = check_r_libraries_environment();
        String path = WD_full_path + "pangenome_size/gene/";

        // start of the Rscript
        rscript.append("#! /usr/bin/env RScript\n")
                .append("#Assuming the R libraries were installed via Conda, the next lines can be ignored. If not, use them to manually install the required packages\n")
                .append("#install.packages(\"ggplot2\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("#install.packages(\"svglite\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("\n")
                .append("library(ggplot2)\n")
                .append("#library(svglite) #required to save plot as SVG\n");

        // define integer breaks for ggplot2
        rscript.append("int_breaks <- function(x, n = 5) { # to make sure line breaks are integers\n")
                .append("  l <- pretty(x, n)\n")
                .append("  l[abs(l %% 1) < .Machine$double.eps ^ 0.5]\n")
                .append("}\n\n");

        // core_accessory_unique_size.png
        rscript.append("df = read.csv(\"").append(path).append("core_accessory_unique_size.csv\", sep=\",\", header = TRUE)\n")
                .append("plot1 = ggplot(df, aes(x=x, y=y, color=Category, size=Category)) +\n")
                .append("    scale_color_manual(values=c(\"Median\" = \"#000000\", \"Accessory\" = \"#0072B2\", \"Core\" = \"#009E73\", \"Unique\" = \"#D55E00\")) +\n")
                .append("    scale_size_manual(values=c(\"Median\" = 0.2, \"Accessory\" = 0.8, \"Core\" = 0.8, \"Unique\" = 0.8)) +\n")
                .append("    geom_point() + \n")
                .append("    theme(text = element_text(size=12)) + \n")
                .append("    labs(x = \"Number of genomes\") + \n")
                .append("    labs(y = \"Number of homology groups\") +\n")
                .append("    scale_x_continuous(breaks = int_breaks) +\n")
                .append("\n")
                .append("    # Three options for the legend.\n")
                .append("    # 1. Uncomment the next two lines for a legend to the right of the plot\n")
                .append("    # 2. Only uncomment the first line to have a legend inside the plot\n")
                .append("    # 3. Only uncomment the second line to have no legend\n")
                .append("\n")
                .append("    #theme(legend.position = \"none\") + # line 1\n")
                .append("    theme(legend.justification=c(1,1), legend.position=c(1,1)) + # line 2\n")
                .append("    guides(colour = guide_legend(override.aes = list(size=5))) +\n")
                .append("    theme_bw(base_size = 18) # increase this number of have larger text and numbers\n\n")
                .append("pdf(NULL)\n")
                .append("#ggsave(\"").append(path).append("core_accessory_unique_growth.svg\", plot= plot1, height = 10, width = 10)\n")
                .append("ggsave(\"").append(path).append("core_accessory_unique_growth.png\", plot= plot1, height = 10, width = 10)\n")
                .append("cat(\"\\nPangenome growth written to: ").append(path).append("core_accessory_unique_growth.png\\n\\n\")\n\n");

        // core_dispensable_size.png
        rscript.append("df = read.csv(\"").append(path).append("core_dispensable_size.csv\", sep=\",\", header = TRUE)\n")
                .append("plot2 = ggplot(df, aes(x=x, y=y, color=Category, size=Category)) +\n")
                .append("    scale_color_manual(values=c(\"Median\" = \"#000000\", \"Core\" = \"#009E73\", \"Dispensable\" = \"#D55E00\")) +\n")
                .append("    scale_size_manual(values=c(\"Median\" = 0.2, \"Core\" = 0.8, \"Dispensable\" = 0.8)) +\n")
                .append("    geom_point() + \n")
                .append("    theme(text = element_text(size=12)) + \n")
                .append("    labs(x = \"Number of genomes\") + \n")
                .append("    labs(y = \"Number of homology groups\") +\n")
                .append("    scale_x_continuous(breaks = int_breaks) +\n")
                .append("\n")
                .append("    # Three options for the legend.\n")
                .append("    # 1. Uncomment the next two lines for a legend to the right of the plot\n")
                .append("    # 2. Only uncomment the first line to have a legend inside the plot\n")
                .append("    # 3. Only uncomment the second line to have no legend\n")
                .append("\n")
                .append("    #theme(legend.position = \"none\") + # line 1\n")
                .append("    theme(legend.justification=c(1,1), legend.position=c(1,1)) + # line 2\n")
                .append("    guides(colour = guide_legend(override.aes = list(size=5))) +\n")
                .append("    theme_bw(base_size = 18) # increase this number of have larger text and numbers\n\n")
                .append("pdf(NULL)\n")
                .append("#ggsave(\"").append(path).append("core_dispensable_growth.svg\", plot= plot2, height = 10, width = 10)\n")
                .append("ggsave(\"").append(path).append("core_dispensable_growth.png\", plot= plot2, height = 10, width = 10)\n")
                .append("cat(\"\\nPangenome growth written to: ").append(path).append("core_dispensable_growth.png\\n\\n\")\n\n");

        // core_dispensable_total_size.png
        rscript.append("df = read.csv(\"").append(path).append("core_dispensable_total_size.csv\", sep=\",\", header = TRUE)\n")
                .append("plot3 = ggplot(df, aes(x=x, y=y, color=Category, size=Category)) +\n")
                .append("    scale_color_manual(values=c(\"Median\" = \"#000000\", \"Core\" = \"#009E73\", \"Dispensable\" = \"#D55E00\", \"Total\" = \"#0072B2\")) +\n")
                .append("    scale_size_manual(values=c(\"Median\" = 0.2, \"Core\" = 0.8, \"Dispensable\" = 0.8, \"Total\" = 0.8)) +\n")
                .append("    geom_point() + \n")
                .append("    theme(text = element_text(size=12)) + \n")
                .append("    labs(x = \"Number of genomes\") + \n")
                .append("    labs(y = \"Number of homology groups\") +\n")
                .append("    scale_x_continuous(breaks = int_breaks) +\n")
                .append("\n")
                .append("    # Three options for the legend.\n")
                .append("    # 1. Uncomment the next two lines for a legend to the right of the plot\n")
                .append("    # 2. Only uncomment the first line to have a legend inside the plot\n")
                .append("    # 3. Only uncomment the second line to have no legend\n")
                .append("\n")
                .append("    #theme(legend.position = \"none\") + # line 1\n")
                .append("    theme(legend.justification=c(1,1), legend.position=c(1,1)) + # line 2\n")
                .append("    guides(colour = guide_legend(override.aes = list(size=5))) +\n")
                .append("    theme_bw(base_size = 18) # increase this number of have larger text and numbers\n\n")
                .append("pdf(NULL)\n")
                .append("#ggsave(\"").append(path).append("core_dispensable_total_growth.svg\", plot= plot3, height = 10, width = 10)\n")
                .append("ggsave(\"").append(path).append("core_dispensable_total_growth.png\", plot= plot3, height = 10, width = 10)\n")
                .append("cat(\"\\nPangenome growth written to: ").append(path).append("core_dispensable_total_growth.png\\n\\n\")\n");

        return rscript;
    }

    /**
     * Gene distance NJ tree
     * @param genome_list list of genomes
     */
    private void create_gene_distance_rscript(ArrayList<String> genome_list) {
        String output_path = WD_full_path + "gene_classification/";

        String R_LIB = check_r_libraries_environment();
        StringBuilder rscript = new StringBuilder ();
        rscript.append("#! /usr/bin/env RScript\n\n")
                .append("# Use one of the following files\n")
                .append("# ").append(output_path).append("distance_distinct_genes.csv\n")
                .append("# ").append(output_path).append("distance_inf_distinct_genes.csv\n")
                .append("# ").append(output_path).append("distance_all_genes.csv\n\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"ape\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(ape)\n\n")
                .append("input = read.csv(\"").append(output_path).append("distance_distinct_genes.csv\", sep=\",\", header = TRUE)\n")
                .append("input[").append((genome_list.size() + 2)).append("] <- NULL\n") // # when having 204 genomes, remove column 206
                .append("df2 = subset(input, select = -c(Genomes))\n")
                .append("df.dist2 =as.matrix(df2, labels=TRUE)\n")
                .append("colnames(df.dist2) <- rownames(df.dist2) <- input[['Genomes']]\n")
                .append("NJ_tree <- nj(df.dist2)\n")
                .append("pdf(NULL)\n")
                .append("plot(NJ_tree, main = \"Neighbor Joining\")\n")
                .append("write.tree(NJ_tree, tree.names = TRUE, file=\"").append(output_path).append("gene_distance.tree\")\n")
                .append("cat(\"\\nGene distance tree written to: ").append(output_path).append("gene_distance.tree\\n\\n\")");
       write_SB_to_file_full_path(rscript, output_path + "gene_distance_tree.R");
    }
    
    /**
     * 
     */
    public void print_locate_genes_parameters() {
        if (NODE_VALUE == null) {
            System.out.println("\rNo --value was provided. Length between genes is allowed to be 1Mb");
            NODE_VALUE = "1000000000";
        } else {
            try {
                long length = Long.parseLong(NODE_VALUE);  
                String length_str = b_Kb_Mb_or_Gb(length);
                System.out.println("\r--value was set. Maximum allowed length between two genes is " + NODE_VALUE + ", " + length_str);
            } catch (NumberFormatException nfe ) {
               Pantools.logger.error("The value included with --value is not a number.");
               System.exit(1);
            }
        }
        
        if (core_threshold > 0 && core_threshold < 100) {
            Pantools.logger.info("--core-threshold was set to {}%.", core_threshold);
        } else {
            core_threshold = 100;
            Pantools.logger.info("No --core-threshold was set. Core clusters must be shared by {} genomes.", adj_total_genomes);
        }
        if (GAP_OPEN == 0) {
            Pantools.logger.info("No --gap-open was set. No mismatches allowed during cluster construction.");
        } else {
            Pantools.logger.info("--gap-open was set. {} mismatches allowed (per cluster) during cluster construction.", GAP_OPEN);
        }
    }
    
    /**
     * 
     * Requires 
     * -dp 
     * --homology-groups
     * 
     * Optional: 
     * --phenotype 
     * --core-threshold between 1-99
     * --skip or --reference 
     * --gap-open/-go

     * 
     */ 
    public void locate_genes() {
        GAP_OPEN = 0;
        Pantools.logger.info("Reporting the regions per genome and checks if genes are next to each other.");
        if (SELECTED_HMGROUPS == null) {
            Pantools.logger.error("No --homology-groups/-hm were included.");
            System.exit(1);
        }
        check_database(); // starts up the graph database if needed
        create_directory_in_DB("locate_genes"); 
        if (Mode.equals("IGNORE-COPIES")) {
            Pantools.logger.info("Selected mode {}. Removing co-localized duplicates.", Mode);
        } 
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "locate_genes"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("The function 'locate_genes' does not work on a panproteome.");
            System.exit(1);
        }     
       
        print_locate_genes_parameters();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            if (PHENOTYPE != null) {
                Pantools.logger.info("The '{}' phenotype is included. Reporting gene clusters where the order and number of copies is identical.", PHENOTYPE);
                retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            } else {
                Pantools.logger.info("No --phenotype was selected.");
            } 
            ArrayList<Node> hm_nodes_list = read_hmgroup_input_file();
            stop_if_arraylist_contains_duplicates(hm_nodes_list);
            HashMap<Integer, HashSet<String>> present_sequences = check_which_sequences_are_in_homology_groups(hm_nodes_list);
            HashMap<Node, ArrayList<Node>> mrnas_for_genes_at_same_pos = new HashMap<>(); // key is gene node, value is arraylist with mrna nodes 
            HashMap<String, int[]> gene_start_pos_per_seq = new HashMap<>(); // key is sequence identifier, value is an array with all start positions of genes on a sequence 
            HashMap<String, Node[]> gene_nodes_per_seq = get_ordered_genes_per_seq(false, mrnas_for_genes_at_same_pos, gene_start_pos_per_seq, present_sequences);
          
            HashMap<String, TreeSet<Integer>> gene_positions_per_sequence = new HashMap<>();
            HashMap<String, ArrayList<Node>> mrnas_per_position = retrieve_mrnas_per_genome_map(hm_nodes_list, gene_nodes_per_seq, gene_positions_per_sequence);
           
            HashMap<Integer, String> clustered_hms_map = calculate_geneclusters(gene_positions_per_sequence, mrnas_per_position, gene_nodes_per_seq);
            if (Mode.equals("IGNORE-COPIES")) {
                remove_duplicate_genes_next_to_eachother(clustered_hms_map);
            }
           
            if (adj_total_genomes > 1) { // need at least two genomes two compare clusters 
                int original_core_threshold = core_threshold; 
                String original_phenotype = "";
                if (PHENOTYPE != null) {
                    run_cmpare_geneclusters_with_phenotype(clustered_hms_map);
                    original_phenotype = PHENOTYPE;
                } 
                PHENOTYPE = "";
                skip_list = new ArrayList<>();
                core_threshold = original_core_threshold;
                compare_geneclusters(clustered_hms_map);
                PHENOTYPE = original_phenotype;
                delete_file_in_DB("locate_genes/core_clusters");
            }
            print_locate_genes_output(hm_nodes_list);
            tx.success(); // transaction successful, commit changes
        }
    }
    
    /**
    * 
    * @param include_non_coding_genes non protein coding 
    * @param mrnas_for_genes_at_same_pos
    * @param gene_start_pos_per_seq
    * @param present_sequences_map
    * @return 
    */
    public static HashMap<String, Node[]> get_ordered_genes_per_seq(boolean include_non_coding_genes, 
            HashMap<Node, ArrayList<Node>> mrnas_for_genes_at_same_pos, HashMap<String, int[]> gene_start_pos_per_seq, 
            HashMap<Integer, HashSet<String>> present_sequences_map) {
      
        HashMap<String, Node[]> gene_nodes_per_seq = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            for (int i = 1; i <= total_genomes; i++) {
                int total_gene_counter = 0;
                if (skip_array[i-1]) {
                    continue;
                }
                Node genome_node = GRAPH_DB.findNode(GENOME_LABEL, "number", i);
                int num_sequences = (int) genome_node.getProperty("num_sequences");
                TreeSet<Integer> gene_start_positions = new TreeSet<>(); // automatically ordered, contains starting locations of genes 
                String previous_id = ""; // sequence identifier, used to find the start of a new sequence
                HashSet<String> present_sequences = new HashSet<>();
                if (present_sequences_map != null) {
                    present_sequences = present_sequences_map.get(i);
                    if (present_sequences == null) {
                        continue;
                    }
                    if (present_sequences.size() == num_sequences) { // no need to search this list for valid sequence when all sequences are selected 
                        present_sequences = new HashSet<>();
                    }
                }
                HashMap<Integer, ArrayList<Node>> address_with_node = new HashMap<>();
                for (int j = 1; j <= num_sequences; j++) {
                    if (skip_seq_array != null && skip_seq_array[i-1][j-1]) {
                        continue;
                    }
                    if (!present_sequences.isEmpty() && !present_sequences.contains(i + "_" + j)) {
                        continue;
                    }
                    ResourceIterator<Node> gene_nodes = GRAPH_DB.findNodes(GENE_LABEL, "genome", i, "sequence", j);
                    while (gene_nodes.hasNext()) {
                        total_gene_counter++;
                        if (total_gene_counter % 100 == 0) {
                            System.out.print("\rFinding correct order of genes: "
                                    + "Genome " + i + " sequence " + j + ". " + total_gene_counter + " genes      "); // spaces are intentional
                        }
                        Node gene_node = gene_nodes.next();
                        if (!include_non_coding_genes) {
                            boolean coding = check_if_coding_mrna(gene_node); // Check if ANY of the mRNAs codes for a protein
                            if (!coding) { 
                                continue;
                            }
                        }
                        int[] address = (int[]) gene_node.getProperty("address");
                        gene_start_positions.add(address[2]);
                        try_incr_AL_hashmap(address_with_node, address[2], gene_node); // used to find genes with the same start position
                        previous_id = address[0] + "_" + address[1];
                    }
                    put_ordered_genes_in_map(gene_start_pos_per_seq, gene_start_positions, previous_id, address_with_node, 
                            mrnas_for_genes_at_same_pos, gene_nodes_per_seq); // order the genes by start position
                    gene_start_positions = new TreeSet<>(); // reset variables for next sequence 
                    address_with_node = new HashMap<>();
                }
            }
            tx.success();
        }
        System.out.print("\r                                                                                                    "); // spaces are intentional
        return gene_nodes_per_seq;
    }
    
    /**
     * 
     * @param gene_start_pos_per_seq
     * @param gene_start_positions
     * @param sequence_id
     * @param address_with_node
     * @param mrnas_for_genes_at_same_pos
     * @param gene_nodes_per_seq 
     */
    public static void put_ordered_genes_in_map(HashMap<String, int[]> gene_start_pos_per_seq, TreeSet<Integer> gene_start_positions, 
            String sequence_id, HashMap<Integer, ArrayList<Node>> address_with_node, HashMap<Node, ArrayList<Node>> mrnas_for_genes_at_same_pos, 
            HashMap<String, Node[]> gene_nodes_per_seq) {
        if (gene_start_positions.isEmpty()) {
            return;
        }
        ArrayList<Node> final_node_list = new ArrayList<>();
        int[] gene_start_array = new int[gene_start_positions.size()];
        int gene_counter = 0;
        for (int gene_start : gene_start_positions) {
            gene_start_array[gene_counter] = gene_start;
            ArrayList<Node> gene_nodes_list = address_with_node.get(gene_start);
            if (gene_nodes_list.size() > 1) { // multiple genes at the same positon, inlude all mrna identifiers in a hashmap with first gene as key 
                check_which_hmgroup_of_gene(gene_nodes_list, mrnas_for_genes_at_same_pos);
            }
            final_node_list.add(gene_nodes_list.get(0));
            gene_counter++;
        }
        gene_start_pos_per_seq.put(sequence_id, gene_start_array);
        Node[] final_node_array = final_node_list.toArray(new Node[final_node_list.size()]); // list_to_array list to array 
       
        gene_nodes_per_seq.put(sequence_id, final_node_array);
    }

    /**
     * 
     * @param gene_nodes_list
     * @param mrnas_for_genes_at_same_pos 
     */
    public static void check_which_hmgroup_of_gene(ArrayList<Node> gene_nodes_list, HashMap<Node, ArrayList<Node>> mrnas_for_genes_at_same_pos) {
        for (Node gene_node : gene_nodes_list) {
            Iterable<Relationship> gene_rels = gene_node.getRelationships(RelTypes.codes_for);
            for (Relationship rel : gene_rels) {
                Node mrna_node = rel.getEndNode();
                try_incr_AL_hashmap(mrnas_for_genes_at_same_pos, gene_nodes_list.get(0), mrna_node);
            }
        }
    }
    
    /**
      * Check if ANY of the mRNAs codes for a protein
      * @param gene_node
      * @return boolean
    */
    public static boolean check_if_coding_mrna(Node gene_node) {
        boolean coding = false;
        Iterable<Relationship> gene_rels = gene_node.getRelationships(RelTypes.codes_for);
        for (Relationship rel : gene_rels) { 
            Node mrna_node = rel.getEndNode();
            if (mrna_node.hasProperty("protein_ID")) {
                coding = true;
            }
        }
        return coding;
    }
    
    
    /**
     * 
     * @param hmgroups_list 
     */
    public void stop_if_arraylist_contains_duplicates(ArrayList<Node> hmgroups_list) {
        HashSet<Node> group_set = new HashSet<>();
        for (Node group : hmgroups_list) {
            group_set.add(group);
        }
        if (group_set.size() != hmgroups_list.size()) {
            Pantools.logger.error("There are duplicated homology group identifiers in the given input -hm.");
            System.exit(1);
        }
    }
    
    /**
     * 
     * @param hm_nodes_list
     * @return 
     */
    public HashMap<Integer, HashSet<String>> check_which_sequences_are_in_homology_groups(ArrayList<Node> hm_nodes_list) {
        HashMap<Integer, HashSet<String>> sequence_identifiers = new HashMap<>();
        for (Node hm_node : hm_nodes_list) {
            Iterable<Relationship> relationships = hm_node.getRelationships(has_homolog);
            for (Relationship rel : relationships) {
                Node mrna_node = rel.getEndNode();
                int[] address = (int[]) mrna_node.getProperty("address");
                if (skip_array[address[0]-1]) { // genome nr -1 
                    continue;
                }
                try_incr_hashset_hashmap(sequence_identifiers, address[0], address[0] + "_" + address[1]);
            }
        }
        return sequence_identifiers;
    }
    
    /**
     * 
     * @param hm_node_list
     */
    public void print_locate_genes_output(ArrayList<Node> hm_node_list) {
        String path = WORKING_DIRECTORY + "locate_genes/";

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}gene_clusters_by_size.txt", path);
        Pantools.logger.info(" {}gene_clusters_by_position.txt", path);
        if (!Mode.equals("IGNORE-COPIES")) {
            Pantools.logger.info(" {}duplicate_genes.log", path);
        }
        if (!hm_node_list.isEmpty()) {
            Pantools.logger.info(" {}compare_gene_clusters.txt", path);
            if (phenotype_map != null) {
                Pantools.logger.info(" {}phenotype_clusters.txt", path);
                for (String phenotype : phenotype_map.keySet()) {
                    if (phenotype.equals("?") || phenotype.equals("Unknown")) {
                        continue;
                    }
                    Pantools.logger.info(" {}compare_gene_clusters_{}.txt", path, phenotype);
                }
            }
        }
    } 
    
    /*
     This function is not used with --mode IGNORE-COPIES
     Do you want duplicated genes to disrupt the clusters or not?
    */
    public void remove_duplicate_genes_next_to_eachother(HashMap <Integer, String> clustered_hms_map) {
        StringBuilder duplicate_builder = new StringBuilder();
        for (int genome_nr : clustered_hms_map.keySet()) {
            StringBuilder temp_duplicate_builder = new StringBuilder();
            String all_gene_clusters = clustered_hms_map.get(genome_nr);

            int counter = 0;
            String[] all_clusters_array = all_gene_clusters.split("\n");
            StringBuilder new_cluster = new StringBuilder();
            int multiple_times_present = 0;
            for (String cluster : all_clusters_array) {
                String prev_value = "";
                StringBuilder temp_cluster = new StringBuilder();
                String[] cluster_array = cluster.split(",");
                boolean first_duplicate = true;
                for (int i = 0 ; i < cluster_array.length; i++) { // loop over the ids of the current cluster to find duplicates next to eachother
                    if (!prev_value.equals(cluster_array[i])) {
                        temp_cluster.append(cluster_array[i]).append(",");
                    } else {
                        counter ++;
                    }
                    prev_value = cluster_array[i];
                    for (int j = i + 1 ; j < cluster_array.length; j++) { // loop over array completely to find how many clusters contain duplicate ids
                        if (cluster_array[i].equals(cluster_array[j])) {
                            multiple_times_present ++;
                            if (first_duplicate) {
                                temp_duplicate_builder.append(cluster).append("\n");
                                first_duplicate = false;
                            }
                            temp_duplicate_builder.append(" ").append(cluster_array[i]).append("\n");
                        }
                    }
                }
                new_cluster.append(temp_cluster.toString().replaceFirst(".$","")).append("\n");
            }
            duplicate_builder.append("#Genome ").append(genome_nr).append(" had ").append(multiple_times_present)
                    .append(" duplicated genes (within the same cluster). ").append(counter).append(" time(s) these are next to eachother\n")
                    .append(temp_duplicate_builder.toString()).append("\n");
            clustered_hms_map.put(genome_nr, new_cluster.toString());
        }
        write_SB_to_file_in_DB( duplicate_builder, "locate_genes/duplicate_genes.log");
    }
    
    /**
     * Creates a log file of the input genome or proteome files inside DATABASE/databases/
     */
    public static void genome_overview() {
        StringBuilder output_builder = new StringBuilder();
        String genome_or_proteome = "genome.db/genomes"; 
        try (Transaction tx = GRAPH_DB.beginTx()) {
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes  
            if (PROTEOME) {
                genome_or_proteome = "proteomes";
                output_builder.append("Total genomes: ").append(total_genomes).append("\n\nGenome number, path to proteins\n");
                try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "log/panproteome_input_files.txt"))) {
                    int counter = 0;
                    for (int c = 0; in.ready();) {
                        counter ++;
                        String line = in.readLine().trim();
                        output_builder.append(counter).append(", ").append(line).append("\n");
                    }
                } catch (IOException ioe) {
                    Pantools.logger.error("Unable to read: {}log/panproteome_input_files.txt", WORKING_DIRECTORY);
                    System.exit(1);
                }
            } else { // pangenome 
                ResourceIterator<Node> genome_nodes = GRAPH_DB.findNodes(GENOME_LABEL);
                while (genome_nodes.hasNext()) { 
                    Node genome_node = genome_nodes.next();
                    long node_id = genome_node.getId();
                    int genome_nr = (int) genome_node.getProperty("number");
                    System.out.print("\rRetrieving genome information: " + genome_nr + "/" + total_genomes);
                    String genome_path = (String) genome_node.getProperty("path");
                    String date = (String) genome_node.getProperty("date");
                    long size = get_size_of_file(genome_path);
                    String filesize = b_Kb_Mb_or_Gb(size);
                    output_builder.append("Genome ").append(genome_nr)
                            .append("\nFile location: ").append(genome_path)
                            .append("\nSize: ").append(filesize)
                            .append("\nStored in node: ").append(node_id)
                            .append("\nDate added: ").append(date).append("\n");
                    ResourceIterator<Node> seq_nodes = GRAPH_DB.findNodes(SEQUENCE_LABEL, "genome", genome_nr);
                    while (seq_nodes.hasNext()) { 
                        Node seq_node = seq_nodes.next();
                        long seq_node_id = seq_node.getId();
                        String identifier = (String) seq_node.getProperty("identifier");
                        String title = (String) seq_node.getProperty("title");
                        long length = (long) seq_node.getProperty("length");
                        String length_str = b_Kb_Mb_or_Gb(length);
                        output_builder.append(" ").append(identifier).append(", ").append(length_str).append(", ")
                                .append(title).append(", ").append(seq_node_id).append("\n");
                    }
                    output_builder.append("\n");
                }
                System.out.print("\r                                                      "); // spaces are intentional
            }
            tx.success(); // transaction successful, commit changes
        }
        write_SB_to_file_in_DB(output_builder, "/databases/" + genome_or_proteome + ".txt");
    }

    /*
    You have a region and you want to know if its in a gene or overlaps with one? use this function!

    Requires
     --database-path/-dp 
     --regions-file
    
    Optional 
    --mode partial, also report genes that are only partially overlapped 

    */
    public ArrayList<Node> find_genes_in_region() {
        StringBuilder output_builder = new StringBuilder();
        ArrayList<Node> found_mrnas = new ArrayList<>();
        boolean standalone = false;
        if (PATH_TO_THE_REGIONS_FILE != null && K_SIZE == -1) {
            Pantools.logger.info("Searching for genes in the selected regions.");
            regions_to_search = read_regions_file();
            standalone = true;
            check_database(); // starts up the graph database if needed
            create_directory_in_DB("find_genes/in_region/nucleotide_sequences"); 
            create_directory_in_DB("find_genes/in_region/protein_sequences"); 
            
        } else if (K_SIZE != -1) { 
           // regions are already retrieved by other function 
        } else {
            Pantools.logger.error("No regions provided.");
            System.exit(1);
        }
        if (!Mode.contains("PARTIAL") && standalone) {
            Pantools.logger.info("--mode partial is missing. Only retrieving genes that comppletely overlap with the input regions.");
        } else if (standalone) {
            Pantools.logger.info("--mode partial is selected! Also retrieving genes that partly overlap with the input regions.");
        }
         
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome(pangenome_node, "find_genes_in_regions"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.info("Unable to start the database.");
            return null;
        }
        
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            int region_counter = 0;
            for (String region : regions_to_search) {
                StringBuilder nuc_seq_builder = new StringBuilder();
                StringBuilder prot_seq_builder = new StringBuilder();
                region_counter ++;
                if (standalone) {
                    System.out.print("\rRegion " + region_counter + "/" + regions_to_search.length);
                    output_builder.append("\nRegion ").append(region).append("\n");
                }
               
                String[] region_array = region.split(" ");
                int genome_int = Integer.parseInt(region_array[0]);
                int sequence_int = Integer.parseInt(region_array[1]);
                int start_pos = Integer.parseInt(region_array[2]);
                int end_pos = Integer.parseInt(region_array[3]);
                int front = 99999999, behind = -99999999;
                String front_gene = "", behind_gene = "";
                if (start_pos > end_pos) {
                    if (standalone) {
                        Pantools.logger.info("{} - The end position should be higher as the start position.", region);
                    }
                    continue;
                }
                
                ResourceIterator<Node> mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", genome_int);
                int gene_count = 0, correct_gene_count = 0;
                while (mrna_nodes.hasNext()) {
                    Node mrna_node = mrna_nodes.next();
                    long mrna_node_id = mrna_node.getId();
                    int[] address = (int[]) mrna_node.getProperty("address");
                    if (sequence_int != address[1]) {
                        continue;
                    }
                    boolean found = false, front_bool = false, behind_bool = false;
                    int infront = start_pos-address[3]; 
                    int behind2 = end_pos-address[2];
                    Pantools.logger.trace("Gene {} {} {}.", Arrays.toString(address), infront, behind2);
                    if (infront < front && infront > 0) { // start position of query sequence is below the end position of the target 
                        front = infront;
                        front_bool = true;
                    } else if (behind2 > behind && behind2 < 0) { // end position of query sequence is higher than the start position of the target 
                        behind = behind2;
                        behind_bool = true;
                    }

                    gene_count ++;
                    if (start_pos <= address[2] && end_pos >= address[3]) { // Gene falls in region completely
                        found = true;
                    } else if (start_pos >= address[2] && end_pos <= address[3]) { // Region is completely overlapped by gene
                        found = true;
                    } else if (start_pos >= address[2] && end_pos >= address[3] && 
                            start_pos <= address[3] && Mode.contains("PARTIAL")) { // gene starts halfway the region
                        found = true;
                    } else if (start_pos <= address[2] && end_pos <= address[3] && 
                            end_pos >= address[2] && Mode.contains("PARTIAL")) { // gene ends halfway the region
                        found = true;
                    }

                    if (found || front_bool || behind_bool) {
                        String name = retrieveNamePropertyAsString(mrna_node);
                        String nuc_sequence = get_nucleotide_sequence(mrna_node);
                        String prot_sequence = get_protein_sequence(mrna_node);
                        if (name.equals("")) {
                            name = "-";
                        }
                        String protein_id = "-";
                        if (mrna_node.hasProperty("protein_ID")) {
                            protein_id = (String) mrna_node.getProperty("protein_ID");
                        }
                        String info = protein_id + ", " + name + ", " + address[0] + ", " + mrna_node_id + ", " + nuc_sequence.length() 
                                + ", " + prot_sequence.length() + ", " + address[0] + " " + address[1] + " " + address[2] + " " + address[3];
                        if (found) {
                            found_mrnas.add(mrna_node);
                            if (standalone) {
                                //Pantools.logger.info(info);
                                nuc_sequence = split_seq_in_parts_of_80bp(nuc_sequence);
                                prot_sequence = split_seq_in_parts_of_80bp(prot_sequence);
                                output_builder.append(info).append("\n");
                                nuc_seq_builder.append(">").append(protein_id).append(" ").append(address[0]).append("\n")
                                        .append(nuc_sequence).append("\n");
                                prot_seq_builder.append(">").append(protein_id).append(" ").append(address[0]).append("\n")
                                        .append(prot_sequence).append("\n");
                            } else {
                                output_builder.append(info).append("#");
                            }
                            correct_gene_count ++;
                        } else if (front_bool) {
                            front_gene = info;
                        } else {
                            behind_gene = info;
                        }
                    }
                }
                
                if (gene_count == 0) {
                    output_builder.append(" Sequence ").append(genome_int).append("_").append(sequence_int).append(" has no annotation!");
                } else if (correct_gene_count == 0) {
                    output_builder.append("No overlap with an annotation. ");
                    if (front != 99999999) {
                        output_builder.append("Closest gene in front is ").append(front).append(" bp, ").append(front_gene).append(" & ");
                    } else {
                        output_builder.append("No gene in front of the region is found & ");
                    }

                    if (behind != -99999999) {
                        behind = Math.abs(behind); // make the number positive
                        output_builder.append("Closest gene behind is ").append(behind).append(" bp, ").append(behind_gene);
                    } else {
                        output_builder.append("No gene behind the region is found");
                    }
                }
                if (standalone) {
                   String region_underscore = region.replace(" ","_");
                   write_SB_to_file_in_DB(nuc_seq_builder, "find_genes/in_region/nucleotide_sequences/" + region_underscore + ".fasta");
                   write_SB_to_file_in_DB(prot_seq_builder, "find_genes/in_region/protein_sequences/" + region_underscore + ".fasta");
                }
            }
            tx.success(); // transaction successful, commit changes
        }
       
        if (standalone) {
            write_string_to_file_in_DB("##Gene/protein identifier, gene name, genome, node identifier, gene length, protein length, address\n" + 
                    output_builder.toString(), "find_genes/in_region/find_genes_in_region.log");
            Pantools.logger.info("Output written to:");
            Pantools.logger.info(" {}find_genes/in_region/find_genes_in_region.log", WORKING_DIRECTORY);
            Pantools.logger.info(" {}find_genes/in_region/nucleotide_sequences/", WORKING_DIRECTORY);
            Pantools.logger.info(" {}find_genes/in_region/protein_sequences/", WORKING_DIRECTORY);
        }
        return found_mrnas;
    }
    
    /**
     * Read a txt file given by --regions-file/-rf 
     * 
     * The reverse complement of a region is retrieved when a minus is placed behind it. 
     * If #reverse is found, all other regions will be reverse complement
     * 
     * @return 
     */
    public static String[] read_regions_file() {
        StringBuilder regions_builder = new StringBuilder();
        if (PATH_TO_THE_REGIONS_FILE == null) {
            Pantools.logger.error("No regions provided.");
            System.exit(1);
        }
        
        try (BufferedReader in = new BufferedReader(new FileReader(PATH_TO_THE_REGIONS_FILE))) {
            boolean reverse = false;
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (line.equals("")) {
                    continue;
                }
                if (line.startsWith("#REVERSE") || line.startsWith("#reverse")) {
                    reverse = true;
                    continue;
                }
                if (reverse) {
                    line += "-";
                }
                regions_builder.append(line + ",");
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read the --regions-file: {}", PATH_TO_THE_REGIONS_FILE);
            System.exit(1);
        }
        String[] regions_array = regions_builder.toString().split(",");
        return regions_array;
    }
    
    /*
     Requires 
      -dp 
      -node "5724707";OR -name (GO:1901891)
    */
    public void find_genes_by_annotation() {
        create_directory_in_DB("find_genes/by_annotation/nucleotide_sequences/"); 
        create_directory_in_DB("find_genes/by_annotation/protein_sequences/"); 
        Pantools.logger.info("Finding genes which share a functional annotation.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            check_if_panproteome(pangenome_node); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe ) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        
        long node_id = 0;
        HashSet<Node> function_node_list = new HashSet<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            if (NODE_ID == null && SELECTED_NAME != null) {
                function_node_list = find_functional_annotation_node();
            } else if (NODE_ID != null && SELECTED_NAME == null) {
                node_id = Long.parseLong(NODE_ID);
                Node function_node = GRAPH_DB.getNodeById(node_id);
                function_node_list.add(function_node);
            } else if (NODE_ID == null && SELECTED_NAME == null) {
                Pantools.logger.error("Provide either -node or -name.");
                System.exit(1);
            } else {
                Pantools.logger.error("Use -node OR -name.");
                System.exit(1);
            }
            annotation_get_connected_genes(function_node_list);
            tx.success(); // transaction successful, commit changes
        }
        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}find_genes/by_annotation/find_genes_by_annotation.log", WORKING_DIRECTORY);
        Pantools.logger.info(" {}find_genes/by_annotation/nucleotide_sequences/", WORKING_DIRECTORY);
        Pantools.logger.info(" {}find_genes/by_annotation/protein_sequences/", WORKING_DIRECTORY);
    }
    
    /**
     * Requires unique sequence names
     * @param shared_snps_map
     * @param variable_positions
     * @param conserved_positions
     * @param output_file
     * @param genome_order_file 
     */
    public static void create_identity_matrices(HashMap<String, Integer> shared_snps_map, int variable_positions, int conserved_positions,
                                                Path output_file, Path genome_order_file) {
        
        ArrayList<String> protein_id_list = get_full_protein_identifiers(genome_order_file, false);
        ArrayList<String> genome_order_list = read_genome_order_file(genome_order_file, false);
        int alignment_length = variable_positions + conserved_positions;
        HashMap<String, String> identity_map = new HashMap<>();
        for (int i=0; i < genome_order_list.size(); i++) {
            String genome_nr1 = genome_order_list.get(i);
            try {
                if (skip_array[Integer.parseInt(genome_nr1) - 1]) {
                    continue;
                }
            } catch (NumberFormatException ignored) {
            }
            for (int j=0; j < genome_order_list.size(); j++) {
                String genome_nr2 = genome_order_list.get(j);
                try {
                    if (skip_array[Integer.parseInt(genome_nr2) - 1]) {
                        continue;
                    }
                } catch (NumberFormatException ignored) {
                }
                long shared_snps = 0;
                if (shared_snps_map.containsKey(i + "#" + j + "#nogap")) {
                    shared_snps = shared_snps_map.get(i +"#" + j + "#nogap");
                }
                long shared_positions = 0; 
                if (shared_snps_map.containsKey(i + "#" + j)) { //includes shared gap positions
                    shared_positions = shared_snps_map.get(i + "#" + j);
                }
                long alignment_correction = shared_positions-shared_snps; // remove the number of shared gaps between two sequences from the alignment length 
                String percentage = get_percentage_str(conserved_positions + shared_snps, alignment_length-alignment_correction, 2); 
                identity_map.put(i + "#" + j, percentage);
            }
        }
        
        StringBuilder header_builder = new StringBuilder("Sequences,");
        StringBuilder output_builder = new StringBuilder();
        for (int i=0; i < protein_id_list.size(); i++) {
            String seq_name = protein_id_list.get(i);
            String genome_nr1 = genome_order_list.get(i);
            try {
                if (skip_array[Integer.parseInt(genome_nr1) - 1]) {
                    continue;
                }
            } catch (NumberFormatException ignored) {
            }

            header_builder.append(seq_name);
            if (i != protein_id_list.size()-1) {
                header_builder.append(",");
            } 
            output_builder.append(seq_name).append(",");
            for (int j=0; j < protein_id_list.size(); j++) {
                String genome_nr2 = protein_id_list.get(j);
                try {
                    if (skip_array[Integer.parseInt(genome_nr2) - 1]) {
                        continue;
                    }
                } catch (NumberFormatException ignored) {
                }
                String identity = "100"; // 100 is for sequences against itself
                if (i != j && i < j) { 
                    identity = identity_map.get(i + "#" + j);
                } else if (i != j) { // i > j
                    identity = identity_map.get(j + "#" + i);
                }
                output_builder.append(identity);
                if (j != protein_id_list.size()-1) {
                    output_builder.append(",");
                } 
            }
            output_builder.append("\n");
        }  
        write_string_to_file_full_path(header_builder + "\n" + output_builder, output_file);
    }

    /**
     * 
     * @param hmgroup_str
     * @return 
     */
    public static HashMap<String, Relationship> create_protein_id_hashmap_of_group(String hmgroup_str) {
        long hm_id = Long.parseLong(hmgroup_str);
        Node hm_node = GRAPH_DB.getNodeById(hm_id);
        Iterable<Relationship> rels = hm_node.getRelationships();
        HashMap<String, Relationship> is_similar_rels_map = new HashMap<>();
        for (Relationship rel : rels) {
            Node mrna_node1 = rel.getEndNode();
            long mrna_node_id1 = mrna_node1.getId();
            String protein_id1 = (String) mrna_node1.getProperty("protein_ID");
            int genome_nr1 = (int) mrna_node1.getProperty("genome");
            if (skip_array[genome_nr1-1]) {
                continue;
            }
            Iterable<Relationship> is_similar_rels = mrna_node1.getRelationships(RelTypes.is_similar_to); 
            // only one relation but don't know the direction of the relation yet
            for (Relationship is_similar_rel : is_similar_rels) {
                Node mrna_node2 = is_similar_rel.getEndNode();
                long mrna_node_id2 = mrna_node2.getId();
                if (mrna_node_id1 == mrna_node_id2) {
                    mrna_node2 = is_similar_rel.getStartNode();
                }
                int genome_nr2 = (int) mrna_node2.getProperty("genome");
                if (skip_array[genome_nr2-1]) {
                    continue;
                }
                String protein_id2 = (String) mrna_node2.getProperty("protein_ID");
                is_similar_rels_map.put(protein_id1 + "#" + protein_id2, is_similar_rel);
            }
        }
        return is_similar_rels_map;
    }
    
    /**
     * 
     * @param input_file
     * @return 
     */
    public static HashMap<String, Double> read_identity_similarity_matrix(String input_file) {
        HashMap<String, Double> score_map = new HashMap<>();
        try (BufferedReader in = new BufferedReader(new FileReader(input_file))) { // read original fasta files
            boolean first_line = true;
            String[] header_array = new String[0];
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim(); 
                if (first_line) { // first line is the header 
                    first_line = false;
                    header_array = line.split(",");
                } else {
                    String[] line_array = line.split(",");
                    String[] line_array2 = line_array[0].split(" ");
                    for (int i=1; i < line_array.length; i++) {
                        if (line_array[0].equals(header_array[i])) {
                            continue;
                        }
                        double score = Double.parseDouble(line_array[i]);
                        Pantools.logger.trace("{} vs {} = {}.", line_array[0], header_array[i], line_array[i]);
                        String[] header_array2 = header_array[i].split(" ");
                        score_map.put(line_array2[0] + "#" + header_array2[0], score);
                    }
                }
            }
            in.close();
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read: {}", input_file);
            System.exit(1);
        }
        return score_map;
    }

    /**
     * The default width of the FASTA format is 80
     * Put a newline character after every 80 characters
     * @param sequence
     * @return
     */
    public static String split_seq_in_parts_of_80bp(String sequence) {
        String new_string = "";
        for (int i = 0; i < sequence.length(); i++) {
            char c = sequence.charAt(i);
            new_string += c;
            if ((i+1) % 80 == 0) {
                new_string += "\n";
            }
        }
        if (new_string.endsWith("*")) {
            new_string = new_string.replaceFirst(".$","");
        }
        return new_string;
    }

    /**
     *
     * @param genome
     * @param space_in_front
     * @return
     */
    public static String get_phenotype_for_genome(int genome, boolean space_in_front) {
        String add = "";
        if (PHENOTYPE != null) {
            String pheno = "Unknown";
            if (geno_pheno_map.containsKey(genome)) {
                pheno = geno_pheno_map.get(genome);
                if (pheno.equals("?")) {
                    pheno = "Unknown";
                }
            }
            if (space_in_front) {
                add += " " + pheno;
            } else {
                add = pheno;
            }
        }
        return add;
    }

    /**
     *
     * @param genome
     * @param space_in_front
     * @return
     */
    public static String get_phenotype_for_genome(String genome, boolean space_in_front) {
        try {
            int genome_nr = Integer.parseInt(genome);
            return get_phenotype_for_genome(genome_nr, space_in_front);
        } catch (NumberFormatException e) {
            if (space_in_front) {
                return " Unknown";
            } else {
                return "Unknown";
            }
        }
    }

    /**
     * Read the CLUSTAL formatted output from MAFFT (.afa file extension)
     * @param input_file
     * @param sequence_array
     * @param total_sequences
     * @return list with variable positions. Last position added to snp_positions is the length of the alignment
     */
    public static ArrayList<Integer> read_mafft_alignment(Path input_file, String[] sequence_array, int total_sequences) {
        ArrayList<Integer> variable_positions = new ArrayList<>();
        int total_position = 0;
        try (BufferedReader in = Files.newBufferedReader(input_file)) {
            int line_counter = 0, seq_counter = 0;
            boolean first_round = true;
            for (int c = 0; in.ready();) {
                String line = in.readLine(); //important that read is not trimmed
                line_counter ++;
                if (line_counter < 4) {
                    continue;
                }
                seq_counter ++;
                if (seq_counter == total_sequences+1) { // first row after sequences with **
                    first_round = false;
                    for (int i = 16; i < line.length(); i++) {
                        char aa = line.charAt(i);
                        if (aa != '*') {
                            variable_positions.add(total_position);
                        }
                        total_position ++;
                    }
                    continue;
                }
                if (seq_counter == total_sequences+2) { // always an empty line
                    seq_counter = 0;
                    continue;
                }
                String[] line_array = line.split(" ");
                if (first_round) {
                    sequence_array[seq_counter-1] = line_array[line_array.length-1];
                } else {
                    sequence_array[seq_counter-1] += line_array[line_array.length-1];
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read: {}", input_file);
            System.exit(1);
        }
        variable_positions.add(total_position); // length of the alignment is added as last value
        return variable_positions;
    }

    public static String[][] create_all_seq_position_array(String[] sequence_array, ArrayList<String> genome_order_list,
            int[] sequence_skip_counter, HashSet<String> genome_set) {

        String[][] all_seq_position_array = new String[genome_order_list.size()][0];
        int sequence_counter = 0;
        for (String sequence : sequence_array) {
            sequence_counter ++;
            String genome_nr = genome_order_list.get(sequence_counter-1);
            try {
                if (skip_array[Integer.parseInt(genome_nr) - 1]) {
                    sequence_skip_counter[1]++;
                    continue;
                }
            } catch (NumberFormatException ignored) {
            }
            all_seq_position_array[sequence_counter-1] = sequence.split("");
            genome_set.add(genome_nr);
        }
        sequence_skip_counter[0] = sequence_counter;
        return all_seq_position_array;
    }

    /**
     * A site is parsimony-informative if it contains at least two types of nucleotides (or amino acids), and at least two of them occur with a minimum frequency of two.
     * @param pos_count_map
     * @return
     */
    public static boolean check_if_informative_position(HashMap<String, Integer> pos_count_map) {
        int two_or_more_counter = 0;
        for (String nucleotide_key : pos_count_map.keySet()) {
            if (nucleotide_key.equals("-")) {
                continue;
            }
            int occurrence = pos_count_map.get(nucleotide_key);
            if (occurrence >= 2) {
                two_or_more_counter ++;
            }
        }
        if (two_or_more_counter >= 2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param genome_order
     * @param use_skip_genomes
     * @return
     */
    public static ArrayList<String> read_genome_order_file(Path genome_order, boolean use_skip_genomes) {
        ArrayList<String> genome_nrs_list = new ArrayList<>();
        try (BufferedReader in = Files.newBufferedReader(genome_order)) {
            while (in.ready()) {
                String line = in.readLine().trim();
                String[] line_array = line.split(",");
                try {
                    int genome_nr = Integer.parseInt(line_array[0]);
                    if (use_skip_genomes && skip_array[genome_nr-1]) {
                        continue;
                    }
                    genome_nrs_list.add(line_array[0]);
                } catch (NumberFormatException nfe) {
                    genome_nrs_list.add(line_array[0]);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return genome_nrs_list;
    }

    /**
     *
     * @param genome_order_file
     * @param use_skip_genomes
     * @return
     */
    public static ArrayList<String> get_full_protein_identifiers(Path genome_order_file, boolean use_skip_genomes) {
        ArrayList<String> genome_nrs_list = new ArrayList<>();

        try (BufferedReader in = Files.newBufferedReader(genome_order_file)) {
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.contains(",")) {
                    String[] line_array = line.split(",");
                    if (use_skip_genomes) {
                        int genome_nr = Integer.parseInt(line_array[0]);
                        if (skip_array[genome_nr-1]) {
                            continue;
                        }
                    }
                    genome_nrs_list.add(line_array[1]);
                } else {
                    Pantools.logger.error("not allowed.");
                    System.exit(1);
                }
            }
        } catch (IOException e) {
            Pantools.logger.error(e.getMessage());
            System.exit(1);
        }
        return genome_nrs_list;
    }

    /**
     *
     * @param shared_snps_map
     * @param input_file
     * @param hmgroup
     * @param alignmentTypeShort
     * @param genome_order_file
     * @param trimmedStr
     * @return
     */
    public static int[] count_var_inf_sites_in_msa(HashMap<String, Integer> shared_snps_map, Path input_file,
                                                   String hmgroup, String alignmentTypeShort, Path genome_order_file,
                                                   String trimmedStr) {


        ArrayList<String> genome_order_list = read_genome_order_file(genome_order_file, false);

        String [] sequence_array = new String[genome_order_list.size()]; // sequences are added in next function
        ArrayList<Integer> all_variable_positions = read_mafft_alignment(input_file, sequence_array, genome_order_list.size());
        int[] sequence_skip_counter = new int[]{0,0}; // total sequences, total skipped sequences
        HashSet<String> genome_set = new HashSet<>();
        String[][] all_seq_position_array = create_all_seq_position_array(sequence_array, genome_order_list, sequence_skip_counter, genome_set);
        ArrayList<String> variable_positions = new ArrayList<>();
        ArrayList<String> uninformative_positions = new ArrayList<>();
        int alignment_length = all_variable_positions.get(all_variable_positions.size()-1); // -1 as the last value is the alignment length
        AtomicInteger conserved_sites = new AtomicInteger(alignment_length - (all_variable_positions.size()-1));
        LinkedHashMap<String, ArrayList<Integer>> position_map = new LinkedHashMap<>(); // key is , value are sequence numbers
        count_characters_per_position_of_msa(all_variable_positions, variable_positions, uninformative_positions, conserved_sites, alignmentTypeShort, all_seq_position_array,
                genome_order_list, sequence_skip_counter[0], position_map, shared_snps_map);

        String informative_sites_overview = update_shared_snp_map(variable_positions, uninformative_positions, position_map, sequence_skip_counter[0], shared_snps_map);
        create_matrix_with_var_inf_positions(variable_positions, uninformative_positions, position_map, alignmentTypeShort, input_file.getParent(), trimmedStr);
        StringBuilder alignment_info = prepare_alignment_info(hmgroup, sequence_skip_counter[0], sequence_skip_counter[1], alignment_length, genome_set, input_file,
                conserved_sites.get(), informative_sites_overview, variable_positions, uninformative_positions);

        write_SB_to_file_full_path(alignment_info, input_file.getParent().resolve(alignmentTypeShort + trimmedStr + "_alignment.info"));
        return new int[] {variable_positions.size(), variable_positions.size()-uninformative_positions.size(), conserved_sites.get()}; // variable, informative, conserved
    }

    /**
     *
     * @param all_variable_positions
     * @param final_variable_positions
     * @param uninformative_positions
     * @param conserved_sites
     * @param alignmentTypeShort
     * @param all_seq_position_array
     * @param genome_order_list
     * @param total_sequences
     * @param position_map
     * @param shared_snps_map
     */
    public static void count_characters_per_position_of_msa(ArrayList<Integer> all_variable_positions,
                                                            ArrayList<String> final_variable_positions,
                                                            ArrayList<String> uninformative_positions,
                                                            AtomicInteger conserved_sites, String alignmentTypeShort,
                                                            String[][] all_seq_position_array, ArrayList<String> genome_order_list,
                                                            int total_sequences, LinkedHashMap<String, ArrayList<Integer>> position_map,
                                                            HashMap<String, Integer> shared_snps_map) {

        for (int l = 0; l < all_variable_positions.size()-1; l++) { //-1 because last number is the alignment length
            int snp_pos = all_variable_positions.get(l);
            HashMap<String, Integer> pos_count_map = new HashMap<>();
            for (int i = 0; i < total_sequences; i++) {
                String genome_nr = genome_order_list.get(i);
                try {
                    if (skip_array[Integer.parseInt(genome_nr) - 1]) {
                        continue;
                    }
                } catch (NumberFormatException ignored) {
                }
                String character = all_seq_position_array[i][snp_pos];
                char char1 = character.charAt(0);
                try_incr_AL_hashmap(position_map, snp_pos + character, i);
                try_incr_hashmap(pos_count_map, character, 1);
                if (alignmentTypeShort.equals("prot")) {
                    calculate_sequence_similarity(i, total_sequences, all_seq_position_array, snp_pos, char1, genome_order_list, shared_snps_map);
                }
            }
            if (pos_count_map.size() == 1) { // important! due skipping of sequences, a position is no longer variable
                conserved_sites.getAndIncrement();
            } else {
                final_variable_positions.add(snp_pos +"");
                boolean informative = check_if_informative_position(pos_count_map);
                if (!informative) {
                    uninformative_positions.add(snp_pos +"");
                }
            }
        }
    }

    /**
     * Creates a csv formatted table with the counts of A, T, C, G, or gap for every variable position in the alignment
     * Possible output files:
     *  - nuc_variable_positions.csv
     *  - prot_variable_positions.csv
     *  - prot_trimmed_variable_positions.csv
     *  - nuc_trimmed_variable_positions.csv
     *
     * @param variable_positions
     * @param uninformative_positions
     * @param position_map
     * @param alignmentTypeShort
     * @param output_path
     * @param trim

     */
    public static void create_matrix_with_var_inf_positions(ArrayList<String> variable_positions, ArrayList<String> uninformative_positions,
            LinkedHashMap<String, ArrayList<Integer>> position_map, String alignmentTypeShort, Path output_path, String trim) {

        if (output_path.resolve("mlsa").resolve("input").toFile().exists()) { // do not create this file when running the mlsa_concatenate function
            return;
        }
        StringBuilder matrix_builder = new StringBuilder();
        int[] counts_per_position;// = new int[5];

        if (alignmentTypeShort.equals("prot")) {
            counts_per_position = new int[22]; // A R N D C Q E G H I L K M F P S T W Y V - other
            matrix_builder.append("Position,Informative,A,R,N,D,C,Q,E,G,H,I,L,K,M,F,P,S,T,W,Y,V,gap,other\n");
        } else {
            counts_per_position = new int[6]; // a t c g - other //TODO: take IUPAC bases into account
            matrix_builder.append("Position,Informative,A,T,C,G,gap,other\n");
        }

        String prev_position = "";
        for (String position_character_key : position_map.keySet()) {
            ArrayList<Integer> sequence_numbers = position_map.get(position_character_key);

            String position = position_character_key.replaceFirst(".$",""); // remove last character
            String character = position_character_key.replace(position, "");

            if (!prev_position.equals(position) && !prev_position.equals("")) {
                int pos_int = Integer.parseInt(prev_position);
                if (uninformative_positions.contains(prev_position)) {
                    matrix_builder.append((pos_int+1)).append(",,").append(Arrays.toString(counts_per_position).replace("[","") .replace("]","").replace(" ","")).append("\n");
                } else {
                    matrix_builder.append((pos_int+1)).append(",true,").append(Arrays.toString(counts_per_position).replace("[","") .replace("]","").replace(" ","")).append("\n");
                }
                if (alignmentTypeShort.equals("prot")) {
                    counts_per_position = new int[22]; // A R N D C Q E G H I L K M F P S T W Y V - other
                } else {
                    counts_per_position = new int[6]; // a t c g - other
                }
            }

            if (!variable_positions.contains(position)) {
                continue;
            }

            if (alignmentTypeShort.equals("prot")) { // A R N D C Q E G H I L K M F P S T W Y V gap other
               if (character.equals("A")) {
                    counts_per_position[0] += sequence_numbers.size();
                } else if (character.equals("R")) {
                    counts_per_position[1] += sequence_numbers.size();
                } else if (character.equals("N")) {
                    counts_per_position[2] += sequence_numbers.size();
                } else if (character.equals("D")) {
                    counts_per_position[3] += sequence_numbers.size();
                } else if (character.equals("C")) {
                    counts_per_position[4] += sequence_numbers.size();
                } else if (character.equals("Q")) {
                    counts_per_position[5] += sequence_numbers.size();
                } else if (character.equals("E")) {
                    counts_per_position[6] += sequence_numbers.size();
                } else if (character.equals("G")) {
                    counts_per_position[7] += sequence_numbers.size();
                } else if (character.equals("H")) {
                    counts_per_position[8] += sequence_numbers.size();
                } else if (character.equals("I")) {
                    counts_per_position[9] += sequence_numbers.size();
                } else if (character.equals("L")) {
                    counts_per_position[10] += sequence_numbers.size();
                } else if (character.equals("K")) {
                    counts_per_position[11] += sequence_numbers.size();
                } else if (character.equals("M")) {
                    counts_per_position[12] += sequence_numbers.size();
                } else if (character.equals("F")) {
                    counts_per_position[13] += sequence_numbers.size();
                } else if (character.equals("P")) {
                    counts_per_position[15] += sequence_numbers.size();
                } else if (character.equals("S")) {
                    counts_per_position[15] += sequence_numbers.size();
                } else if (character.equals("T")) {
                    counts_per_position[16] += sequence_numbers.size();
                }else if (character.equals("W")) {
                    counts_per_position[17] += sequence_numbers.size();
                }else if (character.equals("Y")) {
                    counts_per_position[18] += sequence_numbers.size();
                }else if (character.equals("V")) {
                    counts_per_position[19] += sequence_numbers.size();
                } else if (character.equals("-")) {
                   counts_per_position[20] += sequence_numbers.size();
                } else {
                    counts_per_position[21] += sequence_numbers.size();
                }

            } else {
                if (character.equals("A")) {
                    counts_per_position[0] += sequence_numbers.size();
                } else if (character.equals("T")) {
                    counts_per_position[1] += sequence_numbers.size();
                    } else if (character.equals("C")) {
                    counts_per_position[2] += sequence_numbers.size();
                } else if (character.equals("G")) {
                    counts_per_position[3] += sequence_numbers.size();
                } else if (character.equals("-")) {
                    counts_per_position[4] += sequence_numbers.size();
                } else {
                    counts_per_position[5] += sequence_numbers.size();
                }
            }
            prev_position = position;
        }

        if (!prev_position.equals("")) { // there are variable positions in the alignment
            int pos_int = Integer.parseInt(prev_position);
            if (uninformative_positions.contains(prev_position)) {
                matrix_builder.append((pos_int+1)).append(",,").append(Arrays.toString(counts_per_position).replace("[","") .replace("]","").replace(" ","")).append("\n");
            } else {
                matrix_builder.append((pos_int+1)).append(",true,").append(Arrays.toString(counts_per_position).replace("[","") .replace("]","").replace(" ","")).append("\n");
            }
        } else { // no variable positions
            matrix_builder = new StringBuilder("Alignment does not contain variable positions");
        }
        output_path.resolve("var_inf_positions").toFile().mkdirs(); // create directory
        write_SB_to_file_full_path(matrix_builder, output_path.resolve("var_inf_positions").resolve(alignmentTypeShort + trim + "_variable_positions.csv"));
    }

    /**
     * Available keys: sequence combi (1), sequence combi + '#inf' (2), sequence combi + '#nogap' (3), sequence combi + "#similar" (4)
     * @param final_variable_positions
     * @param uninformative_positions
     * @param position_map
     * @param total_sequences
     * @param shared_snps_map
     * @return
     */
    public static String update_shared_snp_map(ArrayList<String> final_variable_positions, ArrayList<String> uninformative_positions,
            LinkedHashMap<String, ArrayList<Integer>> position_map, int total_sequences, HashMap<String, Integer> shared_snps_map) {

        StringBuilder inf_pos_overview = new StringBuilder();
        String prev_position = "";
        StringBuilder values_per_position = new StringBuilder();
        for (String position_character_key : position_map.keySet()) {
            ArrayList<Integer> sequence_numbers = position_map.get(position_character_key);
            ArrayList<Integer> already_added = new ArrayList<>();
            boolean informative = true;
            String position = position_character_key.replaceFirst(".$",""); // remove last character
            String character = position_character_key.replace(position, "");
            if (!final_variable_positions.contains(position)) {
                continue;
            }
            if (uninformative_positions.contains(position)) {
                informative = false;
            }

            if (!prev_position.equals(position) && informative) {
                inf_pos_overview.append(values_per_position.toString().replaceFirst(".$","").replaceFirst(".$",""));
                values_per_position = new StringBuilder();
                int pos_int = Integer.valueOf(position);
                values_per_position.append("\n").append((pos_int+1)).append(": ");
                prev_position = position;
            }
            if (informative) {
                values_per_position.append(character).append(" ").append(sequence_numbers.size()).append(", ");
            }
            for (int sequence_nr : sequence_numbers) {
                for (int i = 0; i < total_sequences; i++) {
                    String sequence_combi_key = i + "#" + sequence_nr;
                    if (already_added.contains(i)) {
                        continue;
                    }
                    if (i > sequence_nr) {
                       sequence_combi_key = sequence_nr + "#" + i;
                    }
                    if (sequence_numbers.contains(i)) {
                        try_incr_hashmap(shared_snps_map, sequence_combi_key, 1);
                    }

                    if (informative && sequence_numbers.contains(i)) {
                        try_incr_hashmap(shared_snps_map, sequence_combi_key + "#inf", 1);
                    }
                    if (!character.equals("-") && sequence_numbers.contains(i)) {
                       try_incr_hashmap(shared_snps_map, sequence_combi_key + "#nogap", 1);
                    }
                }
                already_added.add(sequence_nr);
            }
        }
        inf_pos_overview.append(values_per_position.toString());
        return inf_pos_overview.toString();
    }

    /**
     *
     * @param hmgroup
     * @param total_sequences
     * @param skipped_sequences
     * @param ali_length
     * @param genome_set
     * @param input_file
     * @param conserved_sites
     * @param informative_sites_overview
     * @param variable_positions
     * @param skip_positions
     * @return
     */
    public static StringBuilder prepare_alignment_info(String hmgroup, int total_sequences, int skipped_sequences,
                                                       int ali_length, HashSet<String> genome_set, Path input_file,
                                                       int conserved_sites, String informative_sites_overview,
                                                       ArrayList<String> variable_positions,
                                                       ArrayList<String> skip_positions) {
        String info;
        if (!hmgroup.equals("MSA")) { // hmgroup is a node identifier
            info = "\nHomology group " + hmgroup + ", members: " + total_sequences + "\n#" + input_file + "\n";
        } else if (hmgroup.equals("MSA")) {
            info = "# MSA of " + regions_to_search.length + " regions, alignment length: " + ali_length + "\n";
        } else {
            info = "";
        }
        String add = "";
        if (skipped_sequences > 0) {
            add = " (actual number is higher but some were skipped)";
        }
        StringBuilder alignment_info = new StringBuilder();
        alignment_info.append("Number of sequences        : ").append(total_sequences - skipped_sequences).append(add)
                .append("\nNumber of genomes          : ").append(genome_set.size()).append(add)
                .append("\nAlignment length           : ").append((variable_positions.size() + conserved_sites))
                .append("\nTotal variable positions   : ").append(variable_positions.size())
                .append("\nTotal informative positions: ").append(variable_positions.size()-skip_positions.size())
                .append("\nConserved (*) positions    : ").append(conserved_sites);

        if (informative_sites_overview.length() > 0) {
            alignment_info.append("\n\nInformative positions: ").append(informative_sites_overview).append("\n");
        }
        return alignment_info;
    }

    /**
     * Find similarity of characters based on blosum62
     * @param i
     * @param sequence_counter
     * @param all_positions
     * @param snp_pos
     * @param char1
     * @param genome_order_list
     * @param shared_snps_map
     */
    public static void calculate_sequence_similarity(int i, int sequence_counter, String[][] all_positions, int snp_pos, char char1,
            ArrayList<String> genome_order_list, HashMap<String, Integer> shared_snps_map) {

        for (int j = i+1; j < sequence_counter; j++) { // j = i + 1 to prevent comparison against itself
            String genome_nr2 = genome_order_list.get(j);
            try {
                if (skip_array[Integer.parseInt(genome_nr2) - 1]) {
                    continue;
                }
            } catch (NumberFormatException ignored) {
            }
            String character2 = all_positions[j][snp_pos];
            char char2 = character2.charAt(0);

            int score = match[char1][char2];
            if (score > 0) {
                try_incr_hashmap(shared_snps_map, i + "#" + j + "#similar", 1);
            }
        }
    }

    /**
     *
     * @param seq_trim_map
     * @param gene_name
     * @param path
     * @param nucleotide
     */
    public static void trim_sequences(HashMap<String, int[]> seq_trim_map, String gene_name, String path, boolean nucleotide) {
        StringBuilder output_builder = new StringBuilder();
        String new_path = path.replace(".fasta", "_trimmed.fasta");
        int longest_trim = 0, shortest_trim = 999999;
        try {
            BufferedReader in = new BufferedReader(new FileReader(path));
            String sequence = "", prev_seq = "";
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.equals("")) {
                      continue;
                }

                if (line.contains(">")) {
                    if (prev_seq.length() > 1) {
                        int[] trim_array = seq_trim_map.get(prev_seq);
                        int trim_sum = trim_array[0] + trim_array[1];
                        if (trim_sum < shortest_trim && trim_sum != 0) {
                            shortest_trim = trim_sum;
                        }
                        if (trim_sum > longest_trim) {
                            longest_trim = trim_sum;
                        }
                        output_builder.append(prev_seq).append("\n");
                        trim_sequence(sequence, trim_array, output_builder, nucleotide);
                    }
                    prev_seq = line;
                    sequence = "";
                } else {
                    sequence += line;
                }
            }

            int[] trim_array = seq_trim_map.get(prev_seq);
            int trim_sum = trim_array[0] + trim_array[1];
            if (trim_sum < shortest_trim && trim_sum != 0) {
                shortest_trim = trim_sum;
            }
            if (trim_sum > longest_trim) {
                longest_trim = trim_sum;
            }
            output_builder.append(prev_seq).append("\n");
            trim_sequence(sequence, trim_array, output_builder, nucleotide);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        if (nucleotide) {
            longest_trim = longest_trim*3;
            shortest_trim = shortest_trim*3;
        }
        if (longest_trim < shortest_trim) {
            shortest_trim = longest_trim;
        }
        write_SB_to_file_full_path(output_builder, new_path);
    }

     public static void trim_sequence(String sequence, int[] trim_array, StringBuilder output_builder, boolean nucleotide) {
        int nuc_count = 1, nuc_count_loop = 0;
        int remove_nucs_start = trim_array[0];
        int remove_nucs_end = trim_array[1];
        if (nucleotide) {
            remove_nucs_start = remove_nucs_start*3;
            remove_nucs_end = remove_nucs_end*3;
        }
        remove_nucs_end = sequence.length() - remove_nucs_end; // adjust the ending with the number of nucleotides
        Pantools.logger.trace("{} {}", remove_nucs_start, remove_nucs_end);
        String trimmed_seq = "";
        for (int i = 0; i < sequence.length(); i++) {
            char aa = sequence.charAt(i);
            nuc_count_loop ++;
            if (nuc_count <= remove_nucs_start) {

            } else if (nuc_count > remove_nucs_end) {

            } else {
                trimmed_seq += aa;
            }
            nuc_count ++;
            if (nuc_count_loop == 3) {
                nuc_count_loop = 0;
            }
        }
        trimmed_seq = split_seq_in_parts_of_80bp(trimmed_seq);
        output_builder.append(trimmed_seq).append("\n");
    }

     /**
      * This function finds how many amino acids per sequence needs to be removed based on the longest start and stop gap lengths
      * 1. identify longest start and end gap in any sequence
      * 2. count number of gaps in all sequences according to the longest gaps
      *
      * @param prot_fasta_file
      * @param seq_trim_map
      * @return int[] array of length 3: gaps at start, gaps at end, alignment length
      */
    public static int[] count_aa_to_trim(Path prot_fasta_file, HashMap<String, int[]> seq_trim_map) {
        HashMap<String, String> sequence_map = new HashMap<>(); // key is a fasta header, value is the sequence
        int alignment_length = read_fasta_for_sequence_and_gaps(prot_fasta_file, seq_trim_map, sequence_map );
        int longest_start_gap = 0, longest_end_gap = 0;
        for (String sequence_name : seq_trim_map.keySet()) {
            int[] gaps = seq_trim_map.get(sequence_name);
            if (gaps[0] > longest_start_gap) {
                longest_start_gap = gaps[0];
            }
            if (gaps[1] > longest_end_gap) {
                longest_end_gap = gaps[1];
            }
        }
        for (String sequence_name : seq_trim_map.keySet()) {
            int[] final_start_end_gaps = find_number_of_gaps_without_break(sequence_map.get(sequence_name),
                    longest_start_gap, longest_end_gap); // only find gaps in longest start and end gap
            seq_trim_map.put(sequence_name, final_start_end_gaps);
        }
        return new int[] {longest_start_gap, longest_end_gap, alignment_length}; // gaps at start, gaps at end, alignment length
    }

    /**
     * find the longest start and end gap for each sequence
     *
     * @param prot_fasta_file
     * @param seq_trim_map
     * @param sequence_map
     * @return
     */
    public static int read_fasta_for_sequence_and_gaps(Path prot_fasta_file, HashMap<String, int[]> seq_trim_map, HashMap<String, String> sequence_map) {
        int alignment_length = 0;
        try (BufferedReader in = Files.newBufferedReader(prot_fasta_file)) {
            String prev_seq = "", sequence = "";
           //start_end_gap_array = {0,0};
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.equals("")) {
                    continue;
                }
                if (line.contains(">")) {
                    if (!prev_seq.equals("")) {
                        int[] start_end_gap_array = find_number_of_gaps(sequence, sequence.length(), sequence.length());
                        seq_trim_map.put(prev_seq, start_end_gap_array);
                        sequence_map.put(prev_seq, sequence);
                    }
                    prev_seq = line;
                    sequence = "";
                } else {
                    sequence += line;
                }
            }
            // when done, add the last sequence
            int[] start_end_gap_array = find_number_of_gaps(sequence, sequence.length(), sequence.length());
            alignment_length = sequence.length();
            seq_trim_map.put(prev_seq, start_end_gap_array);
            sequence_map.put(prev_seq, sequence);
        } catch (IOException e) {
            Pantools.logger.error(e.getMessage());
            System.exit(1);
        }
        return alignment_length;
    }

    /**
     * From the start and the end of the sequence, iterate over the sequence until no gap is found
     * @param sequence
     * @param pos_until_start_gap
     * @param pos_until_end_gap
     * @return
     */
    public static int[] find_number_of_gaps(String sequence, int pos_until_start_gap, int pos_until_end_gap) {
        int start_gap_counter = 0, end_gap_counter = 0;
        for (int i = 0; i < pos_until_start_gap; i++) {
            char aa = sequence.charAt(i);
            if (aa == '-') {
                start_gap_counter ++;
            } else {
                break;
            }
        }
        for (int i = sequence.length()-1; i > 0; i--) {
            char aa = sequence.charAt(i);
            if (aa == '-') {
                end_gap_counter ++;
            } else{
                break;
            }
        }
        return new int []{start_gap_counter, end_gap_counter};
    }

    /**
     * Based on the longest start and end gaps that were found earlier, count the number of gaps for each sequence
     * @param sequence
     * @param pos_until_start_gap
     * @param pos_until_end_gap
     * @return
     */
    public static int[] find_number_of_gaps_without_break(String sequence, int pos_until_start_gap, int pos_until_end_gap) {
        int start_gap_counter = 0, end_gap_counter = 0;
        for (int i = 0; i < pos_until_start_gap; i++) {
            char aa = sequence.charAt(i);
            if (aa != '-') {
                start_gap_counter ++;
            }
        }

        int end_position = sequence.length()-1-pos_until_end_gap;
        for (int i = sequence.length()-1; i > end_position; i--) {
            char aa = sequence.charAt(i);
            if (aa != '-') {
                end_gap_counter ++;
            }
        }
        return new int[]{start_gap_counter, end_gap_counter};
    }

    /**
     *
     * @param output_builder metrics.txt file
     * @param genome_csv_builder metrics_per_genome.csv file
     * @param genome_nr
     * @param sequence_values
     * @param num_proteins
     * @param hmgroup_counts [genome numbers][unique genes, homology groups, homologous genes]
     */
    public void write_pangenome_anno_stats_to_builders(StringBuilder output_builder, StringBuilder genome_csv_builder, int genome_nr, double[] sequence_values,
            int num_proteins, int[][] hmgroup_counts) {

        double[] mrna_values = count_occurrence_total_longest_shortest(MRNA_LABEL, genome_nr);
        ResourceIterator<Node> mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", genome_nr);
        int syntenic_nodes = 0;
        while (mrna_nodes.hasNext()) {
            Node mrna_node = mrna_nodes.next();
            if (mrna_node.hasProperty("is_syntenic")) {
                syntenic_nodes ++;
            }
        }

        double[] gene_values = count_occurrence_total_longest_shortest(GENE_LABEL, genome_nr);
        // array of length 8 [total length, count, shortest, longest, median, average, stdev, longest transcript node count]
        double[] cds_values = count_occurrence_total_longest_shortest(CDS_LABEL, genome_nr);
        double[] exon_values = count_occurrence_total_longest_shortest(EXON_LABEL, genome_nr);
        double[] intron_values = count_occurrence_total_longest_shortest(INTRON_LABEL, genome_nr);
        double[] trna_values = count_occurrence_total_longest_shortest(TRNA_LABEL, genome_nr);
        double[] rrna_values = count_occurrence_total_longest_shortest(RRNA_LABEL, genome_nr);
        double[] repeat_values = count_occurrence_total_longest_shortest(REPEAT_LABEL, genome_nr);
        if (gene_values[0] == 0) {
            output_builder.append("\nNumber of annotated genes: 0");
        }
        double genome_size = (long) sequence_values[0];
        write_specific_anno_stats_to_builders("gene", gene_values, genome_size, output_builder, genome_csv_builder);
        //output_builder.append("\nProtein coding mRNAs: ").append(num_proteins);
        output_builder.append("\nHomologous mRNAs: ").append(hmgroup_counts[genome_nr-1][2])
                .append("\nSingleton mRNAs: ").append(hmgroup_counts[genome_nr-1][0])
                .append("\nNumber of homology groups mRNAs are clustered in: ").append(hmgroup_counts[genome_nr-1][1]);
        if (syntenic_nodes > 0) {
            output_builder.append("\nSyntenic mRNAs: ").append(syntenic_nodes);
        }
        write_specific_anno_stats_to_builders("mRNA", mrna_values, genome_size, output_builder, genome_csv_builder);
        write_specific_anno_stats_to_builders("CDS", cds_values, genome_size, output_builder, genome_csv_builder);
        write_specific_anno_stats_to_builders("exon", exon_values, genome_size, output_builder, genome_csv_builder);
        write_specific_anno_stats_to_builders("intron", intron_values, genome_size, output_builder, genome_csv_builder);
        write_specific_anno_stats_to_builders("tRNA", trna_values, genome_size, output_builder, genome_csv_builder);
        write_specific_anno_stats_to_builders("rRNA", rrna_values, genome_size, output_builder, genome_csv_builder);
        write_specific_anno_stats_to_builders("repeat", repeat_values, genome_size, output_builder, genome_csv_builder);
        genome_csv_builder.append(hmgroup_counts[genome_nr-1][1]).append(",").append(hmgroup_counts[genome_nr-1][0]).append(",");
    }

    /**
     *
     * @param type
     * @param values [total length, count, shortest, longest, median, average, stdev]
     * @param genome_size
     * @param output_builder metrics.txt file
     * @param csv_builder metrics_per_genome.csv file
    */
    public void write_specific_anno_stats_to_builders(String type, double[] values, double genome_size, StringBuilder output_builder, StringBuilder csv_builder) {
        DecimalFormat df0 = new DecimalFormat("0");
        DecimalFormat df2 = new DecimalFormat("0.00");
        boolean longest_transcripts = false;
        String type_capitalised = type;
        if (!type_capitalised.equals("mRNA") && !type_capitalised.equals("rRNA") && !type_capitalised.equals("tRNA")) {
           type_capitalised = type.substring(0, 1).toUpperCase() + type.substring(1);
        }
        if (type.equals("mRNA") || type.equals("CDS") || type.equals("intron") || type.equals("exon")) {
            longest_transcripts = true;
        }

        int count = (int) values[1];
        String shortest_str = b_Kb_Mb_or_Gb(values[2]);
        String longest_str = b_Kb_Mb_or_Gb(values[3]);
        String covered_pc = get_percentage_str(values[0], genome_size, 2);
        String average = df2.format(values[5]);
        String median = df0.format(values[4]);
        String stdev = df2.format(values[6]);
        int lt_count = (int) values[7];

        String size_str = b_Kb_Mb_or_Gb(values[0]);
        if (count > 0) {
            output_builder.append("\nTotal ").append(type).append(" count: ").append(count).append("\n");
            if (longest_transcripts) {
                output_builder.append("Further statistics are calculated from the ").append(type).append("s belonging to the longest transcripts\n")
                        .append(type_capitalised).append(" count (longest transcripts): ").append(lt_count).append("\n");
            }
            output_builder.append("Total ").append(type).append(" length: ").append(size_str).append("\n")
                    .append("Shortest ").append(type_capitalised).append(": ").append(shortest_str).append("\n")
                    .append("Longest ").append(type_capitalised).append(": ").append(longest_str).append("\n")
                    .append("Average ").append(type).append(" length: ").append(average).append("\n")
                    .append("Standard deviation ").append(type).append(" length: ").append(stdev).append("\n")
                    .append("Median ").append(type).append(" length: ").append(median).append("\n")
                    .append("Genome covered by ").append(type).append("s: ").append(covered_pc).append("%\n");
        }

        csv_builder.append(count).append(",");
        if (longest_transcripts) {
            csv_builder.append(lt_count).append(",");
        }

        csv_builder.append(size_str).append(",").append(shortest_str).append(",").append(longest_str).append(",")
                .append(average).append(",").append(median).append(",").append(covered_pc).append(",");

        if (type.equals("gene") || type.equals("mRNA") || type.equals("repeat") ) { // the number of genes/mrnas per million base pairs
            double density = calculate_density(genome_size, count);
            if (count > 0) {
                output_builder.append(type_capitalised).append(" density per Mbp: ").append(density).append("\n");
            }
            csv_builder.append(density).append(",");
        }
    }

    /**
     *
     * @param output_builder metrics.txt file
     * @param genome_csv_builder metrics_per_genome.csv file
     * @param genome_nr
     * @param hmgroup_counts
     */
    public static void write_panproteome_anno_stats_to_builders(StringBuilder output_builder, StringBuilder genome_csv_builder, int genome_nr, int[][] hmgroup_counts) {
        double[] mrna_values = count_occurrence_total_longest_shortest(MRNA_LABEL, genome_nr);
        // array of length 8 [total length, count, shortest, longest, median, average, stdev, longest transcript node count]
        String phenotype = "";
        if (PHENOTYPE != null) {
            phenotype = geno_pheno_map.get(genome_nr) + ",";
        }
        DecimalFormat df0 = new DecimalFormat("0");
        DecimalFormat df2 = new DecimalFormat("0.00");

        String shortest_str = b_Kb_Mb_or_Gb(mrna_values[2]);
        String longest_str = b_Kb_Mb_or_Gb(mrna_values[3]);
        String avg = df2.format(mrna_values[5]);
        String median = df0.format(mrna_values[4]);
        String size_str = b_Kb_Mb_or_Gb(mrna_values[0]);
        output_builder.append("\n\n#Genome ").append(genome_nr).append("\n");
        genome_csv_builder.append(genome_nr).append(",");
        if (mrna_values[1] > 0) {
            output_builder.append("Number of proteins: ").append(df0.format(mrna_values[1]))
                    .append("\nHomologous proteins: ").append(hmgroup_counts[genome_nr-1][2])
                    .append("\nSingleton proteins: ").append(hmgroup_counts[genome_nr-1][0])
                    .append("\nProteins cluster into number of homology groups: ").append(hmgroup_counts[genome_nr-1][1])
                    .append("\nTotal protein length: ").append(size_str)
                    .append("\nShortest protein: ").append(shortest_str)
                    .append("\nLongest protein: ").append(longest_str)
                    .append("\nAverage protein length: ").append(avg)
                    .append("\nMedian protein length: ").append(median);
        }
        genome_csv_builder.append(mrna_values[1]).append(",").append(phenotype).append(size_str).append(",").append(shortest_str).append(",").append(longest_str).append(",")
                .append(avg).append(",") .append(median).append(",").append(hmgroup_counts[genome_nr-1][1]).append(",").append(hmgroup_counts[genome_nr-1][0]).append(",");
    }

    /**
     * Calculate N25, N50, N75, N90, N95 and L25, L50, L75, L90, L95 statistics
     * @param genome_nr
     * @param genome_size
     * @return
     */
     public static long[] calculate_N_statistics(int genome_nr, long genome_size) {
        ResourceIterator<Node> sequence_nodes = GRAPH_DB.findNodes(SEQUENCE_LABEL, "genome", genome_nr);
        ArrayList<Long> sequence_lengths = new ArrayList<>();
        while (sequence_nodes.hasNext()) {
            Node seq_node = sequence_nodes.next();
            long length = (long) seq_node.getProperty("length");
            sequence_lengths.add(length);
        }
        double[] n_array = {0.25, 0.50, 0.75, 0.90, 0.95, 1.05,};
        long[] result_array = new long[10]; // N25,L25,N50,L50,N75,L75,N90,L90,N95,L95
        Collections.sort(sequence_lengths, Collections.reverseOrder());
        int counter1 = 0, counter2 = 0, sequence_counter = 0;
        long total_length = 0;
        for (long sequence_length : sequence_lengths) {
            boolean continue1 = false;
            sequence_counter ++;
            total_length += sequence_length;
            while (!continue1) {
                double expected = n_array[counter1] * genome_size;
                if (total_length > expected) {
                    result_array[counter2] = sequence_length;
                    result_array[counter2 +1]= sequence_counter;
                    counter1 ++;
                    counter2 += 2;
                } else {
                    continue1 = true;
                }
            }
        }
        return result_array;
    }

    /**
     *
     * @param standalone
     * @return
     */
    public boolean check_mode_for_ordr_matrix(boolean standalone) {
        boolean ascending_order = true;
        if (Mode.equals("DESC") || Mode.equals("DESCENDING")) {
            ascending_order = false;
            if (standalone) {
                Pantools.logger.info("Matrix values will be ordered in descending order.");
            }
        } else if ((Mode.equals("ASC") || Mode.equals("ASCENDING"))) {
            if (standalone) {
                Pantools.logger.info("Matrix values will be ordered in ascending order.");
            }
        } else if (Mode.equals("0") ) {
             if (standalone) {
                Pantools.logger.info("No --mode argument is given. Matrix values will be ordered in ascending order.");
             }
        } else {
            Pantools.logger.error("--mode {} was not recognized.", Mode);
            System.exit(1);
        }
        return ascending_order;
    }

     /**
      * Requires
      * -dp
      * -if
      *
      *
      * Matrix must contain numerical values
      * @param standalone
      */
     public void order_matrix(boolean standalone) {
        if (standalone) {
            Pantools.logger.info("Creating statistics of a matrix file.");
        }
        boolean ascending_order = check_mode_for_ordr_matrix(standalone);

        if (INPUT_FILE == null) {
            Pantools.logger.error("No matrix file was provided.");
            System.exit(1);
        }
        if (standalone) {
            check_database(); // starts up the graph database if needed
        }
        HashMap<String, HashSet<String>> values_per_phenotype = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            total_genomes = (int) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_genomes");
            create_skip_arrays(false, false); // create skip arrays for sequences and genomes if -skip/-ref is provided by user
            values_per_phenotype = retrieve_all_phenotypes();
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        HashMap<String, String> geno_pheno_map2 = new HashMap<>();
        HashMap<String, ArrayList<Double>> phenotype_distance_map = new HashMap<>();
        HashMap<Double, ArrayList<String>> value_genomes_map = new HashMap<>();
        TreeSet<Double> value_set = new TreeSet<>();
        boolean included_phenotype = false;
        boolean numbers_present = false; // genome numbers in the top and left header
        StringBuilder pheno_builder = new StringBuilder("Phenotypes that match the names in the matrix: ");
        try (BufferedReader in = new BufferedReader(new FileReader(INPUT_FILE))) {
            boolean first = true;
            int line_counter = -1;
            ArrayList<String> header_list = new ArrayList<>();
            String[] header_array = new String[0];
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                line_counter++;
                String[] line_array = line.split(",");
                if (first) {
                    included_phenotype = prepare_header_array_order_matrix(line_array, standalone, geno_pheno_map2, values_per_phenotype, header_list, pheno_builder);
                    first = false;
                    header_array = line.split(",");
                } else {
                    String[] name_array = line_array[0].split(" ");
                    try {
                        int genome_nr = Integer.parseInt(name_array[0]);
                        if (genome_nr <= total_genomes && skip_array[genome_nr-1]) {
                            continue; // the provided table provided must be lower or equal to the number of genomes in the database
                        }
                        numbers_present = true;
                    } catch (NumberFormatException nfe ) {
                         numbers_present = false;
                    }
                    String phenotype = line_array[0];
                    if (included_phenotype) {
                        phenotype = name_array[name_array.length-1];
                    }
                    for (int i = 1; i < line_array.length; i++) { // start at 1
                        if (line_counter >= i) {
                            continue;
                        }
                        if ((line_counter+1) % 100 == 0) {
                            System.out.print("\rReading file: line " + (line_counter+1));
                        }
                        String[] id_array = header_array[i].split(" ");
                        if (numbers_present) {
                            int genome_nr2 = Integer.parseInt(id_array[0]);
                            if (skip_array[genome_nr2-1]) {
                                continue;
                            }
                        }
                        double value = Double.valueOf(line_array[i]);
                        try_incr_AL_hashmap(value_genomes_map, value, header_list.get(line_counter) + "#" + header_list.get(i));
                        value_set.add(value);
                        if (included_phenotype) {
                            String target_pheno = geno_pheno_map2.get(name_array[0]);
                            if (phenotype.equals(id_array[1])) {
                                Pantools.logger.trace("{} {} {} {} {} {}.",  line_array[0], name_array[0], target_pheno, phenotype, value, Arrays.toString(id_array));
                                try_incr_AL_hashmap(phenotype_distance_map, phenotype, value);
                            }
                        }
                    }
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", INPUT_FILE);
            System.exit(1);
        }
        StringBuilder output_builder = new StringBuilder("rank;value;genome 1;genome 2");
        if (!numbers_present) {
            output_builder = new StringBuilder("rank;value;name 1;name 2");
        }
        if (included_phenotype) {
            output_builder.append(";phenotype 1;phenotype 2\n");
        } else {
            output_builder.append("\n");
        }

        DecimalFormat df = new DecimalFormat("0.00000000000");
        ArrayList<String> all_output_rows = new ArrayList<>();
        int value_counter = 0;
        for (Double value : value_set) {
            ArrayList<String> genome_combinations = value_genomes_map.get(value);
            value_counter ++;
            if (value_counter % 100 == 0) {
                System.out.print("\rOrdering values: " + value_counter + "/" + value_set.size());
            }
            for (String genome_combi : genome_combinations) {
                String[] genome_array = genome_combi.split("#");
                //Pantools.logger.info(genome_combi);
                if (included_phenotype) {
                    //int genome1 = Integer.parseInt(genome_array[0]);
                    //int genome2 = Integer.parseInt(genome_array[1]);
                    String pheno1 = geno_pheno_map2.get(genome_array[0]);
                    String pheno2 = geno_pheno_map2.get(genome_array[1]);
                    pheno1 = pheno1.replace("_", " ");
                    pheno2 = pheno2.replace("_", " ");
                    String string1 = df.format(value) + ";" + genome_array[0] + ";" + genome_array[1] + ";" + pheno1 + ";" + pheno2 + ";";
                    all_output_rows.add(string1);
                    //output_builder.append(counter).append(";").append(df.format(value)).append(";").append(genome1).append(";").append(genome2).append(";")
                    //        .append(pheno1).append(";").append(pheno2).append("\n");
                } else {
                    //output_builder.append(counter).append(";").append(df.format(value)).append(";").append(genome_array[0]).append(";").append(genome_array[1]).append("\n");
                    String string1 = df.format(value) + ";" + genome_array[0] + ";" + genome_array[1];
                    all_output_rows.add(string1);
                }
            }
        }
        int counter = 0;
        String prev = "honderd";
        if (ascending_order) {
            for (String row : all_output_rows) {
                String[] row_array = row.split(";");
                if (!row_array[0].equals(prev)) {
                    counter ++;
                }
                output_builder.append(counter).append(";").append(row).append("\n");
                prev = row_array[0];
            }
        } else {
            for (int i = all_output_rows.size() - 1; i >= 0; i--) {
                String row = all_output_rows.get(i);
                String[] row_array = row.split(";");
                if (!row_array[0].equals(prev)) {
                    counter ++;
                }
                output_builder.append(counter).append(";").append(row).append("\n");
                prev = row_array[0];
            }
        }
        write_SB_to_file_full_path(output_builder, INPUT_FILE + "_ORDERED");
        if (included_phenotype) {
            pheno_builder.append("Range of values for genomes with the same phenotype\n");
            for (Map.Entry<String, ArrayList<Double>> entry : phenotype_distance_map.entrySet()) {
                String key = entry.getKey();
                ArrayList<Double> values = entry.getValue();
                Pantools.logger.trace("{} {} {}.", key, values.size(), values);
                double lowest = Double.valueOf("9999999999999999");
                double highest = 0;
                for (Double value : values) {
                    if (value > highest) {
                        highest = value;
                    }

                    if (value < lowest) {
                        lowest = value;
                    }
                }
                if (lowest == highest) {
                    if (lowest % 1 == 0) {
                         pheno_builder.append(key).append(": ").append((int) lowest).append("\n");
                    } else {
                        pheno_builder.append(key).append(": ").append(df.format(lowest)).append("\n");
                    }
                } else {
                    if (lowest % 1 == 0 && highest % 1 == 0) {
                         pheno_builder.append(key).append(": ").append((int)lowest).append(" - ").append((int) highest).append("\n");
                    }else {
                        pheno_builder.append(key).append(": ").append(df.format(lowest)).append(" - ").append(df.format(highest)).append("\n");
                    }
                }
            }
            write_SB_to_file_full_path(pheno_builder, INPUT_FILE + "_PHENOTYPE");
        }
        if (standalone) {
            Pantools.logger.info(" {}_ORDERED", INPUT_FILE);
            if (included_phenotype) {
                Pantools.logger.info(" {}_PHENOTYPE", INPUT_FILE);
            }
        }
    }

    /**
     *
     * @param line_array
     * @param standalone
     * @param geno_pheno_map2
     * @param values_per_phenotype
     * @param header_list
     * @param pheno_builder
     * @return
     */
    public static boolean prepare_header_array_order_matrix(String[] line_array, boolean standalone, HashMap<String, String> geno_pheno_map2
            ,HashMap<String, HashSet<String>> values_per_phenotype, ArrayList<String> header_list, StringBuilder pheno_builder) {
        HashSet<String> added_phenotypes = new HashSet<>();
        String [] header_array = new String[line_array.length];
        String [] header_array_opt2 = new String[line_array.length];
        boolean included_phenotype = false;
        boolean usable_by_rename_matrix = false;
        for (int i = 1; i < line_array.length; i++) { // start at 1 because 0 is Genomes or Sequences
            String[] name_array = line_array[i].split(" ");
            try {
                int genome_nr = Integer.parseInt(name_array[0]);
                if (skip_array[genome_nr-1]) {
                    continue;
                }
                usable_by_rename_matrix = true;
            } catch (NumberFormatException nfe ) {
                  // do nothing
            }

            if (name_array.length == 1) { // no phenotype present
                header_array[i] = line_array[i];
                header_array_opt2[i] = line_array[i];
            } else {
                String name = "";
                for (int j = 0; j < name_array.length; j++) {
                    name += name_array[j] + " ";
                    if (j == name_array.length-2) {
                        header_array[i] = name.replaceFirst(".$",""); // remove last character
                    }
                }
                name = name.replaceFirst(".$",""); // remove last character
                header_array_opt2[i] = name;
                Pantools.logger.trace("{} {} {}.", i, line_array[i], name_array[name_array.length-1]);
                geno_pheno_map2.put(name_array[0], name_array[name_array.length-1]);
                included_phenotype = true;
                added_phenotypes.add(name_array[name_array.length-1]);
            }
        }
        Pantools.logger.trace("opt1 {}\nopt2 {}.", Arrays.toString(header_array), Arrays.toString(header_array_opt2));
        if (included_phenotype) { // check if all found phenotype values match one of the phenotypes in the pangenome
            included_phenotype = false;
            String correct_phenos = "";
            for (String phenotype : values_per_phenotype.keySet()) {
                boolean all_pheno_found = true;
                HashSet<String> present_pvalues = values_per_phenotype.get(phenotype);
                for (String test_pheno : added_phenotypes) {
                    if (test_pheno.equals("?") || test_pheno.equals("Unknown")) {
                        continue;
                    }
                    if (!present_pvalues.contains(test_pheno)) {
                         all_pheno_found = false;
                    }
                }
                if (all_pheno_found) {
                    correct_phenos += phenotype + ", ";
                    included_phenotype = true;
                }
            }
            pheno_builder.append(correct_phenos.replaceFirst(".$","").replaceFirst(".$","")).append("\n"); //remove last character x2
        }
        if (included_phenotype) {
            System.out.println("\rPhenotype information found in header");
            for (String name : header_array) {
                 header_list.add(name);
            }
        } else {
            if (usable_by_rename_matrix && standalone) {
                System.out.println("\rThe header does not have any phenotype information!\n"
                     + "If you want to get extra information for the phenotypes, run 'rename_matrix' and use the output file for this analys.");
            }
            for (String name : header_array_opt2) {
                 header_list.add(name);
            }
        }

        return included_phenotype;
    }

    /**
     * Is able to count all types of 'feature' nodes from GFF files.
     * @param nodeLabel
     * @param genome_nr
     * @return
     */
    public static double[] count_occurrence_total_longest_shortest(Label nodeLabel, int genome_nr) {
        String label_str = nodeLabel.toString();
        String labels_for_longest_transcripts = "CDS, mRNA, intron, exon, ";
        ArrayList<Long> lengths = new ArrayList<>();
        ResourceIterator<Node> all_nodes;
        if (PROTEOME || label_str.equals("sequence") || label_str.equals("repeat")) {
            all_nodes = GRAPH_DB.findNodes(nodeLabel, "genome", genome_nr);
        } else {
            String anno_id = annotation_identifiers.get(genome_nr-1);
            all_nodes = GRAPH_DB.findNodes(nodeLabel, "annotation_id", anno_id);
        }
        long total_length = 0, node_count = 0, shortest = 999999999, longest = 0, seq_length, lt_node_counter = 0;
        while (all_nodes.hasNext()) {
            Node feature_node = all_nodes.next();
            if (PROTEOME) { // length should be int but is stored as long in older pantools versions
                int prot_length_int = (int) feature_node.getProperty("protein_length");
                seq_length = (long) prot_length_int;
            } else {
                if (label_str.equals("sequence")) {
                    seq_length = (long) feature_node.getProperty("length");
                } else {
                    int gene_length_int = (int) feature_node.getProperty("length");
                    seq_length = (long) gene_length_int;
                }
            }
            node_count ++;
            if (labels_for_longest_transcripts.contains(label_str + ",")) {
                if (!PROTEOME && !feature_node.hasProperty("longest_transcript")) {
                   continue; // CDS, mRNA, intron, exon will only be counted if they are part of the longest transcript
                }
            }
            lt_node_counter ++;
            total_length += seq_length;
            if (seq_length > longest) {
                longest = seq_length;
            }
            if (seq_length < shortest) {
                shortest = seq_length;
            }
            lengths.add(seq_length);
        }
        if (shortest == 999999999) {
            shortest = 0;
        }
        double[] median_average_stdev = median_average_stdev_from_AL_long(lengths);
        return new double[] {total_length, node_count, shortest, longest, median_average_stdev[0], median_average_stdev[1], median_average_stdev[2], lt_node_counter};
    }

    /**
     * Calculates density per 1 Mb
     * @param genome_size
     * @param gene_count
     * @return
     */
    public static double calculate_density(double genome_size, double gene_count) {
        double total_mb = genome_size/1000000; // 1 mb
        double gene_count_d = (double) gene_count;
        double density = gene_count_d/total_mb;
        DecimalFormat df = new DecimalFormat("#.##");
        density = Double.valueOf(df.format(density));
        return density;
    }

    /**
     * To get this information on sequence level we must go to the mrna's and retrieve the sequence_nr.
     * Takes kind of long for something not that interesting
     *
     * @return [genome numbers][unique genes, homology groups, homologous genes]
     */
    public static int[][] count_homology_groups_per_genome() {
        ResourceIterator<Node> hm_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
        int total_groups = (int) count_nodes(HOMOLOGY_GROUP_LABEL);
        int group_counter = 0;
        int[][] hmgroup_counts = new int[total_genomes][3]; //[genome numbers][unique genes, homology groups, homologous genes]
        while (hm_nodes.hasNext()) {
            group_counter ++;
            if (group_counter < 100 || group_counter % 100 == 10 || group_counter == total_groups) {
                System.out.print("\rReading homology groups: " + group_counter + "/" + total_groups);
            }
            Node hm_node = hm_nodes.next();
            int[] fields = (int[]) hm_node.getProperty("copy_number");
            int zero_count = 0, selected_pos = 0;
            for (int i = 1; i < fields.length; i++) {
                if (fields[i] == 0) {
                    zero_count ++;
                } else {
                    selected_pos = i; // if a homology group is unique, this position is increased in the array
                    hmgroup_counts[i-1][1] += 1;
                    hmgroup_counts[i-1][2] += fields[i];
                }
            }
            if (zero_count == total_genomes - 1) { // group is unique
                hmgroup_counts[selected_pos-1][0] += fields[selected_pos];
                if (fields[selected_pos] == 1) { // no homologous genes when there 1 unique gene
                    hmgroup_counts[selected_pos-1][2] -= 1;
                }
            }
        }
        return hmgroup_counts;
    }

    public static HashSet<Node> find_functional_annotation_node() {
        HashSet<Node> functionNodes = new HashSet<>();
        HashSet<String> missingFunctions = new HashSet<>();
        HashMap<String, Node> function_node_map = new HashMap<>();
        SELECTED_NAME = SELECTED_NAME.replace(" ","").replace(",,",",");
        Pantools.logger.info("Searching functional annotation nodes with '{}'.", SELECTED_NAME);
        put_function_nodes_in_map(function_node_map, PFAM_LABEL);
        put_function_nodes_in_map(function_node_map, GO_LABEL);
        put_function_nodes_in_map(function_node_map, INTERPRO_LABEL);
        put_function_nodes_in_map(function_node_map, TIGRFAM_LABEL);
        String[] names_array = SELECTED_NAME.split(",");
        for (String name : names_array) {
            if (function_node_map.containsKey(name)) {
                Node function_node = function_node_map.get(name);
                functionNodes.add(function_node);
            } else {
                missingFunctions.add(name);
            }
        }

        if (missingFunctions.size() > 0) {
            Pantools.logger.warn("The following functional annotation nodes were not found: {}.", missingFunctions);
        } else {
            Pantools.logger.info("Found all functions.");
        }
        return functionNodes;
    }

    /**
     * Fills a map with function nodes (keys are retrieved from 'id' property and if available 'alt_id' property)
     * @param function_node_map map to which the nodes are added
     * @param function_label label of the function nodes
     */
    public static void put_function_nodes_in_map(HashMap<String, Node> function_node_map, Label function_label) {
        ResourceIterator<Node> function_nodes = GRAPH_DB.findNodes(function_label);
        while (function_nodes.hasNext()) { // retrieve genome locations
            Node function_node = function_nodes.next();
            String id = (String) function_node.getProperty("id");
            if (function_node.hasProperty("alt_id")) {
                String[] alt_ids = (String[]) function_node.getProperty("alt_id");
                for (String alt_id : alt_ids) {
                    function_node_map.put(alt_id, function_node);
                }
            }
            function_node_map.put(id, function_node);
        }
    }

    /**
     *
     * @param function_node_list
     */
    public static void annotation_get_connected_genes(HashSet<Node> function_node_list) {
        StringBuilder log_builder = new StringBuilder("##Gene/protein identifier, gene name, genome, node identifier, gene length, protein length, address\n");
        for (Node function_node : function_node_list) {
            StringBuilder nuc_seq_builder = new StringBuilder();
            StringBuilder prot_seq_builder = new StringBuilder();
            Iterable<Label> node_labels = function_node.getLabels();
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            Iterable<Relationship> all_rels = pangenome_node.getRelationships(); // all_rels must be initialized already
            boolean correct_label = true;
            for (Label label1 : node_labels) {
                String label_str = label1.toString();
                if (label_str.equals("GO")) {
                    all_rels = function_node.getRelationships(RelTypes.has_go);
                } else if (label_str.equals("interpro")) {
                    all_rels = function_node.getRelationships(RelTypes.has_interpro);
                } else if (label_str.equals("pfam")) {
                    all_rels = function_node.getRelationships(RelTypes.has_pfam);
                } else if (label_str.equals("tigrfam")) {
                    all_rels = function_node.getRelationships(RelTypes.has_tigrfam);
                } else {
                    Pantools.logger.warn("{} is not a function node.", function_node);
                    correct_label = false;
                }
            }
            if (!correct_label) {
                continue;
            }
            String function_id = (String) function_node.getProperty("id");
            log_builder.append("#").append(function_id).append(" ").append(function_node).append("\n");
            TreeSet<Integer> present_genomes = new TreeSet<>();
            for (Relationship rel : all_rels) {
                Node mrna_node = rel.getStartNode();
                long mrna_node_id = mrna_node.getId();
                String name = retrieveNamePropertyAsString(mrna_node);
                if (name.equals("")) {
                    name = "-";
                }
                int[] address = (int[]) mrna_node.getProperty("address");
                if (skip_array[address[0]-1]) { // genome nr -1
                    continue;
                }
                String protein_id = "-";
                String nuc_sequence = get_nucleotide_sequence(mrna_node);
                String prot_sequence = get_protein_sequence(mrna_node);
                if (mrna_node.hasProperty("protein_ID")) {
                    protein_id = (String) mrna_node.getProperty("protein_ID");
                }
                String address_str = Arrays.toString(address);
                address_str = address_str.replace("[","").replace("]","").replace(",","");
                log_builder.append(protein_id).append(", ").append(name).append(", ").append(address[0]).append(", ").append(mrna_node_id).append(", ")
                        .append(nuc_sequence.length()).append(", ").append(prot_sequence.length()).append(", ").append(address_str).append("\n");
                present_genomes.add(address[0]);
                nuc_sequence = split_seq_in_parts_of_80bp(nuc_sequence);
                prot_sequence = split_seq_in_parts_of_80bp(prot_sequence);
                nuc_seq_builder.append(">").append(protein_id).append(" ").append(address[0]).append("\n").append(nuc_sequence).append("\n");
                prot_seq_builder.append(">").append(protein_id).append(" ").append(address[0]).append("\n").append(prot_sequence).append("\n");
            }

            ArrayList<Integer> not_present = new ArrayList<>();
            for (int i = 1; i <= total_genomes; i++) {
                if (!present_genomes.contains(i)) {
                    not_present.add(i);
                }
            }
            String found_str = present_genomes.toString().replace("[","").replace("]","").replace(" ","");
            String not_found_str = not_present.toString().replace("[","").replace("]","").replace(" ","");
            log_builder.append("\nFound in: ").append(found_str).append("\nMissing in: ").append(not_found_str).append("\n\n");
            if (nuc_seq_builder.length() != 0) {
                write_SB_to_file_in_DB(nuc_seq_builder, "find_genes/by_annotation/nucleotide_sequences/" + function_id.replace(":","_") + ".fasta");
                write_SB_to_file_in_DB(prot_seq_builder, "find_genes/by_annotation/protein_sequences/" + function_id.replace(":","_") + ".fasta");
            }
        }
        write_SB_to_file_in_DB(log_builder, "find_genes/by_annotation/find_genes_by_annotation.log");
    }

    /**
     * Return the size of a file in bytes
     * @param path_str
     * @return long
     */
    public static long get_size_of_file(String path_str) {
        Path pad = Paths.get(path_str);
        long size = 0;
        try {
            size = Files.walk(pad).mapToLong(p -> p.toFile().length()).sum();
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to find file {}", path_str);
            System.exit(1);
        }
        return size;
    }

    /**
     * Check if large number have 1000 (Kb), 1 million (Mb) or 1 billion (Gb) bases/base pairs
     * @param size long or double
     * @return
     */
    public static String b_Kb_Mb_or_Gb(double size) {
        String str_size = "";
        double inKB = 0, inMB = 0, inGB = 0;
        if (size == 0) {
            str_size = "0";
        }
        if (size > 1000) {
            inKB = size / 1000;
        } else {
            str_size += size;
            if (str_size.endsWith(".0")) {
                str_size = str_size.replace(".0","");
            }
            return str_size;
        }

        if (inKB > 1000) {
           inMB = inKB / 1000;
        } else {
           String result = String.format("%.2f", inKB);
           str_size = result + " Kb";
           return str_size;
        }

        if (inMB > 1000) {
            inGB = inMB / 1000;
            String result = String.format("%.2f", inGB);
            str_size = result + " Gb";
        } else {
            String result = String.format("%.2f", inMB);
            str_size = result + " Mb";
        }
        return str_size;
    }

    /**
     * Runs the compare geneclusters function only against members of the same phenotype
     * @param clustered_hms_map
     */
    public void run_cmpare_geneclusters_with_phenotype(HashMap<Integer, String> clustered_hms_map) {
        target_genome = null;
        ArrayList<Integer> original_skip = new ArrayList<>();
        for (int genome_nr : skip_list) {
            original_skip.add(genome_nr);
        }

        for (String phenotype : phenotype_map.keySet()) {
            if (phenotype.equals("?") || phenotype.equals("Unknown")) {
                continue;
            }

            int[] phenotype_members = phenotype_map.get(phenotype);
            skip_list = new ArrayList<>();
            for (int genome_nr : original_skip) {
                skip_list.add(genome_nr);
            }
            for (int i = 1; i <= total_genomes; i ++) {
                if (skip_list.contains(i)) { // do not change to skip_array
                    continue;
                }
                if (!ArrayUtils.contains(phenotype_members, i )) {
                    skip_list.add(i);
                }
            }

            core_threshold = total_genomes - skip_list.size();
            compare_geneclusters(clustered_hms_map);
            delete_file_in_DB("locate_genes/core_clusters_" + phenotype);
            delete_file_in_DB("locate_genes/compare_gene_clusters.txt_" + phenotype);
            copy_file(WORKING_DIRECTORY + "locate_genes/core_clusters", WORKING_DIRECTORY + "locate_genes/core_clusters_" + phenotype);
            copy_file(WORKING_DIRECTORY + "locate_genes/compare_gene_clusters.txt", WORKING_DIRECTORY + "locate_genes/compare_gene_clusters_" + phenotype + ".txt");
            delete_file_in_DB("locate_genes/core_clusters");
        }

        HashMap<String, String> clusters_per_pheno = new HashMap<>();
        HashMap<String, String[]> hmgroups_per_pheno = new HashMap<>();
        StringBuilder shared_builder = find_phenotype_specific_clusters(clusters_per_pheno, hmgroups_per_pheno);
        rm_groups_present_in_multiple_phenos(clusters_per_pheno, hmgroups_per_pheno);
        StringBuilder specific_builder = find_phenotype_specific_clusters(clusters_per_pheno);
        write_string_to_file_in_DB(shared_builder.toString() + "\n" + specific_builder.toString(), "locate_genes/phenotype_clusters.txt");
    }

    /**
     *
     * @param clusters_per_pheno
     * @param hmgroups_per_pheno
     */
    public static void rm_groups_present_in_multiple_phenos(HashMap<String, String> clusters_per_pheno, HashMap<String, String[]> hmgroups_per_pheno) {
        for (String phenotype : clusters_per_pheno.keySet()) {
            String all_clusters = clusters_per_pheno.get(phenotype);
            for (String phenotype2 : hmgroups_per_pheno.keySet()) {
                String[] all_groups = hmgroups_per_pheno.get(phenotype2);
                if (phenotype.equals(phenotype2)) {
                    continue;
                }
                for (String hmgroup : all_groups) {
                    if (all_clusters.contains("," + hmgroup + ",") || all_clusters.contains("," + hmgroup + ";") || all_clusters.contains(";" + hmgroup + ";")
                            || all_clusters.contains(";" + hmgroup + ",")) {
                        all_clusters = all_clusters.replace("," + hmgroup + ",",";").replace("," + hmgroup + ";",";").replace(";" + hmgroup + ",",";").replace(";" + hmgroup + ";",";");
                        all_clusters = all_clusters.replace("," + hmgroup + ",",";").replace("," + hmgroup + ";",";").replace(";" + hmgroup + ",",";").replace(";" + hmgroup + ";",";");
                    }
                }
            }
            clusters_per_pheno.put(phenotype, all_clusters);
        }
    }

    /**
     *
     * @param clusters_per_pheno
     * @param hmgroups_per_pheno
     * @return
     */
    public static StringBuilder find_phenotype_specific_clusters(HashMap<String, String> clusters_per_pheno, HashMap<String, String[]> hmgroups_per_pheno) {
        StringBuilder shared_builder = new StringBuilder("Phenotype: " + PHENOTYPE + "\nPhenotype SHARED clusters\n");
        for (String phenotype : phenotype_map.keySet()) {
            if (phenotype.equals("?") || phenotype.equals("Unknown")) {
                continue;
            }
            int pheno_members = phenotype_threshold_map.get(phenotype);
            int total = 0;
            StringBuilder temp_builder = new StringBuilder();
            try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "locate_genes/core_clusters_" + phenotype))) {
                String all_clusters = ",";
                for (int c = 0; in.ready();) {
                    String line = in.readLine().trim();
                    String[] line_array = line.split(",");
                    all_clusters += line + ";";
                    temp_builder.append(line).append("\n");
                    total += line_array.length;
                }
                temp_builder.append("\n");
                clusters_per_pheno.put(phenotype, all_clusters);
            } catch (IOException e) { // file does not exists, no shared clusters
                // do nothing
            }
            shared_builder.append("# ").append(total).append(" Co-localized genes specific to ").append(pheno_members).append(" '").append(phenotype).append("' genomes\n")
                    .append( temp_builder.toString());
            try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "locate_genes/all_" + phenotype))) {
                for (int c = 0; in.ready();) {
                    String line = in.readLine().trim();
                    String[] line_array = line.split(",");
                    hmgroups_per_pheno.put(phenotype, line_array);
                }
            } catch (IOException e) { // file does not exists, no shared clusters
                // do nothing
            }
            delete_file_in_DB("locate_genes/all_" + phenotype);
            delete_file_in_DB("locate_genes/core_clusters_" + phenotype);
        }
        return shared_builder;
    }

    /**
     *
     * @param clusters_per_pheno
     * @return
     */
    public static StringBuilder find_phenotype_specific_clusters(HashMap<String, String> clusters_per_pheno) {
        StringBuilder specific_builder = new StringBuilder("\nPhenotype SPECIFIC clusters\n");
        for (String phenotype : phenotype_map.keySet()) {
            if (phenotype.equals("?")) {
                continue;
            }
            HashMap<Integer, String> sort_by_length_map = new HashMap<>();
            int highest = 0;
            String all_clusters = clusters_per_pheno.get(phenotype);
            int pheno_members = phenotype_threshold_map.get(phenotype);
            int total = 0;
            if (all_clusters == null) {

            } else {
                all_clusters = all_clusters.substring(1, all_clusters.length());
                String[] clusters_array = all_clusters.split(";");
                for (String cluster : clusters_array) {
                    String[] cluster_array = cluster.split(",");

                    if (cluster_array.length > 1) {
                        total += cluster_array.length;
                        try_incr_hashmap(sort_by_length_map, cluster_array.length, cluster + ";");
                        if (highest < cluster_array.length) {
                            highest = cluster_array.length;
                        }
                    }
                }
            }
            specific_builder.append("# ").append(total).append(" Co-localized genes specific to ").append(pheno_members).append(" '").append(phenotype).append("' genomes \n");
            for (int i= highest; i>= 0; i--) {
                if (sort_by_length_map.containsKey(i)) {
                    String clusters = sort_by_length_map.get(i);
                    String[] clusters_array2 = clusters.split(";");
                    for (String cluster : clusters_array2) {
                        specific_builder.append(cluster).append("\n");
                    }
                }
            }
            specific_builder.append("\n");
        }
        return specific_builder;
    }

    /**
     * When a gene has multiple mrnas in multiple
     * @param clustered_hms_map
     * @ return
     */
    public static HashMap<Integer, String[][]> create_extra_copies_all_gene_clusters(HashMap<Integer, String> clustered_hms_map) {
        HashMap<Integer, String[][]> clustered_hms_map2 = new HashMap<>();
        for (int genome_nr : clustered_hms_map.keySet()) {
            String clusters = clustered_hms_map.get(genome_nr);
            String[] all_gene_clusters = clusters.split("\n");
            String[][] new_all_gene_clusters = new String[all_gene_clusters.length][0];
            Pantools.logger.trace("make a new genome {}.", genome_nr);
            StringBuilder new_clusters = new StringBuilder();
            for (int i = 0; i < all_gene_clusters.length; i++) {
                String[] gene_cluster_array = all_gene_clusters[i].split(",");
                String[][] multi_copy_cluster_array = create_extra_copies_of_gene_cluster(gene_cluster_array);
                String[] new_cluster_array = new String[multi_copy_cluster_array.length];
                for (int x = 0; x < multi_copy_cluster_array.length; x++) {
                    String gene_cluster = "";
                    for (String gene : multi_copy_cluster_array[x]) {
                        gene_cluster += gene + ",";
                    }
                    gene_cluster = gene_cluster.replaceFirst(".$","");
                    new_clusters.append(gene_cluster).append("\n");
                    new_cluster_array[x] = gene_cluster;
                }
                new_all_gene_clusters[i] = new_cluster_array;
            }
            clustered_hms_map.put(genome_nr, new_clusters.toString());
            clustered_hms_map2.put(genome_nr, new_all_gene_clusters );
        }
        return clustered_hms_map2;
    }

    /**
     *
     * @param clustered_hms_map
     */
    public void compare_geneclusters(HashMap<Integer, String> clustered_hms_map) {
        HashMap<Integer, String[][]> clustered_hms_map2 = create_extra_copies_all_gene_clusters(clustered_hms_map);
        StringBuilder core_clusters = new StringBuilder();
        StringBuilder output_builder = new StringBuilder("#Pairwise comparison of gene clusters of " + core_threshold + " genomes\n");
        boolean first_genome = true;
        HashMap <String, HashSet<String>> hmgroups_per_phenotype = new HashMap<>();

        for (int genome_nr = 1; genome_nr <= total_genomes; genome_nr ++) {
            if (!clustered_hms_map.containsKey(genome_nr) || skip_list.contains(genome_nr)) {
                continue;
            }
            System.out.print("\rComparing gene clusters against genome " + genome_nr);
            int total_colocalized = 0;
            String[][] TEST_all_gene_clusters = clustered_hms_map2.get(genome_nr);
            for (String[] gene_cluster_array1 : TEST_all_gene_clusters) {
                total_colocalized += gene_cluster_array1[0].split(",").length;
            }
            output_builder.append("\n#Genome ").append(genome_nr).append(". ").append(total_colocalized)
                    .append(" co-localized genes. ").append(TEST_all_gene_clusters.length).append(" clusters\n");
            HashMap<String, String > genome_hmstreak_map = new HashMap<>();
            for (String[] gene_cluster_array1 : TEST_all_gene_clusters) {
                // this can hold multiple core clusters when there are multiple variations of the cluster, must be reduced to 1 cluster
                ArrayList<String> core_clusters_list = new ArrayList<>();
                if (gene_cluster_array1.length != 1) {
                    output_builder.append("\nThe next cluster has ").append(gene_cluster_array1.length).append(" additional versions below because one ore more genes have mRNA's in different homology groups!");
                }
                int cluster_length = 0;
                for (int x = 0; x < gene_cluster_array1.length; x++) {
                    String gene_cluster = gene_cluster_array1[x];
                    Pantools.logger.trace("verg {}", gene_cluster);
                    HashMap<String, Integer> missing_frequency = new HashMap<>();
                    String[] gene_cluster_array = gene_cluster.split(",");
                    for (String gene: gene_cluster_array) {
                        if (PHENOTYPE != null && !PHENOTYPE.equals("")) {
                            String pheno = geno_pheno_map.get(genome_nr);
                            try_incr_hashset_hashmap(hmgroups_per_phenotype, pheno, gene);
                        }
                    }
                    cluster_length = gene_cluster_array.length;
                    String version = "";
                    if (gene_cluster_array1.length != 1) {
                        version = "version " + x + " ";
                    }
                    output_builder.append("\nGene cluster ").append(version).append(": ").append(gene_cluster).append(" (").append(gene_cluster_array.length).append(")\n");
                    TreeSet<Integer> all_present = new TreeSet<>();
                    TreeSet<Integer> somewhat_present = new TreeSet<>();
                    ArrayList<String> all_clusters = new ArrayList<>();
                    all_clusters.add(gene_cluster + ";");
                    String additional_info = compare_current_cluster_to_rest(clustered_hms_map, genome_nr, all_present, somewhat_present,
                            gene_cluster, missing_frequency, genome_hmstreak_map, all_clusters);

                    if (first_genome) {
                        create_core_clusters(missing_frequency, all_clusters, core_clusters_list);
                    }
                    add_presence_absence_to_cluster_output(genome_nr, clustered_hms_map, all_present, somewhat_present, output_builder);
                    add_additional_info_to_cluster_output(additional_info, genome_hmstreak_map, output_builder, gene_cluster_array, gene_cluster);
                }
                if (first_genome) {
                    select_possible_clusters_from_list(core_clusters_list, cluster_length, core_clusters);
                }
            }
            first_genome = false;
        }
        create_compare_gene_clusters_output(core_clusters.toString(), output_builder);
        create_compare_gene_clusters_pheno_output(hmgroups_per_phenotype);
    }

    /**
     *
     * Currently all clusters in core_clusters_list are added
     * @param core_clusters_list
     * @param cluster_length
     * @param core_clusters
     */
    public void select_possible_clusters_from_list(ArrayList<String> core_clusters_list, int cluster_length, StringBuilder core_clusters) {
        Pantools.logger.trace("{} clusters {}.", cluster_length, core_clusters_list);
        int longest_cluster = 0;
        ArrayList<String> longest_cluster_list = new ArrayList<>();
        for (int i= 0; i < core_clusters_list.size(); i++) {
            String[] cluster_array = core_clusters_list.get(i).split(",");
            if (cluster_array.length > longest_cluster) {
                longest_cluster = cluster_array.length;
            }
        }
         for (int i= 0; i < core_clusters_list.size(); i++) {
            String[] cluster_array = core_clusters_list.get(i).split(",");
            if (cluster_array.length == longest_cluster) {
                longest_cluster_list.add(core_clusters_list.get(i));
            }
        }
        if (longest_cluster == cluster_length) {
            core_clusters.append(longest_cluster_list.get(0)).append("\n");
            return;
        }
        for (String longest_cluster2 : longest_cluster_list) {
              core_clusters.append( longest_cluster2).append("\n");
        }
        int total_genes = longest_cluster *longest_cluster_list.size();
        Pantools.logger.trace("longest sum is {} {}.", total_genes, longest_cluster_list); // kijk nog een keer als er nog tenminste 2 plekken zijn
    }

    /**
     *
     * @param hmgroups_per_phenotype
     */
    public void create_compare_gene_clusters_pheno_output( HashMap <String, HashSet<String>> hmgroups_per_phenotype) {
        if (PHENOTYPE != null && !PHENOTYPE.equals("")) {
            for (Map.Entry<String, HashSet<String>> entry2 : hmgroups_per_phenotype.entrySet()) {
                String pheno = entry2.getKey();
                HashSet<String> hmgroups = entry2.getValue();
                Pantools.logger.trace("aantal groepen {} {}.", pheno, hmgroups.size());
                StringBuilder all_genes_builder = new StringBuilder();
                for (String hmgroup : hmgroups) {
                     all_genes_builder.append(hmgroup).append(",");
                }
                write_string_to_file_in_DB(all_genes_builder.toString().replaceFirst(".$",""), "locate_genes/all_" + pheno);
            }
        }
    }

    /**
     *
     * @param gene_cluster
     * @return
     */
    public String create_rv_genecluster(String gene_cluster) {
        String rv_genecluster = "";
        String[] rv_gene_cluster_array = gene_cluster.split(",");
        ArrayUtils.reverse(rv_gene_cluster_array);
        for (String gene: rv_gene_cluster_array) {
            rv_genecluster += gene +",";
        }
        rv_genecluster = rv_genecluster.replaceFirst(".$","");
        return rv_genecluster;
    }

    /**
     *
     * @param clustered_hms_map
     * @param genome_nr
     * @param all_present
     * @param somewhat_present
     * @param gene_cluster
     * @param missing_frequency
     * @param genome_hmstreak_map
     * @param all_clusters
     * @return
     */
    public String compare_current_cluster_to_rest(HashMap<Integer, String> clustered_hms_map, int genome_nr, TreeSet<Integer> all_present, TreeSet<Integer> somewhat_present,
            String gene_cluster, HashMap<String, Integer> missing_frequency, HashMap<String, String > genome_hmstreak_map, ArrayList<String> all_clusters) {
        String additional_info = "";
        String rv_genecluster = create_rv_genecluster(gene_cluster);
        String[] gene_cluster_array = gene_cluster.split(",");
        for (Map.Entry<Integer, String> entry2 : clustered_hms_map.entrySet()) {
            String hm_streak = "", all_hm_streaks = "", new_streak = "";
            int genome_nr2 = entry2.getKey();
            if (genome_nr2 == genome_nr || skip_list.contains(genome_nr2)) {
                continue;
            }

            String key2_str = Integer.toString(genome_nr2);
            String value2 = entry2.getValue();
            if (value2.contains(gene_cluster) || value2.contains(rv_genecluster)) {
                all_present.add(genome_nr2);
                new_streak = gene_cluster;
            } else if (gene_cluster_array.length > 2) {
                int no_count = 0, short_count = 0, yes_count = 0;
                String previous = "nothing";
                for (int i = 0; i < gene_cluster_array.length-1; i++) {
                    int iplus = i+1;
                    String gene_part = gene_cluster_array[i] + "," + gene_cluster_array[iplus];
                    boolean previous_was_dupli = false;
                    if (i > 0) {
                        if (gene_cluster_array[i-1].equals(gene_cluster_array[i])) { // this can only happen in mode 1. // previous was duplicated
                            previous_was_dupli = true;
                        }
                    }
                    String rv_gene_part = gene_cluster_array[iplus] + "," + gene_cluster_array[i];
                    if (value2.contains(gene_part) || value2.contains(rv_gene_part)) { //
                        previous = "yes";
                        yes_count ++;
                        if (previous_was_dupli) { // this can only happen in mode 1.
                            if (new_streak.endsWith(gene_cluster_array[i] + ";")) {
                                hm_streak += gene_cluster_array[iplus]+ ",";
                                new_streak += gene_cluster_array[iplus]+ ",";
                            } else {
                                hm_streak += gene_cluster_array[i] + "," + gene_cluster_array[iplus] + ",";
                                new_streak += gene_cluster_array[i] + "," + gene_cluster_array[iplus] + ",";
                            }
                        } else if (hm_streak.length() == 0) {
                            hm_streak += gene_cluster_array[i] + "," + gene_cluster_array[iplus] + ",";
                            new_streak += gene_cluster_array[i] + "," + gene_cluster_array[iplus] + ",";
                        } else {
                            hm_streak += gene_cluster_array[iplus]+ ",";
                            new_streak += gene_cluster_array[iplus]+ ",";
                        }
                    } else {
                        if (!value2.contains(gene_cluster_array[i])) { // completey missing
                            try_incr_hashmap(missing_frequency, gene_cluster_array[i], 1);
                        }

                        if (!value2.contains(gene_cluster_array[iplus])) { // completey missing
                            try_incr_hashmap(missing_frequency, gene_cluster_array[iplus], 1);
                        }

                        if (hm_streak.length() > 1) {
                            all_hm_streaks += hm_streak + " ";
                            new_streak = new_streak.replaceFirst(".$","");
                            new_streak += ";";
                        }
                        hm_streak = "";
                        if (i == 0 || i == gene_cluster_array.length-2 || "shorter".equals(previous)) {
                            previous = "shorter";
                            short_count ++;
                        } else {
                            if (!"no".equals(previous)) {
                                no_count ++;
                            }
                            previous = "no";
                        }
                    }
                }

                if (hm_streak.length() > 1) {
                    all_hm_streaks += hm_streak + "   ";
                }

                all_hm_streaks = all_hm_streaks.replaceFirst(".$","").replaceFirst(".$","").replaceFirst(".$","").replaceFirst(".$","");
                genome_hmstreak_map.put(key2_str, all_hm_streaks);
                if (no_count == 0 && yes_count > 0 && short_count == 0) { // additional gene copies
                    additional_info += genome_nr2 + " is present but has additonal gene copies\n";
                    all_present.add(genome_nr2);
                } else if (no_count < 2 && short_count != 0 && yes_count > 0) { // missing a gene at the edges of the cluster
                    additional_info += genome_nr2 + " is present but shorter\n";
                    somewhat_present.add(genome_nr2);
                } else if (yes_count == 0) {
                    // missing, do nothing
                } else { // the cluster is found completely (or partly) in several piecies
                    no_count ++;
                    additional_info += genome_nr2 + " is in " + no_count + " pieces";
                    if (short_count != 0) {
                        additional_info += " and is shorter";
                    }
                    additional_info += "\n";
                    somewhat_present.add(genome_nr2);
                }
            }
            if (new_streak.endsWith(",")) {
                new_streak = new_streak.replaceFirst(".$","");
            }
            if (!new_streak.endsWith(";")) {
                new_streak += ";";
            }
            all_clusters.add(new_streak);
        }
        return additional_info;
    }

    /**
     *
     * @param genome_nr
     * @param clustered_hms_map
     * @param all_present
     * @param somewhat_present
     * @param output_builder
     */
    public void add_presence_absence_to_cluster_output(int genome_nr, HashMap<Integer, String> clustered_hms_map,
            TreeSet<Integer> all_present, TreeSet<Integer> somewhat_present, StringBuilder output_builder) {
        TreeSet<Integer> not_present = new TreeSet<>();
        for (Map.Entry<Integer, String> entry2 : clustered_hms_map.entrySet()) {
            int genome2 = entry2.getKey();
            if (genome2 == genome_nr) {
                 continue;
            }
            if (all_present.contains(genome2) || somewhat_present.contains(genome2) || skip_list.contains(genome2)) {
                continue;
            }
            not_present.add(genome2);
        }

        String missing_str = "";
        if (not_present.size() > 0) {
            missing_str = "(" + not_present.size() + ")";
        }
        String not_present_str = not_present.toString().replace(" ","").replace("[","").replace("]","");
        String all_present_str = all_present.toString().replace(" ","").replace("[","").replace("]","");
        output_builder.append(" Present in ").append(all_present.size()).append(" genomes: ").append(all_present_str)
                .append("\n Missing ").append(missing_str).append(": ").append(not_present_str).append("\n");
    }

    /**
     *
     * @param missing_frequency
     * @param all_clusters
     * @param core_clusters_list
     */
    public void create_core_clusters(HashMap<String, Integer> missing_frequency, ArrayList<String> all_clusters, ArrayList<String> core_clusters_list) {
        boolean pass = false;
        int loop_counter = 0;
        if (missing_frequency.size() > 0) {
            for (Map.Entry<String, Integer> entry2 : missing_frequency.entrySet()) { // use missing hmgroups to split found clusters
                String hmgroup = entry2.getKey();
                for (int i= 0; i < all_clusters.size(); i++) {
                    String clustertje = all_clusters.get(i);
                    if (clustertje.contains(hmgroup)) {
                        clustertje = clustertje.replace(hmgroup,";").replace(";;",";").replace(",;",";").replace(";,",";").replace(";;",";");
                        all_clusters.set(i, clustertje);
                    }
                }
            }
        }

        while (!pass) {
            HashSet<String> core_set = find_longest_streak(all_clusters);

            if (core_set.size() > 0 ) {
                loop_counter ++;
                for (String conserved : core_set) {
                    if (conserved.equals("")) {
                        Pantools.logger.info("HOHO stop 19.");
                        pass = true;
                        System.exit(1);
                    } else {
                        if (!core_clusters_list.contains(conserved)) {
                            core_clusters_list.add(conserved);
                        }
                    }
                }
            } else {
                Pantools.logger.trace("end loop ");
                pass = true;
            }
        }
    }

    /**
     *
     * @param all_clusters
     * @return
     */
    public static HashSet<String> find_longest_streak(ArrayList<String> all_clusters) {
        boolean print = false;
        HashSet<String> core_clusters = new HashSet<>();
        for (int i= 0; i < all_clusters.size(); i++) { // all values should be the same
            String clusters = all_clusters.get(i);
            if (clusters.equals("") || clusters.equals(";")) {
                core_clusters = new HashSet<>();
                return core_clusters;
            }
            String[] clusters_array = clusters.split(";");
            Pantools.logger.trace("{}) {} clusters {}.", i, clusters_array.length, Arrays.toString(clusters_array));
            for (String cluster : clusters_array) {
                int counter = 0;
                String[] array = cluster.split(",");
                if (array.length == 1) {
                     continue;
                }
                for (int j= 0; j < all_clusters.size(); j++) { 
                    String target_clusters = all_clusters.get(j);
                    if (target_clusters.contains(cluster)) {
                        counter ++;
                    }
                }
                Pantools.logger.trace(" >{}< ({}) present {}.", cluster, array.length, counter);
                if (counter >= core_threshold) {
                     core_clusters.add(cluster);
                }
            }
        }
         
        if (core_clusters.isEmpty()) {
            return core_clusters;
        }
        for (String core_cluster : core_clusters) {
                Pantools.logger.trace("verwijder >{}<.", core_cluster);
            for (int i= 0; i < all_clusters.size(); i++) { 
                String clustertje = all_clusters.get(i);
                if (clustertje.contains(core_cluster)) {
                    //all_clusters.remove(i);
                    Pantools.logger.trace("voor {}.", clustertje);
                    clustertje = clustertje.replace(core_cluster,";").replace(";;",";").replace(",;",";").replace(";,",";").replace(";;",";");
                    if (clustertje.startsWith(";")) {
                        clustertje = clustertje.substring(1, clustertje.length());
                    }
                    if (!clustertje.endsWith(";")) {
                        clustertje += ";";
                    }

                    all_clusters.set(i, clustertje);
                    Pantools.logger.trace("na {}.", clustertje);
                }  else {
                    Pantools.logger.trace("nee {} {}.", i, clustertje);
                    System.exit(1);
                }
            }
            String[] cluster_array = core_cluster.split(",");
            Pantools.logger.trace(cluster_array.length);
        }
        Pantools.logger.trace("zonder de blokken.");
        for (int i= 0; i < all_clusters.size(); i++) {
            Pantools.logger.trace(" ->{}.", all_clusters.get(i));
        }
      
        return core_clusters;
    }
    
    /**
     * 
     * @param additional_info
     * @param genome_hmstreak_map
     * @param output_builder
     * @param gene_cluster_array
     * @param gene_cluster 
     */
    public static void add_additional_info_to_cluster_output(String additional_info, HashMap<String, String > genome_hmstreak_map, StringBuilder output_builder, 
           String[] gene_cluster_array, String gene_cluster) { 
        
        if (additional_info.length() == 0) {
            return;
        } 
        String[] additional_array = additional_info.split("\n");
        for (String additional : additional_array) { 
            String[] additional2_array = additional.split(" ");
            if (additional2_array[additional2_array.length-1].equals("pieces") || additional2_array[additional2_array.length-1].equals("shorter")) { 
                String hmstreak = genome_hmstreak_map.get(additional2_array[0]);
                String[] hmstreak_array = hmstreak.split(",");
                output_builder.append(" ").append(additional).append(": \n ");
                if (hmstreak.contains("  ")) {
                    String[] streak_array = hmstreak.split(",  ");
                    for (String streak : streak_array) {
                        String[] streak_array2 = streak.split(",");
                        output_builder.append(" ").append(streak).append(" (").append(streak_array2.length).append(") \n"); 
                    }
                   output_builder.append("\n");
                } else {
                    output_builder.append(" ").append(hmstreak).append(" (").append(hmstreak_array.length).append(") \n");
                }
            } else { 
                output_builder.append(" ").append(additional).append("\n");
            }
        }
    }
    
    /**
     * 
     * @param core_clusters
     * @param output_builder 
     */
    public static void create_compare_gene_clusters_output(String core_clusters, StringBuilder output_builder) {
        String header;
        if (core_clusters.length() > 1) {
            HashMap<Integer, String> sort_by_length_map = new HashMap<>();
            int total_genes = 0;
            String[] core_array = core_clusters.split("\n");
            int highest = 0;
            for (String cluster : core_array) {
                String[] cluster_array = cluster.split(",");
                total_genes += cluster_array.length; 
                if (cluster_array.length > highest) {
                    highest = cluster_array.length;
                }
                try_incr_hashmap(sort_by_length_map, cluster_array.length, cluster + "\n");
                
            }
            String sorted_clusters = "";
            for (int i= highest; i>= 0; i--) { 
                if (!sort_by_length_map.containsKey(i)) {
                    continue;
                }
                String clusters = sort_by_length_map.get(i);
                sorted_clusters += clusters.replace(" ","") ;
            }
            
            if (core_threshold != 100) {
                header = "# " + total_genes + " + Co-localized genes between at least " + core_threshold + " genomes. "
                        + "The clusters consist of homology group identifiers\n" + sorted_clusters + "\n";
            } else {
                header = "# " + total_genes + " Co-localized genes between " + core_threshold + " genomes. "
                        + "The clusters consists of homology group identifiers\n" + sorted_clusters + "\n";
            }
            write_string_to_file_in_DB(sorted_clusters,  "locate_genes/core_clusters" );
        } else {
            header = "#No shared geneclusters were found across " + core_threshold + " genomes";
        }
        write_string_to_file_in_DB(header + output_builder.toString(), "locate_genes/compare_gene_clusters.txt");
    }
        
    /**
     * 
     * @param gene_cluster_array
     * @return 
     */
    public static String[][] create_extra_copies_of_gene_cluster(String[] gene_cluster_array) {
        int new_clusters = 0;
        String[][] all_clusters = new String[1][gene_cluster_array.length];
        all_clusters[0] = gene_cluster_array;
        ArrayList<String> variable_genes = new ArrayList<>();
        HashMap<Integer, String[]> variable_genes_per_pos = new  HashMap<>();
       
        boolean pass = false;
        while (!pass) {
            boolean stop = false;
            String gene = "";
            int new_cluster_count = 0;
            int pos = 0;
            for (int i=0; i < all_clusters.length; i++) {
                if (stop) {
                    continue;
                }
                for (int j=0; j < all_clusters[i].length; j++) {
                    if (stop) {
                        continue;
                    }
                    gene = all_clusters[i][j];
                    if (gene.contains("/")) {
                        String[] gene_array = gene.split("/");
                        new_cluster_count = gene_array.length;
                        stop = true;
                        pos = j;
                    }
                }
            }
            if (!stop) {
                pass = true;
            } else {
                String[] gene_array = gene.split("/");
                String[][] new_all_clusters = new String[all_clusters.length*new_cluster_count][gene_cluster_array.length];
                int position =0;
                for (int loop=0; loop < new_cluster_count; loop++) {
                    for (int i=0; i < all_clusters.length; i++) {
                        new_all_clusters[position] = all_clusters[i];
                        position ++;
                    }
                }
                int gene_nr = 0;
                boolean first = true;
                for (int i=0; i < new_all_clusters.length; i++) {
                    
                    if ((i != 0 && i % all_clusters.length == 0) || (all_clusters.length == 1 && !first)) {
                         gene_nr ++;
                         first = false;
                    }
                    new_all_clusters[i][pos] = gene_array[gene_nr];
                    String[] newcluster = new String[new_all_clusters[i].length]; // do this or else the arrays have the same reference
                    for (int j = 0; j < new_all_clusters[i].length; j++) {
                        newcluster[j] = new_all_clusters[i][j];
                    } 
                    new_all_clusters[i] = newcluster;
                }
                all_clusters = new String[all_clusters.length*new_cluster_count][gene_cluster_array.length];
                for (int i=0; i < new_all_clusters.length; i++) {
                    all_clusters[i] =  new_all_clusters[i];
                } 
            }
        }
        if (new_clusters == 0) {
            all_clusters[0] = gene_cluster_array;
            return all_clusters;
        } 
        return all_clusters;
    }
    
 
    public void add_streak_to_by_size_builder2(ArrayList<Integer> position_streak, String identifier, HashMap<String, ArrayList<Node>> mrnas_per_position, String add_str, 
            String add_str2, StringBuilder overview_by_pos, HashMap<Integer, StringBuilder> cluster_info_by_size, StringBuilder all_HMs) {
        
        boolean first_node = true;
        String start_address = "";  
        int[] address = new int[0];
        StringBuilder gene_id_builder = new StringBuilder();
        StringBuilder mrna_node_id_builder = new StringBuilder();
        StringBuilder mrna_id_builder = new StringBuilder();
        StringBuilder hm_id_builder = new StringBuilder();
        StringBuilder distance_between_genes = new StringBuilder();
        int previous_gene_end = -1;
        for (int position :  position_streak) { 
            ArrayList<Node> mrna_nodes = mrnas_per_position.get(identifier + "#" + position);
            String all_hm_node_identifers = "";
            String all_mrna_node_identifers = "";
            String all_mrna_identifers = "";
            for (Node mrna_node : mrna_nodes) {
                long mrna_node_id = mrna_node.getId();
                all_mrna_node_identifers += mrna_node_id + "/";
                String mrna_id = (String) mrna_node.getProperty("id");
                all_mrna_identifers +=  mrna_id + "/";
                Relationship hm_relation = mrna_node.getSingleRelationship(has_homolog, Direction.INCOMING);
                Node hm_node = hm_relation.getStartNode();
                long hm_node_id = hm_node.getId();
                if (!all_hm_node_identifers.contains(hm_node_id + "/")) {
                    all_hm_node_identifers += hm_node_id + "/";
                }
            }
            hm_id_builder.append(all_hm_node_identifers.replaceFirst(".$","")).append(",");
            mrna_node_id_builder.append(all_mrna_node_identifers.replaceFirst(".$","")).append(",");
            mrna_id_builder.append(all_mrna_identifers.replaceFirst(".$","")).append(",");
           
            ArrayList<Node> gene_node_list = mrnas_per_position.get(identifier + "#" + position + "_gene");
            Node gene_node = gene_node_list.get(0);
            address = (int[]) gene_node.getProperty("address");
            long gene_node_id = gene_node.getId();
            if (first_node) {
                start_address  = address[0] + " " + address[1] + " " + address[2];
                first_node = false;
            } else {
                int distance =  address[2] - previous_gene_end;
                if (distance < 0) {
                    //distance = 0;
                }
                distance_between_genes.append(distance).append(",");
            }
            gene_id_builder.append(gene_node_id).append(",");
            previous_gene_end  = address[3];
        }
        
        String number_of_genes = "1 gene";
        if (position_streak.size() > 1) {
            number_of_genes = position_streak.size() + " genes";
        }
        String distances_str = distance_between_genes.toString().replaceFirst(".$","");
        if (distances_str.equals("")) {
            distances_str = "-";
        }
        String cluster_info =  start_address + " " + address[3] + "\n"
                + number_of_genes  + "\n" 
                + gene_id_builder.toString().replaceFirst(".$","") + "\n" 
                + mrna_id_builder.toString().replaceFirst(".$","") + "\n" 
                + mrna_node_id_builder.toString().replaceFirst(".$","") + "\n" 
                + hm_id_builder.toString().replaceFirst(".$","") + "\n" 
                + distances_str;
        all_HMs.append(hm_id_builder.toString().replaceFirst(".$","")).append("\n");        
        overview_by_pos.append( add_str).append(cluster_info).append("\n").append(add_str2);
        try_incr_SB_hashmap(cluster_info_by_size, position_streak.size(), cluster_info + "\n\n");      
    }
    
    /**
     * 
     * @param gene_positions_per_sequence
     * @param mrnas_per_position
     * @param gene_nodes_per_seq
     * @return 
     */
    public HashMap<Integer, String> calculate_geneclusters(HashMap<String, TreeSet<Integer>> gene_positions_per_sequence, 
            HashMap<String, ArrayList<Node>> mrnas_per_position, HashMap<String, Node[]> gene_nodes_per_seq) {
        
        System.out.print("\rCalculating clusters:              "); // spaces are intentional
        HashMap<Integer, String> clustered_hms_map = new HashMap<>();   
        String start_str = "#Each cluster has 7 lines of information\n"
                + "#1. Cluster address\n"
                + "#2. Cluster size\n"
                + "#3. gene node identifier\n"
                + "#4. mRNA identifier (GFF)\n"
                + "#5. mRNA node identifier\n"
                + "#6. Homology group node identifier\n"
                + "#7. Distance between genes (No the negative numbers are not a bug, you can verify this in the GFF file)\n\n";
        StringBuilder overview_by_size = new StringBuilder(start_str);
        StringBuilder overview_by_pos = new StringBuilder(start_str);
        
        for (int i=1; i <= total_genomes; i++) { 
            if (skip_list.contains(i)) {
                continue;
            }
           
            StringBuilder all_HMs = new StringBuilder();
            HashMap<Integer, StringBuilder> cluster_info_by_size = new HashMap<>();
            Node genome_node = GRAPH_DB.findNode(GENOME_LABEL, "number", i);
            int num_sequences = (int) genome_node.getProperty("num_sequences");
            int total_input_genes = 0;
            boolean first_node_genome = true;
            int[] gene_cluster_counts = new int[2]; // total  genes in clusters, total clusters 
            System.out.print("\rCalculating clusters: genome " + i + "   ");
            for (int j = 1; j <= num_sequences; j++) {   
                if (!gene_positions_per_sequence.containsKey(i + "_" + j)) {
                    continue;
                }
                String identifier = i + "_" + j;
              
                TreeSet<Integer> mrna_positions = gene_positions_per_sequence.get(identifier);
                total_input_genes += mrna_positions.size();
             
                boolean first_node_seq = true, ignore_first_between = true;
                ArrayList<Integer> position_streak = new ArrayList<>();
                Node[] genes_per_seq = gene_nodes_per_seq.get(identifier);
                int previous_position = -2, previous_end = 0, previous_pos_end_streak = -1;
                int genes_between = -2, distance = 0;
                for (int position : mrna_positions) {
                    ArrayList<Node> gene_node_list = mrnas_per_position.get(identifier + "#" + position + "_gene");
                    Node gene_node = gene_node_list.get(0);
                    int[] address = (int[]) gene_node.getProperty("address");
                    if (first_node_genome) { // first mrna of the genome 
                        overview_by_pos.append("\n##Genome ").append(i).append("\n");//.append(". ").append(mrna_nodes.size()).append(" genes\n");
                        first_node_genome = false;
                    }
                    if (first_node_seq) {
                        overview_by_pos.append("#Genome ").append(i).append(" sequence ").append(address[1]).append(". ").append(mrna_positions.size()).append(" input genes\n\n")
                                .append(position).append(" genes before. ").append((address[2]-1)).append("bp\n\n");
                        first_node_seq = false;
                        previous_end = address[3];
                    }  
                    
                    distance = address[2]-previous_end; // distance between genes
                    boolean pass = false;
                    if (position == previous_position +1) { // gene is next to previous 
                        previous_end = address[3];
                        if (Integer.parseInt(NODE_VALUE) > distance) {
                            pass = true;
                        }
                    }
                    if (!pass) {
                        if (position_streak.size() > 0) {
                            genes_between = (position_streak.get(0) - previous_pos_end_streak)-1;
                            String add_str = "";
                            if (!ignore_first_between) {
                                add_str = create_genes_between_string(genes_between, distance, "", genes_per_seq, position_streak.get(0), previous_pos_end_streak, identifier);
                            }
                            ignore_first_between = false;
                            add_streak_to_by_size_builder2(position_streak, identifier, mrnas_per_position, add_str, "", overview_by_pos, cluster_info_by_size, all_HMs);
                            if (position_streak.size() > 1) { // gene cluster has at least two genes
                                gene_cluster_counts[0] += position_streak.size();
                                gene_cluster_counts[1] ++;
                            } 
                            previous_pos_end_streak = previous_position;  
                        }
                        position_streak = new ArrayList<>();
                    }
                    position_streak.add(position);
                    previous_position = position;  
                } 
                genes_between = (position_streak.get(0) - previous_pos_end_streak)-1;
                if (position_streak.size() > 1) { // gene cluster has at least two genes
                    gene_cluster_counts[0] += position_streak.size();
                    gene_cluster_counts[1] ++;
                } 
                String add_str = create_genes_between_string(genes_between, distance, "", genes_per_seq, position_streak.get(0), previous_pos_end_streak, identifier);
                long seq_length = (long) GRAPH_DB.findNode(SEQUENCE_LABEL, "identifier", identifier).getProperty("length");
                int last_gene_pos = position_streak.get(0);
                if (previous_pos_end_streak != -1) {
                    last_gene_pos = previous_pos_end_streak;
                }
               
                int[] last_gene_address = (int[]) genes_per_seq[last_gene_pos].getProperty("address");
                String add_str2 = "\n" + (genes_per_seq.length-previous_position-1) + " more genes found on this sequence. " + (seq_length-last_gene_address[3]) + " bp\n";
                add_streak_to_by_size_builder2(position_streak, identifier, mrnas_per_position, add_str, add_str2, overview_by_pos, cluster_info_by_size, all_HMs);
            }
            prepare_cluster_overview_by_gene_size(cluster_info_by_size, overview_by_size, i, total_input_genes, gene_cluster_counts);
            clustered_hms_map.put(i, all_HMs.toString());
        }
        write_SB_to_file_in_DB(overview_by_pos, "locate_genes/gene_clusters_by_position.txt"); 
        write_SB_to_file_in_DB(overview_by_size, "locate_genes/gene_clusters_by_size.txt"); 
        return clustered_hms_map;
    }
    
    /**
     * 
     * @param cluster_map
     * @param overview_by_size
     * @param genome_nr
     * @param gene_count
     * @param gene_cluster_counts 
     */
    public void prepare_cluster_overview_by_gene_size(HashMap<Integer, StringBuilder> cluster_map, StringBuilder overview_by_size, 
            int genome_nr, int gene_count, int[] gene_cluster_counts) {
        
        int highest_counter = 0;
        for (int cluster_length : cluster_map.keySet()) {
            if (cluster_length > highest_counter) {
                highest_counter = cluster_length;
            } 
        } 
        overview_by_size.append("#Genome ").append(genome_nr)
                .append("\nTotal (input) genes: ").append(gene_count)
                .append("\nTotal neighbouring genes: ").append(gene_cluster_counts[0])
                .append("\nTotal gene clusters: ").append(gene_cluster_counts[1])
                .append("\nLongest consecutive counter: ").append(highest_counter).append("\n\n");
           
        for (int i = highest_counter; i >-1; i--) {
            StringBuilder clusters = cluster_map.get(i);
            if (clusters == null) { // happens when no cluster is found for a certain size
                continue;
            }
            overview_by_size.append(clusters.toString());
        } 
    }
    
    /**
     * 
     * @param genes_between
     * @param distance
     * @param add_str
     * @param gene_per_seq
     * @param gene_pos1
     * @param gene_pos2
     * @param identifier
     * @return 
     */
    public String create_genes_between_string(int genes_between, int distance, String add_str, Node[] gene_per_seq, int gene_pos1, int gene_pos2, String identifier) {
        if (gene_pos2 == -1) { // not initialized tyet
            gene_pos2 = gene_per_seq.length-1;
        }
        Pantools.logger.trace("{} {} {}.", gene_pos1, gene_pos2, Arrays.toString(gene_per_seq));
        Node gene1 = gene_per_seq[gene_pos1];
        Node gene2 = gene_per_seq[gene_pos2];
        int[] gene_address1 = (int[]) gene1.getProperty("address");
        int[] gene_address2 = (int[]) gene2.getProperty("address");
        Pantools.logger.trace("pos{} {} {}.", gene_pos1, gene_pos2, (gene_address1[2]-gene_address2[3]));
        distance = gene_address1[2]-gene_address2[3]; //start position of next gene - end position of first gene
        switch (genes_between) {
            case 0:
                if (distance > 0) {
                    add_str = "\nNo gene between. " + distance + " bp\n\n" + add_str;
                } else {
                    add_str = "\nNo more gene found on this sequence\n\n" + add_str;
                }
                break;
            case 1:
                if (distance > 0 && add_str.length() == 0) {
                   add_str = "\n1 gene between. " + distance + " bp\n\n" + add_str;
                } else {
                   add_str = "\n1 more gene found on this sequence\n\n" + add_str;
                }
                break;
            default:
                if (distance > 0 && add_str.length() == 0) {
                    add_str = "\n" + genes_between  + " genes between. " + distance + " bp\n\n" + add_str;
                }
                break;
        }
        return add_str;
    }
    
    /**
     * 
     * @param hm_node_list
     * @param gene_nodes_per_seq
     * @param gene_positions_per_sequence
     * @return 
     */
    public HashMap<String, ArrayList<Node>> retrieve_mrnas_per_genome_map(ArrayList<Node> hm_node_list, HashMap<String, Node[]> gene_nodes_per_seq, 
            HashMap<String, TreeSet<Integer>> gene_positions_per_sequence) {
        
        HashMap<String, ArrayList<Node>> mrnas_per_position = new HashMap<>();
        TreeSet<Integer> all_genomes_set = new TreeSet<>();
        int hm_counter = 0;
        StringBuilder multiple_copies = new StringBuilder();
        for (Node hm_node : hm_node_list) {
            String group_id = hm_node.getId() +"";
            hm_counter ++;
            if (hm_counter % 10 == 0 || hm_counter == hm_node_list.size()) {
                System.out.print("\rRetrieving mRNAs from homology groups: " + hm_counter + "/" + hm_node_list.size());
            }
            test_if_correct_label(hm_node, HOMOLOGY_GROUP_LABEL, true);
            int[] num_members = (int[]) hm_node.getProperty("copy_number");
            Iterable<Relationship> hm_relations = hm_node.getRelationships();
            boolean first = true;
            for (int i=1; i < num_members.length; i++) {
                if (num_members[i] > 1) {
                    if (first) {
                        multiple_copies.append("group ").append(group_id).append("\n");
                        first = false;
                    }
                    multiple_copies.append(i).append(", ").append(num_members[i]).append(" copies\n");
                }
            }
            for (Relationship rel : hm_relations) {
                Node mrna_node = rel.getEndNode();
                Relationship mrna_rel = mrna_node.getSingleRelationship(RelTypes.codes_for, Direction.INCOMING);
                Node gene_node = mrna_rel.getStartNode();
                int genome_nr = (int) mrna_node.getProperty("genome");
                if (skip_array[genome_nr-1]) {
                    continue;
                }  
                int sequence = (int) mrna_node.getProperty("sequence");
                String identifier = genome_nr + "_" + sequence;
                Node[] gene_nodes = gene_nodes_per_seq.get(identifier);
               
                int pos = -1;
                for(int i = 0; i < gene_nodes.length; i++) {
                    if (gene_nodes[i].getId() == gene_node.getId()) {
                        pos = i;
                        break;
                    }
                }
                TreeSet<Integer> gene_positions = gene_positions_per_sequence.get(identifier);
                if (gene_positions == null) {
                    gene_positions  = new TreeSet<>();
                }
                gene_positions.add(pos);
                gene_positions_per_sequence.put(identifier, gene_positions);
                try_incr_AL_hashmap(mrnas_per_position, identifier + "#" + pos, mrna_node);
                ArrayList<Node> gene = new ArrayList<>();
                gene.add(gene_node);
                mrnas_per_position.put(identifier + "#" + pos + "_gene", gene);
                all_genomes_set.add(genome_nr);
            }
        }
       
        core_threshold = all_genomes_set.size() * core_threshold / 100; // transform percentage to number of genomes 
        Pantools.logger.info("Threshold for reporting shared geneclusters is {}.", core_threshold);
        String multiple_copies_str = multiple_copies.toString();
        if (multiple_copies_str.length() > 0) {
            write_string_to_file_in_DB(multiple_copies_str, "MULTIPLE_COPIES");
        }
        return mrnas_per_position;
    }
   
    /**
     * Create two files 
     * 1. core_unique_thresholds.csv, he number of core/softcore unique/cloud homology groups for all tested thresholds. 
     * 2. thresholds_r.csv, the input file for the core/unique thresholds Rscript 
     * @param total_hmgroups_map
     * @param total_hmgroups
     * @param normal_core
     * @param normal_unique 
     */
    public static void create_core_unique_thresholds_rscript_input(HashMap<Integer, int[]> total_hmgroups_map, int total_hmgroups, int normal_core, int normal_unique) {
        StringBuilder rscript_builder = new StringBuilder("x,y,Category\n");
        rscript_builder.append("0.00,").append(normal_unique).append(",Unique/Cloud\n");
        double genomes_per_percent = adj_total_genomes/100.0;
        StringBuilder unique_builder = new StringBuilder("#Unique/Cloud cut-off, maximum allowed genomes, groups, percentage of all groups\n");
        String percentage = get_percentage_str(normal_unique, total_hmgroups, 2); 
        unique_builder.append("0.00 no cut-off,1,").append(normal_unique).append(",").append(percentage).append("\n");
        ArrayList<String> core_output_lines = new ArrayList<>();
        String core_output;
        for (int j=1; j <= 100; j++) { 
            int[] values = total_hmgroups_map.get(j);
            if (j == 100) {
                percentage = get_percentage_str(values[0], total_hmgroups, 2); 
                core_output = "1.0 no cut-off," + total_genomes + "," + values[0] + "," + percentage + "\n";
                rscript_builder.append("1.0,").append(values[0]).append(",Core/Softcore\n");  
            } else if (j > 9) {
                rscript_builder.append("0.").append(j).append(",").append(values[0]).append(",Core/Softcore\n")
                        .append("0.").append(j).append(",").append(values[1]).append(",Unique/Cloud\n");
                percentage = get_percentage_str(values[1], total_hmgroups, 2); 
                unique_builder.append("0.").append(j).append(",").append(Math.round((j*genomes_per_percent))).append(",").append(values[1]).append(",").append(percentage).append("\n");
                percentage = get_percentage_str(values[0], total_hmgroups, 2); 
                core_output = "0." + j + "," + Math.round((j*genomes_per_percent)) + "," + values[0] + "," + percentage + "\n";
            } else {
                rscript_builder.append("0.0").append(j).append(",").append(values[0]).append(",Core/Softcore\n")
                        .append("0.0").append(j).append(",").append(values[1]).append(",Unique/Cloud\n");
                percentage = get_percentage_str(values[1], total_hmgroups, 2); 
                unique_builder.append("0.0").append(j).append(",").append(Math.round((j*genomes_per_percent))).append(",").append(values[1]).append(",").append(percentage).append("\n");
                percentage = get_percentage_str(values[0], total_hmgroups, 2); 
                core_output = "0.0" + j + "," + Math.round((j*genomes_per_percent)) + "," + values[0] + "," + percentage + "\n";
            }
            core_output_lines.add(core_output);
        }
        Collections.reverse(core_output_lines); // arraylist in reverse order 
        StringBuilder core_builder = new StringBuilder("#Core/Softcore cut-off, Minimum allowed genomes, groups, percentage of all groups\n");
        for (String line : core_output_lines) {
            core_builder.append(line);
        }
        write_SB_to_file_in_DB(rscript_builder, "core_unique_thresholds/thresholds_r.csv"); // for Rscript 
        write_string_to_file_in_DB("#Number of homology groups when changing core and unique thresholds\n\n" 
                + core_builder.toString() + "\n" 
                + unique_builder.toString(), "core_unique_thresholds/core_unique_thresholds.csv");
    }
    
    /**
     * Create a Rscript to plot the core and unique thresholds 
     */
    public static void create_core_unique_thresholds_rscript() {
        String R_LIB = check_r_libraries_environment();
        StringBuilder script_build = new StringBuilder();
        script_build.append("#!/usr/bin/env Rscript\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"ggplot2\", lib=\"").append(R_LIB).append("\")\n")
                .append("library(ggplot2)\n")
                .append("d = read.csv(\"").append(WD_full_path).append("core_unique_thresholds/thresholds_r.csv\", sep=\",\",header = TRUE)\n")
                .append("ggplot(d, aes(x=x, y=y, color=Category)) +\n")
                .append("    geom_point() +\n")
                .append("    #geom_smooth(method=lm, aes(fill=Category), se=TRUE, color=\"darkred\", fullrange=FALSE, level=0.95) +\n")
                .append("    theme(text = element_text(size=12)) +\n")
                .append("    labs(x = \"Cut-off\") +\n")
                .append("    labs(y = \"Number of homology groups\") +\n")
                .append("    theme_classic(base_size = 13)\n")
                .append("    #ggtitle(\"Number of classified core & unique homology groups using different cut-offs\")\n\n")
                .append("pdf(NULL)\n")
                .append("ggsave(\"").append(WD_full_path).append("core_unique_thresholds/core_unique_thresholds.png\", width = 25, height = 10, units = \"cm\")\n")
                .append("print(\"Threshold plot written to: ").append(WD_full_path).append("core_unique_thresholds/core_unique_thresholds.png\")"); 
        write_SB_to_file_in_DB(script_build, "core_unique_thresholds/core_unique_thresholds.R");
    }
   
    /**
     * Count all functional annotations in the pangenome and put the counts in function_count_map
     * @return function_count_map
     */
    public HashMap<String, int[]> count_functions_for_metrics() {
        HashMap<String, HashSet<Node>> mrnas_with_function_set = new HashMap<>();
            // key is genome nr or sequence id. value are mrna's with at least one function
        HashMap<String, int[]> function_count_map = new HashMap<>(); // key is genome or sequence id + type of function or "ALL"  
       
        count_phobius_signalp_for_metrics(mrnas_with_function_set, function_count_map);
        count_cog_for_metrics(mrnas_with_function_set, function_count_map);
        get_BGCs_per_sequence(mrnas_with_function_set, function_count_map);
        
        Label[] labels = new Label[]{GO_LABEL, PFAM_LABEL, INTERPRO_LABEL, TIGRFAM_LABEL};
        RelationshipType[] reltypes = new RelationshipType[]{RelTypes.has_go, RelTypes.has_pfam, RelTypes.has_interpro, RelTypes.has_tigrfam};
        for (int i = 0; i < labels.length; i++) {
            HashMap<String, HashSet<Node>> temp_mrnas_with_function_set = new HashMap<>();
            HashMap<String, HashSet<Node>> temp_function_per_sequence_set = new HashMap<>();
            Label function_label = labels[i];
            RelationshipType reltype = reltypes[i];   
            String[] label_array = function_label.toString().split("_"); // example = pfam_label 
            String type = label_array[0]; // example = pfam
            String type_uppercase = type.toUpperCase(); // example = PFAM
            int node_counter = 0, total_rels = 0, connected_counter = 0;
            int total_nodes = (int) count_nodes(function_label);
            ResourceIterator<Node> all_nodes = GRAPH_DB.findNodes(function_label);
            HashMap<String, HashSet<Node>> count_mrna_nodes = new HashMap<>(); // key is sequence identifier 
            HashMap<String, int[]> count_functions_per_genome_sequence = new HashMap<>();  
            while (all_nodes.hasNext()) {
                Node function_node = all_nodes.next();
                Iterable<Relationship> all_relations = function_node.getRelationships(reltype);
                if (node_counter < 100 || node_counter % 101 == 0 || node_counter == total_nodes) {
                    System.out.print("\rCounting '" + type_uppercase + "' nodes and its relationships: " + node_counter + "/" + total_nodes + "    " );
                }
                boolean connnected = false;
                for (Relationship rel : all_relations) {
                    Node mrna_node = rel.getStartNode();                 
                    int genome_nr = (int) mrna_node.getProperty("genome");
                    if (!PROTEOME) { // skip the mRNAs from annotations that are not currently selected.
                        String annotation_id = (String) mrna_node.getProperty("annotation_id");
                        if (!annotation_identifiers.contains(annotation_id)) {
                            continue;
                        }
                    } else if (skip_array[genome_nr-1]) { // proteome is true
                        continue;
                    }

                    if (!PROTEOME && !mrna_node.hasProperty("longest_transcript")) {
                        continue; // will only be counted if they are part of the longest transcript
                    } 
                    connnected  = true;
                    int [] function_genome_counts = count_functions_per_genome_sequence.get(genome_nr + "");
                    if (function_genome_counts == null) {
                        function_genome_counts = new int[3];
                    }
                    function_genome_counts[2] ++;
                    count_functions_per_genome_sequence.put(genome_nr + "", function_genome_counts);
                    try_incr_hashset_hashmap(temp_function_per_sequence_set, genome_nr+"", function_node);
                    try_incr_hashset_hashmap(temp_mrnas_with_function_set, genome_nr +"", mrna_node);
                    try_incr_hashset_hashmap(mrnas_with_function_set, genome_nr +"", mrna_node);
                    total_rels ++;
                   
                    if (PROTEOME) { // no address properties available in panproteome so continue
                        continue;
                    }
                    int[] address = (int[]) mrna_node.getProperty("address");
                    int [] function_seq_counts = count_functions_per_genome_sequence.get(address[0] + "_" + address[1]);
                    if (function_seq_counts == null) {
                        function_seq_counts = new int[3];
                    }
                    function_seq_counts[2] ++;
                    count_functions_per_genome_sequence.put(address[0] + "_" + address[1], function_seq_counts);
                    
                    try_incr_hashset_hashmap(count_mrna_nodes, address[0] + "_" + address[1] , mrna_node);
                    try_incr_hashset_hashmap(temp_mrnas_with_function_set, address[0] + "_" + address[1], mrna_node);  
                    try_incr_hashset_hashmap(mrnas_with_function_set, address[0] + "_" + address[1], mrna_node);
                    try_incr_hashset_hashmap(temp_function_per_sequence_set, address[0] + "_" + address[1], function_node);   
                }
                node_counter ++;
                if (connnected) {
                    connected_counter ++;
                }
            }
            int[] node_counts = new int[]{connected_counter, node_counter, total_rels};
            function_count_map.put("pangenome#" + type, node_counts);

            for (String sequence_id : temp_mrnas_with_function_set.keySet()) {   
                int[] counts = count_functions_per_genome_sequence.get(sequence_id);
                int nr_function_nodes = temp_mrnas_with_function_set.get(sequence_id).size();
                counts[0] += nr_function_nodes;
                count_functions_per_genome_sequence.put(sequence_id, counts);
            }

            for (String sequence_id : temp_function_per_sequence_set.keySet()) {   
                int[] counts = count_functions_per_genome_sequence.get(sequence_id);
                int nr_mrna_nodes = temp_function_per_sequence_set.get(sequence_id).size();
                counts[1] += nr_mrna_nodes;
                count_functions_per_genome_sequence.put(sequence_id, counts);
            }
            for (String sequence_id : count_functions_per_genome_sequence.keySet()) {   
                function_count_map.put(sequence_id + "#" + type, count_functions_per_genome_sequence.get(sequence_id));
            }
            System.out.print("\r                                                                           " ); // spaces are intentional 
        }

        for (String sequence_id : mrnas_with_function_set.keySet()) {   
            int nr_function_nodes = mrnas_with_function_set.get(sequence_id).size();
            int[] total_nodes = new int[]{nr_function_nodes};
            function_count_map.put(sequence_id + "#all", total_nodes);
        }
        return function_count_map;
    }
    
    /**
     * Count phobius secreted, receptor and transmembrane nodes 
     * @param mrnas_with_function_set key is genome nr or sequence id. value are mrna's with at least one function
     * @param function_count_map
     */
    public void count_phobius_signalp_for_metrics(HashMap<String, HashSet<Node>> mrnas_with_function_set,
                                                  HashMap<String, int[]> function_count_map) {

        HashMap<String, Integer> temp_mrnas_with_function = new HashMap<>();
            // key is genome or sequence number with "#ph_secreted" or "ph_transmembrane"
        ResourceIterator<Node> signalp_nodes = GRAPH_DB.findNodes(SIGNALP_LABEL); // count signalp nodes
        int total_signalp = (int) count_nodes(SIGNALP_LABEL);
        long node_counter = 0;
        int signalp = 0;
        while (signalp_nodes.hasNext()) {
            node_counter ++;
            if (node_counter < 100 || node_counter % 101 == 0 || total_signalp == node_counter) {
                System.out.print("\rGathering SignalP annotations: " + node_counter + "/" + total_signalp + "   ");
            }
            Node mrna_node = signalp_nodes.next();
            int genome_nr = 0, sequence_nr = 0;
            if (PROTEOME) {
                genome_nr = (int) mrna_node.getProperty("genome");
            } else {
                int[] address = (int[]) mrna_node.getProperty("address");
                genome_nr = address[0];
                sequence_nr = address[1];
            }
            
            if (PROTEOME && skip_array[genome_nr-1]) { // skip the mRNAs from annotations that are not currently selected.
                continue;
            } else if (!PROTEOME) {
                String annotation_id = (String) mrna_node.getProperty("annotation_id");
                if (!annotation_identifiers.contains(annotation_id)) {
                    continue;
                }
            }
            if (!PROTEOME && !mrna_node.hasProperty("longest_transcript")) {
                continue; // will only be counted if they are part of the longest transcript
            } 
            signalp ++;
            try_incr_hashmap(temp_mrnas_with_function, genome_nr + "#signalp", 1);
            if (!PROTEOME) {
                try_incr_hashmap(temp_mrnas_with_function, genome_nr + "_" + sequence_nr + "#signalp", 1);
            }
            try_incr_hashset_hashmap(mrnas_with_function_set, genome_nr +"", mrna_node);
            if (!PROTEOME) {
                try_incr_hashset_hashmap(mrnas_with_function_set, genome_nr + "_" + sequence_nr, mrna_node);
            } 
        }
        
        // count phobius nodes 
        ResourceIterator<Node> all_nodes = GRAPH_DB.findNodes(PHOBIUS_LABEL);
        int total_phobius = (int) count_nodes(PHOBIUS_LABEL);
        node_counter = 0;
        int total_secreted = 0, total_transmembranes = 0;
        while (all_nodes.hasNext()) {
            node_counter ++;
            if (node_counter < 100 || node_counter % 101 == 0 || total_phobius == node_counter) {
                System.out.print("\rGathering Phobius annotations: " + node_counter + "/" + total_phobius  + "   ");
            }
            Node mrna_node = all_nodes.next();
            int genome_nr = 0, sequence_nr = 0;
            if (PROTEOME) {
                genome_nr = (int) mrna_node.getProperty("genome");
            } else {
                int[] address = (int[]) mrna_node.getProperty("address");
                genome_nr = address[0];
                sequence_nr = address[1];
            }
            
            if (PROTEOME && skip_array[genome_nr-1]) { // skip the mRNAs from annotations that are not currently selected.
                continue;
            } else if (!PROTEOME) {
                String annotation_id = (String) mrna_node.getProperty("annotation_id");
                if (!annotation_identifiers.contains(annotation_id)) {
                    continue;
                }
            }
            if (!PROTEOME && !mrna_node.hasProperty("longest_transcript")) {
                continue; // will only be counted if they are part of the longest transcript
            } 
            
            String signalpep = (String) mrna_node.getProperty("phobius_signal_peptide"); // can be 'yes' or 'no' 
            int transmem_domains = (int) mrna_node.getProperty("phobius_transmembrane");
            if (signalpep.equals("yes")) {
                try_incr_hashmap(temp_mrnas_with_function, genome_nr + "#ph_secreted", 1);
                if (!PROTEOME) {
                    try_incr_hashmap(temp_mrnas_with_function, genome_nr + "_" + sequence_nr + "#ph_secreted", 1);
                }
                total_secreted ++;
            }
            if (transmem_domains > 0) {
                try_incr_hashmap(temp_mrnas_with_function, genome_nr + "#ph_transmembrane", 1);
                if (!PROTEOME) {
                    try_incr_hashmap(temp_mrnas_with_function, genome_nr + "_" + sequence_nr + "#ph_transmembrane", 1);
                }    
                total_transmembranes ++;
            }
            try_incr_hashset_hashmap(mrnas_with_function_set, genome_nr +"", mrna_node);
            if (!PROTEOME) {
                try_incr_hashset_hashmap(mrnas_with_function_set, genome_nr + "_" + sequence_nr, mrna_node);
            } 
        }
        
        for (String key : temp_mrnas_with_function.keySet()) {
            int nr_of_mrnas = temp_mrnas_with_function.get(key);
            String[] key_array = key.split("#");
            int[] phobius_signalp_nodes = function_count_map.get(key_array[0] + "#phobius_signalp");
            if (phobius_signalp_nodes == null) {
                phobius_signalp_nodes = new int[3];
            }
            if (key_array[1].equals("ph_secreted")) {
                phobius_signalp_nodes[0] += nr_of_mrnas;
            } else if (key_array[1].equals("ph_transmembrane")) { // 
                phobius_signalp_nodes[1] += nr_of_mrnas;
            } else { // signalp
               phobius_signalp_nodes[2] += nr_of_mrnas;
            }
            function_count_map.put(key_array[0] + "#phobius_signalp", phobius_signalp_nodes);
        }

        for (int i=1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            if (!function_count_map.containsKey(i + "#phobius_signalp")) {
                int[] phobius_signalp_nodes = new int[3];
                function_count_map.put(i + "#phobius_signalp", phobius_signalp_nodes);
            }  
        }
        function_count_map.put("pangenome#phobius_signalp", new int[]{total_secreted, total_transmembranes, signalp});
    }
 
    /**
     * SELECTED_HMGROUPS must be set: the input file to a 'homology_group' node identifiers file
     * The variable can be set by the user with --homology-groups
     *
     * Homology group identifiers files have different formats for the core, accessory, unique and phenotype groups
     * @return list with 'homology_group' node identifiers
     */
    public static ArrayList<Node> read_hmgroup_input_file() {
        String hmgroups_str = "";
        String[] hmgroup_array = new String[0];
        if (SELECTED_HMGROUPS == null) {
            Pantools.logger.error("No homology groups were provided.");
            System.exit(1);
        }
        
        try (BufferedReader in = new BufferedReader(new FileReader(SELECTED_HMGROUPS))) {
            StringBuilder temp_hmgroup_str = new StringBuilder();
            for (int c = 0; in.ready();) {
                hmgroups_str = in.readLine().trim();
                if (hmgroups_str.equals("") || hmgroups_str.startsWith("Genome")) {
                    // do nothing
                } else if (!hmgroups_str.contains(" genomes") && !hmgroups_str.contains("Phenotype")
                        && !hmgroups_str.contains("#")) { // accessory hmgroups were provided
                    temp_hmgroup_str.append(hmgroups_str).append(", ");
                }
            }   
            temp_hmgroup_str = new StringBuilder(temp_hmgroup_str.toString().replaceFirst(".$", "").replaceFirst(".$", ""));
            hmgroups_str = temp_hmgroup_str.toString();
            if (hmgroups_str.contains("/")) {
                Pantools.logger.error("Homology group input not correctly formatted.");
                System.exit(1);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to open the groups provided via --homology-groups or -hm. Stopping now.");
            System.exit(1);
        }
        hmgroups_str = hmgroups_str.replace(", ",",").replace(" ,",",");
        if (hmgroups_str.equals("")) {
            Pantools.logger.error("There seems to be no identifiers in the provided homology groups.");
            System.exit(1);
        }
        if (hmgroups_str.endsWith(",")) {
            hmgroups_str = hmgroups_str.replaceFirst(".$","");
        }
        
        if (hmgroups_str.contains(", ")) {
            hmgroup_array = hmgroups_str.split(", ");
        } else if (hmgroups_str.contains(",")) {
            hmgroup_array = hmgroups_str.split(",");
        } else {
            hmgroup_array = new String[]{hmgroups_str};
        }
        
        ArrayList<Node> hm_node_list = new ArrayList<>();
        for (String hmgroup_str : hmgroup_array) {
            long hm_node_id = Long.valueOf(hmgroup_str);
            Node hm_node= GRAPH_DB.getNodeById(hm_node_id);
            hm_node_list.add(hm_node);
        }
        return hm_node_list;
    }
    
    /**
     * Is able to read the format of 'unique_groups.csv'
     * @return 
     */
    public static HashMap<Integer, String[]> read_unique_hmgroups() {
        HashMap<Integer, String[]> hms_per_genome = new HashMap<>();
        String[]line_array;
        try (BufferedReader in = new BufferedReader(new FileReader(SELECTED_HMGROUPS))) {
            int genome_nr = 0;
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim(); 
                if (line.equals("")) {
                     continue;
                }
                if (line.startsWith("Genome ")) {
                    line_array = line.split(" ");
                    genome_nr = Integer.parseInt(line_array[1]);
                } else {
                    hms_per_genome.put(genome_nr, line.split(","));
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to open: {}", SELECTED_HMGROUPS);
            System.exit(1);
        }
        return hms_per_genome;
    } 
    
    /**
     * Creates accessory_combinations.csv. The node identifiers of accessory homology groups, ordered by the combination of genomes by which they are shared.
     * @param accessory_combinations 
     */
    private void create_accessory_combination_output(HashMap<String, StringBuilder> accessory_combinations) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "gene_classification/accessory_combinations.csv"))) {
            out.write("#This file cannot be read by other PanTools functions. Split the file on the semicolon and use the homology group node identifiers in the right column\n");
            boolean done = false;
            int max_accessory = core_threshold - 1;
            while (!done) {
                String percentage = get_percentage_str(max_accessory, core_threshold, 2); 
                out.write("#" + max_accessory + " genomes " + percentage + "%\n");
                ArrayList<String> remove_keys = new ArrayList<>();
                for (String key : accessory_combinations.keySet()) {
                    String[] key_array =  key.split(",");
                    if (key_array.length != max_accessory) {
                        continue;
                    }
                    remove_keys.add(key);
                    StringBuilder value = accessory_combinations.get(key);
                    out.write(key.replace("[","").replace("]","").replace(" ","") + ";" + value.toString().replaceFirst(".$","") + "\n");
                }
                for (String key : remove_keys) {
                    accessory_combinations.remove(key);
                }
                max_accessory--;
                if (max_accessory == unique_threshold) {
                    done = true;
                }
            }
        } catch (IOException e) {
            Pantools.logger.error(e.getMessage());
            System.exit(1);
        }   
    }

   /**
    * Create file with statistics of the core, accessory, unique groups of the pangenome and individual genomes.
    * @param total_hmgroups
    * @param total_sco
    * @param core_access_uni_map
    * @param total_core_groups 
    * @param total_unique_groups 
    */
    
    public static void create_gclass_overview(ArrayList<String> genome_list,
                                              int total_hmgroups,
                                              int total_sco,
                                              HashMap<String, int[]> core_access_uni_map,
                                              int total_core_groups,
                                              int total_unique_groups) {

        StringBuilder output = new StringBuilder();
        //StringBuilder distribution_output1 = new StringBuilder("genes,class,phenotype\n"); // gene count // do not remove! 
        //StringBuilder distribution_output2 = new StringBuilder("genes,class,phenotype\n"); // gene percentage
        int mrna_count = 0;
        double lowest_core = 100, highest_core = 0, lowest_accessory = 100, highest_accessory = 0, lowest_unique = 100, highest_unique = 0;
        ArrayList<Double> core_pcs = new ArrayList<>();
        ArrayList<Double> unique_pcs = new ArrayList<>();
        ArrayList<Double> accessory_pcs = new ArrayList<>();
        for (String genome : genome_list) {
            try {
                if (skip_array[Integer.parseInt(genome) - 1]) {
                    output.append("Skipped genome ").append(genome).append("\n\n");
                    continue;
                }
            } catch (NumberFormatException ignores) {
            }

            int[] total_gene_counts = core_access_uni_map.get(genome + "_total"); // core, accessory, unique
            int[] distinct_gene_counts = core_access_uni_map.get(genome + "_distinct"); // core, accessory, unique
            int total = total_gene_counts[2] + total_gene_counts[1] + total_gene_counts[0];
            int distinct_total = distinct_gene_counts[2] + distinct_gene_counts[1] + distinct_gene_counts[0];
            mrna_count += total;
            double uni_pc = percentage(total_gene_counts[2], total);
            unique_pcs.add(uni_pc);
            String uni_pc_str = String.format("%.2f", uni_pc);
            if (uni_pc < lowest_unique) {
               lowest_unique = uni_pc;
            } 
            if (uni_pc > highest_unique) {
                highest_unique = uni_pc;
            }
            
            double accessory_pc = percentage(total_gene_counts[1], total);
            accessory_pcs.add(accessory_pc);
            String access_pc_str = String.format("%.2f", accessory_pc);
            if (accessory_pc < lowest_accessory) {
               lowest_accessory = accessory_pc;
            } 
            if (accessory_pc > highest_accessory) {
                highest_accessory = accessory_pc;
            }
            
            double core_pc = percentage(total_gene_counts[0], total);
            core_pcs.add(core_pc);
            String core_pc_str = String.format("%.2f", core_pc);
            if (core_pc < lowest_core) {
               lowest_core = core_pc;
            } 
            if (core_pc > highest_core) {
                highest_core = core_pc;
            }
           
            output.append("Genome ").append(genome);
            if (PHENOTYPE != null) { //to prevent a number format exception in the next line
                if (geno_pheno_map.containsKey(Integer.parseInt(genome))) {
                    String phenotype = geno_pheno_map.get(Integer.parseInt(genome));
                    phenotype = phenotype.replace("_", " ");
                    output.append(" (").append(phenotype).append(")");
                    // do not remove!
                /*
                distribution_output1.append(total_gene_counts[0]).append(",Core,").append(phenotype).append("\n");
                distribution_output2.append(core_pc).append(",Core,").append(phenotype).append("\n");
                distribution_output1.append(total_gene_counts[1]).append(",Accessory,").append(phenotype).append("\n" );
                distribution_output2.append(accessory_pc).append(",Accessory,").append(phenotype).append("\n");
                distribution_output1.append(total_gene_counts[2]).append(",Unique,").append(phenotype).append("\n");
                distribution_output2.append(uni_pc).append(",Unique,").append(phenotype).append("\n");
                */
                }
            }
            output.append("\nTotal mRNAs: ").append(total);
            if (total > 0) {
                output.append("\nCore: ").append(total_gene_counts[0]).append(" ").append(core_pc_str).append("%\n") 
                    .append("Accessory: ").append(total_gene_counts[1]).append(" ").append(access_pc_str).append("%\n")
                    .append("Unique: ").append(total_gene_counts[2]).append(" ").append(uni_pc_str).append("%\n\n")
                    .append("Total groups: ").append(distinct_total)
                    .append("\nCore: ").append(distinct_gene_counts[0])
                    .append("\nAccessory: ").append(distinct_gene_counts[1])
                    .append("\nUnique: ").append(distinct_gene_counts[2]);
           }
           output.append("\n\n");
        }
        int total_access_groups = total_hmgroups - total_core_groups- total_unique_groups;
        String sco_pc = get_percentage_str(total_sco, total_hmgroups, 2);
        String total_uni_pc = get_percentage_str(total_unique_groups, total_hmgroups, 2);
        String total_access_pc = get_percentage_str(total_access_groups, total_hmgroups, 2);
        String total_core_pc = get_percentage_str(total_core_groups, total_hmgroups, 2);
        
        double[] unique_m_a_sd = median_average_stdev_from_AL_double(unique_pcs);
        double[] core_m_a_sd = median_average_stdev_from_AL_double(core_pcs);
        double[] accessory_m_a_sd = median_average_stdev_from_AL_double(accessory_pcs);
        DecimalFormat df = new DecimalFormat("0.00");
        String output_header = "Grouping version: " + grouping_version 
                + "\nTotal genomes: " + genome_list.size()
                + "\nTotal mRNAs: " + mrna_count 
                + "\nTotal homology groups: " + total_hmgroups 
                + "\nSingle copy ortholog groups: " + total_sco + " " + sco_pc + "%"
                + "\n\nCore groups: " + total_core_groups + " " + total_core_pc + "%\n"
                + "Accessory groups: " + total_access_groups + " " + total_access_pc + "%\n"
                + "Unique groups: " + total_unique_groups + " " + total_uni_pc + "%\n\n"
                + "Average % of genes per class for individual genomes, standard deviation, range\n" 
                + "Core " + df.format(core_m_a_sd[1]) + ", " + df.format(core_m_a_sd[2]) + ", " 
                        + df.format(lowest_core) + "-" +  df.format(highest_core) + "%\n"
                + "Accessory " + df.format(accessory_m_a_sd[1]) + ", " + df.format(accessory_m_a_sd[2]) + 
                        ", " + df.format(lowest_accessory) + "-" + df.format(highest_accessory) + "%\n"
                + "Unique " + df.format(unique_m_a_sd[1])  + ", " + df.format(unique_m_a_sd[2]) + ", " 
                        + df.format(lowest_unique) + "-" + df.format(highest_unique) + "%\n\n";
        write_string_to_file_in_DB(output_header + output.toString(), "gene_classification/gene_classification_overview.txt");
        /*
        if (PHENOTYPE != null) { // do not remove! 
            write_SB_to_file_in_DB(distribution_output1, "gene_classification/class_distribution1.csv");
            write_SB_to_file_in_DB(distribution_output2, "gene_classification/class_distribution2.csv"); 
        }*/
    }

    /**
     *
     * @return array with phenotype nodes on genomeNr-1 positions
     */
    private Node[] getExistingPhenotypeNodes() {
        Node[] phenotypeNodes = new Node[total_genomes];
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> phenotypeNodeIterator = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (phenotypeNodeIterator.hasNext()) {
                Node phenotypeNode = phenotypeNodeIterator.next();
                int genomeNr = (int) phenotypeNode.getProperty("genome");
                phenotypeNodes[genomeNr - 1] = phenotypeNode;
                phenotypeNode.getPropertyKeys();
            }
            tx.success();
        }
        return phenotypeNodes;
    }

    /**
     *
     */
    private void removePreviousPhenotypeNodes() {
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> phenotypeNodeIterator = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (phenotypeNodeIterator.hasNext()) {
                Node phenotypeNode = phenotypeNodeIterator.next();
                Iterable<Relationship> relations = phenotypeNode.getRelationships();
                for (Relationship relation : relations) {
                    relation.delete();
                }
            }
            tx.success();
        }
    }

    /**
     * 
     * @param seq
     * @param start_ptr
     * @param address
     * @return 
     */
    public static HashSet<Node> extract_sequence_return_unique_nodes(StringBuilder seq, IndexPointer start_ptr, int[] address) {
        address[3] += K_SIZE;
        Relationship rel;
        Node neighbor, node;
        HashSet<Node> node_set = new HashSet<>();
        int[] addr = new int[]{address[0], address[1], address[2], address[3]};
        int begin = addr[2] - 1, end = addr[3] - 1;
        int len = 0, node_len, neighbor_len = 0, seq_len, position;
        String rel_name, origin = "G" + address[0] + "S" + address[1], neighbor_str , node_str;
        seq_len = end - begin + 1;
        seq.setLength(0);
        position = start_ptr.offset;
        node = GRAPH_DB.getNodeById(start_ptr.node_id);
        node_set.add(node);
        node_len = (int) node.getProperty("length");
       
        // Takes the part of the region lies in the first node of the path that region takes in the graph
        if (start_ptr.canonical) {
            if (position + seq_len - 1 <= node_len - 1) { // The whole sequence lies in this node
                len += seqLayer.append_fwd(seq, (String) node.getProperty("sequence"), position, position + seq_len - 1);
            } else {
                len += seqLayer.append_fwd(seq, (String) node.getProperty("sequence"), position, node_len - 1);
            }
        } else {
            if (position - (seq_len - 1) >= 0) { // The whole sequence lies in this node
                len += seqLayer.append_rev(seq, (String) node.getProperty("sequence"), position - (seq_len - 1), position);
            } else {
                len += seqLayer.append_rev(seq, (String) node.getProperty("sequence"), 0, position);
            }
        }
        
        if (start_ptr.canonical) {
            int real_length = node_len - (K_SIZE-1);
            real_length -= (position+1);
            len = node_len - real_length;
        } else { 
            len = node_len - position;
        }
       
        while (len < seq_len) {  
            addr[2] = (begin + len) - K_SIZE + 1;      
            rel = seqLayer.get_outgoing_edge(node, origin, addr[2]);
            if (rel == null) {
                Pantools.logger.warn("This function is interrupted because there is a bug in locate() function!! Region {}.", Arrays.toString(address));
                return node_set;
            }
            neighbor = rel.getEndNode();
            node_set.add(neighbor);
            rel_name = rel.getType().name();
            neighbor_len = (int) neighbor.getProperty("length");
           
            if (rel_name.charAt(1) == 'F') {// Enterring forward side
                if (len + neighbor_len - K_SIZE + 1 > seq_len) // neighbor is the last node of the path
                    len += seqLayer.append_fwd(seq, (String) neighbor.getProperty("sequence"), K_SIZE - 1, seq_len - len + K_SIZE - 2);
                else 
                    len += seqLayer.append_fwd(seq, (String) neighbor.getProperty("sequence"), K_SIZE - 1, neighbor_len - 1);
            } else { // Enterring reverse side
                if (len + neighbor_len - K_SIZE + 1 > seq_len) // neighbor is the last node of the pat
                    len += seqLayer.append_rev(seq, (String) neighbor.getProperty("sequence"), neighbor_len - K_SIZE - (seq_len - len) + 1, neighbor_len - K_SIZE);
                else 
                    len += seqLayer.append_rev(seq, (String) neighbor.getProperty("sequence"), 0, neighbor_len - K_SIZE);
            }
            node = neighbor;
        } // while
        node_set.add(node);
        return node_set;
    }
    
    /**
     * 
     * @param value1
     * @param value2
     * @param decimals number of decimals the percentage should be rounded to
     * @return 
     */
    public static String get_percentage_str(long value1, long value2, int decimals) {
        double percentage = ((double) value1 / (double) value2) * 100;
        if (Double.isNaN(percentage)) {
            return "0";
        }
        return String.format("%." + decimals + "f", percentage);
    }
    
    /**
     * @param value1
     * @param value2
     * @param decimals  number of decimals the percentage should be rounded to
     * @return
     */
    public static String get_percentage_str(int value1, int value2, int decimals) {
        double percentage = ((double) value1 / (double) value2) * 100;
        if (Double.isNaN(percentage)) {
            return "0";
        }
        return String.format("%." + decimals + "f", percentage);
    }
    
    /**
     * @param value1
     * @param value2
     * @param decimals number of decimals the percentage should be rounded to
     * @return 
     */
    public static String get_percentage_str(double value1, double value2, int decimals) {
        double percentage = (value1 / value2) * 100;
        if (Double.isNaN(percentage)) {
            return "0";
        }
        return String.format("%." + decimals + "f", percentage);
    }
      
    /**
     * Writes group_size_occurrences.txt as part of the gene_classifications directory
     * @param hmsize_frequency 
     */
    private void write_hmgroup_size_frequencies(ArrayList<String> genome_list, HashMap<String, Integer> hmsize_frequency) {
        ArrayList<Integer> all_sizes_genomes = new ArrayList<>();
        ArrayList<Integer> all_sizes_total = new ArrayList<>();
        for (String hmgroup_size : hmsize_frequency.keySet()) { 
            if (hmgroup_size.endsWith("_genomes")) {
                hmgroup_size = hmgroup_size.replace("_genomes", "");
                int size = Integer.parseInt(hmgroup_size);
                all_sizes_genomes.add(size);
            } else { // ends with "_total" 
                hmgroup_size = hmgroup_size.replace("_total", "");
                int size = Integer.parseInt(hmgroup_size);
                all_sizes_total.add(size);
            }
       }
       StringBuilder output_builder = new StringBuilder("Two overviews of homology group sizes based on number of proteins and number of genomes\n"
            + "#Group size based on number of proteins, occurrences\n");
       create_hmgroup_freq(all_sizes_total, hmsize_frequency, output_builder, "_total");
       output_builder.append("\n\n#Group size based on number of genomes (maximum of ").append(genome_list.size()).append("), occurrences\n");
       create_hmgroup_freq(all_sizes_genomes, hmsize_frequency, output_builder, "_genomes");
       write_SB_to_file_in_DB(output_builder, "gene_classification/group_size_occurrences.txt");
    }
    
    /**
     * 
     * @param all_sizes_list
     * @param hmsize_frequency
     * @param output_builder
     * @param type 
     */
    public static void create_hmgroup_freq(ArrayList<Integer> all_sizes_list, HashMap<String, Integer> hmsize_frequency, StringBuilder output_builder, String type) {     
        Collections.sort(all_sizes_list);
        for (int size : all_sizes_list) {
            int value = hmsize_frequency.get(size + type);
            output_builder.append(size).append(", ").append(value).append("\n");
        }
    }
    
    /**
     * Calculate the GC content per sequence 
     * GC ratio per block (of 1000bp) is currently disabled. This is used for CIRCOS plots and the ability to do this should not be removed
     * @param genome_nr
     * @param genome_size
     * @param nuc_count_per_seq
     */
    public void calculate_gc_content(int genome_nr, long genome_size, HashMap<String, long[]> nuc_count_per_seq) {
        StringBuilder gc_block_builder = new StringBuilder(); // not used but do not remove! important for CIRCOS plots  
        int block = (int) genome_size / 5000; // do not remove, for circos
        if (check_if_file_exists(WORKING_DIRECTORY + "metrics/GC/" + genome_nr + "/nucleotide_count.txt")) {
            read_nucleotide_count(genome_nr, nuc_count_per_seq);
            return;
        }

        for (int sequence = 1; sequence <= GENOME_DB.num_sequences[genome_nr]; ++sequence) { // retrieve the sequence from the genomeDb
            StringBuilder seq = new StringBuilder();
            long[] nucleotide_count = new long []{0,0,0,0,0,0}; // A, T, C, G, N, other 
            int begin = 0;
            int end = (int) GENOME_DB.sequence_length[genome_nr][sequence] - 1;
            GENOME_SC.get_gc_sequence_calc_per_block(seq, genome_nr, sequence, begin, end - begin + 1, true,
                    nucleotide_count, block, gc_block_builder); // function is found in sequenceScanner.java
            nuc_count_per_seq.put(genome_nr + "_" + sequence, nucleotide_count);
        }
        //write_SB_to_file_in_DB(gc_block_builder, "metrics/GC/" + genome + "/gc_per_block.txt"); // Do not remove! important for CIRCOS plots
        long[] nucleotide_count = write_nucleotide_counts_to_file(nuc_count_per_seq, genome_nr);
        nuc_count_per_seq.put(genome_nr +"", nucleotide_count); // GC content of the complete genome  
    }
    
    /**
     * This output file is recognized next time so the GC content does not need to be calculated again 
     * @param nuc_count_per_seq
     * @param genome_nr
     * @return nucleotide counts for the complete genome
     */
    public long[] write_nucleotide_counts_to_file(HashMap<String, long[]> nuc_count_per_seq, int genome_nr) {
        long[] nucleotide_count = {0,0,0,0,0,0}; // A, T, C, G, N, other 
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "metrics/GC/" + genome_nr + "/nucleotide_count.txt"))) {
            for (Map.Entry<String, long[]> entry : nuc_count_per_seq.entrySet()) {
                String sequence_id = entry.getKey();
                if (!sequence_id.startsWith(genome_nr + "_")) {
                    continue;
                }
                long[] nuc_counts_sequence = entry.getValue(); // [A, T, C, G, N, other]
                nucleotide_count[0] += nuc_counts_sequence[0];
                nucleotide_count[1] += nuc_counts_sequence[1];
                nucleotide_count[2] += nuc_counts_sequence[2];
                nucleotide_count[3] += nuc_counts_sequence[3];
                nucleotide_count[4] += nuc_counts_sequence[4];
                nucleotide_count[5] += nuc_counts_sequence[5];
                out.write("#sequence " + sequence_id + "\nA: " + nuc_counts_sequence[0] + " T: " + nuc_counts_sequence[1] +
                        " C: " + nuc_counts_sequence[2] + " G: " + nuc_counts_sequence[3] + " N: " + nuc_counts_sequence[4] + 
                        " other: " + nuc_counts_sequence[5] + "\n");
            }
            out.close();
        } catch (IOException e) {
            Pantools.logger.error("Unable to read: {}metrics/GC/{}/nucleotide_count.txt", WORKING_DIRECTORY, genome_nr);
            System.exit(1);
        }
        return nucleotide_count;
    }
    
    /**
     * Read the file with nucleotide counts that was created in a previous run
     * @param genome_nr
     * @param nuc_count_per_seq 
     */
    public static void read_nucleotide_count(int genome_nr, HashMap<String, long[]> nuc_count_per_seq) {
        long[] total_count = {0,0,0,0,0,0}; // nucleotide counts for the entire genome, [A, T, C, G, N, other]
        try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "metrics/GC/" + genome_nr + "/nucleotide_count.txt"))) { // read the GFF file 
            String identifier = "";
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                String[] line_array = line.split(" ");
                if (line.startsWith("#")) {
                    identifier = line_array[1];
                    continue;
                }
                int position = 0;
                long[] sequence_counts = {0,0,0,0,0,0};  // nucleotide counts for a sequence, [A, T, C, G, N, other]
                for (int i=1; i<= line_array.length-1; i += 2) {  
                    sequence_counts[position] = Long.parseLong(line_array[i]);
                    total_count[position] += Long.parseLong(line_array[i]);
                    position ++;
                }
                nuc_count_per_seq.put(identifier, sequence_counts); 
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read info file.");
            System.exit(1);
        }
        nuc_count_per_seq.put(genome_nr + "", total_count); 
    }

    // universal function
    /**
     * String and Integer
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void incr_hashmap(HashMap<String, Integer> hashmap, String key, int value) {
        int total_value = hashmap.get(key);
        total_value += value;
        hashmap.put(key, total_value);
    }

    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_hashmap(HashMap<String, Integer> hashmap, String key, int value) {
        int total_value;
        if (hashmap.containsKey(key)) {
            total_value = hashmap.get(key);
            total_value += value;
        } else {
            total_value = value;
        }
        hashmap.put(key, total_value);
    }
    
    /**
     * String and Long
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_hashmap(HashMap<String, Long> hashmap, String key, long value) {
        long total_value;
        if (hashmap.containsKey(key)) {
            total_value = hashmap.get(key);
            total_value += value;
        } else {
            total_value = value;
        }
        hashmap.put(key, total_value);
    }

    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_hashmap(HashMap<Integer, String> hashmap, int key, String value) {
        String total_value;
        if (hashmap.containsKey(key)) {
            total_value = hashmap.get(key);
            total_value += value;
        } else {
            total_value = value;
        }
        hashmap.put(key, total_value);
    }

    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void incr_hashmap(HashMap<Integer, String> hashmap, int key, String value) {
        String total_value = hashmap.get(key);
        if (total_value != null) {
            total_value += value ;
        } else {
            total_value = value;
        }
        hashmap.put(key, total_value);
    }

    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_hashmap(HashMap<Long, Long> hashmap, long key, long value) {
        long total_value;
        if (hashmap.containsKey(key)) {
            total_value = hashmap.get(key);
            total_value += value;
        } else {
            total_value = value;
        }
        hashmap.put(key, total_value);
    }

    //Stringbuilders
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_SB_hashmap(HashMap<String, StringBuilder> hashmap, String key, String value) {
        StringBuilder output_builder;
        try {
             output_builder = hashmap.get(key);
             output_builder.append(value);
        } catch (NullPointerException null1) {
           output_builder = new StringBuilder(value); 
        }
        hashmap.put(key, output_builder);
    }
    
    //SB Integer
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_SB_hashmap(HashMap<Integer, StringBuilder> hashmap, int key, String value) {
        StringBuilder builder;
        try {
            builder = hashmap.get(key);
            builder.append(value);
        } catch (NullPointerException null1) {
            builder = new StringBuilder(value);
        }
        hashmap.put(key, builder);
    }
    
    //SB node
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_SB_hashmap(HashMap<Node, StringBuilder> hashmap, Node key, String value) {
        StringBuilder builder;
        try {
            builder = hashmap.get(key);
            builder.append(value);
        } catch (NullPointerException null1) {
            builder = new StringBuilder(value);
        }
        hashmap.put(key, builder);
    }

    //ArrayList
    //AL Double
    /**
     *
     * @param hashmap
     * @param key
     * @param value
     */
    public static void try_incr_AL_hashmap(HashMap<String, ArrayList<Object>> hashmap, String key, Object value) {
        ArrayList<Object> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }

    //ArrayList
    //AL Double
    /**
     *
     * @param hashmap
     * @param key
     * @param value
     */
    public static void try_incr_AL_hashmap(HashMap<Integer, ArrayList<Object>> hashmap, int key, Object value) {
        ArrayList<Object> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }

    //ArrayList
    //AL Double 
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_AL_hashmap(HashMap<Double, ArrayList<String>> hashmap, Double key, String value) {
        ArrayList<String> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    } 
     
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_AL_hashmap(HashMap<Double, ArrayList<Integer>> hashmap, double key, int value) {
        ArrayList<Integer> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }
    
    //AL String
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_AL_hashmap(HashMap<String, ArrayList<Double>> hashmap, String key, Double value) {
        ArrayList<Double> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        } 
        value_array.add(value);
        hashmap.put(key, value_array);
    }
    
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_AL_hashmap(HashMap<String, ArrayList<Integer>> hashmap, String key, int value) {
        ArrayList<Integer> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }
    
     /**
      * 
      * @param hashmap
      * @param key
      * @param value 
      */
    public static void try_incr_AL_hashmap(HashMap<String, ArrayList<String>> hashmap, String key, String value) {
        ArrayList<String> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }
    
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_AL_hashmap(HashMap<String, ArrayList<Node>> hashmap, String key, Node value) {
        ArrayList<Node> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }

    //AL with Integer as key
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_AL_hashmap(HashMap<Integer, ArrayList<String>> hashmap, int key, String value) {
        ArrayList<String> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }
    
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_AL_hashmap(HashMap<Integer, ArrayList<Node>> hashmap, int key, Node value) {
        ArrayList<Node> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }
    
    //AL long
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_AL_hashmap(HashMap<Node, ArrayList<Node>> hashmap, Node key, Node value) {
        ArrayList<Node> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        } 
        value_array.add(value);
        hashmap.put(key, value_array);
    } 
    
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_AL_hashmap(HashMap<Node, ArrayList<String>> hashmap, Node key, String value) {
        ArrayList<String> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    } 
    
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_AL_hashmap(HashMap<Node, ArrayList<Integer>> hashmap, Node key, int value) {
        ArrayList<Integer> value_list = hashmap.get(key);
        if (value_list == null) {
            value_list = new ArrayList<>();
        }
        value_list.add(value);
        hashmap.put(key, value_list);
    }
    
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public synchronized static void try_incr_AL_chashmap(ConcurrentHashMap <String, ArrayList<Node>> hashmap, String key, Node value) {
        ArrayList<Node> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }
     
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public synchronized static void try_incr_AL_chashmap(ConcurrentHashMap<Integer, ArrayList<Integer>> hashmap, int key, int value) {
        ArrayList<Integer> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    } 
    
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public synchronized static void try_incr_AL_chashmap(ConcurrentHashMap<Long, ArrayList<Node>> hashmap, Long key, Node value) {
        ArrayList<Node> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    } 
    
    //Array
    //String
    /**
     * 
     * @param hashmap
     * @param key
     * @param value
     * @param position 
     */
    public static void incr_array_hashmap(HashMap<String, int[]> hashmap, String key, int value, int position) {
        int[] value_array = hashmap.get(key);
        value_array[position] += value;
        hashmap.put(key, value_array);
    }
    
    //Integer
    /**
     * 
     * @param hashmap
     * @param key
     * @param value
     * @param position 
     */
    public static void incr_array_hashmap(HashMap<Integer, int[] > hashmap, int key, int value, int position) {
        int[] value_array = hashmap.get(key);
        value_array[position] += value;
        hashmap.put(key, value_array);
    }

    /**
     *
     * @param hashmap
     * @param key
     * @param value
     * @param position
     * @param array_size
     */
    public static void try_incr_array_hashmap(HashMap<String, int[] > hashmap, String key, int value, int position,
                                              int array_size) {
        int[] value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new int[array_size];
        }
        value_array[position] += value;
        hashmap.put(key, value_array);
    }
    //Hashset
    //hashset String
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_hashset_hashmap(HashMap<String, HashSet<String>> hashmap, String key, String value) {
        HashSet<String> value_set = hashmap.get(key);
        if (value_set == null) {
            value_set = new HashSet<>();
        }
        value_set.add(value);
        hashmap.put(key, value_set);
    }
  
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_hashset_hashmap(HashMap<String, HashSet<Node>> hashmap, String key, Node value) {
        HashSet<Node> value_set = hashmap.get(key);
        if (value_set == null) {
            value_set = new HashSet<>();
        }
        value_set.add(value);
        hashmap.put(key, value_set);
    }
   
    //Integer
    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_hashset_hashmap(HashMap<Integer, HashSet<String>> hashmap, int key, String value) {
        HashSet<String> value_set = hashmap.get(key);
        if (value_set == null) {
            value_set = new HashSet<>();
        }
        value_set.add(value);
        hashmap.put(key, value_set);
    }

    /**
     * 
     * @param hashmap
     * @param key
     * @param value 
     */
    public static void try_incr_treeset_hashmap(HashMap<Integer, TreeSet<Double>> hashmap, int key, Double value) {
        TreeSet<Double> value_set = hashmap.get(key);
        if (value_set == null) {
            value_set = new TreeSet<>();
        }
        value_set.add(value);
        hashmap.put(key, value_set);
    } 
    
    //Average median standard deviation functions
    /**
     * Return the AVERAGE value of an array
     * @param array
     * @return 
     */
    public static double get_average_from_array(int[] array) {
        double total_value = 0;
        for (double value : array) {
            total_value += value;
        }
        return total_value / array.length;
    }
    
    /**
     * Return the AVERAGE value of a list
     * @param list
     * @return 
     */
    public static double get_average_from_AL_int(ArrayList<Integer> list) {
        double total_value = 0;
        for (int value : list) {
            if (Double.isNaN(value)) {
                total_value += 0;
            } else {
                total_value += value;
            }
        }
        return total_value / list.size();
    }
    
    /**
     * Return the AVERAGE value of a list
     * @param list
     * @return 
     */
    public static double get_average_from_AL_double(ArrayList<Double> list) {
        double total_value = 0;
        for (double value : list) {
            if (Double.isNaN(value)) {
                total_value += 0;
            } else {
                total_value += value;
            }
        }
        return total_value / list.size();
    }
    
    /**
     * Return the AVERAGE value of a list
     * @param list
     * @return 
     */
    public static double get_average_from_AL_long(ArrayList<Long> list) {
        double total_value = 0;
        for (long value : list) {
            if (Double.isNaN(value)) {
                total_value += 0;
            } else {
                total_value += value;
            }
        }
        return total_value / list.size();
    }
   
    /**
     * Return the MEDIAN value of a list
     * @param list values in list must be sorted 
     * @return 
     */
    public static double get_median_from_AL_double(ArrayList<Double> list) {
        double median;
        if (list.size() == 1) {
            median = list.get(0);
        } else if (list.size() == 2) {
            median = (list.get(0) + list.get(1))/2;
        } else if (list.size() % 2 == 0) { // list has even length 
            int length = list.size()/2; 
            median = (list.get(length) + list.get(length+1))/2;
        } else { // uneven length 
            int length = (list.size()+1)/2;
            median = list.get(length);
        }       
        return median;
    }
    
    /**
     * Return the MEDIAN value of a list
     * @param list values in list must be sorted 
     * @return 
     */
    public static long get_median_from_AL_long(ArrayList<Long> list) {
        long median;
        if (list.size() == 1) {
            median = list.get(0);
        } else if (list.size() == 2) {
            median = (list.get(0) + list.get(1))/2;
        } else if (list.size() % 2 == 0) { // list has even length 
            int length = list.size()/2;
            median = (list.get(length) + list.get(length+1))/2;
        } else { // uneven length 
            int length = (list.size()+1)/2;
            median = list.get(length);
        }
        return median;
    }
    
    /**
     * Returns the MEDIAN value of a list
     * @param list values in list must be sorted 
     * @return 
     */
    public static double get_median_from_AL_int(ArrayList<Integer> list) {
        int median;
        if (list.size() == 1) {
            median = list.get(0);
        } else if (list.size() == 2) {
            median = (list.get(0) + list.get(1))/2;
        } else if (list.size() % 2 == 0) { // list has even length 
            int length = list.size()/2;
            median = (list.get(length) + list.get(length+1))/2;
        } else { // uneven length 
            int length = (list.size()+1)/2;
            median = list.get(length);
        }       
        return median;
    }
    
    /**
     * 
     * @param value1 
     * @param value2
     * @return float 
     */
    public static Double percentage(int value1, int value2) {
        double percentage = ((double) value1 / (double) value2) * 100;
        if (Double.isNaN(percentage)) {
            percentage = 0;
        }
        return percentage;
    }
   
    /**
     * 
     * @param value1
     * @param value2
     * @return 
     */
    public static double divide(int value1, int value2) {
        double division = (double) value1 / (double) value2;
        if (Double.isNaN(division)) {
            division = 0;
        }
        return division;
    } 
    
    /**
     * 
     * @param value1
     * @param value2
     * @return 
     */
    public static double divide(long value1, long value2) {
        double division = (double) value1 / (double) value2;
        if (Double.isNaN(division)) {
            division = 0;
        }
        return division;
    }
     
    /**
    * Return array with [median, average, standard deviation, shortest, longest, total, count] 
    * @param list
    * @return
    */
    public static double[] median_average_stdev_from_AL_int(ArrayList<Integer> list) {
        if (list.isEmpty()) {
            return new double []{0, 0, 0, 0, 0, 0, 0};
        }
        Collections.sort(list);
        double median = get_median_from_AL_int(list);
        double average = get_average_from_AL_int(list);
        double deviation = get_stdev_from_AL_int(list, average); 
        double total = 0;
        for (int value : list) {
            total += value;
        }
        return new double []{median, average, deviation, list.get(0), list.get(list.size()-1), total, list.size()};
    }
    
    /**
    * Return array with [median, average, standard deviation, shortest, longest, total, count] 
    * @param list
    * @return
    */
    public static double[] median_average_stdev_from_AL_double(ArrayList<Double> list) {
        if (list.isEmpty()) {
            return new double []{0, 0, 0, 0, 0, 0, 0};
        }
        Collections.sort(list);
        double median = get_median_from_AL_double(list);
        double average = get_average_from_AL_double(list);
        double deviation = get_stdev_from_AL_double(list, average); 
        double total = 0;
        for (double value : list) {
            total += value;
        }
        return new double []{median, average, deviation, list.get(0), list.get(list.size()-1), total, list.size()};
    }
    
    /**
    *Return array with [median, average, standard deviation, shortest, longest, total, count] 
    * @param list
    * @return
    */
    public static double[] median_average_stdev_from_AL_long(ArrayList<Long> list) {
        if (list.isEmpty()) {
            return new double []{0, 0, 0, 0, 0, 0, 0};
        }
        Collections.sort(list);
        double median = get_median_from_AL_long(list);
        double average = get_average_from_AL_long(list);
        double deviation = get_stdev_from_AL_long(list, average);
        long total = 0;
        for (long value : list) {
            total += value;
        }
        return new double []{median, average, deviation, list.get(0), list.get(list.size()-1), total, list.size()};
    }
    
    /**
    * Return array with [median, average, standard deviation, shortest, longest, total, count] 
    * @param array
    * @return
    */
    public static double[] median_average_stdev_from_array(int[] array) {
        if (array.length == 0 || (array.length == 1 && array[0] == 0)) {
            return new double []{0, 0, 0, 0, 0, 0, 0};
        }
        ArrayList<Integer> list = new ArrayList<>();
        for (int value : array) {
            list.add(value);
        }
        double[] median_average_deviation = median_average_stdev_from_AL_int(list);
        return median_average_deviation;
    }
    
    /**
     * 
     * @param list
     * @param average
     * @return 
     */
    public static double get_stdev_from_AL_long(ArrayList<Long> list, double average) {
        double sum = 0;  
	    for (int i=0; i < list.size(); i++) {
            sum += Math.pow(list.get(i)-average,2);
	    }
        double deviation = Math.sqrt(sum/(list.size()));
        return deviation;
    }
    
    /**
     * 
     * @param list
     * @param average
     * @return 
     */
    public static double get_stdev_from_AL_double(ArrayList<Double> list, double average) {
        double sum = 0;  
	for(int i=0; i < list.size(); i++) {
            sum += Math.pow(list.get(i)-average,2);
	}
        double deviation = Math.sqrt(sum/(list.size()));
        return deviation;
    }
    
    /**
     * 
     * @param list
     * @param average
     * @return 
     */
    public static double get_stdev_from_AL_int(ArrayList<Integer> list, double average) {
        double sum = 0;  
	for(int i=0; i < list.size(); i++) {
            sum += Math.pow(list.get(i)-average,2);
	}
        double deviation = Math.sqrt(sum/(list.size()));
        return deviation;
    }
    
    /**
     * 
     * @param array
     * @return 
     */
    public static long sum_of_array(int[] array) {
        long total = 0;
        for (long number : array) {
            total += number;
        }
        return total;
    }
    
    /**
     * 
     * @param list
     * @return 
     */
    public static ArrayList<Integer> remove_duplicates_from_AL_int(ArrayList<Integer> list) {
        List<Integer> newList = list.stream() 
            .distinct() 
            .collect(Collectors.toList()); 
        Collections.sort(newList);
        ArrayList tester = new ArrayList<>(newList);
        return tester;
    }
}
