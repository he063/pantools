/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.wur.bif.pantools.pangenome;

import nl.wur.bif.pantools.pantools.Pantools;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import static nl.wur.bif.pantools.utils.Globals.*;


/**
 *
 * @author Eef Jonkheer, Bioinformatics group, Wageningen University, the Netherlands
 */
public class create_skip_arrays {
    
    /**
     * --reference/-ref, --skip. allowed input is by using commas and hyphens. Example 1,2,3,4-10.
     * needs a test to see if it is over 1 and below the total nr of genomes +1
     * @param ignore_ref_argument
     * @param print
     */
    public static void create_skip_arrays(boolean ignore_ref_argument, boolean print) {
        skip_list = new ArrayList<>();
        skip_seq_list = new ArrayList<>();
        skip_array = new boolean[total_genomes];
        skip_seq_array = new boolean[total_genomes][0];
        
        for (int i = 1; i <= total_genomes; i++) {
            skip_array[i-1] = false;
        }
        
        int genome_counter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> genome_nodes = GRAPH_DB.findNodes(GENOME_LABEL);
            while (genome_nodes.hasNext()) {
                genome_counter ++;
                if (genome_counter % 1000 == 0 && genome_counter > 1){
                    System.out.print("\rGathering genomes: " + genome_counter);
                }
                Node genome_node = genome_nodes.next();     
                int genome_nr = (int) genome_node.getProperty("number");
                int num_sequences = (int) genome_node.getProperty("num_sequences");
                skip_seq_array[genome_nr-1] = new boolean[num_sequences];
                for (int j = 1; j <= num_sequences; j++) {
                    skip_seq_array[genome_nr-1][j-1] = false;         
                }
            }
            tx.success();
        } 
        System.out.print("\r                                              "); 
        boolean file_included = false;
        if (skip_genomes != null) {
            file_included = check_if_file_exists(skip_genomes);
        } 
        
        if (skip_genomes != null && file_included) { // a file was included with the --skip argument 
            read_skip_file();
        } else if (target_genome != null && !ignore_ref_argument) { // --reference was provided
            target_genome = target_genome.replace(" ","");
            if (target_genome.endsWith(",")) {
                target_genome = target_genome.replaceFirst(".$",""); // remove last character
            }

            String[] temp_target_array = target_genome.split(",");
            ArrayList<Integer> target_list = new ArrayList<>();
            for (String genome_str : temp_target_array) {
                if (genome_str.contains("-")) {
                    String[] genome_array = genome_str.split("-");
                    int start = Integer.parseInt(genome_array[0]);
                    int end = Integer.parseInt(genome_array[1]);
                    for (int genome_nr = start; genome_nr <= end; genome_nr++) {
                        if (genome_nr > 0 && genome_nr <= total_genomes) {
                            target_list.add(genome_nr);
                        }
                    }
                } else {
                    int genome_nr = Integer.parseInt(genome_str);
                    if (genome_nr > 0 && genome_nr <= total_genomes) {
                        target_list.add(genome_nr);
                    }
                }
            }

            if (target_list.size() < 101 && print) {
                System.out.print("\rSelected " + target_list.size() + " genome(s) " + target_genome + "\n");
            } else if (print) {
                System.out.print("\rSelected " + target_list.size() + " genome(s)\n");
            }
            for (int i=1; i<= total_genomes; i++) {
                if (target_list.contains(i)) {
                    continue;
                }
                skip_list.add(i);
                skip_array[i-1] = true;
            }
        } else if (skip_genomes != null) { // --skip argument was provided
            skip_genomes = skip_genomes.replace(",,", ",");
            if (skip_genomes.endsWith(",")) {
                skip_genomes = skip_genomes.replaceFirst(".$",""); // remove last character
            }
            String[] temp_skip_array = skip_genomes.split(",");
            for (String genome_str : temp_skip_array) {
                if (genome_str.contains("-")) {
                    String[] genome_array = genome_str.split("-");
                    int start = Integer.parseInt(genome_array[0]);
                    int end = Integer.parseInt(genome_array[1]);
                    for (int genome_nr = start; genome_nr <= end; genome_nr ++) {
                        if (genome_nr > 0 && genome_nr <= total_genomes) {
                            skip_list.add(genome_nr);
                            skip_array[genome_nr-1] = true;
                        }
                    }
                } else {
                    int genome_nr = Integer.parseInt(genome_str);
                    if (genome_nr > 0 && genome_nr <= total_genomes) {
                        skip_list.add(genome_nr);
                        skip_array[genome_nr-1] = true;
                    }
                }
            }
        }

        adj_total_genomes = total_genomes - skip_list.size();
        if (skip_list.size() == 1) {
            System.out.println("\rSkipping genome " + skip_list.toString().replace(" ","").replace("[","").replace("]",""));
        } else if (skip_list.size() > 0 && skip_list.size() < 50 && print) {
            System.out.println("\rSkipping " + skip_list.size() + " genome(s) " + skip_list.toString().replace(" ","").replace("[","").replace("]",""));
        } else if (skip_list.size() >= 50 && print) {
           System.out.println("\rSelected " + adj_total_genomes + " genomes, skipping " + skip_list.size() + " genomes");
        }
        if (adj_total_genomes == 0) {
            Pantools.logger.error("No genomes are in the current selection.");
            System.exit(1);
        }

    }
    
    /**
     * Returns the size of a file
     * @param path
     * @return
     */
    public static boolean check_if_file_exists(String path) {
        File file = new File(path);
        boolean exists = file.exists();
        if (exists) { // if the file exists, check if anything is in there
            if (file.length() < 1) {
                exists = false;
            }
        }
        return exists;
    }

    /**
     * (currently) Assumes there is 1 annotation per genome
     * 
     * #MINIMUM_NUMBER_GENES = 100
     * #SEQUENCE_MINIMUM_NUMBER_GENES = 100
     * #GENOMES_WITH_MCSCANX_ID
     * #FIRST_X_SEQUENCES = 999   (take the first 999 sequences per genome)
     * #LARGEST_X_SEQUENCES = 999 (the the largest 999 sequences per genome)
   
     * #SKIP_SEQUENCE_IDENTIFIER = 
     * #SELECT_SEQUENCE_IDENTIFIER = 

     * 
     */
    public static void read_skip_file() {    
        try (BufferedReader in = new BufferedReader(new FileReader(skip_genomes))) {
            while (in.ready()) {         
                String line = in.readLine().trim();
                if (line.startsWith("#GENOME_MINIMUM_NUMBER_GENES =") && !line.equals("#GENOME_MINIMUM_NUMBER_GENES =")) {
                    skip_genomes_based_on_gene_number(line.split("="));            
                } else if (line.startsWith("#SEQUENCE_MINIMUM_NUMBER_GENES =") && !line.equals("#SEQUENCE_MINIMUM_NUMBER_GENES =")) {
                    skip_sequences_based_on_gene_number(line.split("="));                  
                } else if (line.startsWith("#GENOMES_WITH_MCSCANX_ID =" ) && !line.equals("#GENOMES_WITH_MCSCANX_ID =")) {
                    skip_genomes_based_on_having_mcscanx_id();
                } else if (line.startsWith("#FIRST_X_SEQUENCES =") && !line.equals("#FIRST_X_SEQUENCES =")) {
                    Pantools.logger.error("working on this 1.");
                    //skip_genomes_based_on_seq_nr();
                    System.exit(1);
                } else if (line.startsWith("#LARGEST_X_SEQUENCES =") && !line.equals("#LARGEST_X_SEQUENCES =")) {
                    Pantools.logger.error("working on this 2.");
                    System.exit(1);
                } else if (line.startsWith("#SELECT_SEQUENCE_IDENTIFIER =") && !line.equals("#SELECT_SEQUENCE_IDENTIFIER =")) {
                    select_genomes_sequences_based_on_identifiers(line);
                } else if (line.startsWith("#SKIP_SEQUENCE_IDENTIFIER =") && !line.equals("#SKIP_SEQUENCE_IDENTIFIER =")) {
                    skip_genomes_sequences_based_on_identifiers(line);
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", skip_genomes);
            System.exit(1);
        }
        if (skip_list.isEmpty() && skip_seq_list.isEmpty()) {
            Pantools.logger.error("None of the argument in the skip file were recognized.");
            System.exit(1);
        }
        Pantools.logger.trace("skip list {}.", skip_list);
    }
    
    public static void skip_genomes_sequences_based_on_identifiers(String line) {
        String[] line_array = line.split("=");
        List<String> selected_sequences = Arrays.asList(line_array[1].replace(" ","").split(","));
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (int i = 1; i <= total_genomes; i++) {
                Node genome_node = GRAPH_DB.findNode(GENOME_LABEL, "number", i);
                int num_sequences = (int) genome_node.getProperty("num_sequences");
                skip_seq_array[i-1] = new boolean[num_sequences];
                for (int j = 1; j <= num_sequences; j++) {
                    if (selected_sequences.contains(i + "_" + j)) { 
                        skip_seq_array[i-1][j-1] = true; 
                        skip_seq_list.add(i + "_" + j);
                    } else {
                        skip_seq_array[i-1][j-1] = false; // do not skip
                    }              
                }
            }
            tx.success();
        } 
        update_skip_genomes_based_on_seqs();  
    }
    
    /**
     * If all sequences of a genome are skipped, skip the genome as well
     */
    public static void update_skip_genomes_based_on_seqs() {
        for (int i = 0; i < skip_seq_array.length; i++) {  
            boolean any_seq_not_skipped = false; 
            for (int j = 0; j < skip_seq_array[i].length; j++) {
                if (!skip_seq_array[i][j]) {
                    any_seq_not_skipped = true;
                }
            }
            if (!any_seq_not_skipped) {
                skip_list.add(i+1);
                skip_array[i] = true;
            }
        }
    }
    
    public static void select_genomes_sequences_based_on_identifiers(String line) {
        String[] line_array = line.split("=");
        List<String> selected_sequences = Arrays.asList(line_array[1].replace(" ","").split(","));
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (int i = 1; i <= total_genomes; i++) {
                Node genome_node = GRAPH_DB.findNode(GENOME_LABEL, "number", i);
                int num_sequences = (int) genome_node.getProperty("num_sequences");
                skip_seq_array[i-1] = new boolean[num_sequences];
                for (int j = 1; j <= num_sequences; j++) {
                    if (selected_sequences.contains(i + "_" + j)) { 
                        skip_seq_array[i-1][j-1] = false; // do not skip
                    } else {
                        skip_seq_array[i-1][j-1] = true;    
                        skip_seq_list.add(i + "_" + j);
                    }              
                }
            }
            tx.success();
        } 
        update_skip_genomes_based_on_seqs();
    }
            
    public static void skip_genomes_based_on_having_mcscanx_id() {
        ResourceIterator<Node> genome_nodes = GRAPH_DB.findNodes(GENOME_LABEL);
        int skip_counter = 0;
        while (genome_nodes.hasNext()) {
            Node genome_node = genome_nodes.next();
            int genome = (int) genome_node.getProperty("number");
            System.out.print("\rGathering genomes: genome " + genome + "  ");
            if (!genome_node.hasProperty("MCScanX_ID")) {
                skip_list.add(genome);
                skip_counter ++;
                skip_array[genome-1] = true;
            }  
        }
        System.out.println("\r" + skip_counter + " genomes were skipped because they didn't have a MCScanX identifier");
    }
    
    public static void skip_genomes_based_on_gene_number(String[] line_array) {
        HashSet<Integer>  present_annotations = new HashSet<>();
        int minimum_number = Integer.parseInt(line_array[1].replace(" ",""));
        ResourceIterator<Node> annotation_nodes = GRAPH_DB.findNodes(ANNOTATION_LABEL); // currently assumes there is 1 annotation per genome
        int skip_counter = 0, annotation_node_counter = 0;
        while (annotation_nodes.hasNext()) {
            Node anno_node = annotation_nodes.next();
            int genome = (int) anno_node.getProperty("genome");
            System.out.print("\rGathering genomes: genome " + genome + "  ");
            int num_genes = (int) anno_node.getProperty("total_genes");
            present_annotations.add(genome);
            if (num_genes < minimum_number) {
                skip_list.add(genome);
                skip_counter ++;
                skip_array[genome-1] = true;
            }
            annotation_node_counter ++;
        }
        
        if (annotation_node_counter == 0) {
            Pantools.logger.error("This skip option is not available as no genes are annotated yet in the pangenome.");
            System.exit(1);
        }
        
        // genomes that don't have an annotation node are not skipped yet
        for (int i=1; i <= total_genomes; i++) {
            if (!present_annotations.contains(i)) {
                skip_list.add(i);
                skip_counter ++;
                skip_array[i-1] = true;
            }
        }
        System.out.println("\r" + skip_counter + " genomes were skipped based on a minimum number of " + minimum_number + " genes per genome");
    }
            
    /**
     * 
     * @param line_array 
     */
    public static void skip_sequences_based_on_gene_number(String[] line_array) {
        HashSet<Integer> present_annotations = new HashSet<>();
        int minimum_number = Integer.parseInt(line_array[1].replace(" ",""));
        ResourceIterator<Node> annotation_nodes = GRAPH_DB.findNodes(ANNOTATION_LABEL); // currently assumes there is 1 annotation per genome
        System.out.print("\rPreparing skipped sequences");
        int skip_counter = 0, annotation_node_counter = 0;
        while (annotation_nodes.hasNext()) {
            Node anno_node = annotation_nodes.next();
            int genome = (int) anno_node.getProperty("genome");
            int num_genes = (int) anno_node.getProperty("total_genes");
            present_annotations.add(genome);
            if (num_genes < minimum_number) {
                skip_list.add(genome);
                skip_counter ++;
                skip_array[genome-1] = true;
            }
            annotation_node_counter ++;
        }
        if (annotation_node_counter == 0) {
            Pantools.logger.error("This skip option is not available as no genes are annotated yet in the pangenome.");
            System.exit(1);
        }
        
        // genomes that don't have an annotation node are not skipped yet
        for (int i=1; i <= total_genomes; i++) {
            if (!present_annotations.contains(i)) {
                skip_list.add(i);
                skip_counter ++;
                skip_array[i-1] = true;
            }
        }
        HashMap<String, Integer> gene_count_per_sequence = new HashMap<>();
        ResourceIterator<Node> gene_nodes = GRAPH_DB.findNodes(GENE_LABEL);
        while (gene_nodes.hasNext()) {
            Node gene_node = gene_nodes.next();
            int[] address = (int[]) gene_node.getProperty("address");
            int number_of_genes = 0;
            if (gene_count_per_sequence.containsKey(address[0] + "_" + address[1])) {
                 number_of_genes = gene_count_per_sequence.get(address[0] + "_" + address[1]);
            }
            number_of_genes ++;
            gene_count_per_sequence.put(address[0] + "_" + address[1], number_of_genes); 
        }
        
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (int i = 1; i <= total_genomes; i++) {
                Node genome_node = GRAPH_DB.findNode(GENOME_LABEL, "number", i);
                int num_sequences = (int) genome_node.getProperty("num_sequences");
                skip_seq_array[i-1] = new boolean[num_sequences];
                for (int j = 1; j <= num_sequences; j++) {
                    int gene_count = 0;
                    if (gene_count_per_sequence.containsKey(i + "_" + j)) {                      
                        gene_count = gene_count_per_sequence.get(i + "_" + j);
                    }
                    if (gene_count < minimum_number) {
                        skip_seq_array[i-1][j-1] = true;
                        skip_counter ++;
                        skip_seq_list.add(i + "_" + j);
                    }  
                }
            }
            tx.success();
        } 
        System.out.println("\r" + skip_counter + " sequences were skipped based on a minimum number of " + minimum_number + " genes per sequence");
    }
}
