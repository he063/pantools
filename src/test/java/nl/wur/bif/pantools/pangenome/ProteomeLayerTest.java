package nl.wur.bif.pantools.pangenome;

import nl.wur.bif.pantools.pantools.Pantools;
import nl.wur.bif.pantools.utils.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.registerShutdownHook;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for all functions within the ProteomeLayer class.
 *
 * @author Robin van Esch, Bioinformatics group, Wageningen University, the Netherlands
 */

public class ProteomeLayerTest {

    private static ProteomeLayer proteomeLayer;

    @BeforeAll
    static void disableLogging() {
        Pantools.logger = LogManager.getLogger(Pantools.class);
        System.setProperty("log4j2.rootLevel", "off");
    }
    @BeforeEach
    void createProteomeLayer() {
        proteomeLayer = new ProteomeLayer();
    }

    @Test
    void checkWhichRnToUseTest() {
        assertAll(
                // test for null input
                () -> assertEquals(new ArrayList<>(), proteomeLayer.check_which_rn_to_use(false, null)),

                // test for incorrect separators.
                () -> ExitAssertions.assertExits(1, () -> proteomeLayer.check_which_rn_to_use(false, "95-55-25")),

                // test for correct input
                () -> assertEquals(Arrays.asList("85", "75", "65", "45", "35"),
                        proteomeLayer.check_which_rn_to_use(false, "95,55,25"))
        );
    }

    @Nested
    @DisplayName("Tests for CountBuscoTrueFalsePosNegatives")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class CountBuscoTrueFalsePosNegativesTests {

        final StringBuilder buscoLogBuilder = new StringBuilder();
        final HashMap<String, Integer> hmGroupSizeMap = new HashMap<>();
        HashMap<String, Integer> hmCountMap;
        int[] statistics;

        @BeforeAll
        void setAdjTotalGenomes() {
            adj_total_genomes = 10;
            hmGroupSizeMap.put("hm1", 10);
            hmGroupSizeMap.put("hm2", 9);
            hmGroupSizeMap.put("hm3", 15);
        }

        @BeforeEach
        void createHmCountMap() {
            statistics = new int[5];
            hmCountMap = new HashMap<>();
        }

        // test for perfect BUSCO group
        @Test
        void CountBuscoPerfectTest() {
            hmCountMap.put("hm1", 10);
            int[] expected = new int[]{1, 10, 0, 0, 0};
            proteomeLayer.count_busco_true_false_pos_negatives(hmCountMap, statistics, buscoLogBuilder, hmGroupSizeMap);
            assertArrayEquals(expected, statistics);
        }

        // test for imperfect BUSCO group
        @Test
        void CountBuscoImperfectTest() {
            hmCountMap.put("hm1", 10);
            hmCountMap.put("hm2", 3);
            hmCountMap.put("hm3", 13);
            int[] expected = new int[]{0, 13, 13, 2, 0};
            proteomeLayer.count_busco_true_false_pos_negatives(hmCountMap, statistics, buscoLogBuilder, hmGroupSizeMap);
            assertArrayEquals(expected, statistics);
        }

        // test for perfect null group
        @Test
        void CountBuscoNullTest() {
            hmCountMap.put("hm1", 10);
            hmCountMap.put(null, 3);
            hmCountMap.put("hm3", 13);
            int[] expected = new int[]{0, 13, 10, 2, 1};
            proteomeLayer.count_busco_true_false_pos_negatives(hmCountMap, statistics, buscoLogBuilder, hmGroupSizeMap);
            assertArrayEquals(expected, statistics);
        }

        // test for impossible data
        @Test
        void CountBuscoImpossibleTest() {
            hmCountMap.put("hm1", 11);
            hmCountMap.put("hm2", 10);
            hmCountMap.put("hm3", 16);
            int[] expected = new int[]{0, 16, 21, -1, 0}; // TODO: should expect exception || impossible scenario
            proteomeLayer.count_busco_true_false_pos_negatives(hmCountMap, statistics, buscoLogBuilder, hmGroupSizeMap);
            assertArrayEquals(expected, statistics);
        }

    }

    @Test
    void countDifferentGroupsForBuscoTest() {
        HashMap<String, Integer> hmCountMap;
        final StringBuilder buscoLogBuilder = new StringBuilder();

        final ArrayList<String> foundMrnas = new ArrayList<> (
                Arrays.asList("mrna1", "mrna2", "mrna3", "mrna4", "mrna5", "mrna6")
        );

        final HashMap<String, String> mrnaHmgroupMap = new HashMap<String, String>() {{
            put("mrna1", "hm1");
            put("mrna2", "hm2");
            put("mrna3", "hm2");
            put("mrna4", "hm3");
            put("mrna5", "hm3");
            put("mrna6", "hm3");
        }};

        hmCountMap = proteomeLayer.count_different_groups_for_busco(foundMrnas, mrnaHmgroupMap, buscoLogBuilder);

        assertAll(
                () -> assertEquals(1, hmCountMap.get("hm1")),
                () -> assertEquals(2, hmCountMap.get("hm2")),
                () -> assertEquals(3, hmCountMap.get("hm3"))
        );
    }

    @Test
    void findBuscosSharedByAllGenomesTest() {
        adj_total_genomes = 2;
        final HashMap<String, ArrayList<String>> buscoMrnaMap = new HashMap<String, ArrayList<String>>() {{
            put("busco1", new ArrayList<>(Arrays.asList("mrna1", "mrna2")));
            put("busco2", new ArrayList<>(Arrays.asList("mrna3", "mrna4")));
            put("busco3", new ArrayList<>(Collections.singletonList("mrna5")));
        }};
        final StringBuilder buscoLogBuilder = new StringBuilder();
        proteomeLayer.find_buscos_shared_by_all_genomes(buscoMrnaMap, buscoLogBuilder, "busco_path");
        final String[] lines = buscoLogBuilder.toString().split("\\n");
        assertAll(
                () -> assertEquals("1(1), 2(2)", lines[0].substring(95,105)),
                () -> assertEquals("2/3", lines[2].substring(6,9)),
                () -> assertEquals("4", lines[3].substring(lines[3].length()-1)),
                () -> assertEquals("busco1, 2", lines[6]),
                () -> assertEquals("busco3, 1", lines[7]),
                () -> assertEquals("busco2, 2", lines[8])
        );
    }

    @Test
    void updateSkipArrayBasedOnAnnoIdsTest() {
        annotation_identifiers = new ArrayList<>(
                Arrays.asList("1_1", "2_1", "3_0", "4_0")
        );
        adj_total_genomes = 3;
        skip_list = new ArrayList<>();
        final ArrayList<Integer> expectedSkipList = new ArrayList<>(
                Collections.singletonList(3)
        );

        skip_array = new boolean[4];
        Arrays.fill(skip_array, Boolean.FALSE);
        skip_array[3] = true;

        boolean[] expected = new boolean[4];
        Arrays.fill(expected, Boolean.FALSE);
        expected[3] = true;
        expected[2] = true;

        proteomeLayer.update_skip_array_based_on_anno_ids(); // TODO: refactor function to make it less obscure

        assertAll(
                () -> assertEquals(2, adj_total_genomes),
                () -> assertEquals(expectedSkipList, skip_list),
                () -> assertArrayEquals(expected, skip_array)
        );
    }

    @Nested
    @DisplayName("Tests for methods using Neo4j graph databases")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class Neo4jTests {
        private Transaction tx;
        private Node pangenomeNode;

        @BeforeAll
        void createGRAPH_DB() throws IOException {
            final Path tempDirectory = Files.createTempDirectory(RandomStringUtils.randomAlphanumeric(10));
            FileUtils.addShutDownHook(tempDirectory);
            final Path graphDB = Files.createDirectory(tempDirectory.resolve("graph.db"));
            GRAPH_DB = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(graphDB.toFile()).newGraphDatabase();
            registerShutdownHook(GRAPH_DB);
            PATH_TO_THE_PANGENOME_DATABASE = tempDirectory.toString();
            WORKING_DIRECTORY = tempDirectory.toString();
            GRAPH_DATABASE_PATH = "";
        }

        @BeforeEach
        void startTransaction() {
            tx = GRAPH_DB.beginTx();
            pangenomeNode = createPangenomeNode();
        }

        @Test
        void getProteinSequenceTest() {
            String seq1 = "NLYIQWLKDGGPSSGRPPPS"; // 1L2Y
            try {
                Node mrnaNode = GRAPH_DB.createNode(MRNA_LABEL);
                mrnaNode.setProperty("protein_sequence", seq1);
                String seq2 = proteomeLayer.get_protein_sequence(mrnaNode);
                mrnaNode.setProperty("protein", seq1);
                String seq3 = proteomeLayer.get_protein_sequence(mrnaNode);
                assertAll(
                        () -> assertEquals(seq1, seq2),
                        () -> assertEquals(seq1, seq3)
                );
            } catch(NotFoundException nfe) {
                fail("Unable to start database");
            }
        }

        public ArrayList<ShowAvailableGroupingParameters> showAvailableGroupingData() {
            ArrayList<ShowAvailableGroupingParameters> data = new ArrayList<>();

            // test for multiple grouping versions
            String expected1 = "Grouping 1: grouping_parametersGrouping 2: grouping_parameters";
            data.add(new ShowAvailableGroupingParameters(1, false, new int[]{1, 2}, expected1));

            // test to check active grouping: active grouping
            String expected2 = "Active grouping version: 1";
            data.add(new ShowAvailableGroupingParameters(1, true, new int[]{}, expected2));

            // test to check active grouping: no active grouping
            String expected3 = "There is no grouping active";
            data.add(new ShowAvailableGroupingParameters(0, true, new int[]{}, expected3));

            return data;
        }

        @ParameterizedTest
        @CsvSource({"-1,1,'1'", "-2,0,''", "3,2,'123'"})
        void rmGroupingVFromPangenomeNodeTest(int version, int highest, String expected) {
            grouping_version = version;
            pangenomeNode.setProperty("grouping_v1", "grouping_parameters");
            pangenomeNode.setProperty("grouping_v2", "grouping_parameters");
            pangenomeNode.setProperty("grouping_v3", "grouping_parameters");
            pangenomeNode.setProperty("highest_grouping", 3);
            proteomeLayer.setPangenomeNode(pangenomeNode);
            Node hmNode = GRAPH_DB.createNode(HOMOLOGY_GROUP_LABEL);
            hmNode.setProperty("group_version", 1);
            proteomeLayer.rm_grouping_v_from_pangenome_node(true);
            pangenomeNode = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            if (!pangenomeNode.hasProperty("highest_grouping")) {
                pangenomeNode.setProperty("highest_grouping", 0);
            }
            String groupings = pangenomeNode.getPropertyKeys().toString().replaceAll("[^0-9]","");
            assertAll(
                    () -> assertEquals(highest, pangenomeNode.getProperty("highest_grouping")),
                    () -> assertEquals(expected, groupings)
            );
        }

        @ParameterizedTest
        @CsvSource({"true,true","false,true","false,false"})
        void wasTheLastGroupingSuccessfulTest(boolean expected, boolean exists) {
            if (exists) {
                pangenomeNode.setProperty("last_grouping_successful", expected);
            }
            proteomeLayer.setPangenomeNode(pangenomeNode);
            boolean actual = proteomeLayer.was_the_last_grouping_successful();
            assertEquals(expected, actual);
        }

        public ArrayList<RetrieveNodeParameters> retrieveNodeData() {
            ArrayList<RetrieveNodeParameters> data = new ArrayList<>();

            // no grouping selected or active
            data.add(new RetrieveNodeParameters(null, -1, 0, 0, true, false, false));

            // no grouping active
            data.add(new RetrieveNodeParameters(null, 1, 1, 1, false, true, false));

            // remove ALL inactive groups
            data.add(new RetrieveNodeParameters("ALL_INACTIVE", 0, -1, 1, false, false, true));

            // remove both active and inactive groups
            data.add(new RetrieveNodeParameters("ALL", 0, -2, 2, false, true, true));

            // a specific grouping version was given by the user; only active nodes
            data.add(new RetrieveNodeParameters("1", 1, 1, 1, false, true, false));

            // a specific grouping version was given by the user; only inactive nodes
            data.add(new RetrieveNodeParameters("1", 1, 1, 1, false, false, true));

            // a specific grouping version was given by the user; no nodes
            data.add(new RetrieveNodeParameters("1", 1, 1, 1, true, false, false));

            // a specific grouping version was given by the user; active and inactive nodes
            data.add(new RetrieveNodeParameters("1", 1, 1, 1, true, true, true));

            return data;
        }

        @ParameterizedTest
        @MethodSource(value = "retrieveNodeData")
        void retrieveHmgroupNodesForSelectedGroupingTest(RetrieveNodeParameters data) {
            GROUPING_VERSION = data.version;
            grouping_version = data.groupingVersion;
            pangenomeNode.setProperty("last_grouping_successful", true);
            pangenomeNode.setProperty("grouping_v1", "grouping_parameters");
            proteomeLayer.setPangenomeNode(pangenomeNode);
            if (data.active) {
                Node activeNode = GRAPH_DB.createNode(HOMOLOGY_GROUP_LABEL);
                activeNode.setProperty("group_version", data.groupingVersion);
            }
            if (data.inactive) {
                Node inactiveNode = GRAPH_DB.createNode(INACTIVE_HOMOLOGY_GROUP_LABEL);
                inactiveNode.setProperty("group_version", data.groupingVersion);
            }
            if (data.expectExit) {
                ExitAssertions.assertExits(1,
                        () -> proteomeLayer.retrieve_hmgroup_nodes_for_selected_grouping());
            } else {
                ArrayList<Node> nodes = proteomeLayer.retrieve_hmgroup_nodes_for_selected_grouping();
                assertAll(
                        () -> assertEquals(data.expectedVersion, grouping_version),
                        () -> assertEquals(data.expectedNodes, nodes.size())
                );
            }
        }

        @Test
        void removeHmgroupsAndRelationsTest() {
            ArrayList<Node> nodes;
            try {
                Node node1 = GRAPH_DB.createNode(MRNA_LABEL);
                Node node2 = GRAPH_DB.createNode(MRNA_LABEL);
                node1.createRelationshipTo(node2, RelTypes.is_similar_to);
                nodes = new ArrayList<>(
                        Arrays.asList(node1, node2)
                );
                proteomeLayer.remove_hmgroups_and_relations(nodes);
                assertAll(
                        () -> assertFalse(GRAPH_DB.findNodes(MRNA_LABEL).hasNext()),
                        () -> assertFalse(GRAPH_DB.getAllRelationships().iterator().hasNext())
                );
            } catch(NotFoundException nfe) {
                fail("Unable to start database");
            }
        }

        @Test
        void deleteIsSimilarToRelationsTest() {
            total_genomes = 3;
            try {
                Node node1, node2, node3;
                node1 = node2 = node3 = GRAPH_DB.createNode(MRNA_LABEL);
                node1.setProperty("genome", 1);
                node2.setProperty("genome", 2);
                node3.setProperty("genome", 3);
                node1.createRelationshipTo(node2, RelTypes.is_similar_to);
                node1.createRelationshipTo(node3, RelTypes.is_similar_to);
                node2.createRelationshipTo(node3, RelTypes.is_similar_to);
                proteomeLayer.delete_is_similar_to_relations();
                assertAll(
                        () -> assertFalse(node1.hasRelationship()),
                        () -> assertFalse(node2.hasRelationship()),
                        () -> assertFalse(node3.hasRelationship())
                );
            } catch(NotFoundException nfe) {
                fail("Unable to start database");
            }
        }

        @Test
        void putGroupingVersionTest() {
            grouping_version = 1;
            try {
                Node pangenomeNode = createPangenomeNode();
                pangenomeNode.setProperty("highest_grouping", 0);
                proteomeLayer.setPangenomeNode(pangenomeNode);
                MIN_NORMALIZED_SIMILARITY = 1;
                MCL_INFLATION = 2;
                INTERSECTION_RATE = 3;
                CONTRAST = 4;
                longest_transcripts = true;
                skip_list = new ArrayList<>(Arrays.asList(1, 2, 3));
                proteomeLayer.put_grouping_version(true);
                String expected = "similarity: 1, inflation: 2.0, intersection rate 3.0, " +
                        "contrast: 4.0, Only longest gene transcripts";
                assertAll(
                        () -> assertEquals(1, pangenomeNode.getProperty("highest_grouping")),
                        () -> assertEquals(expected, pangenomeNode.getProperty("grouping_v1"))
                );
            } catch(NotFoundException nfe) {
                fail("Unable to start database");
            }
        }

        @Test
        void checkIfGroupingVExistsTest() {
            try {
                Node hmNode = GRAPH_DB.createNode(HOMOLOGY_GROUP_LABEL);
                hmNode.setProperty("group_version", 1);
                proteomeLayer.setPangenomeNode(pangenomeNode);
                ExitAssertions.assertExits(1, () -> proteomeLayer.check_if_grouping_v_exists(hmNode));
                pangenomeNode.setProperty("grouping_v1", "grouping_parameters");
                proteomeLayer.setPangenomeNode(pangenomeNode);
                proteomeLayer.check_if_grouping_v_exists(hmNode);
            } catch(NotFoundException nfe) {
                fail("Unable to start database");
            }
        }

        @Test
        void moveGroupingTest() {
            try {
                pangenomeNode.setProperty("num_genomes", 1);
                pangenomeNode.setProperty("grouping_v1", "grouping_parameters");

                Node hmNode1 = GRAPH_DB.createNode(HOMOLOGY_GROUP_LABEL);
                hmNode1.setProperty("group_version", 1);
                Node hmNode2 = GRAPH_DB.createNode(HOMOLOGY_GROUP_LABEL);
                hmNode2.setProperty("group_version", 1);
                hmNode1.createRelationshipTo(hmNode2, RelTypes.has_homolog);

                proteomeLayer.move_grouping(false);
                Relationship rel = GRAPH_DB.getAllRelationships().stream().iterator().next();

                assertAll(
                        () -> assertFalse(GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL).hasNext()),
                        () -> assertTrue(GRAPH_DB.findNodes(INACTIVE_HOMOLOGY_GROUP_LABEL).hasNext()),
                        () -> assertFalse(rel.isType(RelTypes.has_homolog)),
                        () -> assertTrue(rel.isType(RelTypes.has_inactive_homolog))
                );
            } catch(NotFoundException nfe) {
                fail("Unable to start database");
            }
        }

        @Test
        void stopIfHmgroupsExistTest() {
            try {
                GRAPH_DB.createNode(HOMOLOGY_GROUP_LABEL);
                ExitAssertions.assertExits(1, proteomeLayer::stop_if_hmgroups_exist);
            } catch(NotFoundException nfe) {
                fail("Unable to start database");
            }
        }

        @Test
        void removeComponentFromMrnaNodesTest() {
            total_genomes = 2;
            try {
                Node pangenomeNode = createPangenomeNode();
                pangenomeNode.setProperty("components_presents", true);
                for (int i = 1; i <= total_genomes; ++i) {
                    for (int j = 1; j <= 2; ++j) {
                        Node mrnaNode = GRAPH_DB.createNode(MRNA_LABEL);
                        mrnaNode.setProperty("genome", i);
                        mrnaNode.setProperty("component", 0);
                    }
                }
                proteomeLayer.remove_component_from_mrna_nodes(pangenomeNode);
                ResourceIterator<Node> mrnaNodes = GRAPH_DB.findNodes(MRNA_LABEL);
                while (mrnaNodes.hasNext()) {
                    Node mrnaNode = mrnaNodes.next();
                    assertFalse(mrnaNode.hasProperty("component"));
                }
            } catch(NotFoundException nfe) {
                fail("Unable to start database");
            }
        }

        @Test
        void getNumProteinsFromIndividualGenomesTest() {
            annotation_identifiers = new ArrayList<>(
                    Arrays.asList("1_1", "2_1")
            );
            try {
                for (int i = 1; i < 4; i++) {
                    Node annotationNode = GRAPH_DB.createNode(ANNOTATION_LABEL);
                    annotationNode.setProperty("identifier", i + "_" + 1);
                    annotationNode.setProperty("num_proteins", 100);
                }
                assertEquals(200, proteomeLayer.get_num_proteins_from_individual_genomes());
            } catch(NotFoundException nfe) {
                fail("Unable to start database");
            }
        }

        @Test
        void getNumberOfProteinsTest() { // does not test for longest_transcript or pangenome
            PROTEOME = true;
            longest_transcripts = false;
            try {
                pangenomeNode.setProperty("num_proteins", 100);
                proteomeLayer.setPangenomeNode(pangenomeNode);
                ByteArrayOutputStream outContent = new ByteArrayOutputStream();
                System.setOut(new PrintStream(outContent));
                proteomeLayer.get_number_of_proteins();
                String message = outContent.toString().replace("\n", "").replace("\r", "");
                outContent.reset();
                pangenomeNode.setProperty("num_proteins", 0);
                proteomeLayer.setPangenomeNode(pangenomeNode);

                assertAll (
                        () -> ExitAssertions.assertExits(1, () -> proteomeLayer.get_number_of_proteins()),
                        () -> assertEquals("Proteins = 100", message)
                );
            } catch(NotFoundException nfe) {
                fail("Unable to start database");
            }
        }

        @Test
        void checkHighestGroupingVersionTest() {
            try {
                int highest1 = ProteomeLayer.check_highest_grouping_version();
                pangenomeNode.setProperty("highest_grouping", 1);
                int highest2 = ProteomeLayer.check_highest_grouping_version();
                assertAll(
                        () -> assertEquals(0, highest1),
                        () -> assertEquals(1, highest2)
                );

            } catch(NotFoundException nfe) {
                fail("Unable to start database");
            }
        }

        @AfterEach
        void endTransaction() {
            tx.close();
        }

        @AfterEach
        void cleanDatabase() {
            tx = GRAPH_DB.beginTx();
            try {
                GRAPH_DB.execute("MATCH (n) DETACH DELETE n");
            } catch (NotFoundException nfe) {
                nfe.printStackTrace();
            }
            tx.close();
        }

        @AfterAll
        void databaseCleanup() throws IOException {
            GRAPH_DB.shutdown();
            //FileUtils.deleteDirectory(tempDirectory.toFile());
        }

    }

    /**
     * Method source class for showAvailableGroupingTest
     */
    static class ShowAvailableGroupingParameters {
        long totalGroups;
        boolean checkActiveGrouping;
        int[] groupingVersions;
        String expected;
        public ShowAvailableGroupingParameters (long groups, boolean check, int[] ver, String exp) {
            totalGroups = groups;
            checkActiveGrouping = check;
            groupingVersions = ver;
            expected = exp;
        }
    }

    /**
     * Method source class for retrieveHmgroupNodesForSelectedGroupingTest
     */
    static class RetrieveNodeParameters {
        String version;
        int groupingVersion;
        int expectedVersion;
        int expectedNodes;
        boolean expectExit;
        boolean active;
        boolean inactive;
        public RetrieveNodeParameters (String ver, int gv, int ev, int en, boolean ee, boolean a, boolean i) {
            version = ver;
            groupingVersion = gv;
            expectedVersion = ev;
            expectedNodes = en;
            expectExit = ee;
            active = a;
            inactive = i;
        }
    }

    /**
     * Retrieve the pangenome node from the graph database
     * @return pangenome node
     */
    Node createPangenomeNode() {
        return GRAPH_DB.createNode(PANGENOME_LABEL);
    }
}